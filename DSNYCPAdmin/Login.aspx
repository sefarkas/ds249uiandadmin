﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.Administrator.Login" Title="DSNY : DS-249 ADMIN" CodeBehind="Login.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
        <script language="javascript" type="text/javascript">

        $.when($.ready).then(function () 
        {
            hideLoader();
        });


        $("form").live("submit", function ()
        {
            // Strongly recommended: Hide loader after 20 seconds, even if the page hasn't finished loading
            setTimeout(hideLoader, 60 * 1000);
            ShowProgress();
        });

    </script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="900px" border="0" cellspacing="0" cellpadding="2" class="main" align="center">
        <tr>
            <td colspan="3" class="headertext">
                DSNY: DS-249 ADMIN<br />
                <img src="includes/img/border_top.gif" alt="" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table class="loginBox" cellpadding="14" align="center">
                    <tr>
                        <td>
                            PLEASE SIGN IN BELOW WITH
                            <br />
                            YOUR ID AND PASSWORD
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr class="loginBoxInternal" align="left">
                        <td>
                            <asp:Label ID="lMessage" runat="server" Font-Size="Small"></asp:Label>
                            <br />
                            Sign in ID*<br />
                            <asp:TextBox ID="txtUserName" runat="server" Width="275"></asp:TextBox><br />
                            <br />
                            Password*<br />
                            <asp:TextBox ID="txtPassword" runat="server" Width="275" TextMode="Password">ds249</asp:TextBox><br />
                            <br />
                            <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="includes/img/btn_login.gif"
                                OnClick="btnLogin_Click" />
                            <br />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Version: <font face="Arial" size="2"><span style="font-family: Arial; font-size: 10pt">
                                2.0.56<br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;06/30/2012</span></font>
                        </td>
                    </tr>
                </table>
                <table class="loginBoxLower" align="center">
                    <tr>
                        <td>
                                <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                                <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                    at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                        itServiceDesk@dsny.nyc.gov</a></span>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
