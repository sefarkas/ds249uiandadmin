﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" Inherits="DSNY.DSNYCP.Administrator.Complaints" Codebehind="Complaints.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="includes/css/styles1.css" rel="stylesheet" type="text/css" />
    <link href="includes/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="includes/css/CaseManagementStyle.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Pragma" content="no-cache" />
    <style type="text/css">
        .style23
        {
            width: 28%;
            text-align: left;
            font-size: 17px;
            font-weight: bold;
            font-family: Arial,Verdana,sans-serif;
        }
        .style24
        {
            width: 182px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 900px; height: 30px;" class="tableMargin" border="0px" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top">
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111  or email us
                                at <a href="mailto:itservicedesk@dsny.nyc.gov" title="Send us an email" style="color:#1e951f;">
                                    itServiceDesk@dsny.nyc.gov</a></span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        <td class="styleHeading">
            <div>
                <span>DS-249 COMPLAINT MANAGEMENT </span>
                
            </div>
        </td>
        <td class="styleHeading">
            <asp:ImageButton ID="btnLogout" ImageUrl="~/includes/img/btn_logout.gif" runat="server"
                    OnClick="btnLogout_Click" CausesValidation="False" />
        </td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
            <td colspan="2">
                <asp:Button ID="UserManagementButton" runat="server" 
                                Text="User Management" onclick="UserManagementButton_Click" CausesValidation="false" />
                                
            </td>
        </tr>
        
        
    </table>
    <asp:UpdatePanel ID="upButtons" runat="server">
        <ContentTemplate>
            <table style="width: 900px; height: 50px" class="tableMargin" border="0px" cellpadding="0px"
                cellspacing="0px" align="center">
                <tr>
                    <td class="style23" align="left">
                        SEARCH FOR COMPLAINTS&nbsp;&nbsp;
                    </td>
                    <td class="style24">
                        <asp:TextBox ID="txtSearch" runat="server" Width="172px" Height="17px" CssClass="inputFieldStyle"></asp:TextBox>
                    </td>
                    <td>
                        <asp:ImageButton ID="btnSearch" ImageUrl="~/includes/img/search_btn.gif" runat="server"
                            OnClick="btnSearch_Click" />
                        <asp:ImageButton ID="btnSearchCancel" ImageUrl="../../CommonUI/images/btn_cancel.gif" runat="server"
                            OnClick="btnSearchCancel_Click" CausesValidation="False" Visible="false" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table style="width: 900px;" border="0px" cellpadding="0px" cellspacing="0px" align="center">
        <tr>
            <td colspan="2" align="center">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:Label ID="lbMessage" runat="server" Width="50%" Font-Size="Small" Font-Bold="true"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                    runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                    <Animations>                                
                        <OnUpdated>
                            <FadeOut Duration="5.0" Fps="24" />
                        </OnUpdated>
                    </Animations>
                </cc1:UpdatePanelAnimationExtender>
            </td>
        </tr>
       
        </table>

      
    <table style="width: 900px;" class="tableMargin" cellpadding="0px" cellspacing="0px"
        align="center">
        <tr>
            <td background="includes/img/table_top.jpg" align="left" class="sort1" style="border-left-style: solid;
                border-left-width: 1px; border-left-color: #000000;">
                Complaint List in Order of Most Recent
            </td>
            <td background="includes/img/table_top.jpg" align="right" class="sort2" style="border-right-style: solid;
                border-right-width: 1px; border-right-color: #000000">
  Click column header to sort
            </td>
        </tr>
    </table>
    <!-- update panel -->
    <asp:UpdatePanel ID="upGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
        </Triggers>
        <ContentTemplate>
        <table id="tableCmb" style="width: 900px;" border="0px" cellpadding="0px" cellspacing="0px" align="center">
        <tr style ="height:5px;" >
          <td> </td>
         </tr>
        <tr>
          <td>
               <asp:Label ID="lblCmbCmpMsg" runat="server" Width="80%"  Font-Bold="True" 
                   Font-Size="Small" ForeColor="Red"></asp:Label>
         </td>
         </tr>
         <tr style ="height:5px;"><td></td></tr>
    </table>
            <table style="width: 900px" class="tableMargin" border="0px" cellpadding="0px" cellspacing="0px"
                align="center">
                <tr>
                    <td>
                        <div>
                            <asp:GridView ID="gvComplaints" runat="server" Width="900px" AutoGenerateColumns="False"
                                BorderColor="#235705" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a"
                                AllowSorting="True" PageSize="50" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Left"
                                AllowPaging="True" OnRowCommand="gvComplaints_RowCommand" OnRowDataBound="gvComplaints_RowDataBound"
                                OnSorting="gvComplaints_Sorting" OnPageIndexChanging="gvComplaints_PageIndexChanging"
                                OnDataBound="gvComplaints_DataBound" 
                                OnRowCreated="gvComplaints_RowCreated" 
                                EmptyDataText="Sorry. No Complaints Found." >
                                <EmptyDataRowStyle CssClass="fromdate" />
                                <PagerTemplate>
                                    <table style="width: 99%;" border="0px" cellpadding="0px" cellspacing="0px">
                                        <tr>
                                            <td style="text-align: right; font-family: Arial,Verdana,sans-serif; font-size: 1.2em;
                                                margin-left: 2px;">
                                                Page
                                            </td>
                                            <td style="width: 6%;">
                                                <asp:ImageButton ID="imgDoublePrevious" ImageUrl="~/includes/img/PreviousDoubleArrow.GIF"
                                                    CommandName="Page" CausesValidation="false" CommandArgument='<%#gvComplaints.PageIndex - 4%>'
                                                    runat="server" />
                                                <asp:ImageButton ID="imgPrevious" ImageUrl="~/includes/img/PreviousSingleArrow.GIF"
                                                    CommandName="Page" CausesValidation="false" CommandArgument="Prev" ToolTip="Previous Page"
                                                    runat="server" />
                                            </td>
                                            <td align="center" style="width: 10%;">
                                                <asp:Menu ID="menuPager" Orientation="Horizontal" OnMenuItemClick="menuPager_MenuItemClick"
                                                    Font-Size="1.2em" ForeColor="#1e951f" StaticSelectedStyle-ForeColor="Black" StaticSelectedStyle-BorderStyle="Solid"
                                                    StaticSelectedStyle-BorderWidth="1px" StaticSelectedStyle-BorderColor="#A4D49A"
                                                    runat="server" />
                                            </td>
                                            <td style="text-align: left; width: 6%">
                                                <asp:ImageButton ID="imgNext" runat="server" CausesValidation="false" CommandName="Page"
                                                    CommandArgument="Next" ToolTip="Next Page" ImageUrl="~/includes/img/NextSingleArrow.GIF" />
                                                <asp:ImageButton ID="imgDoubleNext" runat="server" CausesValidation="false" CommandName="Page"
                                                    CommandArgument='<%#gvComplaints.PageIndex + 6%>' ImageUrl="~/includes/img/NextDoubleArrow.GIF" />
                                            </td>
                                            <td style="font-family: Arial,Verdana,sans-serif; font-size: 1.2em; width: 7%;">
                                                of
                                                <%=gvComplaints.PageCount%>
                                            </td>
                                            <td style="width: 75%; text-align: right; margin-left: 0px; padding-left: 0px;">
                                                <asp:Label ID="lblPage" runat="server" Text="Jump to Page:" ForeColor="Black" Font-Names="Arial,Verdana,sans-serif"
                                                    Font-Size="1.2em"></asp:Label>
                                                <asp:TextBox ID="txtJmpToPage" runat="server" Width="20px" AutoPostBack="true" OnTextChanged="txtJmpToPage_TextChanged"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <PagerSettings Position="TopAndBottom"></PagerSettings>
                                <Columns>
                                    <asp:TemplateField HeaderText="Action">
                                        <ItemTemplate>
                                            <asp:Button ID="DeleteButton" runat="server" Text="Delete" CommandName="DeleteComplaint" CommandArgument='<%#Eval("ComplaintID") %>' />
                                            <cc1:ConfirmButtonExtender ID="DeleteConfirmButtonExtender" runat="server" ConfirmText="Are you sure you want to Delete?" TargetControlID="DeleteButton">
                                            </cc1:ConfirmButtonExtender>
                                            <asp:Button ID="UndeleteButton" runat="server" Text="Un Delete" CommandName="UnDelete" CommandArgument='<%#Eval("ComplaintID") %>' />
                                            <cc1:ConfirmButtonExtender ID="UndeleConfirmButtonExtender" runat="server" ConfirmText="Are you sure you want to Un Delete?" TargetControlID="UndeleteButton">
                                            </cc1:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="25px" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="25px" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="DATE" DataField="CreatedDate" SortExpression="CreatedDate"
                                        DataFormatString="{0:MMM dd, yyyy}" HtmlEncode="false">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="120px" />
                                        <ItemStyle Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em" BorderColor="#235705"
                                            BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"
                                            Width="120px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="INDEX #" DataField="IndexNo" SortExpression="IndexNo">
                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                            Font-Size="1.2em" Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="70px" />
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="70px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ComplaintStatus" Visible="true">
                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" ForeColor="Black"
                                            Font-Underline="False" Wrap="False" Width="80px" />
                                        <ItemStyle Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2" BorderColor="#235705"
                                            BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" Width="80px"
                                            Wrap="False" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="STATUS">
                                        <ItemTemplate>
                                            <asp:Button ID="PrintButton" runat="server" Text="Re-Open" CommandName="Reopen" CommandArgument='<%#Eval("ComplaintID") %>' />
                                             <cc1:ConfirmButtonExtender ID="DeleteConfirmButtonExtender1" runat="server" ConfirmText="Are you sure you want to re-open?" TargetControlID="PrintButton">
                                            </cc1:ConfirmButtonExtender>
                                        </ItemTemplate>
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="80px" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="80px" />
                                    </asp:TemplateField>               
                                    <asp:BoundField HeaderText="DISTRICT" DataField="LocationName" SortExpression="LocationName">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="200px" />
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Wrap="False" Width="200px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="NAME (F. LAST)" DataField="InitialAndLastName" SortExpression="LastName">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="120px" />
                                        <ItemStyle Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em" BorderColor="#235705"
                                            BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"
                                            Width="120px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="NAME (LAST)" DataField="LastName" SortExpression="LastName"
                                        Visible="false">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="125px" />
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Wrap="False" Width="125px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="CHARGES">
                                        <ItemTemplate>
                                            <asp:Image ID="imgStatus" runat="server" ImageUrl="~/includes/img/arrow.gif" />
                                            <cc1:PopupControlExtender ID="pceStatus" runat="server" DynamicServiceMethod="GetChargesList"
                                                DynamicContextKey='<%#Eval("ComplaintID") %>' DynamicControlID="Panel1" TargetControlID="imgStatus"
                                                PopupControlID="Panel1" Position="Bottom">
                                            </cc1:PopupControlExtender>
                                            <asp:Label ID="lbCharge" runat="server" Text="Label"></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="80px" />
                                        <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center"
                                            VerticalAlign="Middle" Width="80px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Charge" HeaderText="CHARGES" SortExpression="Charge">
                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                            Font-Size="1.2em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                            VerticalAlign="Middle" Width="80px" Wrap="False" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                            Font-Size="1.2em" HorizontalAlign="Center" VerticalAlign="Middle" Width="80px"
                                            Font-Bold="false" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="REFERENCE #" DataField="RefNo" SortExpression="RefNo">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="110px" />
                                        <ItemStyle Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em" BorderColor="#235705"
                                            BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"
                                            Width="110px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="AUTHOR" DataField="CreatedBy" SortExpression="CreatedBy">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="120px" />
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Wrap="False" Width="120px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="ROUTE" DataField="RoutingLocation" SortExpression="RoutingLocation">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="20px" />
                                        <ItemStyle Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em" BorderColor="#235705"
                                            BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False"
                                            Width="20px" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IsDeleted" />
                                    <asp:BoundField DataField="FinalStatuscd" SortExpression="FinalStatuscd">
                                       <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="0px" />
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Wrap="False" Width="0px" />
                                    </asp:BoundField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table style="width: 900px" class="tableMargin" border="0px" cellpadding="0px" cellspacing="0px"
        align="center">
        <tr>
            <td width="900px" background="~/includes/img/table_top.jpg" align="left" style="border-right-style: solid;
                border-left-style: solid; border-right-width: 1px; border-left-width: 1px; border-right-color: #000000;
                border-left-color: #000000">
                &nbsp
            </td>
        </tr>
    </table>
    <table style="width: 100%" border="0px" cellpadding="0px" cellspacing="0px">
        <tr>
            <td height="22" align="center" valign="middle" class="bggrey">
                <span align="center" class="font11white">
            Copyright 2008 The City of New York
        York
        </tr>
    </table>
    <asp:Panel ID="Panel1" runat="server">
    </asp:Panel>
    
</asp:Content>
