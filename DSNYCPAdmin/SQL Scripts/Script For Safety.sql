--Security service for authenticated user for the application specified

--Add Safety application and its corresponding roles
GO

insert [dbo].[tblMembership](MemberShipDesc,IsActive)
values('SAFETY',1)

GO
insert [dbo].[tblRoles](Roles)
values('SAFETY_OFFICER')

GO
insert [dbo].[tblRoles](Roles)
values('CHIEF')

GO

insert [dbo].[tblRoles](Roles)
values('DIST')

GO

insert [dbo].[tblRoles](Roles)
values('BORO')

GO

insert [dbo].[tblRoles](Roles)
values('SAFETY')

GO

insert [dbo].[tblRoles](Roles)
values('SAFETY_EO')


-- ========================================================
-- Author:		<Shiva Lakshmi Mahankali>
-- Create date: <04/04/2011>
-- Description:	<Roles Assigned To Authenticated User For Application>
-- ========================================================

-- exec [usp_AuthenticatedUserForApplication] 'pgupta', 'ds249','EMPLOYEEAPPLICATION'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_AuthenticatedUserForApplication]

@user VARCHAR(50),
@pwd VARCHAR(50),
@appname VARCHAR(50)

AS
BEGIN
	
	SET NOCOUNT ON;    
	DECLARE
	@hash VARBINARY (255),
	@Result INT,
	@PersonalID BIGINT,
	@IsTemperoryPassword BIT,
	@IsAdmin BIT,
	@LastLogin DATETIME,
	@UserID VARCHAR(50),
	@Email VARCHAR(50)
	
	SELECT @hash=Password, @PersonalID=PersonalID, 
	@IsTemperoryPassword=IsTempPassword,
	@IsAdmin = IsAdmin ,
	@LastLogin = LastLogin,
	@UserID = UserID,
	@Email = EmailID
	
	FROM tblds249Users
	WHERE RTRIM(UserID) = RTRIM(@user)
	AND IsDeleted=0
	SET @Result = pwdcompare (@pwd, @hash)	
	
	IF(@Result <> 1)
	BEGIN
		SET @PersonalID = -1
		SET @IsTemperoryPassword = 0
		SET @IsAdmin = 0	
		
	END
	
IF(@PersonalID>0)
	IF(@appname='EMPLOYEEAPPLICATION')
		BEGIN
			SET @appname='EMPLOYEE APPLICATION'
		END

	BEGIN
				SELECT		
				memroles.[PersonalID]
				,Firstname
				,LastName 
				,ds249.UserID 
				,[MembershipID]
				,[RoleID]
				,roles.Roles					
		FROM [dbo].[tblMembershipRoles] memRoles
		INNER JOIN tblRoles roles ON roles.Id =memRoles.RoleId 
		inner join tblPersons persons on memroles.PersonalID=persons.PersonID
		INNER JOIN tblDS249Users ds249 ON ds249.PersonalID =memRoles.PersonalID 
		WHERE memRoles.PersonalID =@PersonalID AND memRoles.MembershipID =(SELECT ID FROM tblMembership		WHERE MemberShipDesc=@appname)
	END
	
END
