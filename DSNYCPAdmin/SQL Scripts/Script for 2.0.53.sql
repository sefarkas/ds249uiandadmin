/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsForAdmin]    Script Date: 05/24/2012 13:46:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsForAdmin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsForAdmin]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 05/24/2012 13:46:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_ReopenOrCloseApprovedComplaint]    Script Date: 05/24/2012 13:46:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ReopenOrCloseApprovedComplaint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_ReopenOrCloseApprovedComplaint]
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary1]    Script Date: 05/24/2012 13:46:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RptGetDisciplinary1]
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary2]    Script Date: 05/24/2012 13:46:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RptGetDisciplinary2]
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary3]    Script Date: 05/24/2012 13:46:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RptGetDisciplinary3]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsSecurity]    Script Date: 05/24/2012 13:46:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsSecurity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsSecurity]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetDisposition]    Script Date: 05/24/2012 13:46:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnGetDisposition]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnGetDisposition]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetMedicalDisposition]    Script Date: 05/24/2012 13:46:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnGetMedicalDisposition]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnGetMedicalDisposition]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetBCADDisposition]    Script Date: 05/24/2012 13:46:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnGetBCADDisposition]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnGetBCADDisposition]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetAdvocateDisposition]    Script Date: 05/24/2012 13:46:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnGetAdvocateDisposition]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[fnGetAdvocateDisposition]
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetAdvocateDisposition]    Script Date: 05/24/2012 13:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnGetAdvocateDisposition]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fnGetAdvocateDisposition]
		(
			-- Add the parameters for the function here
			@IndexNo bigint
		)
		RETURNS varchar(1000)
		AS
		BEGIN
--			-- Declare the return variable here
			--declare @IndexNo bigint = 103727
			DECLARE @tblDisposition	 table
			(
				disposition varchar(1000)
			)
			
			
			-- Add the T-SQL statements to compute the return value here
			Insert into @tblDisposition
			select 
			isnull(
			 Case 
			 --Added for VOIDED
			 --when  a.FinalStatuscd=97 and a.NonPenaltyResultcd=''134'' and a.RoutingLocation=''B'' then  + ''VOIDED''
			 when b.Code_Name ='''' OR b.Code_Name IS null then ''''
			 
									 when b.Code_Category_Id=9 and b.Code_Name = ''REPRIMAND'' then  + ''REPRIMAND''

									  when b.Code_Category_Id=9 and b.Code_Name = ''SUSPENSION'' and a.Term>0  then CAST(a.Term AS varchar) + '' ''+ CAST(a.Term_Desc AS varchar)+ '' SUSPENSION'' 

									  when b.Code_Category_Id=9 and b.Code_Name = ''Vacation''   and a.Term>0 then CAST(a.Term AS varchar) + '' ''+ CAST(a.Term_Desc AS varchar)+ '' VACATION'' 
		                              
									  when b.Code_Category_Id=9 and b.Code_Name = ''FINED''  and a.datavalue >0 then  ''FINED $'' + CAST(a.DataValue AS varchar) 
		                              
									  when b.Code_Category_Id=9 and b.Code_Name = ''FINED''  and a.Term>0 then  CAST(a.Term AS varchar) + '' ''+  '' FINE '' + CAST(a.Term_Desc AS varchar)

		                              
									  when b.Code_Category_Id=9 and b.Code_Name = ''Fired'' then  + ''FIRED''
		                              
									  when b.Code_Category_Id=9 and b.Code_Name = ''Demoted'' then  + ''DEMOTED''
		                                                            
									  when b.Code_Category_Id=9 and b.Code_Name = ''Other'' then ''OTHER - ''+CAST(a.Comments as varchar(max))

		                              
									   when b.Code_Category_Id=9 and b.Code_Name = ''ACD - 6 Months'' then  + ''ACD - 6 MONTHS''
		                              
									   when b.Code_Category_Id=9 and b.Code_Name = ''ACD - 12 Months'' then  + ''ACD - 12 MONTHS''
		     
		                              
									   when b.Code_Category_Id=9 and b.Code_Name = ''Suspension Time Served'' then  + ''SUSPENSION TIME SERVED''
		                              
		                              
		                              
									  when b.Code_Category_Id=9 and b.Code_Name = ''Comp Time''  and a.Term>0  then CAST(a.Term AS varchar) + '' ''+ CAST(a.Term_Desc AS varchar)+'' COMP TIME''
									  
									  when b.Code_Category_Id=9 and b.Code_Name = ''Void/Dismissed'' then  + ''VOID/DISMISSED''
									  
									  when b.Code_Category_Id=9 and b.Code_Name = ''Last Chance Agreement - 24 months'' then  + ''LAST CHANCE AGREEMENT - 24 MONTHS''
									  when b.Code_Category_Id=9 and b.Code_Name = ''Lifetime substance abuse testing'' then  + ''LIFETIME SUBSTANCE ABUSE TETSING''

									  
									  when b.Code_Category_Id=9 and b.Code_Name = ''Commissioners Probation''  and  a.Term>0 then CAST(a.Term AS varchar) + '' ''+ CAST(a.Term_Desc AS varchar)	+ '' COMMISIONERS PROBABTION''	 
		             

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21) and b.Code_Name = ''REPRIMAND'' Then ''REPRIMAND''

									 


									  else '''' 
									 end 
									  -- Added to get comment for parent Index
									  ,'''') 
									  + isnull( '' see '' + cast(nullif(a.ParentIndexNo,0) as varchar(max)) ,'''')
		                              
									  as Disposition

		From 

		(  
		select distinct (c.IndexNo), C.RespondentFName, C.RespondentLName, C.CreatedDate

					, Case when c.RoutingLocation = ''A'' then ap.PenaltyTypeCd
					else ''1'' end as PenaltyCD
					, ap.DataValue,ap.Term,ap.Term_Desc,
					ap.comments,
					 ap.FineDays, ap.SuspensionDays,
		             
					 ap.AdvocatePenaltyID
					-- Added to get comment for parent Index
					, isnull(C.ParentIndexNo,'''') as ParentIndexNo
					,c.RoutingLocation
		From  tblDS249Complaints C inner  join tblDS249Charges ca on c.ComplaintId = ca.ComplaintId 

					inner join dbo.tblComplaintAdvocacy ac on ac.ComplaintId = ca.ComplaintId

					inner join dbo.tblAdvocatedPenalty ap on ac.AdvocacyCaseId = ap.AdvocacyCaseID

		where  (ac.IsAvailable is null)  and c.indexno = @IndexNo 
		)a

		left outer join 
		(select code_ID, Code_Category_ID, Code_Name from Code where Code_Category_Id in (9, 20,21)  ) b

		on a.PenaltyCD = b.Code_Id

		order by a.AdvocatePenaltyID
				
				DECLARE @Disposition varchar(max)
			
				Select @Disposition = isnull(@Disposition,'''') + Disposition + '', ''
				from @tblDisposition
				Where Disposition is not null or LTRIM(RTRIM(Disposition)) <> ''''
				
				--Added for VOIDED
				if  @Disposition like ''%VOIDED%''
					select @Disposition=''VOIDED.''

			
				SET @Disposition = left(@Disposition,len(@Disposition)-1)
			
			
			RETURN REPLACE(@Disposition,'', ,'','','')	

		END
		
--select dbo.fnGetDisposition(@IndexNo), 
--select dbo.fnGetAdvocateDisposition(103727)' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetBCADDisposition]    Script Date: 05/24/2012 13:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnGetBCADDisposition]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fnGetBCADDisposition]
		(
			-- Add the parameters for the function here
			@IndexNo bigint
		)
		RETURNS varchar(1000)
		AS
		BEGIN
			-- Declare the return variable here
			--declare @IndexNo bigint = 103727
			DECLARE @tblDisposition	 table
			(
				disposition varchar(1000)
			)
			
			
			-- Add the T-SQL statements to compute the return value here
			Insert into @tblDisposition
			select 
			isnull(
			 Case 
			 --Added for VOIDED
			 when  a.FinalStatuscd=97 and a.NonPenaltyResultcd=134 and a.RoutingLocation=''B'' then  + ''VOIDED''
			 when b.Code_Name ='''' OR b.Code_Name IS null then ''''
									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21) and b.Code_Name = ''REPRIMAND'' Then ''REPRIMAND''

									when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION'' 

											and ltrim(rtrim(a.Days)) ='''' and ltrim(rtrim(a.Hours))='''' and ltrim(rtrim(a.Minutes)) =''''  then   ''0 Days, 0 Hours, 0 Mins SUSPENSION''
									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION'' 

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes <>''''  then  a.days + '' Days, '' + a.Hours + '' Hours, '' + a.Minutes + '' Mins SUSPENSION''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes =''''  then a.days + '' Days, '' + a.Hours+ '' Hours SUSPENSION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days <>'''' and a.Hours='''' and a.Minutes =''''  then a.days + '' Days SUSPENSION''    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days ='''' and a.Hours<>'''' and a.Minutes <>''''  then a.Hours + '' Hours, '' + a.Minutes + '' Mins SUSPENSION''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days ='''' and a.Hours<>'''' and a.Minutes =''''  then a.Hours + '' Hours SUSPENSION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days ='''' and a.Hours='''' and a.Minutes <>''''  then a.Minutes + '' Mins SUSPENSION''  

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days <>'''' and a.Hours='''' and a.Minutes <>''''  then a.Days + '' Days, '' + a.Minutes + '' Mins SUSPENSION''  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION'' 

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins VACATION''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours VACATION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days <>'''' and a.Hours='''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days VACATION''    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days ='''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins VACATION''

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days ='''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.Hours as varchar)+ '' Hours VACATION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days ='''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Minutes as varchar)+ '' Mins VACATION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days <>'''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Days as varchar)+ '' Days, '' + CAST(a.Minutes as varchar)+ '' Mins VACATION''  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime'' 

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins HOLIDAY/CompTime''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours HOLIDAY/CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days <>'''' and a.Hours='''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days HOLIDAY/CompTime''    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days ='''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins HOLIDAY/CompTime''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days ='''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.Hours as varchar)+ '' Hours HOLIDAY/CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days ='''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Minutes as varchar)+ '' Mins HOLIDAY/CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days <>'''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Days as varchar)+ '' Days, '' + CAST(a.Minutes as varchar)+ '' Mins HOLIDAY/CompTime''  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime'' 

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins CompTime''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days <>'''' and a.Hours='''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days CompTime''    

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days ='''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins CompTime''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days ='''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.Hours as varchar)+ '' Hours CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days ='''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Minutes as varchar)+ '' Mins CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days <>'''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Days as varchar)+ '' Days, '' + CAST(a.Minutes as varchar)+ '' Mins CompTime''  

										--Added comments to be added in the history section view model of DS249
										
										when b.Code_Category_Id=21  and b.Code_Name = ''FINED'' 

											and a.MoneyValue <>'''' then +''$''+CAST(a.MoneyValue as varchar)+'' FINED''


									  else '''' end 
									  -- Added to get comment for parent Index
									  ,'''') 
									  + isnull( '' see '' + cast(nullif(a.ParentIndexNo,0) as varchar(max)) ,'''')
		                              
									  as Disposition
		From 

		(  

		select distinct (c.IndexNo), C.RespondentFName, C.RespondentLName, C.CreatedDate

					,  bp.BCADPenaltyTypecd	as PenaltyCD				

					, bp.Days, bp.Hours, bp.Minutes,bp.MoneyValue
					-- Added to get comment for parent Index
					, isnull(C.ParentIndexNo,'''') as ParentIndexNo
					--Added for VOIDED
					,bc.FinalStatuscd
					--Added for VOIDED
		            ,bc.NonPenaltyResultcd       
					--Added for VOIDED
					,c.RoutingLocation
					,bp.BCADPenaltyId

		From  tblDS249Complaints C inner  join tblDS249Charges ca on c.ComplaintId = ca.ComplaintId 

					inner join dbo.tblBCADComplaint bc on c.ComplaintId = bc.ComplaintId

					inner join dbo.tblBCADPenalty bp on bc.BCADCaseId =bp.PenaltyGroupId
					
					

		where  bc.IsAvailable is null  and c.indexno = @IndexNo 
		)a

		left outer join 

		 

		(select code_ID, Code_Category_ID, Code_Name from Code where Code_Category_Id in (9, 20,21)  ) b

		on a.PenaltyCD = b.Code_Id

		order by a.BCADPenaltyId
				
				DECLARE @Disposition varchar(200)
			
				Select @Disposition = isnull(@Disposition,'''') + Disposition + '', ''
				from @tblDisposition
				Where Disposition is not null or LTRIM(RTRIM(Disposition)) <> ''''
				
				--Added for VOIDED
				if  @Disposition like ''%VOIDED%''
					select @Disposition=''VOIDED.''

			
				SET @Disposition = left(@Disposition,len(@Disposition)-1)
			--select @Disposition
			RETURN REPLACE(@Disposition,'', ,'','','')
			 IF (substring(@Disposition,len(@Disposition),len(@Disposition)))='',''
			
		SET @Disposition=REPLACE(@Disposition,(substring(@Disposition,len(@Disposition),len(@Disposition))),''.'')

		ELSE IF (substring(@Disposition,len(@Disposition)+1,len(@Disposition)))=''''
			
		SET @Disposition=REPLACE(@Disposition,(substring(@Disposition,len(@Disposition),len(@Disposition))),''.'')	
			
			RETURN REPLACE(@Disposition,'', ,'','','')	

		END
		

--select dbo.fnGetDisposition(103727)
--select dbo.fnGetbcadDisposition(103727)' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetMedicalDisposition]    Script Date: 05/24/2012 13:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnGetMedicalDisposition]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fnGetMedicalDisposition]
		(
			-- Add the parameters for the function here
			@IndexNo bigint
		)
		RETURNS varchar(1000)
		AS
		BEGIN
--			-- Declare the return variable here
			--declare @IndexNo bigint = 103727
			DECLARE @tblDisposition	 table
			(
				disposition varchar(1000)
			)
			
			
			-- Add the T-SQL statements to compute the return value here
			Insert into @tblDisposition
			select 
			isnull(
			 Case 
			 --Added for VOIDED
			
			 when b.Code_Name ='''' OR b.Code_Name IS null then ''''
									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21) and b.Code_Name = ''REPRIMAND'' Then ''REPRIMAND''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION'' 

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins SUSPENSION''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours SUSPENSION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days <>'''' and a.Hours='''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days SUSPENSION''    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days ='''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins SUSPENSION''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days ='''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.Hours as varchar)+ '' Hours SUSPENSION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days ='''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Minutes as varchar)+ '' Mins SUSPENSION''  

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days <>'''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Days as varchar)+ '' Days, '' + CAST(a.Minutes as varchar)+ '' Mins SUSPENSION''  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION'' 

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins VACATION''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours VACATION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days <>'''' and a.Hours='''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days VACATION''    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days ='''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins VACATION''

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days ='''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.Hours as varchar)+ '' Hours VACATION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days ='''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Minutes as varchar)+ '' Mins VACATION''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''VACATION''     

											and a.Days <>'''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Days as varchar)+ '' Days, '' + CAST(a.Minutes as varchar)+ '' Mins VACATION''  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime'' 

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins HOLIDAY/CompTime''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours HOLIDAY/CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days <>'''' and a.Hours='''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days HOLIDAY/CompTime''    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days ='''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins HOLIDAY/CompTime''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days ='''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.Hours as varchar)+ '' Hours HOLIDAY/CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''SUSPENSION''   

											and a.Days ='''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Minutes as varchar)+ '' Mins HOLIDAY/CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''HOLIDAY/CompTime''      

											and a.Days <>'''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Days as varchar)+ '' Days, '' + CAST(a.Minutes as varchar)+ '' Mins HOLIDAY/CompTime''  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime'' 

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins CompTime''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days <>'''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days, '' + CAST(a.Hours as varchar)+ '' Hours CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days <>'''' and a.Hours='''' and a.Minutes =''''  then CAST(a.days as varchar)+ '' Days CompTime''    

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days ='''' and a.Hours<>'''' and a.Minutes <>''''  then CAST(a.Hours as varchar)+ '' Hours, '' + CAST(a.Minutes as varchar)+ '' Mins CompTime''

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days ='''' and a.Hours<>'''' and a.Minutes =''''  then CAST(a.Hours as varchar)+ '' Hours CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days ='''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Minutes as varchar)+ '' Mins CompTime''  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = ''CompTime''     

											and a.Days <>'''' and a.Hours='''' and a.Minutes <>''''  then CAST(a.Days as varchar)+ '' Days, '' + CAST(a.Minutes as varchar)+ '' Mins CompTime''  

										--Added comments to be added in the history section view model of DS249
										
										when b.Code_Category_Id=21  and b.Code_Name = ''FINED'' 

											and a.MoneyValue <>'''' then +''$''+CAST(a.MoneyValue as varchar)+'' FINED''


									  else '''' end 
									  -- Added to get comment for parent Index
									  ,'''') 
									  + isnull( '' see '' + cast(nullif(a.ParentIndexNo,0) as varchar(max)) ,'''')
		                              
									  as Disposition
		                               
		                              
		                              

		From 

		(  
		
		select distinct (c.IndexNo), C.RespondentFName, C.RespondentLName, C.CreatedDate, 
							 mp.MedicalPenaltyTypecd as PenaltyCD
					,mp.MedicalPenaltyId		            
					--The below line of code is added to display the dispositions for the medicals.
					, mp.Days, mp.Hours, mp.Minutes,mp.MoneyValue  
					-- Added to get comment for parent Index         
		, isnull(C.ParentIndexNo,'''') as ParentIndexNo
		--Added for VOIDED
		,mc.FinalStatuscd
		--Added for VOIDED
		 ,mc.NonPenaltyResultcd
		--Added for VOIDED
		,c.RoutingLocation
		From  tblDS249Complaints C inner  join tblDS249Charges ca on c.ComplaintId = ca.ComplaintId 
		inner join dbo.tblMedicalComplaint mc on c.ComplaintId = mc.ComplaintId
		inner join dbo.tblMedicalPenalty mp on mc.MedicalCaseId =mp.PenaltyGroupId
		        
where  mc.IsAvailable is null 
--and (ac.IsAvailable is null ) 
and c.indexno =@IndexNo
		
		)a

		left outer join 
		(select code_ID, Code_Category_ID, Code_Name from Code where Code_Category_Id in (9, 20,21)  ) b

		on a.PenaltyCD = b.Code_Id

		order by a.MedicalPenaltyId
				
				DECLARE @Disposition varchar(max)
			
				Select @Disposition = isnull(@Disposition,'''') + Disposition + '', ''
				from @tblDisposition
				Where Disposition is not null or LTRIM(RTRIM(Disposition)) <> ''''
			
				SET @Disposition = left(@Disposition,len(@Disposition)-1)
			
			
			RETURN REPLACE(@Disposition,'', ,'','','')	

		END
		
--select dbo.fnGetDisposition(@IndexNo)' 
END
GO
/****** Object:  UserDefinedFunction [dbo].[fnGetDisposition]    Script Date: 05/24/2012 13:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[fnGetDisposition]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'CREATE FUNCTION [dbo].[fnGetDisposition]
		(
			-- Add the parameters for the function here
			@IndexNo bigint
		)
		RETURNS varchar(max)
		AS
		BEGIN

declare @Disposition varchar(max)			
declare @RoutingLocation char(1)
select @RoutingLocation = c.RoutingLocation from tblds249complaints	c	where IndexNo = @IndexNo

			if(@RoutingLocation =''A'')
			begin
				set @Disposition = dbo.fnGetAdvocateDisposition(@IndexNo)
			end
			else if(@RoutingLocation =''B'')
			begin
				set @Disposition = dbo.fnGetBCADDisposition(@IndexNo)
			end
			else if(@RoutingLocation =''M'')
			begin 
				set @Disposition = dbo.fnGetMedicalDisposition(@IndexNo)
			end
			
			RETURN REPLACE(@Disposition,'', ,'','','')	

		END
		
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsSecurity]    Script Date: 05/24/2012 13:46:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsSecurity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
--[dbo].[usp_GetComplaintsSecurityDistinctComplaintBackupTestDipeshGill] 107
CREATE PROCEDURE [dbo].[usp_GetComplaintsSecurity]
	-- Add the parameters for the stored procedure here
@CreatedBy bigint = 0

AS
BEGIN

	SET NOCOUNT ON;
  declare @count int
   
  set @count=0;

  Declare @locationid int	
	
	select @locationid=LocationID from tblEmployeeLocations Where EmployeeID=@CreatedBy and IsCurrent=1
 
  
  select @count=COUNT(*) from tblMembershipRoles where PersonalID=@CreatedBy and tblMembershipRoles.RoleId=
  (Select tblRoles.Id from tblRoles where tblRoles.Roles=''Commissioner Access'')

  if @count>=1
     Begin
        SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as ''RoutingLocation''
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''      

      ,[CreatedDate]
      , '''' as ''Status''
     ,charges as ''Charges''
	,Chargesdesc as ''ChargesDesc''
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as ''Indicator'',
      a.IsDeleted,
     a.Jt1
 FROM [dbo].[tblDS249Complaints] a 

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  


 
 UNION 
 
 SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
	  ,a.[BoroughID]
	  ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as ''RoutingLocation''
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''      
     
      ,[CreatedDate]
      , '''' as ''Status''
     ,charges as ''Charges''
	,Chargesdesc as ''ChargesDesc''
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as ''Indicator'',
      a.IsDeleted,
     a.Jt1
 FROM [dbo].[tblDS249Complaints] a 

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID   
 where 

  CreatedBy = case when @createdBy = 0 then
					CreatedBy
				Else 
					@createdBy
				end

  Order by ComplaintId desc
     
     
     End
  
  Else
  
     Begin	
     

	WITH tmpCTE (LocationID, ParentLocationID) AS
	(
		SELECT LocationID,ParentLocationID 
		FROM tbllocations
		WHERE locationID = (select LocationID from tblEmployeeLocations Where EmployeeID=@CreatedBy and IsCurrent=1)
		UNION ALL
		SELECT l.LocationID, l.ParentLocationID
		FROM tbllocations l INNER JOIN tmpCTE t	
		ON l.ParentLocationID = t.locationID
	)

	select tmpCTE.LocationID,tmpCTE.ParentLocationID into #TBLTEMPLOCATION FROM tmpCTE 

	SELECT tblEmployeeLocations.EmployeeID INTO #TBLTEMPORARY FROM #TBLTEMPLOCATION left outer join tblEmployeeLocations on #TBLTEMPLOCATION.LocationID=tblEmployeeLocations.LocationID
	where tblEmployeeLocations.IsCurrent=1


select distinct  ComplaintId
from 
(

SELECT 
		a.ComplaintId
		
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
 where a.ComplaintId not in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId))

 
 UNION All

 SELECT  
		a.ComplaintId
		
     
 FROM [dbo].[tblDS249Complaints] a 

 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

 a.ComplaintId  in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId) 
 where hc.EmployeeId=@CreatedBy)
 
 
 union All
 
 --- Select those complaints whose charges not
 -- if charges is created by that person.
-- done  # 3 0
 SELECT 
		a.ComplaintId
		
  
 FROM [dbo].[tblDS249Complaints] a 

 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

  c.PersonID = @CreatedBy
  and a.ComplaintId  in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId))


  
 UNION All
 

   SELECT 
		a.ComplaintId
		
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.RespondentEmpID

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
 where a.ComplaintId not in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId))

 
 UNION All
-- done #5 0 
 SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 

 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.RespondentEmpID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

  a.ComplaintId  in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId) 
 where hc.EmployeeId=@CreatedBy)

   UNION  All
   
--***************** Select the complaint Filed at particular Locations
-- done #6 24487
	SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN tblDS249Charges on a.ComplaintId=tblDS249Charges.ComplaintId

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
 where a.ComplaintId not in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=Convert(int,hc.ChargeTypeId))
 and tblDS249Charges.IncidentLocationID in (select #TBLTEMPLOCATION.LocationID from #TBLTEMPLOCATION)

 union All
 -- done #7 0	
 SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 

 inner JOIN tblDS249Charges on a.ComplaintId=tblDS249Charges.ComplaintId
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

  a.ComplaintId  in 
 (
select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=Convert(int,hc.ChargeTypeId) 
 where hc.EmployeeId=@CreatedBy)
 and tblDS249Charges.IncidentLocationID in (select #TBLTEMPLOCATION.LocationID from #TBLTEMPLOCATION)

  union all
  

		SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPLOCATION on a.LocationId=#TBLTEMPLOCATION.locationid

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
 where a.ComplaintId not in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId))

 union All
-- done  #9 0
 SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 

  inner JOIN #TBLTEMPLOCATION on a.LocationId=#TBLTEMPLOCATION.locationid
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

 a.ComplaintId  in 
 (
select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId) 
 where hc.EmployeeId=@CreatedBy)

  ) tbl order by ComplaintId

  
  drop table #TBLTEMPLOCATION
  drop table #TBLTEMPORARY
  


End  
  
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary3]    Script Date: 05/24/2012 13:46:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary3]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_RptGetDisciplinary3]
	-- Add the parameters for the stored procedure here
		
	@FromDate date,
	@ToDate date
	
AS
BEGIN
select distinct p.FirstName+ '' ''+p.LastName Name ,
		CONVERT(date,adv.HearingDt) HearingDate  
		from tblDS249Complaints comp inner join
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
				on ca.AdvocacyCaseId=ah.AdvocacyCaseID
				where ah.CalStatusCd  in (47) and  
	 HearingTypeCd =84	and		
	  --HearingStatusCd =54 
	  --and
	  -- Actioncd =44 
	  --AND
					 HearingDt >=cast(@FromDate  as date)
	 and HearingDt <=cast(@ToDate  as DATE)
 		        and YEAR(ah.HearingDt) =year(@ToDate)
			    and ca.IsAvailable is null
			    and ah.HearingDt is not null			    
			    ) adv
			   
		on comp.ComplaintId=adv.ComplaintId
		inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		inner join tblPersons p on p.PersonID=emp.PersonID		 
		order by HearingDate asc 
END















' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary2]    Script Date: 05/24/2012 13:46:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_RptGetDisciplinary2]
	
-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	Declare @CountAdvocateYearly INT 
	Declare @CountFieldYearly INT 
	Declare @CountMedicalYearly INT 
	Declare @CountSSLUYearly INT 
	Declare @CountDATYearly INT 
	Declare @CountTrainingYearly INT 
	Declare @Month int
	Declare @Year int
	SET NOCOUNT ON;
	declare @yearStart DATE
	SET @yearStart=CONVERT(DATE, ''01/01/''+ CONVERT(VARCHAR,YEAR(CONVERT(DATE, @FromDate))));
    PRINT @yearStart;
	--************************************ 
	 select @CountAdvocateYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (519,512)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR”( 8275760) 
	 and complanant.ComplainantType=''C''
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print ''@CountAdvocateYearly:''+ convert(varchar, @CountAdvocateYearly)
	 
	  
	 select @CountFieldYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging Field
	    and complanant.ComplainantType=''C''
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print ''@CountFieldYearly:''+ convert(varchar, @CountFieldYearly)
	 
	  
	 select @CountMedicalYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 		
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging 	MEDICAL OFFICE 
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print ''@CountMedicalYearly:''+ convert(varchar, @CountMedicalYearly)
	 
	   select @CountSSLUYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 		
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print ''@CountSSLUYearly:''+ convert(varchar, @CountSSLUYearly)
	 
	 set @CountDATYearly=0
	
	 	 
	select @CountTrainingYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  --16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print ''@CountTrainingYearly:''+ convert(varchar, @CountTrainingYearly)

-- Complaints disposed 

	Declare @AwaitingTrialDateAtOath int
	Declare @CountCasesScheduled int
	Declare @CountCasesHeard int	
	Declare @CountDispostionFromOath int 
	
	Declare @CountAwaitingAdjudicationComplaintsNo int
	Declare @CountAwaitingAdjudicationComplaintsEmp int
	Declare @CountAwaitingOathTrialComplaintsNo int
	Declare @CountAwaitingOathTrialComplaintsEmp int

	
	
	--Updated on 5/22/2012
	
	--Awaiting trial date at OATH
	
	select @AwaitingTrialDateAtOath=COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
	ca.FinalStatuscd=96  -- Open Complaints
	 	and ah.HearingStatusCd=65 	
	and ca.IsAvailable is null

	

	
	--Updated on 5/22/2012
	
	--Cases scheduled for 
	
	
	select   @CountCasesScheduled=COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ah.CalStatusCd=47   -- SCHED			
		and HearingDt >=cast(@FromDate as DATE)
	and HearingDt <=cast(@ToDate as DATE)
	and ca.IsAvailable is null
	
	--Updated on 5/22/2012
	
	--Cases heard for 
	
	
	
select @CountCasesHeard= COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
		 ah.HearingStatusCd in (54,55,56,57,58)   -- SCHED			
		and HearingDt >=cast(@FromDate  as DATE)
	and HearingDt <=cast(@ToDate as DATE)
	and ca.IsAvailable is null
	
	

--Updated on 5/22/2012
	
	--Cases SCHEDULED for OATH
   
	
	If(MONTH(@ToDate)<=11)
	
		BEGIN
			set @Month=MONTH(@ToDate)+1
			set @Year=YEAR(@ToDate)
		END
	ELSE
			BEGIN
				set @Month=1
				set @Year=YEAR(@ToDate)+1
			END
	
	

select distinct CONVERT(date,HearingDt) HearingDt into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where month(HearingDt)=@Month 
	and year(HearingDt) =@Year
	and ca.IsAvailable is null	
	and  ah.CalStatusCd in (47,50)




--select * from #tblHearingDate 

	Declare @CaseScheduledDates varchar(800)
		select @CaseScheduledDates= 
		coalesce(@CaseScheduledDates+'','','''')+ CONVERT(varchar, HearingDt,101) 
		from   #tblHearingDate 
   print ''@CaseScheduledDates''	+ @CaseScheduledDates
	
	
		
--Cases Awaiting Disposition
		
		select @CountDispostionFromOath=COUNT(*) 
				from tblDS249Complaints comp inner join
				
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
			on ca.AdvocacyCaseId=ah.AdvocacyCaseID

			where 
 			 ah.HearingStatusCd=55   -- H Held Desc Reserved.
 			and HearingDt >=cast(@FromDate as DATE)
			and HearingDt <=cast(@ToDate as DATE)
			and ca.IsAvailable is null
			
			) adv
			on comp.ComplaintId=adv.ComplaintId
			inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
			inner join tblPersons p on p.PersonID=emp.PersonID
	
			
		
		
		
	--Updated on 5/22/2012
	
	--Awaiting Adjudictaion at Dept Trials-Complaints
	
	
	select @CountAwaitingAdjudicationComplaintsNo=COUNT(*) from tblComplaintAdvocacy ca
inner join tblAdvocateHearing ah on ah.AdvocacyCaseID=ca.AdvocacyCaseId 
inner join tblDS249Complaints comp on comp.ComplaintId =ca.ComplaintId 
where FinalStatuscd in (96) and HearingDt is null and IsAvailable is null

	
	
	--Updated on 5/22/2012
	
	--Awaiting Adjudictaion at Dept Trials-Employees	
	
	
	select @CountAwaitingAdjudicationComplaintsEmp=COUNT(distinct RespondentEmpID ) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca	on comp.ComplaintId=ca.ComplaintId and 
ActionDate >=cast(@FromDate as DATE)
and ActionDate<=cast(@ToDate as DATE) where FinalStatuscd in (96)

		--Updated on 5/22/2012
	
	--Awaiting OATH Trial at Dept Trials-Complaints	
		
		
	select  @CountAwaitingOathTrialComplaintsNo=COUNT(distinct ca.AdvocacyCaseId) 
from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
on ca.AdvocacyCaseId=ah.AdvocacyCaseID
where ah.HearingStatusCd in (65)   
and FinalStatuscd in (96)
					
and ca.IsAvailable is null
	
	
--Updated on 5/22/2012
	
--Awaiting OATH Trial at Dept Trials-Employees
			
	select  @CountAwaitingOathTrialComplaintsEmp=COUNT(distinct comp.RespondentEmpID) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
			on comp.ComplaintId=ca.ComplaintId
			inner join
			(
			select distinct ca.AdvocacyCaseId 
			from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
			on ca.AdvocacyCaseId=ah.AdvocacyCaseID
			where ah.HearingStatusCd in (65)   
			and FinalStatuscd in (96)		
			and ca.IsAvailable is null
			) adv on ca.AdvocacyCaseId=adv.AdvocacyCaseId

	
	
		Declare @DiscretionaryReport table
     (
      CountAdvocateYearly int,
      CountMedicalYearly int,
      CountSSLUYearly int,
      CountDATYearly int,
      CountTrainingYearly INT,
      CountFieldYearly int,
	  AwaitingTrialDateAtOath int,
	  CountCasesScheduled int,
	  CountCasesHeard int	,
      CountDispostionFromOath int, 
	  CaseScheduledDates varchar(800),
      CountAwaitingAdjudicationComplaintsNo int,
	  CountAwaitingAdjudicationComplaintsEmp int,
	  CountAwaitingOathTrialComplaintsNo int,
	  CountAwaitingOathTrialComplaintsEmp int
      )


  insert @DiscretionaryReport 
  values(@CountAdvocateYearly,@CountMedicalYearly,@CountSSLUYearly,
   @CountDATYearly,@CountTrainingYearly,@CountFieldYearly,
   @AwaitingTrialDateAtOath ,@CountCasesScheduled ,
   @CountCasesHeard ,@CountDispostionFromOath ,
   @CaseScheduledDates ,@CountAwaitingAdjudicationComplaintsNo ,
   @CountAwaitingAdjudicationComplaintsEmp ,
   @CountAwaitingOathTrialComplaintsNo,
   @CountAwaitingOathTrialComplaintsEmp
  )
  select * from @DiscretionaryReport
	

END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary1]    Script Date: 05/24/2012 13:46:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_RptGetDisciplinary1]
	-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @CountNotAdjudicated INT 
	Declare @ComplaintsReceived INT 
	Declare @CountAdvocateMonthly INT 
	Declare @CountFieldMonthly INT 
	Declare @CountMedicalMonthly INT 
	Declare @CountSSLUMonthly INT 
	Declare @CountDATMonthly INT 
	Declare @CountTrainingMonthly INT 
	SET NOCOUNT ON;
	
	
    SELECT @CountNotAdjudicated=COUNT(*) 
	FROM tblComplaintAdvocacy 
	WHERE RecieveDate is not null
	and ModifiedDate is null and FinalStatuscd=96
	 and RecieveDate <=cast(@ToDate as DATE)
    


	 
	  SELECT @ComplaintsReceived=COUNT( distinct ca.ComplaintId)
	 FROM tblComplaintAdvocacy ca inner join tblDS249Complainant cn
	 on ca.ComplaintId=cn.ComplaintId
	 WHERE FinalStatuscd in (96,97)
		and IsAvailable is null 
	   and RecieveDate>=@FromDate
		and RecieveDate<=@ToDate 
		and cn.ComplainantType=''C''
		and cn.LocationId is not null 
		and cn.LocationId <>-1
		
		
	
	--************************************ 
	 select @CountAdvocateMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.RecieveDate>=@FromDate 
		and compAdvocacy.RecieveDate<=@ToDate 
		and complanant.LocationId in (512,519)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR”( 8275760) 
	 and complanant.ComplainantType=''C''
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	
	 
	  
	 select @CountFieldMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null		
	and compAdvocacy.RecieveDate>=@FromDate 
		and compAdvocacy.RecieveDate<=@ToDate 
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging to field
	    and complanant.ComplainantType=''C''
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	
	 
	  
	 select @CountMedicalMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging to MEDICAL 
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	
	 
	   select @CountSSLUMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.RecieveDate>=@FromDate 
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 
	 
	 set @CountDATMonthly=0
	
		 
		select @CountTrainingMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
		on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate 
		and complanant.LocationId in (14,15,16)  -- 16 Location belonging Training and 14,15 are child locations
		and complanant.ComplainantType=''C''
		and complanant.LocationId is not null 
		and complanant.LocationId <>-1

-- Complaints disposed 

	Declare @CountPleadGuilty int
	Declare @CountDismissal int
	Declare @CountBCAD int
	Declare @CountInformalConferences int
	Declare @CountVoids int
	Declare @Retired_Resigned_Terminated int
	Declare @TotalComplaintDisposed int
	Declare @ComplaintRemainingOpened int
	Declare @BcadRouteCount int
    Declare @BcadActionCount int


	select @CountPleadGuilty = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=64   -- Plead Guilty
	and ah.HearingDt>=cast(@FromDate  as DATE)
	and ah.HearingDt<=cast(@ToDate as DATE)
	and ca.IsAvailable is null
	
		select @CountDismissal = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		where ca.FinalStatuscd=97  -- Closed Complaints
		and ah.HearingStatusCd=62   -- Motion to Dismiss
		and ah.HearingDt>=@FromDate 
		and ah.HearingDt<=@ToDate
		and ca.IsAvailable is null


		select @CountBCAD= COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
		where Actioncd=39 
		and ActionDate>=@FromDate 
		and ActionDate<=@ToDate 
	
				
		select @CountInformalConferences = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
		where Actioncd=40 
		and	FinalStatuscd=97 
		and IsAvailable is null
		and ActionDate>=@FromDate 
		and ActionDate<=@ToDate 


		select @CountVoids = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
		where Actioncd=41 
		and	FinalStatuscd=97 
		and IsAvailable is null
		and ActionDate>=@FromDate
		and ActionDate<=@ToDate 


		select @Retired_Resigned_Terminated = COUNT(distinct ca.AdvocacyCaseId) from    
		 tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		where ca.FinalStatuscd=97  -- Closed Complaints
		and (ah.HearingStatusCd=71 or ah.HearingStatusCd=67 or ah.HearingStatusCd=165 or ah.   HearingStatusCd=166) -- Retired /Resigned /Terminated	
		and ca.IsAvailable is null
		and HearingDt>=@FromDate
		and HearingDt<=@ToDate 


		set @TotalComplaintDisposed=@CountPleadGuilty+@CountDismissal+@CountBCAD+
		@CountInformalConferences+@CountVoids + @Retired_Resigned_Terminated
	
	
	select @ComplaintRemainingOpened = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=96 
	and IsAvailable is null	
	and convert(date,ModifiedDate,101) <=@ToDate


		select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
		from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		where ca.FinalStatuscd=96  -- Open Complaints
		and ah.HearingDt >=@FromDate 
		and ah.HearingDt <=@ToDate
		and ca.IsAvailable is null		
		and ah.CalStatusCd= 50 --SCHED


		Declare @ConferenceCalendarDates varchar(800)
		select @ConferenceCalendarDates= 
		coalesce(@ConferenceCalendarDates+'','','''') +  convert( varchar,HearingDt,101) --cast
		from   #tblHearingDate 

	Declare @DiscretionaryReport table
     (CountNotAdjudicated int ,
      ComplaintsReceived int,
      CountAdvocateMonthly int,
      CountMedicalMonthly int,
      CountSSLUMonthly int,
      CountDATMonthly int,
      CountTrainingMonthly int,
      CountFieldMonthly int,
      CountPleadGuilty int,
	  CountDismissal int,
	  CountBCAD int,
	 CountInformalConferences int,
	 CountVoids int,
	 Retired_Resigned_Terminated int,
	 TotalComplaintDisposed int,
	 ComplaintRemainingOpened int,
	 ConferenceCalendarDates varchar(800)
      )


  insert @DiscretionaryReport 
  values(@CountNotAdjudicated,@ComplaintsReceived,@CountAdvocateMonthly,
   @CountMedicalMonthly,@CountSSLUMonthly,@CountDATMonthly,@CountTrainingMonthly,
   @CountFieldMonthly,
   @CountPleadGuilty ,@CountDismissal ,@CountBCAD ,@CountInformalConferences ,
   @CountVoids ,@Retired_Resigned_Terminated ,@TotalComplaintDisposed ,
   @ComplaintRemainingOpened ,@ConferenceCalendarDates
  )
  select * from @DiscretionaryReport

END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_ReopenOrCloseApprovedComplaint]    Script Date: 05/24/2012 13:46:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_ReopenOrCloseApprovedComplaint]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Amit Khandelwal>
-- Create date: <1/7/2009>
-- Description:	<Close Or Re-Open complaints>
-- =============================================
CREATE PROCEDURE [dbo].[usp_ReopenOrCloseApprovedComplaint]
	-- Add the parameters for the stored procedure here
@ComplaintID bigint,
@StatusCd int,
@ModifiedBy bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update tblDS249Complaints
	SET ComplaintStatus = @StatusCd, ComplaintStatusDate = NULL, RoutingLocation = null,
	ModifiedBy=@ModifiedBy, ModifiedDt = GETDATE()
	Where ComplaintId = @ComplaintID
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 05/24/2012 13:46:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
--[usp_GetComplaintsBySearch_Backup] 10045,0, 50,0,'''',''01/02/0200'','''',64
CREATE PROCEDURE [dbo].[usp_GetComplaintsBySearch]
	-- Add the parameters for the stored procedure here
@CreatedBy int=0,
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@RowCount int OUTPUT 
AS
BEGIN
	SET NOCOUNT ON;
 
 DECLARE @FirstRow INT, @LastRow INT,@count INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
 
 select @count=COUNT(*) from tblMembershipRoles where PersonalID=@CreatedBy and tblMembershipRoles.RoleId=
  (Select tblRoles.Id from tblRoles where tblRoles.Roles=''Commissioner Access'')
  --End
  if @count>=1
     Begin
     --************************
     
      if @numericValue != 0  
   Begin      
   
   SELECT 
	    ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  when a.RoutingLocation =''P'' then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '''' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      a.jt1,0  as ''FinalStatusCD'' 
      into #Complaints5  
 FROM dbo.vw_ComissionerAcess a 
 
 where IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue) 
 or isnull(a.jt1,'' '') like ''%''+Convert(varchar,@numericValue)+ ''%''
 order by ComplaintId desc
   
 SELECT  * FROM  #Complaints5 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
 
 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints5
 
 drop table #Complaints5  
   
   End
 else if @chargesValue != '''' 
   Begin
   
      SELECT 
	    ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  when a.RoutingLocation =''P'' then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      , CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '''' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      a.jt1,0  as ''FinalStatusCD'' 
      into #Complaints6  
 FROM dbo.vw_ComissionerAcess a 
 where Charges like ''%''+@chargesValue+ ''%'' order by ComplaintId desc
 
	SELECT  * FROM  #Complaints6 where ROWID BETWEEN @FirstRow AND @LastRow  ORDER BY ROWID asc  
	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints6
	drop table #Complaints6     
   End	
 else if @complaintDate != ''''
   Begin
   
    SELECT 	  
     ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID,
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  when a.RoutingLocation =''P'' then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '''' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      a.jt1,0  as ''FinalStatusCD'' 
      into #Complaints7  
 FROM dbo.vw_ComissionerAcess a 
 where  cast(CreatedDate as date) = CAST(@complaintDate as date)
 order by ComplaintId desc
 	
 --	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID,* into #adf FROM  #Complaints7 where 
 ----	CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 --cast(CreatedDate as date) = CAST(@complaintDate as date)
 	--SELECT * from #adf where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
 	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints7
 	SELECT  * FROM  #Complaints7  where ROWID BETWEEN @FirstRow AND @LastRow  ORDER BY ROWID asc   
 	drop table #Complaints7 
 	--drop table #adf 
   End	
 else if @stringValue != ''''
   Begin
       SELECT 
       ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,       
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  when a.RoutingLocation =''P'' then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '''' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      a.jt1,0  as ''FinalStatusCD'' 
      into #Complaints8  
 FROM dbo.vw_ComissionerAcess a 
 
 where 
 --(LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) like ''%''+@stringValue+ ''%'')
 RespondentFName like ''%''+@stringValue+ ''%'' 
 OR RespondentLName like ''%''+@stringValue+ ''%''  
OR CreatedBy like ''%''+Convert(varchar,@stringValue)+ ''%'' 
 OR LocationName like ''%''+@stringValue+ ''%'' 
 or isnull(a.jt1,'' '') like ''%''+@stringValue+ ''%''
 order by ComplaintId desc
	
	SELECT  * FROM  #Complaints8  where ROWID BETWEEN @FirstRow AND @LastRow  ORDER BY ROWID asc  
	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints8
	drop table #Complaints8     
   End	
     
     
     --*****************************
     
     
     
     End
 Else
  Begin
 
      
	SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,
      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;	  



create table #Complaints(ComplaintId int
	
      )


     
     insert  into #Complaints
exec dbo.[usp_GetComplaintsSecurity] @CreatedBy;




 if @numericValue != 0  
   Begin      
   
  
      
      
      
      
         SELECT 
		a.[ComplaintId]
		,a.IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as ''RoutingLocation''
      ,a.[RespondentEmpID]
      ,a.[RespondentRefNumber]      
      ,a.[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''      
        ,a.[CreatedDate]
      , '''' as ''Status''
     ,a.Charges as ''Charges''
	,a.Chargesdesc as ''ChargesDesc''
	, a.RespondentFName
	,a.RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
      else 0
      end)
      as ''Indicator'',
      a.IsDeleted,
     a.Jt1,0  as ''FinalStatusCD'' ,
     ROWID =IDENTITY(INT,1,1) 
     into #tempNumeric

 FROM [dbo].[tblDS249Complaints] a inner join #Complaints temp
 on a.ComplaintId=temp.ComplaintId
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 where IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue) 
 or jt1 like   Convert(varchar,@numericValue)
 order by ComplaintId desc
 
  SELECT @RowCount=COUNT([ComplaintId]) FROM #tempNumeric
	select * from #tempNumeric
	  WHERE ROWID BETWEEN @FirstRow AND @LastRow
      ORDER BY  ROWID asc;
      drop table #Complaints
      drop table  #tempNumeric
      
      
      

   
   End
 else if @chargesValue != '''' 
   Begin
   
  

      SELECT 
		a.[ComplaintId]
		,a.IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as ''RoutingLocation''
      ,a.[RespondentEmpID]
      ,a.[RespondentRefNumber]      
      ,a.[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''      
        ,a.[CreatedDate]
      , '''' as ''Status''
     ,a.Charges as ''Charges''
	,a.Chargesdesc as ''ChargesDesc''
	, a.RespondentFName
	,a.RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
      else 0
      end)
      as ''Indicator'',
      a.IsDeleted,
     a.Jt1,0  as ''FinalStatusCD'' ,
     ROWID =IDENTITY(INT,1,1) 
     into #tempCharges

 FROM [dbo].[tblDS249Complaints] a inner join #Complaints temp
 on a.ComplaintId=temp.ComplaintId
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 where Charges like ''%''+@chargesValue+ ''%'' order by ComplaintId desc


   SELECT @RowCount=COUNT([ComplaintId]) FROM #tempCharges
 

 
 
	select * from #tempCharges
	  WHERE ROWID BETWEEN @FirstRow AND @LastRow
      ORDER BY  ROWID asc;
      drop table #Complaints
   drop table  #tempCharges
    
   End	
 else if @complaintDate != ''''
   Begin
   
       SELECT 
		a.[ComplaintId]
		,a.IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as ''RoutingLocation''
      ,a.[RespondentEmpID]
      ,a.[RespondentRefNumber]      
      ,a.[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''      
        ,a.[CreatedDate]
      , '''' as ''Status''
     ,a.Charges as ''Charges''
	,a.Chargesdesc as ''ChargesDesc''
	, a.RespondentFName
	,a.RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
      else 0
      end)
      as ''Indicator'',
      a.IsDeleted,
     a.Jt1,0  as ''FinalStatusCD'' ,
     ROWID =IDENTITY(INT,1,1) 
     into #tempComplaintDate

 FROM [dbo].[tblDS249Complaints] a inner join #Complaints temp
 on a.ComplaintId=temp.ComplaintId
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 where
--CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
cast(CreatedDate as date) = CAST(@complaintDate as date)
 order by ComplaintId desc
 
 

  SELECT @RowCount=COUNT([ComplaintId]) FROM #tempComplaintDate
 
	select * from #tempComplaintDate
	  WHERE ROWID BETWEEN @FirstRow AND @LastRow
      ORDER BY  ROWID asc;
      drop table #Complaints 
   
    drop table  #tempComplaintDate
 
   End	
 else if @stringValue != ''''
   Begin

      
       SELECT 
		a.[ComplaintId]
		,a.IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as ''RoutingLocation''
      ,a.[RespondentEmpID]
      ,a.[RespondentRefNumber]      
      ,a.[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''      
        ,a.[CreatedDate]
      , '''' as ''Status''
     ,a.Charges as ''Charges''
	,a.Chargesdesc as ''ChargesDesc''
	, a.RespondentFName
	,a.RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
      else 0
      end)
      as ''Indicator'',
      a.IsDeleted,
     a.Jt1,0  as ''FinalStatusCD'' ,
     ROWID =IDENTITY(INT,1,1) 
     into #tempStringValue

 FROM [dbo].[tblDS249Complaints] a inner join #Complaints temp
 on a.ComplaintId=temp.ComplaintId
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  

 where 


 RespondentFName like ''%''+@stringValue+ ''%'' 
 OR RespondentLName like ''%''+@stringValue+ ''%''  
OR CreatedBy like ''%''+Convert(varchar,@stringValue)+ ''%'' 
 OR LocationName like ''%''+@stringValue+ ''%'' 
 or  jt1 like   ''%''+@stringValue+ ''%'' 
 or c.FirstName like ''%''+@stringValue+ ''%'' 
 or c.LastName like ''%''+@stringValue+ ''%'' 
 order by ComplaintId desc
 
 
SELECT @RowCount=COUNT([ComplaintId]) FROM #tempStringValue
 
 
	select * from #tempStringValue
	  WHERE ROWID BETWEEN @FirstRow AND @LastRow
      ORDER BY  ROWID asc;
      drop table #Complaints
    drop table  #tempStringValue
      
      
      
     
   End	
 	 	
 
END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsForAdmin]    Script Date: 05/24/2012 13:46:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsForAdmin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetComplaintsForAdmin]

@CreatedBy int = 0,
@PageNo int = 0,
@maximumRows int = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	
SELECT 
			comp.[ComplaintId]		
			,IndexNo
			,ca.FinalStatuscd 
			,comp.[LocationId]
			,comp.BoroughID
			,comp.Jt1 
			,comp.promotiondate
			,b.LocationName
			,(CASE
				 WHEN comp.complaintStatus >=4 
			 THEN  comp.RoutingLocation
				 ELSE null 
			   END) AS ''RoutingLocation''
			,[RespondentEmpID]
			,[RespondentRefNumber]      
			,[Approved]
			,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''   
			,comp.[CreatedDate]     
			,'''' AS ''Status''
			,charges AS ''Charges''
			,Chargesdesc AS ''ChargesDesc''
			,RespondentFName
			,RespondentLName
			,comp.complaintStatus
			,comp.complaintStatusDate 
			,(CASE 
				WHEN comp.complaintStatus =1 
				THEN dbo.GetBusinessDays(comp.CreatedDate,GETDATE())  
				ELSE 0   
			  END) AS ''Indicator''
				,comp.IsDeleted 
		FROM [dbo].[tblDS249Complaints] comp
			LEFT OUTER JOIN tblLocations b ON comp.LocationId = b.LocationId 
			LEFT OUTER JOIN uv_EmployeePerson c ON convert(bigint,comp.CreatedBy) = c.EmployeeID
			INNER JOIN  tblComplaintAdvocacy ca on ca.ComplaintId =comp.ComplaintId 
		WHERE ca.IsAvailable is null 
			  and RoutingLocation = ''A''
		  
UNION	
	
SELECT 
		comp.[ComplaintId]		
		,IndexNo		
		,NULL AS ''FinalStatuscd''
		,comp.[LocationId]
		,comp.BoroughID
		,comp.Jt1 
		,comp.promotiondate
		,b.LocationName
		,(CASE WHEN comp.complaintStatus >=4 THEN  comp.RoutingLocation
		ELSE null 
		END) AS ''RoutingLocation''
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,[Approved]
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy'' 
		,comp.[CreatedDate]
		,'''' AS ''Status''
		,charges AS ''Charges''
		,Chargesdesc AS ''ChargesDesc''
		,RespondentFName
		,RespondentLName
		,comp.complaintStatus
		,comp.complaintStatusDate 
		,(CASE 
			WHEN comp.complaintStatus =1
			THEN dbo.GetBusinessDays(comp.CreatedDate,GETDATE())
			ELSE 0  
		END) AS ''Indicator'',
		comp.IsDeleted 
	FROM [dbo].[tblDS249Complaints] comp 
			LEFT OUTER JOIN tblLocations b ON comp.LocationId = b.LocationId 
			LEFT OUTER JOIN uv_EmployeePerson c ON convert(bigint,comp.CreatedBy) = c.EmployeeID
	WHERE ISNULL(comp.RoutingLocation, '''')<>''A''
		
  Order BY ComplaintId desc
  
END

' 
END
GO



GO
insert [dbo].[tblRoles](Roles)
values('ReOpenComplaint')

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetPersonDetailsWS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetPersonDetailsWS]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetPersonDetailsWS]    Script Date: 05/24/2012 14:03:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetPersonDetailsWS]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetPersonDetailsWS]
	-- Add the parameters for the stored procedure here

@PageNo int = 0,
@PageSize int = 0,
@FirstName varchar(50),
@MiddleName varchar(25),
@LastName varchar(50),
@ReferenceNo varchar(50),
@SSNo varchar(12)
AS
BEGIN
	SET NOCOUNT ON;

WITH EmployeeDetails AS
(

	select p.FirstName,p.MiddleName,p.LastName,e.ReferenceNo,p.SSN,ROW_NUMBER() over (order by p.PersonID) rowID
	from tblPersons p inner join 
		tblEmployees e on p.PersonID=e.PersonID
		where p.FirstName like @FirstName and 
		p.LastName like @LastName and
		p.MiddleName like @MiddleName and 
		e.ReferenceNo like @ReferenceNo and 
		ISNULL(p.SSN,'''') like @SSNo
		and isnull(IsActive, 1) = 1
	
 
  ) 
  select * from EmployeeDetails where rowID>=(@PageNo*@PageSize)+1 and 
  rowID <=(@PageNo * @PageSize) + @PageSize ;
  

  
END


' 
END
GO

update tblEmployees set IsActive = 0 where ReferenceNo = '0056975'
update tblDS249Complaints set 
		RespondentRefNumber = '0056075'
		,RespondentEmpID = 151
where IndexNo = 89401
