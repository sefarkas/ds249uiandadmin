﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace DSNY.DSNYCP.Administrator
{
    /// <summary>
    /// This method provides functionality for the unauthenticated user to redirect to the logout page.
    /// </summary>
    public partial class UnAuthorized : System.Web.UI.Page
    {
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// This method allows the user to redirect to the logput page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Logout.aspx");
        }
    }
}