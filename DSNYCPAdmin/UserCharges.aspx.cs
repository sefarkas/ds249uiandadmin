﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.Administrator
{
    /// <summary>
    /// This method provides the functionality for the respective memners to view there role screen
    /// </summary>
    public partial class UserCharges : System.Web.UI.Page
    {

        bool returnValue;
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Button1.Style.Add("display", "none");
                lbMessage.Text = String.Empty;
                if (!Page.IsPostBack)
                {
                    BindGrid();
                }
           
        }
        
        /// <summary>
        /// This button click event fires when the user clicks on the logout button
        /// This redirects the user to the logout page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Logout.aspx");
        }

                /// <summary>
        /// Find The Charges.
        /// </summary>
        /// <param name="prefixText"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string[] GetCharges(string prefixText, int count)
        {            
                ArrayList arrCharge = new ArrayList();
                ChargesList cList = new ChargesList();
                cList.Load(prefixText);

                if (cList.ChargeList.Count > 0)
                {

                    foreach (Charge charge in cList.ChargeList)
                    {
                        arrCharge.Add(charge.ChargeCode + " " + charge.ChargeDescription);

                    }
                }
                return (string[])arrCharge.ToArray(typeof(string));           

        }
        /// <summary>
        /// Extract Reference No
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private String ExtractRefNo(String text)
        {           
                if (text != String.Empty)
                {
                    return text.Substring(0, text.IndexOf(" ")).Trim();
                }
                else
                {
                    return string.Empty;
                }           

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Function to Bind Grid
        /// </summary>
        private void BindGrid()
        {
            HideChargesList hideCharges = null;
            List<HideChargesList> listhideCharges = null;          
                hideCharges = new HideChargesList();
                listhideCharges = new List<HideChargesList>();
                listhideCharges = hideCharges.Load(Convert.ToInt16(Request.QueryString["id"]));
                gvCharges.DataSource = listhideCharges;
                gvCharges.DataBind();           
        }
        /// <summary>
        /// Function to delete Charges for given user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvCharges_RowCommand(object sender, GridViewCommandEventArgs e)
        {           
                HideChargesList hideCharges = new HideChargesList();
                if (e.CommandName == "Delete")
                {
                    hideCharges.DeleteHideCharges(Convert.ToInt16(e.CommandArgument.ToString()));
                    BindGrid();
                    lbMessage.Text = "Charge Deleted Successfully";
                    lbMessage.ForeColor = System.Drawing.Color.Green;

                }            
        }
        /// <summary>
        /// Row Command for Delete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvCharges_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            BindGrid();
        }
        /// <summary>
        /// Move to the Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibCancel_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }
        /// <summary>
        /// Save Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibSave_Click(object sender, ImageClickEventArgs e)
        {    

                string referenceNo = string.Empty;
                HideChargesList hideCharges = new HideChargesList();
                List<HideChargesList> listhideCharges = new List<HideChargesList>();
                referenceNo = ExtractRefNo(txtCharge.Text);
                if (txtCharge.Text == string.Empty)
                {
                    lbMessage.Text = "Please Select Charges";
                    lbMessage.ForeColor = System.Drawing.Color.Red;

                }
                else
                {
                    if (referenceNo != string.Empty)
                    {
                        returnValue = hideCharges.SaveHideCharges(referenceNo, Convert.ToInt16(Request.QueryString["id"]));
                        if (returnValue)
                        {
                            lbMessage.ForeColor = System.Drawing.Color.Green;
                            BindGrid();
                            txtCharge.Text = string.Empty;
                            lbMessage.Text = "Charges Saved Successfully";
                            lbMessage.ForeColor = System.Drawing.Color.Green;
                        }
                        else
                        {
                            lbMessage.Text = "Could not Save Charges";
                            lbMessage.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                }           
        }

    }
}
