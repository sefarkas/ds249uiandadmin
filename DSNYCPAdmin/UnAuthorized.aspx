﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" Inherits="DSNY.DSNYCP.Administrator.UnAuthorized" Title="Untitled Page" Codebehind="UnAuthorized.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="815" border="0" cellspacing="0" cellpadding="2" class="main">
  <tr>
    <td colspan="3" class="headertext">NO AUTHORIZATION<img src="includes/img/clear_pix.gif" height="1" width="315" /><font class="headerHelptext">IT HELPDESK: 212.437.4200</font><br />
        <img src="~/includes/img/border_top.gif" /></td>
  </tr>
  <tr>
    <td colspan="3" class="headertext">
                
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                
                <asp:ImageButton ID="btnLogout" runat="server" 
                    ImageUrl="~/includes/img/btn_logout.gif" onclick="btnLogout_Click" 
                    CausesValidation="False" />
                
            </td>
  </tr>
  <tr>
    <td colspan="3" align=center>
        <table class="loginBox" cellpadding="14">
            <tr>
                <td>
                    Sorry. You are not authorized to view this page.
                </td><br /><br />
            </tr>
            <tr class="loginBoxInternal" align=center>
                <td>                    
                Click here to go <a href="#" onclick="javascript:window.navigate('main.aspx');">back</a>
                </td>
            </tr>
        </table>
    </td>
  </tr>
  </table>
</asp:Content>

