﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;


namespace DSNY.DSNYCP.Administrator
{

        /// <summary>
        /// This class provide functionality to redirect the user based on their membership roles to their websites
        /// </summary>
        public partial class Main : System.Web.UI.Page
        {           

            /// <summary>
            /// This event is fired when the page is loaded
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void Page_Load(object sender, EventArgs e)
            {
                User usr;


            usr = (User)Session["clsUser"];
            if (usr != null)
            {
                if (!usr.IsInMemberShip(GroupName.ADMIN))
                    Response.Redirect("UnAuthorized.aspx");
                else
                {
                    usr.CurrentMembership = GroupName.ADMIN;
                    SecurityUtility.AttachRolesToUser(usr);
                }

                if (!User.IsInRole("Admin"))
                    Response.Redirect("UnAuthorized.aspx");
            }
            else
                Response.Redirect("Login.aspx");


            if (usr.IsTempPassword == true)
                HttpContext.Current.Response.Redirect("ResetPassword.aspx");
            else
                HttpContext.Current.Response.Redirect("Dashboard.aspx");
        }
    }

}


