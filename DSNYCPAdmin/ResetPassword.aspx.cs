﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.Administrator
{
    /// <summary>
    /// This class provide functionality to reset the password for authenticated users
    /// </summary>
    public partial class UI_ResetPassword : System.Web.UI.Page
    {
        User usr;
        /// <summary>
        /// This event fires when the page is initially loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            usr = (User)Session["clsUser"];
            if (usr != null)
            {
                if (!usr.IsInMemberShip(GroupName.ADMIN))
                    Response.Redirect("UnAuthorized.aspx");
                else
                {
                    usr.CurrentMembership = GroupName.ADMIN;
                    SecurityUtility.AttachRolesToUser(usr);
                }

                if (!User.IsInRole("Admin"))
                    Response.Redirect("UnAuthorized.aspx");
            }
            else
                Response.Redirect("Login.aspx");

            SignInIDTextBox.Text = Convert.ToString(Session["User"]);
        }
        /// <summary>
        /// This method provide functionality to reset the password for authenticated users
        /// finally redirect the user to dashboard page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResetPassword_Click(object sender, ImageClickEventArgs e)
        {
            usr = (User)Session["clsUser"];          
                if (Session["UserEmpID"].ToString() != "")
                {
                    usr.PersonalId = Convert.ToInt64(Session["UserEmpID"]);
                }
                usr.UserId = SignInIDTextBox.Text.Trim();
                usr.Password = PasswordTextBox.Text.Trim();
                usr.IsTempPassword = false;
                usr.ResetPassword();
                Response.Redirect("Dashboard.aspx");            
        }
    }
}