<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    Inherits="DSNY.DSNYCP.Administrator.Dashboard" Title="DS-249 User Management"
    CodeBehind="Dashboard.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="includes/css/styles1.css" rel="stylesheet" type="text/css" />
    <link href="includes/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="includes/css/CaseManagementStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style2
        {
            width: 602px;
        }
        .style4
        {
            width: 883px;
        }
        .style5
        {
            width: 466px;
        }
        .style6
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 18px;
            color: #235709;
            font-weight: bold;
            border-bottom: 1px dotted #235709;
            width: 61%;
        }
        .style7
        {
            font-size: medium;
            font-weight: bold;
        }
        .style9
        {
            font-family: Arial;
            font-size: 1.3em;
            margin: 0px 20px 0px 3px;
            _margin-left: 5px;
            font-weight: bold;
            width: 403px;
        }
        .style10
        {
            width: 403px;
        }
        .style11
        {
            width: 232px;
        }
        .style12
        {
            font-family: Arial;
            font-size: 1.3em;
            margin: 0px 20px 0px 3px;
            _margin-left: 5px;
            font-weight: bold;
            width: 231px;
        }
        .style13
        {
            width: 231px;
        }
        .style14
        {
            font-family: Arial;
            font-size: 1.3em;
            margin: 0px 20px 0px 3px;
            _margin-left: 5px;
            font-weight: bold;
            width: 439px;
        }
        .style15
        {
            width: 439px;
        }
        .style16
        {
            font-family: Arial;
            font-size: 1.3em;
            margin: 0px 20px 0px 3px;
            _margin-left: 5px;
            font-weight: bold;
            width: 255px;
        }
        .style17
        {
            width: 255px;
        }
        .style18
        {
            border: 1px solid #1e951f;
            width: 18%;
        }
        .style19
        {
            width: 18%;
        }
        .style20
        {
            width: 170px;
        }
        .style21
        {
            font-family: Arial;
            font-size: 1.3em;
            margin: 0px 20px 0px 3px;
            _margin-left: 5px;
            font-weight: bold;
            width: 419px;
        }
        .style22
        {
            width: 419px;
        }
    </style>

        <script language="javascript" type="text/javascript">


        $.when($.ready).then(function () 
        {
            hideLoader();
        });


        $("form").live("click", function ()
        {
            // Strongly recommended: Hide loader after 20 seconds, even if the page hasn't finished loading
            setTimeout(hideLoader, 1 * 1000);
            ShowProgress();
        });
    </script>    


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="align-content: center">
        <table style="width: 900px; height: 50px" class="tableMargin" border="0" cellpadding="0"
            cellspacing="0" >
            <tr>
                <td>
                    <table>
                        <tr>
                            <td valign="top">
                                <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                                <span class="infoStyleGreen1">Please contact IT Service Desk at 212.291.1111 or email us
                                    at <a href="mailto:itServiceDesk@dsny.nyc.gov" style="color: #1e951f;" title="Send us an email">
                                        itServiceDesk@dsny.nyc.gov</a></span>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="styleHeading">
                    <div>
                        <span>DS-249 USER MANAGEMENT </span>
                    </div>
                </td>
                <td class="styleHeading">
                    <asp:ImageButton ID="btnLogout" ImageUrl="~/includes/img/btn_logout.gif" runat="server"
                        OnClick="BtnLogout_Click" CausesValidation="False" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="ComplaintManagementButton" runat="server" Text="Complaint Management"
                        OnClick="ComplaintManagementButton_Click" CausesValidation="false" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table style="width: 900px; height: 50px" class="tableMargin" border="0" cellpadding="0px"
                    cellspacing="0px" align="center">
                    <tr>
                        <td class="labelBoldStyle">
                            EMPLOYEE SEARCH
                        </td>
                        <td style="width: 80%;">
                            <asp:TextBox ID="EmpSearchTextBox" runat="server" Width="240px" CssClass="inputFieldStyle"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="EmpSearchTextBox_TextBoxWatermarkExtender" runat="server"
                                Enabled="True" TargetControlID="EmpSearchTextBox" WatermarkCssClass="WaterMark"
                                WatermarkText="Enter First Few Characters of the Name">
                            </cc1:TextBoxWatermarkExtender>
                            <asp:Label ID="lbSearchMsg1" runat="server" Text="" Font-Bold="True" Font-Size="Small"></asp:Label>
                            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Filter" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lbMessage" runat="server" Width="50%" Font-Size="Small" Font-Bold="true"></asp:Label>
                <table style="width: 900px" class="tableMargin" border="0" cellpadding="0px" cellspacing="0px"
                    align="center">
                    <tr>
                        <td>
                            <asp:GridView ID="EmployeeGridView" runat="server" Width="950px" AutoGenerateColumns="False"
                                BorderColor="#235705" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a"
                                AllowSorting="True" PageSize="10" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Left"
                                AllowPaging="True" OnSorting="EmployeeGridView_Sorting" OnPageIndexChanging="EmployeeGridView_PageIndexChanging"
                                OnDataBound="EmployeeGridView_DataBound" OnRowDataBound="EmployeeGridView_RowDataBound"
                                OnRowCommand="EmployeeGridView_RowCommand" EmptyDataText="Sorry. No Employee Found.">
                                <EmptyDataRowStyle CssClass="fromdate" />
                                <PagerSettings Position="TopAndBottom"></PagerSettings>
                                <PagerTemplate>
                                    <table style="width: 99%;" border="0" cellpadding="0px" cellspacing="0px">
                                        <tr>
                                            <td style="text-align: left; font-family: Arial,Verdana,sans-serif; font-size: 1.2em;
                                                margin-left: 2px; width: 6%;">
                                                Page
                                            </td>
                                            <td style="width: 6%;" nowrap="nowrap">
                                                <asp:ImageButton ID="imgDoublePrevious" ImageUrl="~/includes/img/PreviousDoubleArrow.GIF"
                                                    CommandName="Page" CausesValidation="false" CommandArgument='<%#EmployeeGridView.PageIndex - 4%>'
                                                    runat="server" />
                                                <asp:ImageButton ID="imgPrevious" ImageUrl="~/includes/img/PreviousSingleArrow.GIF"
                                                    CommandName="Page" CausesValidation="false" CommandArgument="Prev" ToolTip="Previous Page"
                                                    runat="server" />
                                            </td>
                                            <td align="center" style="width: 8%;">
                                                <asp:Menu ID="menuPager" Orientation="Horizontal" OnMenuItemClick="MenuPager_MenuItemClick"
                                                    Font-Size="1.2em" ForeColor="#1e951f" StaticSelectedStyle-ForeColor="Black" StaticSelectedStyle-BorderStyle="Solid"
                                                    StaticSelectedStyle-BorderWidth="1px" StaticSelectedStyle-BorderColor="#A4D49A"
                                                    runat="server" />
                                            </td>
                                            <td style="text-align: left; width: 6%">
                                                <asp:ImageButton ID="imgNext" runat="server" CausesValidation="false" CommandName="Page"
                                                    CommandArgument="Next" ToolTip="Next Page" ImageUrl="~/includes/img/NextSingleArrow.GIF" />
                                                <asp:ImageButton ID="imgDoubleNext" runat="server" CausesValidation="false" CommandName="Page"
                                                    CommandArgument='<%#EmployeeGridView.PageIndex + 6%>' ImageUrl="~/includes/img/NextDoubleArrow.GIF" />
                                            </td>
                                            <td style="font-family: Arial,Verdana,sans-serif; font-size: 1.2em; width: 10%;">
                                                of
                                                <%=EmployeeGridView.PageCount%>
                                            </td>
                                            <td style="width: 75%; text-align: right; margin-left: 0px; padding-left: 0px;">
                                                <asp:Label ID="PageLabel" runat="server" Text="Jump to Page:" ForeColor="Black" Font-Names="Arial,Verdana,sans-serif"
                                                    Font-Size="1.2em"></asp:Label>
                                                <asp:TextBox ID="JmpToPageTextBox" runat="server" Width="20px" AutoPostBack="true"
                                                    OnTextChanged="JmpToPageTextBox_TextChanged"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </PagerTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="Action">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="25px" />
                                        <ItemTemplate>
                                            <asp:Button ID="AddUserButton" runat="server" Text="Add" CommandName="AddUser" CommandArgument='<%#Eval("PersonID") %>'
                                                CausesValidation="false" Visible="false" PostBackUrl="#UserTable" />
                                            <cc1:ConfirmButtonExtender ID="ConfirmButtonExtender2" runat="server" ConfirmText="Are you sure you want to Add?"
                                                TargetControlID="AddUserButton">
                                            </cc1:ConfirmButtonExtender>
                                            <asp:Button ID="DeleteUserButton" runat="server" Text="Delete" CommandName="DeleteUser"
                                                CommandArgument='<%#Eval("PersonID") %>' CausesValidation="false" />
                                            <cc1:ConfirmButtonExtender ID="DeleteConfirmButtonExtender" runat="server" ConfirmText="Are you sure you want to delete?"
                                                TargetControlID="DeleteUserButton">
                                            </cc1:ConfirmButtonExtender>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="PersonID" Visible="false" />
                                    <asp:BoundField DataField="ReferenceNumber" HeaderText="Ref #" SortExpression="ReferenceNumber">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="User Name" SortExpression="UserName">
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="70px" Wrap="False" />
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemTemplate>
                                            <asp:LinkButton ID="UserNameLinkButton" runat="server" Text='<%#Eval("UserName") %>'
                                                CommandName="Reset" CommandArgument='<%#Eval("PersonID") %>' ToolTip="Click to Reset Password."
                                                CausesValidation="false"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FirstName" HeaderText="First Name" SortExpression="FirstName">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MiddleName" HeaderText="MI" ItemStyle-Width="25">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" Width="35px" />
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="35px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name" SortExpression="LastName">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="80" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EmailID" HeaderText="Email">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="70px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PersonnelTitle" HeaderText="Title" SortExpression="PersonnelTitle">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PayCodeDescription" HeaderText="Pay Code">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BackColor="#A4D49A" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="200px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IsDeleted" />
                                    <asp:BoundField DataField="IsAdmin" />
                                    <asp:TemplateField HeaderText="Roles">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle"
                                            Width="25px" />
                                        <ItemTemplate>
                                            <asp:Button ID="btnAddRoles" runat="server" Text="Add Roles" CommandName="AddRoles"
                                                CommandArgument='<%#Eval("PersonID") %>' CausesValidation="false" PostBackUrl="#UserTable" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Charges">
                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                            Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                        <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center"
                                            VerticalAlign="Middle" Width="25px" />
                                        <ItemTemplate>
                                            <asp:Button ID="btnAddCharges" runat="server" Text="Charges" CommandName="AddCharges"
                                                CommandArgument='<%#Eval("PersonID") %>' CausesValidation="false" PostBackUrl="#UserTable" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="UserTable" runat="server" visible="false" style="width: 800px" class="tableMargin"
                    border="0" cellpadding="0" cellspacing="0" align="Left">
                    <tr>
                        <td>
                            User Name
                        </td>
                        <td>
                            <asp:TextBox ID="PersonIDTextBox" runat="server" Visible="false"></asp:TextBox>
                            <asp:TextBox ID="UserNameTextBox" runat="server" Width="220"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="UserNameRFV" runat="server" ErrorMessage="Please enter User Name"
                                Display="Dynamic" ControlToValidate="UserNameTextBox"></asp:RequiredFieldValidator>
                            <asp:Label ID="MessageLabel" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Email ID
                        </td>
                        <td>
                            <asp:TextBox ID="EMailIDTextBox" runat="server" Width="220"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="regEmail" ControlToValidate="EMailIDTextBox"
                                Text="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password
                        </td>
                        <td>
                            <asp:TextBox ID="PasswordTextBox" runat="server" TextMode="Password" Width="220"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRFV" runat="server" ErrorMessage="Please enter Password"
                                Display="Dynamic" ControlToValidate="PasswordTextBox"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">Confirm Password
                        </td>
                        <td>
                            <asp:TextBox ID="ConfirmPasswordTextBox" runat="server" TextMode="Password" Width="220"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="ConfirmPasswordRequiredFieldValidator" runat="server"
                                ErrorMessage="Please Enter Confirm Password" Display="Dynamic" ControlToValidate="ConfirmPasswordTextBox"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="ConfirmPasswordCompareValidator" runat="server" ErrorMessage="Confirm Password does not match with the Password."
                                ControlToValidate="ConfirmPasswordTextBox" ControlToCompare="PasswordTextBox"
                                 ></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td width="15%">
                            &nbsp;
                        </td>
                        <td>
                            <asp:CheckBox ID="AdminUserCheckBox" runat="server" Visible="false" />
                            <asp:HiddenField ID="hdnStatus" runat="server" Visible="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                        </td>
                        <td align="left">
                            <asp:Button ID="SaveButton" runat="server" Text="Save" OnClick="SaveButton_Click" />
                            <asp:Button ID="CancelButton" runat="server" Text="Cancel" CausesValidation="false" />
                        </td>
                    </tr>
                </table>
                <a name="UserTable"></a>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
