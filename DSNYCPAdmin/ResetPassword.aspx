﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    Inherits="DSNY.DSNYCP.Administrator.UI_ResetPassword" CodeBehind="ResetPassword.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="900px" border="0" cellspacing="0" cellpadding="2" class="main" align="center">
        <tr>
            <td colspan="3" class="headertext">
                DSNY: DS-249 COMPLAINTS<br />
                <img src="includes/img/border_top.gif" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table class="loginBox" cellpadding="14" align="center">
                    <tr>
                        <td>
                            PASSWORD RESET
                        </td>
                        <br />
                        <br />
                    </tr>
                    <tr class="loginBoxInternal" align="left">
                        <td>
                            <asp:Label ID="lMessage" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
                            <br />
                            Sign in ID*<br />
                            <asp:TextBox ID="SignInIDTextBox" runat="server" Width="275" Enabled="false"></asp:TextBox><br />
                            <asp:RequiredFieldValidator ID="UserNameRFV" runat="server" ErrorMessage="Please enter Sign In ID"
                                Display="Dynamic" ControlToValidate="SignInIDTextBox"></asp:RequiredFieldValidator>
                            <br />
                            Password*<br />
                            <asp:TextBox ID="PasswordTextBox" runat="server" Width="275" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="PasswordRFV" runat="server" ErrorMessage="Please enter Password"
                                Display="Dynamic" ControlToValidate="PasswordTextBox"></asp:RequiredFieldValidator>
                            <br />
                            <br />
                            Confirm Password*<br />
                            <asp:TextBox ID="ConfirmPasswordTextBox" runat="server" Width="275" TextMode="Password"></asp:TextBox>
                            <asp:CompareValidator ID="ConfirmPasswordCompareValidator" runat="server" ErrorMessage="Confirm Password does not match with the Password."
                                ControlToValidate="ConfirmPasswordTextBox" ControlToCompare="PasswordTextBox"
                                ValidateEmptyText="true"></asp:CompareValidator>
                            <br />
                            <br />
                            <asp:ImageButton ID="ResetPassword" runat="server" ImageUrl="~/includes/img/btn_reset.gif"
                                OnClick="ResetPassword_Click" />
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
                <table class="loginBoxLower" align="center">
                    <tr>
                        <td>
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                    itServiceDesk@dsny.nyc.gov</a></span>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
