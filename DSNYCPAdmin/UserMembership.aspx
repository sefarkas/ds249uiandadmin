﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeBehind="UserMembership.aspx.cs" Inherits="DSNY.DSNYCP.Administrator.UserMembership" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="includes/CSS/DSNYcss.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        function HideModalPopup() {
            var modal = $find('<%=ModalPopupExtender1.ClientID %>');
            modal.hide();
        }

        function PerformPostBack(Sender, e) {            
            document.getElementById('ctl00_ContentPlaceHolder1_Button4').click();
        }
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table style="width: 900px; height: 50px" class="tableMargin" border="0px" cellpadding="0px"
            cellspacing="0px" align="center">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td valign="top">
                                <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                                <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                    at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                        itServiceDesk@dsny.nyc.gov</a></span>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="styleHeading">
                    <div>
                        <span>DS-249 USER MEMBERSHIP </span>
                    </div>
                </td>
                <td class="styleHeading">
                    <asp:ImageButton ID="btnLogout" ImageUrl="~/includes/img/btn_logout.gif" runat="server"
                        OnClick="btnLogout_Click" CausesValidation="False" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label ID="lbMessage" runat="server" Width="50%" Font-Size="Small" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                        runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                        <Animations>                                
                                                <OnUpdated>
                                                    <FadeOut Duration="5.0" Fps="24" />
                                                </OnUpdated>
                        </Animations>
                    </cc1:UpdatePanelAnimationExtender>
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="upMembership" runat="server">
            <ContentTemplate>
                <center>
                    <table>
                        <tr>
                            <td class="fromdate">
                                <br />
                                Assign Membership & Roles for User:
                                <asp:Label ID="lbUserName" runat="server" Text=""></asp:Label>
                                <br />
                                <asp:Button ID="Button4" runat="server" Text="Button" OnClick="Button4_Click" />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvMembership" runat="server" AutoGenerateColumns="False" BorderColor="#235705"
                                    HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a" EmptyDataText="Sorry. No Membership Data to Display."
                                    OnRowDataBound="gvMembership_RowDataBound" DataKeyNames="MembershipDescription"
                                    OnRowCommand="gvMembership_RowCommand">
                                    <EmptyDataRowStyle CssClass="fromdate" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                                Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                                ForeColor="Black" Font-Underline="False" Wrap="False" />
                                            <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center"
                                                VerticalAlign="Middle" Width="25px" />
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkMembership" runat="server" AutoPostBack="true" CausesValidation="false"
                                                    ForeColor="#A4D49A" PostBackUrl="#UserTable" OnCheckedChanged="chkMembership_CheckedChanged"
                                                    Text='<%#Eval("MembershipID") %>' />
                                                <asp:HiddenField ID="hdnMembership" runat="server" Value='<%#Eval("MembershipID") %>'>
                                                </asp:HiddenField>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="MembershipID" />
                                        <asp:BoundField DataField="MembershipDescription" HeaderText="Membership/Groups">
                                            <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                                Font-Bold="true" HorizontalAlign="Left" VerticalAlign="Middle" BorderWidth="2px"
                                                ForeColor="Black" Font-Underline="False" Wrap="False" />
                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Roles">
                                            <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                                Font-Bold="true" HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px"
                                                ForeColor="Black" Font-Underline="False" Wrap="False" />
                                            <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center"
                                                VerticalAlign="Middle" Width="25px" />
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbRoles" CommandName="roles" CommandArgument='<%#Eval("MembershipID") %>'
                                                    runat="server" Enabled="false" Style="cursor: pointer; text-decoration: underline;"
                                                    Font-Bold="true" ToolTip="Click to assign roles to this group">Roles</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:ImageButton ID="ibSave" runat="server" ImageUrl="~/includes/img/btn_save.gif"
                                    OnClick="ibSave_Click" />
                                <asp:ImageButton ID="ibCancel" runat="server" ImageUrl="~/includes/img/btn_cancel.gif"
                                    OnClick="ibCancel_Click" CausesValidation="false" />
                            </td>
                        </tr>
                    </table>
                </center>
                <cc1:ModalPopupExtender ID="ModalPopupExtender1" runat="server" PopupControlID="Panel1"
                    TargetControlID="btnUpdate" CancelControlID="Button3" OnCancelScript="return HideModalPopup();"
                    BackgroundCssClass="modalBackground">
                </cc1:ModalPopupExtender>
                <asp:Panel ID="Panel1" runat="server" BackColor="LightGray" Width="428px" Style="display: none">
                    <asp:HiddenField ID="hdnMembershipID" runat="server" />
                    <br />
                    Assign Roles for Membership/Group:
                    <asp:Label ID="lbMembershipName" runat="server" Text=""></asp:Label>
                    <br />
                    <hr />
                    <asp:CheckBoxList ID="cblRoleList" runat="server" CellSpacing="0" CellPadding="0"
                        BorderWidth="0">
                    </asp:CheckBoxList>                  
                    <asp:Button ID="btnUpdate" runat="server" Text="Done" CausesValidation="false" />
                    <asp:Button ID="Button3" runat="server" Text="Cancel" CausesValidation="false" />
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
