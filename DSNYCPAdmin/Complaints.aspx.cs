﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using AjaxControlToolkit;
using System.Configuration;
using DSNY.DSNYCP.DTO;
using System.Globalization;


namespace DSNY.DSNYCP.Administrator
{
    /// <summary>
    /// This class provides the functionality for events and methods for the Admin to view the complaints.
    /// </summary>
    public partial class Complaints : System.Web.UI.Page
    {

        #region Page Level Variables

        ComplaintList complaintList;
        PersonnelList personnelList;
        User usr;

        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
     
        #endregion

        /// <summary>
        ///  This method sorts the databound items in a gridview
        /// </summary>
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {           
                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                Response.ExpiresAbsolute = DateTime.Now;

                usr = (User)Session["clsUser"];
                if (usr != null)
                {
                    if (!usr.IsInMemberShip(GroupName.ADMIN))
                        Response.Redirect("UnAuthorized.aspx");
                    else
                    {
                        usr.CurrentMembership = GroupName.ADMIN;
                        SecurityUtility.AttachRolesToUser(usr);
                    }

                    if (!User.IsInRole("Case Management"))
                        Response.Redirect("UnAuthorized.aspx");
                }
                else
                    Response.Redirect("Login.aspx");



                if (Session["IsAuthenticated"] == null)
                {
                    Response.Redirect("login.aspx?Session=False");
                }
                if (Session["IsAuthenticated"].ToString() == "false")
                {
                    Response.Redirect("login.aspx?Session=False");
                }
                Page.Form.DefaultButton = btnSearch.UniqueID;
                lbMessage.Text = String.Empty;
                lblCmbCmpMsg.Text = String.Empty;
                if (!IsPostBack)
                {
                    PopulateGrid(gvComplaints.PageIndex);
                }            
        }


        #region Private Methods
        /// <summary>
        /// This method returns the list of charges based on the search criteria
        /// </summary>
        /// <param name="contextKey"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string GetChargesList(string contextKey)
        {
            StringBuilder sTemp = new StringBuilder();

            VoilationList voilationList = new VoilationList();
          
                voilationList.Load(Convert.ToInt64(contextKey));

                if (voilationList.List.Count > 0)
                {
                    String code;
                    String desc;
                    int spaceIndex;

                    sTemp.Append("<table style='width:300px; border: #060F40 2px solid;color: #060F40;background: #ffffcc;'>");
                    sTemp.Append(String.Format("<tr><td colspan={0}><b>Charges:</b></td><tr>", voilationList.List.Count));
                    sTemp.Append("<tr><td><table>");
                    foreach (Voilation voilation in voilationList.List)
                    {
                        if (voilation.ViolationDescription != String.Empty)
                        {
                            spaceIndex = voilation.ViolationDescription.IndexOf(" ");
                            code = voilation.ViolationDescription.Substring(0, spaceIndex);
                            desc = voilation.ViolationDescription.Substring(spaceIndex).Trim();
                            sTemp.Append("<tr><td valign='top'>" + code + "</td><td>" + desc + "</td></tr>");
                        }
                    }
                    sTemp.Append("</table></td></tr></table>");
                }
                else
                    sTemp.Append("<i>Charges for this Complaint Not Found...</i>");
                return sTemp.ToString();
          
        }

        /// <summary>
        /// This method populates the grid view
        /// </summary>
        private void PopulateGrid(int pageNo)
        {
            int maximumRows = Convert.ToInt32(ConfigurationManager.AppSettings["MaximumRows"]);
                gvComplaints.DataSource = MergeData(pageNo, maximumRows);
                gvComplaints.DataBind();           
        }
        /// <summary>
        /// This method loads the DS249 complaints
        /// </summary>
        private void LoadComplaints(int pageNo, int maximumRows)
        {           
                Int64 rowsCount = 0;
                complaintList = new ComplaintList();
                pageNo = -3;
                maximumRows = -3;
            complaintList.Load(RequestSource.DS249, pageNo, maximumRows, ref rowsCount);
           
        }
        /// <summary>
        /// This method loads the Personnels info to the list
        /// </summary>
        private void LoadPersonnels()
        {           
                personnelList = new PersonnelList();
                personnelList.Load(true);           
        }
        /// <summary>
        /// This method bind the data from the dataset to the data table
        /// </summary>
        /// <returns></returns>
        private DataTable MergeData(int pageNo, int maximumRows)
        {
          
                LoadComplaints(pageNo, maximumRows);
                DataTable dt = new DataTable();
                dt.Columns.Add("CreatedDate", Type.GetType("System.DateTime"));
                dt.Columns.Add("ComplaintID", Type.GetType("System.Int64"));
                dt.Columns.Add("FinalStatusCD", Type.GetType("System.Int64"));
                dt.Columns.Add("IndexNo", Type.GetType("System.Int64"));
                dt.Columns.Add("RoutingLocation", Type.GetType("System.String"));
                dt.Columns.Add("ComplaintStatus", Type.GetType("System.Int16"));
                dt.Columns.Add("LocationName", Type.GetType("System.String"));
                dt.Columns.Add("FirstName", Type.GetType("System.String"));
                dt.Columns.Add("LastName", Type.GetType("System.String"));
                dt.Columns.Add("InitialAndLastName", Type.GetType("System.String"));
                dt.Columns.Add("RefNo", Type.GetType("System.String"));
                dt.Columns.Add("CreatedBy", Type.GetType("System.String"));
                dt.Columns.Add("Charge", Type.GetType("System.String"));
                dt.Columns.Add("ChargeDesc", Type.GetType("System.String"));
                dt.Columns.Add("IsDeleted", Type.GetType("System.Boolean"));
                DataRow dr;                
                foreach (Complaint complaint in complaintList.List)
                {
                    dr = dt.NewRow();

                    dr["CreatedDate"] = complaint.CreatedDate.ToShortDateString();
                    dr["ComplaintID"] = complaint.ComplaintId.ToString();
                    dr["FinalStatusCD"] = complaint.FinalStatusCD;
                    dr["IndexNo"] = complaint.IndexNo.ToString();
                    dr["RoutingLocation"] = complaint.RoutingLocationDescription;
                    dr["ComplaintStatus"] = complaint.ComplaintStatus;
                    dr["LocationName"] = complaint.LocationName;
                    dr["RefNo"] = complaint.ReferenceNumber;
                    dr["CreatedBy"] = complaint.CreatedBy;
                    dr["Charge"] = complaint.Charge;
                    dr["ChargeDesc"] = complaint.ChargeDescription;
                    dr["InitialAndLastName"] = complaint.InitialAndLastName.Trim();
                    dr["FirstName"] = complaint.RespondentFirstName.Trim();
                    dr["LastName"] = complaint.RespondentLastName.Trim();
                    dr["IsDeleted"] = complaint.IsDeleted;
                    dt.Rows.Add(dr);
                }                
                return dt;           
        }
        /// <summary>
        ///  Populates the data to the data table
        /// </summary>
        /// <returns></returns>
        private DataTable PopulateTable(int pageNo)
        {           
                DataTable dt;
                int maximumRows = 0;
                dt = MergeData(pageNo, maximumRows);
                return dt;           
        }
        /// <summary>
        /// Returns true if the search is sucessfull
        /// </summary>
        /// <param name="Search"></param>
        private void SearchComplaints(bool Search)
        {           
                DataView dv;
                dv = new DataView(PopulateTable(gvComplaints.PageIndex));
                
                if (Search == true)
                {
                    bool isDecimal;
                    decimal bal;
                    isDecimal = decimal.TryParse(txtSearch.Text.Trim(), out bal);

                    if (IsNumeric(txtSearch.Text.Trim()))
                    {
                        dv.RowFilter = String.Format("IndexNo = {0} OR RefNo Like '%{0}%'", txtSearch.Text.Trim());
                    }
                    else if (isDecimal)
                    {
                        dv.RowFilter = String.Format("Charge Like '%{0}%'", txtSearch.Text.Trim());
                    }
                    else if (IsDate(txtSearch.Text.Trim()))
                    {
                        dv.RowFilter = String.Format("CreatedDate ='{0}'", txtSearch.Text.Trim());
                    }
                    else
                        dv.RowFilter = GetSearchString();
                }
                else
                    dv.RowFilter = String.Empty;
                
                gvComplaints.DataSource = dv;
                gvComplaints.DataBind();
           
        }
        /// <summary>
        /// This method returns true if the search text entered is numeric
        /// </summary>
        /// <param name="strTextEntry"></param>
        /// <returns></returns>
        private bool IsNumeric(string strTextEntry)
        {           
                Regex objNotWholePattern = new Regex("[^0-9]");
                return !objNotWholePattern.IsMatch(strTextEntry);            
        }
        /// <summary>
        /// This method returns true if the search text entered is valid date
        /// </summary>
        /// <param name="strTextEntry"></param>
        /// <returns></returns>
        public bool IsDate(string strDate)
        {
            return ClassHierarchy.Utilities.CommonFormats.IsDate(strDate);            
        }

        /// <summary>
        /// This method returns true if the search text entered is a valid date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        private String GetSearchString()
        {           
                StringBuilder searchString = new StringBuilder();
                searchString.Append(String.Format("FirstName Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("LastName Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("RefNo Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("LocationName Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("RoutingLocation Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("CreatedBy Like '%{0}%'", txtSearch.Text.Trim()));
                return searchString.ToString();           
        }


        #endregion
        /// <summary>
        /// This button click events redirects the page to logout page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("logout.aspx");
        }
        /// <summary>
        /// Returns string by concatinating the data in the search text box
        /// </summary>
        /// <returns></returns>
        
        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {           
                if (txtSearch.Text.Length > 0)
                {
                    SearchComplaints(true);
                }
                else
                {
                    SearchComplaints(false);
                }           
        }
        /// <summary>
        /// This is a text change event that fires when the user enters the search criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchCancel_Click(object sender, ImageClickEventArgs e)
        {           
                txtSearch.Text = String.Empty;           
        }

        #region GridView Events
        /// <summary>
        /// This event is fired when a data row is bound to data in a GridView control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
                if (e.CommandName == "Reopen")
                {
                    Complaint complaint = new Complaint();
                    complaint.ComplaintId = Convert.ToInt64(e.CommandArgument);
                    complaint.ComplaintStatus = 1;
                    complaint.CreatedBy = Session["UserEmpID"].ToString();
                    complaint.ReopenORCloseApproveComplaint();
                    string status;
                    status = "Reopen";
                    complaint.ComplaintHistoryDetails(complaint.ComplaintId, status, usr.PersonalId, System.DateTime.Now);
                    PopulateGrid(gvComplaints.PageIndex);
                }
                else if (e.CommandName.ToLower() == "deletecomplaint")
                {
                    Complaint complaint = new Complaint();
                    complaint.ComplaintId = Convert.ToInt64(e.CommandArgument);
                    complaint.ComplaintStatus = 1;
                    complaint.CreatedBy = Session["UserEmpID"].ToString();
                    complaintList = new ComplaintList();
                    DataTable dt = MergeData(complaint.ComplaintId);
                    DataView dv = new DataView(dt);                     
                    if (dv.Count > 0)
                    {
                      
                        lblCmbCmpMsg.Text = "Parent complaint cannot be voided or deleted unless the children  are voided or deleted";
                   }
                    else
                    {

                        complaint.DeleteUndeleteComplaint();
                        string status;
                            status = "Delete";
                        complaint.ComplaintHistoryDetails(complaint.ComplaintId, status, usr.PersonalId, System.DateTime.Now);
                        PopulateGrid(gvComplaints.PageIndex);
                    }
                                       
                }
                else if (e.CommandName.ToLower() == "undelete")
                {
                    Complaint complaint = new Complaint();
                    complaint.ComplaintId = Convert.ToInt64(e.CommandArgument);
                    complaint.ComplaintStatus = 0;
                    complaint.CreatedBy = Session["UserEmpID"].ToString();
                    complaint.DeleteUndeleteComplaint();
                    string status;
                    status = "Un Delete";
                    complaint.ComplaintHistoryDetails(complaint.ComplaintId, status, usr.PersonalId, System.DateTime.Now);
                    PopulateGrid(gvComplaints.PageIndex);
                }           
        }
        private DataTable MergeData(Int64 complaintId)
        {           
                CheckCombineComplaint(complaintId);
                DataTable dt = new DataTable();
                dt.Columns.Add("IndexNo", Type.GetType("System.Int64"));              
                DataRow dr;
                foreach (Complaint complaint in complaintList.List)
                {
                    dr = dt.NewRow();                 
                    dr["IndexNo"] = complaint.IndexNo.ToString();                      
                    dt.Rows.Add(dr);
                }
                return dt;           
        }
                
        private void CheckCombineComplaint(Int64 complaintId)
        {          
                complaintList = new ComplaintList();
                complaintList.CheckCombineComplaint(complaintId);           
        }

        /// <summary>
        /// This event is fired when a data row is bound to create a new row based on the membership status.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                PopupControlExtender pce = e.Row.FindControl("pceStatus") as PopupControlExtender;

                string behaviorID = string.Concat("pce", e.Row.RowIndex);
                pce.BehaviorID = behaviorID;

                Image i = (Image)e.Row.Cells[1].FindControl("imgStatus");

                string OnMouseOverScript = string.Format("$find('{0}').showPopup();", behaviorID);
                string OnMouseOutScript = string.Format("$find('{0}').hidePopup();", behaviorID);

                i.Attributes.Add("onmouseover", OnMouseOverScript);
                i.Attributes.Add("onmouseout", OnMouseOutScript);
            }
        }

        /// <summary>
        /// This event is fired when a data row is bound to data in a GridView control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
                String Role = string.Empty;
                 MembershipList mbrshipList = usr.UserMembership;
                 for (int memberShipCounter = 0; memberShipCounter < usr.UserMembership.List.Count; memberShipCounter++)
                 {
                     if (usr.UserMembership.List[memberShipCounter].MembershipDescription == GroupName.ADMIN.ToString())
                     {
                         for (int roleCounter = 0; roleCounter < usr.UserMembership.List[memberShipCounter].Roles.List.Count; roleCounter++)
                         {
                             if (usr.UserMembership.List[memberShipCounter].Roles.List[roleCounter].RoleDescription.Equals(DS249Enums.ComplaintType.ReopenComplaint.ToString(), StringComparison.InvariantCultureIgnoreCase))
                                 Role = DS249Enums.ComplaintType.ReopenComplaint.ToString();                            
                         }
                     }
                 }
                 

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[9].Visible = false;
                    e.Row.Cells[13].Text = "";
                    e.Row.Cells[13].Visible = false;
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DateTime createdDate = DateTime.MinValue;
                    DateTime d2 = new DateTime();
                    d2 = DateTime.Now;
                    int DaysDiff = -1;
                    if (e.Row.Cells[1].Text != String.Empty)
                    {
                        createdDate = Convert.ToDateTime(e.Row.Cells[1].Text);
                        TimeSpan span = d2.Subtract(createdDate);
                        DaysDiff = span.Days;
                    }
                    String district = e.Row.Cells[5].Text.Trim();
                    if (e.Row.Cells[5].Text != String.Empty)
                        if (e.Row.Cells[5].Text.Length > 20)
                        {
                            e.Row.Cells[5].Text = e.Row.Cells[5].Text.Substring(0, 17) + "...";
                            e.Row.Cells[5].Attributes.Add("title", district);
                        }
                    Label lblCharge = (Label)e.Row.FindControl("lbCharge");                    
                    Boolean display = false;
                    String charge = GetStringValue(e.Row.Cells[9].Text, 2, ref display);
                    if (display)
                    {
                        lblCharge.Text = charge.ToString();
                        e.Row.FindControl("imgStatus").Visible = true;
                    }
                    else
                    {
                        lblCharge.Text = charge.ToString();
                        e.Row.FindControl("imgStatus").Visible = false;
                    }
                    e.Row.Cells[9].Visible = false;                    
                    
                    //Re Open Complaint

                    e.Row.Cells[14].Visible = false;

                    switch (e.Row.Cells[3].Text)
                    {
                            //Complaint status 1- New Complaint In DS249 System
                        case "1":
                            e.Row.FindControl("PrintButton").Visible = false;
                            break;
                        case "2":
                            e.Row.FindControl("PrintButton").Visible = true;
                            break;
                        case "3":
                            e.Row.FindControl("PrintButton").Visible = false;
                            break;
                        //Complaint Status 4 - Approved Complaints In DS249 System
                        //Open complaint routed to Advocate  
                        case "4":
                            //routed Advocate open complaint the Re-Open button to be invisible 
                            //routed Advocate closed complaint the Re-Open button to be visble
                            //if the user has Reopen rights then only enable reopen button

                            if (e.Row.Cells[12].Text.ToString() == GroupName.ADVOCATE.ToString())
                            {
                                e.Row.FindControl("PrintButton").Visible = true;
                            }
                            break;

                        //Complaint Status 5 - Routed Complaints In DS249 System  to Advocate
                            //Open complaint routed to any of Advocate  
                        case "5":
                            //routed Advocate open complaint the Re-Open button to be invisible 
                            //routed Advocate closed complaint the Re-Open button to be visble
                            //if the user has Reopen rights then only enable reopen button

                            //Open advocate complaint can be reopened by ADMIN-
                            long CaseStatusOpen=(long)DS249Enums.CaseStatues.Open;
                            long CaseStatusClosed = (long)DS249Enums.CaseStatues.Closed;
                            if (e.Row.Cells[14].Text == CaseStatusOpen.ToString() && e.Row.Cells[12].Text.ToUpper().ToString() == GroupName.ADVOCATE.ToString() && string.IsNullOrEmpty(Role))
                            {
                                e.Row.FindControl("PrintButton").Visible = false;
                            }

                            if (e.Row.Cells[14].Text == CaseStatusOpen.ToString() && e.Row.Cells[12].Text.ToUpper().ToString() == GroupName.ADVOCATE.ToString() && Role == DS249Enums.ComplaintType.ReopenComplaint.ToString())
                            {
                                e.Row.FindControl("PrintButton").Visible = true;
                            }

                            if (e.Row.Cells[14].Text == CaseStatusClosed.ToString() && e.Row.Cells[12].Text.ToUpper().ToString() == GroupName.ADVOCATE.ToString())
                            {
                                e.Row.FindControl("PrintButton").Visible = true;
                            }
                            break;                        
                        //check for default value to set the visibility property of Re-Open button.
                        default:
                            break;
                    }
                    e.Row.Cells[3].Visible = false;
                    String author = e.Row.Cells[11].Text.ToString();
                    if (author.Length > 15)
                    {
                        e.Row.Cells[11].Text = author.Substring(0, 12) + "...";
                        e.Row.Cells[11].Attributes.Add("title", author);
                    }

                    Button delButton = (Button)e.Row.FindControl("DeleteButton");
                    Button UndelButton = (Button)e.Row.FindControl("UndeleteButton");
                    if (e.Row.Cells[13].Text == "True")
                    {
                        delButton.Visible = false;
                        UndelButton.Visible = true;
                    }
                    else
                    {
                        delButton.Visible = true;
                        UndelButton.Visible = false;
                    }
                    e.Row.Cells[13].Width = 0;
                    e.Row.Cells[13].Visible = false;
                }
            
        }
        /// <summary>
        /// Occurs when the hyperlink to sort a column is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_Sorting(object sender, GridViewSortEventArgs e)
        {

                string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Descending)
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, ASCENDING);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, DESCENDING);
                }         

        }

        /// <summary>
        /// Returns true if the search is sucessfull
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="direction"></param>

        private void SortGridView(string sortExpression, string direction)
        {           
                DataView dv;
                dv = new DataView(PopulateTable(gvComplaints.PageIndex));
                dv.Sort = sortExpression + direction;
                gvComplaints.DataSource = dv;
                gvComplaints.DataBind();           
        }
        /// <summary>
        /// This is a Page Index Change event on a datagrid, that fires Occurs when one of the pager buttons is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
             if (e.NewPageIndex >= 0)
                {
                    gvComplaints.PageIndex = e.NewPageIndex;
                    gvComplaints.DataSource = PopulateTable(gvComplaints.PageIndex);
                    gvComplaints.DataBind();
                }
                else if ((e.NewPageIndex == -1) || (e.NewPageIndex == -2) || (e.NewPageIndex == -3) || (e.NewPageIndex == -4) || (e.NewPageIndex == -5))
                {
                    gvComplaints.PageIndex = 0;
                    gvComplaints.DataSource = PopulateTable(gvComplaints.PageIndex);
                    gvComplaints.DataBind();
                }
                else
                {
                    return;
                }           

        }
        /// <summary>
        /// This is a text change event that fires when the user enters the page number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJmpToPage_TextChanged(object sender, EventArgs e)
        {           
                GridViewRow rowTop = gvComplaints.TopPagerRow;
                TextBox txtJTPTop = (TextBox)rowTop.Cells[0].FindControl("txtJmpToPage");
                GridViewRow rowBottom = gvComplaints.BottomPagerRow;
                TextBox txtJTPBottom = (TextBox)rowBottom.Cells[0].FindControl("txtJmpToPage");
                if (txtJTPTop.Text.Trim().Length > 0)
                {
                    gvComplaints.PageIndex = Convert.ToInt32(txtJTPTop.Text.Trim()) - 1;
                    gvComplaints.DataSource = PopulateTable(gvComplaints.PageIndex);
                    gvComplaints.DataBind();
                }
                else if (txtJTPBottom.Text.Trim().Length > 0)
                {
                    gvComplaints.PageIndex = Convert.ToInt32(txtJTPBottom.Text.Trim()) - 1;
                    gvComplaints.DataSource = PopulateTable(gvComplaints.PageIndex);
                    gvComplaints.DataBind();
                }
                else
                {
                    return;
                }           

        }

        /// <summary>
        /// For each GridView row is databound, the GridView's RowDataBound event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_DataBound(object sender, EventArgs e)
        {
           
                Menu menuPagerTop = (Menu)gvComplaints.TopPagerRow.FindControl("menuPager");
                Menu menuPagerBottom = (Menu)gvComplaints.BottomPagerRow.FindControl("menuPager");
                if ((gvComplaints.PageCount - gvComplaints.PageIndex) >= 5)
                {
                    for (int i = gvComplaints.PageIndex; i < gvComplaints.PageIndex + 5; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvComplaints.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }
                }
                else if ((gvComplaints.PageCount - gvComplaints.PageIndex) == 4)
                {
                    for (int i = gvComplaints.PageIndex - 1; i < gvComplaints.PageIndex + 4; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvComplaints.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }
                }
                else if ((gvComplaints.PageCount - gvComplaints.PageIndex) == 3)
                {
                    for (int i = gvComplaints.PageIndex - 2; i < gvComplaints.PageIndex + 3; i++)
                    {

                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvComplaints.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }
                }
                else if ((gvComplaints.PageCount - gvComplaints.PageIndex) == 2)
                {
                    for (int i = gvComplaints.PageIndex - 3; i < gvComplaints.PageIndex + 2; i++)
                    {

                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvComplaints.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }

                }
                else if ((gvComplaints.PageCount - gvComplaints.PageIndex) == 1)
                {
                    for (int i = gvComplaints.PageIndex - 4; i < gvComplaints.PageIndex + 1; i++)
                    {

                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvComplaints.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }

                }
                else
                {
                    return;
                }
                if (gvComplaints.PageIndex == gvComplaints.PageCount - 1)
                {
                    ImageButton imgDbleNxt = (ImageButton)gvComplaints.TopPagerRow.FindControl("imgDoubleNext");
                    ImageButton imgNxt = (ImageButton)gvComplaints.TopPagerRow.FindControl("imgNext");
                    imgDbleNxt.Enabled = false;
                    imgNxt.Enabled = false;
                    imgNxt.ToolTip = "";

                }
                else if (gvComplaints.PageIndex == 0)
                {
                    ImageButton imgDblePrv = (ImageButton)gvComplaints.TopPagerRow.FindControl("imgDoublePrevious");
                    ImageButton imgPrv = (ImageButton)gvComplaints.TopPagerRow.FindControl("imgPrevious");
                    imgDblePrv.Enabled = false;
                    imgPrv.Enabled = false;
                    imgPrv.ToolTip = "";

                }
                else
                {
                    return;
                }
           

        }
        /// <summary>
        /// This event Occurs when a menu item in a Menu control is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void menuPager_MenuItemClick(object sender, MenuEventArgs e)
        {           
                gvComplaints.PageIndex = Int32.Parse(e.Item.Value);
                gvComplaints.DataSource = PopulateTable(gvComplaints.PageIndex);
                gvComplaints.DataBind();            
        }

        #endregion

        /// <summary>
        /// This method returns string based on the search criteria
        /// </summary> 
        /// <param name="str"></param>
        /// <param name="characterToCount"></param>
        /// <param name="Display"></param>
        /// <returns></returns>
        public static String GetStringValue(String str, int characterToCount, ref Boolean Display)
        {
            String[] aryList;
            String[] separator = { "," };
           
                aryList = str.Split(separator, StringSplitOptions.None);
                String result = String.Empty;
                if (aryList.Length > characterToCount)
                {
                    for (int i = 0; i < characterToCount; i++)
                    {
                        result += aryList[i].ToString() + ", ";
                    }
                    Display = true;
                    return result;
                }
                else
                    return str;           
        }
        /// <summary>
        /// This button click events redirects the page to dashboard page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UserManagementButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }

    }
}