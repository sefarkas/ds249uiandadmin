﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using System.Collections;
using System.Data;


namespace DSNY.DSNYCP.Administrator
{
    /// <summary>
    /// This class provides the functionality for events and methods for the Admin Dashboard.
    /// </summary>
   
    public partial class Dashboard : System.Web.UI.Page
    {

       
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
        
        PersonnelList pList = new PersonnelList();
        User usr;
        /// <summary>
        ///  This method sorts the databound items in a gridview
        /// </summary>
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            usr = (User)Session["clsUser"];
            if (usr != null)
            {
                if (!usr.IsInMemberShip(GroupName.ADMIN))
                    Response.Redirect("UnAuthorized.aspx");
                else
                {
                    usr.CurrentMembership = GroupName.ADMIN;
                    SecurityUtility.AttachRolesToUser(usr);
                }

                if (!User.IsInRole("Admin"))
                    Response.Redirect("UnAuthorized.aspx");
            }
            else
                Response.Redirect("Login.aspx");

            if (Session["IsAuthenticated"] == null)
            {
                Response.Redirect("login.aspx?Session=False");
            }
            if (Session["IsAuthenticated"].ToString() == "false")
            {
                Response.Redirect("login.aspx?Session=False");
            }

            Page.Form.DefaultButton = Button1.UniqueID;
            if (!Page.IsPostBack)
            {
                DataTable employeeTable;
                LoadPersons(String.Empty);
                employeeTable = GetDataTableFromList();
                EmployeeGridView.DataSource = employeeTable;
                EmployeeGridView.DataBind();
            }
            UserTable.Visible = false;
            DisplayButtonsBasedOnPermission();
        }
        /// <summary>
        /// This methods the persons info to the list
        /// </summary>
        /// <param name="searchString"></param>
        private void LoadPersons(string searchString)
        {
            pList.LoadAll(searchString);
        }
        /// <summary>
        /// This method displays the button based on the membership role permissions
        /// </summary>
        private void DisplayButtonsBasedOnPermission()
        {
            if (!User.IsInRole("Case Management"))
            {
                ComplaintManagementButton.Visible = false;
            }
            else
                ComplaintManagementButton.Visible = true;
        }

        /// <summary>
        /// This is a text change event that fires based on the employee search criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EmpSearchTextBox_TextChanged()
        {
            DataTable employeeTable;
            String empname;           
                empname = EmpSearchTextBox.Text.ToString().Trim();
                LoadPersons(empname);
                employeeTable = GetDataTableFromList();
                EmployeeGridView.DataSource = employeeTable;
                EmployeeGridView.DataBind();
            
        }
        /// <summary>
        /// This is a text change event that fires based on the employee search criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            EmpSearchTextBox_TextChanged();
        }
        /// <summary>
        /// This event occurs when the hyperlink to sort a column is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EmployeeGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

                if (GridViewSortDirection == SortDirection.Descending)
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, ASCENDING);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, DESCENDING);
                }    

        }

        /// <summary>
        /// This method sorts the data from the data view based on the search criteria
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="direction"></param>

        private void SortGridView(string sortExpression, string direction)
        {           
                DataView dv;
            dv = new DataView( PopulateTable() )
            {
                Sort = sortExpression + direction
            };
            EmployeeGridView.DataSource = dv;
                EmployeeGridView.DataBind();           
        }
        /// <summary>
        /// This is a Page Index Change event on a datagrid, that fires Occurs when one of the pager buttons is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EmployeeGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {           
                if (e.NewPageIndex >= 0)
                {
                    EmployeeGridView.PageIndex = e.NewPageIndex;
                    EmployeeGridView.DataSource = PopulateTable();
                    EmployeeGridView.DataBind();
                }
                else if ((e.NewPageIndex == -1) || (e.NewPageIndex == -2) || (e.NewPageIndex == -3) || (e.NewPageIndex == -4) || (e.NewPageIndex == -5))
                {
                    EmployeeGridView.PageIndex = 0;
                    EmployeeGridView.DataSource = PopulateTable();
                    EmployeeGridView.DataBind();
                }
                else
                {
                    return;
                }            

        }
        /// <summary>
        /// This methods laods the person list data to the data table
        /// </summary>
        /// <returns></returns>
        private DataTable PopulateTable()
        {            
                DataTable personnelList;
                if (Session["Employee"] != null)
                    personnelList = (DataTable)Session["Employee"];
                else
                {
                    LoadPersons(EmpSearchTextBox.Text.Trim());
                    personnelList = GetDataTableFromList();
                }
                return personnelList;          
        }
        /// <summary>
        /// This methopds gets the data from the personlist and binds it to data table 
        /// </summary>
        /// <returns></returns>
        private DataTable GetDataTableFromList()
        {
             DataTable dt = new DataTable();
                dt.Columns.Add("PersonID", Type.GetType("System.Int64"));
                dt.Columns.Add("ReferenceNumber", Type.GetType("System.String"));
                dt.Columns.Add("UserName", Type.GetType("System.String"));
                dt.Columns.Add("FirstName", Type.GetType("System.String"));
                dt.Columns.Add("MiddleName", Type.GetType("System.String"));
                dt.Columns.Add("LastName", Type.GetType("System.String"));
                dt.Columns.Add("SSN", Type.GetType("System.String"));
                dt.Columns.Add("IsActive", Type.GetType("System.Boolean"));
                dt.Columns.Add("PersonnelTitle", Type.GetType("System.String"));
                dt.Columns.Add("PayCodeDescription", Type.GetType("System.String"));
                dt.Columns.Add("EmailID", Type.GetType("System.String"));
                dt.Columns.Add("IsDeleted", Type.GetType("System.Boolean"));
                dt.Columns.Add("IsAdmin", Type.GetType("System.Boolean"));
                DataRow dr;
                foreach (Personnel personnel in pList.List)
                {
                    dr = dt.NewRow();
                    dr["PersonID"] = personnel.PersonId;
                    dr["ReferenceNumber"] = personnel.ReferenceNumber;
                    dr["UserName"] = personnel.UserName;
                    dr["FirstName"] = personnel.FirstName;
                    dr["MiddleName"] = personnel.MiddleName;
                    dr["LastName"] = personnel.LastName;
                    dr["SSN"] = personnel.Ssn;
                    dr["IsActive"] = personnel.IsActive;
                    dr["PersonnelTitle"] = personnel.PersonnelTitle;
                    dr["PayCodeDescription"] = personnel.PayCodeDescription;
                    dr["EmailID"] = personnel.EmailId;
                    dr["IsDeleted"] = personnel.IsDeleted;
                    dr["IsAdmin"] = personnel.IsAdmin;
                    dt.Rows.Add(dr);
                }

                Session["Employees"] = dt;
                return dt;            
        }  
        /// <summary>
        /// This method gets the list of user info and binds the data to the data table
        /// </summary>
        /// <param name="contextKey"></param>
        /// <returns></returns>
        protected void EmployeeGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[10].Visible = false;
                e.Row.Cells[11].Visible = false;

                if (!User.IsInRole("Assign Roles"))
                    e.Row.Cells[12].Visible = false;
            }


            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Button AddNewButton = (Button)e.Row.FindControl("AddUserButton");
                Button DeleteButton = (Button)e.Row.FindControl("DeleteUserButton");
                LinkButton UserNameLinkButton = (LinkButton)e.Row.FindControl("UserNameLinkButton");
                Button AddRoles = (Button)e.Row.FindControl("btnAddRoles");
              // unused according to VS2019 by Farkas 27Feb2020  Button AddCharges = (Button)e.Row.FindControl("btnAddCharges");
                String isDeleted = e.Row.Cells[10].Text;
                String userName = ((LinkButton)e.Row.Cells[3].FindControl("UserNameLinkButton")).Text.Trim();

                if (isDeleted == "True" || userName == "")
                {
                    AddNewButton.Visible = true;
                    UserNameLinkButton.Visible = false;
                    DeleteButton.Visible = false;
                    AddRoles.Visible = false;
                }
                else
                {
                    AddNewButton.Visible = false;
                    UserNameLinkButton.Visible = true;
                    DeleteButton.Visible = true;
                    AddRoles.Visible = true;
                }
                e.Row.Cells[10].Visible = false;
                e.Row.Cells[10].Width = 0;
                e.Row.Cells[11].Visible = false;
                e.Row.Cells[11].Width = 0;

                if (!User.IsInRole("Assign Roles"))
                    e.Row.Cells[12].Visible = false;

            }

        }
        /// <summary>
        /// For each GridView row is databound, the GridView's RowDataBound event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EmployeeGridView_DataBound(object sender, EventArgs e)
        {
           
                Menu menuPagerTop = (Menu)EmployeeGridView.TopPagerRow.FindControl("menuPager");
                Menu menuPagerBottom = (Menu)EmployeeGridView.BottomPagerRow.FindControl("menuPager");

                if ((EmployeeGridView.PageCount - EmployeeGridView.PageIndex) >= 5)
                {
                    for (int i = EmployeeGridView.PageIndex; i < EmployeeGridView.PageIndex + 5; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (EmployeeGridView.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }
                }
                else if ((EmployeeGridView.PageCount - EmployeeGridView.PageIndex) == 4)
                {
                    int index = 0;
                    if (EmployeeGridView.PageIndex > 4)
                    {
                        index = EmployeeGridView.PageIndex - 5;
                    }
                    for (int i = index; i < EmployeeGridView.PageIndex + 4; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (EmployeeGridView.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }
                }
                else if ((EmployeeGridView.PageCount - EmployeeGridView.PageIndex) == 3)
                {
                    //int index = 0;
                    //if (EmployeeGridView.PageIndex > 3)
                    //{
                    //    index = EmployeeGridView.PageIndex - 4;
                    //}
                    for (int i = EmployeeGridView.PageIndex; i < EmployeeGridView.PageIndex + 3; i++)
                    {

                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (EmployeeGridView.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }
                }
                else if ((EmployeeGridView.PageCount - EmployeeGridView.PageIndex) == 2)
                {
                    int index = 0;
                    if (EmployeeGridView.PageIndex > 2)
                    {
                        index = EmployeeGridView.PageIndex - 3;
                    }
                    for (int i = index; i < EmployeeGridView.PageIndex + 2; i++)
                    {

                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (EmployeeGridView.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }

                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }

                }
                else if ((EmployeeGridView.PageCount - EmployeeGridView.PageIndex) == 1)
                {
                    int index = 0;
                    if (EmployeeGridView.PageIndex > 1)
                    {
                        index = EmployeeGridView.PageIndex - 2;
                    }
                    for (int i = index; i < EmployeeGridView.PageIndex + 1; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (EmployeeGridView.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }
                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);
                    }

                }
                else
                {
                    return;
                }
                if (EmployeeGridView.PageIndex == EmployeeGridView.PageCount - 1)
                {
                    ImageButton imgDbleNxt = (ImageButton)EmployeeGridView.TopPagerRow.FindControl("imgDoubleNext");
                    ImageButton imgNxt = (ImageButton)EmployeeGridView.TopPagerRow.FindControl("imgNext");
                    imgDbleNxt.Enabled = false;
                    imgNxt.Enabled = false;
                    imgNxt.ToolTip = "";
                }
                else if (EmployeeGridView.PageIndex == 0)
                {
                    ImageButton imgDblePrv = (ImageButton)EmployeeGridView.TopPagerRow.FindControl("imgDoublePrevious");
                    ImageButton imgPrv = (ImageButton)EmployeeGridView.TopPagerRow.FindControl("imgPrevious");
                    imgDblePrv.Enabled = false;
                    imgPrv.Enabled = false;
                    imgPrv.ToolTip = "";
                }
                else
                {
                    return;
                }          

        }
        /// <summary>
        /// This is a text change event that fires when the user enters the page number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void JmpToPageTextBox_TextChanged(object sender, EventArgs e)
        {
           
                GridViewRow rowTop = EmployeeGridView.TopPagerRow;
                TextBox txtJTPTop = (TextBox)rowTop.Cells[0].FindControl("JmpToPageTextBox");
                GridViewRow rowBottom = EmployeeGridView.BottomPagerRow;
                TextBox txtJTPBottom = (TextBox)rowBottom.Cells[0].FindControl("JmpToPageTextBox");
                if (txtJTPTop.Text.Trim().Length > 0)
                {
                    EmployeeGridView.PageIndex = Convert.ToInt32(txtJTPTop.Text.Trim()) - 1;
                    EmployeeGridView.DataSource = PopulateTable();
                    EmployeeGridView.DataBind();
                }
                else if (txtJTPBottom.Text.Trim().Length > 0)
                {
                    EmployeeGridView.PageIndex = Convert.ToInt32(txtJTPBottom.Text.Trim()) - 1;
                    EmployeeGridView.DataSource = PopulateTable();
                    EmployeeGridView.DataBind();
                }
                else
                {
                    return;
                }         

        }
        /// <summary>
        /// This event Occurs when a menu item in a Menu control is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void MenuPager_MenuItemClick(object sender, MenuEventArgs e)
        {           
                EmployeeGridView.PageIndex = Int32.Parse(e.Item.Value);
                EmployeeGridView.DataSource = PopulateTable();
                EmployeeGridView.DataBind();            
        }
        /// <summary>
        /// The RowCommand event is raised when a button is clicked in the GridView control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EmployeeGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {            
                if (e.CommandName == "DeleteUser")
                {
                    Complaint complaint = new Complaint();
                usr = new User
                {
                    PersonalId = Convert.ToInt64( e.CommandArgument )
                };
                usr.Delete();


                    User user;
                    user = (User)Session["clsUser"];
                    string Status;
                    
                    Status = "Delete User";
                    complaint.AdminPersonDetails(user.PersonalId, Status, usr.PersonalId);



                    EmpSearchTextBox_TextChanged();
                }
                else if (e.CommandName == "AddRoles")
                {
                    Response.Redirect(String.Format("UserMembership.aspx?ID={0}", e.CommandArgument.ToString()));
                }


                else if (e.CommandName == "AddCharges")
                {
                    Response.Redirect(String.Format("UserCharges.aspx?ID={0}", e.CommandArgument.ToString()));
                }

                else if (e.CommandName == "AddUser")
                {
                    String userName = String.Empty;
                    usr = new User();
                    Session["UsrMgmt"] = usr;

                    PersonIDTextBox.Text = e.CommandArgument.ToString();
                    UserTable.Visible = true;
                    UserNameTextBox.Enabled = true;
                    UserNameTextBox.Focus();

                    DataView dv;
                dv = new DataView( PopulateTable() )
                {
                    RowFilter = "PersonID = " + Convert.ToInt64( e.CommandArgument )
                };

                if (dv.Count > 0)
                    {
                        UserNameTextBox.Text = dv[0].Row["UserName"].ToString();
                        if (UserNameTextBox.Text.Trim() == "")
                        {

                            userName = GetNewUserName(Convert.ToInt64(e.CommandArgument));
                            UserNameTextBox.Enabled = true;
                            UserNameTextBox.Text = userName;
                            Session["IsNewUser"] = true;
                        }
                        else
                        {
                            Session["IsNewUser"] = false;
                        }

                        EMailIDTextBox.Text = dv[0].Row["EmailID"].ToString();
                    }
                    AdminUserCheckBox.Checked = false;
                    usr.PersonalId = Convert.ToInt64(PersonIDTextBox.Text);
                    usr.UserId = Convert.ToString(userName);

                }
                else if (e.CommandName == "Reset")
                {
                    Int64 personID = Convert.ToInt64(e.CommandArgument);

                    usr = new User();
                    usr.Load(personID);
                    Session["UsrMgmt"] = usr;

                    PersonIDTextBox.Text = personID.ToString();
                    UserNameTextBox.Enabled = false;
                    UserTable.Visible = true;
                    hdnStatus.Value = "Reset";
                    PasswordTextBox.Focus();
                    Session["IsNewUser"] = false;
                    DataView dv;

                dv = new DataView( PopulateTable() )
                {
                    RowFilter = "PersonID = " + personID
                };
                if (dv.Count > 0)
                    {
                        UserNameTextBox.Text = dv[0].Row["UserName"].ToString();
                        EMailIDTextBox.Text = dv[0].Row["EmailID"].ToString();
                        AdminUserCheckBox.Checked = (Boolean)dv[0].Row["IsAdmin"];
                    }
                    else
                    {
                        AdminUserCheckBox.Checked = false;
                    }

                }            

        }

        /// <summary>
        /// This method returns the name of the user based on the personnel ID
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>

        private String GetNewUserName(Int64 personID)
        {
            usr = new User
            {
                PersonalId = personID
            };
            return usr.GetNewUserId();
        }

        /// <summary>
        /// This method saves the information of the user to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SaveButton_Click(object sender, EventArgs e)
        {
            Complaint complaint = new Complaint();
            usr = (User)Session["UsrMgmt"];

            User user;
            user = (User)Session["clsUser"];

            if (PersonIDTextBox.Text.Trim() != "")
            {
                usr.PersonalId = Convert.ToInt64(PersonIDTextBox.Text);
            }
            usr.UserId = UserNameTextBox.Text.Trim();
            usr.Password = PasswordTextBox.Text.Trim();
            usr.EmailId = EMailIDTextBox.Text.Trim();
            usr.IsTempPassword = true;
            usr.IsAdmin = AdminUserCheckBox.Checked;
            // String result = usr.Save((Boolean)Session["IsNewUser"], user.PersonalId);
            String result = usr.Save((Boolean)Session["IsNewUser"]);
            if (result == "")
            {
                UserNameTextBox.Text = "";
                EMailIDTextBox.Text = "";
                UserTable.Visible = false;
                if (usr.EmailId != "")
                {
                    SendEmail(usr.UserId, usr.Password, usr.EmailId);
                }
                   
                string Status;
                Status = "New User";
                    
                if(hdnStatus.Value=="Reset")
                    Status = "ResetPassword";
                    
                    
                complaint.AdminPersonDetails(user.PersonalId, Status,  usr.PersonalId);
                   
                MessageLabel.Visible = false;
                MessageLabel.Text = "";
                EmpSearchTextBox_TextChanged();
                if (hdnStatus.Value == "Reset")
                {
                    hdnStatus.Value = "";
                }
                else
                {
                Response.Redirect(String.Format("UserMembership.aspx?ID="+ usr.PersonalId, true));
                }
            }
            else
            {
            // allow the Admin program to set a new PWD for an existing user ...
                try
                {
                    usr.IsTempPassword = false;
                    usr.SaveUserPwdViaDS249Admin();
                    UserTable.Visible = true;
                    UserNameTextBox.Focus();
                    MessageLabel.Visible = true;
                    MessageLabel.Text = usr.UserId + " has the password you just entered.";
                    MessageLabel.ForeColor = System.Drawing.Color.Green;
                
                }
                catch (Exception ex)
                {
                    MessageLabel.Visible = true;
                    MessageLabel.Text = "System did not change password for "  + usr.UserId + " because " + ex.Message;
                    MessageLabel.ForeColor = System.Drawing.Color.Red;
                }
            }

        }
        /// <summary>
        /// This method sends email with userid and password
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        private void SendEmail(String userName, String password, String email)
        {
            DSNY.DSNYCP.ClassHierarchy.Admin adminSvc = new DSNY.DSNYCP.ClassHierarchy.Admin
            {
                ToAddress = new String[1]
            };
            adminSvc.ToAddress.SetValue(email, 0);
            adminSvc.SendAccountInformationEmail(userName, password);
        }
        /// <summary>
        /// This button click events redirects the page to complaints
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ComplaintManagementButton_Click(object sender, EventArgs e)
        {            
            Response.Redirect("Complaints.aspx");
        }
        /// <summary>
        /// This button click events redirects the page to logout page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnLogout_Click(object sender, ImageClickEventArgs e)
        {            
            Response.Redirect("logout.aspx");
        }

    }
}