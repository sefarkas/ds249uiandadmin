﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.Administrator.Error" Title="DSNY : DS-249 ADMIN" CodeBehind="Error.aspx.cs" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 900px; height: 30px;" class="tableMargin" border="0px" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top">
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                    itServiceDesk@dsny.nyc.gov</a></span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="styleHeading">
                <div>
                    <span>ERROR</span></div>
            </td>
        </tr>
    </table>
     <table align="center" width="80%">
        <tr>
            <td class="todate">
                <br />
                <br />
                Opps...!!! An Unexpected Error Has Occured In The Application. Please Contact Helpdesk.<br />
                To Start Over again, Please Click 
             <%--   <a href="Login.aspx">Here</a>--%>
                <br />             
                <asp:Button ID="btnErrorID" runat="server" Text="Here" 
                    onclick="btnErrorID_Click"></asp:Button>
               
            </td>
        </tr>       
    </table>
</asp:Content>
