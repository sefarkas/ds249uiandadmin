﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DSNY.DSNYCP.ClassHierarchy;



namespace DSNY.DSNYCP.Administrator
{
    /// <summary>
    /// This method provides functionality to login authenticated user to the website.
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {           
                Session["IsAuthenticated"] = "false";
                Session["UserRoles"] = null;
                txtUserName.Focus();
                if (Request.QueryString["Session"] == "False")
                {
                    lMessage.Text = "Session Expired! Please Login Again";
                    lMessage.ForeColor = System.Drawing.Color.Red;
                }
                Page.Form.DefaultButton = btnLogin.UniqueID;
                if (IsPostBack)
                {
                    
                } 
        }
        /// <summary>
        /// This method allows the user to login authenticated user to the website.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {
             User user = new User();
                user.UserId = txtUserName.Text.Trim();
                user.Password = txtPassword.Text.Trim();
                if (user.AuthenticateUser())
                {
                    if (user.IsInMemberShip(GroupName.ADMIN))
                    {
                        if (user.PersonalId != -1)
                        {
                            Session["IsAuthenticated"] = "true";
                            Session["UserEmpID"] = user.PersonalId;
                            Session["User"] = txtUserName.Text;
                            Session["clsUser"] = user;
                            SecurityUtility.RedirectFromLoginPage(txtUserName.Text.Trim(), "Dummy", true);
                        }
                        else
                        {
                            txtUserName.Text = String.Empty;
                            lMessage.Text = "Login Failed. The UserName/Password Entered is Incorrect";
                            lMessage.ForeColor = System.Drawing.Color.Red;
                        }
                    }
                    else
                    {
                        txtUserName.Text = String.Empty;
                        lMessage.Text = "Login Failed. You need to be an Admin to login into the system.";
                        lMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else
                {
                    txtUserName.Text = String.Empty;
                    lMessage.Text = "Login Failed. The UserName/Password Entered is Incorrect";
                    lMessage.ForeColor = System.Drawing.Color.Red;
                }
           
        }
    }
}