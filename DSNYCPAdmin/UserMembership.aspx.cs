﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.Administrator
{
    /// <summary>
    /// This method provides the functionality for the respective memners to view there role screen
    /// </summary>
    public partial class UserMembership : System.Web.UI.Page
    {
        User usr;
        RoleList roleList;
        String aryJS = string.Empty;        

        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {


            Button4.Style.Add("display", "none");    
                btnUpdate.OnClientClick = String.Format("PerformPostBack('{0}','{1}')", btnUpdate.UniqueID, "");
                if (!IsPostBack)
                {
                    usr = new User();
                    Int64 personalID = Convert.ToInt64(Request.QueryString["Id"]);
                    usr.Load(personalID);
                    lbUserName.Text = usr.UserId.ToString();
                    Session["usrMgmt"] = usr;
                    LoadMembershipGrid();
                }
                foreach (ListItem li in cblRoleList.Items)
                {
                    li.Attributes.Add("dvalue", li.Value);
                }          
        }


        protected void Button4_Click(object sender, EventArgs e)
        {
            btnUpdate_Click(sender, e);
        }
        /// <summary>
        /// This is a checkbox check event that gets selected when the member hits on the role button 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkMembership_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox checkbox = (CheckBox)sender;
            GridViewRow row = (GridViewRow)checkbox.NamingContainer;

            LinkButton lbRoles = (LinkButton)row.FindControl("lbRoles");
            String ID = gvMembership.DataKeys[row.DataItemIndex].Value.ToString();
            lbRoles.Enabled = true;

            if (checkbox.Checked)
            {
                AddMembership(Convert.ToInt64(checkbox.Text));
                lbRoles.Enabled = true;                
            }
            else
            {
                RemoveMembership(Convert.ToInt64(checkbox.Text));
                lbRoles.Enabled = false;
                lbRoles.Attributes.Remove("onclick");
            }
            LoadMembershipGrid();
        }
        /// <summary>
        /// This button click event fires when the user clicks on the logout button
        /// This redirects the user to the logout page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Logout.aspx");
        }
        /// <summary>
        /// For each GridView row is databound, the GridView's RowDataBound event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvMembership_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[1].Visible = false;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                usr = (User)Session["UsrMgmt"];
                Membership membership = usr.UserMembership.List.Find(delegate(Membership p) { return p.MembershipId == Convert.ToInt64(e.Row.Cells[1].Text); });
                e.Row.Cells[1].Visible = false;
                CheckBox chkMembership = (CheckBox)e.Row.FindControl("chkMembership");
                LinkButton lbRoles = (LinkButton)e.Row.FindControl("lbRoles");
                String ID = gvMembership.DataKeys[e.Row.DataItemIndex].Value.ToString();
                if (membership != null)
                {
                    chkMembership.Checked = true;
                    lbRoles.Enabled = true;                   
                }
            }
        }

        protected void gvMembership_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("roles", StringComparison.InvariantCultureIgnoreCase))
            {
                LoadRolesList(Convert.ToInt32(e.CommandArgument));
            }
        }
        /// <summary>
        /// This method updated the roles from the membership data table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            Int64 membershipID = Convert.ToInt64(hdnMembershipID.Value);
            AddRolesToMembership(membershipID);
            LoadMembershipGrid();
            cblRoleList.ClearSelection();
        }
        /// <summary>
        /// This button click event fires when the user clicks on the cancel button
        /// This redirects the user to the dashboard page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibCancel_Click(object sender, ImageClickEventArgs e)
        {
            Session["UsrMgmt"] = null;
            Response.Redirect("Dashboard.aspx");
        }
        /// <summary>
        /// This button click event fires when the user clicks on the save button
        /// This saves the information of the user to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibSave_Click(object sender, ImageClickEventArgs e)
        {
            User user;
            user = (User)Session["clsUser"];
            try
            {
                string msg = SaveUserMembership();
                if (msg.Equals(string.Empty))
                {        
                    lbMessage.Text = "Membership Saved Successfully";
                    lbMessage.ForeColor = System.Drawing.Color.Green;
                    usr.HistoryMemberShipRoles(user.PersonalId);            
                }
                else
                {
                    lbMessage.Text = "Error Occured: Unable to save membership (" + msg + ")";
                    lbMessage.ForeColor = System.Drawing.Color.Red;                
                }
            }
            catch (Exception ex)
            {
                lbMessage.Text = "Exception: Unable to save membership (" + ex.Message + ")";
                lbMessage.ForeColor = System.Drawing.Color.Red;
            }

        }
        /// <summary>
        /// This is a  button click event fires when the user clicks on the button
        /// This saves the updated information of the user to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       


        #region PRIVATE METHODS

        private void LoadRolesList(int membershipID)
        {
            roleList = new RoleList();
            roleList.Load(membershipID);
            cblRoleList.DataTextField = "RoleDescription";
            cblRoleList.DataValueField = "RoleID";
            cblRoleList.DataSource = roleList.List;
            cblRoleList.DataBind();

            //Select roles checkboxes
            usr = (User)Session["UsrMgmt"];
            Membership membership = usr.UserMembership.List.Find(delegate(Membership p) { return p.MembershipId == membershipID; });
            foreach (Role role in membership.Roles.List)
            {
                cblRoleList.Items.FindByValue(role.RoleId.ToString()).Selected = true;                
            }
            //Set value of hidden field which is used later to collect roles assigned to a perticular membership.
            hdnMembershipID.Value = membershipID.ToString();
            ModalPopupExtender1.Show();
        }

        private void AddMembership(Int64 MembershipID)
        {
            usr = (User)Session["UsrMgmt"];
            MembershipList membershipList = usr.UserMembership;
            membershipList.AddMembership(MembershipID, usr.PersonalId);
        }

        private void RemoveMembership(Int64 MembershipID)
        {
            usr = (User)Session["UsrMgmt"];
            MembershipList membershipList = usr.UserMembership;
            membershipList.RemoveMembership(MembershipID);
        }

        private void AddRolesToMembership(Int64 MembershipId)
        {
            User user = new User();
            usr = (User)Session["UsrMgmt"];
            user.AuthenticateUser();
            Session["clsUser"] = user;
            MembershipList membershipList = usr.UserMembership;
            membershipList.AddRolesToMembership(MembershipId, GetRoleIDList());
        }

        private List<Int16> GetRoleIDList()
        {
            List<Int16> roleID = new List<short>();
            for (int i = 0; i < cblRoleList.Items.Count; i++)
            {
                if (cblRoleList.Items[i].Selected)
                    roleID.Add(Convert.ToInt16(cblRoleList.Items[i].Value));
            }
            return roleID;

        }
        private String SaveUserMembership()
        {
            usr = (User)Session["UsrMgmt"];
            return usr.Save(false);
        }

        private void LoadMembershipGrid()
        {
            MembershipList membershipList = new MembershipList();
            membershipList.Load();
            gvMembership.DataSource = membershipList.List;
            gvMembership.DataBind();
        }


        #endregion

       
    }
}
