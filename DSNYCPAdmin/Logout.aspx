﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.Administrator.Logout" Title="Logout" CodeBehind="Logout.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="815" border="0" cellspacing="0" cellpadding="2" class="main">
        <tr>
            <td colspan="3" class="headertext">
                LOG OUT<img src="includes/img/clear_pix.gif" height="1" width="315" /><font class="headerHelptext">IT
                    HELPDESK: 212.437.4200</font><br />
                <img src="includes/img/border_top.gif" />
            </td>
        </tr>
        <tr>
            <td colspan="3" class="headertext">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table class="loginBox" cellpadding="14">
                    <tr>
                        <td>
                            You have successfully logged out
                        </td>
                        <br />
                        <br />
                    </tr>
                    <tr align="center">
                        <td align="center">
                            Click here to
                            <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/includes/img/btn_login.gif"
                                OnClick="btnLogin_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
