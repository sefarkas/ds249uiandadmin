﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master"
    CodeBehind="UserCharges.aspx.cs" Inherits="DSNY.DSNYCP.Administrator.UserCharges" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="includes/CSS/DSNYcss.css" rel="stylesheet" type="text/css" />
    <link href="includes/CSS/styles.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">

        var vcHasResult = 0;
        var vcGetttingResult = 0;


        function PerformPostBack(sender, e) {
            //__doPostBack('ctl00_ctl00_MasterContentPlaceHolder_CustomerContentPlaceHolder_THangarFormView_HangarNumberTextBoxAdd', 'TextChanged');

           // $get('ctl00$ContentPlaceHolder1$Button1').click();
        }

        // Function for Employee Search
        function vcShownev(source, e) {
            vcHasResult = 1;
            vcGetttingResult = 0;
            $get('<%=lbvcSearchMsg.ClientID%>').innerText = "";
        }

        function vcPopulatingev(source, e) {
            vcGetttingResult = 1;
            vcHasResult = 0;
            // $get("ctl00_ContentPlaceHolder1_lbSearchMsg1").innerText = "Retreiving..";
            $get('<%=lbvcSearchMsg.ClientID%>').innerText = "Retreiving..";
        }

        function vcOnhiding(source, e) {

            if (vcGetttingResult == 1) {
                if (vcHasResult == 0) {
                    $get('<%=lbvcSearchMsg.ClientID%>').innerText = "No Match Found";

                }
            }
        }

        function OpenViewDocument() {

            var URL = "ViewDocument.aspx";
            var dialogResults = window.open(URL, '');


        }


   
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <table style="width: 900px; height: 50px" class="tableMargin" border="0px" cellpadding="0px"
            cellspacing="0px" align="center">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td valign="top">
                                <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                                <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                    at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                        itServiceDesk@dsny.nyc.gov</a></span>
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="styleHeading">
                    <div>
                        <span>DS-249 USER CHARGES </span>
                    </div>
                </td>
                <td class="styleHeading">
                    <asp:ImageButton ID="btnLogout" ImageUrl="~/includes/img/btn_logout.gif" runat="server"
                        OnClick="btnLogout_Click" CausesValidation="False" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label ID="lbMessage" runat="server" Width="50%" Font-Size="Small" Font-Bold="true"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                        runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                        <Animations>                                
                                                <OnUpdated>
                                                    <FadeOut Duration="5.0" Fps="24" />
                                                </OnUpdated>
                        </Animations>
                    </cc1:UpdatePanelAnimationExtender>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:UpdatePanel ID="upMembership" runat="server">
                        <ContentTemplate>
                            <center>
                                <table>
                                    <tr>
                                        <td class="fromdate">
                                            <br />
                                            Assign Charges to User:
                                            <asp:Label ID="lbUserName" runat="server" Text=""></asp:Label>
                                            <br />
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:HiddenField ID="hdnUniqueID" runat="server" />
                                            <asp:HiddenField ID="hdnComplaintID" runat="server" />
                                            <asp:HiddenField ID="hdnChargeRefNo" runat="server" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="upSearch" runat="server">
                                                <ContentTemplate>
                                                    <table border="0" style="width: 955px">
                                                        <tr>
                                                            <td>
                                                                <td>
                                                                    <b>CHARGES: </b>
                                                                    <asp:Label ID="lblMandatoryCharge" runat="server" Font-Bold="true" ForeColor="Red"
                                                                        Text="*" Visible="true"></asp:Label>
                                                                    <br />
                                                                    <table border="0" style="width: 955px">
                                                                        <tr>
                                                                            <td align="center" valign="top">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td align="left">
                                                                                            <asp:TextBox ID="txtCharge" runat="server" AutoCompleteType="Disabled" CssClass="inputField1Style"
                                                                                                Height="16px" Width="240px"></asp:TextBox>
                                                                                        </td>
                                                                                        <td>
                                                                                            <asp:Image ID="imgIntelAssComp" runat="server" ImageUrl="~/includes/img/ds249_i_mini_btn.png" />
                                                                                            <asp:ImageButton ID="ibSave" runat="server" ImageUrl="~/includes/img/btn_save.gif"
                                                                                                OnClick="ibSave_Click" CausesValidation="false" />
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <asp:Label ID="lblCharge" runat="server" class="labelStyle" Visible="false"></asp:Label>
                                                                    <cc1:TextBoxWatermarkExtender ID="txtCharge_TextBoxWatermarkExtender" runat="server"
                                                                        Enabled="True" TargetControlID="txtCharge" WatermarkCssClass="WaterMark" WatermarkText="Enter Charge Code Eg. 1.4">
                                                                    </cc1:TextBoxWatermarkExtender>
                                                                    <cc1:AutoCompleteExtender ID="txtCharge_AutoCompleteExtender" runat="server" CompletionInterval="1000"
                                                                        BehaviorID="txtCharge" DelimiterCharacters="" MinimumPrefixLength="2" Enabled="True"
                                                                        CompletionListCssClass="CompletionList" CompletionListHighlightedItemCssClass="HighlightedItem"
                                                                        CompletionListItemCssClass="ListItem" CompletionSetCount="12" EnableCaching="true"
                                                                        OnClientHiding="vcOnhiding" OnClientItemSelected="PerformPostBack" OnClientPopulated="vcShownev"
                                                                        OnClientPopulating="vcPopulatingev" ServiceMethod="GetCharges" ServicePath=""
                                                                        TargetControlID="txtCharge" UseContextKey="True">
                                                                    </cc1:AutoCompleteExtender>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCharge"
                                                                        ErrorMessage="Enter Charges">!</asp:RequiredFieldValidator>
                                                                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="Small" Text=""></asp:Label>
                                                                     <asp:Button ID="Button1" runat="server" CausesValidation="False" OnClick="Button1_Click"
                                                                        Text="Button" />
                                                                    <asp:Label ID="lbvcSearchMsg" runat="server" Font-Bold="True" Font-Size="Small" Text=""></asp:Label>
                                                                </td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                    </tr>
                                </table>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:GridView ID="gvCharges" runat="server" AutoGenerateColumns="False" BorderColor="#235705"
                                                HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a" EmptyDataText="Sorry. No Charges to Display."
                                                OnRowCommand="gvCharges_RowCommand" OnRowDeleting="gvCharges_RowDeleting">
                                                <EmptyDataRowStyle CssClass="fromdate" />
                                                <Columns>
                                                    <asp:BoundField DataField="ReferenceNumber" HeaderText="ReferenceNo">
                                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                                            Font-Bold="true" HorizontalAlign="Left" VerticalAlign="Middle" BorderWidth="2px"
                                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ChargeDescription" HeaderText="Charge">
                                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                                            Font-Bold="true" HorizontalAlign="Left" VerticalAlign="Middle" BorderWidth="2px"
                                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="Delete" CommandArgument='<%#Eval("ViewChargeId") %>'
                                                                CausesValidation="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ViewChargeId" HeaderText="ViewChargeId" Visible="false">
                                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.2em"
                                                            Font-Bold="true" HorizontalAlign="Left" VerticalAlign="Middle" BorderWidth="2px"
                                                            ForeColor="Black" Font-Underline="False" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:BoundField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="ibCancel" runat="server" ImageUrl="~/includes/img/btn_cancel.gif"
                                                OnClick="ibCancel_Click" CausesValidation="false" />
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
