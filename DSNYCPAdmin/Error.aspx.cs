﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DSNY.DSNYCP.Administrator
{
    public partial class Error : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["errorID"] != null)
            {
                btnErrorID.Text = "Error ID : " + Session["errorID"].ToString();
            }
        }
      

        protected void btnErrorID_Click(object sender, EventArgs e)
        {          

            Response.Redirect("Dashboard.aspx");
        }
    }
}