﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DSNY.DSNYCP.IInterfaces;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.Services;



namespace DSNYBroker
{
    /// <summary>
    /// Summary description for DS249
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.None)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class DS249 : System.Web.Services.WebService,IService

    {       

        #region IService Members
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.ComplaintDTO> LoadComplaint(long Id)
        {
            Service loadComplaint = new Service();
            List<ComplaintDTO> complaintDTO; 
            complaintDTO = loadComplaint.LoadComplaint(Id);
            return complaintDTO;
        }
        [WebMethod]
        public List<ComplaintDTO> SearchComplaintId(string SearchText)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SearchComplaintId(SearchText);
            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.ComplaintHistoryDTO> LoadComplaintHistory(long Id, long ComplaintID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadComplaintHistory(Id, ComplaintID);
            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.ComplaintDTO> LoadComplaintList(short PersonalID, int pageNo, int maximumRows, ref Int64 rowsCount)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadComplaintList(PersonalID, pageNo, maximumRows, ref rowsCount); 

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.PersonnelByImageDTO> LoadEmployeeImage(int Id, byte[] EmployeeImage)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadEmployeeImage(Id, EmployeeImage); 

            
        }
        [WebMethod(MessageName = "LoadPersonnelLong")]
        public List<DSNY.DSNYCP.DTO.PersonnelByIdDTO> LoadPersonnel(long Id)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadPersonnel(Id); 
            
            
        }
        [WebMethod(MessageName = "LoadPersonnelLongString")]
        public List<DSNY.DSNYCP.DTO.PersonnelByRefNoDTO> LoadPersonnel(string RefNo)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadPersonnel(RefNo);

            
        }
        [WebMethod(MessageName = "LoadPersonnelListString")]
        public List<DSNY.DSNYCP.DTO.PersonnelListDTO> LoadPersonnelList(string SearchString)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadPersonnelList(SearchString); 

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.EmployeeInfoDTO> LoadEmployeeInfo(long ID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadEmployeeInfo(ID);

            
        }
        [WebMethod(MessageName = "LoadPersonnelList")]
        public List<DSNY.DSNYCP.DTO.PersonnelDTO> LoadPersonnelList()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadPersonnelList();

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.EmployeeListDTO> LoadEmployeeList()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadEmployeeList(); 
        }

        [WebMethod(MessageName = "LoadChargesString")]
        public List<DSNY.DSNYCP.DTO.ChargeDTO> LoadCharges(string ChargeCode)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadCharges(ChargeCode);

            
        }
        [WebMethod(MessageName = "LoadCharges")]
        public List<DSNY.DSNYCP.DTO.ChargesWithoutParametersDTO> LoadCharges()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadCharges();

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.CodeDTO> LoadCodeList(short CategoryID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadCodeList(CategoryID);
            
        }

        [WebMethod]
        public List<DSNY.DSNYCP.DTO.BCADStatusDTO> LoadBCADCaseStatus()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadBCADCaseStatus(); 
        }

        [WebMethod]
        public List<DSNY.DSNYCP.DTO.LocationDTO> LoadLocations()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadLocations();

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.BoroughDTO> LoadBoroughs()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadBoroughs();

            
        }
        [WebMethod(MessageName = "LoadTitles")]
        public List<DSNY.DSNYCP.DTO.TitleDTO> LoadTitles()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadTitles();

            
        }
        [WebMethod(MessageName = "LoadTitlesString")]
        public List<DSNY.DSNYCP.DTO.TitleListDTO> LoadTitles(string SearchText)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadTitles(SearchText);

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.WitnessDto> LoadWitnesses(long ComplaintID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadWitnesses(ComplaintID);

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.ComplainantDTO> LoadComplainants(long ComplaintID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadComplainants(ComplaintID);
        }

        [WebMethod]
        public List<DSNY.DSNYCP.DTO.ViolationDTO> LoadViolations(long ComplaintID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadViolations(ComplaintID); 

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.IncedentDTO> LoadIncidents(long ComplaintID, long ChargeID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadIncidents(ComplaintID, ChargeID);

            
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.ComplaintDTO> LoadBCADComplaints(int pageNo, int maximumRows)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadBCADComplaints(pageNo, maximumRows);              
        }

        [WebMethod]
        public List<DSNY.DSNYCP.DTO.ComplaintDTO> LoadCombinedComplaints(int pageNo, Int64 employeeId, int maximumRows, Int64 complaintId,Int64 indexNo,ref Int64 rowsCount)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadCombinedComplaints(pageNo, employeeId, maximumRows, complaintId,indexNo,ref rowsCount);             
        }

        [WebMethod]
        public List<DSNY.DSNYCP.DTO.BCADPenaltyDTO> LoadBCADPenalty(long BCADCaseID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadBCADPenalty(BCADCaseID); 

            
        }
        [WebMethod]
        public List<AdvocateAssociatedComplaintDTO> LoadAssociatedComplaints(long advocacyCaseID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadAssociatedComplaints(advocacyCaseID); 

            
        }
        [WebMethod]
        public List<AdvocateHearingDTO> LoadAdvocateHearingList(long advocacyCaseID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadAdvocateHearingList(advocacyCaseID);  

           
        }
        [WebMethod]
        public List<AdvocateAppealDTO> LoadAdvocateAppealList(long advocacyCaseID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadAdvocateAppealList(advocacyCaseID); 

           
        }
        [WebMethod]
        public List<AdvocatePenaltyDTO> LoadAdvocatePenaltyList(long advocacyCaseID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadAdvocatePenaltyList(advocacyCaseID); 

            
        }
        [WebMethod]
        public List<CombinedPenaltyDTO> LoadCombinedPenaltyList(long advocacyCaseID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadCombinedPenaltyList(advocacyCaseID);


        }
        [WebMethod]
        public bool SaveComplaint(DSNY.DSNYCP.DTO.ComplaintDTO complaint, Int64 personID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveComplaint(complaint, personID); 
            
        }
        [WebMethod]
        public bool ReopenORCloseApproveComplaint(DSNY.DSNYCP.DTO.ComplaintDTO complaint)
        {
            Service loadComplaint = new Service();
            return loadComplaint.ReopenORCloseApproveComplaint(complaint); 

            
        }
        [WebMethod]
        public bool DeleteUndeleteComplaint(long complaintID, short status)
        {
            Service loadComplaint = new Service();
            return loadComplaint.DeleteUndeleteComplaint(complaintID, status); 
        }

        [WebMethod]
        public bool SaveWitness(DSNY.DSNYCP.DTO.WitnessDto witness)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveWitness(witness);

            
        }
        [WebMethod]
        public bool SaveComplainant(DSNY.DSNYCP.DTO.ComplainantDTO complainant)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveComplainant(complainant);
            

            
        }
        [WebMethod]
        public bool SaveVoilation(DSNY.DSNYCP.DTO.ViolationDTO voilation)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveVoilation(voilation);
        }
        [WebMethod]
        public bool SaveIncident(DSNY.DSNYCP.DTO.IncedentDTO incident)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveIncident(incident);
        }
        [WebMethod]
        public bool UpdateComplaintStatus(long ComplaintID, short Status)
        {
            Service loadComplaint = new Service();
            return loadComplaint.UpdateComplaintStatus(ComplaintID, Status);
        }
        [WebMethod]
        public bool CreateNewBCAD(DSNY.DSNYCP.DTO.BCADComplaintDTO bcadComplaint)
        {
            Service loadComplaint = new Service();
            return loadComplaint.CreateNewBCAD(bcadComplaint);
        }
        [WebMethod]
        public bool OpenAdvocateComplaint(long AdvocacyCaseId, short receivedUser, DateTime receivedDate)
        {
            Service loadComplaint = new Service();
            return loadComplaint.OpenAdvocateComplaint(AdvocacyCaseId, receivedUser, receivedDate);
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.BCADComplaintDTO> GetBCADComplaint(long ComplaintID, DSNY.DSNYCP.DTO.BCADComplaintDTO bcadComplaint)
        {
            Service loadComplaint = new Service();
            return loadComplaint.GetBCADComplaint(ComplaintID, bcadComplaint);
        }
        [WebMethod]
        public List<ComplaintAdvocacyDTO> GetAdvocateComplaint(long ComplaintID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.GetAdvocateComplaint(ComplaintID);
        }
        [WebMethod]
        public bool SetBCADStatus(long complaintID, short statusCode)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SetBCADStatus(complaintID, statusCode);
        }
        [WebMethod]
        public bool SetAdvocateCaseStatus(long complaintID, short statusCode, long advocateCaseID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SetAdvocateCaseStatus( complaintID,  statusCode,  advocateCaseID);
        }
        [WebMethod]
        public bool ReturnBCADToAuthor(long ComplaintID, long PersonalID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.ReturnBCADToAuthor(ComplaintID, PersonalID);
        }
        [WebMethod]
        public bool ReturnAdvocateCaseToAuthor(long ComplaintID, long advocacyCaseId, long PersonalID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.ReturnAdvocateCaseToAuthor(ComplaintID, advocacyCaseId, PersonalID);
        }
        [WebMethod]
        public bool ReturnBCADToAdvocate(long ComplaintID, long PersonalID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.ReturnBCADToAdvocate(ComplaintID, PersonalID);
        }
        [WebMethod]
        public bool ReturnAdvocateToBCAD(long complaintID, long advocacyCaseID, long PersonalID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.ReturnAdvocateToBCAD(complaintID, advocacyCaseID, PersonalID);
        }
        [WebMethod]
        public bool SaveBCADComplaint(DSNY.DSNYCP.DTO.BCADComplaintDTO bcadComplaint)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveBCADComplaint(bcadComplaint);
        }
        [WebMethod]
        public bool SaveBCADPenaltyList(DSNY.DSNYCP.DTO.BCADPenaltyDTO bcadPenalty)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveBCADPenaltyList(bcadPenalty);
        }
        [WebMethod]
        public bool SaveAdvocateComplaint(DSNY.DSNYCP.DTO.ComplaintAdvocacyDTO advocate)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveAdvocateComplaint(advocate);
        }
        [WebMethod]
        public List<ComplaintByDistrictDTO> GetComplaintByDistrict()
        {
            Service loadComplaint = new Service();
            return loadComplaint.GetComplaintByDistrict();
        }
        [WebMethod]
        public bool AuthenticateUser(DSNY.DSNYCP.DTO.UserDTO user)
        {
            Service loadComplaint = new Service();
            return loadComplaint.AuthenticateUser(user);
        }
        [WebMethod]
        public DSNY.DSNYCP.DTO.UserDTO ValidateUserEmail(string emailID)
        {

            Service loadComplaint = new Service();
            return loadComplaint.ValidateUserEmail(emailID);
        }
        [WebMethod]
        public List<DSNY.DSNYCP.DTO.PersonnelDTO> LoadAllPersonnel(string searchString)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadAllPersonnel(searchString);
        }
        [WebMethod]
        public List<PayCodeDTO> LoadPayCodeList()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadPayCodeList();
        }
        [WebMethod]
        public string SaveUser(DSNY.DSNYCP.DTO.UserDTO user)
        //public string SaveUser(DSNY.DSNYCP.DTO.UserDTO user, Int64 userId)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveUser(user);
            //return loadComplaint.SaveUser(user,userId);
        }
        [WebMethod]
        public bool ResetPassword(DSNY.DSNYCP.DTO.UserDTO user)
        {
            Service loadComplaint = new Service();
            return loadComplaint.ResetPassword(user);
        }
        [WebMethod]
        public bool DeleteUser(long personID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.DeleteUser(personID);
        }
        [WebMethod]
        public string GetNewUserID(long personID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.GetNewUserID(personID);
        }

        #endregion


        #region IService Members

        [WebMethod]
        public List<MedicalStatusDTO> LoadMedicalCaseStatus()
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadMedicalCaseStatus();
        }

        [WebMethod]
        public List<ComplaintDTO> LoadMedicalComplaints(int pageNo, int maximumRows)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadMedicalComplaints(pageNo, maximumRows);
        }

        [WebMethod]
        public List<MedicalPenaltyDTO> LoadMedicalPenalty(long medicalCaseID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.LoadMedicalPenalty(medicalCaseID);
        }

        [WebMethod]
        public bool CreateNewMedical(MedicalComplaintDTO medicalComplaint)
        {
            Service loadComplaint = new Service();
            return loadComplaint.CreateNewMedical(medicalComplaint);
        }

        [WebMethod]
        public List<MedicalComplaintDTO> GetMedicalComplaint(long ComplaintID, MedicalComplaintDTO medicalComplaint)
        {
            Service loadComplaint = new Service();
            return loadComplaint.GetMedicalComplaint(ComplaintID, medicalComplaint);
        }


        [WebMethod]
        public bool SetMedicalStatus(long complaintID, short statusCode)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SetMedicalStatus(complaintID, statusCode);
        }

        [WebMethod]
        public bool ReturnMedicalToAuthor(long ComplaintID, long personalID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.ReturnMedicalToAuthor(ComplaintID, personalID);
        }

        [WebMethod]
        public bool ReturnMedicalToAdvocate(long ComplaintID, long PersonalID)
        {
            Service loadComplaint = new Service();
            return loadComplaint.ReturnMedicalToAdvocate(ComplaintID, PersonalID);
        }

        [WebMethod]
        public bool SaveMedicalComplaint(MedicalComplaintDTO medicalComplaint)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveMedicalComplaint(medicalComplaint);
        }

        [WebMethod]
        public bool SaveMedicalPenaltyList(MedicalPenaltyDTO medicalPenalty)
        {
            Service loadComplaint = new Service();
            return loadComplaint.SaveMedicalPenaltyList(medicalPenalty);
        }

        #endregion

        #region IService Members


        public List<HideChargesDto> GetHideCharges(short employeeId)
        {
            throw new NotImplementedException();
        }

        public bool SaveHideCharges(string referenceNo, long employeeId)
        {
            throw new NotImplementedException();
        }

        public bool DeleteHideCharges(short viewChargesId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
