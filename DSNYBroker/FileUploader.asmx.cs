﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using DSNY.DSNYCP.Services;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.DAL;



namespace DSNYBroker
{
    /// <summary>
    /// This services provide functionality for the upload files of the complaint attachments.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class FileUploader : System.Web.Services.WebService
    {
        


        public FileUploader()
        {


        }

        /// <summary>
        /// This function returns the attachments of the complaints based on its complaint id, file data and the application name.
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="files"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        [WebMethod]

        public bool UploadFiles(Int64 ParentID,Int64 LotId, Int64 InspectionRequestId,  byte[] files, String serverLocation, String fileLoc, String applicationName, String contentType, String FileName, String Child1,
            DateTime imageDate, String whenTaken, String takenBy, String note)
        {
           
                Service complaintAttachments = new Service();

                return complaintAttachments.UploadFiles(ParentID, LotId, InspectionRequestId, files,
                    serverLocation,fileLoc, applicationName, 0, FileName, 0, Child1, contentType, Child1,
                    imageDate, whenTaken, takenBy, note);
            
        }


       

        /// <summary>
        /// This function returns the attachments of the complaints based on its complaint id, file data and the application name.
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="files"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        //do
        [WebMethod]
        public bool UploadFiles(long ParentID, byte[] files, String applicationName, String contentType, String FileName, String Child1)
        {            
                Service complaintAttachments = new Service();
                return complaintAttachments.UploadFiles(ParentID, files, applicationName, 0, FileName, 0, Child1, contentType);           
        }

         [WebMethod]
        public bool UploadFiles(long ParentID, byte[] files, String contentType, String FileName, String Child1)
        {           
                Service complaintAttachments = new Service();
                return complaintAttachments.UploadFiles(ParentID, files, 0, FileName, 0, Child1, contentType);           
        }


        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID, string ChildID, string AppName)
        {
                UpLoadFileDTO upload = new UpLoadFileDTO();           
                Service complaintAttachments = new Service();
                List<UpLoadFileDTO> listUpLoadFile = complaintAttachments.LoadComplaintAttachments(DocumentID, ParentID, ChildID, AppName);
                return listUpLoadFile;           
        }

        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID, Int64 LotId, Int64 InspectionRequestId, string ChildID, String AppName)
        {
            UpLoadFileDTO upload = new UpLoadFileDTO();         
                Service complaintAttachments = new Service();
                            List<UpLoadFileDTO> listUpLoadFile = complaintAttachments.LoadComplaintAttachments(DocumentID, ParentID, LotId, InspectionRequestId, ChildID, AppName);
                return listUpLoadFile;
          
        }

        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID, string ChildID)
        {
            UpLoadFileDTO upload = new UpLoadFileDTO();           
                Service complaintAttachments = new Service();
                List<UpLoadFileDTO> listUpLoadFile = complaintAttachments.LoadComplaintAttachments(DocumentID, ParentID, ChildID);
                return listUpLoadFile;           
        }
        public bool DeleteSupportedDocument(Int64 ParentID)
        {
            Service complaintAttachments = new Service();
            return complaintAttachments.DeleteSupportedDocument(ParentID);
        }

       

    }
}