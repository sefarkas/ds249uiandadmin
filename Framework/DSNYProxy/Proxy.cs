﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Services;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.IInterfaces;
using System.Net;
using System.Security.Principal;
using System.IO;
using System.Configuration;


namespace DSNY.DSNYCP.Proxys
{

    /// <summary>
    /// This class provides the functionality for the set of methods and functions used in complaints and admin module
    /// </summary>
    

    public class Proxy : IService   
    {   
       
        Service service;
        DSNY.DSNYCP.Proxys.SecuritySvc.Security securitySvc;
        

        public Proxy()
        {
            service = new Service();
        }
        
        public Proxy(Boolean WebRequest)
        {
            securitySvc = new DSNY.DSNYCP.Proxys.SecuritySvc.Security();
            String url = Convert.ToString(ConfigurationManager.AppSettings["SecuritySvc"]);
            if(url != string.Empty)
                securitySvc.Url = url;
        }
        /// <summary>
        /// This function returns the list of complaint attachments
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="files"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        //DO
        public bool UploadFiles(long ParentID, byte[] files, String applicationName, String contentType, String FileName, String Child1)
        {
            DSNYBroker.FileUploader fileUploader = new DSNYBroker.FileUploader();

            return fileUploader.UploadFiles(ParentID, files, applicationName, contentType, FileName, Child1);
        }


        public bool UploadFiles(long ParentID,Int64 LotId, Int64 InspectionRequestId, byte[] files, String ServerLocation,  String fileLoc, String applicationName, String contentType, String FileName, String Child1,
            DateTime imageDate, String whenTaken, String takenBy, String note)
        {
            DSNYBroker.FileUploader fileUploader = new DSNYBroker.FileUploader();
            return fileUploader.UploadFiles(ParentID,LotId, InspectionRequestId, files, ServerLocation, fileLoc, applicationName, contentType, FileName, Child1,
                imageDate, whenTaken, takenBy, note);
        }


        public bool UploadFiles(long ParentID, byte[] files, String contentType, String FileName, String Child1)
        {
            DSNYBroker.FileUploader fileUploader = new DSNYBroker.FileUploader();

            return fileUploader.UploadFiles(ParentID, files, contentType, FileName, Child1);
        }

        /// <summary>
        /// This function returns the list of complaint attachments
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="files"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>




        public bool SaveCombinePenalties(long ParentID, Int16 ChildCombPenID, Int16 CombPenID)
        {


            return service.SaveCombinePenalties(ParentID, ChildCombPenID, CombPenID);
        }

        /// <summary>
        /// This function returns the list of complaint attachments
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="files"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>

        public bool DeleteCombinePenalties(long ParentID)
        {


            return service.DeleteCombinePenalties(ParentID);
        }
        /// <summary>
        /// This method returns the list of complaints w.r.t ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool DeleteSupportedDocument(Int64 ParentID)
        {
            DSNYBroker.FileUploader fileUploader = new DSNYBroker.FileUploader();
            return fileUploader.DeleteSupportedDocument(ParentID);
        }
        

        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID, string  ChildID, string AppName)
        {           
                DSNYBroker.FileUploader fileUploader = new DSNYBroker.FileUploader();
                return fileUploader.LoadComplaintAttachments(DocumentID, ParentID, ChildID, AppName);                          
        }

        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID, Int64 LotId, Int64 InspectionRequestId, 
            string ChildID, string AppName)
        {           
                DSNYBroker.FileUploader fileUploader = new DSNYBroker.FileUploader();
                return fileUploader.LoadComplaintAttachments(DocumentID, ParentID,LotId, InspectionRequestId, ChildID, AppName);
           
        }

       

        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID, string ChildID)
        {            
                DSNYBroker.FileUploader fileUploader = new DSNYBroker.FileUploader();
                return fileUploader.LoadComplaintAttachments(DocumentID, ParentID, ChildID);
            
        }

        public int SaveErrorLog(string Url, string RequestType, string UserID, string ErrorMessage, string StackTrace, int? AppID)
        {
           
                return service.SaveErrorLog(Url, RequestType, UserID, ErrorMessage, StackTrace, AppID);
            
        }

        public List<ComplaintDTO> LoadComplaint(Int64 Id)
        {            
                return service.LoadComplaint(Id);
            
        }

        /// <summary>
        /// This method returns the list of complaints w.r.t the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <returns></returns>
        public List<ComplaintDTO> SearchComplaintId(String SearchText)
        {           
                return service.SearchComplaintId(SearchText);            
        }
        /// <summary>
        /// This method loads the lsit of complaints history w.r.t ID and complaint ID
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<ComplaintHistoryDTO> LoadComplaintHistory(Int64 Id, Int64 ComplaintID)
        {            
                return service.LoadComplaintHistory(Id, ComplaintID);            
        }

        /// <summary>
        /// This method loads the list of complaints w.r.t personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<ComplaintDTO> LoadComplaintList(Int16 PersonalID, int pageNo, int maximumRows,ref Int64 rowsCount)
        {            
                return service.LoadComplaintList(PersonalID, pageNo, maximumRows, ref rowsCount);           
        }
                     

        /// <summary>
        /// This method loads the list of complaints w.r.t personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<ComplaintDTO> LoadSearchComplaintList(int createdBy,int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue,ref int rowCount)
        {           
                return service.LoadSearchComplaintList(createdBy, pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue,ref rowCount);            
        }

        /// <summary>
        /// This method loads the list of complaints w.r.t personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<ComplaintDTO> LoadAdvocateSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue,ref Int32 rowCount)
        {           
                return service.LoadAdvocateSearchComplaintList(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue,ref rowCount);           
        }

        /// <summary>
        /// This method loads the list of complaints w.r.t personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<ComplaintDTO> LoadCadSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue)
        {         
                return service.LoadBCADSearchComplaintList(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue);
            
        }

        /// <summary>
        /// This method loads the list of complaints w.r.t personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<ComplaintDTO> LoadMedicalSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue)
        {           
                return service.LoadMedicalSearchComplaintList(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue);
            
        }

        /// <summary>
        /// This method loads the list of complaints w.r.t personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public int LoadComplaintListCount(Int16 PersonalID)
        {
           
                return service.LoadComplaintListCount(PersonalID);               
           
        }


        /// <summary>
        /// This method loads the list of personnel info w.r.t ID 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<PersonnelByIdDTO> LoadPersonnel(Int64 ID)
        {
           
                return service.LoadPersonnel(ID);            
        }
        /// <summary>
        /// This method loads the list of personnel info w.r.t refference number
        /// </summary>
        /// <param name="RefNumber"></param>
        /// <returns></returns>
        public List<PersonnelByRefNoDTO> LoadPersonnel(String RefNumber)
        {           
                return service.LoadPersonnel(RefNumber);            
        }
        /// <summary>
        /// This method loads the list of employee images info w.r.t employee reference number
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="EmployeeImage"></param>
        /// <returns></returns>
        public List<PersonnelByImageDTO> LoadEmployeeImage(Int32 Id, byte[] EmployeeImage)
        {            
                return service.LoadEmployeeImage(Id, EmployeeImage);            
        }

        
        /// <summary>
        ////This will Load Personnel List which statisfy the Search Criteria.
        /// </summary>
        /// <param name="SearchString"></param>
        /// <returns></returns>
        public List<PersonnelListDTO> LoadPersonnelList(String SearchString)
        {
            return service.LoadPersonnelList(SearchString);
        }

        /// <summary>
        /// This method will load Partial personnel List.
        /// </summary>
        /// <returns></returns>
        public List<PersonnelDTO> LoadPersonnelList()
        {           
                return service.LoadPersonnelList();            
        }
        /// <summary>
        /// This method returns the list of employee info w.r.t ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<EmployeeInfoDTO> LoadEmployeeInfo(Int64 ID)
        {           
                return service.LoadEmployeeInfo(ID);            
        }
        /// <summary>
        /// This method returns the list of employee info
        /// </summary>
        /// <returns></returns>
        public List<EmployeeListDTO> LoadEmployeeList()
        {
            return service.LoadEmployeeList();            
        }

       /// <summary>
        ///  //This will load complete personnel list for ADMIN
       /// </summary>
       /// <param name="SearchString"></param>
       /// <returns></returns>
        public List<PersonnelDTO> LoadAllPersonnel(String SearchString)
        {
            return service.LoadAllPersonnel(SearchString);
        }
        /// <summary>
        ///  This will load the pay code list
        /// </summary>
        /// <returns></returns>
        public List<PayCodeDTO> LoadPayCodeList()
        {            
                return service.LoadPayCodeList();            
        }
        /// <summary>
        /// This method will load the charges w.r.t charge code
        /// </summary>
        /// <param name="ChargeCode"></param>
        /// <returns></returns>
        public List<ChargeDTO> LoadCharges(String ChargeCode)
        {           
                return service.LoadCharges(ChargeCode);            
        }
        /// <summary>
        /// This method will load the charges without parameters
        /// </summary>
        /// <returns></returns>
        public List<ChargesWithoutParametersDTO> LoadCharges()
        {           
                return service.LoadCharges();            
        }
        /// <summary>
        /// This method will load the code list w.r.t category
        /// </summary>
        /// <param name="CategoryID"></param>
        /// <returns></returns>
        public List<CodeDTO> LoadCodeList(Int16 CategoryID)
        {           
                return service.LoadCodeList(CategoryID);            
        }
        /// <summary>
        /// This method will returns the list of BCAD case status
        /// </summary>
        /// <returns></returns>
        public List<BCADStatusDTO> LoadBcadCaseStatus()
        {           
                return service.LoadBCADCaseStatus();            
        }
        /// <summary>
        /// This method will returns the list of locations
        /// </summary>
        /// <returns></returns>
        public List<MedicalStatusDTO> LoadMedicalCaseStatus()
        {            
                return service.LoadMedicalCaseStatus();           
        }



        public List<LocationDTO> LoadLocations()
        {           
                return service.LoadLocations();            
        }
        /// <summary>
        /// This method will returns the list of boroughs
        /// </summary>
        /// <returns></returns>
        public List<BoroughDTO> LoadBoroughs()
        {           
                return service.LoadBoroughs();           

        }
        /// <summary>
        /// This method will returns the list of titles
        /// </summary>
        /// <returns></returns>
        public List<TitleDTO> LoadTitles()
        {           
                return service.LoadTitles();           
        }
        /// <summary>
        /// This method will returns the list titles with respect to search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <returns></returns>
        public List<TitleListDTO> LoadTitles(String SearchText)
        {            
                return service.LoadTitles(SearchText);            
        }

        /// <summary>
        /// This method will returns the list of witnesses w.r.t complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<WitnessDto> LoadWitnesses(Int64 ComplaintID)
        {           
                return service.LoadWitnesses(ComplaintID);            
        }
        /// <summary>
        /// This method will returns the list of complaints w.r.t complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<ComplainantDTO> LoadComplainants(Int64 ComplaintID)
        {           
                return service.LoadComplainants(ComplaintID);            
        }
        /// <summary>
        /// This method will returns the list of violations w.r.t complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<ViolationDTO> LoadViolations(Int64 ComplaintID)
        {           
                return service.LoadViolations(ComplaintID);            
        }
        /// <summary>
        /// This method will returns the list of incedents w.r.t complaint ID and charge ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="ChargeID"></param>
        /// <returns></returns>
        public List<IncedentDTO> LoadIncidents(Int64 ComplaintID, Int64 ChargeID)
        {          
                return service.LoadIncidents(ComplaintID, ChargeID);           
        }
        /// <summary>
        /// This method will returns the list of BCAD complaints
        /// </summary>
        /// <returns></returns>
        public List<ComplaintDTO> LoadBCADComplaints(int pageNo, int maximumRows)
        {           
                return service.LoadBCADComplaints(pageNo, maximumRows);            
        }
        /// <summary>
        /// This method will returns the list of advocate complaints
        /// </summary>
        /// <returns></returns>
        public List<ComplaintDTO> LoadMedicalComplaints(int pageNo, int maximumRows)
        {            
                return service.LoadMedicalComplaints(pageNo, maximumRows);           
        }


        public List<ComplaintDTO> LoadAdvocateComplaints(int pageNo, int maximumRows, ref Int64 rowsCount)
        {           
                return service.LoadAdvocateComplaints(pageNo, maximumRows,ref rowsCount);           
        }


        public List<ComplaintDTO> LoadCombinedComplaints(int pageNo, Int64 employeeID, int maximumRows, Int64 complaintId,Int64 indexNo,ref Int64 rowCount)
        {           
                return service.LoadCombinedComplaints(pageNo, employeeID, maximumRows, complaintId,indexNo,ref rowCount);            
        }


        public List<ComplaintDTO> CheckCombinedComplaints(Int64 complaintId)
        {          
                return service.CheckCombinedComplaints(complaintId);          
        }


        public int LoadAdvocateListCount(Int16 personId)
        {            
                return service.LoadAdvocateListCount(personId);            
        }

        public int LoadCombineListCount(Int64 employeeID, Int64 complaintId, Int64 indexNo)
        {           
                return service.LoadCombineListCount(employeeID, complaintId, indexNo);            
        }
        

        public int LoadMedicalListCount(Int16 PersonalID)
        {           
                return service.LoadMedicalListCount(PersonalID);           
        }

        public int LoadBCADListCount(Int16 PersonalID)
        {           
                return service.LoadBCADListCount(PersonalID);            
        }

        /// <summary>
        /// This method loads the list of complaints w.r.t personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public int LoadSearchComplaintListCount(Int16 PersonalID, int numericValue, string decimalValue, string dateValue, string stringValue)
        {            
                return service.LoadSearchComplaintListCount(PersonalID, numericValue, decimalValue, dateValue, stringValue);           
        }


        public int LoadSearchAdvocateListCount(Int16 PersonalID, int numericValue, string decimalValue, string dateValue, string stringValue)
        {           
              return service.LoadSearchAdvocateListCount(PersonalID, numericValue, decimalValue, dateValue, stringValue);
           
        }

        public int LoadSearchBCADListCount(Int16 PersonalID, int numericValue, string decimalValue, string dateValue, string stringValue)
        {
           return service.LoadSearchBCADListCount(PersonalID, numericValue, decimalValue, dateValue, stringValue);           
        }

        public int LoadSearchMedicalListCount(Int16 PersonalID, int numericValue, string decimalValue, string dateValue, string stringValue)
        {           
            return service.LoadSearchMedicalListCount(PersonalID, numericValue, decimalValue, dateValue, stringValue);          
        }
               
        /// <summary>
        /// This function returns true if the complaints are saved sucessfully  w.r.t complaint ID
      /// </summary>
      /// <param name="complaint"></param>
      /// <returns></returns>

        public bool SaveComplaint(ComplaintDTO complaint, Int64 personID)
        {
             return service.SaveComplaint(complaint, personID);           
        }
        /// <summary>
        /// This method re-opens or closes the approved complaints  w.r.t complaint ID
        /// </summary>
        /// <param name="complaint"></param>
        /// <returns></returns>
        public bool ReopenORCloseApproveComplaint(ComplaintDTO complaint)
        {
            return service.ReopenORCloseApproveComplaint(complaint);
        }
        /// <summary>
        /// This method returs true if the undeleted complaint is sucessfully deleted.
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool DeleteUndeleteComplaint(Int64 complaintID, Int16 status)
        {
            return service.DeleteUndeleteComplaint(complaintID, status);
        }

        public bool ComplaintHistoryDetails(Int64 complaintID, String status, Int64 UserId, DateTime date)
        {
            return service.ComplaintHistoryDetails(complaintID, status, UserId, date);
        }

        public bool AdminPersonDetails(Int64 UserId, String Status, Int64 personId)
        {
            return service.AdminPersonDetails(UserId, Status, personId);
        }
        /// <summary>
        /// This method retuens true if the witnesses are saved sucessfully
        /// </summary>
        /// <param name="witness"></param>
        /// <returns></returns>
        public bool SaveWitness(WitnessDto witness)
        {            
                return service.SaveWitness(witness);            
        }
        /// <summary>
        /// This method retuens true if the complaints are saved sucessfully
        /// </summary>
        /// <param name="complainant"></param>
        /// <returns></returns>
        public bool SaveComplainant(ComplainantDTO complainant)
        {           
                return service.SaveComplainant(complainant);            
        }
        /// <summary>
        /// This method retuens true if the vilolations are saved sucessfully
        /// </summary>
        /// <param name="voilation"></param>
        /// <returns></returns>
        public bool SaveVoilation(ViolationDTO voilation)
        {           
                return service.SaveVoilation(voilation);
         }
        /// <summary>
        /// This method retuens true if the incdents are saved sucessfully
        /// </summary>
        /// <param name="incident"></param>
        /// <returns></returns>
        public bool SaveIncident(IncedentDTO incident)
        {           
                return service.SaveIncident(incident);           
        }
        /// <summary>
        /// This method retuens true if the complaint status are updated sucessfully
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public bool UpdateComplaintStatus(Int64 ComplaintID, Int16 Status)
        {            
                return service.UpdateComplaintStatus(ComplaintID, Status);           
        }

        public bool UpdateCombinedComplaints(String indexno, Int64 parentIndexNo,String uncmbIndexNo,Int64 uncmbParentIndexNo)
        {        
           
            return service.UpdateCombinedComplaints(indexno, parentIndexNo,uncmbIndexNo,uncmbParentIndexNo);
           
        }



        public Int64 CheckIsParent(Int64 IndexNo, ref Int64 IsChild)
        {            
                return service.CheckIsParent(IndexNo, ref IsChild);           
        }

         /// <summary>
        /// This method retuns the list of complaints by district
        /// </summary>
        /// <returns></returns>
        public List<ComplaintByDistrictDTO> GetComplaintByDistrict()
        {           
                return service.GetComplaintByDistrict();           
        }
        /// <summary>
        /// This method retuens true if the BCAD complaints are created sucessfully
        /// </summary>
        /// <param name="bcadComplaint"></param>
        /// <returns></returns>
        public bool CreateBCADComplaint(BCADComplaintDTO bcadComplaint)
        {           
                return service.CreateNewBCAD(bcadComplaint);           
        }
        /// <summary>
        /// This method retuens true if the advocate complaints are opened sucessfully
        /// </summary>
        /// <param name="AdvocacyCaseId"></param>
        /// <param name="receivedUser"></param>
        /// <param name="receivedDate"></param>
        /// <returns></returns>
        public bool CreateMedicalComplaint(MedicalComplaintDTO medicalComplaint)
        {           
                return service.CreateNewMedical(medicalComplaint);           
        }

        public bool OpenAdvocateComplaint(Int64 AdvocacyCaseId, Int16 receivedUser, DateTime receivedDate)
        {           
                return service.OpenAdvocateComplaint(AdvocacyCaseId, receivedUser, receivedDate);           
        }
        /// <summary>
        /// This method returns the list of BCAD complaints with respect to ComplaintID and bcadComplaint
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="bcadComplaint"></param>
        /// <returns></returns>
        public List<BCADComplaintDTO> GetBCADComplaint(Int64 ComplaintID, BCADComplaintDTO bcadComplaint)
        {            
                return service.GetBCADComplaint(ComplaintID, bcadComplaint);           
        }
        /// <summary>
        /// This method returns the list of advocate complaints with respect to ComplaintID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<MedicalComplaintDTO> GetMedicalComplaint(Int64 ComplaintID, MedicalComplaintDTO bcadComplaint)
        {           
                return service.GetMedicalComplaint(ComplaintID, bcadComplaint);           
        }

        
        public List<ComplaintAdvocacyDTO> GetAdvocateComplaint(Int64 ComplaintID)
        {           
                return service.GetAdvocateComplaint(ComplaintID);           
        }
        /// <summary>
        /// This method returns true if the BCAD complaints are set sucessfully
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public bool SetBCADStatus(long complaintID, short statusCode)
        {           
                return service.SetBCADStatus(complaintID, statusCode);            
        }
        /// <summary>
        /// This method returns true if the BCAD complaints status are set sucessfully
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="statusCode"></param>
        /// <param name="advocateCaseID"></param>
        /// <returns></returns>
        public bool SetMedicalStatus(long complaintID, short statusCode)
        {
           return service.SetMedicalStatus(complaintID, statusCode);
          
        }
        
        public bool SetAdvocateCaseStatus(Int64 complaintID, Int16 statusCode, Int64 advocateCaseID)
        {           
                return service.SetAdvocateCaseStatus(complaintID, statusCode, advocateCaseID);           
        }
        /// <summary>
        /// This method returns true if BCAD to Authour are routed  sucessfully
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool ReturnBCADToAuthor(Int64 ComplaintID, Int64 PersonalID)
        {           
                return service.ReturnBCADToAuthor(ComplaintID, PersonalID);           
        }
        /// <summary>
        /// This method returns true if the BCAD complaints are routed to advocate's sucessfully
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool ReturnBCADToAdvocate(Int64 ComplaintID, Int64 PersonalID)
        {           
                return service.ReturnBCADToAdvocate(ComplaintID, PersonalID);            
        }
        /// <summary>
        /// This method returns true if the advocate's to BCAD complaints are set sucessfully
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public bool ReturnMedicalToAdvocate(Int64 ComplaintID,Int64 PersonalID)
        {           
                return service.ReturnMedicalToAdvocate(ComplaintID, PersonalID);           
        }
        /// <summary>
        /// This method returns true if advocates to authour are routed  sucessfully
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="advocacyCaseId"></param>
        /// <returns></returns>

        public bool ReturnAdvocateCaseToAuthor(Int64 complaintID, Int64 advocacyCaseId, Int64 PersonalID)
        {
            return service.ReturnAdvocateCaseToAuthor(complaintID, advocacyCaseId, PersonalID);
        }
        public bool ReturnAdvocateToBCAD(Int64 complaintID, Int64 advocacyCaseID,Int64 PersonalID)
        {
            return service.ReturnAdvocateToBCAD(complaintID, advocacyCaseID, PersonalID);
        }
        

        public bool ReturnMedicalToAuthor(Int64 ComplaintID, Int64 PersonalID)
        {          
                return service.ReturnMedicalToAuthor(ComplaintID,PersonalID);           
        }

               
        /// <summary>
        /// This method returns true if BCAD complaints are saved sucessfully
        /// </summary>
        /// <param name="bcadComplaint"></param>
        /// <returns></returns>
        public bool SaveBCADComplaint(BCADComplaintDTO bcadComplaint)
        {          
                return service.SaveBCADComplaint(bcadComplaint);
         }

        public bool SaveMedicalComplaint(MedicalComplaintDTO medicalComplaint)
        {           
                return service.SaveMedicalComplaint(medicalComplaint);          
        }

        /// <summary>
        /// This method returns the list of BCAD penalty w.r.t BCAD case ID
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <returns></returns>

        public List<BCADPenaltyDTO> LoadBCADPenalty(Int64 BCADCaseID)
        {            
                return service.LoadBCADPenalty(BCADCaseID);           
        }


        public List<MedicalPenaltyDTO> LoadMedicalPenalty(Int64 MedicalCaseID)
        {           
                return service.LoadMedicalPenalty(MedicalCaseID);           
        }

        /// <summary>
        /// This method returns true if BCAD penalies are saved sucessfully
        /// </summary>
        /// <param name="bcadPenalty"></param>
        /// <returns></returns>


        public bool SaveBCADPenalty(BCADPenaltyDTO bcadPenalty)
        {           
                return service.SaveBCADPenaltyList(bcadPenalty);           
        }
        /// <summary>
        /// This method returns true if advocate complaints are saved sucessfully
        /// </summary>
        /// <param name="advocate"></param>
        /// <returns></returns>
        public bool SaveMedicalPenalty(MedicalPenaltyDTO medicalPenalty)
        {            
                return service.SaveMedicalPenaltyList(medicalPenalty);          
        }

        public bool SaveAdvocateComplaint(ComplaintAdvocacyDTO advocate)
        {            
                return service.SaveAdvocateComplaint(advocate);           
        }

        /// <summary>
        /// This methods returns the list of Authenticate User w.r.t user id and password
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public List<UserDTO> AuthenticateUser(String UserID, String Password)
        {
            List<UserDTO> objUsrDTO = new List<UserDTO>();
        // by Farkas 27Feb2020 not used -->    List<DSNY.DSNYCP.Proxys.SecuritySvc.UserDTO> myList = new List<DSNY.DSNYCP.Proxys.SecuritySvc.UserDTO>();

            DSNY.DSNYCP.Proxys.SecuritySvc.UserDTO[] userList = securitySvc.AuthenticateUser(UserID, Password);


            foreach (DSNY.DSNYCP.Proxys.SecuritySvc.UserDTO users in userList)
            {
                UserDTO usersData = new UserDTO  // simplified declaration by Farkas 27Feb2020
                {
                    PersonalID = users.PersonalID,
                    IsTempPassword = users.IsTempPassword,
                    IsAdmin = users.IsAdmin,

                    UserID = users.UserID,
                    EmailID = users.EmailID
                };

                objUsrDTO.Add(usersData);
            }

          

                 return objUsrDTO;
            
        }

        /// <summary>
        /// Loads the Current selected User from the database
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<UserDTO> LoadUser(Int64 PersonalID)
        {           
                List<UserDTO> objUserDTO = new List<UserDTO>();               
                DSNY.DSNYCP.Proxys.SecuritySvc.UserDTO[] userList = securitySvc.LoadUser(PersonalID);

                foreach (DSNY.DSNYCP.Proxys.SecuritySvc.UserDTO user in userList)
                {
                UserDTO roleData = new UserDTO   // simplified declaration by Farkas 27Feb2020
                {
                    PersonalID = user.PersonalID,
                    UserID = user.UserID,
                    IsTempPassword = user.IsTempPassword,
                    IsAdmin = user.IsAdmin,
                    LastLogin = user.LastLogin,
                    IsNew = user.IsNew,
                    Password = user.Password,
                    EmailID = user.EmailID
                };

                objUserDTO.Add(roleData);
                }

                return objUserDTO;               
           
        }

        /// <summary>
        /// This method returns the list of members with respect to the personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<MembershipDTO> GetMembership(Int64 PersonalID)
        {
            List<MembershipDTO> objMemberDTO = new List<MembershipDTO>();
            // by Farkas 27Feb2020 not used -->    List<DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO> myList = new List<DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO>();


            DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO[] membersList = securitySvc.GetMembership(PersonalID);

                foreach (DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO members in membersList)
                {
                MembershipDTO membersData = new MembershipDTO   // simplified declaration by Farkas 27Feb2020
                {
                    MembershipID = members.MembershipID,
                    PersonalID = members.PersonalID,
                    MembershipDesc = members.MembershipDesc
                };

                objMemberDTO.Add(membersData);
                }
                return objMemberDTO;
           
        }
        /// <summary>
        /// This method returns the list of members 
        /// </summary>
        /// <returns></returns>
        public List<MembershipDTO> GetMembership()
        {
           
                List<MembershipDTO> objMemDTO = new List<MembershipDTO>();
            // by Farkas 27Feb2020 not used -->    List<DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO> myList = new List<DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO>();


            DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO[] membersList = securitySvc.GetMembershipList();

                foreach (DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO members in membersList)
                {
                MembershipDTO membersData = new MembershipDTO   // simplified declaration by Farkas 27Feb2020
                {
                    MembershipID = members.MembershipID,

                    MembershipDesc = members.MembershipDesc
                };

                objMemDTO.Add(membersData);
                }
                    

                return objMemDTO;          
           
        }
            
        /// <summary>
        /// This method returns the list of roles
        /// </summary>
        /// <returns></returns>

        public List<RoleDTO> GetRoles(int membershipID)
        {
                 List<RoleDTO> objRoleDTO = new List<RoleDTO>();

                DSNY.DSNYCP.Proxys.SecuritySvc.RoleDTO[] roleList = securitySvc.GetRoles(membershipID);

                foreach (DSNY.DSNYCP.Proxys.SecuritySvc.RoleDTO roles in roleList)
                {
                RoleDTO roleData = new RoleDTO   // simplified declaration by Farkas 27Feb2020
                {
                    RoleID = roles.RoleID,
                    RoleDesc = roles.RoleDesc
                };

                objRoleDTO.Add(roleData);
                }                
                return objRoleDTO;                      
        }

        /// <summary>
        /// This method returns the list of members roles with respect to membership id and personnal ID
        /// </summary>
        /// <param name="MembershipID"></param>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<MembershipDTO> GetMembershipRole(Int64 MembershipID, Int64 PersonalID)
        {
           
                List<MembershipDTO> objMembershipDTO = new List<MembershipDTO>();
            // by Farkas 27Feb2020 not used -->    List<DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO> myList = new List<DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO>();


            DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO[] membershipList = securitySvc.GetMembershipRoles( MembershipID, PersonalID );

                foreach (DSNY.DSNYCP.Proxys.SecuritySvc.MembershipDTO member in membershipList)
                {
                    MembershipDTO memberData = new MembershipDTO // simplified declaration by Farkas 27Feb2020
                    {
                        ID = member.ID,
                        PersonalID = member.PersonalID,
                        RoleID = member.RoleID,
                        RoleDesc = member.RoleDesc
                    };


                    objMembershipDTO.Add(memberData);
                }
                return objMembershipDTO;
            
        }
        /// <summary>
        /// This method retuens the list of valid email id w.r.t email id
        /// </summary>
        /// <param name="emailID"></param>
        /// <returns></returns>
        public DSNY.DSNYCP.DTO.UserDTO ValidateUserEmail(String emailID)
        {
            return service.ValidateUserEmail(emailID);
        }
        /// <summary>
        /// This method returns the user saved based on the used
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        //public String SaveUser(UserDTO user, Int64 userId)
        public String SaveUser(UserDTO user)
        {
            //return service.SaveUser(user, userId);
            return service.SaveUser(user);
        }
        public bool HistoryMemberShipRoles(UserDTO user,Int64 userId)
        {
            return service.HistoryMemberShipRoles(user, userId);
        }
        /// <summary>
        /// This function returns true if the password is saved succesfully
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool ResetPassword(UserDTO user)
        {
            return service.ResetPassword(user);
        }
        /// <summary>
        /// This function returns true if the user is deleted succesfully w.r.t person ID
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>
        public bool DeleteUser(Int64 personID)
        {
            return service.DeleteUser(personID);
        }

        /// <summary>
        /// Loads all Associated Complaints.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<AdvocateAssociatedComplaintDTO> LoadAssociatedComplaints(Int64 advocacyCaseID)
        {
           return service.LoadAssociatedComplaints(advocacyCaseID);
        }
        public List<AdvocateHistoryDto> LoadAdvocateHistory(Int64 ComplaintID, Int64 EmployeeID)
        {
            return service.LoadAdvocateHistory(ComplaintID, EmployeeID);
        }   
     
        /// <summary>
        /// Load the Hearing List for the Advocate.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<AdvocateHearingDTO> LoadAdvocateHearingList(Int64 advocacyCaseID)
        {
            return service.LoadAdvocateHearingList(advocacyCaseID);
        }

        /// <summary>
        /// Load the Appeal List for the Advocate.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<AdvocateAppealDTO> LoadAdvocateAppealList(Int64 advocacyCaseID)
        {
            return service.LoadAdvocateAppealList(advocacyCaseID);
        }

        /// <summary>
        /// Load the Penalty List for the Advocate.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<CombinedPenaltyDTO> LoadCombinedPenaltyList(Int64 advocacyCaseID)
        {
            return service.LoadCombinedPenaltyList(advocacyCaseID);
        }


        /// <summary>
        /// Load the Penalty List for the Advocate.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<AdvocatePenaltyDTO> LoadAdvocatePenaltyList(Int64 advocacyCaseID)
        {
            return service.LoadAdvocatePenaltyList(advocacyCaseID);
        }

        /// <summary>
        /// Gets the new user id from the PersonsID.
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>
        public String GetNewUserID(Int64 personID)
        {
            return service.GetNewUserID(personID);
        }

        public List<HideChargesDto> LoadHideCharges(Int16 employeeId)
        {            
                return service.GetHideCharges(employeeId);           

        }
        /// <summary>
        /// Return data after saving
        /// </summary>
        /// <param name="referenceNo"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>

        public bool SaveHideCharges(String referenceNo, Int16 employeeId)
        {         
                
                return service.SaveHideCharges(referenceNo, employeeId);            
        }

        public bool DeleteHideCharges(Int16 viewChargesId)
        {           
                return service.DeleteHideCharges(viewChargesId);            
        }


        # region PersonellWs
        public List<PersonnelWSDTO> GetPersonelDataWS(int pageNo, int pageSize, string firstName, string middleName, string lastName, string referenceNo, string ssnNo)
        {
            return service.GetPersonelDataWS(pageNo, pageSize, firstName, middleName, lastName, referenceNo, ssnNo);
        }

        #endregion


        #region IService Members


        public List<BCADStatusDTO> LoadBCADCaseStatus()
        {
            throw new NotImplementedException();
        }

        public List<BCADPenaltyDTO> LoadBcadPenalty(long bcadCaseId)
        {
            throw new NotImplementedException();
        }

        public bool CreateNewBCAD(BCADComplaintDTO bcadComplaint)
        {
            throw new NotImplementedException();
        }

        public bool SaveBCADPenaltyList(BCADPenaltyDTO bcadPenalty)
        {
            throw new NotImplementedException();
        }

        public bool AuthenticateUser(UserDTO user)
        {
            throw new NotImplementedException();
        }

        public bool CreateNewMedical(MedicalComplaintDTO medicalComplaint)
        {
            throw new NotImplementedException();
        }

        public bool SaveMedicalPenaltyList(MedicalPenaltyDTO medicalPenalty)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IService Members


        public List<HideChargesDto> GetHideCharges(short employeeId)
        {
            throw new NotImplementedException();
        }

        public bool SaveHideCharges(string referenceNo, long employeeId)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
