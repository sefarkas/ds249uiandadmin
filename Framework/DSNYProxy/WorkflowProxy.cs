﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Services;
using DSNY.DSNYCP.DTO;
namespace DSNY.DSNYCP.Proxys
{
    public class WorkflowProxy
    {
        DSNYWorkflowService WorkflowService;

        public WorkflowProxy()
        {
            WorkflowService = new DSNYWorkflowService();
        }
        public int SaveUser(string userName, int GroupId,string LastName,string NTId,string email)
        {
            return WorkflowService.SaveUser(userName, GroupId, LastName, NTId, email);

        }
        public List<WorkflowApplicationDTO> GetAllApplications()
        {
            return WorkflowService.GetAllApplications();
        }

        public bool DeleteUser(int userId)
        {
            return WorkflowService.DeleteUser(userId);
        }
        public bool DeleteGroup(int groupId)
        {
            return WorkflowService.DeleteGroup(groupId);
        }
        public int SaveGroup(string groupName, int applicationId, int parentGroupId)
        {
            return WorkflowService.SaveGroup(groupName, applicationId, parentGroupId);
        }

        public int SaveApplication(string applicationName)
        {
            return WorkflowService.SaveApplication(applicationName);

        }

        public bool DeleteApplication(int applicationId)
        {
            return WorkflowService.DeleteApplication(applicationId);
        }

        public  List<WorkflowGroupsDTO> GetAllGroups()
        {
            return WorkflowService.GetAllGroups();
        }
        public List<WorkflowGroupsDTO> GetAllGroups(int applicationId)
        {
            return WorkflowService.GetAllGroups(applicationId);
        }

        public List<WorkflowGroupsDTO> GetApplicationGroupDetails()
        {
            return WorkflowService.GetApplicationGroupDetails();
        }

        public  List<WorkFlowUser> GetUserDetails()
        {
            return WorkflowService.GetUserDetails();
        }

        public bool IsFinalApproval(int groupId, int applicationID)
        {
            return WorkflowService.IsFinalApproval(groupId, applicationID);

        }

        public List<int> GetAuthorizedRequests(int groipId, int appId)
        {
            return WorkflowService.GetAuthorizedRequests(groipId, appId);
        }
    }
}
