﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DSNY.DSNYCP.DAL;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.IInterfaces;
using System.ComponentModel;
using System.IO;
using System.Transactions;
using System.Security.Permissions ;





namespace DSNY.DSNYCP.Services
{
    /// <summary>
    /// This class file provides the functionality and the set of methods commonly used for DS249/ ADVOCATES/BCAD & EMPLOYEE APPS 
    /// </summary>
    public class Service:IService
    {
        DataAccess dataAccess;


        #region Load Methods
        public List<ComplaintDTO> LoadComplaint(Int64 Id)
        {
            ComplaintDTO complaint = new ComplaintDTO();           
                if(dataAccess == null)
                    dataAccess = new DataAccess();
                List<ComplaintDTO> listComplaint = dataAccess.GetComplaint(Id);               
                return listComplaint;          
        }

        public int SaveErrorLog(string Url, string RequestType, string UserID, string ErrorMessage, string StackTrace, int? AppID)
        {
            AppErrorDTO complaint = new AppErrorDTO();

                if (dataAccess == null)
                    dataAccess = new DataAccess();

                return dataAccess.SaveErrorLog(Url, RequestType, UserID, ErrorMessage, StackTrace, AppID); ;
        }

        
        /// <summary>
        /// This function returns the complaint attachments
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="files"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        public bool UploadFiles(Int64 Id, Int64 LotId, Int64 InspectionRequestId, byte[] fileBytes,
            String ServerLocation, String fileLocation, String applicationName, int controlID, String fileName, 
            Int64 panelID, String chargeCode, String contentType, String childId1,
            DateTime imageDate, String whenTaken, String takenBy, String note)
        //public bool UploadFiles(long ParentID, byte[] files, String applicationName, String contentType, String FileName, String Child1)
        {
            bool IsSuccess = false;
            TransactionScope scope = null;
            
                using ( scope = new TransactionScope())
                {
                    if (dataAccess == null)
                        dataAccess = new DataAccess();
                    String fileDirectory = String.Empty;
                    if (!fileLocation.Contains(fileName))
                    {
                        fileDirectory = ServerLocation + fileLocation;
                        fileLocation += "\\" + Id +  "_" + LotId + "_"  + InspectionRequestId +
                            "_" + childId1 + "_" + fileName;
                    }
                    else
                    {
                        int indx = fileLocation.LastIndexOf("\\");
                        fileDirectory = ServerLocation + fileLocation.Substring(0, indx);
                    }
                   
                    IsSuccess =  dataAccess.UploadFiles(Id,LotId, InspectionRequestId, fileLocation, applicationName, contentType, fileName, chargeCode, childId1,
                       imageDate, whenTaken, takenBy, note);
                    String absoluteFileLocation = ServerLocation + fileLocation;
                    FileIOPermission fper = new
                       FileIOPermission(FileIOPermissionAccess.AllAccess, fileDirectory); 
                    if (!Directory.Exists(fileDirectory))
                        Directory.CreateDirectory(fileDirectory);
                    UploadFilesInDirectory(absoluteFileLocation,  fileBytes);
                    scope.Complete();
                }           
            return IsSuccess;
        }


        private void UploadFilesInDirectory(String ImageLocation,
             byte[] Image)
        {
            FileStream fs = null;          
           
                if (!String.IsNullOrEmpty(ImageLocation) && (Image != null))
                {
                    if (File.Exists(ImageLocation))
                        File.Delete(ImageLocation);
                    fs = File.Create(ImageLocation);
                     fs.Write(Image, 0, Image.Length);                    
                }                     
        }
        
        public bool UploadFiles(long Id, byte[] files, String applicationName, int controlID, String fileName, Int64 panelID, String chargeCode, String contentType)
        
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.UploadFiles(Id, files, applicationName, contentType, fileName, chargeCode);

           
        }

        /// <summary>
        /// This function returns the complaint attachments
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="files"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        public bool UploadFiles(long Id, byte[] files, int controlID, String fileName, Int64 panelID, String chargeCode, String contentType)
         {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.UploadFiles(Id, files, contentType, fileName, chargeCode);
        }


        public bool SaveCombinePenalties(long ParentID, Int16 ChildCombPenID, Int16 CombPenID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.SaveCombinePenalties(ParentID, ChildCombPenID, CombPenID);           
        }
        public bool DeleteCombinePenalties(long ParentID)
        {          
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.DeleteCombinePenalties(ParentID);
        
        }
        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentId, Int64 ParentId, string  ChildId ,string AppName)
        {
            UpLoadFileDTO complaint = new UpLoadFileDTO();          
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<UpLoadFileDTO> complaintAttachments = dataAccess.LoadComplaintAttachments(DocumentId, ParentId, ChildId, AppName);              
                return complaintAttachments;          
        }

        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID,Int64 LotId, Int64 InspectionRequestId,
             string ChildID, String AppName)
        {
            UpLoadFileDTO complaint = new UpLoadFileDTO();           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<UpLoadFileDTO> complaintAttachments = dataAccess.LoadComplaintAttachments(DocumentID, ParentID, LotId, InspectionRequestId, ChildID, AppName);
                return complaintAttachments;           
        }

        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 DocumentId, Int64 ParentId, string ChildId)
        {
            UpLoadFileDTO complaint = new UpLoadFileDTO();            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<UpLoadFileDTO> complaintAttachments = dataAccess.LoadComplaintAttachments(DocumentId, ParentId, ChildId);              
                return complaintAttachments;            
        }
        public bool DeleteSupportedDocument(Int64 ParentID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.DeleteSupportedDocument(ParentID);           
        }

        
        /// <summary>
        /// This function returns the list of complaints based on the search critera
        /// </summary>
        /// <param name="SearchText"></param>
        /// <returns></returns>
        public List<ComplaintDTO> SearchComplaintId(String SearchText)
        {             
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<ComplaintDTO> Complaint = dataAccess.SearchComplaintId(SearchText);
                return Complaint;           
        }

        /// <summary>
        /// This function returns the list of complaints history based on ID and the complaint ID
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<ComplaintHistoryDTO> LoadComplaintHistory(Int64 Id, Int64 ComplaintID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<ComplaintHistoryDTO> hDTO = dataAccess.GetComplaintHistory(Id, ComplaintID);
                return hDTO;          
        }


        public List<ComplaintDTO> LoadComplaintList(Int16 personalID, int pageNo, int maximumRows,ref Int64 rowsCount)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                complaintList = dataAccess.GetComplaints(personalID, pageNo, maximumRows,ref rowsCount);
                return complaintList;         
        }


        public List<ComplaintDTO> LoadSearchComplaintList(int createdBy, int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue, ref int rowCount)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();
          
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                complaintList = dataAccess.GetSearchComplaints(createdBy, pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue,ref rowCount);
                return complaintList;            
        }

        public List<ComplaintDTO> LoadAdvocateSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue, ref Int32 rowCount)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                complaintList = dataAccess.GetAdvocateSearchComplaints(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue,ref rowCount);
                return complaintList;          
        }

        public List<ComplaintDTO> LoadBCADSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                complaintList = dataAccess.GetBCADSearchComplaints(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue);
                return complaintList;           
        }

        public List<ComplaintDTO> LoadMedicalSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();          
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                complaintList = dataAccess.GetMedicalSearchComplaints(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue);
                return complaintList;           
        }

        public int LoadComplaintListCount(Int16 personalID)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.GetComplaintsListCount(personalID);            
        }


        /// <summary>
        /// This function returns the list of employee images based on ID and binary employee image data
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="EmployeeImage"></param>
        /// <returns></returns>

        public List<PersonnelByImageDTO> LoadEmployeeImage(Int32 Id, Byte[] EmployeeImage)
        {
            if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<PersonnelByImageDTO> personnelImageDTO = dataAccess.LoadEmployeeImage(Id, EmployeeImage);
                return personnelImageDTO;            
        }
        /// <summary>
        /// This function returns the list of personnels based on ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public List<PersonnelByIdDTO> LoadPersonnel(Int64 Id)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<PersonnelByIdDTO> personnelByIDDTO = dataAccess.GetPersonnel(Id);
                return personnelByIDDTO;            
        }
        /// <summary>
        /// This function returns the list of personnels based on reference number
        /// </summary>
        /// <param name="RefNo"></param>
        /// <returns></returns>
        public List<PersonnelByRefNoDTO> LoadPersonnel(String RefNo)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<PersonnelByRefNoDTO> personnelByRefNoDTO = dataAccess.GetPersonnel(RefNo);
                return personnelByRefNoDTO;            
        }
        /// <summary>
        /// This method Return Personnel List to the calling function
        /// </summary>
        /// <param name="SearchString"></param>
        /// <returns></returns>
        public List<PersonnelListDTO> LoadPersonnelList(String SearchString)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<PersonnelListDTO> personnelListDTO = dataAccess.GetPersonnelList(SearchString);
                return personnelListDTO;
           
        }
        /// <summary>
        /// This method returns the list of employees based on their ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public List<EmployeeInfoDTO> LoadEmployeeInfo(Int64 ID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<EmployeeInfoDTO> employeeInfoDTO = dataAccess.LoadEmployeeInfo(ID);
                return employeeInfoDTO;           
        }
        /// <summary>
        /// This method Return Personnel List to the calling function
        /// </summary>
        /// <returns></returns>
        public List<PersonnelDTO> LoadPersonnelList()
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<PersonnelDTO> personnelListDTO = dataAccess.GetPersonnelList();
                return personnelListDTO;           
        }
        /// <summary>
        /// This method Return Employee List to the calling function
        /// </summary>
        /// <returns></returns>
        public List<EmployeeListDTO> LoadEmployeeList()
        {
             if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<EmployeeListDTO> employeesListDTO = dataAccess.LoadEmployeeList();
                return employeesListDTO;            
        }
        /// <summary>
        /// This method Return charges List to the calling function based on the charge code
        /// </summary>
        /// <param name="ChargeCode"></param>
        /// <returns></returns>
        public List<ChargeDTO> LoadCharges(String ChargeCode)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<ChargeDTO> chargesDTO = dataAccess.GetCharges(ChargeCode);
                return chargesDTO;            
        }
        /// <summary>
        /// This method Return charges List without parameters to the calling function
        /// </summary>
        /// <returns></returns>
        public List<ChargesWithoutParametersDTO> LoadCharges()
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<ChargesWithoutParametersDTO> chargeDTO = dataAccess.GetCharges();
                return chargeDTO;           
        }
        /// <summary>
        /// This method loads the list of code based on the category ID
        /// </summary>
        /// <param name="CategoryID"></param>
        /// <returns></returns>
        public List<CodeDTO> LoadCodeList(Int16 CategoryID)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<CodeDTO> codeDTO = dataAccess.GetCodeList(CategoryID);
                return codeDTO;            
        }
        /// <summary>
        /// This method loads the list of BCAD Case status.
        /// </summary>
        /// <returns></returns>
        public List<BCADStatusDTO> LoadBCADCaseStatus()
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<BCADStatusDTO> bcadStatusDTO = dataAccess.GetBCADCaseStatus();
                return bcadStatusDTO;           
        }

        public List<MedicalStatusDTO> LoadMedicalCaseStatus()
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();  
                List<MedicalStatusDTO> medicalStatusDTO = dataAccess.GetMedicalCaseStatus();            
                return medicalStatusDTO;           
        }

        public List<LocationDTO> LoadLocations()
        {           
                List<LocationDTO> locationDTO = new List<LocationDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                locationDTO = dataAccess.GetLocations();
                return locationDTO;           
        }
        /// <summary>
        /// This method loads the list of BOROUGH 
        /// </summary>
        /// <returns></returns>
        public List<BoroughDTO> LoadBoroughs()
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<BoroughDTO> dsBorough = dataAccess.GetBoroughs();
                return dsBorough;
           
        }
        /// <summary>
        /// This method loads the list of titles 
        /// </summary>
        /// <returns></returns>
        public List<TitleDTO> LoadTitles()
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<TitleDTO> listTitle = dataAccess.GetTitles();
                return listTitle;            
        }
        /// <summary>
        /// This method loads the list of titles w.r.t search criteria 
        /// </summary>
        /// <param name="SearchText"></param>
        /// <returns></returns>
        public List<TitleListDTO> LoadTitles(String SearchText)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<TitleListDTO> titleDTO = dataAccess.GetTitles(SearchText);
                return titleDTO;           
        }
        /// <summary>
        /// This method loads the list of witnesses based on the complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<WitnessDto> LoadWitnesses(Int64 ComplaintID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<WitnessDto> listWitnessDTO = dataAccess.LoadWitnesses(ComplaintID);               
                return listWitnessDTO;            
        }
        /// <summary>
        /// This method loads the list of complaints based on the complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<ComplainantDTO> LoadComplainants(Int64 ComplaintID)
        {        
            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<ComplainantDTO> listComplainants = dataAccess.LoadComplainants(ComplaintID);
                return listComplainants;           
        }
        /// <summary>
        /// This method loads the list of violations based on the complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public List<ViolationDTO> LoadViolations(Int64 ComplaintID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<ViolationDTO> voilationDTO = dataAccess.LoadViolations(ComplaintID);
                return voilationDTO;            
        }
        /// <summary>
        /// This method loads the list of incident based on the complaint ID  and charge ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="ChargeID"></param>
        /// <returns></returns>
        public List<IncedentDTO> LoadIncidents(Int64 ComplaintID, Int64 ChargeID)
        {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<IncedentDTO> incidentMapper = dataAccess.LoadIncidents(ComplaintID, ChargeID);
                return incidentMapper;           
        }
        /// <summary>
        /// This method loads the list of BCAD complaints
        /// </summary>
        /// <returns></returns>
        public List<ComplaintDTO> LoadBCADComplaints(int pageNo, int maximumRows)
        {           
                List<ComplaintDTO> cDTOBCAD = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                cDTOBCAD = dataAccess.GetBCADComplaints(pageNo, maximumRows);
                return cDTOBCAD;            
        }

        public List<ComplaintDTO> LoadMedicalComplaints(int pageNo, int maximumRows)
        {            
                List<ComplaintDTO> cDTOMedical = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                cDTOMedical = dataAccess.GetMedicalComplaints(pageNo, maximumRows);              
                return cDTOMedical;
        }

        /// <summary>
        /// This method loads the list of advocate complaints
        /// </summary>
        /// <returns></returns>

        public List<ComplaintDTO> LoadAdvocateComplaints(int pageNo, int maximumRows, ref Int64 rowsCount)
        {            
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                cDTO = dataAccess.GetAdvocateComplaints(pageNo, maximumRows,ref rowsCount);
                return cDTO;            
        }

        public List<ComplaintDTO> LoadCombinedComplaints(int pageNo, Int64 employeeID, int maximumRows, Int64 complaintId, Int64 indexNo, ref Int64 rowsCount)
        {            
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                cDTO = dataAccess.GetCombinedComplaints(pageNo, employeeID, maximumRows, complaintId,indexNo,ref rowsCount);
                return cDTO;            
        }

        public List<ComplaintDTO> CheckCombinedComplaints(Int64 complaintId)
        {           
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                cDTO = dataAccess.CheckCombinedComplaints(complaintId);
                return cDTO;           
        }

        public int LoadAdvocateListCount(Int16 personalID)
        {            
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.LoadAdvocateListCount(personalID);
          
        }

        public int LoadCombineListCount(Int64 employeeID, Int64 complaintId, Int64 indexNo)
        {           
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.LoadCombineListCount(employeeID,complaintId,indexNo);           
        }


        public int LoadBCADListCount(Int16 personalID)
        {            
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.LoadBCADListCount(personalID);           
        }

        public int LoadMedicalListCount(Int16 personalID)
        {           
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.LoadMedicalListCount(personalID);           
        }

        public int LoadSearchComplaintListCount(Int16 personalID, int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.GetSearchComplaintsListCount(personalID, numericValue, decimalValue, dateValue, stringValue);            
        }

        public int LoadSearchAdvocateListCount(Int16 personalID, int numericValue, string decimalValue, string dateValue, string stringValue)
        {            
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.LoadSearchAdvocateListCount(personalID, numericValue, decimalValue, dateValue, stringValue);           
        }

        public int LoadSearchBCADListCount(Int16 personalID, int numericValue, string decimalValue, string dateValue, string stringValue)
        {           
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.LoadSearchBCADListCount(personalID, numericValue, decimalValue, dateValue, stringValue);
           
        }

        public int LoadSearchMedicalListCount(Int16 personalID, int numericValue, string decimalValue, string dateValue, string stringValue)
        {           
                List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.LoadSearchMedicalListCount(personalID, numericValue, decimalValue, dateValue, stringValue);          
        }


        /// <summary>
        /// This method loads the list of BCAD Penalties
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <returns></returns>
        public List<BCADPenaltyDTO> LoadBCADPenalty(Int64 BCADCaseID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<BCADPenaltyDTO> bcadPenaltyDTO = dataAccess.LoadBCADPenalty(BCADCaseID);
                return bcadPenaltyDTO;
           
        }
        /// <summary>
        /// This method loads the list of Advocate Associated Complaint
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<MedicalPenaltyDTO> LoadMedicalPenalty(Int64 MedicalCaseID)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<MedicalPenaltyDTO> medicalPenaltyDTO = dataAccess.LoadMedicalPenalty(MedicalCaseID);                
                return medicalPenaltyDTO;
           
        }
                
        public List<AdvocateAssociatedComplaintDTO> LoadAssociatedComplaints(Int64 advocacyCaseID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<AdvocateAssociatedComplaintDTO> advAssociatedComplaint = dataAccess.LoadAssociatedComplaints(advocacyCaseID);
                return advAssociatedComplaint;           
        }

        public List<AdvocateHistoryDto> LoadAdvocateHistory(Int64 ComplaintID, Int64 EmployeeID)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<AdvocateHistoryDto> advAssociatedHistory = dataAccess.LoadAssociatedHistory(ComplaintID, EmployeeID);
                return advAssociatedHistory;           
        }

        /// <summary>
        /// This method loads the list of Advocate hearing Complaint
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<AdvocateHearingDTO> LoadAdvocateHearingList(Int64 advocacyCaseID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<AdvocateHearingDTO> dsPenalty = dataAccess.LoadAdvocateHearingList(advocacyCaseID);
                return dsPenalty;           
        }
        /// <summary>
        /// This method loads the list of Advocate appeal Complaint
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<AdvocateAppealDTO> LoadAdvocateAppealList(Int64 advocacyCaseID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<AdvocateAppealDTO> advAppeal = dataAccess.LoadAdvocateAppealList(advocacyCaseID);
                return advAppeal;            
        }

        /// <summary>
        /// This method loads the list of Advocate appeal Complaint
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<CombinedPenaltyDTO> LoadCombinedPenaltyList(Int64 advocacyCaseID)
        {
            if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<CombinedPenaltyDTO> advCombPenalty = dataAccess.LoadCombinedPenaltyList(advocacyCaseID);
                return advCombPenalty;           
        }
        /// <summary>
        /// This method loads the list of Advocate PenaltyList based on advocate case ID
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<AdvocatePenaltyDTO> LoadAdvocatePenaltyList(Int64 advocacyCaseID)
        {
           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<AdvocatePenaltyDTO> advPenalty = dataAccess.LoadAdvocatePenaltyList(advocacyCaseID);
                return advPenalty;           
        }


        #endregion

        #region Save Methods
        /// <summary>
        /// Thsi function saves the complaints based on the complaint ID
        /// </summary>
        /// <param name="complaint"></param>
        /// <returns></returns>
        public bool SaveComplaint(ComplaintDTO complaint, Int64 personID)
        {
           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                Int64 ID = complaint.ComplaintID;


                if (dataAccess.SaveComplaint(complaint, personID))
                {
                    return true;
                }
                else
                    return false;
           
        }
        /// <summary>
        /// This function returns true if the complaint is reopen or a close approve complaint
        /// </summary>
        /// <param name="complaint"></param>
        /// <returns></returns>
        public bool ReopenORCloseApproveComplaint(ComplaintDTO complaint)
        {
            if (dataAccess == null)
                dataAccess = new DataAccess();
            return dataAccess.ReopenORCloseApproveComplaint(complaint.ComplaintID,
                                            complaint.ComplaintStatus,
                                            complaint.CreatedBy);
        }
        /// <summary>
        /// This function returns true if the Undelete Complaint are deleted sucesfully w.r.t  complaintID & status
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool DeleteUndeleteComplaint(Int64 complaintID, Int16 status)
        {
            if (dataAccess == null)
                dataAccess = new DataAccess();
            return dataAccess.DeleteUndeleteComplaint(complaintID, status);
        }
        public bool ComplaintHistoryDetails(Int64 complaintID, String status, Int64 UserId, DateTime date)
        {
            if (dataAccess == null)
                dataAccess = new DataAccess();
            return dataAccess.ComplaintHistoryDetails(complaintID, status, UserId, date);
        }

        public bool AdminPersonDetails(Int64 UserId, String Status, Int64 personId)
        {
            if (dataAccess == null)
                dataAccess = new DataAccess();
            return dataAccess.AdminPersonDetails(UserId, Status, personId);
        }

        public bool SaveWitness(WitnessDto witness)
        {
            if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.SaveWitness(witness.ComplaintID,
                                                witness.RefNo,
                                                witness.PersonnelName,
                                                witness.UniqueID);           
        }
        /// <summary>
        /// This function returns true if the complaints are saved sucessfully w.r.t complaint ID
        /// </summary>
        /// <param name="complainant"></param>
        /// <returns></returns>
        public bool SaveComplainant(ComplainantDTO complainant)
        {
            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.SaveComplainant(complainant.ComplaintID,
                                            complainant.RefNo,
                                            complainant.ComplainantName,
                                            complainant.UniqueID,
                                            complainant.LocationID,
                                            complainant.TitleID,
                                            complainant.Statement,
                                            complainant.OnDate,
                                            complainant.Text,
                                            complainant.ThisDate,
                                            complainant.Category,
                                            complainant.ReportedDate,
                                            complainant.FromLocation,
                                            complainant.ReportedTime,
                                            complainant.ReportingTime,
                                            complainant.TelephoneNo);
            
        }
        /// <summary>
        /// This function returns true if the voilation are saved sucessfully w.r.t voilation ID
        /// </summary>
        /// <param name="voilation"></param>
        /// <returns></returns>
        public bool SaveVoilation(ViolationDTO voilation)
        {
            if (dataAccess == null)
                    dataAccess = new DataAccess();
                Int64 ID = -1;
                ChargeDTO charge = voilation.Charge;
                if (dataAccess.SaveViolation(voilation.ComplaintID,
                                                charge.ChargeCode,
                                                voilation.UniqueID,
                                                voilation.IncidentLocationID,
                                                voilation.IncidentType,
                                                ref ID))
                {
                    voilation.ChargeID = ID;
                    return true;
                }
                else
                {
                    return false;
                }
           
        }
        /// <summary>
        /// This function returns true if the incident are saved sucessfully w.r.t incident ID
        /// </summary>
        /// <param name="incident"></param>
        /// <returns></returns>
        public bool SaveIncident(IncedentDTO incident)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.SaveIncident(incident.ComplaintID, incident.Count, incident.IncedentDate, incident.ChargeID);
            
        }
        /// <summary>
        /// This function returns true if the ComplaintStatus are Update sucessfully w.r.t incident ID & Status
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>
        public bool UpdateComplaintStatus(Int64 ComplaintID, Int16 Status)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.UpdateComplaintStatus(ComplaintID, Status))
                {
                    return true;
                }
                else
                {
                    return false;
                }
           
        }



        public bool UpdateCombinedComplaints(String indexno, Int64 parentIndexNo,String uncmbIndexNo,Int64 uncmbParentIndexNo)
        {
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.UpdateCombinedComplaints(indexno, parentIndexNo,uncmbIndexNo,uncmbParentIndexNo))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }



        public Int64 CheckIsParent(Int64 IndexNo, ref Int64 IsChild)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                return dataAccess.CheckIsParent(IndexNo, ref IsChild);             
            
        }
         /// <summary>
        /// This function returns true if the new BCAD is created succesfully
        /// </summary>
        /// <param name="bcadComplaint"></param>
        /// <returns></returns>

        public bool CreateNewBCAD(BCADComplaintDTO bcadComplaint)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.CreateBCADComplaint(bcadComplaint.ComplaintID, bcadComplaint.CreateUser);
         }
        /// <summary>
        /// This function returns true if the advocate complaints are opened successfully 
        /// w.r.t case id, received user and received date.
        /// </summary>
        /// <param name="AdvocacyCaseId"></param>
        /// <param name="receivedUser"></param>
        /// <param name="receivedDate"></param>
        /// <returns></returns>

        public bool CreateNewMedical(MedicalComplaintDTO medicalComplaint)
        {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.CreateMedicalComplaint(medicalComplaint.ComplaintID, medicalComplaint.CreateUser);           
        }

        public bool OpenAdvocateComplaint(Int64 AdvocacyCaseId, Int16 receivedUser, DateTime receivedDate)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.OpenAdvocateComplaint(AdvocacyCaseId, receivedUser, receivedDate);          
        }

        /// <summary>
        /// This function returns the list of BCAD complaints w.r.t ComplaintID and the list of complaints
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="bcadComplaint"></param>
        /// <returns></returns>
        public List<BCADComplaintDTO> GetBCADComplaint(Int64 ComplaintID, BCADComplaintDTO bcadComplaint)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<BCADComplaintDTO> bcadComplaintDTO = dataAccess.GetBCADComplaint(ComplaintID);
                return bcadComplaintDTO;         

        }


        public List<MedicalComplaintDTO> GetMedicalComplaint(Int64 ComplaintID, MedicalComplaintDTO medicalComplaint)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<MedicalComplaintDTO> medicalComplaintDTO = dataAccess.GetMedicalComplaint(ComplaintID);             
                return medicalComplaintDTO;           

        }       

        /// <summary>
        /// This function returns the list of BCAD complaints w.r.t ComplaintID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>

        public List<ComplaintAdvocacyDTO> GetAdvocateComplaint(Int64 ComplaintID)
        {
            if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<ComplaintAdvocacyDTO> dsAdvComplaint = dataAccess.GetAdvocateComplaint(ComplaintID);
                return dsAdvComplaint;           
        }
        
        /// <summary>
        /// This function returns true if the BCAD status is set sucessfully w.r.t complaint ID and status code.
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="statusCode"></param>
        /// <returns></returns>
        public bool SetBCADStatus(long complaintID, short statusCode)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.SetBCADStatus(complaintID, statusCode))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }


        public bool SetMedicalStatus(long complaintID, short statusCode)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.SetMedicalStatus(complaintID, statusCode))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }
        
        /// <summary>
        /// This function returns true if the Advocate status is set sucessfully w.r.t complaint ID, advocateCaseID and status code
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="statusCode"></param>
        /// <param name="advocateCaseID"></param>
        /// <returns></returns>


        public bool SetAdvocateCaseStatus(Int64 complaintID, Int16 statusCode, Int64 advocateCaseID)
        {
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.SetAdvocateCaseStatus(complaintID, statusCode, advocateCaseID))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }
        
        /// <summary>
        /// This function returns true if the case status is routed from the Advocate to the authour sucessfully w.r.t complaint ID and advocacyCaseId
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="advocacyCaseId"></param>
        /// <returns></returns>
        public bool ReturnMedicalToAuthor(Int64 ComplaintID, Int64 personalID)
        {
            if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.ReturnMedicalToAuthor(ComplaintID, personalID))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }


        /// <summary>
        /// This function returns true if the case status is routed from the BCAD to the authour sucessfully w.r.t complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool ReturnBCADToAuthor(Int64 ComplaintID, Int64 PersonalID)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.ReturnBCADToAuthor(ComplaintID, PersonalID))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }
        /// <summary>
        /// This function returns true if the case status is routed from the BCAD to the Advocate sucessfully w.r.t complaint ID 
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool ReturnBCADToAdvocate(Int64 ComplaintID, Int64 personalID)
        {
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.ReturnBCADToAdvocate(ComplaintID, personalID))
                {
                    return true;
                }
                else
                {
                    return false;
                }            
        }

        /// <summary>
        /// This function returns true if the case status is routed from the Advocateto the BCAD  sucessfully w.r.t complaint ID and advocacyCaseID
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>

        public bool ReturnMedicalToAdvocate(Int64 ComplaintID, Int64 personalID)
        {             
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.ReturnMedicalToAdvocate(ComplaintID, personalID))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }
        public bool ReturnAdvocateCaseToAuthor(Int64 ComplaintID, Int64 advocacyCaseId, Int64 personalID)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.ReturnAdvocateCaseToAuthor(ComplaintID, advocacyCaseId,personalID))
                {
                    return true;
                }
                else
                {
                    return false;
                }          
        }
        public bool ReturnAdvocateToBCAD(Int64 complaintID, Int64 advocacyCaseID, Int64 PersonalID)
        {   
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                if (dataAccess.ReturnAdvocateToBCAD(complaintID, advocacyCaseID, PersonalID))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }
        /// <summary>
        /// This function returns true if the BCAD complaints are saved sucessfully w.r.t complaint ID
        /// </summary>
        /// <param name="bcadComplaint"></param>
        /// <returns></returns>
        public bool SaveBCADComplaint(BCADComplaintDTO bcadComplaint)
        {          
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.SaveBCADComplaint(bcadComplaint);         
        }
        /// <summary>
        /// This function returns true if the BCAD Penalty are saved sucessfully w.r.t bcad Penalty ID
        /// </summary>
        /// <param name="bcadPenalty"></param>
        /// <returns></returns>
        public bool SaveMedicalComplaint(MedicalComplaintDTO medicalComplaint)
        {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.SaveMedicalComplaint(medicalComplaint);         
        }


        public bool SaveBCADPenaltyList(BCADPenaltyDTO bcadPenalty)
        {
              if (dataAccess == null)
                    dataAccess = new DataAccess();
                if (dataAccess.SaveBCADPenaltyList(bcadPenalty.PenaltyGroupID,
                                                    bcadPenalty.PenaltyTypeCode,
                                                    bcadPenalty.Days,
                                                    bcadPenalty.Hours,
                                                    bcadPenalty.Minutes,
                                                    bcadPenalty.MoneyValue,
                                                    bcadPenalty.Notes,
                                                    bcadPenalty.UniqueID))
                {
                    return true;
                }
                else
                {
                    return false;
                }            
        }

        /// <summary>
        /// Return DTO FOR Medical Penalty List
        /// </summary>
        /// <param name="medicalPenalty"></param>
        /// <returns></returns>
        public bool SaveMedicalPenaltyList(MedicalPenaltyDTO medicalPenalty)
        {
            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
       
                if (dataAccess.SaveMedicalPenaltyList(medicalPenalty.PenaltyGroupID,
                                                    medicalPenalty.PenaltyTypeCode,
                                                    medicalPenalty.Days,
                                                    medicalPenalty.Hours,
                                                    medicalPenalty.Minutes,
                                                    medicalPenalty.MoneyValue,
                                                    medicalPenalty.Notes,
                                                    medicalPenalty.UniqueID))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }

        public bool SaveAdvocateComplaint(ComplaintAdvocacyDTO advocate)
        {
           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.SaveAdvocateComplaint(advocate);           
        }

     



        #endregion

        #region Assembler Methods
        /// <summary>
        /// This method assembles the data from the DTO  complaint 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="complaint"></param>
        /// <returns></returns>
        private ComplaintDTO AssembleComplaint(IDataReader dr, ComplaintDTO complaint)
        {        
              while (dr.Read())
              { 
                  complaint.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                  if(dr["LocationId"] != DBNull.Value)
                  complaint.LocationID = Convert.ToInt64(dr["LocationId"]);
                  if(dr["LocationName"] != DBNull.Value)
                      complaint.LocationName = Convert.ToString(dr["LocationName"]);
                  if(dr["RespondentEmpID"] != DBNull.Value)
                  complaint.EmployeeID = Convert.ToInt64(dr["RespondentEmpID"]);                  
                  if(dr["RespondentRefNumber"] != DBNull.Value)
                  complaint.ReferenceNumber = Convert.ToString(dr["RespondentRefNumber"]);
                  if(dr["TitleId"] != DBNull.Value)
                  complaint.TitleID = Convert.ToInt64(dr["TitleId"]);
                  if (dr["BadgeNumber"] != DBNull.Value)
                  complaint.BadgeNumber = Convert.ToString(dr["BadgeNumber"]);                  
                  if (dr["ComplaintActionID"] != DBNull.Value)
                    complaint.ComplaintActionID = Convert.ToInt16(dr["ComplaintActionID"]);                  
                  if (dr["IsSubmitted"] != DBNull.Value)
                    complaint.IsSubmitted = Convert.ToBoolean(dr["IsSubmitted"]);
                  if(dr["ChargesServedDate"]!= DBNull.Value)
                     complaint.ChargesServedDate = Convert.ToDateTime(dr["ChargesServedDate"]);
                  if(dr["SuspensionOrderedByEmpId"]!= DBNull.Value)
                     complaint.SuspensionOrderedById = Convert.ToInt64(dr["SuspensionOrderedByEmpId"]);
                  if (dr["SuspensionOrderedName"] != DBNull.Value)
                      complaint.SuspensionOrderedByName = Convert.ToString(dr["SuspensionOrderedName"]);
                  if (dr["SuspensionOrderedRefNo"] != DBNull.Value)
                      complaint.SuspensionOrderedByRefNo = Convert.ToString(dr["SuspensionOrderedRefNo"]);
                  if(dr["SuspensionOrderedDt"]!= DBNull.Value)
                        complaint.SuspensionOrderDate = Convert.ToDateTime(dr["SuspensionOrderedDt"]);
                  if(dr["ChargesServedByEmpId"]!= DBNull.Value)
                    complaint.ChargesServedBy = Convert.ToInt64(dr["ChargesServedByEmpId"]);
                  if (dr["ChargesServedByName"] != DBNull.Value)
                      complaint.ChargesServedByName= Convert.ToString(dr["ChargesServedByName"]);
                  if (dr["ChargesServedByRefNo"] != DBNull.Value)
                      complaint.ChargesServedByRefNo = Convert.ToString(dr["ChargesServedByRefNo"]);

                  if(dr["SuspensionLiftedByEmpId"]!= DBNull.Value)
                    complaint.SuspensionLiftedBy = Convert.ToInt64(dr["SuspensionLiftedByEmpId"]);
                  if (dr["SuspensionLiftedByName"] != DBNull.Value)
                      complaint.SuspensionLiftedByName= Convert.ToString(dr["SuspensionLiftedByName"]);
                  if (dr["SuspensionLiftedByRefNo"] != DBNull.Value)
                      complaint.SuspensionLiftedByRefNo = Convert.ToString(dr["SuspensionLiftedByRefNo"]);

                  if(dr["SuspensionLiftedDt"]!= DBNull.Value)
                    complaint.SuspensionLifedDate = Convert.ToDateTime(dr["SuspensionLiftedDt"]);
                  if(dr["RoutingLocation"] != DBNull.Value)
                     complaint.RoutingLocation = Convert.ToString(dr["RoutingLocation"]);
                  if(dr["IsProbation"]!= DBNull.Value)
                    complaint.IsProbation = Convert.ToBoolean(dr["IsProbation"]);
                  if(dr["Approved"]!= DBNull.Value)
                      complaint.IsApproved = Convert.ToBoolean(dr["Approved"]);                
                 if(dr["CreatedBy"] != DBNull.Value)
                     complaint.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                  if(dr["CreatedDate"]!= DBNull.Value)
                     complaint.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                  if (dr["Comments"] != DBNull.Value)
                     complaint.Comments = Convert.ToString(dr["Comments"]);
                  if (dr["StreetNumber"] != DBNull.Value)
                      complaint.StreetNumber = Convert.ToString(dr["StreetNumber"]);
                  if (dr["StreetName"] != DBNull.Value)
                      complaint.StreetName = Convert.ToString(dr["StreetName"]);
                  if (dr["AptNo"] != DBNull.Value)
                      complaint.AptNo = Convert.ToString(dr["AptNo"]);
                  if (dr["City"] != DBNull.Value)
                      complaint.City = Convert.ToString(dr["City"]);
                  if (dr["State"] != DBNull.Value)
                      complaint.State = Convert.ToString(dr["State"]);
                  if (dr["Zip"] != DBNull.Value)
                      complaint.ZipCode = Convert.ToString(dr["Zip"]);
                  if (dr["zipSuffix"] != DBNull.Value)
                      complaint.ZipSuffix = Convert.ToString(dr["zipSuffix"]);
                  if(dr["PayCodeId"] != DBNull.Value)
                      complaint.PayCodeID = Convert.ToInt16(dr["PayCodeId"]);
                  if(dr["PayDesc"] != DBNull.Value)
                      complaint.PayCodeDesc = Convert.ToString(dr["PayDesc"]);
                  if(dr["ChartNumber"] != DBNull.Value)
                      complaint.ChartNo = Convert.ToString(dr["ChartNumber"]);
                  if (dr["VacationSchedule"] != DBNull.Value)
                      complaint.VacationSchedule = Convert.ToString(dr["VacationSchedule"]);
                  if (dr["RespondentFName"] != DBNull.Value)
                      complaint.RespondentFName = dr["RespondentFName"].ToString();
                  if (dr["RespondentLName"] != DBNull.Value)
                      complaint.RespondentLName = dr["RespondentLName"].ToString();
                  if (dr["RespondentDOB"] != DBNull.Value)
                      complaint.RespondentDOB = Convert.ToDateTime(dr["RespondentDOB"]);
                  if (dr["RespondentApptDt"] != DBNull.Value)
                      complaint.RespondentAptDt = Convert.ToDateTime(dr["RespondentApptDt"]);
                  if (dr["Title"] != DBNull.Value)
                      complaint.TitleDesc = Convert.ToString(dr["Title"]);
              }
              return complaint;        
        }
        /// <summary>
        ///  This method assembles the complaint history data from the DTO  complaint 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="historyList"></param>
        /// <returns></returns>
        private List<ComplaintHistoryDTO> AssembleComplaintHistory(IDataReader dr, List<ComplaintHistoryDTO> historyList)
        {
             ComplaintHistoryDTO history;
                List<ComplaintHistoryDTO> hList = new List<ComplaintHistoryDTO>();
                while (dr.Read())
                {
                    history = new ComplaintHistoryDTO();
                    history.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                    if(dr["CreatedDate"] != DBNull.Value)
                        history.VoilationDate = Convert.ToDateTime(dr["CreatedDate"]);
                    if(dr["Charges"] != DBNull.Value)
                        history.Voilations = Convert.ToString(dr["Charges"]);
                    if(dr["ChargesDesc"] != DBNull.Value)
                        history.VoilationDesc = Convert.ToString(dr["ChargesDesc"]);
                    hList.Add(history);
                }
                return hList;            
        }

        /// <summary>
        /// This method assembles the complaint data from the DTO  complaint 
        /// </summary>
        /// <param name="dr"></param>
        /// <returns></returns>
        private List<ComplaintDTO> AssembleComplaintList(IDataReader dr)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();            
                ComplaintDTO complaint;
                List<ComplaintDTO> cList = new List<ComplaintDTO>();
                while (dr.Read())
                {
                    complaint = new ComplaintDTO();
                    complaint.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                    if(dr["LocationId"] != DBNull.Value)
                        complaint.LocationID = Convert.ToInt64(dr["LocationId"]);
                    if(dr["LocationName"] != DBNull.Value)
                        complaint.LocationName = Convert.ToString(dr["LocationName"]);
                    if(dr["RespondentEmpID"] != DBNull.Value)
                        complaint.EmployeeID = Convert.ToInt64(dr["RespondentEmpID"]);                    
                    if(dr["RespondentRefNumber"] != DBNull.Value)
                        complaint.ReferenceNumber = Convert.ToString(dr["RespondentRefNumber"]);
                    if(dr["Approved"] != DBNull.Value)
                        complaint.IsApproved = Convert.ToBoolean(dr["Approved"]);

                    if (dr["ApprovedDate"] != DBNull.Value)
                        complaint.ApprovedDate = Convert.ToDateTime(dr["ApprovedDate"]);
                   
                    if(dr["CreatedBy"] != DBNull.Value)
                        complaint.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                    if(dr["CreatedDate"] != DBNull.Value)
                        complaint.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    complaint.Status = Convert.ToString(dr["Status"]);
                    if (dr["Charges"] != DBNull.Value)
                        complaint.Charge = dr["Charges"].ToString();
                    if (dr["ChargesDesc"] != DBNull.Value)
                        complaint.ChargeDesc = dr["ChargesDesc"].ToString();
                    if (dr["RespondentFName"] != DBNull.Value)
                        complaint.RespondentFName = dr["RespondentFName"].ToString();
                    if (dr["RespondentLName"] != DBNull.Value)
                        complaint.RespondentLName = dr["RespondentLName"].ToString();
                    if (dr["complaintStatus"] != DBNull.Value)
                        complaint.ComplaintStatus = Convert.ToInt16(dr["complaintStatus"]);
                    if (dr["complaintStatusDate"] != DBNull.Value)
                        complaint.ComplaintStatusDate = Convert.ToDateTime(dr["complaintStatusDate"]);

                    cList.Add(complaint);
                }
                return cList;          
          
        }
       
        #endregion

        #region Reporting Methods
        /// <summary>
        /// This method returns the list of complaints w.r.t districts 
        /// </summary>
        /// <returns></returns>
        public List<ComplaintByDistrictDTO> GetComplaintByDistrict()
        {          
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.GetComplaintByDistrict();          
        }        


        #endregion

        #region "ADMIN"
        /// <summary>
        /// This method loads the list of personnels with respect to the search criteria
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>
        public List<PersonnelDTO> LoadAllPersonnel(String searchString)
        {
            PersonnelListDTO pList = new PersonnelListDTO();           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<PersonnelDTO> dsPersonnelLst = dataAccess.GetAllPersonnel(searchString);
                return dsPersonnelLst;            
        }
        /// <summary>
        /// This method loads the list of code
        /// </summary>
        /// <returns></returns>
        public List<PayCodeDTO> LoadPayCodeList()
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<PayCodeDTO> payCodeDTO = dataAccess.GetPayCodeList();
                return payCodeDTO;            
        }
        /// <summary>
        /// This method returns the the saved user from the DTO user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        //public String SaveUser(UserDTO user, Int64 userId)
        public String SaveUser(UserDTO user)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();                
                return dataAccess.SaveUser(user);           
        }
        public bool HistoryMemberShipRoles(UserDTO user,Int64 userId)
        {
            if (dataAccess == null)
                dataAccess = new DataAccess();
            return dataAccess.HistoryMemberShipRoles(user, userId);
        }
        
        /// <summary>
        /// This methos returns true if the password is reset sucessfully.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean ResetPassword(UserDTO user)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                if (dataAccess.ResetPassword(user.UserID, user.Password, user.IsTempPassword))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }
        /// <summary>
        /// This method returns true if the user is deleted sucessfully.
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>
        public Boolean DeleteUser(Int64 personID)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                if (dataAccess.DeleteUser(personID))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            
        }
        /// <summary>
        /// This function retuns the new user w.r.t the personnel ID
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>
        public String GetNewUserID(Int64 personID)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.GetNewUserId(personID);            
        }
        /// <summary>
        /// This method returns true if the user is authenticated
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public Boolean AuthenticateUser(UserDTO user)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                Int64 ID = user.PersonalID;
                Boolean IsTempPwd = user.IsTempPassword;
                Boolean isAdmin = user.IsAdmin;
                if (dataAccess.AuthenticateUser(ref ID, ref IsTempPwd, ref isAdmin, user.UserID, user.Password))
                {
                    user.PersonalID = ID;
                    user.IsTempPassword = IsTempPwd;
                    user.IsAdmin = isAdmin;
                    return true;
                }
                else
                {
                    return false;
                }           
        }
        /// <summary>
        /// This method returns true if the email is valid for the authrntic user
        /// </summary>
        /// <param name="emailID"></param>
        /// <returns></returns>
        public UserDTO ValidateUserEmail(String emailID)
        {
            Boolean isAdmin = false;
            UserDTO usr = new UserDTO();           
                if (dataAccess == null)
                    dataAccess = new DataAccess();

                usr.UserID = dataAccess.ValidateUserEmail(emailID, ref isAdmin);
                usr.IsAdmin = isAdmin;
           
            return usr;
        }




        /// <summary>
        /// Return list of hide charges for the given user
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public List<HideChargesDto> GetHideCharges(Int16 employeeId)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<HideChargesDto> hideChargesDto = dataAccess.GetChargesTBLHideCharges(employeeId);               
                return hideChargesDto; 
        }

        /// <summary>
        /// Save Hide Charges 
        /// </summary>
        /// <param name="referenceNo"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public bool SaveHideCharges(String referenceNo, Int64 employeeId)
        {            
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.SaveHideCharges(referenceNo ,employeeId);           
        }

        /// <summary>
        /// Delete hide Charges
        /// </summary>
        /// <param name="viewChargesId"></param>
        /// <returns></returns>

        public bool DeleteHideCharges(Int16 viewChargesId)
        {           
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                return dataAccess.DeleteHideCharges(viewChargesId);           
        }        

        #endregion

        # region PersonnelWS
        public List<PersonnelWSDTO> GetPersonelDataWS(int pageNo, int pageSize, string firstName, string middleName, string lastName, string referenceNo, string ssnNo)
        {
            dataAccess = new DataAccess();
            return dataAccess.GetPersonelDataWS(pageNo, pageSize, firstName, middleName, lastName, referenceNo,ssnNo);
        }
        #endregion
    }
}
