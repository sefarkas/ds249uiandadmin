﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.DAL;
using DSNY.DSNYCP.IInterfaces;

namespace DSNY.DSNYCP.Services
{
   public class DSNYWorkflowService : IDSNYWorkflowService 
    {
        
        #region IDSNYWorkflowService Members

        public List<WorkFlowUser> GetALLUserByGroupIDApllicationID(int groupId, int ApplicationId)
        {

            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetAllUserByGroupApplication(groupId, ApplicationId);


        }
        public List<WorkflowApplicationDTO> GetAllApplications()
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetAllApplications();
        }
        public int SaveUser(string UserName, int GroupId,string LastName,string NTID,string Email)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.SaveUser(UserName, GroupId,LastName,NTID,Email);

        }
        public bool DeleteUser(int UserId)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.DeleteUser(UserId);
        }
        public bool DeleteGroup(int groupId)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.DeleteGroup(groupId);
        }
        public int SaveGroup(string GroupName, int ApplicationId, int parentGroupId)
        {

            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.SaveGroup(GroupName, ApplicationId, parentGroupId);
        }

        public int SaveApplication(string ApplicationName)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.SaveApplication(ApplicationName);
        
        }

        public bool DeleteApplication(int ApplicationId)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.DeleteApplication(ApplicationId);
        }
        public int GetTotalApprovalLevels(int groupId, int ApplicationId)
        {

            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetTotalApprovalLevels(groupId, ApplicationId);


        }
        

        public string GetConfigValueBykey(string key)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetConfigValueByKey(key);

        }

        public bool IsFinalApproval(int GroupId,int applicationID)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.IsFinalApproval(GroupId, applicationID);

        }


        #endregion

        #region IDSNYWorkflowService Members

        

        #endregion

        #region IDSNYWorkflowService Members


        public bool SaveWorkflow(WorkflowDTO workflow)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.SaveWorkflowApprovalDetails(workflow);
        }

        #endregion

        public List<WorkflowGroupsDTO> gGetAllGroups()
        {
            throw new NotImplementedException();
        }

        public List<WorkflowGroupsDTO> GetAllGroups()
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetAllGroups();
        }

        public List<WorkflowGroupsDTO> GetAllGroups(int ApplicationId)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetAllGroups(ApplicationId);
        }

        public List<WorkflowGroupsDTO> GetApplicationGroupDetails()
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetApplicationGroupDetails();
        }

        public List<WorkFlowUser> GetUserDetails()
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetUserDetails();
        }

        #region IDSNYWorkflowService Members


        public int SaveUser(string UserName, int GroupId)
        {
            throw new NotImplementedException();
        }

        #endregion

        public List<int> GetAuthorizedRequests(int groipId, int AppId)
        {
            WorkFlowDataAccess workFlowDataAccess = new WorkFlowDataAccess();
            return workFlowDataAccess.GetAuthorizedRequests(groipId, AppId);
        }
    }
}
