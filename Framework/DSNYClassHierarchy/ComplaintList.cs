﻿    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to load complaint, load complaints 
    /// based on the search and Assemble Complaint List
    /// </summary>
    public class ComplaintList
    {
        Proxy proxy;      
        public ComplaintList()
        {
            list = null;
        }


        /// <summary>
        /// Private Variable
        /// </summary>
        List<Complaint> list;

        /// <summary>
        /// Public Property
        /// </summary>
        public List<Complaint> List
        {
            get { return list; }
            set { list = value; }
        }

        
        ///logical building for Reopen for admin 
        /// <summary>
        /// This method loads the list of complaints based on the 
        /// type od resource(BCAD/ADVOCATE/DS249)
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool Load(RequestSource source, int pageNo, int maximumRows,ref Int64 rowsCount)
        {           
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();
           
                Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                switch (source)
                {
                    case RequestSource.DS249:
                        complaintListDto = proxy.LoadComplaintList(createdBy, pageNo, maximumRows, ref rowsCount);
                        AssembleComplaintList(complaintListDto, RequestSource.DS249);
                        break;
                    case RequestSource.BCAD:
                        complaintListDto = proxy.LoadBCADComplaints(pageNo, maximumRows);
                        AssembleComplaintList(complaintListDto, RequestSource.BCAD);
                        break;
                    case RequestSource.Advocate:
                        complaintListDto = proxy.LoadAdvocateComplaints(pageNo, maximumRows,ref rowsCount);
                        AssembleComplaintList(complaintListDto, RequestSource.Advocate);
                        break;
                    case RequestSource.Medical:
                        complaintListDto = proxy.LoadMedicalComplaints(pageNo, maximumRows);                        
                        AssembleComplaintList(complaintListDto, RequestSource.Medical);
                        break;
                    default:
                        break;
                }
                return true;            
        }

        /// <summary>
        /// This method loads the list of complaints based on the 
        /// type od resource(ADVOCATE)
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>

        public bool LoadCombinedComplaints(RequestSource source, int pageNo, Int64 employeeID, int maximumRows, Int64 complaintId, Int64 indexNo, ref Int64 rowsCount)
        {           
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();
           
                Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                complaintListDto = proxy.LoadCombinedComplaints(pageNo, employeeID, maximumRows, complaintId, indexNo, ref rowsCount);
                AssembleCombinedComplaintList(complaintListDto, RequestSource.Advocate);
                return true; 
           
        }

         /// </summary>
        /// <returns></returns>
        public  bool CheckCombineComplaint(Int64 complaintID)
        {          
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();           
                if (proxy == null)
                    proxy = new Proxy();
                complaintListDto = proxy.CheckCombinedComplaints(complaintID);
                AssembleCombineComplaintList(complaintListDto, RequestSource.Advocate);          

            return true;
        }                          

        /// <summary>
        /// This method loads the list of complaints 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public List<Complaint> LoadSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue,ref Int32  rowCount)
        {           
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();
            Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                complaintListDto = proxy.LoadSearchComplaintList(createdBy, pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue, ref rowCount);
                return AssembleSearchComplaintList(complaintListDto, RequestSource.DS249);     
            
        }

        /// <summary>
        /// This method loads the list of complaints 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public List<Complaint> LoadAdvocateSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue,ref Int32  rowCount)
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();
                Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                complaintListDto = proxy.LoadAdvocateSearchComplaintList(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue,ref rowCount);
                return AssembleSearchComplaintList(complaintListDto, RequestSource.Advocate);          
        }

        /// <summary>
        /// This method loads the list of complaints 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public List<Complaint> LoadCadSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();           
                Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                complaintListDto = proxy.LoadCadSearchComplaintList(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue);
                return AssembleSearchComplaintList(complaintListDto, RequestSource.BCAD);            
        }

        /// <summary>
        /// This method loads the list of complaints 
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public List<Complaint> LoadMedicalSearchComplaintList(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>(); 
              Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                complaintListDto = proxy.LoadMedicalSearchComplaintList(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue);
                return AssembleSearchComplaintList(complaintListDto, RequestSource.Medical);           
        }


        /// <summary>
        /// This method gives the count of the number of Complaints
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int LoadComplaintListCount()
        {           
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();
                 Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadComplaintListCount(createdBy);         
           
        }

        /// <summary>
        /// This method gives the count of the number of Complaints
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>

        public int LoadAdvocateListCount()
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();
            Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadAdvocateListCount(createdBy);
           
        }

        public int LoadCombineListCount(Int64 employeeID, Int64 complaintId, Int64 indexNo)
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();        
              if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadCombineListCount(employeeID, complaintId, indexNo);
        }





         /// <summary>
        /// This method gives the count of the number of Complaints
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int LoadMedicalListCount()
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();
            Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadMedicalListCount(createdBy);           
        }

        /// <summary>
        /// This method gives the count of the number of Complaints
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int LoadBCADListCount()
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();
               Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadBCADListCount(createdBy);           
        }


        /// <summary>
        /// This method gives the count of the number of Complaints
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int LoadSearchComplaintListCount(int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();           
                Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadSearchComplaintListCount(createdBy, numericValue, decimalValue, dateValue, stringValue);
           
        }

        /// <summary>
        /// This method gives the count of the number of Complaints
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int LoadSearchAdvocateListCount(int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();          
                Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadSearchAdvocateListCount(createdBy, numericValue, decimalValue, dateValue, stringValue);
           
        }

        /// <summary>
        /// This method gives the count of the number of Complaints
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int LoadSearchCadListCount(int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();    
                 Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadSearchBCADListCount(createdBy, numericValue, decimalValue, dateValue, stringValue);
        }

        /// <summary>
        /// This method gives the count of the number of Complaints
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int LoadSearchMedicalListCount(int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            List<ComplaintDTO> complaintListDto = new List<ComplaintDTO>();            
                Int16 createdBy = Convert.ToInt16(HttpContext.Current.Session["UserEmpID"]);
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.LoadSearchMedicalListCount(createdBy, numericValue, decimalValue, dateValue, stringValue);
          
        }        

        /// <summary>
        /// This method loads the list of complaints based on the Prefex Text
        /// </summary>
        /// <param name="PrefixText"></param>
        /// <returns></returns>
        public bool Load(String prefix)
        {           
                if (proxy == null)
                    proxy = new Proxy();
                List<ComplaintDTO> ComplaintList;
                ComplaintList = proxy.SearchComplaintId(prefix);
                AssembleComplaintList(ComplaintList);
                return true;           
        }

        /// <summary>
        /// This method assembles the list of Assemble Complaint from the DTO.
        /// </summary>
        /// <param name="dsComplaintList"></param>
        private void AssembleComplaintList(List<ComplaintDTO> ComplaintList)
        {
            if (list == null)
                list = new List<Complaint>();
            Complaint complaint;
            foreach (ComplaintDTO complnt in ComplaintList)
            {
                complaint = new Complaint();
                complaint.IndexNo = complnt.IndexNo;
                list.Add(complaint);
            }            
        }
        
        /// <summary>
        /// This method assembles the list of Assemble Complaint List from the DTO
        /// </summary>
        /// <param name="dsComplaintList"></param>
        /// <param name="source"></param>
        private List<Complaint> AssembleSearchComplaintList(List<ComplaintDTO> complaintListDto, RequestSource source)
        {

            List<Complaint> returnedList = new List<Complaint>();

            Complaint complaint;
            
            foreach (ComplaintDTO cDTO in complaintListDto)
            {
                complaint = new Complaint();
                complaint.ComplaintId = cDTO.ComplaintID;

                if (source == RequestSource.Advocate)
                {
                    complaint.DataSource = cDTO.DataSource;
                }

                complaint.IndexNo = cDTO.IndexNo;

                complaint.LocationId = cDTO.LocationID;
            
                complaint.LocationName = cDTO.LocationName;

                complaint.RoutingLocation = cDTO.RoutingLocation;
            
                complaint.EmployeeId = cDTO.EmployeeID;

                complaint.ReferenceNumber = cDTO.ReferenceNumber;

                complaint.IsApproved = cDTO.IsApproved;
                complaint.ApprovedDate = cDTO.ApprovedDate;

                complaint.CreatedBy = cDTO.CreatedBy;

                complaint.CreatedDate = cDTO.CreatedDate;

                complaint.IncidentDate = cDTO.IncidentDate;

                complaint.Jt1 = cDTO.Jt1;

                complaint.Status = cDTO.Status;

                complaint.Charge = cDTO.Charge;

                complaint.RespondentFirstName = cDTO.RespondentFName;

                complaint.RespondentLastName = cDTO.RespondentLName;

                complaint.ComplaintStatus = cDTO.ComplaintStatus;

                complaint.ComplaintStatusDate = cDTO.ComplaintStatusDate;

                complaint.Indicator = cDTO.Indicator;

                complaint.IsDeleted = cDTO.IsDeleted;

                if (source == RequestSource.DS249)
                {
                    complaint.IsDeleted = cDTO.IsDeleted;
                    complaint.Jt1 = cDTO.Jt1;

                }

                if (source == RequestSource.Advocate)
                {
                    complaint.CaseId = cDTO.CaseID;
                    complaint.Jt1 = cDTO.Jt1;
                }

                returnedList.Add(complaint);
            }
            return returnedList;
        }

        /// <summary>
        /// This method assembles the list of Assemble Complaint List from the DTO
        /// </summary>
        /// <param name="dsComplaintList"></param>
        /// <param name="source"></param>
        private void AssembleComplaintList(List<ComplaintDTO> complaintListDto, RequestSource source)
        {
            if (list == null)
                list = new List<Complaint>();

            Complaint complaint;
            
            foreach (ComplaintDTO cDTO in complaintListDto)
            {
                complaint = new Complaint();
                complaint.ComplaintId = cDTO.ComplaintID;

                if (source == RequestSource.Advocate)
                {
                    complaint.DataSource = cDTO.DataSource;
                }
                complaint.IndexNo = cDTO.IndexNo;

                complaint.ParentIndexNo = cDTO.ParentIndexNo;
                
                complaint.LocationId = cDTO.LocationID;                
            
                complaint.LocationName = cDTO.LocationName;
                
                complaint.RoutingLocation = cDTO.RoutingLocation;

                complaint.FinalStatusCD = cDTO.FinalStatusCD;
            
                complaint.EmployeeId = cDTO.EmployeeID;

                complaint.ReferenceNumber = cDTO.ReferenceNumber;

                complaint.IsApproved = cDTO.IsApproved;

                complaint.ApprovedDate = cDTO.ApprovedDate;

                complaint.CreatedBy = cDTO.CreatedBy;

                complaint.CreatedDate = cDTO.CreatedDate;

                complaint.IncidentDate = cDTO.IncidentDate;                

                complaint.Status = cDTO.Status;

                complaint.Charge = cDTO.Charge;              

                complaint.RespondentFirstName = cDTO.RespondentFName;

                complaint.RespondentLastName = cDTO.RespondentLName;

                complaint.ComplaintStatus = cDTO.ComplaintStatus;

                complaint.ComplaintStatusDate = cDTO.ComplaintStatusDate;

                complaint.Indicator = cDTO.Indicator;

                if (source == RequestSource.DS249)
                {
                    complaint.IsDeleted = cDTO.IsDeleted;
                    complaint.Jt1 = cDTO.Jt1;
                }

                if (source == RequestSource.Advocate)
                {
                    complaint.CaseId = cDTO.CaseID;
                    complaint.Jt1 = cDTO.Jt1;
                }

                list.Add(complaint);
            }
            }

        private void AssembleCombinedComplaintList(List<ComplaintDTO> complaintListDto, RequestSource source)
        {
            if (list == null)
                list = new List<Complaint>();

            Complaint complaint;

            foreach (ComplaintDTO cDTO in complaintListDto)
            {
                complaint = new Complaint();
                complaint.ComplaintId = cDTO.ComplaintID;
                if (source == RequestSource.Advocate)
                {
                    complaint.DataSource = cDTO.DataSource;
                }
                complaint.IndexNo = cDTO.IndexNo;
                complaint.ParentIndexNo = cDTO.ParentIndexNo;
                complaint.IncidentDate = cDTO.IncidentDate;
                complaint.ComplaintStatusDate = cDTO.ComplaintStatusDate;
                complaint.LocationId = cDTO.LocationID;
                complaint.RespondentFirstName = cDTO.RespondentFName;
                complaint.RespondentLastName = cDTO.RespondentLName;
                complaint.Status = cDTO.Status;
                complaint.Charge = cDTO.Charge;        

                if (source == RequestSource.Advocate)
                {
                    complaint.CaseId = cDTO.CaseID;
                }

                list.Add(complaint);
            }
        }
        
        /// <summary>
        /// This method assembles the list of Assemble Complaint List from the DTO
        /// </summary>
        /// <param name="dsComplaintList"></param>
        /// <param name="source"></param>
        private void AssembleCombineComplaintList(List<ComplaintDTO> complaintListDto, RequestSource source)
        {
            if (list == null)
                list = new List<Complaint>();

            Complaint complaint;

            foreach (ComplaintDTO cDTO in complaintListDto)
            {
                complaint = new Complaint();               

               complaint.IndexNo = cDTO.IndexNo;                

                list.Add(complaint);
            }
        }


















    } 

   
}
