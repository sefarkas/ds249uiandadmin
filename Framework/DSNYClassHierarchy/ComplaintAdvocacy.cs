﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to Open Case,Save complaints, Load complaints, Get Associated ComplaintList, Get Hearing List,
    /// Get Appeal List, Get Peanlty List, Set Advocate CaseStatus, Return To Author, Return To BCAD,
    /// Create Complaint Advocacy  	
    /// </summary>
    public class ComplaintAdvocacy
    {
        ComplaintAdvocacyDTO advocateDTO;
        Proxy proxy;

        public ComplaintAdvocacy()
        {
            _advocacyCaseId = -1;
            _complaintId = -1;
            _caseClassificationcd = -1;
            _originationSourcecd = -1;
            _specialStatus_cd = -1;
            _trialReturnStatuscd = -1;
            _trialReturnDate = DateTime.MinValue;
            _reviewStatuscd = -1;
            _reviewStatusDate = DateTime.MinValue;
            _actioncd = -1;
            _actionDate = DateTime.MinValue;
            _isClosed = -1;
            _closedDate = DateTime.MinValue;
            _cobStatus_cd = -1;
            _cobDate = DateTime.MinValue;
            _finalStatuscd = -1;
            _caseComment = String.Empty;
            _recieveUser = -1;
            _receiveUserName = String.Empty;
            _recieveDate = DateTime.MinValue;
            _modifiedUser = -1;
            _modifiedUserName = String.Empty;
            _modifiedDate = DateTime.MinValue;
            _isAvailable = String.Empty;
            _cobStatus_cd = -1;
            _cobDate = DateTime.MinValue;
            _closedDate = DateTime.MinValue;
            isProbation = false;
        }

        #region Private Variable
        /// <summary>
        /// Private Variable
        /// </summary>
            private Int64 _advocacyCaseId;
            private Int64 _complaintId;
            private Int64 _caseClassificationcd;
            private Int64 _originationSourcecd;
            private Int64 _specialStatus_cd;
            private Int64 _trialReturnStatuscd;
            private DateTime _trialReturnDate;
            private Int64 _reviewStatuscd;
            private DateTime _reviewStatusDate;
            private Int64 _actioncd;
            private DateTime _actionDate;
            private short _isClosed;
            private DateTime _closedDate;
            private Int64 _cobStatus_cd;
            private DateTime _cobDate;
            private Int16 _finalStatuscd;
            private String _caseComment;
            private Int16 _recieveUser;
            private String _receiveUserName;
            private DateTime _recieveDate;
            private Int16 _modifiedUser;
            private String _modifiedUserName;
            private DateTime _modifiedDate;
            private String _isAvailable;
            private AdvocateAssociatedComplaintList associatedComplaints;
            private AdvocateHearingList hearingList;
            private AdvocatePenaltyList penaltyList;
            private CombinedPenaltyList combPenaltyList;           
            private AdvocateAppealList appealList;
            private VoilationList chargeList;
            private bool isProbation;
        #endregion

        #region Public Properties
        /// <summary>
        /// Private Variable
        /// </summary>
        public Int64 AdvocacyCaseId
            {
                get { return _advocacyCaseId; }
                set { _advocacyCaseId = value; }
            }
        public Int64 ComplaintId
        {
            get { return _complaintId; }
            set { _complaintId = value; }
        }
        public Int64 CaseClassificationCD
        {
            get { return _caseClassificationcd; }
            set { _caseClassificationcd = value; }
        }
        public Int64 OriginationSourceCD
        {
            get { return _originationSourcecd; }
            set{_originationSourcecd = value;}
        }
        public Int64 SpecialStatus_CD
        {
            get { return _specialStatus_cd; }
            set { _specialStatus_cd = value; }
        }
        public Int64 TrialReturnStatusCD
        {
            get { return _trialReturnStatuscd; }
            set { _trialReturnStatuscd = value; }
        }
        public DateTime TrialReturnDate
        {
            get { return _trialReturnDate; }
            set { _trialReturnDate = value; }
        }
        public Int64 ReviewStatusCD
            {
                get { return _reviewStatuscd; }
                set { _reviewStatuscd = value; }
        }
        public DateTime ReviewStatusDate
            {
                get { return _reviewStatusDate; }
                set { _reviewStatusDate = value; }
        }
        public Int64 ActionCD
            {
                get { return _actioncd; }
                set { _actioncd = value; }
        }
        public DateTime? ActionDate{get;set;}
      
        public Int16 IsClosed
            {
                get { return _isClosed; }
                set { _isClosed = value; }
        }
        public DateTime? ClosedDate {get;set;}
       
        public Int64 CobStatus_CD
            {
                get { return _cobStatus_cd; }
                set { _cobStatus_cd = value; }
        }
        public DateTime? CobDate{get;set;}
            
        public Int16 FinalStatusCD
            {
                get { return _finalStatuscd; }
                set { _finalStatuscd = value; }
        }
        public String CaseComment
            {
                get { return _caseComment; }
                set { _caseComment = value; }
        }
        public Int16 ReceiveUser
        {
            get { return _recieveUser; }
            set { _recieveUser = value; }
        }
        public String RecieveUserName
        {
            get { return _receiveUserName; }
            set { _receiveUserName = value; }
        }
        public DateTime RecieveDate
            {
                get { return _recieveDate; }
                set { _recieveDate = value; }
        }
        public Int16 ModifiedUser
        {
            get { return _modifiedUser; }
            set { _modifiedUser = value; }
        }
        public String ModifiedUserName
        {
            get { return _modifiedUserName; }
            set { _modifiedUserName = value; }
        }
        public DateTime ModifiedDate
            {
                get { return _modifiedDate; }
                set { _modifiedDate = value; }
        }
        public String IsAvailable
        {
            get { return _isAvailable; }
            set { _isAvailable = value; }
        }

        public AdvocateAssociatedComplaintList AssociatedComplaints
        {
            get { return associatedComplaints; }
            set { associatedComplaints = value; }
        }
        public AdvocateHearingList HearingList
        {
            get { return hearingList; }
            set { hearingList = value; }
        }
        public AdvocatePenaltyList PenaltyList
        {
            get { return penaltyList; }
            set { penaltyList = value; }
        }

        public AdvocateAppealList AppealList
        {
            get { return appealList; }
            set { appealList = value; }
        }
        public Boolean IsProbation
        {
            get { return isProbation; }
            set { isProbation = value; }
        }
        public VoilationList ChargeList
        {
            get { return chargeList; }
            set { chargeList = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// This method returns true if the Advocate case is sucessfully opened.
        /// </summary>
        /// <returns></returns>
        public bool OpenCase()
        {
            if (proxy == null)
                proxy = new Proxy();
            return proxy.OpenAdvocateComplaint(this._advocacyCaseId, this._recieveUser, this._recieveDate);
        }
        /// <summary>
        /// This method returns true if the Advocate case is sucessfully saved
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {            
                if (proxy == null)
                    proxy = new Proxy();
                CreateComplaintAdvocacy();
                return proxy.SaveAdvocateComplaint(advocateDTO);         
         
        }
        
        /// <summary>
        /// This method returns true if the Advocate case is sucessfully loaded
        /// </summary>
        public void Load()
        {            
                List<ComplaintAdvocacyDTO> AdvComplaint = new List<ComplaintAdvocacyDTO>();
               
                if (proxy == null)
                    proxy = new Proxy();
                               
                AdvComplaint = proxy.GetAdvocateComplaint(_complaintId);

                AssembleAdvocateComplaint(AdvComplaint);

                this.associatedComplaints = new AdvocateAssociatedComplaintList();
                this.associatedComplaints.List = GetAssociatedComplaintList();

                this.hearingList = new AdvocateHearingList();
                this.hearingList.List = GetHearingList();

                this.appealList = new AdvocateAppealList();
                this.appealList.List = GetAppealList();

                this.penaltyList = new AdvocatePenaltyList();
                this.penaltyList.List = GetPeanltyList();

                this.combPenaltyList = new CombinedPenaltyList();
                this.combPenaltyList.List = GetCombinedPeanltyList();

        }
        /// <summary>
        /// This method returns the list the Advocate Associated Complaint 
        /// </summary>
        /// <returns></returns>
        private List<AdvocateAssociatedComplaint> GetAssociatedComplaintList()
        {
            AdvocateAssociatedComplaintList associatedComplaint = new AdvocateAssociatedComplaintList();
            associatedComplaint.Load(this._advocacyCaseId);
            associatedComplaint.NewUniqueId = Convert.ToInt16(associatedComplaint.List.Count);
            return associatedComplaint.List;
        }
        /// <summary>
        /// This method returns the list the Advocate Hearing Complaint 
        /// </summary>
        /// <returns></returns>
        private List<AdvocateHearing> GetHearingList()
        {
            AdvocateHearingList hearings = new AdvocateHearingList();
            hearings.Load(this._advocacyCaseId);
            hearings.NewUniqueId = Convert.ToInt16(hearings.List.Count);
            return hearings.List;
        }
        /// <summary>
        /// This method returns the list the Advocate Appeal Complaint 
        /// </summary>
        /// <returns></returns>
        private List<AdvocateAppeal> GetAppealList()
        {
            AdvocateAppealList appeals = new AdvocateAppealList();
            appeals.Load(this._advocacyCaseId);
            appeals.NewUniqueId = Convert.ToInt16(appeals.List.Count);
            return appeals.List;
        }
        /// <summary>
        /// This method returns the list the Advocate Penalty Complaint
        /// </summary>
        /// <returns></returns>
        private List<AdvocatePenalty> GetPeanltyList()
        {
            AdvocatePenaltyList penalty = new AdvocatePenaltyList();
            penalty.Load(this._advocacyCaseId);
            penalty.NewUniqueId = Convert.ToInt16(penalty.List.Count);
            return penalty.List;
        }

        /// <summary>
        /// This method returns the list the Advocate Penalty Complaint
        /// </summary>
        /// <returns></returns>
        private List<CombinedPenalty> GetCombinedPeanltyList()
        {
            CombinedPenaltyList combPenalty = new CombinedPenaltyList();
            combPenalty.Load(this._advocacyCaseId);           
            return combPenalty.List;
        }
        /// <summary>
        /// This method returns the Set Advocate Case Status Complaint
        /// </summary>
        /// <returns></returns>
        public bool SetAdvocateCaseStatus()
        {           
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.SetAdvocateCaseStatus(this._complaintId, this._finalStatuscd,  this._advocacyCaseId);           
        }
        /// <summary>
        /// This method returns the name of the authour based on complaint Id and advocacyCase Id
        /// </summary>
        /// <returns></returns>
        public bool ReturnToAuthor(Int64 PersonalID)
        {            
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.ReturnAdvocateCaseToAuthor(_complaintId, _advocacyCaseId, PersonalID);            
        }
        /// <summary>
        /// This method returns the name of the BCAD complant based on complaint Id and advocacyCase Id
        /// </summary>
        /// <returns></returns>
        public bool ReturnToBcad(Int64 PersonalID)
        {           
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.ReturnAdvocateToBCAD(_complaintId, _advocacyCaseId, PersonalID);           
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// This method creates advocate complaints
        /// </summary>
        private void CreateComplaintAdvocacy()
        {            
                advocateDTO = new ComplaintAdvocacyDTO();
                advocateDTO.ComplaintId = this._complaintId;
                advocateDTO.Actioncd = this._actioncd;
                advocateDTO.ActionDate = this.ActionDate;
                advocateDTO.AdvocacyCaseId = this._advocacyCaseId;
                advocateDTO.CaseClassificationcd = this._caseClassificationcd;
                advocateDTO.CaseComment = this._caseComment;
                advocateDTO.ClosedDate = this.ClosedDate;
                advocateDTO.COBDate = this.CobDate;
                advocateDTO.COBStatus_cd = this._cobStatus_cd;
                advocateDTO.FinalStatuscd = this._finalStatuscd;
                advocateDTO.IsAvailable = this._isAvailable;
                advocateDTO.isClosed = this._isClosed;
                advocateDTO.ModifiedDate = this._modifiedDate;
                advocateDTO.ModifiedUser = this._modifiedUser;
                advocateDTO.OriginationSourcecd = this._originationSourcecd;
                advocateDTO.RecieveDate = this._recieveDate;
                advocateDTO.RecieveUser = this._recieveUser;
                advocateDTO.ReviewStatuscd = this._reviewStatuscd;
                advocateDTO.ReviewStatusDate = this._reviewStatusDate;
                advocateDTO.SpecialStatus_cd = this._specialStatus_cd;
                advocateDTO.TrialReturnDate = this._trialReturnDate;
                advocateDTO.TrialReturnStatuscd = this._trialReturnStatuscd;                
                CreateAssociatedComplaints();
                CreatePenalty();
                CreateHearings();
                CreateAppeals();
                CreateOverrideCharges();               
        }

        /// <summary>
        /// This method creates the override charges 
        /// </summary>
        private void CreateOverrideCharges()
        {
            ViolationDTO chargeDTO;
            advocateDTO.Charges = new List<ViolationDTO>();
            foreach (Voilation charge in this.chargeList.List)
            {
                chargeDTO = new ViolationDTO();
                chargeDTO.ChargeID = charge.ChargeId;
                chargeDTO.ComplaintID = charge.ComplaintId;              
                if (!String.IsNullOrWhiteSpace(charge.OverRideViolationDescription)) 
                {
                    String voilation = charge.OverRideViolationDescription.Substring(0, charge.OverRideViolationDescription.IndexOf(" "));
                    chargeDTO.OverideCharge = voilation;
                }
                chargeDTO.PrimaryIndicator = charge.PrimaryIndicator;
                advocateDTO.Charges.Add(chargeDTO);
            }
        }
        /// <summary>
        /// This method creates AssociatedComplaints
        /// </summary>
        private void CreateAssociatedComplaints()
        {   
            AdvocateAssociatedComplaintDTO assDTO;
            advocateDTO.AssociatedComplaints = new List<AdvocateAssociatedComplaintDTO>();

            foreach (AdvocateAssociatedComplaint assComplaint in this.AssociatedComplaints.List)
            {
                assDTO = new AdvocateAssociatedComplaintDTO();
                assDTO.AdvocacyCaseID = assComplaint.AdvocacyCaseId;
                assDTO.AsscociatedCompaintID = assComplaint.AssociatedComplaintId;
                assDTO.AssociatedID = assComplaint.AssociatedId;
                advocateDTO.AssociatedComplaints.Add(assDTO);
            }
        }
        /// <summary>
        /// This method creates hearing
        /// </summary>
        private void CreateHearings()
        {
            AdvocateHearingDTO hearingDTO;
            advocateDTO.Hearings = new List<AdvocateHearingDTO>();

            foreach (AdvocateHearing hearing in this.hearingList.List)
            {
                hearingDTO = new AdvocateHearingDTO();
                hearingDTO.AdvocacyCaseID = hearing.AdvocacyCaseId;
                hearingDTO.AdvocateHearingID = hearing.AdvocateHearingId;
                hearingDTO.CalStatusCd = hearing.CalStatusCD;
                hearingDTO.HearingComments = hearing.HearingComments;
                hearingDTO.HearingDt = hearing.HearingDate;
                hearingDTO.HearingStatusCd = hearing.HearingStatusCD;
                hearingDTO.HearingTypeCd = hearing.HearingTypeCD;
                hearingDTO.ModifiedBy = hearing.ModifiedBy;
                hearingDTO.ModifiedDt = hearing.ModifiedDate;
                hearing.UniqueId = hearing.UniqueId;
                advocateDTO.Hearings.Add(hearingDTO);
            }
        }
        /// <summary>
        /// This method creates appeals
        /// </summary>
        private void CreateAppeals()
        {
            AdvocateAppealDTO appealDTO;
            advocateDTO.Appeals = new List<AdvocateAppealDTO>();

            foreach (AdvocateAppeal appeal in this.appealList.List)
            {
                appealDTO = new AdvocateAppealDTO();
                appealDTO.AdvocacyCaseID = appeal.AdvocacyCaseId;
                appealDTO.AdvocateAppealID = appeal.AdvocateAppealId;
                appealDTO.AppealComments = appeal.AppealComments;
                appealDTO.AppealDt = appeal.AppealDate;
                appealDTO.AppealresultCd = appeal.AppealresultCD;
                appealDTO.AppealCd = appeal.AppealCD;
                appealDTO.ModifiedBy = appeal.ModifiedBy;
                appealDTO.ModifiedDt = appeal.ModifiedDate;
                appealDTO.UniqueID = appeal.UniqueId;
                advocateDTO.Appeals.Add(appealDTO);
            }
        }
        /// <summary>
        /// This method creates penalty
        /// </summary>
        private void CreatePenalty()
        {
            AdvocatePenaltyDTO penaltyDTO;
            advocateDTO.Penaltys = new List<AdvocatePenaltyDTO>();
            foreach (AdvocatePenalty penalty in this.penaltyList.List)
            {
                penaltyDTO = new AdvocatePenaltyDTO();
                penaltyDTO.AdvocacyCaseID = penalty.AdvocatePenaltyId;
                penaltyDTO.AdvocatePenaltyID = penalty.AdvocatePenaltyId;
                penaltyDTO.ApplyToTotal = penalty.ApplyToTotal;
                penaltyDTO.DataValue = penalty.DataValue;
                penaltyDTO.FineDays = penalty.FineDays;
                penaltyDTO.Comments = penalty.PenaltyComments;
                penaltyDTO.PenaltyTypeCd = penalty.PenaltyTypeCD;
                penaltyDTO.SuspensionDays = penalty.SuspensionDays;
                penaltyDTO.Term = penalty.Term;
                penaltyDTO.Term_Desc = penalty.Term_Desc; 
                penaltyDTO.ACD = penalty.ACD;
                penaltyDTO.DemotedRank = penalty.DemotedRank;
                penaltyDTO.TimeServed= penalty.TimeServed;
                penaltyDTO.CombPen= penalty.CombPen;
                penaltyDTO.RandomTest= penalty.RandomTest;
                penaltyDTO.VacDays = penalty.VacDays;

                penaltyDTO.UniqueID = penalty.UniqueId;
                advocateDTO.Penaltys.Add(penaltyDTO);
            }
        }

        private void CreateCombinePenalties()
        {
            CombinedPenaltyDTO combPenaltyDTO;
            advocateDTO.CombPenaltys =new List<CombinedPenaltyDTO>();            
            foreach (CombinedPenalty combPenalty in this.combPenaltyList.List)
            {
                combPenaltyDTO = new CombinedPenaltyDTO();
                combPenaltyDTO.ParentCombPenID = combPenalty.ParentCombPenID;
                combPenaltyDTO.ChildCombPenID = combPenalty.ChildCombPenID;
                advocateDTO.CombPenaltys.Add(combPenaltyDTO);
            }
        }

        public bool SaveCombinePenalties(long ParentID, Int16 ChildCombPenID, Int16 CombPenID)
        {
            if (proxy == null)
                proxy = new Proxy();
            return proxy.SaveCombinePenalties(ParentID, ChildCombPenID, CombPenID);
        }
        public bool DeleteCombinePenalties(long ParentID)
        {
            if (proxy == null)
                proxy = new Proxy();
            return proxy.DeleteCombinePenalties(ParentID);
        }


        /// <summary>
        /// This method assembles the list of Advocate Complaint from the DTO.
        /// </summary>
        /// <param name="dsAdvocateComplaint"></param>
        private void AssembleAdvocateComplaint(List<ComplaintAdvocacyDTO> AdvocateComplaint)
        {           
                foreach (ComplaintAdvocacyDTO objAdvDTO in AdvocateComplaint)
                {                    
                    this.AdvocacyCaseId = objAdvDTO.AdvocacyCaseId;
                    this.ComplaintId = objAdvDTO.ComplaintId;
                    this.CaseClassificationCD = objAdvDTO.CaseClassificationcd;
                    this.OriginationSourceCD = objAdvDTO.OriginationSourcecd;
                    this.SpecialStatus_CD = objAdvDTO.SpecialStatus_cd;
                    this.TrialReturnStatusCD = objAdvDTO.TrialReturnStatuscd;
                    this.TrialReturnDate = objAdvDTO.TrialReturnDate;
                    this.ReviewStatusCD = objAdvDTO.ReviewStatuscd;
                    this.ReviewStatusDate = objAdvDTO.ReviewStatusDate;
                    this.ActionCD = objAdvDTO.Actioncd;
                    this.ActionDate = objAdvDTO.ActionDate;
                    this.IsClosed = objAdvDTO.isClosed;
                    this.ClosedDate = objAdvDTO.ClosedDate;
                    this.CobStatus_CD = objAdvDTO.COBStatus_cd;
                    this.CobDate = objAdvDTO.COBDate;
                    this.FinalStatusCD = objAdvDTO.FinalStatuscd;
                    this.CaseComment = objAdvDTO.CaseComment;
                    this.RecieveUserName = objAdvDTO.RecieveUserName;
                    this.RecieveDate = objAdvDTO.RecieveDate;
                    this.ModifiedUserName = objAdvDTO.ModifiedUserName;
                    this.ModifiedDate = objAdvDTO.ModifiedDate;
                    this.IsAvailable = objAdvDTO.IsAvailable;
                    this.IsProbation = objAdvDTO.IsProbation;
                }           
        }
        #endregion
    }
}
