﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to load complaint and Assemble Complaint History
    /// </summary>
    public class ComplaintHistoryList
    {
        Proxy proxy;

        public ComplaintHistoryList()
        {
            list = null;
        }

        private List<ComplaintHistory> list;

        public List<ComplaintHistory> List
        {
            get { return list; }
            set { list = value; }
        }
        /// <summary>
        /// This method loads the list of complaints based on Personnel ID and complain ID
        /// </summary>
        /// <param name="PersonnelID"></param>
        /// <param name="ComplaintID"></param>
        public void Load(Int64 personnelId, Int64 complaintId)
        {
            List<ComplaintHistoryDTO> historyDTO;            
                if (proxy == null)
                    proxy = new Proxy();
                historyDTO = proxy.LoadComplaintHistory(personnelId, complaintId);
                AssembleComplaintHistory(historyDTO);            
        }
        /// <summary>
        /// This method assembles the list of Complaint History from the DTO.
        /// </summary>
        /// <param name="dshistoryList"></param>
        private void AssembleComplaintHistory(List<ComplaintHistoryDTO> historyDTO)
        {
            if (list == null)
                list = new List<ComplaintHistory>();           
                ComplaintHistory history;
              
                foreach (ComplaintHistoryDTO historyList in historyDTO)
                    {
                        history = new ComplaintHistory();
                        history.ComplaintId = historyList.ComplaintID;
                        history.IndexNo = historyList.IndexNo;

                        history.ViolationDate = historyList.VoilationDate;
                            history.Violations = historyList.Voilations;
                            history.ViolationDescription = historyList.VoilationDesc;
                            if ((historyList.Penalty.Trim() == ",") || (historyList.Penalty.Trim() == ", ,") || (historyList.Penalty.Trim() == ", , ,") || (historyList.Penalty.Trim() == ", , , ,"))
                            {
                                history.Penalty = "";
                            }
                            else
                            {
                                history.Penalty = historyList.Penalty;
                            }
                            history.RoutingLocation = historyList.RoutingLocation;
                        list.Add(history);
                    }            
        }
    }
}
