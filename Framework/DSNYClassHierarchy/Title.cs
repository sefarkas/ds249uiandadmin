﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    ///  This class contains all private varaibles and public properties
    /// </summary>
    public class Title
    {
        public Title()
        {
            titleID = -1;
            titleCode = string.Empty;
            title = string.Empty;
        }
        /// <summary>
        /// Private variable
        /// </summary>
        private Int64 titleID;
        private String titleCode;
        private String title;

        /// <summary>
        /// Public property
        /// </summary>
        public Int64 TitleId
        {
            get { return titleID; }
            set { titleID = value; }
        }

        public String TitleCode
        {
            get { return titleCode; }
            set { titleCode = value; }
        }

        public String TitleDescription
        {
            get { return title; }
            set { title = value; }
        }
    }
}
