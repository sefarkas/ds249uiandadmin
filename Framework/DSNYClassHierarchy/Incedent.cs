﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to loads the Incident based on the charge ID,saves the Incident based on the insident ID
    /// method creates new Incident.
    /// </summary>
    public class Incedent
    {
        Proxy proxy;
        IncedentDTO incedentDTO;
        public Incedent()
        {
            uniqueID = 0;
            complaintID = -1;
            incedentDate = DateTime.MinValue;
            serialNo = 0;
            chargeID = -1;
            count = 0;
        }

        #region Private Variables

        /// <summary>
        /// Private Variables
        /// </summary>
        private Int16 uniqueID;
        private Int64 complaintID;
        private DateTime incedentDate;
        private int serialNo;
        private Int64 chargeID;
        private Int16 count;
        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int16 UniqueId
        {
            get { return uniqueID; }
            set { uniqueID = value; }
        }

        public Int64 ComplaintId
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int16 Count
        {
            get { return count; }
            set { count = value; }
        }

        public DateTime IncidentDate
        {
            get { return incedentDate; }
            set { incedentDate = value; }
        }

        public int SerialNo
        {
            get { return serialNo; }
            set { serialNo = value; }
        }

        public Int64 ChargeId
        {
            get { return chargeID; }
            set { chargeID = value; }
        }
        #endregion

        #region Public Method
        /// <summary>
        /// This methods loads the Incident based on the charge ID
        /// </summary>
        /// <param name="ChargeID"></param>
        /// <returns></returns>
            public Boolean Load(Int64 chargeId)
            {                
                    return true;               
            }
            /// <summary>
            /// This method saves the Incident based on the insident ID
            /// </summary>
            public void Save()
            {          
                   
                    if (proxy == null)
                        proxy = new Proxy();
                    CreateIncedent();
                    proxy.SaveIncident(incedentDTO);               
            }
        /// <summary>
            /// This method creates new Incident 
        /// </summary>
            private void CreateIncedent()
            {
                incedentDTO = new IncedentDTO();
                incedentDTO.ComplaintID = this.complaintID;
                incedentDTO.Count = this.count;
                incedentDTO.IncedentDate = this.incedentDate;
                incedentDTO.ChargeID = this.chargeID;
            }
        #endregion
    }
}
