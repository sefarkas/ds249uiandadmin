﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to add, remove, update and remove the witnesses
    /// load witnesses with respect to complaint id and Assemble those WitnessList and finally save the list.
    /// </summary>
    public class WitnessList
    {
        private Witness witness;

        Proxy proxy;

        public WitnessList()
        {
            list = new List<Witness>();
            newUniqueID = 1;
        }

        /// <summary>
        /// Private Variable
        /// </summary>
        private List<Witness> list;
        private Int16 newUniqueID;


        /// <summary>
        /// Public Property
        /// </summary>
        public List<Witness> List
        {
            get { return list; }
            set { list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return newUniqueID; }
            set { newUniqueID = value; }
        }

        public Witness Witness
        {
            get { return witness; }
            set { witness = value;}
        }

        /// <summary>
        /// This method returns true if the Witnesses are added sucessfully
        /// </summary>
        /// <returns></returns>
        public Boolean AddWitness()
        {
            witness = new Witness();
                if (list.Count == 0)
                {
                    witness.UniqueId = 1;
                    newUniqueID = 1;
                }
                else
                {
                    witness.UniqueId = ++newUniqueID;                                
                }
                list.Add(witness);
                return true;           
        }
        /// <summary>
        /// This method returns true if the Witnesses are removed sucessfully
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public Boolean RemoveWitness(Int64 uniqueId)
        {           
                witness = list.Find(delegate(Witness p) { return p.UniqueId == uniqueId; });
                list.Remove(witness);
                return true;           
        }
        /// <summary>
        /// This method returns true if the Witnesses are updated sucessfully
        /// </summary>
        /// <param name="WitnessID"></param>
        /// <returns></returns>
        public Boolean UpdateWitness(Int64 witnessId)
        {
             witness = list.Find(delegate(Witness p) { return p.WitnessId == witnessId; });
                return true;           
        }
        /// <summary>
        /// This method returns true if the Witnesses are loaded sucessfully based on the complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        public void Load(Int64 complaintId)
        {
            List<WitnessDto> listWitness;           
                if (proxy == null)
                    proxy = new Proxy();
                listWitness = proxy.LoadWitnesses(complaintId);
                AssembleWitnessList(listWitness);           
        }
        /// <summary>
        /// This method assembles the list of Witness List from the DTO.
        /// </summary>
        /// <param name="dsWitnessList"></param>
        public void AssembleWitnessList(List<WitnessDto> witnessDto)
        {
            if (list == null)
                list = new List<Witness>();
            
                Witness witness;
               
                Int16 i = 1;
                    foreach (WitnessDto lstWitness in witnessDto)
                    {
                        witness = new Witness();
                        witness.UniqueId = i;
                            witness.ComplaintId = lstWitness.ComplaintID;
                            witness.PersonnelName = lstWitness.PersonnelName;
                            witness.RefNo = lstWitness.RefNo;
                        list.Add(witness);
                        i++;
                    }
           
        }
        /// <summary>
        /// This method returns true if the Witnesses are saved sucessfully based on the complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool SaveList(Int64 complaintId)
        {            
                foreach (Witness witness in list)
                {
                    witness.Save(complaintId);
                }
                return true;
           
        }

    }
}
