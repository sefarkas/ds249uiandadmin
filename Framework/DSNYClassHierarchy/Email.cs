﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;


namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to sends email from IT Helpdesk 
    /// to the user with Login details.
    /// </summary>
    class Email
    {

        SMTPEmailSvc.EmailSvc emailSvc;

        public String AttachFileName { get; set; }
        public String[] ToAddress { get; set; }
        public String[] ToccAddress { get; set; }
        public String[] TobccAddress { get; set; }
        public String EmailBody { get; set; }
        
        /// <summary>
        /// This method sends email from IT Helpdesk to the user with Login details.
        /// </summary>
        public void SendEmail()
        {            
                emailSvc = new SMTPEmailSvc.EmailSvc();
                String url = ConfigurationManager.AppSettings["SMTPSvc"].ToString();
                String fromAddress = ConfigurationManager.AppSettings["FromEmail"].ToString();
                emailSvc.Url = url;
                emailSvc.Timeout = System.Threading.Timeout.Infinite;
                emailSvc.SendMail("Login Details for DS-249", EmailBody, fromAddress, ToAddress, ToccAddress, TobccAddress);
        }
    }
}
