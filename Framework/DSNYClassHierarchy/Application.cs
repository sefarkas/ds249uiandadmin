﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides boolean functionality to Load Application.
    /// </summary>
    public class Application
    {      
        public Application()
        {
            personalID = -1;
            applicationID = -1;
            applicationDesc = String.Empty;
            roleID = -1;
            roles = new RoleList();
            membership = new MembershipList();
            //roles = 0;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64 personalID;
        private Int64 applicationID;
        private String applicationDesc;
        private Int16 roleID;
        private RoleList roles;
        private MembershipList membership;

        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int64 PersonalId
        {
            get { return personalID; }
            set { personalID = value; }
        }

        public Int64 ApplicationID
        {
            get { return applicationID; }
            set { applicationID = value; }
        }

        public String ApplicationDesc
        {
            get { return applicationDesc; }
            set { applicationDesc = value; }
        }

        public RoleList Roles
        {
            get { return roles; }
            set { roles = value; }
        }

        public MembershipList Membership
        {
            get { return membership; }
            set { membership = value; }
        }

        public Int16 RoleID
        {
            get { return roleID; }
            set { roleID = value; }
        }


        public String RolesAsString
        {
            get
            {
                String tempRoles = string.Empty;
                if (roles.List.Count > 0)
                {
                    foreach (Role role in roles.List)
                    {
                        tempRoles += role.RoleId.ToString() + ",";
                    }
                    return tempRoles.Substring(0, tempRoles.Length - 1);
                }
                else
                {
                    return string.Empty;
                }
            }

        }

        #endregion

        #region Public Method

        public bool Load()
        {           
            return true;            
        }

        #endregion
    }
}
