﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Data;
using System.Collections;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functionality to 
    /// loads the users based on the personal ID,method returns true for authenticated users
    /// Gets the list of users based on personal ID,assembles the list of Users from the DTO
    ///  returns the new users and calls Createmembership method,method to returns the 
    /// new member created,  method to returns the Membership based on the group name(DS249, Advocate and BCAD)
    /// saves the User based on the boolean value, method to resets the password, method to deletes the user
    /// method to generate the new user ID, method to validate the user from the group(DS249, Advocate and BCAD)
    /// and method to return the string array by asigin respective roles to there members.
    /// </summary>

    public class User
    {

        Proxy proxy;
        UserDTO user;
        Membership usrMembership;

        public User()
        {
            personalId = -1;
            userId = String.Empty;
            password = String.Empty;
            emailId = String.Empty;
            userMembership = new MembershipList();
            lastLoginDateTime = DateTime.MinValue;
            lastLogin = DateTime.MinValue;           
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64 personalId;
        private String userId;
        private String password;
        private String emailId;
        private Boolean isTempPassword;
        private Boolean isAdmin;
        private MembershipList userMembership;
        private DateTime lastLoginDateTime;
        private DateTime lastLogin;
        private GroupName currentMembership;       
        #endregion

        #region Public Property

        /// <summary>
        /// Public Property
        /// </summary>
        public Int64 PersonalId
        {
            get { return personalId; }
            set { personalId = value; }
        }

        public String UserId
        {
            get { return userId; }
            set { userId = value; }
        }

        public String Password
        {
            get { return password; }
            set { password = value; }
        }

        public String EmailId
        {
            get { return emailId; }
            set { emailId = value; }
        }

        public Boolean IsTempPassword
        {
            get { return isTempPassword; }
            set { isTempPassword = value; }
        }

        public Boolean IsAdmin
        {
            get { return isAdmin; }
            set { isAdmin = value; }
        }

        public MembershipList UserMembership
        {
            get { return userMembership; }
            set { userMembership = value; }
        }

        public DateTime LastLoginDateTime
        {
            get { return lastLoginDateTime; }
            set { lastLoginDateTime = value; }
        }
        public DateTime LastLogin
        {
            get { return lastLogin; }
            set { lastLogin = value; }
        }
        public GroupName CurrentMembership
        {
            get { return currentMembership; }
            set { currentMembership = value; }
        }

        #endregion
        
        #region Public Method
        /// <summary>
        /// This method loads the users based on the personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>

        public bool Load(Int64 personalId)
        {
            if (proxy == null)
                proxy = new Proxy(true);
            List<UserDTO> usrDTO = proxy.LoadUser(personalId);
            AssembleUser(usrDTO);
            return true;
        }
        /// <summary>
        /// This method returns true for authenticated users
        /// </summary>
        /// <returns></returns>
        public Boolean AuthenticateUser()
        {

            List<UserDTO> listUser;
            if (proxy == null)
                proxy = new Proxy(true);

            listUser = proxy.AuthenticateUser(this.userId, this.password);            
            
            if (listUser != null)
            {
                if (listUser.Count > 0)
                {
                    AssembleUser(listUser);
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }


        #region PRIVATE METHODS
        /// <summary>
        /// This method returns the list of users based on personal ID
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        private List<Membership> GetMembershipList(Int64 PersonalID)
        {
            MembershipList membershipList = new MembershipList();
            membershipList.Load(PersonalID);
            return membershipList.List;
        }
        /// <summary>
        /// This method assembles the list of Users from the dataset.
        /// </summary>
        /// <param name="drUser"></param>
        private void AssembleUser(List<UserDTO> listUser)
        {      
               
                foreach (UserDTO ulist in listUser)
                {               
                    this.personalId = ulist.PersonalID;
                    this.UserId = ulist.UserID;
                    this.EmailId = ulist.RoleID;
                    this.IsTempPassword = ulist.IsTempPassword;
                    this.IsAdmin = ulist.IsAdmin;
                    this.userMembership = new MembershipList();
                    this.userMembership.List = GetMembershipList(this.personalId);               

                }           
            
        }


        /// <summary>
        /// This method returns the new users and calls Createmembership method
        /// </summary>
        /// <param name="isNew"></param>
        private void CreateUser(Boolean isNew)
        {
            user = new UserDTO();
            user.PersonalID = this.personalId;
            user.UserID = this.userId;
            user.Password = this.password;
            user.RoleID = this.EmailId;
            user.IsTempPassword = this.isTempPassword;
            user.IsAdmin = this.isAdmin;
            user.IsNew = isNew;
            CreateMembership();
        }

        /// <summary>
        /// This method returns the new member created
        /// </summary>
        private void CreateMembership()
        {
            MembershipDTO membershipDTO;
            RoleDTO roleDTO;
            user.UserMembership = new List<MembershipDTO>();
            if (this.UserMembership != null)
            {
                foreach (Membership membership in this.UserMembership.List)
                {
                    membershipDTO = new MembershipDTO();
                    membershipDTO.MembershipID = membership.MembershipId;
                    membershipDTO.PersonalID = membership.PersonalId;
                    membershipDTO.Roles = new List<RoleDTO>();
                    foreach (Role role in membership.Roles.List)
                    {
                        roleDTO = new RoleDTO();
                        roleDTO.MembershipID = role.MembershipId;
                        roleDTO.PersonalID = role.PersonalId;
                        roleDTO.RoleID = role.RoleId;
                        membershipDTO.Roles.Add(roleDTO);
                    }
                    user.UserMembership.Add(membershipDTO);
                }
            }
        }
        /// <summary>
        /// This method returns the Membership based on the group name(DS249, Advocate and BCAD)
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        private Membership GetMembership(GroupName groupName)
        {
            switch (groupName)
            {
                case GroupName.DS249:
                    return this.userMembership.List.Find(delegate(Membership p) { return p.MembershipDescription == "DS249"; });
                case GroupName.ADVOCATE:
                    return this.userMembership.List.Find(delegate(Membership p) { return p.MembershipDescription == "ADVOCATE"; });
                case GroupName.BCAD:
                    return this.userMembership.List.Find(delegate(Membership p) { return p.MembershipDescription == "BCAD"; });
                case GroupName.ADMIN:
                    return this.userMembership.List.Find(delegate(Membership p) { return p.MembershipDescription == "ADMIN"; });
                case GroupName.MEDICAL:
                    return this.userMembership.List.Find(delegate(Membership p) { return p.MembershipDescription == "MEDICAL"; });
                case GroupName.EMPLOYEE:
                    return this.userMembership.List.Find(delegate(Membership p) { return p.MembershipDescription == "EMPLOYEE"; });
                default:
                    return null;
            }
        }

        #endregion
        /// <summary>
        /// This method saves the User based on the boolean value
        /// </summary>
        /// <param name="isNew"></param>
        /// <returns></returns>
       public String Save(Boolean isNew)
        {           
            CreateUser(isNew);            
            proxy = new Proxy();           
            return proxy.SaveUser(user);
        }
        public String SaveUserPwdViaDS249Admin()
        {
            proxy = new Proxy();
            return proxy.SaveUser( user );
        }

        public Boolean HistoryMemberShipRoles(Int64 userId)
        {
            proxy = new Proxy();
            return proxy.HistoryMemberShipRoles(user, userId);
        }
        /// <summary>
        /// This method resets the password
        /// </summary>
        public void ResetPassword()
        {
            user = new UserDTO();
            user.UserID = this.userId;
            user.Password = this.password;
            user.IsTempPassword = this.isTempPassword;           
                proxy = new Proxy();
            proxy.ResetPassword(user);
        }
        /// <summary>
        /// This method deletes the user
        /// </summary>
        public void Delete()
        {
            if (proxy == null)
                proxy = new Proxy();
            proxy.DeleteUser(this.personalId);
        }
        /// <summary>
        /// This method will validate the user email ID
        /// </summary>
        public void ValidateUserEmail()
        {
            if (proxy == null)
                proxy = new Proxy();
            UserDTO usr = proxy.ValidateUserEmail(this.emailId);            
            this.userId = usr.UserID;
            this.isAdmin = usr.IsAdmin;
        }
        /// <summary>
        /// This method will generate the new user ID
        /// </summary>
        /// <returns></returns>
        public String GetNewUserId()
        {
            if (proxy == null)
                proxy = new Proxy();
            return proxy.GetNewUserID(this.personalId);
        }
        /// <summary>
        /// This method will validate the user from the group(DS249, Advocate and BCAD)
        /// </summary>
        /// <param name="groupName"></param>
        /// <returns></returns>
        public Boolean IsInMemberShip(GroupName groupName)
        {
            
            switch (groupName)
            { 
                case GroupName.DS249:
                    usrMembership = GetMembership(groupName);
                    return usrMembership == null? false: true;                                                        
                case GroupName.ADVOCATE:
                    usrMembership = GetMembership(groupName);
                    return usrMembership == null? false: true;                                                        
                case GroupName.BCAD:
                    usrMembership = GetMembership(groupName);
                    return usrMembership == null ? false : true;
                case GroupName.ADMIN:
                    usrMembership = GetMembership(groupName);
                    return usrMembership == null ? false : true;
                case GroupName.MEDICAL:
                    usrMembership = GetMembership(groupName);
                    return usrMembership == null ? false : true;
                case GroupName.EMPLOYEE:
                    usrMembership = GetMembership(groupName);
                    return usrMembership == null ? false : true;                
                default:
                    return false;
            }
        }

      
        /// <summary>
        /// This method will return the string array by asigin respective roles to there members.
        /// </summary>
        /// <returns></returns>
        public string[] MembershipRoles()
        {
             ArrayList aryRoles = new ArrayList();            
            usrMembership = GetMembership(currentMembership);
            RoleList roles = usrMembership.Roles;
            foreach (Role role in roles.List)
            {
                aryRoles.Add(role.RoleDescription);
            }
            return (string[])aryRoles.ToArray(typeof(string));            
        }      

        #endregion
    }
}
