﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace DSNY.DSNYCP.ClassHierarchy.Utilities
{
    /// <summary>
    /// Common methods for formatting data (e.g. employee full name).
    /// </summary>
    public sealed class CommonFormats
    {
        #region Name Formats

        /// <summary>
        /// Formats an employee full name to "last, first middle").
        /// </summary>
        /// <param name="first">Person's first name</param>
        /// <param name="last">Person's last name</param>
        /// <param name="middle">Person's middle name</param>
        /// <returns>Employee's full name.</returns>
        public static String FormatFullName(String first, String last, String middle)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.IsNullOrEmpty(last) ? "" : last.Trim() + ", ");
            sb.Append(String.IsNullOrEmpty(first) ? "" : first.Trim());
            sb.Append(" ");
            sb.Append(String.IsNullOrEmpty(middle) ? "" : middle.Trim());

            return sb.ToString().Trim();
        }

        /// <summary>
        /// Formats a location.
        /// </summary>
        /// <param name="first">Employee's pay code</param>
        /// <param name="last">Employee's pay description</param>
        /// <returns>Employee's location.</returns>
        public static String FormatLocation(String payCode, String payDescription)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.IsNullOrEmpty(payCode) ? "" : payCode.Trim() + " - ");
            sb.Append(String.IsNullOrEmpty(payDescription) ? "" : payDescription.Trim());

            return sb.ToString().Trim();
        }
        public static bool IsDate(string strDate)
        {
                  
            try
            {
                DateTime myDateTime = DateTime.Parse(strDate);
                return true;
            }
            catch (FormatException)
            {               
                return false;
            }
           
        }
        #endregion Name Formats

        #region Private Constructor

        /// <summary>
        /// Required so compiler does not automatically add a default constructor.
        /// </summary>
        private CommonFormats() { }

        #endregion Private Constructor
    }
}