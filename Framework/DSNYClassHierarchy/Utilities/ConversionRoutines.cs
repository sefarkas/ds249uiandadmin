﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy.Utilities
{
    /// <summary>
    /// Common conversion methods (e.g. database to C# and vice-versa)/
    /// </summary>
    public sealed class ConversionRoutines
    {
        #region DB Conversions Methods

        /// <summary>
        /// Converts a C# DateTime to a String that is consistent with Database Datetime.
        /// </summary>
        /// <param name="dt">C# DateTime</param>
        /// <param name="format">String format</param>
        /// <returns>A DateTime in the form of a String and is consistent with DB Datetime data type</returns>
        public static String DBToString(DateTime dt, String format)
        {
            if (dt == DateTime.MinValue)
                return ""; // minvalue is too low for database DateTime
            else
                return String.Format(format, dt);
        }

        /// <summary>
        /// Converts a C# TimeSpan to a String that is consistent with Database time.
        /// </summary>
        /// <param name="dt">C# TimeSpan</param>
        /// <param name="format">String format</param>
        /// <returns>A TimeSpan in the form of a String and is consistent with DB time data type</returns>
        public static String DBToString(TimeSpan t, String format)
        {
            if (t == TimeSpan.MinValue)
                return ""; // minvalue is too low for database DateTime
            else
            {
                DateTime dt = new DateTime(DateTime.MinValue.Year, DateTime.MinValue.Month, DateTime.MinValue.Day, t.Hours, t.Minutes, 0);
                return String.Format(format, dt);
            }
        }

        #endregion DB Conversions Methods

        #region General Conversion Methods

        /// <summary>
        /// Converts a String to a DateTime.
        /// </summary>
        /// <param name="s">String to convert</param>
        /// <returns>A DateTime that is consistent with the String value.  If the String is not a valid DateTime, DateTime.MinValue is returned.</returns>
        public static DateTime ToDateTime(String s)
        {
            DateTime dt;           
                dt = Convert.ToDateTime(s);                       
            return dt;
        }

        /// <summary>
        /// Converts a String to a int?
        /// </summary>
        /// <param name="o">String to be converted</param>
        /// <returns><i>o</i> as int or null if <i>o</i> is null or a non-integer. </returns>
        public static int? ToInt(String str)
        {
             return Convert.ToInt32(str);           
        }

        /// <summary>
        /// Converts a String to a TimeSpan.
        /// </summary>
        /// <param name="s">String to convert</param>
        /// <returns>A TimeSpan that is consistent with the String value.  If the String is not a valid TimeSpan, TimeSpan.MinValue is returned.</returns>
        public static TimeSpan ToTimeSpan(String str)
        {            
            TimeSpan t;           
                DateTime dt = Convert.ToDateTime(str);
                t = new TimeSpan(dt.Hour, dt.Minute, dt.Second);
            return t;
        }

        #endregion General Conversion Methods

        #region Private Constructor

        /// <summary>
        /// Required so compiler does not automatically add a default constructor.
        /// </summary>
        private ConversionRoutines() { }

        #endregion Private Constructor
    }
}
