﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    public class DS249Enums
    {
        public  enum CaseStatues
        {
            Open=96,
            Closed = 97,
            None=0
        }
        public enum NonPenaltyResults
        {
            None=0,
            VOID_VOID = 134
        }

        public enum HearingType
        {
            //CANCEL Hearing Type
            CANCEL = 185
        }

        public enum ComplaintType
        {            
            ReopenComplaint,
            None,          
        }       

    }
}
