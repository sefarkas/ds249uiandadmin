﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functinality to add new DS249 complaint, Update Complainant, Remove Complainant, Laod complaints and Assemble Complainant List
    /// </summary>
    public class ComplainantList
    {

        Complainant complainant;
        Proxy proxy;

        public ComplainantList()
        {
            list = new List<Complainant>();
            newUniqueID = 1;
        }

        /// <summary>
        /// Private Variable
        /// </summary>
        private List<Complainant> list;
        private Int16 newUniqueID;

        /// <summary>
        /// Public Property
        /// </summary>
        public List<Complainant> List
        {
            get { return list;}
            set { list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return newUniqueID; }
            set { newUniqueID = value; }

        }

        /// <summary>
        /// This method returns true if the new DS249 complaint is sucessfully added
        /// </summary>
        /// <returns></returns>

        public Boolean AddComplainant()
        {           
                complainant = new Complainant();
                if (list.Count == 0)
                {
                    complainant.UniqueId = 1;
                    newUniqueID = 1;
                }
                else
                {
                    complainant.UniqueId = ++newUniqueID;
                }
                list.Add(complainant);
                return true;            
        }
        /// <summary>
        /// This method returns true if the new DS249 complaint is sucessfully removed
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public Boolean RemoveComplainant(Int64 UniqueID)
        {           
                complainant = list.Find(delegate(Complainant p) { return p.UniqueId == UniqueID; });
                list.Remove(complainant);
                return true;           
        }
        /// <summary>
        /// This method returns true if the new DS249 complaint is sucessfully updated
        /// </summary>
        /// <param name="ComplainantID"></param>
        /// <returns></returns>
        public Boolean UpdateComplainant(Int64 ComplainantID)
        {            
                complainant = list.Find(delegate(Complainant p) { return p.ComplainantId == ComplainantID; });
                return true;            
        }
        /// <summary>
        /// This method loads the list of DS249 complaint based on the complaint ID.
        /// </summary>
        /// <param name="ComplaintID"></param>
        public void Load(Int64 complaintId)
        {
            List<ComplainantDTO> listComplainants;           
                if (proxy == null)
                    proxy = new Proxy();
                listComplainants = proxy.LoadComplainants(complaintId);
                AssembleComplainantList(listComplainants);            
        }
        /// <summary>
        /// This method assembles the list of Assemble Complainant List from the DTO.
        /// </summary>
        /// <param name="dsComplainants"></param>
        public void AssembleComplainantList(List<ComplainantDTO> complainantsDto)
        {
            if (list == null)
                list = new List<Complainant>();           
                Complainant complainant;                
                Int16 i = 1;

                    foreach (ComplainantDTO clist in complainantsDto)
                    {
                        complainant = new Complainant();
                        complainant.UniqueId = i;
                        complainant.ComplainantId = clist.ComplainantID;
                        complainant.ComplainantName = clist.ComplainantName;

                        complainant.RefNo = clist.RefNo;
                        complainant.TitleId = clist.TitleID;
                        complainant.LocationId = clist.LocationID;
                        complainant.Statement = clist.Statement;
                        complainant.OnDate = clist.OnDate;
                        complainant.Text = clist.Text;
                        complainant.ThisDate = clist.ThisDate;
                        complainant.Category = clist.Category;
                        complainant.ReportedDate = clist.ReportedDate;
                        complainant.FromLocation = clist.FromLocation;
                        complainant.ReportedTime = clist.ReportedTime; ;
                        complainant.ReportingTime = clist.ReportingTime;
                        complainant.TelephoneNo = clist.TelephoneNo;
                        complainant.LocationName = clist.LocationName;
                        complainant.Title = clist.Title;

                        list.Add(complainant);
                        i++;
                    }            
            }
        
        /// <summary>
        /// This method saves the DS249 complaints based on the complaint ID.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool Save(Int64 complaintId)
        {
             if (proxy == null)
                    proxy = new Proxy();
                foreach (Complainant complainant in list)
                {
                    complainant.ComplaintId = complaintId;
                    complainant.Save();
                }
                return true;         

        }
}
    
}
