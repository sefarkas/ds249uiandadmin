﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functinality to load the code based on the code ID
    /// </summary>
    public class Code
    {
        public Code()
        {
            codeCategoryID = 0;
            categoryName = String.Empty;
            codeID = 0;
            codeName = String.Empty;
            codeDescription = String.Empty;
            itemOrder = 0;
            isActive = false;
            valueDataType = string.Empty;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int16 codeCategoryID;
        private String categoryName;
        private Int16 codeID;
        private String codeName;
        private String codeDescription;
        private Int16 itemOrder;
        private Boolean isActive;
        private string valueDataType;

        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int16 CodeCategoryId
        {
            get { return codeCategoryID; }
            set { codeCategoryID = value; }
        }

        public String CategoryName
        {
            get { return categoryName; }
            set { categoryName = value; }
        }

        public String ValueDataType
        {
            get { return valueDataType; }
            set { valueDataType = value; }
        }

        public Int16 CodeId
        {
            get { return codeID; }
            set { codeID = value; }
        }

        public String CodeName
        {
            get { return codeName; }
            set { codeName = value; }
        }

        public String CodeDescription
        {
            get { return codeDescription; }
            set { codeDescription = value; }
        }

        public Int16 ItemOrder
        {
            get { return itemOrder; }
            set { itemOrder = value; }
        }

        public Boolean IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// This method loads the list of code based on the code ID
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public bool Load(Int16 id)
        {          
                return true;           
        }
       
        #endregion



    }
}
