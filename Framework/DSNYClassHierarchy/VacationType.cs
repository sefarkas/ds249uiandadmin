﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class contains all private varaibles and public properties
    /// </summary>
    [Serializable]
    public class VacationType
    {
        /// <summary>
        /// Private variables
        /// </summary>
        private long m_EmployeeVacationTypeId;
        private string m_EmployeeVacation;
        /// <summary>
        /// Public property
        /// </summary>
        public long EmployeeVacationTypeId
        {
            get { return m_EmployeeVacationTypeId; }
            set { m_EmployeeVacationTypeId = value; }
        }
        public string  EmployeeVacation
        {
            get { return m_EmployeeVacation; }
            set { m_EmployeeVacation = value; }
        }

    }   
}
