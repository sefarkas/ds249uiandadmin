﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;
namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to saves the User based on UserName,LastName, NTID and email,
    /// returns the list of workflow application, deletes the user based on the User id ,
    /// deletes the user group based on the group id,saves the user group based on GroupName, ApplicationId and parentGroupId 
    /// saves the application based on application name,retrun the Authorized Requests based on groipId and AppId
    /// deletes the application based on application ID, list of all groups, return the list of all groups,
    /// return the list of all application groups details,return the list of all user details,
    /// return the final approval based on group id and application ID.
    /// </summary>
    
    public class Workflow
    {
        WorkflowProxy WorkflowProxy; 

        public  Workflow()
        {
            WorkflowProxy = new WorkflowProxy();
        }
        /// <summary>
        /// This method saves the User based on UserName,LastName, NTID and email
        /// </summary>
        /// <param name="UserName"></param>
        /// <param name="GroupId"></param>
        /// <param name="LastName"></param>
        /// <param name="NTID"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        public int SaveUser(string userName, int groupId,string lastName,string ntid,string email )
        {
            return WorkflowProxy.SaveUser(userName, groupId,lastName,ntid,email);

        }
        /// <summary>
        /// This method returns the list of workflow application
        /// </summary>
        /// <returns></returns>
        public List<WorkflowApplicationDTO> GetAllApplications()
        {
            return WorkflowProxy.GetAllApplications();
        }
        /// <summary>
        /// This method deletes the user based on the User id 
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public bool DeleteUser(int userId)
        {
            return WorkflowProxy.DeleteUser(userId);
        }
        /// <summary>
        /// This method deletes the user group based on the group id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public bool DeleteGroup(int groupId)
        {
            return WorkflowProxy.DeleteGroup(groupId);
        }
        /// <summary>
        /// This method saves the user group based on GroupName, ApplicationId and parentGroupId 
        /// </summary>
        /// <param name="GroupName"></param>
        /// <param name="ApplicationId"></param>
        /// <param name="parentGroupId"></param>
        /// <returns></returns>
        public int SaveGroup(string groupName, int applicationId, int parentGroupId)
        {
            return WorkflowProxy.SaveGroup(groupName, applicationId, parentGroupId);
        }
        /// <summary>
        /// This method saves the application based on application name.
        /// </summary>
        /// <param name="ApplicationName"></param>
        /// <returns></returns>
        public int SaveApplication(string applicationName)
        {
            return WorkflowProxy.SaveApplication(applicationName);

        }
        /// <summary>
        /// This method deletes the application based on application ID.
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public bool DeleteApplication(int applicationId)
        {
            return WorkflowProxy.DeleteApplication(applicationId);
        }
        /// <summary>
        /// This method will return the list of all groups
        /// </summary>
        /// <returns></returns>

        public List<WorkflowGroupsDTO> GetAllGroups()
        {
            return WorkflowProxy.GetAllGroups();
        }
        /// <summary>
        /// This method will return the list of all groups
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
        public List<WorkflowGroupsDTO> GetAllGroups(int applicationId)
        {
            return WorkflowProxy.GetAllGroups(applicationId);
        }
        /// <summary>
        /// This method will return the list of all application groups details
        /// </summary>
        /// <returns></returns>
        public List<WorkflowGroupsDTO> GetApplicationGroupDetails()
        {
            return WorkflowProxy.GetApplicationGroupDetails();
        }
        /// <summary>
        /// This method will return the list of all user details
        /// </summary>
        /// <returns></returns>
        public List<WorkFlowUser> GetUserDetails()
        {
            return WorkflowProxy.GetUserDetails();
        }
        /// <summary>
        /// This method will return the final approval based on group id and application ID
        /// </summary>
        /// <param name="GroupId"></param>
        /// <param name="applicationID"></param>
        /// <returns></returns>
        public bool IsFinalApproval(int groupId, int applicationId)
        {
            return WorkflowProxy.IsFinalApproval(groupId, applicationId);

        }
        /// <summary>
        /// This method will retrun the Authorized Requests based on groipId and AppId
        /// </summary>
        /// <param name="groipId"></param>
        /// <param name="AppId"></param>
        /// <returns></returns>
        public List<int> GetAuthorizedRequests(int groupId, int applicationId)
        {
            return WorkflowProxy.GetAuthorizedRequests(groupId, applicationId);
        }
    }
}
