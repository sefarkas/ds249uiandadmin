﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class will provide functionality to load associated complaint list, assemble associated complaint list
    /// add associated complaint and remove associated complaint
    /// </summary>
    public class AdvocateAssociatedComplaintList
    {
        Proxy proxy;
        AdvocateAssociatedComplaint associatedComplaint;

        public AdvocateAssociatedComplaintList()
        {
            list = new List<AdvocateAssociatedComplaint>();
            newUniqueID = 0;
        }
        /// <summary>
        /// Private Variable
        /// </summary>
        List<AdvocateAssociatedComplaint> list;
        Int16 newUniqueID;

        /// <summary>
        /// Public Property
        /// </summary>
        public List<AdvocateAssociatedComplaint> List
        {
            get { return list; }
            set { list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return newUniqueID; }
            set { newUniqueID = value; }
        }

        /// <summary>
        /// This method Loads the advocate complaints based on the advocate case number.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        public void Load(Int64 advocacyCaseId)
        {
            List<AdvocateAssociatedComplaintDTO> advAssociatedComplaints = new List<AdvocateAssociatedComplaintDTO>();           
                if (proxy == null)
                    proxy = new Proxy();
                advAssociatedComplaints = proxy.LoadAssociatedComplaints(advocacyCaseId);
                AssembleAssociatedComplaintList(advAssociatedComplaints);          
        }
        /// <summary>
        /// This method assembles the the list of advocate complaints from the DTO.
        /// </summary>
        /// <param name="dsAssociatedComplaints"></param>
        public void AssembleAssociatedComplaintList(List<AdvocateAssociatedComplaintDTO> advAssociatedComplaints)
        {
            if (list == null)
                list = new List<AdvocateAssociatedComplaint>();          
                AdvocateAssociatedComplaint complaint;
                Int16 i = 1;
                foreach (AdvocateAssociatedComplaintDTO advAssociatecComplaint in advAssociatedComplaints)
                {
                    complaint = new AdvocateAssociatedComplaint();
                    complaint.UniqueId = i;
                    complaint.AssociatedId = advAssociatecComplaint.AdvocacyCaseID;
                    complaint.AdvocacyCaseId = advAssociatecComplaint.AdvocacyCaseID;
                    complaint.AssociatedComplaintId = advAssociatecComplaint.AsscociatedCompaintID;
                    
                    list.Add(complaint);
                    i++;
                }               
           
        }
        /// <summary>
        /// This method assembles the the list of advocate complaints history from the DTO.
        /// </summary>
        /// <param name="dsAssociatedComplaints"></param>
        public void AssembleAssociatedComplaintHistoryList(List<AdvocateAssociatedComplaintDTO> advAssociatedComplaints)
        {
            if (list == null)
                list = new List<AdvocateAssociatedComplaint>();       

                AdvocateAssociatedComplaint complaint;
                Int16 i = 1;
                foreach (AdvocateAssociatedComplaintDTO advAssociatecComplaint in advAssociatedComplaints)
                {
                    complaint = new AdvocateAssociatedComplaint();
                   
                    complaint.AssociatedId = advAssociatecComplaint.AdvocacyCaseID;
                    complaint.AdvocacyCaseId = advAssociatecComplaint.AdvocacyCaseID;
                    complaint.AssociatedComplaintId = advAssociatecComplaint.AsscociatedCompaintID;

                    list.Add(complaint);
                    i++;
                }
            
        }
        /// <summary>
        /// This method returns true if the complaints are succesfully assembled.
        /// </summary>
        /// <returns></returns>
        public Boolean AddAssociatedComplaint()
        {
              associatedComplaint = new AdvocateAssociatedComplaint();

                if (list.Count == 0)
                {
                    associatedComplaint.UniqueId = 1;
                    newUniqueID = 1;
                }
                else
                {
                    associatedComplaint.UniqueId = ++newUniqueID;                             
                }
                list.Add(associatedComplaint);
                return true;          
        }
        /// <summary>
        /// This method accepts the advocate ID and returns true if the complaints are succesfully removed.
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public Boolean RemoveAssociatedComplaint(Int64 UniqueID)
        {            
                associatedComplaint = list.Find(delegate(AdvocateAssociatedComplaint p) { return p.UniqueId == UniqueID; });
                list.Remove(associatedComplaint);
                return true;            
        }
    }
}
