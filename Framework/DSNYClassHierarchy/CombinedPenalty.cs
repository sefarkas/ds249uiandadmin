﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    public class CombinedPenalty
    {
        Proxy proxy;
        CombinedPenaltyDTO CombPenaltyDTO;

        public CombinedPenalty()
        {
            _parentCombPenID = -1;
            _childCombPenID = -1;
            _combPenID = -1;
            _isActive = -1;
        }

              private Int64 _parentCombPenID;
              private Int16 _childCombPenID;
              private Int16 _combPenID;
              private Int16 _isActive;


              public Int64 ParentCombPenID
              {
                  get { return _parentCombPenID; }
                  set { _parentCombPenID = value; }
              }
              public Int16 ChildCombPenID
              {
                  get { return _childCombPenID; }
                  set { _childCombPenID = value; }
              }
              public Int16 IsActive
              {
                  get { return _isActive; }
                  set { _isActive = value; }
              }
              public Int16 CombPenID
              {
                  get { return _combPenID; }
                  set { _combPenID = value; }
              }
              private Int16 _uniqueID;
              public Int16 UniqueId
              {
                  get { return _uniqueID; }
                  set { _uniqueID = value; }
              }
              /// <summary>
              /// This method saves the new advocate penalty created
              /// </summary>
              /// <returns></returns>
              public bool Save()
              {
                  if (proxy == null)
                      proxy = new Proxy();
                  CreateCombinedPenalty();

                  return true;
              }

              /// <summary>
              /// This method creates the new advocate penalty.
              /// </summary>
              private void CreateCombinedPenalty()
              {
                  CombPenaltyDTO = new CombinedPenaltyDTO();
                  CombPenaltyDTO.ParentCombPenID = this.ParentCombPenID;
                  CombPenaltyDTO.ChildCombPenID = this.ChildCombPenID;
                  CombPenaltyDTO.CombPenID = this.CombPenID;
                  
              }
    }
}
