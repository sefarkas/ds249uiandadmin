﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class will provide functiionality to load, assemble appeals, 
    /// addAppeal and remove appeal
    /// </summary>
    public class AdvocateAppealList
    {
        Proxy proxy;
        AdvocateAppeal appeal;

        public AdvocateAppealList()
        {
            _list = new List<AdvocateAppeal>();
            _newUniqueID = 0;
        }

        private List<AdvocateAppeal> _list;
        private Int16 _newUniqueID;

        /// <summary>
        /// public Property
        /// </summary>
        public List<AdvocateAppeal> List
        {
            get { return _list; }
            set { _list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return _newUniqueID; }
            set { _newUniqueID = value; }
        }

        /// <summary>
        /// This method loads the advocate appeal based on the advocate complaint ID.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        public void Load(Int64 advocacyCaseId)
        {
            List<AdvocateAppealDTO> advAppeals = new List<AdvocateAppealDTO>();           
                if (proxy == null)
                    proxy = new Proxy();
                advAppeals = proxy.LoadAdvocateAppealList(advocacyCaseId);
                AssembleAppeals(advAppeals);           
        }
        /// <summary>
        /// This method assembles the list of appeals from the DTO.
        /// </summary>
        /// <param name="dsAppeals"></param>
        public void AssembleAppeals(List<AdvocateAppealDTO> advAppeals)
        {
            if (_list == null)
                _list = new List<AdvocateAppeal>();
           
                AdvocateAppeal appeal;

                Int16 i = 1;
                foreach (AdvocateAppealDTO advAppeal in advAppeals)
                {
                    appeal = new AdvocateAppeal();
                    appeal.UniqueId = i;

                    if(!string.IsNullOrEmpty(advAppeal.AdvocateAppealID.ToString()))
                        appeal.AdvocateAppealId = advAppeal.AdvocateAppealID;

                    if (!string.IsNullOrEmpty(advAppeal.AdvocacyCaseID.ToString())) 
                        appeal.AdvocacyCaseId = advAppeal.AdvocacyCaseID;

                    if (!string.IsNullOrEmpty(advAppeal.AppealDt.ToString())) 
                        appeal.AppealDate = advAppeal.AppealDt;

                    if (!string.IsNullOrEmpty(advAppeal.AppealresultCd.ToString())) 
                        appeal.AppealresultCD = advAppeal.AppealresultCd;

                    if (!string.IsNullOrEmpty(advAppeal.AppealComments)) 
                        appeal.AppealComments = advAppeal.AppealComments;

                    if (!string.IsNullOrEmpty(advAppeal.AppealCd.ToString())) 
                        appeal.AppealCD = advAppeal.AppealCd;

                    if (!string.IsNullOrEmpty(advAppeal.ModifiedBy.ToString())) 
                        appeal.ModifiedBy = advAppeal.ModifiedBy;

                    if (!string.IsNullOrEmpty(advAppeal.ModifiedDt.ToString())) 
                        appeal.ModifiedDate = advAppeal.ModifiedDt;                   
                    
                    _list.Add(appeal);
                    i++;
                }              
            
        }

        /// <summary>
        /// This method returns true if the appeal is sucessfully added. 
        /// </summary>
        /// <returns></returns>
        public Boolean AddAppeal()
        {
              appeal = new AdvocateAppeal();

                if (_list.Count == 0)
                {
                    appeal.UniqueId = 1;
                    _newUniqueID = 1;
                }
                else
                {
                    appeal.UniqueId = ++_newUniqueID;                               
                }
                _list.Add(appeal);
                return true;           
        }
        /// <summary>
        /// his method returns true if the appeal is sucessfully removed based on the advocate ID. 
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public Boolean RemoveAppeal(Int64 uniqueId)
        {           
                appeal = _list.Find(delegate(AdvocateAppeal p) { return p.UniqueId == uniqueId; });
                _list.Remove(appeal);
                return true;            
        }
    }
}
