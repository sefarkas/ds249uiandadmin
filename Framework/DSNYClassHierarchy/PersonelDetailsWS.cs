﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.Proxys;
namespace DSNY.DSNYCP.ClassHierarchy
{
 public class PersonelDetailsWS
    {

     public List<PersonnelWs> GetPersonelDataWS(int pageNo, int pageSize, string firstName, string middleName, string lastName, string referenceNo,string ssnNo)
     {         
        
             Proxy  proxy = new Proxy();

             List<PersonnelWSDTO> pDTO = new List<PersonnelWSDTO>();           

             pDTO = proxy.GetPersonelDataWS(pageNo, pageSize, firstName, middleName, lastName, referenceNo,ssnNo);
         
             List<PersonnelWs> pws = new List<PersonnelWs>();

             foreach (PersonnelWSDTO pList in pDTO)
             {
                 PersonnelWs pListCH = new PersonnelWs();
                 pListCH.FirstName = pList.FirstName;
                 pListCH.MiddleName = pList.MiddleName;
                 pListCH.LastName = pList.LastName;
                 pListCH.ReferenceNo = pList.ReferenceNo;
                 pListCH.SSNo = pList.SsnNo;
                 pws.Add(pListCH);
             }
             return pws;         
        
     }

   
    }
 public class PersonnelWs
 {
     public int PageNo { get; set; }
     public int PageSize { get; set; }
     public string FirstName { get; set; }
     public string MiddleName { get; set; }
     public string LastName { get; set; }
     public string ReferenceNo { get; set; }
     public string SSNo { get; set; }
 }
}
