﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
  /// <summary>
    /// This class provide functionality to load charge list, load based on search criteria, Assemble Charge List to DTO
  /// </summary>
   public class ChargesList
    {
       Proxy proxy;

       public ChargesList()
       {
           chargeList = null;
       }

       //Private Variable
       private List<Charge> chargeList;

       //Public Property
       public List<Charge> ChargeList
       {
           get { return chargeList; }
           set { chargeList = value; }
       }

       

       /// <summary>
       /// This method will Search the list based on ChargeCode using wild card character(%).
       /// </summary>
       /// <param name="ChargeCode"></param>
       public void Load(String chargeCode)
       {
           List<ChargeDTO> chargesDTO;          
               if (proxy == null)
                   proxy = new Proxy();
               chargesDTO = proxy.LoadCharges(chargeCode);
               AssembleChargeList(chargesDTO);          
       }
       /// <summary>
       /// This method will Search the list based on ChargeCode
       /// </summary>
       public void Load()
       {
           List<ChargesWithoutParametersDTO> chargesDTO;          
               if (proxy == null)
                   proxy = new Proxy();
               chargesDTO = proxy.LoadCharges();
               AssembleChargeListWithoutParameters(chargesDTO);          
       }
       /// <summary>
       /// This method assembles the list of charges from the DTO.
       /// </summary>
       /// <param name="dsCharges"></param>
       private void AssembleChargeList(List<ChargeDTO> dsCharges)
       {
           if (chargeList == null)
               chargeList = new List<Charge>();             
          
               Charge charge;
               foreach (ChargeDTO cList in dsCharges)
                   {
                       charge = new Charge();
                       charge.ChargeTypeId = cList.ChargeTypeID;
                       charge.ChargeCode = cList.ReferenceNumber;
                       charge.ChargeDescription = cList.ChargeDescription;
                       chargeList.Add(charge);
                   }           
       }


       private void AssembleChargeListWithoutParameters(List<ChargesWithoutParametersDTO> dsCharges)
       {
           if (chargeList == null)
               chargeList = new List<Charge>();
           
               Charge charge;

               foreach (ChargesWithoutParametersDTO cList in dsCharges)
               {
                   charge = new Charge();
                   charge.ChargeTypeId = cList.ChargeTypeID;
                   charge.ChargeCode = cList.ReferenceNumber;
                   charge.ChargeDescription = cList.ChargeDescription;
                   chargeList.Add(charge);
               }          
       }


    }
}
