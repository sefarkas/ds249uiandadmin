﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to get full name, Initial and LastName, Author Initial and LastName, loads the list of complaints based on the complain ID,
    /// Assemble Complaint,Get Witness List, Get Complainants,Get Violations,Get BCAD Complaint,Get Advocate Complaint,
    /// Get Complaint History, Save based on resource, Create Complaint, CreateWitnesses,Create Violations, Update ComplaintStatus,
    /// Reopen OR CloseApproveComplaint, DeleteUndeleteComplaint
    /// </summary>
    public class Complaint
    {
        Proxy proxy;
        ComplaintDTO _complaint;
        public Complaint()
        {
            dSource = string.Empty;
            complaintID = -1;
            indexNo = -1;
            parentIndexNo = -1;
            finalstatuscd = -1;
            locationID = -1;
            boroughID = -1;

            //************PENDING DOCUMENT************
            pendingDoc = string.Empty;

            locationName = string.Empty;
            employeeID = -1;
            refNumber = String.Empty;
            titleID = -1;
            badgeNumber = String.Empty;
            incidentDate = DateTime.MinValue;
            SentDate = DateTime.MinValue;
            incidentType = String.Empty;
            jt1 = string.Empty;
            voilations = null;
            witnessess = null;
            complainants = null;
            comments = String.Empty;
            complaintActionID = 0;
            incidentLocationID = -1;
            incidentLocationName = String.Empty;
            isSubmitted = false;
            suspensionOrderedBy = -1;
            suspensionOrderedByName = String.Empty;
            suspensionLiftedByRefNo = String.Empty;
            suspensionOrderedDt = DateTime.MinValue;
            chargesServedBy = -1;
            chargesServedByName = String.Empty;
            chargesServedByRefNo = String.Empty;
            chargesServedDate = DateTime.MinValue;
            suspensionLiftedBy = -1;
            suspensionLiftedByName = String.Empty;
            suspensionLiftedByRefNo = String.Empty;
            SuspensionLiftedDate = DateTime.MinValue;
            routingLocation = String.Empty;
            routingLocationDesc = String.Empty;
            isProbation = false;
            isApproved = false;
            approvedDate = DateTime.MinValue;
            createdBy = String.Empty;
            createdDate = DateTime.MinValue;
            status = string.Empty;
            chartNumber = String.Empty;
            payCodeID = 0;
            payCodeDesc = String.Empty;
            vacationSchedule = String.Empty;
            charge = String.Empty;
            chargeDesc = String.Empty;
            respondentFName = String.Empty;
            respondentMName = String.Empty;
            respondentLName = String.Empty;
            authorFName = String.Empty;
            authorLName = String.Empty;
            respondentDOB = DateTime.MinValue;
            respondentAptDt = DateTime.MinValue;
            titleDesc = String.Empty;
            complaintStatus = 0;
            complaintStatusDate = DateTime.MinValue;
            indicator = String.Empty;
            streetName = String.Empty;
            streetNumber = String.Empty;
            aptNo = String.Empty;
            city = String.Empty;
            state = String.Empty;
            zip = String.Empty;
            zipSuffix = String.Empty;
            jt1 = String.Empty;

            _complaint = new ComplaintDTO();
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private String dSource;
        private Int64 complaintID;
        private Int64 indexNo;
        private Int64 parentIndexNo;
        private Int64 finalstatuscd;
        private Int64 locationID;
        private Int64 boroughID;

        //************PENDING DOCUMENT************
        private String pendingDoc;
        private String locationName;
        private Int64 employeeID;
        private String refNumber;
        private Int64 titleID;
        private String badgeNumber;
        private DateTime incidentDate;
        private DateTime sentDate;
        private String incidentType;
        private VoilationList voilations;
        private WitnessList witnessess;
        private ComplainantList complainants;
        private ComplaintHistoryList historyList;
        private String comments;
        private int complaintActionID;
        private Int64 incidentLocationID;
        private String incidentLocationName;
        private bool isSubmitted;
        private Int64 suspensionOrderedBy;
        private String suspensionOrderedByName;
        private String suspensionOrderedByRefNo;
        private DateTime suspensionOrderedDt;
        private Int64 chargesServedBy;
        private String chargesServedByName;
        private String chargesServedByRefNo;
        private DateTime chargesServedDate;
        private Int64 suspensionLiftedBy;
        private String suspensionLiftedByName;
        private String suspensionLiftedByRefNo;
        private DateTime suspensionLiftedDt;
        private String routingLocation;
        private String routingLocationDesc;
        private bool isProbation;
        private bool isApproved;
        private DateTime approvedDate;
        private String createdBy;
        private DateTime createdDate;
        private String status;
        private String chartNumber;
        private Int16 payCodeID;
        private String payCodeDesc;
        private String vacationSchedule;
        private String charge;
        private String chargeDesc;
        private String respondentFName;
        private String respondentMName;
        private String respondentLName;
        private String authorFName;
        private String authorLName;
        private DateTime respondentDOB;
        private DateTime respondentAptDt;
        private String titleDesc;
        private Int16 complaintStatus;
        private DateTime complaintStatusDate;
        private BCADComplaint bcadComplaint;
        private ComplaintAdvocacy complaintAdvocacy;
        private String indicator;
        private Boolean isDeleted;
        private String streetNumber;
        private String streetName;
        private String aptNo;
        private String city;
        private String state;
        private String zip;
        private String zipSuffix;
        private string jt1;
        private Int64 caseID;
        private MedicalComplaint medicalComplaint;       


        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        
        public Int64 ComplaintId
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int64 IndexNo
        {
            get { return indexNo; }
            set { indexNo = value; }
        }
                
        public Int64 ParentIndexNo
        {
            get { return parentIndexNo; }
            set { parentIndexNo = value; }
        }

        public Int64 FinalStatusCD
        {
            get { return finalstatuscd; }
            set { finalstatuscd = value; }
        }


        public Int64 LocationId
        {
            get { return locationID; }
            set { locationID = value; }
        }
        public Int64 BoroughId
        {
            get { return boroughID; }
            set { boroughID = value; }
        }
        public DateTime? PromotionDate { get; set; }        

        public String LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public String Jt1
        {
            get { return jt1; }
            set { jt1 = value; }
        }


        public Int64 EmployeeId
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public String ReferenceNumber
        {
            get { return refNumber; }
            set { refNumber = value; }
        }

        public Int64 TitleId
        {
            get { return titleID; }
            set { titleID = value; }
        }

        public String BadgeNumber
        {
            get { return badgeNumber; }
            set { badgeNumber = value; }
        }

        public DateTime IncidentDate
        {
            get { return incidentDate; }
            set { incidentDate = value; }
        }

        public DateTime SentDate
        {
            get { return sentDate; }
            set { sentDate = value; }
        }
        

        public String IncidentType
        {
            get { return incidentType; }
            set { incidentType = value; }
        }



        public VoilationList Violations
        {
            get { return voilations; }
            set { voilations = value; }
        }

        public WitnessList Witnesses
        {
            get { return witnessess; }
            set { witnessess = value; }
        }

        public ComplainantList Complainants
        {
            get { return complainants; }
            set { complainants = value; }
        }

        public ComplaintHistoryList HistoryList
        {
            get { return historyList; }
            set { historyList = value; }
        }

        public String Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        public int ComplaintActionId
        {
            get { return complaintActionID; }
            set { complaintActionID = value; }
        }

        public Int64 IncidentLocationId
        {
            get { return incidentLocationID; }
            set { incidentLocationID = value; }
        }

        public String IncidentLocation
        {
            get { return incidentLocationName; }
            set { incidentLocationName = value; }
        }

        public Boolean IsSubmitted
        {
            get { return isSubmitted; }
            set { isSubmitted = value; }
        }

        public DateTime ChargesServedDate
        {
            get { return chargesServedDate; }
            set { chargesServedDate = value; }
        }

        public Int64 ChargesServedBy
        {
            get { return chargesServedBy; }
            set { chargesServedBy = value; }
        }

        public String ChargesServedByName
        {
            get { return chargesServedByName; }
            set { chargesServedByName = value; }
        }

        public String ChargesServedByRefNo
        {
            get { return chargesServedByRefNo; }
            set { chargesServedByRefNo = value; }
        }

        public Int64 SuspensionOrderedById
        {
            get { return suspensionOrderedBy; }
            set { suspensionOrderedBy = value; }
        }

        public String SuspensionOrderedByName
        {
            get { return suspensionOrderedByName; }
            set { suspensionOrderedByName = value; }
        }

        public String SuspensionOrderedByRefNo
        {
            get { return suspensionOrderedByRefNo; }
            set { suspensionOrderedByRefNo = value; }
        }

        public DateTime SuspensionOrderDate
        {
            get { return suspensionOrderedDt; }
            set { suspensionOrderedDt = value; }
        }

        public Int64 SuspensionLiftedBy
        {
            get { return suspensionLiftedBy; }
            set { suspensionLiftedBy = value; }
        }

        public String SuspensionLiftedByName
        {
            get { return suspensionLiftedByName; }
            set { suspensionLiftedByName = value; }
        }

        public String SuspensionLiftedByRefNo
        {
            get { return suspensionLiftedByRefNo; }
            set { suspensionLiftedByRefNo = value; }
        }

        public DateTime SuspensionLiftedDate
        {
            get { return suspensionLiftedDt; }
            set { suspensionLiftedDt = value; }
        }

        public String RoutingLocation
        {
            get { return routingLocation; }
            set { routingLocation = value; }
        }

        public String RoutingLocationDescription
        {
            get {
                switch (routingLocation)
                { 
                    case "A":
                        return "Advocate";
                    case "B":
                        return "BCAD";
                    case "M":
                        return "Medical";
                    case "P":
                        return "Pending";
                    default:
                        return string.Empty;
                }
            }
        }

        public Boolean IsProbation
        {
            get { return isProbation; }
            set { isProbation = value; }
        }

        public Boolean IsApproved
        {
            get { return isApproved; }
            set { isApproved = value; }
        }

        public DateTime ApprovedDate
        {
            get { return approvedDate; }
            set { approvedDate = value; }
        }

        public String CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public String Status
        {
            get { return status; }
            set { status = value; }
        }

        public String Charge
        {
            get { return charge; }
            set { charge = value; }
        }

        public String ChargeDescription
        {
            get { return chargeDesc; }
            set { chargeDesc = value; }
        }

        public String RespondentFirstName
        {
            get { return respondentFName; }
            set { respondentFName = value; }
        }
        public String AuthorFirstName
        {
            get { return authorFName; }
            set { authorFName = value; }
        }

        public String RespondentMiddleName
        {
            get { return respondentMName; }
            set { respondentMName = value; }
        }

        public String RespondentLastName
        {
            get { return respondentLName; }
            set { respondentLName = value; }
        }
        public String AuthorLastName
        {
            get { return authorLName; }
            set { authorLName = value; }
        }

        public DateTime RespondentDob
        {
            get { return respondentDOB; }
            set { respondentDOB = value; }
        }

        public DateTime RespondentAptDate
        {
            get { return respondentAptDt; }
            set { respondentAptDt = value; }
        }


        public String TitleDescription
        {
            get { return titleDesc; }
            set { titleDesc = value; }
        }

        public Int16 ComplaintStatus
        {
            get { return complaintStatus; }
            set { complaintStatus = value; }
        }

        //************PENDING DOCUMENT************

        public String PendingDoc
        {
            get { return pendingDoc; }
            set { pendingDoc = value; }
        }

        public DateTime ComplaintStatusDate
        {
            get { return complaintStatusDate; }
            set { complaintStatusDate = value; }
        }

        public BCADComplaint BcadComplaint
        {
            get
            {
                return bcadComplaint;
            }
            set { bcadComplaint = value; }
        }

        public ComplaintAdvocacy ComplaintAdvocacy
        {
            get { return complaintAdvocacy; }
            set { complaintAdvocacy = value; }
        }
        
        public String StreetNumber
        {
            get { return streetNumber; }
            set { streetNumber = value; }
        }

        public String StreetName
        {
            get { return streetName; }
            set { streetName = value; }
        }

        public String AptNo
        {
            get { return aptNo; }
            set { aptNo = value; }
        }

        public String City
        {
            get { return city; }
            set { city = value; }
        }

        public String State
        {
            get { return state; }
            set { state = value; }
        }

        public String ZipCode
        {
            get { return zip; }
            set { zip = value; }
        }

        public String ZipSuffix
        {
            get { return zipSuffix; }
            set { zipSuffix = value; }
        }

        public String ChartNo
        {
            get { return chartNumber; }
            set { chartNumber = value; }
        }

        public String VacationSchedule
        {
            get { return vacationSchedule; }
            set { vacationSchedule = value; }
        }

        public Int16 PayCodeId
        {
            get { return payCodeID; }
            set { payCodeID = value; }
        }

        public String PayCodeDescription
        {
            get { return payCodeDesc; }
            set { payCodeDesc = value; }
        }

        public String Indicator
        {
            get { return indicator; }
            set { indicator = value; }
        }

        public Boolean IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        public Int64 CaseId
        {
            get { return caseID; }
            set { caseID = value; }
        }

        public String DataSource
        {
            get { return dSource; }
            set { dSource = value; }
        }

        public MedicalComplaint MedicalComplaint
        {
            get
            {
                return medicalComplaint;
            }
            set { medicalComplaint = value; }
        }
           

        #endregion
        /// <summary>
        /// This function returns the list of complaints attachments
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="files"></param>
        /// <param name="applicationName"></param>
        /// <returns></returns>
        //DO
        public bool UploadFiles(long ParentID, byte[] files, String applicationName, String contentType, String FileName, String Child1)
        {
            if (proxy == null)
                proxy = new Proxy();
            return proxy.UploadFiles(ParentID, files, applicationName, contentType, FileName, Child1);  
        
        }


        public bool UploadFiles(long ParentID, byte[] files, String contentType, String FileName, String Child1)
        {
            if (proxy == null)
                proxy = new Proxy();
            return proxy.UploadFiles(ParentID, files, contentType, FileName, Child1);
        }


        /// <summary>
        /// This methos gets the full name of the user
        /// </summary>
        public String FullName
        {
            get
            {
                if (respondentFName != string.Empty || respondentLName != string.Empty)
                    return RespondentFirstName.Trim() + " " + respondentMName.Trim() + " " + respondentLName.Trim();
                else
                    return String.Empty;
            }
        }
        /// <summary>
        /// This methos gets the initial amd last name of the user
        /// </summary>
        public String InitialAndLastName
        {
            get
            {
                if (respondentFName.Trim() == string.Empty && respondentLName.Trim() == string.Empty)
                    return string.Empty;
                else if (respondentFName.Trim() != string.Empty)
                    return RespondentFirstName.Trim().Substring(0, 1) + ". " + respondentLName.Trim();
                else if (respondentLName != string.Empty)
                    return respondentLName.Trim();
                else
                    return String.Empty;
            }
        }

        /// <summary>
        /// This methos gets the initial and last name of the authour
        /// </summary>
        public String AuthorInitialAndLastName
        {
            get
            {
                
               if (AuthorFirstName.Trim() == string.Empty && AuthorLastName.Trim() == string.Empty)
                    return string.Empty;
                else if (AuthorFirstName.Trim() != string.Empty)
                    return AuthorFirstName.Trim().Substring(0, 1) + ". " + AuthorLastName.Trim();
                else if (AuthorLastName != string.Empty)
                    return respondentLName.Trim();
                else
                    return String.Empty;
            }
        }
       
        

        #region Public Methods
        /// <summary>
        /// This method loads the list of complaints based on the complain ID and 
        /// the type of resource(BCAD/ADVOCATE/DS249)
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool Load(Int64 id, RequestSource source)
        {            
                if (proxy == null)
                    proxy = new Proxy();
                List<ComplaintDTO> dsComplaint = proxy.LoadComplaint(id);
                AssembleComplaint(dsComplaint);
                if (source == RequestSource.DS249)
                {
                    this.witnessess = new WitnessList();
                    this.Witnesses.List = GetWitnessList();
                    this.complainants = new ComplainantList();
                    this.complainants.List = GetComplainants();
                    this.voilations = new VoilationList();
                    this.voilations.List = GetViolations();
                    this.historyList = new ComplaintHistoryList();
                    this.historyList.List = GetComplaintHistory();
                }

                if (source == RequestSource.BCAD)
                {
                    
                    this.bcadComplaint = GetBCADComplaint();
                    this.complainants = new ComplainantList();
                    this.complainants.List = GetComplainants();
                    this.voilations = new VoilationList();
                    this.voilations.List = GetViolations();
                }

                if (source == RequestSource.Medical)
                {

                    this.medicalComplaint = GetMedicalComplaint();
                    this.complainants = new ComplainantList();
                    this.complainants.List = GetComplainants();
                    this.voilations = new VoilationList();
                    this.voilations.List = GetViolations();
                }

                if (source == RequestSource.Advocate)
                {
                    this.complaintAdvocacy = GetAdvocateComplaint();
                    this.complainants = new ComplainantList();
                    this.complainants.List = GetComplainants();
                    this.voilations = new VoilationList();
                    this.voilations.List = GetViolations();
                }

                return true;           
        }
        /// <summary>
        /// This method assembles the list of complaints from the DTO
        /// </summary>
        /// <param name="dscomplaint"></param>

        private void AssembleComplaint(List<ComplaintDTO> listComplaint)
        {           
                foreach (ComplaintDTO lComplaint in listComplaint)
                {
                    this.ComplaintId = lComplaint.ComplaintID;
                    this.IndexNo = lComplaint.IndexNo;

                    this.locationID = lComplaint.LocationID;
                    this.boroughID = lComplaint.BoroughID;
                    this.PromotionDate= lComplaint.PromotionDate;
                    this.locationName = lComplaint.LocationName;
                    this.EmployeeId = lComplaint.EmployeeID;
                    this.ReferenceNumber = lComplaint.ReferenceNumber;
                    this.TitleId = lComplaint.TitleID;
                    this.BadgeNumber = lComplaint.BadgeNumber;
                    this.ComplaintActionId = lComplaint.ComplaintActionID;
                    this.IsSubmitted = lComplaint.IsSubmitted;
                    this.ChargesServedDate = lComplaint.ChargesServedDate;
                    this.SuspensionOrderedById = lComplaint.SuspensionOrderedById;
                    this.SuspensionOrderedByName = lComplaint.SuspensionOrderedByName;
                    this.SuspensionOrderedByRefNo = lComplaint.SuspensionOrderedByRefNo;
                    this.SuspensionOrderDate = lComplaint.SuspensionOrderDate;
                    this.ChargesServedBy = lComplaint.ChargesServedBy;
                    this.ChargesServedByName = lComplaint.ChargesServedByName;
                    this.ChargesServedByRefNo = lComplaint.ChargesServedByRefNo;
                    this.SuspensionLiftedBy = lComplaint.SuspensionLiftedBy;
                    this.SuspensionLiftedByName = lComplaint.SuspensionLiftedByName;
                    this.SuspensionLiftedByRefNo = lComplaint.SuspensionLiftedByRefNo;
                    this.SuspensionLiftedDate = lComplaint.SuspensionLifedDate;
                    this.RoutingLocation = lComplaint.RoutingLocation;
                    this.IsProbation = lComplaint.IsProbation;
                    this.isApproved = lComplaint.IsApproved;
                    this.ApprovedDate = lComplaint.ApprovedDate;
                    this.CreatedBy = lComplaint.CreatedBy;
                    this.CreatedDate = lComplaint.CreatedDate;
                    this.Comments = lComplaint.Comments;
                    this.SentDate = lComplaint.SentDate;
                    this.complaintStatus = lComplaint.ComplaintStatus;
                    this.complaintStatusDate = lComplaint.ComplaintStatusDate;
                    this.StreetNumber = lComplaint.StreetNumber;
                    this.StreetName = lComplaint.StreetName;
                    this.AptNo = lComplaint.AptNo;
                    this.City = lComplaint.City;
                    this.State = lComplaint.State;
                    this.ZipCode = lComplaint.ZipCode;
                    this.ZipSuffix = lComplaint.ZipSuffix;
                    this.PayCodeId = lComplaint.PayCodeID;
                    this.PayCodeDescription = lComplaint.PayCodeDesc;
                    this.ChartNo = lComplaint.ChartNo;
                    this.VacationSchedule = lComplaint.VacationSchedule;
                    this.RespondentFirstName = lComplaint.RespondentFName;
                    this.RespondentLastName = lComplaint.RespondentLName;
                    this.RespondentDob = lComplaint.RespondentDOB;
                    this.RespondentAptDate = lComplaint.RespondentAptDt;
                    this.TitleDescription = lComplaint.TitleDesc;
                    this.IsProbation = lComplaint.IsProbation;
                }
           
        }

        /// <summary>
        /// This methof Gets the list of witness and retuns true
        /// </summary>
        /// <returns></returns>
        private List<Witness> GetWitnessList()
        {
            WitnessList witnesses = new WitnessList();
            witnesses.Load(complaintID);
            return witnesses.List;
        }
        /// <summary>
        /// This method Gets the list of complaints and retuns true
        /// </summary>
        /// <returns></returns>
        private List<Complainant> GetComplainants()
        {
            ComplainantList complainants = new ComplainantList();
            complainants.Load(complaintID);
            return complainants.List;
        }
        /// <summary>
        /// This method Gets the list of violations and retuns true
        /// </summary>
        /// <returns></returns>
        private List<Voilation> GetViolations()
        {
            VoilationList violations = new VoilationList();
            violations.Load(complaintID);
            return violations.List;
        }
        /// <summary>
        /// This method Gets the list of BCAD complaints and retuns true
        /// </summary>
        /// <returns></returns>
        private BCADComplaint GetBCADComplaint()
        {
            BCADComplaint bcadComp = new BCADComplaint();
            bcadComp.ComplaintId = this.complaintID;
            bcadComp.Load();
            return bcadComp;
        }


        /// <summary>
        /// This method Gets the list of BCAD complaints and retuns true
        /// </summary>
        /// <returns></returns>
        private MedicalComplaint GetMedicalComplaint()
        {
            MedicalComplaint medicalComp = new MedicalComplaint();
            medicalComp.ComplaintId = this.complaintID;
            medicalComp.Load();
            return medicalComp;
        }


        /// <summary>
        /// This method Gets the list of Advocate complaints and retuns true
        /// </summary>
        /// <returns></returns>
        private ComplaintAdvocacy GetAdvocateComplaint()
        {
            ComplaintAdvocacy advComp = new ComplaintAdvocacy();
            advComp.ComplaintId = this.complaintID;
            advComp.Load();
            return advComp;
        }
        /// <summary>
        /// This method Gets the list of complaints history and retuns true
        /// </summary>
        /// <returns></returns>
        private List<ComplaintHistory> GetComplaintHistory()
        {
            ComplaintHistoryList historyList = new ComplaintHistoryList();
            historyList.Load(employeeID, complaintID);
            return historyList.List;
        }

        /// <summary>
        /// This method saves the complaints based on the 
        /// type of resource(BCAD/ADVOCATE/DS249)
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public Boolean Save(RequestSource source, Int64 personID)
        {     
               
                if (proxy == null)
                    proxy = new Proxy();
                CreateComplaint(source);
                if (proxy.SaveComplaint(_complaint, personID))
                {
                    this.complaintID = _complaint.ComplaintID;
                    this.indexNo = _complaint.IndexNo;
                    return true;
                }
                else
                    return false;           
        }
        /// <summary>
        /// This method creates a new complaint based on the type of resource(BCAD/ADVOCATE/DS249)
        /// </summary>
        /// <param name="source"></param>
        private void CreateComplaint(RequestSource source)
        {

            _complaint = new ComplaintDTO();
            _complaint.ComplaintID = this.complaintID;
            _complaint.IndexNo = this.indexNo;
            _complaint.LocationID = this.locationID;
            _complaint.BoroughID = this.boroughID;
            _complaint.PromotionDate = this.PromotionDate;
            _complaint.EmployeeID = this.employeeID;
            _complaint.ReferenceNumber = this.refNumber;
            _complaint.TitleID = this.titleID;
            _complaint.BadgeNumber = this.badgeNumber;
            _complaint.ChargesServedByRefNo = this.chargesServedByRefNo;
            _complaint.ChargesServedDate = this.chargesServedDate;
            _complaint.SuspensionOrderedByRefNo = this.suspensionOrderedByRefNo;
            _complaint.SuspensionOrderDate = this.suspensionOrderedDt;
            _complaint.SuspensionLiftedByRefNo = this.SuspensionLiftedByRefNo;
            _complaint.SuspensionLifedDate = this.suspensionLiftedDt;
            _complaint.RoutingLocation = this.routingLocation;
            _complaint.IsProbation = this.isProbation;
            _complaint.IsApproved = this.isApproved;
            _complaint.ApprovedDate = this.approvedDate;
            _complaint.Comments = this.comments;
            _complaint.CreatedBy = this.createdBy;
            _complaint.StreetNumber = this.streetNumber;
            _complaint.StreetName = this.streetName;
            _complaint.AptNo = this.aptNo;
            _complaint.City = this.city;
            _complaint.State = this.state;
            _complaint.ZipCode = this.zip;
            _complaint.ZipSuffix = this.zipSuffix;
            _complaint.PayCodeID = this.payCodeID;
            _complaint.VacationSchedule = this.vacationSchedule;
            _complaint.ChartNo = this.chartNumber;
            _complaint.RespondentFName = this.respondentFName;
            _complaint.RespondentMName = this.respondentMName;
            _complaint.RespondentLName = this.respondentLName;
            _complaint.RespondentDOB = this.respondentDOB;
            _complaint.RespondentAptDt = this.respondentAptDt;         

            if (source == RequestSource.DS249)
            {
                CreateWitnesses();
                CreateComplainants();
                CreateViolations();
            }
        }
        /// <summary>
        /// This method creates list of witnesses
        /// </summary>
        private void CreateWitnesses()
        {
            WitnessDto witDTO;
            _complaint.Witnesses = new List<WitnessDto>();
            foreach (Witness witness in this.Witnesses.List)
            {
                witDTO = new WitnessDto();
                witDTO.ComplaintID = witness.ComplaintId;
                witDTO.RefNo = witness.RefNo;
                witDTO.PersonnelName = witness.PersonnelName;
                witDTO.UniqueID = witness.UniqueId;

                _complaint.Witnesses.Add(witDTO);
            }
        }

        /// <summary>
        /// This method creates Create new Complainants
        /// </summary>

        private void CreateComplainants()
        {
            ComplainantDTO compDTO;
            _complaint.Complainants = new List<ComplainantDTO>();
            foreach (Complainant complainant in this.complainants.List)
            {
                compDTO = new ComplainantDTO();
                compDTO.ComplaintID = complainant.ComplaintId;
                compDTO.RefNo = complainant.RefNo;
                compDTO.ComplainantName = complainant.ComplainantName;
                compDTO.UniqueID = complainant.UniqueId;
                compDTO.LocationID = complainant.LocationId;
                compDTO.TitleID = complainant.TitleId;
                compDTO.Statement = complainant.Statement;
                compDTO.OnDate = complainant.OnDate;
                compDTO.Text = complainant.Text;
                compDTO.ThisDate = complainant.ThisDate;
                compDTO.Category = complainant.Category;
                compDTO.ReportedDate = complainant.ReportedDate;
                compDTO.FromLocation = complainant.FromLocation;
                compDTO.ReportedTime = complainant.ReportedTime;
                compDTO.ReportingTime = complainant.ReportingTime;
                compDTO.TelephoneNo = complainant.TelephoneNo;

                _complaint.Complainants.Add(compDTO);
            }
        }
        /// <summary>
        /// This method creates Create new Violation
        /// </summary>
        private void CreateViolations()
        {
            ViolationDTO violationDTO;
            IncedentDTO incedentDTO;
            _complaint.Voilations = new List<ViolationDTO>();
            foreach (Voilation violation in this.Violations.List)
            {
                violationDTO = new ViolationDTO();
                violationDTO.ComplaintID = violation.ComplaintId;
                violationDTO.Charge = CreateCharge(violation);
                violationDTO.UniqueID = violation.UniqueId;
                violationDTO.IncidentLocationID = violation.IncidentLocationId;
                violationDTO.IncidentType = violation.IncidentType;
                violationDTO.Incidents = new List<IncedentDTO>();
                foreach (Incedent incedent in violation.Incidents.List)
                {
                    incedentDTO = new IncedentDTO();
                    incedentDTO.ChargeID = incedent.ChargeId;
                    incedentDTO.ComplaintID = incedent.ComplaintId;
                    incedentDTO.IncedentDate = incedent.IncidentDate;
                    incedentDTO.UniqueID = incedent.UniqueId;
                    violationDTO.Incidents.Add(incedentDTO);
                }                
                _complaint.Voilations.Add(violationDTO);
            }

        }
       
 
        /// <summary>
        /// This method creates Create new Charges based on the violation
        /// </summary>
        /// <param name="violation"></param>
        /// <returns></returns>
        private ChargeDTO CreateCharge(Voilation violation)
        {
            ChargeDTO charge = new ChargeDTO();
            if (violation.Charge != null)
            {
                charge.ChargeCode = violation.Charge.ChargeCode;
                charge.ChargeDescription = violation.Charge.ChargeDescription;
                charge.ChargeTypeID = violation.Charge.ChargeTypeId;
            }
            return charge;
        }
        /// <summary>
        /// This method updates the complaint status.
        /// </summary>
        /// <returns></returns>
        public Boolean UpdateComplaintStatus()
        {
             if (proxy == null)
                    proxy = new Proxy();
                return proxy.UpdateComplaintStatus(this.complaintID, this.complaintStatus);            
        }
        
        public bool SaveCombinedComplaints(String indexno, Int64 parentIndexNo, string uncmbIndexNo, Int64 uncmbParentIndexNo)
        {
          
                if (proxy == null)
                    proxy = new Proxy();             
                return proxy.UpdateCombinedComplaints(indexno, parentIndexNo, uncmbIndexNo, uncmbParentIndexNo);           
        }


        public Int64 CheckIsParent(Int64 IndexNo, ref Int64 IsChild)
        {
             if (proxy == null)
                    proxy = new Proxy();               
                return proxy.CheckIsParent(IndexNo, ref IsChild);
        }

        private void CreateCombinedComplaint()
        {
                _complaint = new ComplaintDTO();
                _complaint.IndexNo = this.indexNo;
                _complaint.ParentIndexNo = this.parentIndexNo;           
            
        }

                /// <summary>
        /// This method re-opens or closes the approved complaints
        /// </summary>
        /// <returns></returns>
        public Boolean ReopenORCloseApproveComplaint()
        {
            _complaint = new ComplaintDTO();
            _complaint.ComplaintID = this.complaintID;
            _complaint.ComplaintStatus = this.complaintStatus;
            _complaint.CreatedBy = this.createdBy;

            if (proxy == null)
                proxy = new Proxy();
            return proxy.ReopenORCloseApproveComplaint(_complaint);
        }
              

        /// <summary>
        /// This method returs true if the undeleted complaint is sucessfully deleted.
        /// </summary>
        /// <returns></returns>
        public Boolean DeleteUndeleteComplaint()
        {
            if (proxy == null)
                proxy = new Proxy();

            return proxy.DeleteUndeleteComplaint(this.complaintID, this.complaintStatus);
        }

        public Boolean ComplaintHistoryDetails(Int64 complaintID, String status, Int64 UserId, DateTime date)
        {
            if (proxy == null)
                proxy = new Proxy();

            return proxy.ComplaintHistoryDetails(complaintID, status, UserId, date);
        }

        public Boolean AdminPersonDetails(Int64 UserId, String Status, Int64 personId)
        {
            if (proxy == null)
                proxy = new Proxy();

            return proxy.AdminPersonDetails(UserId,Status,personId);
        }

        #endregion
    }
}
