﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security;
using System.Web.Security;
using System.Configuration;
using System.Xml;


namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide method to  redirects the form login page based on userName, Roles, PersistentCookie, Cookie
    /// methods to redirects the user to the page based on userName, Roles, PersistentCookie, Cookie 
    /// method to set the form authentication cookie based on userName, Roles, PersistentCookie, Cookie 
    /// method returns the Cookie time out value,
    /// method to set the authentication of the cookie based on userName, Roles, PersistentCookie, Cookie 
    /// and to Adds roles to the current User in HttpContext after forms authentication authenticates the user
    /// so that, the authorization mechanism can authorize user based on the groups/roles of the user

    /// </summary>
    public class SecurityUtility
    {
        public SecurityUtility()
        { 
        }
        /// <summary>
        /// This method redirects the form login page based on userName, Roles, PersistentCookie, Cookie
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="commaSeperatedRoles"></param>
        /// <param name="createPersistentCookie"></param>
        /// <param name="strCookiePath"></param>
        private static void RedirectFromLoginPageMain(string userName, string commaSeperatedRoles, bool createPersistentCookie, string strCookiePath)
        {
            SetAuthCookieMain(userName, commaSeperatedRoles, createPersistentCookie, strCookiePath);
            string strURL = FormsAuthentication.GetRedirectUrl(userName, createPersistentCookie);
            HttpContext.Current.Response.Redirect(FormsAuthentication.GetRedirectUrl(userName, createPersistentCookie));
        }

        public static void RedirectFromLoginPage(string userName, string commaSeparatedRoles, bool createPersistentCookie, string stringCookiePath)
        {
            RedirectFromLoginPageMain(userName, commaSeparatedRoles, createPersistentCookie, stringCookiePath);
        }
        /// <summary>
        /// This methods redirects the user to the page based on userName, Roles, PersistentCookie, Cookie 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="commaSeperatedRoles"></param>
        /// <param name="createPersistentCookie"></param>
        public static void RedirectFromLoginPage(string userName, string commaSeparatedRoles, bool createPersistentCookie)
        {
            RedirectFromLoginPageMain(userName, commaSeparatedRoles, createPersistentCookie, null);
        }
        /// <summary>
        /// This method set the form authentication cookie based on userName, Roles, PersistentCookie, Cookie 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="commaSeperatedRoles"></param>
        /// <param name="createPersistentCookie"></param>
        /// <param name="strCookiePath"></param>
        /// <returns></returns>
        private static FormsAuthenticationTicket CreateAuthenticationTicket(string userName, string commaSeperatedRoles, bool createPersistentCookie, string strCookiePath)
        {
            string cookiePath = strCookiePath == null ? FormsAuthentication.FormsCookiePath : strCookiePath;
            int expirationMinutes = GetCookieTimeoutValue();            
            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,userName,DateTime.Now,DateTime.Now.AddMinutes(expirationMinutes),createPersistentCookie,commaSeperatedRoles, cookiePath);
            return ticket;
        }

        /// <summary>
        /// This method returns the Cookie time out value
        /// The default is 30 minutes
        /// </summary>
        /// <returns></returns>
        private static int GetCookieTimeoutValue()
        {            
            int timeout = 110;
            XmlDocument webConfig = new XmlDocument();
            webConfig.Load(HttpContext.Current.Server.MapPath(@"~\web.config"));
            XmlNode node = webConfig.SelectSingleNode("/configuration/system.web/authentication/forms");
            if (node != null && node.Attributes["timeout"] != null)
            {
                timeout = int.Parse(node.Attributes["timeout"].Value);
            }
            return timeout;
        }
        /// <summary>
        /// This method set the authentication of the cookie based on userName, Roles, PersistentCookie, Cookie 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="commaSeperatedRoles"></param>
        /// <param name="createPersistentCookie"></param>
        public static void SetAuthCookie(string userName, string commaSeparatedRoles, bool createPersistentCookie)
        {
            SetAuthCookieMain(userName, commaSeparatedRoles, createPersistentCookie, null);
        }

        public static void SetAuthCookie(string userName, string commaSeparatedRoles, bool createPersistentCookie, string stringCookiePath)
        {
            SetAuthCookieMain(userName, commaSeparatedRoles, createPersistentCookie, stringCookiePath);
        }
        /// <summary>
        /// This method returns form authentication ticket with based on userName, Roles, PersistentCookie, Cookie 
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="commaSeperatedRoles"></param>
        /// <param name="createPersistentCookie"></param>
        /// <param name="strCookiePath"></param>
        private static void SetAuthCookieMain(string userName, string commaSeperatedRoles, bool createPersistentCookie, string strCookiePath)
        {
            FormsAuthenticationTicket ticket = CreateAuthenticationTicket(userName, commaSeperatedRoles, createPersistentCookie, strCookiePath);
            string encrypetedTicket = FormsAuthentication.Encrypt(ticket);
            if (!FormsAuthentication.CookiesSupported)
            {
                FormsAuthentication.SetAuthCookie(encrypetedTicket, createPersistentCookie);
            }
            else
            {
                HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypetedTicket);

                if (ticket.IsPersistent) authCookie.Expires = ticket.Expiration;
                HttpContext.Current.Response.Cookies.Add(authCookie);
            }
        }
        
        /// <summary>
        /// Adds roles to the current User in HttpContext after forms authentication authenticates the user
        /// so that, the authorization mechanism can authorize user based on the groups/roles of the user
        /// </summary>
        public static void AttachRolesToUser( User user )
        {
            if (HttpContext.Current.User != null)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (HttpContext.Current.User.Identity is FormsIdentity)
                    {
                        FormsIdentity id = (FormsIdentity)HttpContext.Current.User.Identity;

                        FormsAuthenticationTicket ticket = (id.Ticket);

                        if (!FormsAuthentication.CookiesSupported)
                        {
                            ticket = FormsAuthentication.Decrypt(id.Ticket.Name);
                        }
                        String[] roles = user.MembershipRoles();
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(id, roles);
                    }
                }
                
            }
        }
    }
}
