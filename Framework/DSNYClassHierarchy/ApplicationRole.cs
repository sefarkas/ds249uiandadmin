﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide method to returns the roles.
    /// </summary>
    public class ApplicationRole
    {
        public ApplicationRole()
        {
            id = -1;
            applicationId = -1;
            role = String.Empty;
            roleID = -1;
            personalId = -1;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int16 id;
        private Int64 applicationId;
        private Int16 roleID;
        private String role;
        private Int64 personalId;

        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int16 Id
        {
            get { return id; }
            set { id = value; }
        }

        public Int64 ApplicationId
        {
            get { return applicationId; }
            set { applicationId = value; }
        }

        public Int64 PersonalId
        {
            get { return personalId; }
            set { personalId = value; }
        }

        public Int16 RoleId
        {
            get { return roleID; }
            set { roleID = value; }
        }

        public String RoleDescription
        {
            get { return role; }
            set { role = value; }
        }

        #endregion

        #region Public Method
        /// <summary>
        /// This method returns true is the roles are laoded sucessfully.
        /// </summary>
        /// <returns></returns>
        public bool Load()
        {
            return true;
        }
        #endregion
    }
}

