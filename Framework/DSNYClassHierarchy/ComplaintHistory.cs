﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to load complaint History based on complaints ID
    /// </summary>
    public class ComplaintHistory
    {
        public ComplaintHistory()
        {
            voilationDate = DateTime.MinValue;
            complaintID = -1;
            indexNo = -1;
            voilations = string.Empty;
            voilationDesc = String.Empty;
            penalty = string.Empty;
            author = string.Empty;
            routingLocation = string.Empty;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private DateTime voilationDate;
        private Int64 complaintID;
        private Int64 indexNo;
        private String voilations;
        private String voilationDesc;
        private String penalty;
        private String author;
        private String routingLocation;
        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public DateTime ViolationDate
        {
            get { return voilationDate; }
            set { voilationDate = value; }
        }

        public String ViolationDateAsString
        {
            get { return voilationDate.ToShortDateString(); }
            
        }

        public Int64 ComplaintId
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int64 IndexNo
        {
            get { return indexNo; }
            set { indexNo = value; }
        }

        public String Violations
        {
            get { return voilations;}
            set { voilations = value; }
        }

        public String ViolationDescription
        {
            get { return voilationDesc; }
            set { voilationDesc = value; }
        }

        public String Penalty
        {
            get { return penalty; }
            set { penalty = value; }
        }

        public String Author
        {
            get { return author; }
            set { author = value; }
        }

        public String RoutingLocation
        {
            get { return routingLocation; }
            set { routingLocation = value; }
        }


        #endregion

        #region Public Method
        /// <summary>
        /// This method loads the list of complaints based on the complain ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool Load(Int64 complaintId)
        {           
                return true;            
        }
        #endregion
    }
}
