﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functionality to Load list of titles and Assemble Title List
    /// </summary>
    public class TitleList
    {
         Proxy proxy;

         public TitleList()
        {
            list = null;
        }

        private List<Title> list;

        public List<Title> List
        {
            get { return list; }
            set { list = value; }
        }

        /// <summary>
        /// This method loads the list of titles
        /// </summary>
        public void Load()
        {           
                if (proxy == null)
                    proxy = new Proxy();              
                List<TitleDTO> titlesDTO = proxy.LoadTitles();
                AssembleTitle(titlesDTO);           
        }

        private void AssembleTitle(List<TitleDTO> titleDTO)
        {
            if (list == null)
                list = new List<Title>();           
                Title title;               
                foreach (TitleDTO titleList in titleDTO)
                {
                    title = new Title();
                    title.TitleId = titleList.TitleID;
                    title.TitleCode = titleList.TitleCode;
                    title.TitleDescription = titleList.TitleDescription;
                    list.Add(title);
                }           
        }
        /// <summary>
        /// This method loads the list of titles based on the search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        public void Load(String searchText)
        {           
                if (proxy == null)
                    proxy = new Proxy();
                List<TitleListDTO> titlesDTO = proxy.LoadTitles(searchText);
                AssembleTitleList(titlesDTO);            
        }

        /// <summary>
        /// This method assembles the list of Titles from the DTO.
        /// </summary>
        /// <param name="dsTitle"></param>
        private void AssembleTitleList(List<TitleListDTO> titleDTO)
        {
            if (list == null)
                list = new List<Title>();
           
                Title title;
               
                foreach (TitleListDTO titleList in titleDTO)
                    {
                        title = new Title();
                            title.TitleId = titleList.TitleID;
                            title.TitleCode = titleList.TitleCode;
                            title.TitleDescription = titleList.TitleDescription;
                        list.Add(title);
                    }           
        }

    }
}
