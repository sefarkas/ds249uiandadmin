﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functionality to laod and save personal info.
    /// </summary>

    public class Person
    {
        public Person()
        {
            personID = -1;
            imageID = -1;
            firstName = String.Empty;
            middleName = String.Empty;
            lastName = String.Empty;
            fullName = String.Empty;
            ssn = String.Empty;
            dob = DateTime.MinValue;
            gender = String.Empty;
            userName = String.Empty;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64 personID;
        private Int32 imageID;
        private String firstName;
        private String middleName;
        private String lastName;
        private String fullName;
        private String ssn;
        private DateTime dob;
        private String gender;
        private String userName;
        private String emailID;
        private Boolean isDeleted;
        private Boolean isAdmin;
        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int64 PersonId
        {
            get { return personID; }
            set { personID = value; }
        }
        public Int32 ImageId
        {
            get { return imageID; }
            set { imageID = value; }
        }

        public String FirstName
        {
            get { return firstName.Trim(); }
            set { firstName = value; }
        }
        /// <summary>
        /// This method returns the last name of the user
        /// </summary>
        public String MiddleName
        {
            get { return middleName.Trim(); }
            set { middleName = value; }
        }
        /// <summary>
        /// This method returns the last name of the user
        /// </summary>
        public String LastName
        {
            get { return lastName.Trim(); }
            set { lastName = value; }
        }
        /// <summary>
        /// This method returns the Last name of the user
        /// </summary>
        public String ByLastName
        {
            get {
                if (firstName != string.Empty || lastName!= string.Empty)
                    return lastName.Trim() + ", " + firstName.Trim() + " " + middleName.Trim() + ", ";
                else if (firstName != string.Empty || lastName != string.Empty || middleName == string.Empty)
                    return lastName.Trim() + ", " + firstName.Trim() +", ";
                else
                    return String.Empty; 
            }
        }
        /// <summary>
        /// This method returns the full name of the user
        /// </summary>
        public String FullName
        {
            get
            {
                if (FirstName != string.Empty || LastName != string.Empty)
                    return FirstName.Trim() + " " + MiddleName.Trim() + " " + LastName.Trim();
                else
                    return String.Empty;
            }
        }

        public String Ssn
        {
            get { return ssn;}
            set { ssn = value; }
        }

        public DateTime Dob
        {
            get { return dob; }
            set { dob = value; }
        }

        public String Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public String UserName
        {
            get { return userName; }
            set { userName = value; }
        }

        public String EmailId
        {
            get { return emailID; }
            set { emailID = value; }
        }

        public Boolean IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        public Boolean IsAdmin
        {
            get { return isAdmin; }
            set { isAdmin = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// This method returns true if the person info is loaded sucessfully.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public Boolean Load(Int64 id)
        {           
                return true;           
        }
        /// <summary>
        /// This method returns true if the person info is saved sucessfully.
        /// </summary>
        /// <returns></returns>
        public Boolean Save()
        {           
                return true;            
        }   
        #endregion
    }
}
