﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide private variables and public methods
    /// </summary>
    public class Address
    {

        public Address()
        {
            addressID = 0;
            addressTypeID = 0;
            addressType = String.Empty;
            streetNumber = String.Empty;
            streetName = String.Empty;
            aptNo = String.Empty;
            city = String.Empty;
            state = String.Empty;
            zip = String.Empty;
            zipSuffix = String.Empty;
            isActive = String.Empty;
        }

        #region Private Variables
        
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int16 addressID;
        private Int16 addressTypeID;
        private String addressType;
        private String streetNumber;
        private String streetName;
        private String aptNo;
        private String city;
        private String state;
        private String zip;
        private String zipSuffix;
        private String isActive;

        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int16 AddressId
        {
            get { return addressID; }
            set { addressID = value; }
        }

        public Int16 AddressTypeId
        {
            get { return addressTypeID;}
            set { addressTypeID = value; }
        }

        public String AddressType
        {
            get { return addressType; }
            set { addressType = value; }
        }

        public String StreetNumber
        {
            get { return streetNumber; }
            set { streetNumber = value; }
        }

        public String StreetName
        {
            get { return streetName; }
            set { streetName = value; }
        }

        public String AptNo
        {
            get { return aptNo; }
            set { aptNo = value; }
        }

        public String City
        {
            get { return city; }
            set { city = value; }
        }

        public String State
        {
            get { return state; }
            set { state = value; }
        }

        public String ZipCode
        {
            get { return zip; }
            set { zip = value; }
        }

        public String ZipSuffix
        {
            get { return zipSuffix; }
            set { zipSuffix = value; }
        }

        public String IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }


        #endregion

        #region Public Methods
        
        #endregion
    }
}
