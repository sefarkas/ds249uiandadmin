﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to Load charges
    /// </summary>
    public class Charge
    {
        public Charge()
        {

            chargeTypeId = 0;
            chargeCode = String.Empty;
            chargeDescription = String.Empty;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int16 chargeTypeId;
        private String chargeCode;
        private String chargeDescription;

        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int16 ChargeTypeId
        {
            get { return chargeTypeId; }
            set { chargeTypeId = value; }
        }

        public String ChargeCode
        {
            get { return chargeCode; }
            set { chargeCode = value; }
        }

        public String ChargeDescription
        {
            get { return chargeDescription; }
            set { chargeDescription = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// This methods loads the list of charges based on the Charge code.
        /// </summary>
        /// <param name="ChargeCode"></param>
        /// <returns></returns>
        public bool Load(String chargeCode)
        {            
                return true;            
        }
        
        #endregion
    }
}
