﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    /// <summary>
    /// This class contains all private varaibles and public properties
    /// </summary>
    public class EmployeeVacationApprovalDetails
    {

        /// <summary>
        /// Private variables
        /// </summary>
        private VacationTypeDTO m_VacationType;

        private string m_Value;
        private DateTime m_DateApproved;
        private string m_SignOffUser;
        private int m_EmployeeVacationInfoId;


        /// <summary>
        /// Public property
        /// </summary>
        public VacationTypeDTO VacationType
        {
            get { return m_VacationType; }
            set { m_VacationType = value; }
        }
              

        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

        
        public DateTime DateApproved
        {
            get { return m_DateApproved; }
            set { m_DateApproved = value; }
        }
        

        public string SignOffUser
        {
            get { return m_SignOffUser; }
            set { m_SignOffUser = value; }
        }
               

        public int EmployeeVacationInfoId
        {
            get { return m_EmployeeVacationInfoId; }
            set { m_EmployeeVacationInfoId = value; }
        }
    }
}
