﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Drawing;
using DSNY.DSNYCP.DTO;


using DSNY.DSNYCP.Proxys;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functionality to load personal info, 
    /// laod personal based on the serach criteria, employee images based on the employee ID,
    /// assembles the list of Personnel info from the DTO and save  Personnel info
    /// </summary>
    public class Personnel: Person
    {

        Proxy proxy;

        public Personnel()
        {
            personnelID = -1;
            referenceNo = string.Empty;
            appointmentDate = DateTime.MinValue;
            isBCAD = String.Empty;
            chartNumber = string.Empty;
            payCodeID = -1;
            employeeID = -1;
            images = null;
            reffNo = -1;
            payCodeDesc = String.Empty;
            dsnyHireDate = DateTime.MinValue;
            nycHireDate = DateTime.MinValue;
            phoneNumber = string.Empty;
            leaveStatusCode = -1;            
            probationCode = -1;
            probationStartDate = DateTime.MinValue;
            probationEndDate = DateTime.MinValue;
            vacationSchedule = string.Empty;
            isActive = false;
            title = String.Empty;
            badgeNumber = String.Empty;
            titleID = -1;
            effectiveDate = DateTime.MinValue;
           
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64 personnelID;       
        private String referenceNo;
        private DateTime appointmentDate;
        private String isBCAD;
        private String chartNumber;
        private Int16 payCodeID;
        private Int64 employeeID;           
        private byte[] images;
        private Int64 reffNo;
        private String payCodeDesc;
        private DateTime dsnyHireDate;
        private DateTime nycHireDate;
        private String phoneNumber;
        private Int16 leaveStatusCode;        
        private Int16 probationCode;
        private DateTime probationStartDate;
        private DateTime probationEndDate;
        private String vacationSchedule;
        private Boolean isActive;
        private Address personnelAddress;
        private String badgeNumber;
        private String title;
        private Int64 titleID;
        private DateTime effectiveDate;

        #endregion

        #region Public Property

        public Int64 PersonnelId
        {
            get { return personnelID; }
            set { personnelID = value; }
        }

        public String ReferenceNumber
        {
            get { return referenceNo; }
            set { referenceNo = value; }
        }

        public DateTime AppointmentDate
        {
            get { return appointmentDate; }
            set { appointmentDate = value; }
        }

        public String IsBcad
        {
            get { return isBCAD; }
            set { isBCAD = value; }
        }

        public String ChartNumber
        {
            get { return chartNumber; }
            set { chartNumber = value; }
        }

        public Int16 PayCodeId
        {
            get { return payCodeID; }
            set { payCodeID = value; }
        }
        public Int64 EmployeeId
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public Byte[] Images
        {
            get { return images; }
            set { images = value; }
        }
        public Int64 ReferenceNo
        {
            get { return reffNo ; }
            set { reffNo = value; }
        }
        public String PayCodeDescription
        {
            get { return payCodeDesc; }
            set { payCodeDesc = value; }
        }

        public DateTime DsnyHireDate
        {
            get { return dsnyHireDate; }
            set { dsnyHireDate = value; }
        }

        public DateTime NycHireDate
        {
            get { return nycHireDate; }
            set { nycHireDate = value; }
        }

        public String PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        public Int16 LeaveStatusCode
        {
            get { return leaveStatusCode; }
            set { leaveStatusCode = value; }
        }

        public Int16 ProbationCode
        {
            get { return probationCode; }
            set { probationCode = value; }
        }

        public DateTime ProbationStartDate
        {
            get { return probationStartDate; }
            set { probationStartDate = value; }
        }

        public DateTime ProbationEndDate
        {
            get { return probationEndDate; }
            set { probationEndDate = value; }
        }

        public String VacationSchedule
        {
            get { return vacationSchedule; }
            set { vacationSchedule = value; }
        }

        public Boolean IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public Address PersonnelAddress
        {
            get { return personnelAddress; }
            set { personnelAddress = value; }
        }

        public String BadgeNumber
        {
            get { return badgeNumber; }
            set { badgeNumber = value; }
        }

        public String PersonnelTitle
        {
            get { return title; }
            set { title = value; }
        }

        public Int64 TitleId
        {
            get { return titleID; }
            set { titleID = value; }
        }
        public DateTime EffectiveDate
        {
            get { return effectiveDate; }
            set { effectiveDate = value; }
        }


        #endregion
        
        #region Public Methods
        /// <summary>
        /// This method loads the personnel details based on the personnel ID
        /// </summary>
        /// <param name="Id"></param>
        public new void Load(Int64 id)
        {
            proxy = new Proxy();
                List<PersonnelByIdDTO> personnelByID = proxy.LoadPersonnel(id);
                //commented for testing
                AssemblePersonnelByID(personnelByID);          
        }


        /// <summary>
        /// This method loads the personnel details based on the reffernce number
        /// </summary>
        /// <param name="RefNumber"></param>
        public void Load(String refNumber)
        {           
                proxy = new Proxy();
                List<PersonnelByRefNoDTO> personnelByRefNoDTO = proxy.LoadPersonnel(refNumber);
                //commented for testing
               AssemblePersonnel(personnelByRefNoDTO);           
        }

       
        /// <summary>
        /// This method loads the employee images based on the employee ID and the image data in bytes
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="EmployeeImage"></param>
        public void LoadEmployeeImage(Int32 id, Byte[] employeeImage)
        {            
                proxy = new Proxy();
                List<PersonnelByImageDTO> personnelImageDTO = proxy.LoadEmployeeImage(id, employeeImage);
                //commented for testing
                AssemblePersonnelImage(personnelImageDTO);           
        }


        private void AssemblePersonnelImage(List<PersonnelByImageDTO> personnelByImageDTO)
        {
            Address address;
                foreach (PersonnelByImageDTO personnelByImageList in personnelByImageDTO)
                {
                    this.ImageId = personnelByImageList.ImageID;
                    this.FirstName = personnelByImageList.FirstName;
                    this.MiddleName = personnelByImageList.MiddleName;
                    this.LastName = personnelByImageList.LastName;
                    this.Ssn = personnelByImageList.SSN;
                    this.Gender = personnelByImageList.Gender;
                    this.Dob = personnelByImageList.DOB;

                    this.PersonnelId = personnelByImageList.PersonnelID;
                    this.ReferenceNumber = personnelByImageList.ReferenceNo;
                    this.AppointmentDate = personnelByImageList.AppointmentDate;
                    this.PayCodeId = personnelByImageList.PayCodeID;
                    this.PayCodeDescription = personnelByImageList.PayCodeDescription;
                    this.IsBcad = personnelByImageList.IsBCAD;
                    this.ChartNumber = personnelByImageList.ChartNumber;
                    this.DsnyHireDate = personnelByImageList.DSNYHireDate;
                    this.NycHireDate = personnelByImageList.NYCHireDate;
                    this.PhoneNumber = personnelByImageList.PhoneNumber;
                    this.LeaveStatusCode = personnelByImageList.LeaveStatusCode;
                    this.ProbationCode = personnelByImageList.ProbationCode;
                    this.ProbationStartDate = personnelByImageList.ProbationStartDate;
                    this.ProbationEndDate = personnelByImageList.ProbationEndDate;
                    this.VacationSchedule = personnelByImageList.VacationSchedule;
                    this.IsActive = personnelByImageList.IsActive;
                    address = new Address();
                    address.AddressId = (short)personnelByImageList.Address.AddressID;
                    address.AddressTypeId = personnelByImageList.Address.AddressTypeID;
                    address.StreetNumber = personnelByImageList.Address.StreetNumber;
                    address.StreetName = personnelByImageList.Address.StreetName;
                    address.AptNo = personnelByImageList.Address.AptNo;
                    address.City = personnelByImageList.Address.City;
                    address.State = personnelByImageList.Address.State;
                    address.ZipCode = personnelByImageList.Address.ZipCode;
                    address.ZipSuffix = personnelByImageList.Address.ZipSuffix;
                    this.PersonnelAddress = address;

                    this.PersonnelTitle = personnelByImageList.PersonnelTitle;
                    this.BadgeNumber = personnelByImageList.BadgeNumber;
                    this.TitleId = personnelByImageList.TitleID;
                }           
        }

        /// <summary>
        /// This method assembles the list of Personnel info from the personnelByRefNoDTO
        /// </summary>
        /// <param name="dspersonnel"></param>
        private void AssemblePersonnel(List<PersonnelByRefNoDTO> personnelByRefNoDTO)
        {
                    Address address;
                foreach (PersonnelByRefNoDTO personnelByRefNoList in personnelByRefNoDTO)
                    {
                            this.ImageId  = personnelByRefNoList.ImageID;
                            this.FirstName = personnelByRefNoList.FirstName;
                            this.MiddleName = personnelByRefNoList.MiddleName;
                            this.LastName = personnelByRefNoList.LastName;
                            this.Ssn = personnelByRefNoList.SSN;
                            this.Gender = personnelByRefNoList.Gender;
                            this.Dob = personnelByRefNoList.DOB;
                       
                            this.PersonnelId = personnelByRefNoList.PersonnelID;
                            this.ReferenceNumber = personnelByRefNoList.ReferenceNo;
                            this.AppointmentDate = personnelByRefNoList.AppointmentDate;
                            this.PayCodeId = personnelByRefNoList.PayCodeID;
                            this.PayCodeDescription = personnelByRefNoList.PayCodeDescription;
                            this.IsBcad = personnelByRefNoList.IsBCAD;
                            this.ChartNumber = personnelByRefNoList.ChartNumber;
                            this.DsnyHireDate = personnelByRefNoList.DSNYHireDate;
                            this.NycHireDate = personnelByRefNoList.NYCHireDate;
                            this.PhoneNumber = personnelByRefNoList.PhoneNumber;
                            this.LeaveStatusCode = personnelByRefNoList.LeaveStatusCode;
                            this.ProbationCode = personnelByRefNoList.ProbationCode;
                            this.ProbationStartDate = personnelByRefNoList.ProbationStartDate;
                            this.ProbationEndDate = personnelByRefNoList.ProbationEndDate;
                            this.vacationSchedule = personnelByRefNoList.VacationSchedule;
                            this.IsActive = personnelByRefNoList.IsActive;
                            address = new Address();
                            address.AddressId =(short)personnelByRefNoList.Address.AddressID;
                            address.AddressTypeId = personnelByRefNoList.Address.AddressTypeID;
                            address.StreetNumber = personnelByRefNoList.Address.StreetNumber;
                            address.StreetName = personnelByRefNoList.Address.StreetName;
                            address.AptNo = personnelByRefNoList.Address.AptNo;
                            address.City = personnelByRefNoList.Address.City;
                            address.State = personnelByRefNoList.Address.State;
                            address.ZipCode = personnelByRefNoList.Address.ZipCode;
                            address.ZipSuffix = personnelByRefNoList.Address.ZipSuffix;
                            this.PersonnelAddress = address;

                            this.PersonnelTitle = personnelByRefNoList.PersonnelTitle;
                            this.BadgeNumber = personnelByRefNoList.BadgeNumber;
                            this.TitleId =personnelByRefNoList.TitleID;
                            this.EffectiveDate = personnelByRefNoList.EffectiveDate;                    
                    }                
            
        }
        private void AssemblePersonnelByID(List<PersonnelByIdDTO> personnelByIDTO)
        {           
                Address address;
                foreach (PersonnelByIdDTO personnelByIDList in personnelByIDTO)
                {
                    this.ImageId = personnelByIDList.ImageID;
                    this.FirstName = personnelByIDList.FirstName;
                    this.MiddleName = personnelByIDList.MiddleName;
                    this.LastName = personnelByIDList.LastName;
                    this.Ssn = personnelByIDList.SSN;
                    this.Gender = personnelByIDList.Gender;
                    this.Dob = personnelByIDList.DOB;
                    this.PersonnelId = personnelByIDList.PersonnelID;
                    this.ReferenceNumber = personnelByIDList.ReferenceNo;
                    this.AppointmentDate = personnelByIDList.AppointmentDate;
                    this.PayCodeId = personnelByIDList.PayCodeID;
                    this.PayCodeDescription = personnelByIDList.PayCodeDescription;
                    this.IsBcad = personnelByIDList.IsBCAD;
                    this.ChartNumber = personnelByIDList.ChartNumber;
                    this.DsnyHireDate = personnelByIDList.DSNYHireDate;
                    this.NycHireDate = personnelByIDList.NYCHireDate;
                    this.PhoneNumber = personnelByIDList.PhoneNumber;
                    this.LeaveStatusCode = personnelByIDList.LeaveStatusCode;
                    this.ProbationCode = personnelByIDList.ProbationCode;
                    this.ProbationStartDate = personnelByIDList.ProbationStartDate;
                    this.ProbationEndDate = personnelByIDList.ProbationEndDate;
                    this.VacationSchedule = personnelByIDList.VacationSchedule;
                    this.IsActive = personnelByIDList.IsActive;
                    address = new Address();
                    address.AddressId = (short)personnelByIDList.Address.AddressID;
                    address.AddressTypeId = personnelByIDList.Address.AddressTypeID;
                    address.StreetNumber = personnelByIDList.Address.StreetNumber;
                    address.StreetName = personnelByIDList.Address.StreetName;
                    address.AptNo = personnelByIDList.Address.AptNo;
                    address.City = personnelByIDList.Address.City;
                    address.State = personnelByIDList.Address.State;
                    address.ZipCode = personnelByIDList.Address.ZipCode;
                    address.ZipSuffix = personnelByIDList.Address.ZipSuffix;
                    this.PersonnelAddress = address;

                    this.PersonnelTitle = personnelByIDList.PersonnelTitle;
                    this.BadgeNumber = personnelByIDList.BadgeNumber;
                    this.TitleId = personnelByIDList.TitleID;
                }                
           
        }
        /// <summary>
        /// This method returns true if the Personnel info is saved sucessfully.
        /// </summary>
        /// <returns></returns>
        public new bool Save()
        {
          
                return true;            
        }   
        #endregion
    }
}
