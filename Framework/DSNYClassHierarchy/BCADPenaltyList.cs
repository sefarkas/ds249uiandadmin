﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide functionality to Remove BCAD Penalty,Update BCAD Penalty,Load BCAD PenaltyList and AssembleBCADPenaltyList
    /// </summary>
   public class BCADPenaltyList
    {
       private BcadPenalty _bcadPenalty;
        Proxy proxy;

       public BCADPenaltyList()
       {
           list = new List<BcadPenalty>();
           newUniqueID = 1;
           _bcadPenalty = new BcadPenalty();
       }

       /// <summary>
       /// Private Variable
       /// </summary>
       private List<BcadPenalty> list;
       private Int16 newUniqueID;
       private short modifiedUser;
       /// <summary>
       /// Public Property
       /// </summary>

       public BcadPenalty BPenalty
       {
           get { return _bcadPenalty; }
           set { _bcadPenalty = value; }
       }

       public List<BcadPenalty> PenaltyList
       {
           get { return list; }
           set { list = value; }
       }

       public Int16 NewUniqueId
       {
           get { return newUniqueID; }
           set { newUniqueID = value; }
       }

       public short ModifiedUser
       {
           get { return modifiedUser; }
           set { modifiedUser = value; }
       }

       /// <summary>
       /// This method returns true if the BCAD penalty is sucessfully added based on the BCAD ID
       /// </summary>
       /// <returns></returns>

       public Boolean AddBcadPenalty()
       {          
               _bcadPenalty = new BcadPenalty();
               if (list.Count == 0)
               {
                   _bcadPenalty.UniqueId = 1;
                   newUniqueID = 1;
               }
               else
               {
                   _bcadPenalty.UniqueId = ++newUniqueID;
               }
               list.Add(_bcadPenalty);
               return true;          
       }
       /// <summary>
       /// This method returns true if the BCAD penalty is sucessfully removed based on the BCAD ID
       /// </summary>
       /// <returns></returns>

       public Boolean RemoveBCADPenalty(Int64 UniqueID)
       {          
               _bcadPenalty = list.Find(delegate(BcadPenalty p) { return p.UniqueId == UniqueID; });
               list.Remove(_bcadPenalty);
               return true;          
       }
       /// <summary>
       /// This method returns true if the BCAD penalty is sucessfully updated based on the BCAD ID
       /// </summary>
       /// <returns></returns>

       public Boolean UpdateBCADPenalty(Int64 BCADPenaltyID)
       {           
               _bcadPenalty = list.Find(delegate(BcadPenalty p) { return p.PenaltyId == BCADPenaltyID; });
               return true;          
       }
       /// <summary>
       /// This method loads the list of BCAD complaints based on the BCAD case ID.
       /// </summary>
       /// <param name="BCADCaseID"></param>
       public void Load(Int64 bcadCaseId)
       {           
               if (proxy == null)
                   proxy = new Proxy();
               List<BCADPenaltyDTO> bcadPenaltyDTO = proxy.LoadBCADPenalty(bcadCaseId);
               AssembleBcadPenaltyList(bcadPenaltyDTO);          
       }
       /// <summary>
       /// This method assembles the list of BCAD penalty list from the DTO
       /// </summary>
       /// <param name="dsPenalyList"></param>
       public void AssembleBcadPenaltyList(List<BCADPenaltyDTO> bcadPenaltyDto)
       {
           if (list == null)
               list = new List<BcadPenalty>();
           
           Int16 i = 1;          
               foreach (BCADPenaltyDTO bcadPenaltyList in bcadPenaltyDto)
                   {
                       BcadPenalty bcadPenalty = new BcadPenalty();

                       bcadPenalty.UniqueId = i;
                       bcadPenalty.PenaltyGroupId = bcadPenaltyList.PenaltyGroupID;
                       bcadPenalty.PenaltyTypeCode = bcadPenaltyList.PenaltyTypeCode;
                       bcadPenalty.Days = bcadPenaltyList.Days;
                       bcadPenalty.Hours = bcadPenaltyList.Hours;
                       bcadPenalty.Minutes = bcadPenaltyList.Minutes;
                       bcadPenalty.MoneyValue = bcadPenaltyList.MoneyValue;
                       bcadPenalty.Notes = bcadPenaltyList.Notes;
                       list.Add(bcadPenalty);
                       i++;
                   }          
       }
       /// <summary>
       /// /// <summary>
       /// This method saves the BCAD penaly based on the BCAD case ID.
       /// </summary>
       /// <param name="BCADCaseID"></param>
       /// </summary>
       /// <param name="BCADCaseID"></param>
       /// <returns></returns>
       public bool Save(Int64 cadCaseId)
       {
            if (proxy == null)
                   proxy = new Proxy();
               foreach (BcadPenalty bcadPenalty in list)
               {
                   bcadPenalty.PenaltyGroupId = cadCaseId;
                   bcadPenalty.Save();
               }
               return true;          
       }


    }
}
