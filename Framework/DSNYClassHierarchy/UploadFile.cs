﻿using System;

using System.Collections.Generic;

using System.Linq;
using System.IO;
using System.Text;
using System.Security.Permissions;
using System.Data;
using System.Net;
using DSNY.DSNYCP.Proxys;

using DSNY.DSNYCP.DTO;




namespace DSNY.DSNYCP.ClassHierarchy
{

    public class UploadFile
    {

        Proxy proxy;

        private Int32 indexNo;

        private Int32 incidentId;

        private Int64 complaintId;

        private Int64 panelID;

        private byte[] image;           

        private string chargeCode;      

        private Int64 parentID;

        private string  childId;

        private string applicationName;

        private string attachmentName;

        private string contenttype;

        public List<UpLoadFileDTO> dsLoadComplaintAttachments;

        public UploadFile()
        {

            list = null;

        }

        List<UploadFile> list;

        public List<UploadFile> List
        {

            get { return list; }

            set { list = value; }

        }

        public Int32 IndexNo
        {

            get { return indexNo; }

            set { indexNo = value; }

        }

        public Int64 ComplaintId
        {

            get { return complaintId; }

            set { complaintId = value; }

        }



        public Int32 IncidentID
        {

            get { return incidentId; }

            set { incidentId = value; }

        }

        public Int64 PanelId
        {

            get { return panelID; }

            set { panelID = value; }

        }

        public byte[] Image
        {
            get { return image; }
            set { image = value; }
        }

        public string ChargeCode
        {
            get { return chargeCode; }
            set { chargeCode = value; }
        }           

        public Int64 ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }

        public string  ChildID
        {
            get { return childId; }
            set { childId = value; }
        }

        public string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }
        }

        public string AttachmentName
        {
            get { return attachmentName; }
            set { attachmentName = value; }
        }

        public string ContentType
        {
            get { return contenttype; }
            set { contenttype = value; }
        }
        public String ServerPath { get; set; }
        public String ImageLocation { get; set; }
        public DateTime ImageDate { get; set; }
        public String ImageDateString { get; set; }
        public String BlockLoc { get; set; }
        public String UsedBlockLoc { get; set; }
        public String WhenTaken { get; set; }
        public String TakenBy { get; set; }
        public String Note { get; set; }
        public String CCUNo { get; set; }
        public String District { get; set; }
        public String Location { get; set; }
        public String BlockNo { get; set; }
        public String LotNo { get; set; }
        public String House1{ get; set; }
		public String House2 { get; set; }
		public String StreetAddress { get; set; }     
		public String BoroCode { get; set; }  
		public String CCUYear { get; set; }
		public String CrossStreetOne { get; set; }
		public String CrossStreettwo  { get; set; }   
		public String Block { get; set; }
		public String Used_House1 { get; set; }
		public String Used_House2 { get; set; }
		public String Used_StreetAddress { get; set; }
		public String Used_CrossStreetOne { get; set; }
		public String Used_CrossStreetTwo  { get; set; }
        public String Zone { get; set; }
        public Int64 InspectionRequestId { get; set; }
        public Int64 LotId { get; set; }
        public Int64 DocumentId { get; set; }
        public String[] WhenTakenList
        {
            get;
            private set;
        }    

        public List<UploadFile> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID, string  ChildID, string AppName)
        {           
                if (proxy == null)
                    proxy = new Proxy();               
                dsLoadComplaintAttachments = proxy.LoadComplaintAttachments(DocumentID, ParentID, ChildID, AppName);
                List<UploadFile> fileUpload = AssembleComplaintAttachments(dsLoadComplaintAttachments);
                return fileUpload;          
        }

        public List<UploadFile> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID,Int64 LotId, string ChildID, string AppName,Int64 RequestId)
        {
            if (proxy == null)
                    proxy = new Proxy();
                dsLoadComplaintAttachments = proxy.LoadComplaintAttachments(DocumentID, ParentID,LotId, RequestId, ChildID, AppName);
                List<UploadFile> fileUpload = AssembleComplaintAttachmentsForLotCleaning(dsLoadComplaintAttachments);
                return fileUpload;           
        }      

       

        public byte[] GetImageFromLocation(String fileLocation)
        {
            FileStream fs = null;
            fileLocation = this.ServerPath + fileLocation;
            byte[] bytes = null;          
                if (!String.IsNullOrEmpty(fileLocation))
                {
                     fileLocation = fileLocation.Trim();
                    int index = fileLocation.LastIndexOf("\\");
                     int fileLength = fileLocation.Length - 1;
                    String fileName = fileLocation.Substring(index +1, (fileLength -index));
                    String directory = fileLocation.Substring(0, index + 1);                  
                  
                    DirectoryInfo di = new DirectoryInfo(directory);
                    if (di.Exists)
                    {
                        FileInfo[] fis = di.GetFiles(fileName);
                        if (fis.Length == 1)
                        {
                            fs = fis[0].OpenRead();
                            bytes = new byte[fs.Length];
                            IAsyncResult asyncResult = fs.BeginRead(bytes, 0, (int)fs.Length, null, null);
                            while (!asyncResult.IsCompleted) 
                            {
                                continue;
                            }
                            fs.Close();
                        }
                    }                   
                   
                    else if (File.Exists(fileLocation))
                    {
                        using (fs = new FileStream(fileLocation,
                                    FileMode.Open, FileAccess.Read))
                        {
                            bytes = new byte[fs.Length];
                            IAsyncResult asyncResult = fs.BeginRead(bytes, 0, (int)fs.Length, null, null);
                            while (!asyncResult.IsCompleted) 
                            {
                                continue;
                            }
                        }
                    }
                }
                return bytes;           
        }

        private List<UploadFile> AssembleComplaintAttachments(List<UpLoadFileDTO> listUpLoadFile)
        {          

                if (list == null)
                    list = new List<UploadFile>();
                UploadFile uploadfile;
                foreach (UpLoadFileDTO lUpLoadFile in listUpLoadFile)
                {
                    uploadfile = new UploadFile();
                    uploadfile.IndexNo  = lUpLoadFile.IndexNo;
                    uploadfile.ParentID = lUpLoadFile.ParentID;                
                    uploadfile.Image = lUpLoadFile.Image;                  
                    uploadfile.ChildID = lUpLoadFile.ChildID;
                    uploadfile.ApplicationName = lUpLoadFile.ApplicationName;
                    uploadfile.AttachmentName = lUpLoadFile.AttachmentName;
                    uploadfile.ContentType = lUpLoadFile.ContentType;
                    uploadfile.ImageDate = lUpLoadFile.ImageDate;
                    uploadfile.ImageDateString = lUpLoadFile.ImageDate.ToString("MM/dd/yyyy");
                    uploadfile.Note = lUpLoadFile.Note;
                    uploadfile.WhenTaken = lUpLoadFile.WhenTaken;
                    uploadfile.TakenBy = lUpLoadFile.TakenBy;                
                    uploadfile.District = lUpLoadFile.District;
                    uploadfile.Location = lUpLoadFile.Location;
                    uploadfile.BlockNo = lUpLoadFile.BlockNo;                               
                    uploadfile.CCUNo = lUpLoadFile.CCUNo;
                    uploadfile.Zone = lUpLoadFile.Zone;
                    uploadfile.Used_CrossStreetTwo = lUpLoadFile.Used_CrossStreetTwo;
                    uploadfile.Used_CrossStreetOne = lUpLoadFile.Used_CrossStreetOne;
                    uploadfile.Used_StreetAddress = lUpLoadFile.Used_StreetAddress;
                    uploadfile.Used_House2 = lUpLoadFile.Used_House2;
                    uploadfile.Used_House1 = lUpLoadFile.Used_House1;
                    uploadfile.Block = lUpLoadFile.Block;
                    uploadfile.CrossStreettwo = lUpLoadFile.CrossStreettwo;
                    uploadfile.CrossStreetOne = lUpLoadFile.CrossStreetOne;
                    uploadfile.CCUYear = lUpLoadFile.CCUYear;
                    uploadfile.BoroCode = lUpLoadFile.BoroCode;
                    uploadfile.StreetAddress = lUpLoadFile.StreetAddress;
                    uploadfile.House2 = lUpLoadFile.House2;
                    uploadfile.House1 = lUpLoadFile.House1;
                    uploadfile.LotNo = lUpLoadFile.LotNo;
                    uploadfile.InspectionRequestId = lUpLoadFile.InspectionRequestId;
                    uploadfile.LotId = lUpLoadFile.LotId;
                    list.Add(uploadfile);
                }
                return list;           
        }


        private List<UploadFile> AssembleComplaintAttachmentsForLotCleaning(List<UpLoadFileDTO> listUpLoadFile)
        {
           
                if (list == null)

                    list = new List<UploadFile>();
                UploadFile uploadfile;
                foreach (UpLoadFileDTO lUpLoadFile in listUpLoadFile)
                {
                    uploadfile = new UploadFile();                   
                    uploadfile.IndexNo = lUpLoadFile.IndexNo;
                    uploadfile.ParentID = lUpLoadFile.ParentID;
                    uploadfile.ImageLocation = lUpLoadFile.ImageLocation;
                    uploadfile.Image = GetImageFromLocation(lUpLoadFile.ImageLocation);
                    uploadfile.ChildID = lUpLoadFile.ChildID;
                    uploadfile.ApplicationName = lUpLoadFile.ApplicationName;
                    uploadfile.AttachmentName = lUpLoadFile.AttachmentName;
                    uploadfile.DocumentId = lUpLoadFile.DocumentID;
                    uploadfile.LotId = lUpLoadFile.LotId;
                    uploadfile.BlockLoc = lUpLoadFile.BlockLoc;
                    uploadfile.UsedBlockLoc = lUpLoadFile.UsedBlockLoc;
                    uploadfile.ContentType = lUpLoadFile.ContentType;
                    uploadfile.ImageDate = lUpLoadFile.ImageDate;
                    uploadfile.ImageDateString = lUpLoadFile.ImageDate.ToString("MM/dd/yyyy");
                    uploadfile.Note = lUpLoadFile.Note;
                    uploadfile.WhenTaken = lUpLoadFile.WhenTaken;
                    uploadfile.TakenBy = lUpLoadFile.TakenBy;
                    uploadfile.District = lUpLoadFile.District;
                    uploadfile.Location = lUpLoadFile.Location;
                    uploadfile.BlockNo = lUpLoadFile.BlockNo;
                    uploadfile.CCUNo = lUpLoadFile.CCUNo;
                    uploadfile.Zone = lUpLoadFile.Zone;
                    uploadfile.Used_CrossStreetTwo = lUpLoadFile.Used_CrossStreetTwo;
                    uploadfile.Used_CrossStreetOne = lUpLoadFile.Used_CrossStreetOne;
                    uploadfile.Used_StreetAddress = lUpLoadFile.Used_StreetAddress;
                    uploadfile.Used_House2 = lUpLoadFile.Used_House2;
                    uploadfile.Used_House1 = lUpLoadFile.Used_House1;
                    uploadfile.Block = lUpLoadFile.Block;
                    uploadfile.CrossStreettwo = lUpLoadFile.CrossStreettwo;
                    uploadfile.CrossStreetOne = lUpLoadFile.CrossStreetOne;
                    uploadfile.CCUYear = lUpLoadFile.CCUYear;
                    uploadfile.BoroCode = lUpLoadFile.BoroCode;
                    uploadfile.StreetAddress = lUpLoadFile.StreetAddress;
                    uploadfile.House2 = lUpLoadFile.House2;
                    uploadfile.House1 = lUpLoadFile.House1;
                    uploadfile.LotNo = lUpLoadFile.LotNo;
                    uploadfile.InspectionRequestId = lUpLoadFile.InspectionRequestId;
                    list.Add(uploadfile);
                }
                return list;           
        }

        public List<UploadFile> LoadComplaintAttachments(Int64 DocumentID, Int64 ParentID, string ChildID)
        {           

                if (proxy == null)

                    proxy = new Proxy();

                dsLoadComplaintAttachments = proxy.LoadComplaintAttachments(DocumentID, ParentID, ChildID);

                List<UploadFile> fileUpload = AssembleComplaintAttachments(dsLoadComplaintAttachments);

                return fileUpload;
        }

        public bool DeleteSupportedDocument(Int64 ParentID)
        {
            if (proxy == null)
                proxy = new Proxy();
            return proxy.DeleteSupportedDocument(ParentID);
        }               
      
        public bool UploadFiles()
        {
            if (proxy == null)
                proxy = new Proxy();           
            bool IsUploaded = false;           
                      IsUploaded = proxy.UploadFiles(ParentID,LotId, InspectionRequestId,Image,ServerPath, ImageLocation,
                        ApplicationName, ContentType, AttachmentName, ChildID,
                        ImageDate, WhenTaken, TakenBy, Note);                    
                return IsUploaded;          
        }
    }
}


