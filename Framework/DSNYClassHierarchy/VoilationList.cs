﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides the functionality to Add, remove and update the violation
    /// loads the violation on the basis of complaint ID,provide the list of Voilation from the DTO
    /// saves the violation with respect to the the complaint ID
    /// and provide the list of Incedents based on the complaintID and Charge ID
    /// </summary>
    public class VoilationList
    {

        Voilation voilation;
        Proxy proxy;

        public VoilationList()
        {
            list = new List<Voilation>();
            newUniqueID = 1;
        }

       
        List<Voilation> list;
        Int16 newUniqueID;

        /// <summary>
        /// Public Property
        /// </summary>
        public List<Voilation> List
        {
            get { return list; }
            set { list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return newUniqueID; }
            set { newUniqueID = value; }
        }

         /// <summary>
        /// This method returns true if the violation is succesfully added
         /// </summary>
         /// <returns></returns>
        public Boolean Addviolation()
        {           
                voilation = new Voilation();
                if (list.Count == 0)
                {
                    voilation.UniqueId = 1;
                    newUniqueID = 1;
                }
                else
                {
                    voilation.UniqueId = ++newUniqueID; 
                }
                list.Add(voilation);
                return true;            
        }
        /// <summary>
        /// This method returns true if the violation is succesfully removed on the basis of violation ID
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public Boolean Removevoilation(Int64 UniqueID)
        {            
                voilation = list.Find(delegate(Voilation p) { return p.UniqueId == UniqueID; });
                list.Remove(voilation);
                return true;           
        }
        /// <summary>
        /// This method returns true if the violation is succesfully updated on the basis of violation ID
        /// </summary>
        /// <param name="voilationID"></param>
        /// <returns></returns>
        public Boolean Updatevoilation(Int64 voilationID)
        {            
                voilation = list.Find(delegate(Voilation p) { return p.ViolationId == voilationID; });
                return true;           
        }
        /// <summary>
        /// This method loads the violation on the basis of complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        public void Load(Int64 complaintId)
        {
            List<ViolationDTO> violationsDTO;            
                if (proxy == null)
                    proxy = new Proxy();
                violationsDTO = proxy.LoadViolations(complaintId);
                AssembleViolationList(violationsDTO);           
        }
        /// <summary>
        /// This method assembles the list of Voilation from the DTO.
        /// </summary>
        /// <param name="dsViolations"></param>
        public void AssembleViolationList(List<ViolationDTO> violationDto)
        {
            if (list == null)
                list = new List<Voilation>();           
                Voilation violation;
                Charge charge;
                
                Int16 i = 1;
                foreach (ViolationDTO vList in violationDto)
                    {
                        violation = new Voilation();
                        charge = new Charge();
                        violation.UniqueId = i;
                            violation.ChargeId = vList.ChargeID;
                            violation.ComplaintId = vList.ComplaintID;
                            violation.IncidentLocationId = vList.IncidentLocationID;
                            violation.IncidentType = vList.IncidentType;
                            charge.ChargeCode = vList.Charge.ChargeCode;

                            if (vList.Charge.ChargeDescription != String.Empty)
                        {
                            charge.ChargeDescription = charge.ChargeCode + " " + vList.Charge.ChargeDescription;
                        
                            violation.ViolationDescription = charge.ChargeCode + " " + vList.Charge.ChargeDescription;                            
                        }

                            violation.OverRideViolationDescription = vList.OverRideVoilationDesc ;

                        violation.IncidentLocationName = vList.IncidentLocationName;
                            violation.PrimaryIndicator = vList.PrimaryIndicator;
                            
                        violation.Charge = charge;
                        
                        violation.Incidents = new IncedentList();
                        violation.Incidents.List = GetIncidentList(violation.ComplaintId,violation.ChargeId);
                        violation.Incidents.NewUniqueId = Convert.ToInt16(violation.Incidents.List.Count);
                        violation.Incidentdate = vList.IncidentDate;
                        list.Add(violation);
                        i++;
                    }               
        }

        /// <summary>
        /// This method provides the list of Incedents based on the complaintID and Charge ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="ChargeID"></param>
        /// <returns></returns>
        private List<Incedent> GetIncidentList(Int64 ComplaintID, Int64 ChargeID)
        {
            IncedentList incidents = new IncedentList();
            incidents.Load(ComplaintID, ChargeID);            
            return incidents.List;
        }
        /// <summary>
        /// This method saves the violation with respect to the the complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool Save(Int64 complaintId)
        {           
                Int16 i = 1;
                if (proxy == null)
                    proxy = new Proxy();
                foreach (Voilation voilation in list)
                {                    
                    voilation.ComplaintId = complaintId;
                    voilation.Save();
                    foreach (Incedent incident in voilation.Incidents.List)
                    {
                        incident.Count = i;
                        incident.ChargeId = voilation.ChargeId;
                        incident.ComplaintId = complaintId;
                        incident.Save();
                        i = 0;
                    }
                }
                return true;           
        }
    }
}
