﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class contains all private varaibles and public properties
    /// </summary>
    public class EmployeeVacationDetails
    {

        /// <summary>
        /// Private variable
        /// </summary>
        private VacationType m_VacationType;

        private DateTime m_FromDate;
        private DateTime m_ToDate;
        private int m_NoOfDays;
        private string m_Value;

        /// <summary>
        /// Public property
        /// </summary>
        public VacationType VacationType
        {
            get { return m_VacationType; }
            set { m_VacationType = value; }
        }        

        public DateTime FromDate
        {
            get { return m_FromDate; }
            set { m_FromDate = value; }
        }        

        public DateTime ToDate
        {
            get { return m_ToDate; }
            set { m_ToDate = value; }
        }        

        public int NoOfDays
        {
            get { return m_NoOfDays; }
            set { m_NoOfDays = value; }
        }
        

        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

    }
}
