﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide functionality to loads the list of DS249 complaints based on the complaint ID,
    /// method saves the DS249 complaint, method creates a new DS249 complaint,method creates a new DS249 complaint.
    /// </summary>
    public class Complainant
    {
        Proxy proxy;
        ComplainantDTO compDTO;
        /// <summary>
        /// Variables for Complaint Statement.
        /// </summary>
        public Complainant()
        {
            uniqueID = 0;
            complainantID = 0;
            complaintID = -1;
            employeeID = -1;
            complainantName = String.Empty;
            statement = String.Empty;
            complainantType = String.Empty;
            locationID = -1;
            titleID = -1;
            refNo = String.Empty;
            locationName = String.Empty;
            title = String.Empty;

           
            onDate = DateTime.MinValue;
            text = String.Empty;
            thisDate = DateTime.MinValue;
            category = String.Empty;
            reportedDate = DateTime.MinValue;
            fromLocation = String.Empty;
            reportedTime = String.Empty;
            reportingTime = String.Empty;
            telephone = String.Empty;
        }
        
        #region Private Variables
        /// <summary>
        /// Private variables for Complaint Statement.
        /// </summary>
        private Int16 uniqueID;
        private Int16 complainantID;
        private Int64 complaintID;
        private Int64 employeeID;
        private String complainantName;
        private String statement;
        private String complainantType;
        private Int64 locationID;
        private Int64 titleID;
        private String refNo;

        private String locationName;
        private String title;

       
        private DateTime onDate;
        private string text;
        private DateTime thisDate;
        private String category;
        private DateTime reportedDate;
        private String fromLocation;
        private String reportedTime;
        private String reportingTime;
        private String telephone;

        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int16 UniqueId
        {
            get { return uniqueID; }
            set { uniqueID = value; }
        }
        
        public Int16 ComplainantId
        {
            get {return complainantID; }
            set { complainantID = value; }
        }

        public Int64 ComplaintId
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int64 EmployeeId
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public String ComplainantName
        {
            get { return complainantName; }
            set { complainantName = value; }
        }

        public String Statement
        {
            get { return statement; }
            set { statement = value; }
        }

        public String ComplaintType
        {
            get { return complainantType; }
            set { complainantType = value; }
        }

        public Int64 LocationId
        {
            get { return locationID; }
            set { locationID = value; }
        }

        public String LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public Int64 TitleId
        {
            get { return titleID; }
            set { titleID = value; }
        }

        public String Title
        {
            get { return title; }
            set { title = value; }
        }

        public String RefNo
        {
            get { return refNo;}
            set{refNo =value;}
        }


        //Property for Complaint Statement.

        public DateTime OnDate
        {
            get { return onDate; }
            set { onDate = value; }
        }

        public String Text
        {
            get { return text; }
            set { text = value; }
        }

        public DateTime ThisDate
        {
            get { return thisDate; }
            set { thisDate = value; }
        }

        public String Category
        {
            get { return category; }
            set { category = value;}
        }

        public DateTime ReportedDate
        {
            get { return reportedDate; }
            set { reportedDate = value; }
        }

        public String FromLocation
        {
            get { return fromLocation; }
            set { fromLocation = value; }
        }

        public String ReportedTime
        {
            get { return reportedTime; }
            set { reportedTime = value; }
        }

        public String ReportingTime
        {
            get { return reportingTime; }
            set { reportingTime = value; }
        }

        public String TelephoneNo
        {
            get { return telephone; }
            set { telephone = value; }
        }

        #endregion

        #region Public Method
        /// <summary>
        /// This method loads the list of DS249 complaints based on the complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool Load(Int64 complaintId)
        {           
                return true;            
        }

        /// <summary>
        /// This method saves the DS249 complaint
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {                          
                if (proxy == null)
                    proxy = new Proxy();
                CreateComplainant();
                return proxy.SaveComplainant(compDTO);            
        }
        /// <summary>
        /// This method creates a new DS249 complaint
        /// </summary>
        private void CreateComplainant()
        {
            compDTO = new ComplainantDTO();
            compDTO.ComplaintID = this.complaintID;
            compDTO.RefNo = this.refNo;
            compDTO.ComplainantName = this.complainantName;
            compDTO.UniqueID = this.uniqueID;
            compDTO.LocationID = this.locationID;
            compDTO.TitleID = this.titleID;
            compDTO.Statement = this.statement;
            compDTO.OnDate = this.onDate;
            compDTO.Text = this.text;
            compDTO.ThisDate = this.thisDate;
            compDTO.Category = this.category;
            compDTO.ReportedDate = this.reportedDate;
            compDTO.FromLocation = this.fromLocation;
            compDTO.ReportedTime = this.reportedTime;
            compDTO.ReportingTime = this.reportingTime;
            compDTO.TelephoneNo = this.telephone;

        }
        #endregion


    }
}
