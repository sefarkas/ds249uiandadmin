﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
   public class HideChargesList
    {
       Proxy proxy;
       List<HideChargesDto> hideChargeDto = new List<HideChargesDto>();
       public HideChargesList()
      {
          viewChargeId = -1;
          chargeTypeId = -1;
          employeeId = -1;
          chargeDescription = string.Empty;
      }

        #region Private Variables

        private Int16 viewChargeId;
        private Int16 chargeTypeId;
        private Int16 employeeId;
        private String chargeDescription;
        private String referenceNumber;
        #endregion

        #region Public Property

        public Int16 ViewChargeId
        {
            get { return viewChargeId; }
            set { viewChargeId = value; }
        }

        public Int16 ChargeTypeId
        {
            get { return chargeTypeId; }
            set { chargeTypeId = value; }
        }

        public Int16 EmployeeId
        {
            get { return employeeId; }
            set { employeeId = value; }
        }

        public String ChargeDescription
        {
            get { return chargeDescription; }
            set { chargeDescription = value; }
        }

        public String ReferenceNumber
        {
            get { return referenceNumber; }
            set { referenceNumber = value; }
        }


        #endregion

        #region Methods
       /// <summary>
       /// Return list of charges 
       /// </summary>
       /// <param name="employeeId"></param>
       /// <returns></returns>
        public List<HideChargesList> Load(Int16 employeeId)
        {           
                proxy = new Proxy();
                List<HideChargesList> hidechargesDto = new List<HideChargesList>();
                hideChargeDto = proxy.LoadHideCharges(employeeId);
                foreach (HideChargesDto hideList in hideChargeDto)
                {
                    HideChargesList hideChargesList = new HideChargesList();
                    hideChargesList.ViewChargeId = hideList.ViewChargeId;
                    hideChargesList.EmployeeId = hideList.EmployeeId;
                    hideChargesList.ChargeTypeId = hideList.ChargeTypeId;
                    hideChargesList.ChargeDescription = hideList.ChargeDescription;
                    hideChargesList.referenceNumber = hideList.ReferenceNumber;
                    hidechargesDto.Add(hideChargesList);
                }
                return hidechargesDto;           
        }
       /// <summary>
       /// Save Hide Charges
       /// </summary>
       /// <param name="referenceNo"></param>
       /// <param name="employeeId"></param>
       /// <returns></returns>
        public bool SaveHideCharges(String referenceNo, Int16 employeeId)
        {
                proxy = new Proxy();              
               return  proxy.SaveHideCharges(referenceNo, employeeId);              
           
        }


       /// <summary>
       /// Function delete charges from database
       /// </summary>
       /// <param name="viewChargesId"></param>
       /// <returns></returns>
        public bool DeleteHideCharges(Int16 viewChargesId)
        {
            proxy = new Proxy();
            return proxy.DeleteHideCharges(viewChargesId);           
        }


        #endregion

    }
}
