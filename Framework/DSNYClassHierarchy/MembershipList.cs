﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functionality to load, save, remove, add roles, 
    /// get roles to the membership list and assemble membership List
    /// </summary>
   public class MembershipList
    {
       private Membership membership;
       Proxy proxy;       

       public MembershipList()
       {
           list = new List<Membership>();
       }

       private List<Membership> list;


       public List<Membership> List
       {
           get { return list; }
           set { list = value; }
       }
       /// <summary>
       /// This method loads the list of members based on the personal ID
       /// </summary>
       /// <param name="PersonalID"></param>
       public void Load(Int64 personalId)
       {
           List<MembershipDTO> dsMemberships;           
                if (proxy == null)
                    proxy = new Proxy(true);
                dsMemberships = proxy.GetMembership(personalId);
                AssembleUserMembershipList(dsMemberships);           
       }
       /// <summary>
       /// This method loads the list of members
       /// </summary>
       public void Load()
       {
           List<MembershipDTO> dsMemberships;           
               if (proxy == null)
                   proxy = new Proxy(true);
               dsMemberships = proxy.GetMembership();
               AssembleMembershipList(dsMemberships);               
          
       }

       public bool Save()
       {
           
           return true;
       }
       /// <summary>
       /// This method Add the members to the list based on membership ID and personal ID
       /// </summary>
       /// <param name="MembershipID"></param>
       /// <param name="PersonalID"></param>
       public void AddMembership(Int64 membershipId, Int64 personalId)
       {
           Membership membership = new Membership();
           membership.MembershipId = membershipId;
           membership.PersonalId = personalId;
           this.list.Add(membership);
       }
       /// <summary>
       /// This method remove the members from the list based on the membershipID 
       /// </summary>
       /// <param name="membershipId"></param>
       public void RemoveMembership(Int64 membershipId)
       {
           Membership membership = this.list.Find(delegate(Membership p) { return p.MembershipId == membershipId; });                    
           this.list.Remove(membership);
       }
       /// <summary>
       /// This method Add roles to the members based on membership ID and role ID
       /// </summary>
       /// <param name="membershipId"></param>
       /// <param name="roles"></param>
       public void AddRolesToMembership(Int64 membershipId, List<Int16> roles)
       {
           Membership membership = this.list.Find(delegate(Membership p) { return p.MembershipId == membershipId; });
           if (membership != null)
           {
               RoleList roleList = new RoleList();
               Role role;
               foreach (Int16 roleId in roles)
               {
                   role = new Role();
                   role.RoleId = roleId;
                   role.MembershipId = membership.MembershipId;
                   role.PersonalId = membership.PersonalId;
                   roleList.List.Add(role);
               }
               membership.Roles = roleList;
           }
       }
       /// <summary>
       /// This method assembles the list of members from the DTO.
       /// </summary>
       /// <param name="dsMemberships"></param>
       private void AssembleMembershipList(List<MembershipDTO> listMemberships)
       {          
               foreach (MembershipDTO mlist in listMemberships)                   
                   {
                       membership = new Membership();
                     membership.MembershipId= mlist.MembershipID;
                     membership.MembershipDescription= mlist.MembershipDesc;
                           list.Add(membership);
                   }           
       }
       /// <summary>
       /// This method assembles the list of User Membership List from the DTO.
       /// </summary>
       /// <param name="dsMemberships"></param>
       private void AssembleUserMembershipList(List<MembershipDTO> listMemberships)
       {
                  foreach (MembershipDTO mlist in listMemberships)                   
                   {
                       membership = new Membership();
                       membership.MembershipId=mlist.MembershipID;
                       membership.PersonalId=mlist.PersonalID;
                       membership.MembershipDescription=mlist.MembershipDesc;
                       membership.Roles = new RoleList();
                       membership.Roles.List = GetRoles(membership.MembershipId, membership.PersonalId);
                       list.Add(membership);            
                   }         
       }
       /// <summary>
       /// This method gets the list of roles based on membership ID and personal ID
       /// </summary>
       /// <param name="MembershipID"></param>
       /// <param name="PersonalID"></param>
       /// <returns></returns>
       private List<Role> GetRoles(Int64 MembershipId, Int64 PersonalId)
       {
           RoleList roles = new RoleList();
           roles.Load(MembershipId, PersonalId);
           return roles.List;
       }
       
    }
}
