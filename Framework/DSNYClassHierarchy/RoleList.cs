﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.Proxys;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide method to return the list of roles nases on the MembershipID and PersonalID,
    /// method to assembles the list of roles list from the DTO and method to assembles the list of roles list from the DTO.
    /// </summary>
    public class RoleList
    {
        Proxy proxy;
        Role role;

        public RoleList()
        {
            list = new List<Role>();
        }
        /// <summary>
        /// Private variable
        /// </summary>
        private List<Role> list;
        /// <summary>
        /// Public property
        /// </summary>
        public List<Role> List
        {
            get { return list; }
            set { list = value; }
        }

        /// <summary>
        /// This method will return the list of roles
        /// </summary>
        public void Load(int membershipID)
        {
            List<RoleDTO> Roles;           
                if (proxy == null)
                    proxy = new Proxy(true);
                Roles = proxy.GetRoles(membershipID);
                AssembleRoles(Roles);
            
        }
        /// <summary>
        /// This method will return the list of roles nases on the MembershipID and PersonalID
        /// </summary>
        /// <param name="MembershipID"></param>
        /// <param name="PersonalID"></param>
        public void Load(Int64 membershipId, Int64 personalId)
        {
            List<MembershipDTO> dsRoles;           
                if (proxy == null)
                    proxy = new Proxy(true);
                dsRoles = proxy.GetMembershipRole(membershipId, personalId);
                AssembleRoleList(dsRoles);           
        }
        /// <summary>
        /// This method assembles the list of roles list from the DTO.
        /// </summary>
        /// <param name="dsRoles"></param>
        private void AssembleRoleList(List<MembershipDTO> membershipRoles)
        {           
                foreach (MembershipDTO mlist in membershipRoles)                   
                    {
                        role = new Role
                        {
                            Id = mlist.ID,
                            PersonalId = mlist.PersonalID,
                            MembershipId = mlist.MembershipID,
                            RoleId = mlist.RoleID,
                            RoleDescription = mlist.RoleDesc
                        };
                        list.Add(role);
                    }            
        }

        /// <summary>
        /// This method assembles the list of roles from the DTO.
        /// </summary>
        /// <param name="dsRoles"></param>
        private void AssembleRoles(List<RoleDTO> listRoles)
        {           
           
                foreach (RoleDTO rlist in listRoles)                   
                    {
                        role = new Role
                        {
                            RoleId = rlist.RoleID,
                            RoleDescription = rlist.RoleDesc
                        };
                        list.Add(role);

                    }           
        }

    }
}
