﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{

    /// <summary>
    /// This class provides functinality to loads the list of code based on the category ID
    /// loads the list of load pay code,loads the list of codes BCAD case status,assembles the list of code list from the DTO
    /// assembles the list of pay code list from the DTO.
    /// </summary>
    public class CodeList
    {

        Proxy proxy;

        public CodeList()
        {
            list = null;
        }

        /// <summary>
        /// Private Variable
        /// </summary>

        List<Code> list;
        List<Code> payCodeList;

        /// <summary>
        /// Public Property
        /// </summary>

        public List<Code> CList
        {
            get { return list; }
            set { list = value; }
        }

        public List<Code> PayCodeList
        {
            get { return payCodeList; }
            set { payCodeList = value; }
        }

        /// <summary>
        /// This method loads the list of code based on the category ID
        /// </summary>
        /// <param name="CategoryID"></param>



        public void Load(Int16 categoryId)
        {          
                if (proxy == null)
                    proxy = new Proxy();
                List<CodeDTO> codeListDTO = proxy.LoadCodeList(categoryId);
                AssembleCodeList(codeListDTO);           
        }
        /// <summary>
        /// This method loads the list of load pay code
        /// </summary>
        public void LoadPayCodes()
        {            
                if (proxy == null)
                    proxy = new Proxy();
                List<PayCodeDTO> payCodeDTO = proxy.LoadPayCodeList();
                AssemblePayCodeList(payCodeDTO);            
        }

        
        /// <summary>
        /// This method loads the list of codes BCAD case status
        /// </summary>
        public void Load()
        {           
                if (proxy == null)
                    proxy = new Proxy();
                List<BCADStatusDTO> bcadcaseStatusListDTO = proxy.LoadBcadCaseStatus();
                AssembleBCADStatutsList(bcadcaseStatusListDTO);           
        }
        private void AssembleBCADStatutsList(List<BCADStatusDTO> bcadStatusDTO)
        {
            if (list == null)
                list = new List<Code>();           
                Code code;
                foreach (BCADStatusDTO listBCADStatus in bcadStatusDTO)
                {
                    code = new Code();
                    code.CodeId = listBCADStatus.CodeID;
                    code.CodeName = listBCADStatus.CodeName;
                    code.CodeDescription = listBCADStatus.CodeDescription;
                    list.Add(code);
                }           
        }

        /// <summary>
        /// This method assembles the list of code list from the DTO.
        /// </summary>
        /// <param name="dsCodeList"></param>
        private void AssembleCodeList(List<CodeDTO> codeDTO)
        {
            if (list == null)
                list = new List<Code>();
              Code code;
                foreach (CodeDTO listCode in codeDTO)
                    {
                        code = new Code();
                        code.CodeId = listCode.CodeID;
                        code.CodeName = listCode.CodeName;
                        code.CodeDescription = listCode.CodeDescription;
                        code.ValueDataType = listCode.ValueDataType;
                        list.Add(code);
                    }            
        }

        /// <summary>
        /// This method assembles the list of pay code list from the DTO.
        /// </summary>
        /// <param name="payCodeList"></param>
        private void AssemblePayCodeList(List<PayCodeDTO> payCodeDto)
        {
            if (payCodeList == null)
                payCodeList = new List<Code>();
           
                Code code;
                foreach (PayCodeDTO payCodeListDTO in payCodeDto)
                    {
                        code = new Code();
                        code.CodeId = payCodeListDTO.PayCodeId;
                            code.CodeDescription = payCodeListDTO.PayCodeDesc;
                        payCodeList.Add(code);
                    }           
        }

    }
}
