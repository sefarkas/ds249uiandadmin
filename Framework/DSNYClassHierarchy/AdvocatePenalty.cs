﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    ///  This class will provide functionality to save and Create Advocate penalty.
    /// </summary>
    public class AdvocatePenalty
    {
        Proxy proxy;
        AdvocatePenaltyDTO penaltyDTO;
        public AdvocatePenalty()
        {
            _uniqueID = -1;
            _advocatePenaltyID = -1;
            _advocacyCaseID = -1;
            _penaltyTypeCd = -1;
            _dataValue = String.Empty;
            _suspensionDays = 0;
            _fineDays = 0;
            _applyToTotal = false;
            _demotedRank = String.Empty;
            _acd = String.Empty;
            _vacDays = 0;
            _combpen = false;
            _randomTest = false;
            _timeServed = 0;
            _comments = String.Empty;
            _term_desc = string.Empty;
            _term= 0;
            _value_data_type = string.Empty;
        }
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int16 _uniqueID;
        private Int64 _advocatePenaltyID;
        private Int64 _advocacyCaseID;
        private Int64 _penaltyTypeCd;
        private String _dataValue;
        private Int16 _suspensionDays;
        private Int16 _fineDays;
        private Boolean _applyToTotal;        
        private String _demotedRank;
        private String _acd;
        private Int16 _vacDays;
        private Boolean _combpen;
        private Boolean _randomTest;
        private Int16 _timeServed;
        private Int16 _term;
        private String _term_desc;
        private String _comments;
        private String _value_data_type; 
        /// <summary>
        /// Public property
        /// </summary>
        public Int16 UniqueId
        {
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }
        public Int64 AdvocatePenaltyId
        {
            get { return _advocatePenaltyID; }
            set { _advocatePenaltyID = value; }
        }
        public Int64 AdvocacyCaseId
        {
            get { return _advocacyCaseID; }
            set { _advocacyCaseID = value; }
        }
        public Int64 PenaltyTypeCD
        {
            get { return _penaltyTypeCd; }
            set { _penaltyTypeCd = value; }
        }
        public String  DataValue
        {
            get { return _dataValue; }
            set { _dataValue = value; }
        }
     
        public String  Value_Data_Type
        {
            get { return _value_data_type; }
            set { _value_data_type = value; }
        }
        public Int16 Term
        {
            get { return _term; }
            set { _term = value; }
        }
        public String Term_Desc
        {
            get { return _term_desc; }
            set { _term_desc = value; }
        }
        public Int16 SuspensionDays
        {
            get { return _suspensionDays; }
            set { _suspensionDays = value; }
        }
        public Int16 FineDays
        {
            get { return _fineDays; }
            set { _fineDays = value; }
        }

        public Boolean ApplyToTotal
        {
            get { return _applyToTotal; }
            set { _applyToTotal = value; }
        }
        
        public String DemotedRank
        {
            get { return _demotedRank; }
            set { _demotedRank = value; }
        }
        public String ACD
        {
            get { return _acd; }
            set { _acd = value; }
        }
        public Int16 VacDays
        {
            get { return _vacDays; }
            set { _vacDays = value; }
        }
        public Boolean CombPen
        {
            get { return _combpen; }
            set { _combpen = value; }
        }
        public Boolean RandomTest
        {
            get { return _randomTest; }
            set { _randomTest = value; }
        }
        public Int16 TimeServed
        {
            get { return _timeServed; }
            set { _timeServed = value; }
        }
        
        public String PenaltyComments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        
        /// <summary>
        /// This method saves the new advocate penalty created
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            if (proxy == null)
                proxy = new Proxy();
            CreateAdvocatePenalty();
            
            return true;
        }

        /// <summary>
        /// This method creates the new advocate penalty.
        /// </summary>
        private void CreateAdvocatePenalty()
        {
            penaltyDTO = new AdvocatePenaltyDTO();
            penaltyDTO.UniqueID = this._uniqueID;
            penaltyDTO.AdvocatePenaltyID = this._advocatePenaltyID;
            penaltyDTO.AdvocacyCaseID = this._advocacyCaseID;
            penaltyDTO.PenaltyTypeCd = this._penaltyTypeCd;
            penaltyDTO.DataValue = this._dataValue;
            penaltyDTO.SuspensionDays = this._suspensionDays;
            penaltyDTO.FineDays = this._fineDays;
            penaltyDTO.ApplyToTotal = this._applyToTotal;
            penaltyDTO.Term = this._term;
            penaltyDTO.Term_Desc = this._term_desc;            
            penaltyDTO.DemotedRank = this._demotedRank;
            penaltyDTO.ACD = this._acd;
            penaltyDTO.VacDays = this._vacDays;
            penaltyDTO.CombPen = this._combpen;
            penaltyDTO.RandomTest = this._randomTest;
            penaltyDTO.TimeServed = this._timeServed;
            penaltyDTO.Comments = this._comments;
            penaltyDTO.Term =this._term;
            penaltyDTO.Term_Desc = this._term_desc;

        }

      
    }
}
