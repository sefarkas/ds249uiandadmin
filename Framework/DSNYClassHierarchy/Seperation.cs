﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    ///  This class contains all private varaibles and public properties
    /// </summary>
    [Serializable]
    public class Seperation
    {
        /// <summary>
        /// Private variables
        /// </summary>
        private string m_SeperationType;
        private int m_SeperationTypeId;
        /// <summary>
        /// public property
        /// </summary>
        public string SeparationType
        {
            get { return m_SeperationType; }
            set { m_SeperationType = value; }
        }
        
        public int SeparationTypeId
        {
            get { return m_SeperationTypeId; }
            set { m_SeperationTypeId = value; }
        }
    }
}
