﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides boolean functionality to Load memebership.
    /// </summary>
    public class Membership
    {
        public Membership()
        {
            personalID = -1;
            membershipID = -1;
            membership = String.Empty;
            roles = new RoleList();
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64 personalID;
        private Int64 membershipID;
        private String membership;
        private RoleList roles;       


        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int64 PersonalId
        {
            get { return personalID; }
            set { personalID = value; }
        }

        public Int64 MembershipId
        {
            get { return membershipID; }
            set { membershipID = value; }
        }

        public String MembershipDescription
        {
            get { return membership; }
            set { membership = value; }
        }

        public RoleList Roles
        {
            get { return roles;}
            set { roles = value; }
        }

        public String RolesAsString
        {
            get{            
                String tempRoles =  string.Empty;
                if (roles.List.Count > 0)
                {
                    foreach (Role role in roles.List)
                    {
                        tempRoles += role.RoleId.ToString() + ",";
                    }
                    return tempRoles.Substring(0, tempRoles.Length - 1);
                }
                else
                {
                    return string.Empty;
                }
        }
        
    }

        #endregion

        #region Public Method

        public bool Load()
        {
            return true;
        }

        #endregion
    }
}
