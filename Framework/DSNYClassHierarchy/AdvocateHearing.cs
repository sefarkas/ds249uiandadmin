﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class will provide functionality to Save advocate hearing
    /// </summary>
    public class AdvocateHearing
    {
        Proxy proxy;
        public AdvocateHearing()
        {
            _uniqueID = 0;
            _advocateHearingID = -1;
            _advocacyCaseID = -1;
            _hearingDt = DateTime.MinValue;
            _hearingStatusCd = -1;
            _calStatusCd = -1;
            _hearingTypeCd = -1;
            _hearingComments = String.Empty;
            _modifiedBy = -1;
            _modifiedDt = DateTime.MinValue;
        }

        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64 _uniqueID; // 16 --> 64 by Farkas 28Feb2020
        private Int64 _advocateHearingID;
        private Int64 _advocacyCaseID;
        private DateTime _hearingDt;
        private Int64 _hearingStatusCd;
        private Int64 _calStatusCd;
        private Int64 _hearingTypeCd;
        private String _hearingComments;
        private Int64 _modifiedBy; // 16 --> 64 by Farkas 28Feb2020
        private DateTime _modifiedDt;

        /// <summary>
        /// Public property
        /// </summary>
        public Int64 UniqueId
        { // 16 --> 64 by Farkas 28Feb2020
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }

        public Int64 AdvocateHearingId
        {
            get { return _advocateHearingID; }
            set { _advocateHearingID = value; }
        }
        public Int64 AdvocacyCaseId
        {
            get { return _advocacyCaseID; }
            set { _advocacyCaseID = value; }
        }
        public DateTime? HearingDate {get; set;}
        //{
        //    get { return _hearingDt; }
        //    set { _hearingDt = value; }
        //}
        public Int64 HearingStatusCD
        {
            get { return _hearingStatusCd; }
            set { _hearingStatusCd = value; }
        }
        public Int64 CalStatusCD
        {
            get { return _calStatusCd; }
            set { _calStatusCd = value; }
        }
        public Int64 HearingTypeCD
        {
            get { return _hearingTypeCd; }
            set { _hearingTypeCd = value; }
        }
        public String HearingComments
        {
            get { return _hearingComments; }
            set { _hearingComments = value; }
        }
        public Int64 ModifiedBy
        { // 16 --> 64 by Farkas 28Feb2020
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _modifiedDt; }
            set { _modifiedDt = value; }
        }
        /// <summary>
        /// This method returns true if the hearing are sucessfully loaded based on the Charge ID
        /// </summary>
        public Boolean Load(Int64 chargeId)
        {
           
                return true;           
        }

        /// <summary>
        /// This method saves the new hearing created 
        /// </summary>
        public void Save()
        {          
                if (proxy == null)
                    proxy = new Proxy();
                    CreateHearing();            
        }

        private void CreateHearing()
        {
            
        }
    }
}
