﻿using System;
using DT= DSNY.DSNYCP.DTO;
using System.Collections;
using DSNY.DSNYCP.Proxys;
using System.Collections.Generic;
using CH = DSNY.DSNYCP.ClassHierarchy;
namespace DSNY.DSNYCP.ClassHierarchy
{

    public class AppError
    {
        Proxy proxy;
        public long ErrorID { get; set; }
        public string Url { get; set; }
        public string RequestType { get; set; }
        public string UserID { get; set; }
        public string ErrorMessage { get; set; }
        public string StackTrace { get; set; }
        //public List<DT.AppErrorDTO> dsAppErrorLog;
           

         public int SaveErrorLog()
            {
                
                    if (proxy == null)
                        proxy = new Proxy();

                  
                    return proxy.SaveErrorLog(this.Url, this.RequestType, this.UserID, this.ErrorMessage, this.StackTrace, null);
            }            
    }
}
