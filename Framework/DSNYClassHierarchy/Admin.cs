﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide functionality to send account information email 
    /// and to reset password
    /// </summary>
    public class Admin
    {
        Email emailSvc = new Email();
        public String[] ToAddress { get; set; }
        /// <summary>
        /// This method sends email from the Admin to the user with the temporary password.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        public void SendAccountInformationEmail(String userName, String password)
        {
            StringBuilder emailBody = new StringBuilder();
            emailBody.Append("*****PLEASE DO NOT REPLY TO THIS EMAIL******");
            emailBody.Append(Environment.NewLine);
            emailBody.Append(Environment.NewLine);
            emailBody.Append("Your Sign in ID is: " + userName);
            emailBody.Append(Environment.NewLine);
            emailBody.Append("Your Temporary Password is: " + password);
            emailBody.Append(Environment.NewLine);
            emailSvc.ToAddress = ToAddress;
            emailSvc.EmailBody = emailBody.ToString();
            emailSvc.SendEmail();
        }
        /// <summary>
        /// This method sends email from the Admin to the user with the temporary password.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="emailID"></param>
        public void SendResetPasswordEmail(String userName, String password, String emailId)
        {

            StringBuilder emailBody = new StringBuilder();
            emailBody.Append("*****PLEASE DO NOT REPLY TO THIS EMAIL******");
            emailBody.Append(Environment.NewLine);
            emailBody.Append(Environment.NewLine);
            emailBody.Append("Your Sign in ID is: " + userName);
            emailBody.Append(Environment.NewLine);
            emailBody.Append("Your Temporary Password is: " + password);
            emailBody.Append(Environment.NewLine);
            emailSvc.ToAddress = new String[1];
            emailSvc.ToAddress.SetValue(emailId, 0);
            emailSvc.EmailBody = emailBody.ToString();

            emailSvc.SendEmail();
        }
    }
}
