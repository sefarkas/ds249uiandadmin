﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class will provide functionality to Save associated complaints
    /// </summary>
    public class AdvocateAssociatedComplaint
    {
        Proxy proxy;

        public AdvocateAssociatedComplaint()
        {
            _uniqueID = 0;
            _associatedID = -1;
            _advocacyCaseID = -1;
            _asscociatedCompaintID = -1;
        }

        #region Private Variables
        
        /// <summary>
        /// Private Variables
        /// </summary>
        
        private Int64 _associatedID;
        private Int64 _advocacyCaseID;
        private Int64 _asscociatedCompaintID;
        private Int16 _uniqueID;
        
        #endregion

        /// <summary>
        /// Public property
        /// </summary>
        public Int16 UniqueId
        {
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }

        public Int64 AssociatedId 
        {
            get { return _associatedID; }
            set { _associatedID = value; }
        }
        
        public Int64 AdvocacyCaseId
        {
            get { return _advocacyCaseID; }
            set { _advocacyCaseID = value; }
        }
        public Int64 AssociatedComplaintId
        {
            get { return _asscociatedCompaintID; }
            set { _asscociatedCompaintID = value; }
        }

        /// <summary>
        /// This method saves the advocate complaint.
        /// </summary>
        public void Save()
        {            
                if (proxy == null)
                    proxy = new Proxy();
                CreateAssociatedComplaint();           
        }

        private void CreateAssociatedComplaint()
        {
            
        }

    }
}
