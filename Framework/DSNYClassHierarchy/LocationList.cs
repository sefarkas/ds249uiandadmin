﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functionality to Loads loaction,loads the loaction based on the Borough
    /// and assembles the list of locations from the DTO.
    /// </summary>
    public class LocationList
    {
        Proxy proxy;

        public LocationList()
        {
            list = null;
        }
        /// <summary>
        /// Private List
        /// </summary>
        private List<LocationDS> list;
        /// <summary>
        /// Public property
        /// </summary>
        public List<LocationDS> List
        {
            get { return list; }
            set { list = value; }
        }
        /// <summary>
        /// This method loads the loaction
        /// </summary>
        public void Load()
        {           
                if (proxy == null)
                    proxy = new Proxy();
                List<LocationDTO> dsLocations = proxy.LoadLocations();
                AssembleLocationList(dsLocations);           
        }
        /// <summary>
        /// This method loads the loaction based on the Borough
        /// </summary>
        /// <param name="OnlyBorough"></param>
        public void Load(Boolean onlyBorough)
        {            
                if (proxy == null)
                    proxy = new Proxy();
                List<BoroughDTO> dsLocations = proxy.LoadBoroughs();
                AssembleBoroughList(dsLocations);           
        }
        /// <summary>
        /// This method assembles the list of locations from the DTO.
        /// </summary>
        /// <param name="dslocationList"></param>
        private void AssembleLocationList(List<LocationDTO> locationList)
        {
            if (list == null)
                list = new List<LocationDS>();           
                LocationDS location;
                foreach (LocationDTO listValue in locationList)
                {
                    location = new LocationDS();
                    
                    location.LocationId = listValue.LocationID;
                    location.LocationName = listValue.LocationName;
                    location.LocationParentId = listValue.LocationParentID;
                    list.Add(location);
                }          
        }

        /// <summary>
        /// This method assembles the list of boroughs from the DTO
        /// </summary>
        /// <param name="boroughList"></param>
        private void AssembleBoroughList(List<BoroughDTO> boroughList)
        {
            if (list == null)
                list = new List<LocationDS>();          
                LocationDS location;
                foreach (BoroughDTO listValue in boroughList)
                {
                    location = new LocationDS();
                    location.LocationId = listValue.LocationID;
                    location.LocationName = listValue.LocationName;                   
                    location.LocationParentId = listValue.LocationParentID;
                    list.Add(location);
                }          
        }
    }
}
