﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    public class Witness
    {
        /// <summary>
        /// This class provides functionality load, create and save witeneses.
        /// </summary>
        Proxy proxy;
        private WitnessDto witDTO;
        public Witness()
        {
            uniqueID = 0;
            witnessID = -1;
            complaintID = -1;
            personnelID = -1;
            personnelName = String.Empty;
            refNo = String.Empty;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int16 uniqueID;
        private Int64 witnessID;
        private Int64 complaintID;
        private Int64 personnelID;
        private String personnelName;
        private String refNo;

        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int16 UniqueId
        {
            get { return uniqueID; }
            set { uniqueID = value; }
        }
        public Int64 WitnessId
        {
            get { return witnessID; }
            set { witnessID = value; }
        }

        public Int64 ComplaintId
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int64 PersonnelId
        {
            get { return personnelID;}
            set { personnelID = value; }
        }

        public String PersonnelName
        {
            get { return personnelName; }
            set { personnelName = value; }
        }

        public String RefNo
        {
            get { return refNo; }
            set { refNo = value; }
        }

        #endregion

        #region Pubic Methods
        /// <summary>
        /// This method returns true if the Witness are loaded sucessfully
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public Boolean Load(Int64 complaintId)
        {            
                return true;          
        }
        /// <summary>
        /// This method returns true if the Witness are Saved sucessfully based on complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public Boolean Save(Int64 complaintId)
        {           
                this.complaintID = complaintId;
                if (proxy == null)
                    proxy = new Proxy();
                CreateWitness();
                return proxy.SaveWitness(witDTO);         
        }
        /// <summary>
        /// This method will create witnesses
        /// </summary>
        private void CreateWitness()
        { 
            witDTO = new WitnessDto();
            witDTO.ComplaintID = this.complaintID;
            witDTO.RefNo = this.refNo;
            witDTO.PersonnelName = this.personnelName;
            witDTO.UniqueID = this.uniqueID;
        }
        
        #endregion

    }
}
