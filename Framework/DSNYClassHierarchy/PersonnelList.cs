﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Collections.ObjectModel;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functionality to load personal info, load personal info based on the search criteria
    /// Load employee Info and Assemble Employees Info
    /// </summary>
    public class PersonnelList
    {
        Proxy proxy;      

        private Personnel _personnel;
        public PersonnelList()
        {
            list = null;
        }

        public Personnel Personnel
        {
            get { return _personnel; }
            set { _personnel = value; }
        }

        List<Personnel> list;

        public List<Personnel> List
        {
            get { return list; }
            set { list = value; }
        }
        /// <summary>
        /// This methods loads the list of Personnel details based on the search criteria.
        /// </summary>
        /// <param name="SearchString"></param>
        /// <returns></returns>
        public bool Load(String searchString)
        {           
                if (proxy == null)
                    proxy = new Proxy();
                List<PersonnelListDTO> personnelListDTO = proxy.LoadPersonnelList(searchString);
                AssemblePersonnelList(personnelListDTO);
                return true;           
        }

        

        /// <summary>
        ///This methods loads the list of Personnel details 
        /// 
        /// </summary>
        /// <param name="Partial"></param>
        /// <returns></returns>
        public bool Load(bool partial)
        {
              if (proxy == null)
                    proxy = new Proxy();
                List<PersonnelDTO> personDTO = proxy.LoadPersonnelList();
                AssemblePersonnelListWithoutSearchString(personDTO);
                return true;           
        }
                

        /// <summary>
        /// This method assembles the list of personnels without search criteria from the DTO
        /// </summary>
        /// <param name="dsPersonnelDTO"></param>
        private void AssemblePersonnelListWithoutSearchString(Collection<PersonnelDTO> dsPersonnelDTO)
        {
            if (list == null)
                list = new List<Personnel>();          
                Personnel personnel;

                foreach (PersonnelDTO pList in dsPersonnelDTO)
                {
                    personnel = new Personnel();
                    personnel.FirstName = pList.FirstName;
                    personnel.MiddleName = pList.MiddleName;
                    personnel.LastName = pList.LastName;
                    personnel.PersonId = pList.PersonId;  
                    list.Add(personnel);
                }                
        }

        /// <summary>
        /// This method assembles the list of personnels without search criteria from the DTO
        /// </summary>
        /// <param name="dsPersonnelDTO"></param>
        private void AssemblePersonnelListWithoutSearchString(List<PersonnelDTO> dsPersonnelDTO)
        {
            if (list == null)
                list = new List<Personnel>();           
                Personnel personnel;
                foreach (PersonnelDTO pList in dsPersonnelDTO)
                {
                    personnel = new Personnel();
                    personnel.FirstName = pList.FirstName;
                    personnel.MiddleName = pList.MiddleName;
                    personnel.LastName = pList.LastName;
                    personnel.PersonnelId = pList.PersonnelID;
                    personnel.ReferenceNumber = pList.ReferenceNumber;
                    list.Add(personnel);
                }           
        }
        /// <summary>
        /// This methods loads the list of employees based on the employeeID.
        /// </summary>
        /// <param name="ID"></param>
        public void LoadEmployeeInfo(Int64 id)
        {            
                if (proxy == null)
                    proxy = new Proxy();
                List<EmployeeInfoDTO> employeeInfoDTO = proxy.LoadEmployeeInfo(id);
                AssembleEmployeesInfo(employeeInfoDTO);           
        }
        /// <summary>
        /// This methods loads the list of employees
        /// </summary>
        /// <param name="Partial"></param>
        /// <returns></returns>
        public bool LoadEmployees(bool partial)
        {           
                if (proxy == null)
                    proxy = new Proxy();
                List<EmployeeListDTO> employeesDTO = proxy.LoadEmployeeList();
                AssembleEmployeesList(employeesDTO);
                return true;         
        }

        /// <summary>
        /// This method assembles the list of Employees from the DTO.
        /// </summary>
        /// <param name="dsEmployees"></param>
        private void AssembleEmployeesInfo(List<EmployeeInfoDTO> employeeDTO)
        {
            if (list == null)
                list = new List<Personnel>();        

                foreach (EmployeeInfoDTO employeeinfoList in employeeDTO)
                {
                    _personnel = new Personnel();
                    _personnel.FirstName = employeeinfoList.FirstName;
                    _personnel.LastName = employeeinfoList.LastName;
                    _personnel.ImageId = employeeinfoList.ImageID;
                    byte[] array = (byte[])employeeinfoList.Images;
                    _personnel.Images = array;
                    _personnel.ReferenceNo = employeeinfoList.ReferenceNo;
                    list.Add(_personnel);
                }            
        }
        /// <summary>
        /// This method assembles the list of employees from the DTO.
        /// </summary>
        /// <param name="dsEmployees"></param>
        private void AssembleEmployeesList(List<EmployeeListDTO> employeesListDTO)
        {
            if (list == null)
                list = new List<Personnel>();
              Personnel personnel;                
                foreach (EmployeeListDTO employeeList in employeesListDTO)
                    {
                        personnel = new Personnel();
                            personnel.FirstName = employeeList.FirstName;
                            personnel.LastName = employeeList.LastName;
                            personnel.ImageId = employeeList.ImageID;
                            personnel.ReferenceNo = employeeList.ReferenceNo;
                        list.Add(personnel);
                    }           
        }


        /// <summary>
        /// This method assembles the list of Personnel from the DTO.
        /// </summary>
        /// <param name="dsPersonnelList"></param>
        private void AssemblePersonnelList(List<PersonnelListDTO> dsPersonnelDTO)
        {
            if (list == null)
                list = new List<Personnel>();            
                Personnel personnel;
                foreach (PersonnelListDTO pList in dsPersonnelDTO)
                {
                    personnel = new Personnel();
                    personnel.FirstName = pList.FirstName;
                    personnel.MiddleName = pList.MiddleName;
                    personnel.LastName = pList.LastName;
                    personnel.PersonnelId = pList.PersonnelID;
                    personnel.ReferenceNumber = pList.ReferenceNumber;
                    list.Add(personnel);
                }          
        }

        private void AssembleSearchListForLotCleaning(List<PersonnelListDTO> dsPersonnelDTO)
        {
            if (list == null)
                list = new List<Personnel>();           
                Personnel personnel;
                foreach (PersonnelListDTO pList in dsPersonnelDTO)
                {
                    personnel = new Personnel();
                    personnel.FirstName = pList.FirstName;
                    personnel.LastName = pList.LastName;
                    personnel.EmployeeId = pList.PersonnelID;                  
                    list.Add(personnel);
                }            
        }

        /// <summary>
        /// This method loads all the Personnel info details based on the search criteria.
        /// </summary>
        /// <param name="SearchString"></param>
        /// <returns></returns>
        public bool LoadAll(String searchString)
        {             if (proxy == null)
                    proxy = new Proxy();
                List<PersonnelDTO> dsPerson = proxy.LoadAllPersonnel(searchString);
                AssembleAllPersonnel(dsPerson);
                return true;        
        }

        /// <summary>
        /// This method assembles the list of Personnel list from the DTO.
        /// </summary>
        /// <param name="dsPersonnelList"></param>
        private void AssembleAllPersonnel(List<PersonnelDTO> dsPerson)
        {           
            if (list == null)
                list = new List<Personnel>();
            foreach (PersonnelDTO plist in dsPerson)
            {
                Personnel = new Personnel();
                Personnel.PersonId = plist.PersonnelID;
                Personnel.ReferenceNumber = plist.ReferenceNumber;
                Personnel.UserName = plist.UserName;
                Personnel.FirstName = plist.FirstName;
                Personnel.LastName = plist.LastName;
                Personnel.Ssn = plist.SSN;
                Personnel.IsActive = plist.IsActive;
                Personnel.PersonnelTitle = plist.PersonnelTitle;
                Personnel.PayCodeDescription = plist.PayCodeDescription;
                Personnel.EmailId = plist.EmailID;
                Personnel.IsDeleted = plist.IsDeleted;
                Personnel.IsAdmin = plist.IsAdmin;
                list.Add(Personnel);
             }            
        }
    }
}
