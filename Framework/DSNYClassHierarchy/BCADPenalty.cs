﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide functionality to Save BCAD penalty and creates the BCAD penalty.
    /// </summary>
    public class BcadPenalty
    {
        Proxy proxy;
        BCADPenaltyDTO penaltyDTO;
        public BcadPenalty()
        {
            uniqueID = 0;
            penaltyID = -1;
            penaltyGroupID = -1;
            penaltyTypeCode = 0;
            days = String.Empty;
            hours = String.Empty;
            minutes = String.Empty;
            moneyValue = String.Empty;
            notes = String.Empty;
            modifiedDate = DateTime.MinValue;
            modifiedUser = 0;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int16 uniqueID;
        private Int64 penaltyID;
        private Int64 penaltyGroupID;
        private Int16 penaltyTypeCode;
        private String days;
        private String hours;
        private String minutes;
        private String moneyValue;
        private String notes;
        private int modifiedUser;
        private DateTime modifiedDate;
        
        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int16 UniqueId
        {
            get { return uniqueID; }
            set { uniqueID = value; }
        }
        
        public Int64 PenaltyId
        {
            get { return penaltyID; }
            set { penaltyID = value; }
        }

        public Int64 PenaltyGroupId
        {
            get { return penaltyGroupID; }
            set { penaltyGroupID = value; }
        }

        public Int16 PenaltyTypeCode
        {
            get { return penaltyTypeCode; }
            set { penaltyTypeCode = value; }
        }

        public String Days
        {
            get { return days; }
            set { days = value; }
        }

        public String Minutes
        {
            get { return minutes; }
            set { minutes = value; }
        }

        public String Hours
        {
            get { return hours; }
            set { hours = value; }
        }

        public String MoneyValue
        {
            get { return moneyValue; }
            set { moneyValue = value; }
        }

        public String Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        public int ModifiedUser
        {
            get { return modifiedUser; }
            set { modifiedUser = value; }
        }

        public DateTime ModifiedDate
        {
            get { return modifiedDate; }
            set { modifiedDate = value; }
        }
        #endregion


        #region Public Methods
        /// <summary>
        /// This method saves the BCAD penalty. 
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            if (proxy == null)
                proxy = new Proxy();
            CreateBCADPenalty();
            return proxy.SaveBCADPenalty(penaltyDTO);
        }
        /// <summary>
        /// This method creates the BCAD penalty.
        /// </summary>
        private void CreateBCADPenalty()
        {
            penaltyDTO = new BCADPenaltyDTO();
            penaltyDTO.UniqueID = this.uniqueID;
            penaltyDTO.PenaltyID = this.PenaltyId;
            penaltyDTO.PenaltyGroupID = this.PenaltyGroupId;
            penaltyDTO.PenaltyTypeCode = this.PenaltyTypeCode;
            penaltyDTO.Days = this.Days;
            penaltyDTO.Hours = this.hours;
            penaltyDTO.Minutes = this.minutes;
            penaltyDTO.MoneyValue = this.moneyValue;
            penaltyDTO.Notes = this.notes;
            penaltyDTO.ModifiedDate = this.modifiedDate;
            penaltyDTO.ModifiedUser = this.modifiedUser;
        }

        #endregion

    }
}
