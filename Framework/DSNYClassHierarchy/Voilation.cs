﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides functionality to validate the Incident Date based on single, Married or retired
    /// method to load the violation  based on the complaint ID, method to load the violation  based on the complaint ID, 
    /// method to save the violation, method to create the violation, method to create the violation charges
    /// </summary>
   public class Voilation
    {

       Proxy proxy;
       ViolationDTO violation;
       public Voilation()
       {
           uniqueID = 0;
           chargeID = -1;
           complaintID = -1;
           voilationID = -1;
           voilationDesc = String.Empty;
           overrideVoilationDesc = String.Empty;
           charge = null;
           overideCharge = null;
           primaryInd = false;
           incidentLocationID = -1;
           incidentLocationName = String.Empty;
           incidentType = String.Empty;
           incedentList = null;
           incidentDate = DateTime.MinValue;
       }

        #region Private Variables
           /// <summary>
           /// Private Variables
           /// </summary>
            private Int64 chargeID;
            private Int16 uniqueID;            
            private Int64 complaintID;
            private Int64 voilationID;
            private String voilationDesc;
            private String overrideVoilationDesc;
            private Charge charge;
            private Charge overideCharge;
            private Boolean primaryInd;
            private Int64 incidentLocationID;
            private String incidentLocationName;
            private String incidentType;
            private IncedentList incedentList;
            private DateTime incidentDate;   
        #endregion


        #region Public Property
       /// <summary>
            /// Public Property
       /// </summary>
            public Int16 UniqueId
            {
                get { return uniqueID; }
                set { uniqueID = value; }
            }

            public Int64 ChargeId
            {
                get { return chargeID; }
                set { chargeID = value; }
            }
            public Int64 ComplaintId
            {
                get { return complaintID; }
                set { complaintID = value; }
            }
       
            public Int64 ViolationId
            {
                get { return voilationID; }
                set { voilationID = value; }
            }

            public String ViolationDescription
            {
                get { return voilationDesc; }
                set { voilationDesc = value; }
            }

            public String OverRideViolationDescription 
            {
                get { return overrideVoilationDesc; }
                set { overrideVoilationDesc = value; }
            }

            public Charge Charge
            {
                get { return charge; }
                set { charge = value; }
            }

            public Charge OverRideCharge
            {
                get { return overideCharge; }
                set { overideCharge = value; }
            }

            public Boolean PrimaryIndicator
            {
                get { return primaryInd; }
                set { primaryInd = value; }
            }

            public Int64 IncidentLocationId
            {
                get { return incidentLocationID; }
                set { incidentLocationID = value; }
            }

            public String IncidentLocationName
            {
                get { return incidentLocationName; }
                set { incidentLocationName = value; }
            }
       
            public String IncidentType
            {
                get { return incidentType; }
                set { incidentType = value; }
            }

            public IncedentList Incidents
            {
                get { return incedentList; }
                set { incedentList = value; }
            }

            public DateTime Incidentdate
            {
                get { return incidentDate; }
                set { incidentDate = value; }
            }
            /// <summary>
            /// This method will validate the Incident Date based on single, Married or retired 
            /// </summary>
            public String ParseIncidentDateAsString
            {
                get {

                    String dates = String.Empty;                    

                    foreach (Incedent incident in incedentList.List)
                    {
                        switch (this.IncidentType)
                        {
                            case "S":
                                if (incident.IncidentDate != DateTime.MinValue)
                                    dates = incident.IncidentDate.ToShortDateString();
                                return dates;
                            case "M":
                                dates = dates + incident.IncidentDate.ToShortDateString() + ", ";
                                break;
                            case "R":
                                if (incident.IncidentDate != DateTime.MinValue)
                                    dates = dates + incident.IncidentDate.ToShortDateString() + " ~ ";
                                break;
                        }
                    }
                    if (dates.Length > 1)
                        return dates.Substring(0, dates.Length - 2);
                    else
                        return dates;
                }
            }
        #endregion

        #region Public Method
         
         /// <summary>
         /// This method will load the violation  based on the complaint ID
         /// </summary>
         /// <param name="ComplaintID"></param>
         /// <returns></returns>
         public Boolean Load(Int64 complaintId)
            {               
                    return true;                
            }
          /// <summary>
         /// This method will save the violation
          /// </summary>
 
          public void Save()
            {               
                    if (proxy == null)
                        proxy = new Proxy();
                    CreateViolation();
                    proxy.SaveVoilation(violation);
                    this.chargeID = violation.ChargeID;                  
               
            }
           /// <summary>
          /// This method will create the violation
           /// </summary>
            private void CreateViolation()
            {
                violation = new ViolationDTO();
                violation.ComplaintID = this.complaintID;
                violation.Charge = CreateCharge();
                violation.UniqueID = this.uniqueID;
                violation.IncidentLocationID = this.incidentLocationID;
                violation.IncidentType = this.incidentType;
            }
           /// <summary>
            /// This method will create the violation charges
           /// </summary>
           /// <returns></returns>
            private ChargeDTO CreateCharge()
            {
                ChargeDTO charge = new ChargeDTO();
                if (this.charge != null)
                {
                    charge.ChargeCode = this.violation.Charge.ChargeCode;
                    charge.ChargeDescription = this.charge.ChargeDescription;
                    charge.ChargeTypeID = this.Charge.ChargeTypeId;
                }
                return charge;
            }
        #endregion
    }
}
