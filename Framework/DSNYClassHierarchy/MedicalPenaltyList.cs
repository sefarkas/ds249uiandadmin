﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
 public class MedicalPenaltyList
    {
        private MedicalPenalty _medicalPenalty;
        Proxy proxy;
        public MedicalPenaltyList()
        {
            list = new List<MedicalPenalty>();
            newUniqueID = 1;
            _medicalPenalty = new MedicalPenalty();
        }
        private List<MedicalPenalty> list;
        private Int16 newUniqueID;
        private short modifiedUser;



        public MedicalPenalty MPenalty
        {
            get { return _medicalPenalty; }
            set { _medicalPenalty = value; }
        }

        public List<MedicalPenalty> PenaltyList
        {
            get { return list; }
            set { list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return newUniqueID; }
            set { newUniqueID = value; }
        }

        public short ModifiedUser
        {
            get { return modifiedUser; }
            set { modifiedUser = value; }
        }


        /// <summary>
        /// This method returns true if the BCAD penalty is sucessfully added based on the BCAD ID
        /// </summary>
        /// <returns></returns>

        public Boolean AddMedicalPenalty()
        {          
                _medicalPenalty = new MedicalPenalty();
                if (list.Count == 0)
                {
                    _medicalPenalty.UniqueId = 1;
                    newUniqueID = 1;
                }
                else
                {
                    _medicalPenalty.UniqueId = ++newUniqueID;
                }
                list.Add(_medicalPenalty);
                return true;           
        }
        /// <summary>
        /// This method returns true if the BCAD penalty is sucessfully removed based on the BCAD ID
        /// </summary>
        /// <returns></returns>

        public Boolean RemoveMedicalPenalty(Int64 uniqueId)
        {
             _medicalPenalty = list.Find(delegate(MedicalPenalty p) { return p.UniqueId == uniqueId; });
                list.Remove(_medicalPenalty);
                return true;           
        }
        /// <summary>
        /// This method returns true if the BCAD penalty is sucessfully updated based on the BCAD ID
        /// </summary>
        /// <returns></returns>

        public Boolean UpdateMedicalPenalty(Int64 medicalPenaltyId)
        {            
                _medicalPenalty = list.Find(delegate(MedicalPenalty p) { return p.PenaltyId == medicalPenaltyId; });
                return true;           
        }
        /// <summary>
        /// This method loads the list of BCAD complaints based on the BCAD case ID.
        /// </summary>
        /// <param name="BCADCaseID"></param>
        public void Load(Int64 medicalCaseId)
        {           
                if (proxy == null)
                    proxy = new Proxy();
                List<MedicalPenaltyDTO> medicalPenaltyDTO = proxy.LoadMedicalPenalty(medicalCaseId);
                AssembleMedicalPenaltyList(medicalPenaltyDTO);
            
        }
        /// <summary>
        /// This method assembles the list of BCAD penalty list from the DTO
        /// </summary>
        /// <param name="dsPenalyList"></param>
        public void AssembleMedicalPenaltyList(List<MedicalPenaltyDTO> medicalPenaltyDto)
        {
            if (list == null)
                list = new List<MedicalPenalty>();          
            Int16 i = 1;  
               foreach (MedicalPenaltyDTO medicalPenaltyList in medicalPenaltyDto)
                {
                    MedicalPenalty medicalPenalty = new MedicalPenalty();
                    medicalPenalty.UniqueId = i;
                    medicalPenalty.PenaltyGroupId = medicalPenaltyList.PenaltyGroupID;
                    medicalPenalty.PenaltyTypeCode = medicalPenaltyList.PenaltyTypeCode;
                    medicalPenalty.Days = medicalPenaltyList.Days;
                    medicalPenalty.Hours = medicalPenaltyList.Hours;
                    medicalPenalty.Minutes = medicalPenaltyList.Minutes;
                    medicalPenalty.MoneyValue = medicalPenaltyList.MoneyValue;
                    medicalPenalty.Notes = medicalPenaltyList.Notes;
                    list.Add(medicalPenalty);
                    i++;
                }           
        }
        /// <summary>
        /// /// <summary>
        /// This method saves the BCAD penaly based on the BCAD case ID.
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <returns></returns>
        public bool Save(Int64 medicalCaseId)
        {              if (proxy == null)
                    proxy = new Proxy();
                foreach (MedicalPenalty medicalPenalty in list)
                {
                    medicalPenalty.PenaltyGroupId = medicalCaseId;
                    medicalPenalty.Save();
                }
                return true;
           
        }
     
    }
}
