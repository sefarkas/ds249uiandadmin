﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
 public   class MedicalComplaint
    {
        public MedicalComplaint()
        {
            medicalCaseID = -1;
            complaintID = -1;
            createUser = 0;
            createDate = DateTime.MinValue;
            modifiedUser = 0;
            modifiedDate = DateTime.MinValue;
            finalStatusCd = 0;
            nonPenaltyResultCd = 0;
            adjudicationBodyCd = 0;
            adjudicationDate = DateTime.MinValue;
            isAvailable = String.Empty;
            penalty = null;
            isProbation = false;
        }


        MedicalComplaintDTO medicalDTO;
        Proxy proxy;

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64 medicalCaseID;
        private Int64 complaintID;
        private Int16 createUser;
        private DateTime createDate;
        private Int16 modifiedUser;
        private DateTime modifiedDate;
        private Int16 finalStatusCd;
        private Int16 nonPenaltyResultCd;
        private Int16 adjudicationBodyCd;
        private DateTime adjudicationDate;
        private String isAvailable;
        private bool isProbation;
        private MedicalPenaltyList penalty;

        #endregion


        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int64 MedicalCaseId
        {
            get { return medicalCaseID; }
            set { medicalCaseID = value; }
        }

        public Int64 ComplaintId
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int16 CreateUser
        {
            get { return createUser; }
            set { createUser = value; }
        }

        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }

        public Int16 ModifiedUser
        {
            get { return modifiedUser; }
            set { modifiedUser = value; }
        }

        public DateTime ModifiedDate
        {
            get { return modifiedDate; }
            set { modifiedDate = value; }
        }

        public Int16 FinalStatusCD
        {
            get { return finalStatusCd; }
            set { finalStatusCd = value; }
        }

        public Int16 NonPenaltyResultCD
        {
            get { return nonPenaltyResultCd; }
            set { nonPenaltyResultCd = value; }
        }

        public Int16 AdjudicationBodyCD
        {
            get { return adjudicationBodyCd; }
            set { adjudicationBodyCd = value; }
        }

        public DateTime AjudicationDate
        {
            get { return adjudicationDate; }
            set { adjudicationDate = value; }
        }

        public String IsAvailable
        {
            get { return isAvailable; }
            set { isAvailable = value; }
        }

        public MedicalPenaltyList Penalty
        {
            get { return penalty; }
            set { penalty = value; }
        }
        public Boolean IsProbation
        {
            get { return isProbation; }
            set { isProbation = value; }
        }

        #endregion




        #region Public Methods
        /// <summary>
        /// This method creates new BCAD complaint.
        /// </summary>
        /// <returns></returns>
        public bool CreateNew()
        {
            if (proxy == null)
                proxy = new Proxy();
            CreateMedicalComplaint("new");        
            return proxy.CreateMedicalComplaint(medicalDTO);
        }

        /// <summary>
        /// This method creates new BCAD complaint based on the mode
        /// </summary>
        /// <param name="mode"></param>
        private void CreateMedicalComplaint(String mode)
        {
            medicalDTO = new MedicalComplaintDTO();
            medicalDTO.ComplaintID = this.complaintID;
            medicalDTO.CreateUser = this.CreateUser;
            if (mode == "save")
            {
                medicalDTO.MedicalCaseID = this.medicalCaseID;
                medicalDTO.CreateDate = this.createDate;
                medicalDTO.ModifiedUser = this.modifiedUser;
                medicalDTO.ModifiedDate = this.modifiedDate;
                medicalDTO.FinalStatusCd = this.finalStatusCd;
                medicalDTO.NonPenaltyResultCd = this.nonPenaltyResultCd;
                medicalDTO.AdjudicationBodyCd = this.adjudicationBodyCd;
                medicalDTO.AjudicationDate = this.adjudicationDate;
                medicalDTO.IsAvailable = this.isAvailable;
                CreatePenalty();
            }
        }
        /// <summary>
        /// This method creates new BCAD penalty.
        /// </summary>
        private void CreatePenalty()
        {
            MedicalPenaltyDTO penaltyDTO;
            medicalDTO.Penalty = new List<MedicalPenaltyDTO>();
            foreach (MedicalPenalty penalty in this.Penalty.PenaltyList)
            {
                penaltyDTO = new MedicalPenaltyDTO();
                penaltyDTO.Days = penalty.Days;
                penaltyDTO.Hours = penalty.Hours;
                penaltyDTO.Minutes = penalty.Minutes;
                penaltyDTO.ModifiedDate = penalty.ModifiedDate;
                penaltyDTO.ModifiedUser = this.modifiedUser;
                penaltyDTO.MoneyValue = penalty.MoneyValue;
                penaltyDTO.Notes = penalty.Notes;
                penaltyDTO.PenaltyTypeCode = penalty.PenaltyTypeCode;
                penaltyDTO.PenaltyGroupID = penalty.PenaltyGroupId;
                penaltyDTO.PenaltyID = penalty.PenaltyId;
                penaltyDTO.UniqueID = penalty.UniqueId;
                medicalDTO.Penalty.Add(penaltyDTO);
            }
        }

        /// <summary>
        /// This method loads the list of BCAD complaints based on the complaint ID.
        /// </summary>
        public void Load()
        {            
                if (proxy == null)
                    proxy = new Proxy();
                List<MedicalComplaintDTO> medicalComplaintDTO = proxy.GetMedicalComplaint(complaintID, medicalDTO);
                AssembleMedicalComplaint(medicalComplaintDTO);
                this.penalty = new MedicalPenaltyList();
                this.penalty.PenaltyList = GetPenaltyList();
           
        }
        /// <summary>
        /// This method sets the type of the status based on the complaint ID
        /// </summary>
        /// <returns></returns>
        public bool SetMedicalStatus()
        {          
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.SetMedicalStatus(this.complaintID, this.finalStatusCd);           
        }

        public bool ReturnToAuthor(Int64 PersonalID)
        {          
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.ReturnMedicalToAuthor(complaintID, PersonalID);           
        }
        /// <summary>
        /// This method returns the name of the authour based on the complaint ID.
        /// </summary>
        /// <returns></returns>
        public bool ReturnToAdvocate(Int64 PersonalID)
        {
            if (proxy == null)
                    proxy = new Proxy();
                return proxy.ReturnMedicalToAdvocate(complaintID, PersonalID);          
        }
        /// <summary>
        /// This method saves the new BCAD complaint created
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {          
                if (proxy == null)
                    proxy = new Proxy();
                CreateMedicalComplaint("save");
                if (proxy.SaveMedicalComplaint(medicalDTO))
                {
                    return true;
                }
                else
                {
                    return false;
                }           
        }

        /// <summary>
        /// This method gets the list of Medical penalties
        /// </summary>
        /// <returns></returns>
        private List<MedicalPenalty> GetPenaltyList()
        {
            MedicalPenaltyList penaltyList = new MedicalPenaltyList();
            penaltyList.Load(medicalCaseID);
            return penaltyList.PenaltyList;
        }

        /// <summary>
        /// This method assembles the list of appeals from the DTO.
        /// </summary>
        /// <param name="medicalComplaint"></param>
        private void AssembleMedicalComplaint(List<MedicalComplaintDTO> medicalComplaintDTO)
        {           
                foreach (MedicalComplaintDTO medicalComplaintList in medicalComplaintDTO)
                {
                    this.MedicalCaseId = medicalComplaintList.MedicalCaseID;
                    this.ComplaintId = medicalComplaintList.ComplaintID;
                    this.CreateUser = medicalComplaintList.CreateUser;
                    this.CreateDate = medicalComplaintList.CreateDate;
                    this.ModifiedUser = medicalComplaintList.ModifiedUser;
                    this.ModifiedDate = medicalComplaintList.ModifiedDate;
                    this.FinalStatusCD = medicalComplaintList.FinalStatusCd;
                    this.NonPenaltyResultCD = medicalComplaintList.NonPenaltyResultCd;
                    this.AdjudicationBodyCD = medicalComplaintList.AdjudicationBodyCd;
                    this.AjudicationDate = medicalComplaintList.AjudicationDate;
                    this.IsAvailable = medicalComplaintList.IsAvailable;
                    this.IsProbation = medicalComplaintList.IsProbation; 
                }            
        }

        #endregion
    }
}
