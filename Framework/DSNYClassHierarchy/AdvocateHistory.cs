﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    public class AdvocateHistory
    {
        Proxy proxy;

        public AdvocateHistory()
        {
            _indexNo = -1;
            _complaintID = -1;
            _voilationDate = DateTime.MinValue;
            _charges = String.Empty;
            _chargesDesc = String.Empty;
            _penalty = String.Empty;
            _parentindexNo = -1;
            _jt1 = String.Empty;
        }
        #region Private Variables

        /// <summary>
        /// Private Variables
        /// </summary>

        private Int64 _indexNo;
        private Int64 _complaintID;
        private DateTime _voilationDate;
        private String _charges;
        private String _chargesDesc;
        private String _penalty;
        private Int64 _parentindexNo;
        private String _jt1;

        #endregion
        /// <summary>
        /// Public property
        /// </summary>
        public Int64 ndexNo
        {
            get { return _indexNo; }
            set { _indexNo = value; }
        }
        public Int64 ParentIndexNo
        {
            get { return _parentindexNo; }
            set { _parentindexNo = value; }
        }
        public Int64 ComplaintID
        {
            get { return _complaintID; }
            set { _complaintID = value; }
        }
        public DateTime VoilationDate
        {
            get { return _voilationDate; }
            set { _voilationDate = value; }
        }
        public String Charges
        {
            get { return _charges; }
            set { _charges = value; }
        }
        public String ChargesDesc
        {
            get { return _chargesDesc; }
            set { _chargesDesc = value; }
        }
        public String Penalty
        {
            get { return _penalty; }
            set { _penalty = value; }
        }

        public String JT1
        {
            get { return _jt1; }
            set { _jt1 = value; }
        }
        public List<AdvocateHistory> Load(Int64 ComplaintId, Int64 EmployeeId)
        {
            List<AdvocateHistoryDto> advHistory = new List<AdvocateHistoryDto>();           
                List<AdvocateHistoryDto> AdvocateHistoryDto = new List<AdvocateHistoryDto>();
                if (proxy == null)
                    proxy = new Proxy();
                AdvocateHistoryDto = proxy.LoadAdvocateHistory(ComplaintId, EmployeeId);
                return AssembleAdvocateHistoryList(AdvocateHistoryDto);          
        }
        public List<AdvocateHistory> AssembleAdvocateHistoryList(List<AdvocateHistoryDto> AdvocateHistoryDto)
        {
            List<AdvocateHistory> returnedList = new List<AdvocateHistory>();
           
            AdvocateHistory complaint;
            foreach (AdvocateHistoryDto advHisDTO in AdvocateHistoryDto)
            {
                complaint = new AdvocateHistory();
                complaint.ComplaintID = advHisDTO.ComplaintID;
                complaint.ndexNo = advHisDTO.ndexNo;
                complaint.VoilationDate = advHisDTO.VoilationDate;
                complaint.Charges = advHisDTO.Charges;
                complaint.ChargesDesc = advHisDTO.ChargesDesc;
                complaint.Penalty = advHisDTO.Penalty;
                complaint.JT1 = advHisDTO.JT1;

                if (advHisDTO.ParentIndexNo != -1)
                    complaint.ParentIndexNo = advHisDTO.ParentIndexNo;               
                returnedList.Add(complaint);
                   
                }
            return returnedList;
           
        }

    }
}
