﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    ///  This class contains all private varaibles and public properties
    /// </summary>
    public class LocationDS
    {
        public LocationDS()
        {
            locationID = -1;
            locationName = string.Empty;
            locationParentID = -1;            
        }
        /// <summary>
        /// Private variables
        /// </summary>
        private Int64 locationID;
        private String locationName;
        private Int64 locationParentID;

        /// <summary>
        /// Public property
        /// </summary>
        public Int64 LocationId
        {
            get { return locationID; }
            set { locationID = value; }
        }

        public String LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public Int64 LocationParentId
        {
            get { return locationParentID; }
            set { locationParentID = value; }
        }
    }
}
