﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides with the functionality to Create New BCAD Complaint, Create BCAD Complaint, Set BCAD Status for BCAD Complaint, 
    /// Return To Author for BCAD Complaint, Return To Advocate for BCAD Complaint, Save BCAD Complaint, GetPenaltyList for BCAD Complaint and Assemble BCAD Complaint
    /// </summary>
    public class BCADComplaint
    {

        BCADComplaintDTO bcadDTO;
        Proxy proxy;

        public BCADComplaint()
        {
            bcadCaseID = -1;
            complaintID = -1;
            createUser = 0;
            createDate = DateTime.MinValue;
            modifiedUser = 0;
            modifiedDate = DateTime.MinValue;
            finalStatusCD = 0;
            nonPenaltyResultCD = 0;
            adjudicationBodyCD = 0;
            ajudicationDate = DateTime.MinValue;
            isAvailable = String.Empty;
            penalty = null;
            isProbation = false;
           
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64  bcadCaseID;
        private Int64 complaintID;
        private Int16 createUser;
        private DateTime createDate;
        private Int16 modifiedUser;
        private DateTime modifiedDate;
        private Int16 finalStatusCD;
        private Int16 nonPenaltyResultCD;
        private Int16 adjudicationBodyCD;
        private DateTime ajudicationDate;
        private String isAvailable;
        private BCADPenaltyList penalty;
        private bool isProbation;
        #endregion


        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int64 BcadCaseId
        {
            get { return bcadCaseID; }
            set { bcadCaseID = value; }
        }

        public Int64 ComplaintId
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int16 CreateUser
        {
            get { return createUser; }
            set { createUser = value; }
        }

        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }

        public Int16 ModifiedUser
        {
            get { return modifiedUser; }
            set { modifiedUser = value; }
        }

        public DateTime ModifiedDate
        {
            get { return modifiedDate; }
            set { modifiedDate = value; }
        }
        public Boolean IsProbation
        {
            get { return isProbation; }
            set { isProbation = value; }
        }
        public Int16 FinalStatusCD
        {
            get { return finalStatusCD; }
            set { finalStatusCD = value; }
        }


        public Int16 AdjudicationBodyCd
        {
            get { return adjudicationBodyCD; }
            set { adjudicationBodyCD = value; }
        }
        public Int16 NonPenaltyResultCD
        {
            get { return nonPenaltyResultCD; }
            set { nonPenaltyResultCD = value; }
        }

        public Int16 AdjudicationBodyCD
        {
            get { return adjudicationBodyCD; }
            set { adjudicationBodyCD = value; }
        }

        public DateTime AdjudicationDate
        {
            get { return ajudicationDate; }
            set { ajudicationDate = value; }
        }

        public String IsAvailable
        {
            get { return isAvailable; }
            set { isAvailable = value; }
        }

        public BCADPenaltyList Penalty
        {
            get { return penalty; }
            set { penalty = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// This method creates new BCAD complaint.
        /// </summary>
        /// <returns></returns>
        public bool CreateNew()
        {
            if (proxy == null)
                proxy = new Proxy();
            CreateBCadComplaint("new");
            return proxy.CreateBCADComplaint(bcadDTO);            
        }

        /// <summary>
        /// This method creates new BCAD complaint based on the mode
        /// </summary>
        /// <param name="mode"></param>
        private void CreateBCadComplaint(String mode)
        {
            bcadDTO = new BCADComplaintDTO();
            bcadDTO.ComplaintID = this.ComplaintId;
            bcadDTO.CreateUser = this.CreateUser;
            if (mode == "save")
            {
                bcadDTO.BCADCaseID = this.bcadCaseID;
                bcadDTO.CreateDate = this.createDate;
                bcadDTO.ModifiedUser = this.modifiedUser;
                bcadDTO.ModifiedDate = this.modifiedDate;
                bcadDTO.FinalStatusCd = this.finalStatusCD;
                bcadDTO.NonPenaltyResultCd = this.nonPenaltyResultCD;
                bcadDTO.AdjudicationBodyCd = this.adjudicationBodyCD;
                bcadDTO.AjudicationDate = this.ajudicationDate;
                bcadDTO.IsAvailable = this.isAvailable;
                CreatePenalty();
            }
        }
        /// <summary>
        /// This method creates new BCAD penalty.
        /// </summary>
        private void CreatePenalty()
        {
            BCADPenaltyDTO penaltyDTO;
            bcadDTO.Penalty = new List<BCADPenaltyDTO>();
            foreach (BcadPenalty penalty in this.Penalty.PenaltyList)
            {
                penaltyDTO = new BCADPenaltyDTO();
                penaltyDTO.Days = penalty.Days;
                penaltyDTO.Hours = penalty.Hours;
                penaltyDTO.Minutes = penalty.Minutes;
                penaltyDTO.ModifiedDate = penalty.ModifiedDate;
                penaltyDTO.ModifiedUser = this.modifiedUser;
                penaltyDTO.MoneyValue = penalty.MoneyValue;
                penaltyDTO.Notes = penalty.Notes;
                penaltyDTO.PenaltyTypeCode = penalty.PenaltyTypeCode;
                penaltyDTO.PenaltyGroupID = penalty.PenaltyGroupId;
                penaltyDTO.PenaltyID = penalty.PenaltyId;
                penaltyDTO.UniqueID = penalty.UniqueId;
                bcadDTO.Penalty.Add(penaltyDTO);
            }
        }

        /// <summary>
        /// This method loads the list of BCAD complaints based on the complaint ID.
        /// </summary>
        public void Load()
        {           
                if (proxy == null)
                    proxy = new Proxy();
                List<BCADComplaintDTO> bcadComplaintDTO = proxy.GetBCADComplaint(complaintID, bcadDTO);
                AssembleBCADComplaint(bcadComplaintDTO);
                this.penalty = new BCADPenaltyList();
                this.penalty.PenaltyList = GetPenaltyList();                        
        }
        /// <summary>
        /// This method sets the type of the status based on the complaint ID
        /// </summary>
        /// <returns></returns>
        public bool SetBcadStatus()
        {
            if (proxy == null)
                    proxy = new Proxy();
                return proxy.SetBCADStatus(this.ComplaintId, this.FinalStatusCD);                    
        }

        public bool ReturnToAuthor(Int64 PersonalID)
        {           
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.ReturnBCADToAuthor(complaintID, PersonalID);           
        }
        /// <summary>
        /// This method returns the name of the authour based on the complaint ID.
        /// </summary>
        /// <returns></returns>
        public bool ReturnToAdvocate(Int64 PersonalID)
        {            
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.ReturnBCADToAdvocate(complaintID, PersonalID);            
        }
        /// <summary>
        /// This method saves the new BCAD complaint created
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
              if (proxy == null)
                    proxy = new Proxy();
                CreateBCadComplaint("save");
                if (proxy.SaveBCADComplaint(bcadDTO))
                {
                    return true;
                }
                else {
                    return false;
                }             
           
        }

        /// <summary>
        /// This method gets the list of BCAD penalties
        /// </summary>
        /// <returns></returns>
        private List<BcadPenalty> GetPenaltyList()
        {
            BCADPenaltyList penaltyList = new BCADPenaltyList();
            penaltyList.Load(bcadCaseID);
            return penaltyList.PenaltyList;
        }

        /// <summary>
        /// This method assembles the list of appeals from the DTO.
        /// </summary>
        /// <param name="bcadComplaint"></param>
        private void AssembleBCADComplaint(List<BCADComplaintDTO> bcadComplaintDTO)
        {            
                foreach (BCADComplaintDTO bcadComplaintList in bcadComplaintDTO)
                    {
                        this.BcadCaseId = bcadComplaintList.BCADCaseID;
                        this.ComplaintId = bcadComplaintList.ComplaintID;
                        this.CreateUser = bcadComplaintList.CreateUser;
                        this.CreateDate = bcadComplaintList.CreateDate;
                        this.ModifiedUser = bcadComplaintList.ModifiedUser;
                        this.ModifiedDate = bcadComplaintList.ModifiedDate;
                        this.FinalStatusCD = bcadComplaintList.FinalStatusCd;
                        this.NonPenaltyResultCD = bcadComplaintList.NonPenaltyResultCd;
                        this.AdjudicationBodyCD = bcadComplaintList.AdjudicationBodyCd;
                        this.AdjudicationDate = bcadComplaintList.AjudicationDate;
                        this.IsAvailable = bcadComplaintList.IsAvailable;
                        this.IsProbation = bcadComplaintList.IsProbation;                         
                    }           
        }

        #endregion
    }
}
