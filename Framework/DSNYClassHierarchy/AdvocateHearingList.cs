﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class will provide functionality to load, add, remove advocate hearing and to Assemble advocate Hearings 
    /// </summary>
    public class AdvocateHearingList
    {
        Proxy proxy;
        AdvocateHearing hearing;

        public AdvocateHearingList()
        {
            _list = new List<AdvocateHearing>();
            _newUniqueID = 0;
        }

        private List<AdvocateHearing> _list;
        private Int16 _newUniqueID;

        /// <summary>
        /// Public property
        /// </summary>
        public List<AdvocateHearing> List
        {
            get { return _list; }
            set { _list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return _newUniqueID; }
            set { _newUniqueID = value; }
        }
        /// <summary>
        /// This method loads the advocate hearing bases on the advocate case ID
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        public void Load(Int64 advocacyCaseId)
        {
            List<AdvocateHearingDTO> advHearings;
               if (proxy == null)
                    proxy = new Proxy();
                advHearings = proxy.LoadAdvocateHearingList(advocacyCaseId);
                AssembleHearings(advHearings);            
        }
        /// <summary>
        /// This method assembles the list of hearings from the DTO.
        /// </summary>
        /// <param name="dsHearings"></param>
        public void AssembleHearings(List<AdvocateHearingDTO> advHearings)
        {
            if (_list == null)
                _list = new List<AdvocateHearing>();
           
                AdvocateHearing hearing;

                Int16 i = 1;

                foreach (AdvocateHearingDTO objAdvHearing in advHearings)
                    {
                        hearing = new AdvocateHearing();
                        hearing.UniqueId = i;
                       
                        if(!string.IsNullOrEmpty(objAdvHearing.AdvocateHearingID.ToString()))
                            hearing.AdvocateHearingId = objAdvHearing.AdvocateHearingID;

                        if (!string.IsNullOrEmpty(objAdvHearing.AdvocacyCaseID.ToString()))                        
                            hearing.AdvocacyCaseId = objAdvHearing.AdvocacyCaseID;

                        if (!string.IsNullOrEmpty(objAdvHearing.HearingDt.ToString()))                        
                            hearing.HearingDate = objAdvHearing.HearingDt;

                        if (!string.IsNullOrEmpty(objAdvHearing.HearingStatusCd.ToString()))                        
                            hearing.HearingStatusCD = objAdvHearing.HearingStatusCd;

                        if (!string.IsNullOrEmpty(objAdvHearing.CalStatusCd.ToString()))                        
                           hearing.CalStatusCD = objAdvHearing.CalStatusCd;

                        if (!string.IsNullOrEmpty(objAdvHearing.HearingTypeCd.ToString()))                        
                           hearing.HearingTypeCD = objAdvHearing.HearingTypeCd;

                        if (!String.IsNullOrEmpty(objAdvHearing.HearingComments.ToString()))                        
                            hearing.HearingComments = objAdvHearing.HearingComments;

                        if (!string.IsNullOrEmpty(objAdvHearing.ModifiedBy.ToString()))                        
                            hearing.ModifiedBy = objAdvHearing.ModifiedBy;

                        if (!string.IsNullOrEmpty(objAdvHearing.ModifiedDt.ToString()))                        
                            hearing.ModifiedDate = objAdvHearing.ModifiedDt;

                        _list.Add(hearing);
                        i++;
                    }               
        }
        /// <summary>
        /// This method returns true if the new hearing are sucessfully added.
        /// </summary>
        /// <returns></returns>
        public Boolean AddHearing()
        {
              hearing = new AdvocateHearing();
                if (_list.Count == 0)
                {
                    hearing.UniqueId = 1;
                    _newUniqueID = 1;
                }
                else
                {
                    hearing.UniqueId = ++_newUniqueID;                              
                }
                _list.Add(hearing);
                return true;           
        }
        /// <summary>
        /// This method returns true if the hearing is sucessfully removed based on the advocate ID
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public Boolean RemoveHearing(Int64 uniqueId)
        {           
                hearing = _list.Find(delegate(AdvocateHearing p) { return p.UniqueId == uniqueId; });
                _list.Remove(hearing);
                return true;           
        }
    }
}
