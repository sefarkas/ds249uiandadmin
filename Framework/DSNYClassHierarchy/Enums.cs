﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
   /// <summary>
    /// This class contains all enum collections values
   /// </summary>
        public enum StatusCode
        { 
            OPEN,
            CLOSE,
            UNREAD            
        }

        public enum RequestSource
        { 
            BCAD,
            Advocate,
            DS249,
            Medical,
            Admin
        }

        public enum PMDRequestSource
        {
            VACATION,
            DIF,
            MEDICAL,
            COMPLAINT
        }

        public enum GroupName
        { 
            DS249,
            BCAD,
            ADVOCATE ,
            ADMIN,
            MEDICAL,
            EMPLOYEE
        }
}
