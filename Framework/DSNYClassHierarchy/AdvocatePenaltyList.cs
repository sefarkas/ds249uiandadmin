﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides with the functionality to laod, add, remove advocate penalty list and Assemble Incident List
    /// </summary>
    public class AdvocatePenaltyList
    {
        Proxy proxy;
        AdvocatePenalty penalty;

        public AdvocatePenaltyList()
        {
            _list = new List<AdvocatePenalty>();
            _newUniqueID = 0;
        }
        
        List<AdvocatePenalty> _list;
        Int16 _newUniqueID;

        public List<AdvocatePenalty> List
        {
            get { return _list; }
            set { _list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return _newUniqueID; }
            set { _newUniqueID = value; }
        }
        /// <summary>
        /// This method loads the list of advoacte penalty based on the advocate case ID.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        public void Load(Int64 advocacyCaseId)
        {
            List<AdvocatePenaltyDTO> advPenalty;            
                if (proxy == null)
                    proxy = new Proxy();
                advPenalty = proxy.LoadAdvocatePenaltyList(advocacyCaseId);
                AssembleIncidentList(advPenalty);            
        }

        /// <summary>
        /// This method assembles the list of assemble incident list from the DTO.
        /// </summary>
        /// <param name="dsPenalty"></param>
        public void AssembleIncidentList(List<AdvocatePenaltyDTO> advocatePenaltyDto)
        {
            if (_list == null)
                _list = new List<AdvocatePenalty>();          
                AdvocatePenalty penalty;

                Int16 i = 1;

                foreach (AdvocatePenaltyDTO objAdvPenalty in advocatePenaltyDto)
                {
                    penalty = new AdvocatePenalty();
                    penalty.UniqueId = i;
                     
                    if(!string.IsNullOrEmpty(objAdvPenalty.AdvocatePenaltyID.ToString()))        
                        penalty.AdvocatePenaltyId = objAdvPenalty.AdvocatePenaltyID;

                    if (!string.IsNullOrEmpty(objAdvPenalty.AdvocacyCaseID.ToString()))        
                    penalty.AdvocacyCaseId = objAdvPenalty.AdvocacyCaseID;

                    if (!string.IsNullOrEmpty(objAdvPenalty.PenaltyTypeCd.ToString()))        
                    penalty.PenaltyTypeCD = objAdvPenalty.PenaltyTypeCd;

                    if (!string.IsNullOrEmpty(objAdvPenalty.DataValue.ToString()))        
                    penalty.DataValue = objAdvPenalty.DataValue;

                    if (!string.IsNullOrEmpty(objAdvPenalty.Term.ToString()))
                        penalty.Term  = objAdvPenalty.Term;

                    if (!string.IsNullOrEmpty(objAdvPenalty.Term_Desc.ToString()))
                        penalty.Term_Desc= objAdvPenalty.Term_Desc;

                    if (!string.IsNullOrEmpty(objAdvPenalty.Value_Data_Type.ToString()))
                        penalty.Value_Data_Type = objAdvPenalty.Value_Data_Type;
                  

                    if (!string.IsNullOrEmpty(objAdvPenalty.ApplyToTotal.ToString()))
                        penalty.ApplyToTotal = objAdvPenalty.ApplyToTotal;

                    if (!string.IsNullOrEmpty(objAdvPenalty.Comments.ToString()))        
                    penalty.PenaltyComments = objAdvPenalty.Comments;
                        
                    _list.Add(penalty);
                    i++;
                }              
           
        }


        /// <summary>
        /// This method returns true if the hearing is sucessfully removed added.
        /// </summary>
        /// <returns></returns>
        public Boolean AddPenalty()
        {            
                penalty = new AdvocatePenalty();
               if (_list.Count == 0)
                {
                    penalty.UniqueId = 1;
                    _newUniqueID = 1;
                }
                else
                {
                    penalty.UniqueId = ++ _newUniqueID; 
                }
                _list.Add(penalty);
                return true;           
        }
        /// <summary>
        /// This method returns true if the advovate penalty is sucessfully removed based on the advocate ID
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public Boolean RemovePenalty(Int64 UniqueID)
        {
            
                penalty = _list.Find(delegate(AdvocatePenalty p) { return p.UniqueId == UniqueID; });
                _list.Remove(penalty);
                return true;
           
        }

    }
}
