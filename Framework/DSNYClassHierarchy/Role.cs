﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provide method to returns the roles.
    /// </summary>
    public class Role
    {
        public Role()
        {
            id = -1;
            membershipId = -1;
            role = String.Empty;
            roleID = -1;
            personalId = -1;
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>
        private Int64 id;  // 16 -> 64 by Farkas 27Feb2020
        private Int64 membershipId;
        private Int64 roleID; // Int16 roleID; by Farkas 27Feb2020
        private String role;
        private Int64 personalId;

        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>
        public Int64 Id  // 16 -> 64 by Farkas 27Feb2020
        {
            get { return id; }
            set { id = value; }
        }

        public Int64 MembershipId
        {
            get { return membershipId; }
            set { membershipId = value; }
        }

        public Int64 PersonalId
        {
            get { return personalId; }
            set { personalId = value; }
        }

        public Int64 RoleId // Int16 RoleId by Farkas 27Feb2020
        {
            get { return roleID; }
            set { roleID = value; }
        }

        public String RoleDescription
        {
            get { return role; }
            set { role = value; }
        }

        #endregion

        #region Public Method
        /// <summary>
        /// This method returns true is the roles are laoded sucessfully.
        /// </summary>
        /// <returns></returns>
        public bool Load()
        {
            return true;
        }
        #endregion
    }
}

