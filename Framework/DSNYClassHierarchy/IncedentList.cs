﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This method provides functionality to list of Incidents based on the Incident ID and Charge ID,
    /// assembles the list of Assemble Incident from the DTO,Add Incedent details and remove Incedent details
    /// </summary>
    public class IncedentList
    {
        Proxy proxy;
        Incedent incedent;

        public IncedentList()
        {
            list = new List<Incedent>();
            newUniqueID = 0;
        }
        /// <summary>
        /// Private Variable
        /// </summary>
        List<Incedent> list;
        Int16 newUniqueID;

        /// <summary>
        /// public Property
        /// </summary>
        public List<Incedent> List
        {
            get { return list; }
            set { list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return newUniqueID; }
            set { newUniqueID = value; }
        }

        /// <summary>
        /// This method loads the list of Incidents based on the Incident ID and Charge ID.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="ChargeID"></param>

        public void Load(Int64 complaintId, Int64 chargeId)
        {
            List<IncedentDTO> IncedentDTO;           
                if (proxy == null)
                    proxy = new Proxy();
                IncedentDTO = proxy.LoadIncidents(complaintId, chargeId);
                AssembleIncidentList(IncedentDTO);           
        }

        /// <summary>
        /// This method assembles the list of Assemble Incident from the DTO.
        /// </summary>
        /// <param name="dsIncedents"></param>
        public void AssembleIncidentList(List<IncedentDTO> incidentDto)
        {
            if (list == null)
                list = new List<Incedent>();           
                Incedent incident;                
                Int16 i = 1;
                foreach (IncedentDTO incedentList in incidentDto)
                    {
                        incident = new Incedent();
                        incident.UniqueId = i;
                            incident.ComplaintId = incedentList.ComplaintID;
                            incident.IncidentDate = incedentList.IncedentDate;
                            incident.ChargeId = incedentList.ChargeID;
                        list.Add(incident);
                        i++;
                    }            
        }


        /// <summary>
        /// This method returns true if the incident is sucessfully added.
        /// </summary>
        /// <returns></returns>
        public Boolean AddIncident()
        {            
                incedent = new Incedent();
                if (list.Count == 0)
                {
                    incedent.UniqueId = 1;
                    newUniqueID = 1;
                }
                else
                {
                    incedent.UniqueId = ++newUniqueID;                                
                }
                list.Add(incedent);
                return true;           
        }
        /// <summary>
        /// This method returns true if the incident is sucessfully deleted based on the incident ID.
        /// </summary>
        /// <param name="UniqueID"></param>
        /// <returns></returns>
        public Boolean RemoveIncident(Int64 UniqueID)
        {            
                incedent = list.Find(delegate(Incedent p) { return p.UniqueId == UniqueID; });
                list.Remove(incedent);
                return true;           
        }

        
    }

    
 
}
