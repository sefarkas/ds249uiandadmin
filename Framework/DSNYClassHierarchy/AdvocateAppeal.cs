﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class will provide functiionality to save and createAdvocateAppeal
    /// </summary>
    public class AdvocateAppeal
    {
        Proxy proxy;
        AdvocateAppealDTO appealDTO;
        public AdvocateAppeal()
        {
            _advocateAppealID = -1;
            _advocacyCaseID = -1;
            _appealDt = DateTime.MinValue;
            _appealresultCd = -1;
            _appealComments = String.Empty;
            _modifiedBy = -1;
            _modifiedDt = DateTime.MinValue;
            appealCd = -1;
        }
        /// <summary>
        /// Private variables
        /// </summary>
        private Int64 _uniqueID; // 16 --> 64 by Farkas 28Feb2020
        private Int64 _advocateAppealID;
        private Int64 _advocacyCaseID;
        private DateTime _appealDt;
        private Int64 _appealresultCd;
        private String _appealComments;
        private Int64 _modifiedBy; // 16 --> 64 by Farkas 28Feb2020
        private DateTime _modifiedDt;
        private Int64 appealCd;

        /// <summary>
        /// Public properties
        /// </summary>
        public Int64 UniqueId
        { // 16 --> 64 by Farkas 28Feb2020
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }

        public Int64 AdvocateAppealId
        {
            get { return _advocateAppealID; }
            set { _advocateAppealID = value; }
        }
        public Int64 AdvocacyCaseId
        {
            get { return _advocacyCaseID; }
            set { _advocacyCaseID = value; }
        }
        public DateTime? AppealDate{get;set;}
      
        public Int64 AppealresultCD
        {
            get { return _appealresultCd; }
            set { _appealresultCd = value; }
        }
        public String AppealComments
        {
            get { return _appealComments; }
            set { _appealComments = value; }
        }
        public Int64 ModifiedBy
        { // 16 --> 64 by Farkas 28Feb2020
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _modifiedDt; }
            set { _modifiedDt = value; }
        }

        public Int64 AppealCD
        {
            get { return appealCd; }
            set { appealCd = value; }
        }

        #region Public Methods
        /// <summary>
        /// This boolean function returns true if the new advocate appeal is successfully created.
        /// </summary>
        /// <returns></returns>
        public bool Save()
        {
            if (proxy == null)
                proxy = new Proxy();
            CreateAdvocateAppeal();
            return true;
        }
        /// <summary>
        /// Creates advocate appeal
        /// </summary>
        private void CreateAdvocateAppeal()
        {
            appealDTO = new AdvocateAppealDTO();
            appealDTO.UniqueID = this._uniqueID;
            appealDTO.AdvocateAppealID = this._advocateAppealID;
            appealDTO.AdvocacyCaseID = this._advocacyCaseID;
            appealDTO.AppealDt = this.AppealDate;
            appealDTO.AppealresultCd = this._appealresultCd;
            appealDTO.AppealComments = this._appealComments;
            appealDTO.AppealCd = this.appealCd;
            appealDTO.ModifiedBy = this._modifiedBy;
            appealDTO.ModifiedDt = this._modifiedDt;
        }

        #endregion
    }
}
