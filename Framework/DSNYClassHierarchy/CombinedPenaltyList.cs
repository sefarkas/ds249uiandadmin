﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    public class CombinedPenaltyList
    {
        Proxy proxy;
        public CombinedPenaltyList()
        {
            _list = new List<CombinedPenalty>();
            _newUniqueID = 0;
        }

        List<CombinedPenalty> _list;
        Int16 _newUniqueID;

       

        public List<CombinedPenalty> List
        {
            get { return _list; }
            set { _list = value; }
        }

        public Int16 NewUniqueId
        {
            get { return _newUniqueID; }
            set { _newUniqueID = value; }
        }
        
        /// <summary>
        /// This method loads the list of advoacte penalty based on the advocate case ID.
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        public bool Load(Int64 advocacyCaseId)
        {
            List<CombinedPenaltyDTO> advCombPenalty;           
                if (proxy == null)
                    proxy = new Proxy();
                advCombPenalty = proxy.LoadCombinedPenaltyList(advocacyCaseId);
                AssembleCombPenalyList(advCombPenalty);
                return true;           
        }

        /// <summary>
        /// This method assembles the list of assemble incident list from the DTO.
        /// </summary>
        /// <param name="dsPenalty"></param>
        public void AssembleCombPenalyList(List<CombinedPenaltyDTO> combinedPenaltyDTO)
        {
            if (_list == null)
                _list = new List<CombinedPenalty>();          
                CombinedPenalty combPenalty;   
                foreach (CombinedPenaltyDTO objCombPenalty in combinedPenaltyDTO)
                {
                    combPenalty = new CombinedPenalty();
                    if (!string.IsNullOrEmpty(objCombPenalty.CombPenID.ToString()))
                        combPenalty.CombPenID = objCombPenalty.CombPenID;
                    if (!string.IsNullOrEmpty(objCombPenalty.ParentCombPenID.ToString()))
                        combPenalty.ParentCombPenID = objCombPenalty.ParentCombPenID;
                    if (!string.IsNullOrEmpty(objCombPenalty.ChildCombPenID.ToString()))
                        combPenalty.ChildCombPenID = objCombPenalty.ChildCombPenID;
                    _list.Add(combPenalty);
               }
            
        }
    }
}