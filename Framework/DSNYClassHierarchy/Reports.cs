﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.Proxys;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.ClassHierarchy
{
    /// <summary>
    /// This class provides method to returns the list of Complaints with respect to District in a DTO
    /// </summary>

    public class Reports
    {
        Proxy proxy;
        /// <summary>
        /// This method returns the list of Complaints with respect to District in a DTO
        /// </summary>
        /// <returns></returns>
        public List<ComplaintByDistrictDTO> GetComplaintsByDistrict()
        {           
                if (proxy == null)
                    proxy = new Proxy();
                return proxy.GetComplaintByDistrict();           
        }

    }
}
