﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.Mapper
{
    /// <summary>
    /// This class provides the functionality for the set of methods used for complaints.
    /// </summary>
    public class ComplaintDS249Mapper
    {
        /// <summary>
        /// This method returns the list of complaint of DS249 from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<ComplaintDTO> ComplaintListDS249(DataSet ds)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();          
               
                List<ComplaintDTO> cList = new List<ComplaintDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ComplaintDTO cDTO = new ComplaintDTO();
                    if (dr["ComplaintId"] != DBNull.Value)
                        cDTO.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                     if (dr["IndexNo"] != DBNull.Value)
                        cDTO.IndexNo = Convert.ToInt64(dr["IndexNo"]);
                    if (dr["LocationId"] != DBNull.Value)
                        cDTO.LocationID = Convert.ToInt64(dr["LocationId"]);
                    if (dr["FinalStatusCD"] != DBNull.Value)
                        cDTO.FinalStatusCD = Convert.ToInt64(dr["FinalStatusCD"]);
                    if (dr["BoroughID"] != DBNull.Value)
                        cDTO.BoroughID = Convert.ToInt64(dr["BoroughID"]);
                    if (dr["PromotionDate"] != DBNull.Value)
                        cDTO.PromotionDate = Convert.ToDateTime(dr["PromotionDate"]);
                    if (dr["LocationName"] != DBNull.Value)
                        cDTO.LocationName = Convert.ToString(dr["LocationName"]);
                    if (dr["RoutingLocation"] != DBNull.Value)
                        cDTO.RoutingLocation = Convert.ToString(dr["RoutingLocation"]);
                    if (dr["RespondentEmpID"] != DBNull.Value)
                        cDTO.EmployeeID = Convert.ToInt64(dr["RespondentEmpID"]);
                    if (dr["RespondentRefNumber"] != DBNull.Value)
                        cDTO.ReferenceNumber = Convert.ToString(dr["RespondentRefNumber"]);
                    if (dr["Approved"] != DBNull.Value)
                        cDTO.IsApproved = Convert.ToBoolean(dr["Approved"]);
                    if (dr["CreatedBy"] != DBNull.Value)
                        cDTO.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                    if (dr["CreatedDate"] != DBNull.Value)
                        cDTO.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    //if (dr["Status"] != DBNull.Value)
                    //    cDTO.Status = Convert.ToString(dr["Status"]);
                    if (dr["Charges"] != DBNull.Value)
                        cDTO.Charge = dr["Charges"].ToString();
                    if (dr["RespondentFName"] != DBNull.Value)
                        cDTO.RespondentFName = dr["RespondentFName"].ToString();
                    if (dr["RespondentLName"] != DBNull.Value)
                        cDTO.RespondentLName = dr["RespondentLName"].ToString();
                    if (dr["complaintStatus"] != DBNull.Value)
                        cDTO.ComplaintStatus = Convert.ToInt16(dr["complaintStatus"]);
                    if (dr["complaintStatusDate"] != DBNull.Value)
                        cDTO.ComplaintStatusDate = Convert.ToDateTime(dr["complaintStatusDate"]);
                    if (dr["Indicator"] != DBNull.Value)
                        cDTO.Indicator = Convert.ToString(dr["Indicator"]);
                    if (dr["IsDeleted"] != DBNull.Value)
                        cDTO.IsDeleted = Convert.ToBoolean(dr["IsDeleted"]);
                    if (dr["Jt1"] != DBNull.Value)
                        cDTO.Jt1 = Convert.ToString(dr["Jt1"]);

                    cList.Add(cDTO);
                }
                return cList;           

        }
        public List<ComplaintDTO> ComplaintAdminListDS249(DataSet ds)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();
              List<ComplaintDTO> cList = new List<ComplaintDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ComplaintDTO cDTO = new ComplaintDTO();
                    if (dr["ComplaintId"] != DBNull.Value)
                        cDTO.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                    if (dr["FinalStatusCD"] != DBNull.Value)
                        cDTO.FinalStatusCD = Convert.ToInt64(dr["FinalStatusCD"]);
                    if (dr["IndexNo"] != DBNull.Value)
                        cDTO.IndexNo = Convert.ToInt64(dr["IndexNo"]);
                    if (dr["LocationId"] != DBNull.Value)
                        cDTO.LocationID = Convert.ToInt64(dr["LocationId"]);
                    if (dr["BoroughID"] != DBNull.Value)
                        cDTO.BoroughID = Convert.ToInt64(dr["BoroughID"]);
                    if (dr["PromotionDate"] != DBNull.Value)
                        cDTO.PromotionDate = Convert.ToDateTime(dr["PromotionDate"]);
                    if (dr["LocationName"] != DBNull.Value)
                        cDTO.LocationName = Convert.ToString(dr["LocationName"]);
                    if (dr["RoutingLocation"] != DBNull.Value)
                        cDTO.RoutingLocation = Convert.ToString(dr["RoutingLocation"]);
                    if (dr["RespondentEmpID"] != DBNull.Value)
                        cDTO.EmployeeID = Convert.ToInt64(dr["RespondentEmpID"]);
                    if (dr["RespondentRefNumber"] != DBNull.Value)
                        cDTO.ReferenceNumber = Convert.ToString(dr["RespondentRefNumber"]);
                    if (dr["Approved"] != DBNull.Value)
                        cDTO.IsApproved = Convert.ToBoolean(dr["Approved"]);
                    if (dr["CreatedBy"] != DBNull.Value)
                        cDTO.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                    if (dr["CreatedDate"] != DBNull.Value)
                        cDTO.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    if (dr["Status"] != DBNull.Value)
                        cDTO.Status = Convert.ToString(dr["Status"]);
                    if (dr["Charges"] != DBNull.Value)
                        cDTO.Charge = dr["Charges"].ToString();
                    if (dr["RespondentFName"] != DBNull.Value)
                        cDTO.RespondentFName = dr["RespondentFName"].ToString();
                    if (dr["RespondentLName"] != DBNull.Value)
                        cDTO.RespondentLName = dr["RespondentLName"].ToString();
                    if (dr["complaintStatus"] != DBNull.Value)
                        cDTO.ComplaintStatus = Convert.ToInt16(dr["complaintStatus"]);
                    if (dr["complaintStatusDate"] != DBNull.Value)
                        cDTO.ComplaintStatusDate = Convert.ToDateTime(dr["complaintStatusDate"]);
                    if (dr["Indicator"] != DBNull.Value)
                        cDTO.Indicator = Convert.ToString(dr["Indicator"]);
                    if (dr["IsDeleted"] != DBNull.Value)
                        cDTO.IsDeleted = Convert.ToBoolean(dr["IsDeleted"]);
                    if (dr["Jt1"] != DBNull.Value)
                        cDTO.Jt1 = Convert.ToString(dr["Jt1"]);

                    cList.Add(cDTO);
                }
                return cList;          

        }
         public List<AppErrorDTO> ErrorLog(DataSet ds)
        {
            List<AppErrorDTO> complaintList = new List<AppErrorDTO>();           
               
                List<AppErrorDTO> cErrorList = new List<AppErrorDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AppErrorDTO errorDTO = new AppErrorDTO();
                    if (dr["Url"] != DBNull.Value)
                        errorDTO.Url = Convert.ToString(dr["Url"]);
                    if (dr["RequestType"] != DBNull.Value)
                        errorDTO.RequestType = Convert.ToString(dr["RequestType"]);   
                    if (dr["UserID"] != DBNull.Value)
                        errorDTO.UserID = Convert.ToString(dr["UserID"]);   
                    if (dr["ErrorMessage"] != DBNull.Value)
                        errorDTO.ErrorMessage = Convert.ToString(dr["ErrorMessage"]);   
                    if (dr["StackTrace"] != DBNull.Value)
                        errorDTO.StackTrace = Convert.ToString(dr["StackTrace"]);   
                    if (dr["AppID"] != DBNull.Value)
                        errorDTO.ErrorID = Convert.ToInt16(dr["AppID"]);
                    cErrorList.Add(errorDTO);
                }
                return cErrorList;          

        }

        public List<ComplaintDTO> ComplaintList(DataSet ds)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();            

                List<ComplaintDTO> cList = new List<ComplaintDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ComplaintDTO cDTO = new ComplaintDTO();
                    if (dr["IndexNo"] != DBNull.Value)
                        cDTO.IndexNo = Convert.ToInt64(dr["IndexNo"]);
                    cList.Add(cDTO);
                }
                return cList;          
        }

        /// <summary>
        /// This method returns the list of location DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>

        public List<LocationDTO> GetLocationsMapper(DataSet ds)
        {
            List<LocationDTO> locationListDTO = new List<LocationDTO>();       
             
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    LocationDTO locationList = new LocationDTO();
                    if (dr["LocationId"] != DBNull.Value)
                        locationList.LocationID = Convert.ToInt64(dr["LocationId"]);
                    if (dr["LocationName"] != DBNull.Value)
                        locationList.LocationName = Convert.ToString(dr["LocationName"]);
                    if (dr["ParentLocationID"] != DBNull.Value)
                        locationList.LocationParentID = Convert.ToInt64(dr["ParentLocationID"]);
                        
                    locationListDTO.Add(locationList);
                }
                return locationListDTO;            }

        /// <summary>
        /// This method returns the list of Borough DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<BoroughDTO> GetBoroughMapper(DataSet ds)
        {
            List<BoroughDTO> boroughListDTO = new List<BoroughDTO>();
               foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    BoroughDTO boroughList = new BoroughDTO();
                    if (dr["LocationId"] != DBNull.Value)
                        boroughList.LocationID = Convert.ToInt64(dr["LocationId"]);
                    if (dr["LocationName"] != DBNull.Value)
                        boroughList.LocationName = Convert.ToString(dr["LocationName"]);
                    if (dr["ParentLocationID"] != DBNull.Value)
                        boroughList.LocationParentID = Convert.ToInt64(dr["ParentLocationID"]);
                             boroughListDTO.Add(boroughList);
                }
                return boroughListDTO;           

        }

        /// <summary>
        /// This method returns the list of Personnel info DTO from the dataset.
        /// </summary>
        /// <param name="dsPersonnelList"></param>
        /// <returns></returns>
        public List<PersonnelListDTO> PersonnelListMapper(DataSet dsPersonnelList)
        {
            List<PersonnelListDTO> pDTO = new List<PersonnelListDTO>();
                if (dsPersonnelList.Tables.Count > 0)
                {
                    foreach (DataRow drPL in dsPersonnelList.Tables[0].Rows)
                    {
                        PersonnelListDTO pList = new PersonnelListDTO();
                        if (drPL["FirstName"] != DBNull.Value)
                            pList.FirstName = Convert.ToString(drPL["FirstName"]);
                        if (drPL["MiddleName"] != DBNull.Value)
                            pList.MiddleName = Convert.ToString(drPL["MiddleName"]);
                        if (drPL["LastName"] != DBNull.Value)
                            pList.LastName = Convert.ToString(drPL["LastName"]);
                        if (drPL["EmployeeID"] != DBNull.Value)
                            pList.PersonnelID = Convert.ToInt64(drPL["EmployeeID"]);
                        if (drPL["ReferenceNo"] != DBNull.Value)
                            pList.ReferenceNumber = Convert.ToString(drPL["ReferenceNo"]);
                        pDTO.Add(pList);
                    }
                }
            return pDTO;         
        }
        
        /// <summary>
        /// This method returns the list of Personnel info DTO from the dataset.
        /// </summary>
        /// <param name="dsPersonnelList"></param>
        /// <returns></returns>

        public List<PersonnelDTO> PersonnelMapper(DataSet dsPersonnelList)
        {
            List<PersonnelDTO> pDTO = new List<PersonnelDTO>();

                if (dsPersonnelList.Tables.Count > 0)
                {
                    foreach (DataRow drPL in dsPersonnelList.Tables[0].Rows)
                    {
                        PersonnelDTO pList = new PersonnelDTO();

                        if (drPL["FirstName"] != DBNull.Value)
                            pList.FirstName = Convert.ToString(drPL["FirstName"]);
                        if (drPL["MiddleName"] != DBNull.Value)
                            pList.MiddleName = Convert.ToString(drPL["MiddleName"]);
                        if (drPL["LastName"] != DBNull.Value)
                            pList.LastName = Convert.ToString(drPL["LastName"]);
                        if (drPL["EmployeeID"] != DBNull.Value)
                            pList.PersonnelID = Convert.ToInt64(drPL["EmployeeID"]);
                        if (drPL["ReferenceNo"] != DBNull.Value)
                            pList.ReferenceNumber = Convert.ToString(drPL["ReferenceNo"]);

                        pDTO.Add(pList);
                    }
                }
                return pDTO;           
        }
        /// <summary>
        /// This method returns the list of Personnel info w.r.t refference DTO from the dataset.
        /// </summary>
        /// <param name="dsPersonnelList"></param>
        /// <returns></returns>
        public List<PersonnelByRefNoDTO> PersonnelByRefNoMapper(DataSet dspersonnel)
        {            
                List<PersonnelByRefNoDTO> personnelByrefNoDTO = new List<PersonnelByRefNoDTO>();
                
                if (dspersonnel.Tables.Count > 0)
                {
                    foreach (DataRow drP in dspersonnel.Tables[0].Rows)
                    {
                        PersonnelByRefNoDTO personnelByRefNoList = new PersonnelByRefNoDTO();
                        if (drP["ImageID"] != DBNull.Value)
                            personnelByRefNoList.ImageID = Convert.ToInt32(drP["ImageID"]);
                        if (drP["FirstName"] != DBNull.Value)
                            personnelByRefNoList.FirstName = Convert.ToString(drP["FirstName"]);
                        if (drP["MiddleName"] != DBNull.Value)
                            personnelByRefNoList.MiddleName = Convert.ToString(drP["MiddleName"]);
                        if (drP["LastName"] != DBNull.Value)
                            personnelByRefNoList.LastName = Convert.ToString(drP["LastName"]);
                        if (drP["SSN"] != DBNull.Value)
                            personnelByRefNoList.SSN = Convert.ToString(drP["SSN"]); ;
                        if (drP["Gender"] != DBNull.Value)
                            personnelByRefNoList.Gender = Convert.ToString(drP["Gender"]);
                        if (drP["dob"] != DBNull.Value)
                            personnelByRefNoList.DOB = Convert.ToDateTime(drP["dob"]);
                        else
                            personnelByRefNoList.DOB = DateTime.MinValue;
                        if (drP["EmployeeID"] != DBNull.Value)
                            personnelByRefNoList.PersonnelID = Convert.ToInt64(drP["EmployeeID"]);
                        if (drP["ReferenceNo"] != DBNull.Value)
                            personnelByRefNoList.ReferenceNo = Convert.ToString(drP["ReferenceNo"]);
                        if (drP["AppointmentDate"] != DBNull.Value)
                            personnelByRefNoList.AppointmentDate = Convert.ToDateTime(drP["AppointmentDate"]);
                        else
                            personnelByRefNoList.AppointmentDate = DateTime.MinValue;
                        if (drP["PayCodeID"] != DBNull.Value)
                            personnelByRefNoList.PayCodeID = Convert.ToInt16(drP["PayCodeID"]);
                        if (drP["PayDesc"] != DBNull.Value)
                            personnelByRefNoList.PayCodeDescription = Convert.ToString(drP["PayDesc"]);
                        if (drP["IsBCAD"] != DBNull.Value)
                            personnelByRefNoList.IsBCAD = Convert.ToString(drP["IsBCAD"]);
                        if (drP["ChartNumber"] != DBNull.Value)
                            personnelByRefNoList.ChartNumber = Convert.ToString(drP["ChartNumber"]);
                        if (drP["DSNYHireDate"] != DBNull.Value)
                            personnelByRefNoList.DSNYHireDate = Convert.ToDateTime(drP["DSNYHireDate"]);
                        else
                            personnelByRefNoList.DSNYHireDate = DateTime.MinValue;
                        if (drP["NYCHireDate"] != DBNull.Value)
                            personnelByRefNoList.NYCHireDate = Convert.ToDateTime(drP["NYCHireDate"]);
                        else
                            personnelByRefNoList.NYCHireDate = DateTime.MinValue;
                        if (drP["PhoneNumber"] != DBNull.Value)
                            personnelByRefNoList.PhoneNumber = Convert.ToString(drP["PhoneNumber"]);
                        if (drP["LeaveStatusCode"] != DBNull.Value)
                            personnelByRefNoList.LeaveStatusCode = Convert.ToInt16(drP["LeaveStatusCode"]);
                        if (drP["ProbationCode"] != DBNull.Value)
                            personnelByRefNoList.ProbationCode = Convert.ToInt16(drP["ProbationCode"]);
                        if (drP["ProbationStartDt"] != DBNull.Value)
                            personnelByRefNoList.ProbationStartDate = Convert.ToDateTime(drP["ProbationStartDt"]);
                        else
                            personnelByRefNoList.ProbationStartDate = DateTime.MinValue;
                        if (drP["ProbationEndDt"] != DBNull.Value)
                            personnelByRefNoList.ProbationEndDate = Convert.ToDateTime(drP["ProbationEndDt"]);
                        else
                            personnelByRefNoList.ProbationEndDate = DateTime.MinValue;
                        if (drP["VacationSchedule"] != DBNull.Value)
                            personnelByRefNoList.VacationSchedule = Convert.ToString(drP["VacationSchedule"]);
                        if (drP["IsActive"] != DBNull.Value)
                            personnelByRefNoList.IsActive = Convert.ToBoolean(drP["IsActive"]);
                            personnelByRefNoList.Address = new AddressDTO();
                        if (drP["AddressId"] != DBNull.Value)
                            personnelByRefNoList.Address.AddressID = Convert.ToInt16(drP["AddressId"]);
                        if (drP["AddressTypeId"] != DBNull.Value)
                            personnelByRefNoList.Address.AddressTypeID = Convert.ToInt16(drP["AddressTypeId"]);
                        if (drP["StreetNumber"] != DBNull.Value)
                             personnelByRefNoList.Address.StreetNumber = Convert.ToString(drP["StreetNumber"]);
                        if (drP["StreetName"] != DBNull.Value)
                            personnelByRefNoList.Address.StreetName = Convert.ToString(drP["StreetName"]);
                        if (drP["AptNo"] != DBNull.Value)
                            personnelByRefNoList.Address.AptNo = Convert.ToString(drP["AptNo"]);
                        if (drP["City"] != DBNull.Value)
                            personnelByRefNoList.Address.City = Convert.ToString(drP["City"]);
                        if (drP["State"] != DBNull.Value)
                            personnelByRefNoList.Address.State = Convert.ToString(drP["State"]);
                        if (drP["Zip"] != DBNull.Value)
                            personnelByRefNoList.Address.ZipCode = Convert.ToString(drP["Zip"]);
                        if (drP["ZipSuffix"] != DBNull.Value)
                            personnelByRefNoList.Address.ZipSuffix = Convert.ToString(drP["ZipSuffix"]);
                       
                        personnelByRefNoList.PersonnelTitle = Convert.ToString(drP["Title"]);
                        if (drP["BadgeNo"] != DBNull.Value)
                            personnelByRefNoList.BadgeNumber = Convert.ToString(drP["BadgeNo"]);
                        if (drP["TitleID"] != DBNull.Value)
                            personnelByRefNoList.TitleID = Convert.ToInt64(drP["TitleID"]);
                        if (drP["PromotionDate"] != DBNull.Value)
                            personnelByRefNoList.EffectiveDate = Convert.ToDateTime(drP["PromotionDate"]);
                        else
                            personnelByRefNoList.EffectiveDate = DateTime.MinValue;
                        personnelByrefNoDTO.Add(personnelByRefNoList);
                    }
                }
                return personnelByrefNoDTO;           
        }
        /// <summary>
        /// This method returns the list of charges DTO from the dataset.
        /// </summary>
        /// <param name="dsPersonnelList"></param>
        /// <returns></returns>
        public List<ChargeDTO> ChargeListMapper(DataSet dsCharges)
        {           
                List<ChargeDTO> chargeDTO = new List<ChargeDTO>();
                if (dsCharges.Tables.Count > 0)
                {
                    foreach (DataRow drchg in dsCharges.Tables[0].Rows)
                    {
                        ChargeDTO chargeList = new ChargeDTO();
                        if (drchg["ChargeTypeId"] != DBNull.Value)
                            chargeList.ChargeTypeID = Convert.ToInt16(drchg["ChargeTypeId"]);
                        if (drchg["ReferenceNumber"] != DBNull.Value)
                            chargeList.ReferenceNumber = Convert.ToString(drchg["ReferenceNumber"]);
                        if (drchg["ChargeDesc"] != DBNull.Value)
                            chargeList.ChargeDescription = Convert.ToString(drchg["ChargeDesc"]);
                        chargeDTO.Add(chargeList);
                    }
                }
                return chargeDTO;                       
        }
        /// <summary>
        /// This method returns the list of charges without permissions DTO from the dataset.
        /// </summary>
        /// <param name="dsCharges"></param>
        /// <returns></returns>
        public List<ChargesWithoutParametersDTO> ChargeListWithoutParameterMapper(DataSet dsCharges)
        {
            List<ChargesWithoutParametersDTO> chargeDTO = new List<ChargesWithoutParametersDTO>();
                if (dsCharges.Tables.Count > 0)
                {
                    foreach (DataRow drchg in dsCharges.Tables[0].Rows)
                    {
                        ChargesWithoutParametersDTO chargeList = new ChargesWithoutParametersDTO();
                        if (drchg["ChargeTypeId"] != DBNull.Value)
                            chargeList.ChargeTypeID = Convert.ToInt16(drchg["ChargeTypeId"]);
                        if (drchg["ReferenceNumber"] != DBNull.Value)
                            chargeList.ReferenceNumber = Convert.ToString(drchg["ReferenceNumber"]);
                        if (drchg["ChargeDesc"] != DBNull.Value)
                            chargeList.ChargeDescription = Convert.ToString(drchg["ChargeDesc"]);
                        chargeDTO.Add(chargeList);
                    }
                }
                return chargeDTO;            
        }
        /// <summary>
        /// This method returns the list of titles DTO from the dataset.
        /// </summary>
        /// <param name="dsTitle"></param>
        /// <returns></returns>
        public List<TitleListDTO> TitleListMapper(DataSet dsTitle)
        {       
                List<TitleListDTO> tDTO = new List<TitleListDTO>();                
                if (dsTitle.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsTitle.Tables[0].Rows)
                    {
                        TitleListDTO tList = new TitleListDTO();
                        if (dr["TitleID"] != DBNull.Value)
                            tList.TitleID = Convert.ToInt64(dr["TitleID"]);
                        if (dr["TitleCode"] != DBNull.Value)
                            tList.TitleCode = Convert.ToString(dr["TitleCode"]);
                        if (dr["Title"] != DBNull.Value)
                            tList.TitleDescription = Convert.ToString(dr["Title"]);
                        tDTO.Add(tList);
                    }
                }
                return tDTO;
            
        }

        /// <summary>
        /// This method returns the list of titles DTO from the dataset.
        /// </summary>
        /// <param name="dsTitle"></param>
        /// <returns></returns>
        public List<TitleDTO> TitleMapper(DataSet dsTitle)
        {           
                List<TitleDTO> tDTO = new List<TitleDTO>();
              
                if (dsTitle.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsTitle.Tables[0].Rows)
                    {
                        TitleDTO tList = new TitleDTO();
                        if (dr["TitleID"] != DBNull.Value)
                            tList.TitleID = Convert.ToInt64(dr["TitleID"]);
                        if (dr["TitleCode"] != DBNull.Value)
                            tList.TitleCode = Convert.ToString(dr["TitleCode"]);
                        if (dr["Title"] != DBNull.Value)
                            tList.TitleDescription = Convert.ToString(dr["Title"]);
                        tDTO.Add(tList);
                    }
                }
                return tDTO;           
        }
        /// <summary>
        /// This method returns the list of personnels w.r.t ID DTO from the dataset.
        /// </summary>
        /// <param name="dspersonnel"></param>
        /// <returns></returns>
        public List<PersonnelByIdDTO> PersonnelByIDMapper(DataSet dspersonnel)
        {
             List<PersonnelByIdDTO> personnelByIDDTO = new List<PersonnelByIdDTO>();                
                if (dspersonnel.Tables.Count > 0)
                {
                    foreach (DataRow drP in dspersonnel.Tables[0].Rows)
                    {
                        PersonnelByIdDTO personnelByIDList = new PersonnelByIdDTO();
                        if (drP["ImageID"] != DBNull.Value)
                            personnelByIDList.ImageID = Convert.ToInt32(drP["ImageID"]);
                        if (drP["FirstName"] != DBNull.Value)
                            personnelByIDList.FirstName = Convert.ToString(drP["FirstName"]);
                        if (drP["MiddleName"] != DBNull.Value)
                            personnelByIDList.MiddleName = Convert.ToString(drP["MiddleName"]);
                        if (drP["LastName"] != DBNull.Value)
                            personnelByIDList.LastName = Convert.ToString(drP["LastName"]);
                        if (drP["SSN"] != DBNull.Value)
                            personnelByIDList.SSN = Convert.ToString(drP["SSN"]); ;
                        if (drP["Gender"] != DBNull.Value)
                            personnelByIDList.Gender = Convert.ToString(drP["Gender"]);
                        if (drP["dob"] != DBNull.Value)
                            personnelByIDList.DOB = Convert.ToDateTime(drP["dob"]);
                        else
                            personnelByIDList.DOB = DateTime.MinValue;
                        if (drP["EmployeeID"] != DBNull.Value)
                            personnelByIDList.PersonnelID = Convert.ToInt64(drP["EmployeeID"]);
                        if (drP["ReferenceNo"] != DBNull.Value)
                            personnelByIDList.ReferenceNo = Convert.ToString(drP["ReferenceNo"]);
                        if (drP["AppointmentDate"] != DBNull.Value)
                            personnelByIDList.AppointmentDate = Convert.ToDateTime(drP["AppointmentDate"]);
                        else
                            personnelByIDList.AppointmentDate = DateTime.MinValue;
                        if (drP["PayCodeID"] != DBNull.Value)
                            personnelByIDList.PayCodeID = Convert.ToInt16(drP["PayCodeID"]);
                        if (drP["PayDesc"] != DBNull.Value)
                            personnelByIDList.PayCodeDescription = Convert.ToString(drP["PayDesc"]);
                        if (drP["IsBCAD"] != DBNull.Value)
                            personnelByIDList.IsBCAD = Convert.ToString(drP["IsBCAD"]);
                        if (drP["ChartNumber"] != DBNull.Value)
                            personnelByIDList.ChartNumber = Convert.ToString(drP["ChartNumber"]);
                        if (drP["DSNYHireDate"] != DBNull.Value)
                            personnelByIDList.DSNYHireDate = Convert.ToDateTime(drP["DSNYHireDate"]);
                        else
                            personnelByIDList.DSNYHireDate = DateTime.MinValue;
                        if (drP["NYCHireDate"] != DBNull.Value)
                            personnelByIDList.NYCHireDate = Convert.ToDateTime(drP["NYCHireDate"]);
                        else
                            personnelByIDList.NYCHireDate = DateTime.MinValue;
                        if (drP["PhoneNumber"] != DBNull.Value)
                            personnelByIDList.PhoneNumber = Convert.ToString(drP["PhoneNumber"]);
                        if (drP["LeaveStatusCode"] != DBNull.Value)
                            personnelByIDList.LeaveStatusCode = Convert.ToInt16(drP["LeaveStatusCode"]);
                        if (drP["ProbationCode"] != DBNull.Value)
                            personnelByIDList.ProbationCode = Convert.ToInt16(drP["ProbationCode"]);
                        if (drP["ProbationStartDt"] != DBNull.Value)
                            personnelByIDList.ProbationStartDate = Convert.ToDateTime(drP["ProbationStartDt"]);
                        else
                            personnelByIDList.ProbationStartDate = DateTime.MinValue;
                        if (drP["ProbationEndDt"] != DBNull.Value)
                            personnelByIDList.ProbationEndDate = Convert.ToDateTime(drP["ProbationEndDt"]);
                        else
                            personnelByIDList.ProbationEndDate = DateTime.MinValue;
                        //if (drP["VacationSchedule"] != DBNull.Value)
                        //    personnelByIDList.VacationSchedule = Convert.ToString(drP["VacationSchedule"]);
                        if (drP["IsActive"] != DBNull.Value)
                            personnelByIDList.IsActive = Convert.ToBoolean(drP["IsActive"]);
                            personnelByIDList.Address = new AddressDTO();
                        if (drP["AddressId"] != DBNull.Value)
                            personnelByIDList.Address.AddressID = Convert.ToInt32(drP["AddressId"]);
                        if (drP["AddressTypeId"] != DBNull.Value)
                            personnelByIDList.Address.AddressTypeID = Convert.ToInt16(drP["AddressTypeId"]);
                        if (drP["StreetNumber"] != DBNull.Value)
                            personnelByIDList.Address.StreetNumber = Convert.ToString(drP["StreetNumber"]);
                        if (drP["StreetName"] != DBNull.Value)
                            personnelByIDList.Address.StreetName = Convert.ToString(drP["StreetName"]);
                        if (drP["AptNo"] != DBNull.Value)
                            personnelByIDList.Address.AptNo = Convert.ToString(drP["AptNo"]);
                        if (drP["City"] != DBNull.Value)
                            personnelByIDList.Address.City = Convert.ToString(drP["City"]);
                        if (drP["State"] != DBNull.Value)
                            personnelByIDList.Address.State = Convert.ToString(drP["State"]);
                        if (drP["Zip"] != DBNull.Value)
                            personnelByIDList.Address.ZipCode = Convert.ToString(drP["Zip"]);
                        if (drP["ZipSuffix"] != DBNull.Value)
                            personnelByIDList.Address.ZipSuffix = Convert.ToString(drP["ZipSuffix"]);

                        personnelByIDList.PersonnelTitle = Convert.ToString(drP["Title"]);
                        //if (drP["BadgeNo"] != DBNull.Value)
                        //    personnelByIDList.BadgeNumber = Convert.ToString(drP["BadgeNo"]);
                        if (drP["TitleID"] != DBNull.Value)
                            personnelByIDList.TitleID = Convert.ToInt64(drP["TitleID"]);
                        personnelByIDDTO.Add(personnelByIDList);
                    }
                }
                return personnelByIDDTO;           
        }
        /// <summary>
        /// This method returns the list of personnel by Image DTO from the dataset.
        /// </summary>
        /// <param name="dspersonnel"></param>
        /// <returns></returns>
        public List<PersonnelByImageDTO> PersonnelByImageMapper(DataSet dspersonnel)
        {
           
                List<PersonnelByImageDTO> personnelByImageDTO = new List<PersonnelByImageDTO>();
                
                if (dspersonnel.Tables.Count > 0)
                {
                    foreach (DataRow drP in dspersonnel.Tables[0].Rows)
                    {
                        PersonnelByImageDTO personnelByImageList = new PersonnelByImageDTO();
                        if (drP["ImageID"] != DBNull.Value)
                            personnelByImageList.ImageID = Convert.ToInt32(drP["ImageID"]);
                        if (drP["FirstName"] != DBNull.Value)
                            personnelByImageList.FirstName = Convert.ToString(drP["FirstName"]);
                        if (drP["MiddleName"] != DBNull.Value)
                            personnelByImageList.MiddleName = Convert.ToString(drP["MiddleName"]);
                        if (drP["LastName"] != DBNull.Value)
                            personnelByImageList.LastName = Convert.ToString(drP["LastName"]);
                        if (drP["SSN"] != DBNull.Value)
                            personnelByImageList.SSN = Convert.ToString(drP["SSN"]); ;
                        if (drP["Gender"] != DBNull.Value)
                            personnelByImageList.Gender = Convert.ToString(drP["Gender"]);
                        if (drP["dob"] != DBNull.Value)
                            personnelByImageList.DOB = Convert.ToDateTime(drP["dob"]);
                        else
                            personnelByImageList.DOB = DateTime.MinValue;
                        if (drP["EmployeeID"] != DBNull.Value)
                            personnelByImageList.PersonnelID = Convert.ToInt64(drP["EmployeeID"]);
                        if (drP["ReferenceNo"] != DBNull.Value)
                            personnelByImageList.ReferenceNo = Convert.ToString(drP["ReferenceNo"]);
                        if (drP["AppointmentDate"] != DBNull.Value)
                            personnelByImageList.AppointmentDate = Convert.ToDateTime(drP["AppointmentDate"]);
                        else
                            personnelByImageList.AppointmentDate = DateTime.MinValue;
                        if (drP["PayCodeID"] != DBNull.Value)
                            personnelByImageList.PayCodeID = Convert.ToInt16(drP["PayCodeID"]);
                        if (drP["PayDesc"] != DBNull.Value)
                            personnelByImageList.PayCodeDescription = Convert.ToString(drP["PayDesc"]);
                        if (drP["IsBCAD"] != DBNull.Value)
                            personnelByImageList.IsBCAD = Convert.ToString(drP["IsBCAD"]);
                        if (drP["ChartNumber"] != DBNull.Value)
                            personnelByImageList.ChartNumber = Convert.ToString(drP["ChartNumber"]);
                        if (drP["DSNYHireDate"] != DBNull.Value)
                            personnelByImageList.DSNYHireDate = Convert.ToDateTime(drP["DSNYHireDate"]);
                        else
                            personnelByImageList.DSNYHireDate = DateTime.MinValue;
                        if (drP["NYCHireDate"] != DBNull.Value)
                            personnelByImageList.NYCHireDate = Convert.ToDateTime(drP["NYCHireDate"]);
                        else
                            personnelByImageList.NYCHireDate = DateTime.MinValue;
                        if (drP["PhoneNumber"] != DBNull.Value)
                            personnelByImageList.PhoneNumber = Convert.ToString(drP["PhoneNumber"]);
                        if (drP["LeaveStatusCode"] != DBNull.Value)
                            personnelByImageList.LeaveStatusCode = Convert.ToInt16(drP["LeaveStatusCode"]);
                        if (drP["ProbationCode"] != DBNull.Value)
                            personnelByImageList.ProbationCode = Convert.ToInt16(drP["ProbationCode"]);
                        if (drP["ProbationStartDt"] != DBNull.Value)
                            personnelByImageList.ProbationStartDate = Convert.ToDateTime(drP["ProbationStartDt"]);
                        else
                            personnelByImageList.ProbationStartDate = DateTime.MinValue;
                        if (drP["ProbationEndDt"] != DBNull.Value)
                            personnelByImageList.ProbationEndDate = Convert.ToDateTime(drP["ProbationEndDt"]);
                        else
                            personnelByImageList.ProbationEndDate = DateTime.MinValue;
                        if (drP["VacationSchedule"] != DBNull.Value)
                            personnelByImageList.VacationSchedule = Convert.ToString(drP["VacationSchedule"]);
                        if (drP["IsActive"] != DBNull.Value)
                            personnelByImageList.IsActive = Convert.ToBoolean(drP["IsActive"]);
                        personnelByImageList.Address = new AddressDTO();
                        if (drP["AddressId"] != DBNull.Value)
                            personnelByImageList.Address.AddressID = Convert.ToInt16(drP["AddressId"]);
                        if (drP["AddressTypeId"] != DBNull.Value)
                            personnelByImageList.Address.AddressTypeID = Convert.ToInt16(drP["AddressTypeId"]);
                        if (drP["StreetNumber"] != DBNull.Value)
                            personnelByImageList.Address.StreetNumber = Convert.ToString(drP["StreetNumber"]);
                        if (drP["StreetName"] != DBNull.Value)
                            personnelByImageList.Address.StreetName = Convert.ToString(drP["StreetName"]);
                        if (drP["AptNo"] != DBNull.Value)
                            personnelByImageList.Address.AptNo = Convert.ToString(drP["AptNo"]);
                        if (drP["City"] != DBNull.Value)
                            personnelByImageList.Address.City = Convert.ToString(drP["City"]);
                        if (drP["State"] != DBNull.Value)
                            personnelByImageList.Address.State = Convert.ToString(drP["State"]);
                        if (drP["Zip"] != DBNull.Value)
                            personnelByImageList.Address.ZipCode = Convert.ToString(drP["Zip"]);
                        if (drP["ZipSuffix"] != DBNull.Value)
                            personnelByImageList.Address.ZipSuffix = Convert.ToString(drP["ZipSuffix"]);

                        personnelByImageList.PersonnelTitle = Convert.ToString(drP["Title"]);
                        if (drP["BadgeNo"] != DBNull.Value)
                            personnelByImageList.BadgeNumber = Convert.ToString(drP["BadgeNo"]);
                        if (drP["TitleID"] != DBNull.Value)
                            personnelByImageList.TitleID = Convert.ToInt64(drP["TitleID"]);
                        personnelByImageDTO.Add(personnelByImageList);
                    }
                }
                return personnelByImageDTO;           
        }
        /// <summary>
        /// This method returns the list of Employee Info DTO from the dataset.
        /// </summary>
        /// <param name="dsEmployees"></param>
        /// <returns></returns>
        public List<EmployeeInfoDTO> EmployeesInfoMapper(DataSet dsEmployees)
        {             
                List<EmployeeInfoDTO> employeeDTO = new List<EmployeeInfoDTO>();              

                if (dsEmployees.Tables.Count > 0)
                {
                    foreach (DataRow drPL in dsEmployees.Tables[0].Rows)
                    {
                        EmployeeInfoDTO employeeList = new EmployeeInfoDTO();
                        if (drPL["FirstName"] != DBNull.Value)
                            employeeList.FirstName = Convert.ToString(drPL["FirstName"]);
                        if (drPL["LastName"] != DBNull.Value)
                            employeeList.LastName = Convert.ToString(drPL["LastName"]);
                        if (drPL["ImageID"] != DBNull.Value)
                            employeeList.ImageID = Convert.ToInt32(drPL["ImageID"]);
                        if (drPL["Images"] != DBNull.Value)
                        {
                            byte[] array = (byte[])drPL["Images"];
                            employeeList.Images = array;
                        }
                        if (drPL["ReferenceNo"] != DBNull.Value)
                            employeeList.ReferenceNo = Convert.ToInt32(drPL["ReferenceNo"]);

                        employeeDTO.Add(employeeList);
                    }
                }
                return employeeDTO;          
        }
        /// <summary>
        /// This method returns the list of Employee Info DTO from the dataset.
        /// </summary>
        /// <param name="dsEmployees"></param>
        /// <returns></returns>
        public List<EmployeeListDTO> EmployeesListMapper(DataSet dsEmployees)
        {      
           
                List<EmployeeListDTO> employeeListDTO = new List<EmployeeListDTO>();

                if (dsEmployees.Tables.Count > 0)
                {
                    foreach (DataRow drPL in dsEmployees.Tables[0].Rows)
                    {
                        EmployeeListDTO employeeList = new EmployeeListDTO();
                        if (drPL["FirstName"] != DBNull.Value)
                            employeeList.FirstName = Convert.ToString(drPL["FirstName"]);
                        if (drPL["LastName"] != DBNull.Value)
                            employeeList.LastName = Convert.ToString(drPL["LastName"]);
                        if (drPL["ImageID"] != DBNull.Value)
                            employeeList.ImageID = Convert.ToInt32(drPL["ImageID"]);
                        if (drPL["ReferenceNo"] != DBNull.Value)
                            employeeList.ReferenceNo = Convert.ToInt32(drPL["ReferenceNo"]);

                        employeeListDTO.Add(employeeList);
                    }
                }
                return employeeListDTO;
           
        }
    }
}
