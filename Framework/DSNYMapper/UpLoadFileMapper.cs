﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.Mapper
{
    public class UpLoadFileMapper
    {

        public List<UpLoadFileDTO> ComplaintAttachmentsListDS249(DataSet ds)
        {

            List<UpLoadFileDTO> uploadFileList = new List<UpLoadFileDTO>();           

                List<UpLoadFileDTO> upList = new List<UpLoadFileDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    UpLoadFileDTO uDTO = new UpLoadFileDTO();
                    if (dr["IndexNo"] != DBNull.Value)
                        uDTO.IndexNo = Convert.ToInt32(dr["IndexNo"]);
                    if (dr["ParentID"] != DBNull.Value)
                        uDTO.ParentID = Convert.ToInt32(dr["ParentID"]);
                    if (dr["ChildId1"] != DBNull.Value)
                        uDTO.ChildID = Convert.ToString(dr["ChildId1"]);
                    if (dr["ApplicationName"] != DBNull.Value)
                        uDTO.ApplicationName = Convert.ToString(dr["ApplicationName"]);
                    if (dr["AttachementName"] != DBNull.Value)
                        uDTO.AttachmentName = Convert.ToString(dr["AttachementName"]);
                    if (dr["ContentType"] != DBNull.Value)
                        uDTO.ContentType = Convert.ToString(dr["ContentType"]);
                    if (dr["Attachments"] != DBNull.Value)
                    {
                        byte[] Image = (byte[])dr["Attachments"];
                        uDTO.Image = Image;
                    }
                    upList.Add(uDTO);

                }
                return upList;   

        }


        public List<UpLoadFileDTO> ComplaintAttachmentsList(DataSet ds)
        {

            List<UpLoadFileDTO> uploadFileList = new List<UpLoadFileDTO>();            

                List<UpLoadFileDTO> upList = new List<UpLoadFileDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {                     
                    UpLoadFileDTO uDTO = new UpLoadFileDTO();
                    if (dr["IndexNo"] != DBNull.Value)
                        uDTO.IndexNo = Convert.ToInt32(dr["IndexNo"]);
                    if (dr["ParentID"] != DBNull.Value)
                        uDTO.ParentID  = Convert.ToInt32(dr["ParentID"]);
                    if (dr["InspectionRequestId"] != DBNull.Value)
                        uDTO.InspectionRequestId = Convert.ToInt64(dr["InspectionRequestId"]);
                    if (dr["ChildId1"] != DBNull.Value)
                        uDTO.ChildID  = Convert.ToString(dr["ChildId1"]);
                    if (dr["ApplicationName"] != DBNull.Value)
                        uDTO.ApplicationName  = Convert.ToString(dr["ApplicationName"]);
                    if (dr["AttachementName"] != DBNull.Value)
                        uDTO.AttachmentName = Convert.ToString(dr["AttachementName"]);
                    if (dr["ContentType"] != DBNull.Value)
                        uDTO.ContentType = Convert.ToString(dr["ContentType"]);
                    if (dr["Attachments"] != DBNull.Value)
                    {
                        byte[] Image = (byte[])dr["Attachments"];
                        uDTO.Image = Image;                        
                    }
                    if (dr["AttachmentLocation"] != DBNull.Value)
                        uDTO.ImageLocation = Convert.ToString(dr["AttachmentLocation"]);                    
                    if (dr["ImageDate"] != DBNull.Value)
                        uDTO.ImageDate = Convert.ToDateTime(dr["ImageDate"]);
                        uDTO.TakenBy = FieldConvertToSting(dr["TakenBy"]);                    
                        uDTO.WhenTaken = FieldConvertToSting(dr["WhenTaken"]);                    
                        uDTO.Note = FieldConvertToSting(dr["Note"]);
                        uDTO.CCUNo = FieldConvertToSting(dr["CCUNo"]);
                        uDTO.District = FieldConvertToSting(dr["BoroCode"]);
                        uDTO.Location = FieldConvertToSting(dr["CrossStreetOne"]);
                        uDTO.BlockNo = FieldConvertToSting(dr["Block"]);
                        uDTO.LotNo = FieldConvertToSting(dr["Zone"]);
                        uDTO.House1 = FieldConvertToSting(dr["House1"]);
                        uDTO.House2 = FieldConvertToSting(dr["House2"]);
                        uDTO.StreetAddress = FieldConvertToSting(dr["StreetAddress"]);
                        uDTO.BoroCode = FieldConvertToSting(dr["BoroCode"]);
                        uDTO.CCUYear = FieldConvertToSting(dr["CCUYear"]);
                        uDTO.CrossStreetOne = FieldConvertToSting(dr["CrossStreetOne"]);
                        uDTO.CrossStreettwo = FieldConvertToSting(dr["CrossStreettwo"]);
                        uDTO.Block = FieldConvertToSting(dr["Block"]);
                        uDTO.Used_House1 = FieldConvertToSting(dr["Used_House1"]);
                        uDTO.Used_House2 = FieldConvertToSting(dr["Used_House2"]);
                        uDTO.Used_StreetAddress = FieldConvertToSting(dr["Used_StreetAddress"]);
                        uDTO.Used_CrossStreetOne = FieldConvertToSting(dr["Used_CrossStreetOne"]);
                        uDTO.Used_CrossStreetTwo = FieldConvertToSting(dr["Used_CrossStreetTwo"]);
                        uDTO.Zone = FieldConvertToSting(dr["Zone"]);
                        upList.Add(uDTO);
                   
                }
                return upList;
           

        }

        public List<UpLoadFileDTO> ComplaintAttachmentsListForLotCleaning(DataSet ds)
        {
            List<UpLoadFileDTO> uploadFileList = new List<UpLoadFileDTO>();
             List<UpLoadFileDTO> upList = new List<UpLoadFileDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    UpLoadFileDTO uDTO = new UpLoadFileDTO();
                    if (dr["DocumentID"] != DBNull.Value)
                        uDTO.DocumentID = Convert.ToInt32(dr["DocumentID"]);
                    if (dr["ParentID"] != DBNull.Value)
                        uDTO.ParentID = Convert.ToInt32(dr["ParentID"]);
                    if (dr["LotId"] != DBNull.Value)
                        uDTO.LotId = Convert.ToInt32(dr["LotId"]);
                    if (Convert.ToUInt64(dr["ChildId1"]) != 8)
                    {
                        if (dr["InspectionRequestId"] != DBNull.Value)
                            uDTO.InspectionRequestId = Convert.ToInt64(dr["InspectionRequestId"]);
                        uDTO.CCUNo = FieldConvertToSting(dr["CCUNo"]);
                        uDTO.District = FieldConvertToSting(dr["DistCd"]);
                        uDTO.Location = FieldConvertToSting(dr["CrossStreetOne"]);
                        uDTO.BlockNo = FieldConvertToSting(dr["Block"]);
                        uDTO.LotNo = FieldConvertToSting(dr["LotNo"]);
                        uDTO.House1 = FieldConvertToSting(dr["House1"]);
                        uDTO.House2 = FieldConvertToSting(dr["House2"]);
                        uDTO.StreetAddress = FieldConvertToSting(dr["StreetAddress"]);
                        uDTO.BoroCode = FieldConvertToSting(dr["BoroCode"]);
                        uDTO.CCUYear = FieldConvertToSting(dr["CCUYear"]);
                        uDTO.CrossStreetOne = FieldConvertToSting(dr["CrossStreetOne"]);
                        uDTO.CrossStreettwo = FieldConvertToSting(dr["CrossStreettwo"]);
                        uDTO.Block = FieldConvertToSting(dr["Block"]);
                        uDTO.Used_House1 = FieldConvertToSting(dr["Used_House1"]);
                        uDTO.Used_House2 = FieldConvertToSting(dr["Used_House2"]);
                        uDTO.Used_StreetAddress = FieldConvertToSting(dr["Used_StreetAddress"]);
                        uDTO.Used_CrossStreetOne = FieldConvertToSting(dr["Used_CrossStreetOne"]);
                        uDTO.Used_CrossStreetTwo = FieldConvertToSting(dr["Used_CrossStreetTwo"]);
                        uDTO.Zone = FieldConvertToSting(dr["Zone"]);                        
                    }
                    if (dr["ChildId1"] != DBNull.Value)
                        uDTO.ChildID = Convert.ToString(dr["ChildId1"]);
                    if (dr["ApplicationName"] != DBNull.Value)
                        uDTO.ApplicationName = Convert.ToString(dr["ApplicationName"]);
                    if (dr["AttachementName"] != DBNull.Value)
                        uDTO.AttachmentName = Convert.ToString(dr["AttachementName"]);
                    if (dr["ContentType"] != DBNull.Value)
                        uDTO.ContentType = Convert.ToString(dr["ContentType"]);                  
                    if (dr["AttachmentLocation"] != DBNull.Value)
                        uDTO.ImageLocation = Convert.ToString(dr["AttachmentLocation"]);
                    if (dr["Block_loc"] != DBNull.Value)
                        uDTO.BlockLoc = Convert.ToString(dr["Block_loc"]);
                    if (dr["Used_Block_loc"] != DBNull.Value)
                        uDTO.UsedBlockLoc = Convert.ToString(dr["Used_Block_loc"]);
                    if (dr["ImageDate"] != DBNull.Value)
                        uDTO.ImageDate = Convert.ToDateTime(dr["ImageDate"]);
                    uDTO.TakenBy = FieldConvertToSting(dr["TakenBy"]);
                    uDTO.WhenTaken = FieldConvertToSting(dr["WhenTaken"]);
                    uDTO.Note = FieldConvertToSting(dr["Note"]);                    
                    upList.Add(uDTO);
                }
                return upList;            
        }

        private String FieldConvertToSting(object field)
        {
            String fieldString = String.Empty;
            if((field != null) && (field != DBNull.Value))
                fieldString = Convert.ToString(field);
            if (String.IsNullOrEmpty(fieldString))
                return String.Empty;
            else
                return fieldString;
        }
    }
}