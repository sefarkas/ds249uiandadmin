﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.Mapper
{
    /// <summary>
    /// Medical Mapper created for Medical View
    /// </summary>
  public  class MedicalMapper
    {
      /// <summary>
      /// DTO Mapping created for mapper
      /// </summary>
      /// <param name="ds"></param>
      /// <returns></returns>
      public List<ComplaintDTO> ComplaintListMedicalMapper(DataSet ds)
      {
          List<ComplaintDTO> complaintDTO = new List<ComplaintDTO>();         

              foreach (DataRow dr in ds.Tables[0].Rows)
              {
                  ComplaintDTO cList = new ComplaintDTO();
                  if (dr["ComplaintId"] != DBNull.Value)
                      cList.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);              

                  if (dr["IndexNo"] != DBNull.Value)
                      cList.IndexNo = Convert.ToInt64(dr["IndexNo"]);
                  if (dr["LocationId"] != DBNull.Value)
                      cList.LocationID = Convert.ToInt64(dr["LocationId"]);
                  if (dr["LocationName"] != DBNull.Value)
                      cList.LocationName = Convert.ToString(dr["LocationName"]);
                  if (dr["RoutingLocation"] != DBNull.Value)
                      cList.RoutingLocation = Convert.ToString(dr["RoutingLocation"]);
                  if (dr["RespondentEmpID"] != DBNull.Value)
                      cList.EmployeeID = Convert.ToInt64(dr["RespondentEmpID"]);
                  if (dr["RespondentRefNumber"] != DBNull.Value)
                      cList.ReferenceNumber = Convert.ToString(dr["RespondentRefNumber"]);
                  if (dr["Approved"] != DBNull.Value)
                      cList.IsApproved = Convert.ToBoolean(dr["Approved"]);
                  if (dr["CreatedBy"] != DBNull.Value)
                      cList.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                  if (dr["CreatedDate"] != DBNull.Value)
                      cList.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                  if (dr["IncidentDate"] != DBNull.Value)
                      cList.IncidentDate = Convert.ToDateTime(dr["IncidentDate"]);
                  if (dr["Status"] != DBNull.Value)
                      cList.Status = Convert.ToString(dr["Status"]);
                  if (dr["Charges"] != DBNull.Value)
                      cList.Charge = dr["Charges"].ToString();
                  if (dr["RespondentFName"] != DBNull.Value)
                      cList.RespondentFName = dr["RespondentFName"].ToString();
                  if (dr["RespondentLName"] != DBNull.Value)
                      cList.RespondentLName = dr["RespondentLName"].ToString();
                  if (dr["complaintStatus"] != DBNull.Value)
                      cList.ComplaintStatus = Convert.ToInt16(dr["complaintStatus"]);
                  if (dr["complaintStatusDate"] != DBNull.Value)
                      cList.ComplaintStatusDate = Convert.ToDateTime(dr["complaintStatusDate"]);
                  if (dr["Indicator"] != DBNull.Value)
                      cList.Indicator = Convert.ToString(dr["Indicator"]);
                  complaintDTO.Add(cList);                
              }
                return complaintDTO;       

      }

      public List<MedicalPenaltyDTO> MedicalPenaltyListMapper(DataSet dsPenalyList)
      {         
              List<MedicalPenaltyDTO> medicalPenaltyDTO = new List<MedicalPenaltyDTO>();
              if (dsPenalyList.Tables.Count > 0)
              {
                  foreach (DataRow dr in dsPenalyList.Tables[0].Rows)
                  {
                      MedicalPenaltyDTO medicalPenaltyList = new MedicalPenaltyDTO();

                      if (dr["PenaltyGroupId"] != DBNull.Value)
                          medicalPenaltyList.PenaltyGroupID = Convert.ToInt64(dr["PenaltyGroupId"]);
                      if (dr["MedicalPenaltyTypecd"] != DBNull.Value)
                          medicalPenaltyList.PenaltyTypeCode = Convert.ToInt16(dr["MedicalPenaltyTypecd"]);
                      if (dr["Days"] != DBNull.Value)
                          medicalPenaltyList.Days = Convert.ToString(dr["Days"]);
                      if (dr["Hours"] != DBNull.Value)
                          medicalPenaltyList.Hours = Convert.ToString(dr["Hours"]);
                      if (dr["Minutes"] != DBNull.Value)
                          medicalPenaltyList.Minutes = Convert.ToString(dr["Minutes"]);
                      if (dr["MoneyValue"] != DBNull.Value)
                          medicalPenaltyList.MoneyValue = Convert.ToString(dr["MoneyValue"]);
                      if (dr["Notes"] != DBNull.Value)
                          medicalPenaltyList.Notes = Convert.ToString(dr["Notes"]);
                      medicalPenaltyDTO.Add(medicalPenaltyList);

                  }
              }
              return medicalPenaltyDTO;
         
      }

      public List<MedicalComplaintDTO> MedicalComplaintMapper(DataSet dsMedicalComplaint)
      {
         
              List<MedicalComplaintDTO> medicalComplaintDTO = new List<MedicalComplaintDTO>();

              if (dsMedicalComplaint.Tables.Count > 0)
              {
                  foreach (DataRow drMC in dsMedicalComplaint.Tables[0].Rows)
                  {
                      MedicalComplaintDTO listMedicalComplaint = new MedicalComplaintDTO();
                      if (drMC["MedicalCaseId"] != DBNull.Value)
                          listMedicalComplaint.MedicalCaseID = Convert.ToInt64(drMC["MedicalCaseId"]);
                      if (drMC["ComplaintId"] != DBNull.Value)
                          listMedicalComplaint.ComplaintID = Convert.ToInt64(drMC["ComplaintId"]);
                      if (drMC["CreatedUser"] != DBNull.Value)
                          listMedicalComplaint.CreateUser = Convert.ToInt16(drMC["CreatedUser"]);
                      if (drMC["CreatedDate"] != DBNull.Value)
                          listMedicalComplaint.CreateDate = Convert.ToDateTime(drMC["CreatedDate"]);
                      if (drMC["ModifiedUser"] != DBNull.Value)
                          listMedicalComplaint.ModifiedUser = Convert.ToInt16(drMC["ModifiedUser"]);
                      if (drMC["ModifiedDate"] != DBNull.Value)
                          listMedicalComplaint.ModifiedDate = Convert.ToDateTime(drMC["ModifiedDate"]);
                      if (drMC["FinalStatuscd"] != DBNull.Value)
                          listMedicalComplaint.FinalStatusCd = Convert.ToInt16(drMC["FinalStatuscd"]);
                      if (drMC["NonPenaltyResultcd"] != DBNull.Value)
                          listMedicalComplaint.NonPenaltyResultCd = Convert.ToInt16(drMC["NonPenaltyResultcd"]);
                      if (drMC["AdjudicationBodycd"] != DBNull.Value)
                          listMedicalComplaint.AdjudicationBodyCd = Convert.ToInt16(drMC["AdjudicationBodycd"]);
                      if (drMC["AdjudicationDate"] != DBNull.Value)
                          listMedicalComplaint.AjudicationDate = Convert.ToDateTime(drMC["AdjudicationDate"]);
                      if (drMC["IsAvailable"] != DBNull.Value)
                          listMedicalComplaint.IsAvailable = Convert.ToString(drMC["IsAvailable"]);
                      if (drMC["IsProbation"] != DBNull.Value)
                          listMedicalComplaint.IsProbation = Convert.ToBoolean(drMC["IsProbation"]);
                      medicalComplaintDTO.Add(listMedicalComplaint);
                  }
              }
              return medicalComplaintDTO;         
      }

      public List<MedicalStatusDTO> MedicalStatutsListMapper(DataSet ds)
      {         
              List<MedicalStatusDTO> bcadStatusDTO = new List<MedicalStatusDTO>();            
              foreach (DataRow dr in ds.Tables[0].Rows)
              {
                  MedicalStatusDTO medicalStatusList = new MedicalStatusDTO();

                  if (dr["Code_Id"] != DBNull.Value)
                      medicalStatusList.CodeID = Convert.ToInt16(dr["Code_Id"]);
                  if (dr["Code_Name"] != DBNull.Value)
                      medicalStatusList.CodeName = Convert.ToString(dr["Code_Name"]);
                  if (dr["Code_Description"] != DBNull.Value)
                      medicalStatusList.CodeDescription = Convert.ToString(dr["Code_Description"]);
                  bcadStatusDTO.Add(medicalStatusList);
              }             
              return bcadStatusDTO;          
      }

    }
}
