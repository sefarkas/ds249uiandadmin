﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.Mapper
{
    /// <summary>
    /// This class provides the functionality for the set of methods used for BCAD.
    /// </summary>
   public class BCADMapper
    {
        /// <summary>
        /// This method returns the list of BCAD complaint DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
       public List<ComplaintDTO> ComplaintListBCAD(DataSet ds)
       {           
           List<ComplaintDTO> complaintList = new List<ComplaintDTO>();                      
               List<ComplaintDTO> cList = new List<ComplaintDTO>();
               foreach (DataRow dr in ds.Tables[0].Rows)
               {
                   ComplaintDTO cDTO = new ComplaintDTO();
                   if (dr["ComplaintId"] != DBNull.Value)
                       cDTO.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                   if (dr["IndexNo"] != DBNull.Value)
                       cDTO.IndexNo = Convert.ToInt64(dr["IndexNo"]);
                   if (dr["LocationId"] != DBNull.Value)
                       cDTO.LocationID = Convert.ToInt64(dr["LocationId"]);
                   if (dr["LocationName"] != DBNull.Value)
                       cDTO.LocationName = Convert.ToString(dr["LocationName"]);
                   if (dr["RoutingLocation"] != DBNull.Value)
                       cDTO.RoutingLocation = Convert.ToString(dr["RoutingLocation"]);
                   if (dr["RespondentEmpID"] != DBNull.Value)
                       cDTO.EmployeeID = Convert.ToInt64(dr["RespondentEmpID"]);
                   if (dr["RespondentRefNumber"] != DBNull.Value)
                       cDTO.ReferenceNumber = Convert.ToString(dr["RespondentRefNumber"]);
                   if (dr["Approved"] != DBNull.Value)
                       cDTO.IsApproved = Convert.ToBoolean(dr["Approved"]);
                   if (dr["CreatedBy"] != DBNull.Value)
                       cDTO.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                   if (dr["CreatedDate"] != DBNull.Value)
                       cDTO.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                   if (dr["IncidentDate"] != DBNull.Value)
                       cDTO.IncidentDate = Convert.ToDateTime(dr["IncidentDate"]);
                   if (dr["Status"] != DBNull.Value)
                       cDTO.Status = Convert.ToString(dr["Status"]);
                   if (dr["Charges"] != DBNull.Value)
                       cDTO.Charge = dr["Charges"].ToString();
                   if (dr["RespondentFName"] != DBNull.Value)
                       cDTO.RespondentFName = dr["RespondentFName"].ToString();
                   if (dr["RespondentLName"] != DBNull.Value)
                       cDTO.RespondentLName = dr["RespondentLName"].ToString();
                   if (dr["complaintStatus"] != DBNull.Value)
                       cDTO.ComplaintStatus = Convert.ToInt16(dr["complaintStatus"]);
                   if (dr["complaintStatusDate"] != DBNull.Value)
                       cDTO.ComplaintStatusDate = Convert.ToDateTime(dr["complaintStatusDate"]);
                   if (dr["Indicator"] != DBNull.Value)
                       cDTO.Indicator = Convert.ToString(dr["Indicator"]);           
                 
                   cList.Add(cDTO);
               }
               return cList;         
       }
       /// <summary>
       /// This method returns the list of BCAD code list DTO from the dataset.
       /// </summary>
       /// <param name="ds"></param>
       /// <returns></returns>
       public List<CodeDTO> CodeListMapper(DataSet dsCodeList)
       {          
         
               List<CodeDTO> codeDTO = new List<CodeDTO>();
               if (dsCodeList.Tables.Count > 0)
               {
                   foreach (DataRow dr in dsCodeList.Tables[0].Rows)
                   {
                       CodeDTO cList = new CodeDTO();
                       
                       if (dr["Code_Id"] != DBNull.Value)
                           cList.CodeID = Convert.ToInt16(dr["Code_Id"]);
                       if (dr["Code_Name"] != DBNull.Value)
                           cList.CodeName = Convert.ToString(dr["Code_Name"]);
                       if (dr["Code_Description"] != DBNull.Value)
                           cList.CodeDescription = Convert.ToString(dr["Code_Description"]);
                       if (dr["Value_Data_Type"] != DBNull.Value)
                           cList.ValueDataType = Convert.ToString(dr["Value_Data_Type"]);
                       codeDTO.Add(cList);
                   }
               }
               return codeDTO;           
       }
       /// <summary>
       /// This method returns the list of BCAD status list DTO from the dataset.
       /// </summary>
       /// <param name="ds"></param>
       /// <returns></returns>
       public List<BCADStatusDTO> BCADStatutsListMapper(DataSet ds)
       {           
           
               List<BCADStatusDTO> bcadStatusDTO = new List<BCADStatusDTO>();               
               foreach (DataRow dr in ds.Tables[0].Rows)
               {
                   BCADStatusDTO bcadStatusList = new BCADStatusDTO();

                   if (dr["Code_Id"] != DBNull.Value)
                       bcadStatusList.CodeID = Convert.ToInt16(dr["Code_Id"]);
                   if (dr["Code_Name"] != DBNull.Value)
                       bcadStatusList.CodeName = Convert.ToString(dr["Code_Name"]);
                   if (dr["Code_Description"] != DBNull.Value)
                       bcadStatusList.CodeDescription = Convert.ToString(dr["Code_Description"]);
                   bcadStatusDTO.Add(bcadStatusList);
               }
               return bcadStatusDTO;
           
       }
       /// <summary>
       /// This method returns the list of BCAD complaints list DTO from the dataset.
       /// </summary>
       /// <param name="dsbcadComplaint"></param>
       /// <returns></returns>
       public List<BCADComplaintDTO> BCADComplaintMapper(DataSet dsbcadComplaint)
       {
          
               List<BCADComplaintDTO> bcadComplaintDTO = new List<BCADComplaintDTO>();
               
               if (dsbcadComplaint.Tables.Count > 0)
               {
                   foreach (DataRow drBC in dsbcadComplaint.Tables[0].Rows)
                   {
                       BCADComplaintDTO listBCADComplaint = new BCADComplaintDTO();
                       if (drBC["BCADCaseId"] != DBNull.Value)
                           listBCADComplaint.BCADCaseID = Convert.ToInt64(drBC["BCADCaseId"]);
                       if (drBC["ComplaintId"] != DBNull.Value)
                           listBCADComplaint.ComplaintID = Convert.ToInt64(drBC["ComplaintId"]);
                       if (drBC["CreatedUser"] != DBNull.Value)
                           listBCADComplaint.CreateUser = Convert.ToInt16(drBC["CreatedUser"]);
                       if (drBC["CreatedDate"] != DBNull.Value)
                           listBCADComplaint.CreateDate = Convert.ToDateTime(drBC["CreatedDate"]);
                       if (drBC["ModifiedUser"] != DBNull.Value)
                           listBCADComplaint.ModifiedUser = Convert.ToInt16(drBC["ModifiedUser"]);
                       if (drBC["ModifiedDate"] != DBNull.Value)
                           listBCADComplaint.ModifiedDate = Convert.ToDateTime(drBC["ModifiedDate"]);
                       if (drBC["FinalStatuscd"] != DBNull.Value)
                           listBCADComplaint.FinalStatusCd = Convert.ToInt16(drBC["FinalStatuscd"]);
                       if (drBC["NonPenaltyResultcd"] != DBNull.Value)
                           listBCADComplaint.NonPenaltyResultCd = Convert.ToInt16(drBC["NonPenaltyResultcd"]);
                       if (drBC["AdjudicationBodycd"] != DBNull.Value)
                           listBCADComplaint.AdjudicationBodyCd = Convert.ToInt16(drBC["AdjudicationBodycd"]);
                       if (drBC["AdjudicationDate"] != DBNull.Value)
                           listBCADComplaint.AjudicationDate = Convert.ToDateTime(drBC["AdjudicationDate"]);
                       if (drBC["IsAvailable"] != DBNull.Value)
                           listBCADComplaint.IsAvailable = Convert.ToString(drBC["IsAvailable"]);
                       if (drBC["IsProbation"] != DBNull.Value)
                           listBCADComplaint.IsProbation = Convert.ToBoolean(drBC["IsProbation"]);
                       bcadComplaintDTO.Add(listBCADComplaint);
                   }
               }
               return bcadComplaintDTO;           
       }
       /// <summary>
       /// This method returns the list of BCAD penalty list DTO from the dataset.
       /// </summary>
       /// <param name="dsPenalyList"></param>
       /// <returns></returns>
       public List<BCADPenaltyDTO> BCADPenaltyListMapper(DataSet dsPenalyList)
       {          
           List<BCADPenaltyDTO> bcadPenaltyDTO = new List<BCADPenaltyDTO>();

               if (dsPenalyList.Tables.Count > 0)
               {
                   foreach (DataRow dr in dsPenalyList.Tables[0].Rows)
                   {
                       BCADPenaltyDTO bcadPenaltyList = new BCADPenaltyDTO();                      
                       if (dr["PenaltyGroupId"] != DBNull.Value)
                           bcadPenaltyList.PenaltyGroupID = Convert.ToInt64(dr["PenaltyGroupId"]);
                       if (dr["BCADPenaltyTypecd"] != DBNull.Value)
                           bcadPenaltyList.PenaltyTypeCode = Convert.ToInt16(dr["BCADPenaltyTypecd"]);
                       if (dr["Days"] != DBNull.Value)
                           bcadPenaltyList.Days = Convert.ToString(dr["Days"]);
                       if (dr["Hours"] != DBNull.Value)
                           bcadPenaltyList.Hours = Convert.ToString(dr["Hours"]);
                       if (dr["Minutes"] != DBNull.Value)
                           bcadPenaltyList.Minutes = Convert.ToString(dr["Minutes"]);
                       if (dr["MoneyValue"] != DBNull.Value)
                           bcadPenaltyList.MoneyValue = Convert.ToString(dr["MoneyValue"]);
                       if (dr["Notes"] != DBNull.Value)
                           bcadPenaltyList.Notes = Convert.ToString(dr["Notes"]);
                       bcadPenaltyDTO.Add(bcadPenaltyList);
          
                   }
               }
               return bcadPenaltyDTO;          
       }

    }
}
