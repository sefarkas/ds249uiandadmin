﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.DTO;


namespace DSNY.DSNYCP.Mapper
{
    /// <summary>
    /// This class provides the functionality for the set of methods used for users login and admin login.
    /// </summary>
    public class SecurityMapper
    {
        /// <summary>
        /// This method returns the list of Users DTO from the dataset.
        /// </summary>
        /// <param name="dsUserList"></param>
        /// <returns></returns>
        public List<UserDTO> UserListMapper(DataSet dsUserList)
        {          
                List<UserDTO> userList = new List<UserDTO>();

                if (dsUserList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsUserList.Tables[0].Rows)
                    {
                        UserDTO uList = new UserDTO();
                        if (dr["PersonalID"] != DBNull.Value)
                            uList.PersonalID = Convert.ToInt16(dr["PersonalID"]);
                        if (dr["IsTempPassword"] != DBNull.Value)
                            uList.IsTempPassword = Convert.ToBoolean(dr["IsTempPassword"]);
                        if (dr["IsAdmin"] != DBNull.Value)
                            uList.IsAdmin = Convert.ToBoolean(dr["IsAdmin"]);
                        if (dr["LastLogin"] != DBNull.Value)
                            uList.LastLogin = Convert.ToDateTime(dr["LastLogin"]);
                        if (dr["UserID"] != DBNull.Value)
                            uList.UserID = Convert.ToString(dr["UserID"]);
                        if (dr["EmailID"] != DBNull.Value)
                            uList.EmailID  = Convert.ToString(dr["EmailID"]);                                 
                        userList.Add(uList);
                    }
                }
                return userList;           
        }

        // <summary>
        /// This class provides the roles for the user with the application 
        /// </summary>
        public List<UserDTO> UserRolesListMapper(DataSet dsUserList)
        {           
                List<UserDTO> userList = new List<UserDTO>();
                if (dsUserList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsUserList.Tables[0].Rows)
                    {
                        UserDTO uList = new UserDTO();
                        if (dr["PersonalID"] != DBNull.Value)
                            uList.PersonalID = Convert.ToInt16(dr["PersonalID"]);     
                        if (dr["UserID"] != DBNull.Value)
                            uList.UserID = Convert.ToString(dr["UserID"]);                     
                        if (dr["MembershipID"] != DBNull.Value)
                            uList.LastName = Convert.ToString(dr["MembershipID"]);
                        if (dr["RoleID"] != DBNull.Value)
                            uList.RoleID = Convert.ToString(dr["RoleID"]);
                        if (dr["Roles"] != DBNull.Value)
                            uList.Roles = Convert.ToString(dr["Roles"]);
                        if (dr["FirstName"] != DBNull.Value)
                            uList.FirstName = Convert.ToString(dr["FirstName"]);
                        if (dr["LastName"] != DBNull.Value)
                            uList.LastName = Convert.ToString(dr["LastName"]);
                        userList.Add(uList);
                    }
                }
                return userList;           
        }
        
        
        
        /// <summary>
        /// This method returns the list of Personnel info DTO from the dataset.
        /// </summary>
        /// <param name="dsPersonnelList"></param>
        /// <returns></returns>
        public List<PersonnelDTO> PersonnelListMapper(DataSet dsPersonnelList)
        {           
                List<PersonnelDTO> personnelList = new List<PersonnelDTO>();

                if (dsPersonnelList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsPersonnelList.Tables[0].Rows)
                    {
                        PersonnelDTO pList = new PersonnelDTO();

                        if (dr["PersonID"] != DBNull.Value)
                            pList.PersonnelID = Convert.ToInt64(dr["PersonID"]); // 16 --> 64 by Farkas 27Feb2020
                        if (dr["ReferenceNo"] != DBNull.Value)
                            pList.ReferenceNumber = Convert.ToString(dr["ReferenceNo"]);
                        if (dr["FirstName"] != DBNull.Value)
                            pList.FirstName = Convert.ToString(dr["FirstName"]);
                        if (dr["MiddleName"] != DBNull.Value)
                            pList.MiddleName = Convert.ToString(dr["MiddleName"]);
                        if (dr["LastName"] != DBNull.Value)
                            pList.LastName = Convert.ToString(dr["LastName"]);
                        
                        if (dr["UserName"] != DBNull.Value)
                            pList.UserName = Convert.ToString(dr["UserName"]);
                        if (dr["SSN"] != DBNull.Value)
                            pList.SSN = Convert.ToString(dr["SSN"]);
                        if (dr["IsActive"] != DBNull.Value)
                            pList.IsActive = Convert.ToBoolean(dr["IsActive"]);
                        if (dr["Title"] != DBNull.Value)
                            pList.PersonnelTitle = Convert.ToString(dr["Title"]);
                        if (dr["PayCode"] != DBNull.Value)
                            pList.PayCodeDescription = Convert.ToString(dr["PayCode"]);

                        if (dr["EmailID"] != DBNull.Value)
                            pList.EmailID = Convert.ToString(dr["EmailID"]);
                        if (dr["IsActive"] != DBNull.Value)
                            pList.IsDeleted = Convert.ToBoolean(dr["IsDeleted"]);
                        if (dr["IsAdmin"] != DBNull.Value)
                            pList.IsAdmin = Convert.ToBoolean(dr["IsAdmin"]);


                        personnelList.Add(pList);
                    }
                }
                return personnelList;           
        }
       /// <summary>
        /// This method returns the list of roles info DTO from the dataset.
       /// </summary>
       /// <param name="dsRoleList"></param>
       /// <returns></returns>
        public List<RoleDTO> RoleListMapper(DataSet dsRoleList)
        {
           
                List<RoleDTO> roleList = new List<RoleDTO>();

                if (dsRoleList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsRoleList.Tables[0].Rows)
                    {
                        RoleDTO rList = new RoleDTO();


                        if (dr["ID"] != DBNull.Value)
                            rList.RoleID = Convert.ToInt16(dr["ID"]);
                        if (dr["Roles"] != DBNull.Value)
                            rList.RoleDesc = Convert.ToString(dr["Roles"]);
                        roleList.Add(rList);
                    }
                }
             return  roleList;                     
        }
        /// <summary>
        /// This method returns the list of membership info DTO from the dataset.
        /// </summary>
        /// <param name="dsMembershipList"></param>
        /// <returns></returns>
        public List<MembershipDTO> MembershipListMapper(DataSet dsMembershipList)
        {
          
                List<MembershipDTO> MembershipList = new List<MembershipDTO>();

                if (dsMembershipList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsMembershipList.Tables[0].Rows)
                    {
                        MembershipDTO mList = new MembershipDTO();
                        if (dr["ID"] != DBNull.Value)
                            mList.ID = Convert.ToInt64(dr["ID"]);  // 16 --> 64 by Farkas 27Feb2020
                        if (dr["PersonalID"] != DBNull.Value)
                            mList.PersonalID = Convert.ToInt64(dr["PersonalID"]);
                        if (dr["MembershipID"] != DBNull.Value)
                            mList.MembershipID = Convert.ToInt16(dr["MembershipID"]);
                        if (dr["RoleID"] != DBNull.Value)
                            mList.RoleID = Convert.ToInt64( dr["RoleID"] ); // Convert.ToInt16(dr["RoleID"]); by Farkas 27Feb2020
                    if (dr["Roles"] != DBNull.Value)
                            mList.RoleDesc = Convert.ToString(dr["Roles"]);
                        MembershipList.Add(mList);
                    }
                }
                return MembershipList;           
        }

        /// <summary>
        /// This method returns the list of membership info DTO from the dataset.
        /// </summary>
        /// <param name="dsGetMembershipList"></param>
        /// <returns></returns>
        public List<MembershipDTO> GetMembershipMapper(DataSet dsGetMembershipList)
        {
          
                List<MembershipDTO> MembershipList = new List<MembershipDTO>();

                if (dsGetMembershipList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsGetMembershipList.Tables[0].Rows)
                    {
                        MembershipDTO mList = new MembershipDTO();


                        if (dr["MembershipID"] != DBNull.Value)
                            mList.MembershipID = Convert.ToInt16(dr["MembershipID"]);
                        if (dr["PersonalID"] != DBNull.Value)
                            mList.PersonalID = Convert.ToInt64(dr["PersonalID"]);
                        if (dr["MemberShipDesc"] != DBNull.Value)
                            if (Convert.ToString(dr["MemberShipDesc"]) == "EMPLOYEE APPLICATION")
                            { mList.MembershipDesc = "EMPLOYEE"; }
                            else
                            {
                            mList.MembershipDesc = Convert.ToString(dr["MemberShipDesc"]);
                            }
                        MembershipList.Add(mList);
                    }
                }
                return MembershipList;            
        }

        /// <summary>
        /// This method returns the list of membership info DTO from the dataset.
        /// </summary>
        /// <param name="dsGetMembershipList"></param>
        /// <returns></returns>
        public List<MembershipDTO> GetMembersMapper(DataSet dsGetMembershipList)
        {           
                List<MembershipDTO> MembershipList = new List<MembershipDTO>();
                if (dsGetMembershipList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsGetMembershipList.Tables[0].Rows)
                    {
                        MembershipDTO mList = new MembershipDTO();


                        if (dr["ID"] != DBNull.Value)
                            mList.MembershipID = Convert.ToInt16(dr["ID"]);
                       
                        if (dr["MemberShipDesc"] != DBNull.Value)
                            mList.MembershipDesc = Convert.ToString(dr["MemberShipDesc"]);

                        MembershipList.Add(mList);
                    }
                }
                return MembershipList;                       
        }


        /// <summary>
        /// This method returns the list of membership info DTO from the dataset.
        /// </summary>
        /// <param name="dsGetMembershipList"></param>
        /// <returns></returns>
        public List<HideChargesDto> GetHideCharges(DataSet dsGetHideChargesList)
        {
           
                List<HideChargesDto> hideChargesList = new List<HideChargesDto>();

                if (dsGetHideChargesList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsGetHideChargesList.Tables[0].Rows)
                    {
                        HideChargesDto hList = new HideChargesDto();


                        if (dr["ViewChargesId"] != DBNull.Value)
                            hList.ViewChargeId = Convert.ToInt16(dr["ViewChargesId"]);
                        if (dr["ChargeTypeId"] != DBNull.Value)
                            hList.ChargeTypeId = Convert.ToInt16(dr["ChargeTypeId"]);
                        if (dr["EmployeeId"] != DBNull.Value)
                            hList.EmployeeId = Convert.ToInt16(dr["EmployeeId"]);
                        if (dr["ChargeDesc"] != DBNull.Value)
                            hList.ChargeDescription = Convert.ToString(dr["ChargeDesc"]);
                        if (dr["ReferenceNumber"] != DBNull.Value)
                            hList.ReferenceNumber = Convert.ToString(dr["ReferenceNumber"]);

                        hideChargesList.Add(hList);
                    }
                }
                return hideChargesList;            
        }

    }
}

