﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.Mapper
{
    /// <summary>
    /// This class provides the functionality for the set of methods used for advocate.
    /// </summary>
    public class AdvocateMapper
    {
        /// <summary>
        /// This method returns the list of advocate complaints DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<ComplaintDTO> ComplaintListAdvocate(DataSet ds)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();
             List<ComplaintDTO> cList = new List<ComplaintDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ComplaintDTO cDTO = new ComplaintDTO();
                    if (dr["ComplaintId"] != DBNull.Value)
                        cDTO.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                    if (dr["Datasource"] != DBNull.Value)
                        cDTO.DataSource = Convert.ToString(dr["Datasource"]);
                    if (dr["IndexNo"] != DBNull.Value)
                        cDTO.IndexNo = Convert.ToInt64(dr["IndexNo"]);    
                    if (dr["ParentIndexNo"] != DBNull.Value)
                        cDTO.ParentIndexNo = Convert.ToInt64(dr["ParentIndexNo"]);
                    if (dr["LocationId"] != DBNull.Value)
                        cDTO.LocationID = Convert.ToInt64(dr["LocationId"]);
                    if (dr["LocationName"] != DBNull.Value)
                        cDTO.LocationName = Convert.ToString(dr["LocationName"]);
                    if (dr["RoutingLocation"] != DBNull.Value)
                        cDTO.RoutingLocation = Convert.ToString(dr["RoutingLocation"]);
                    if (dr["RespondentEmpID"] != DBNull.Value)
                        cDTO.EmployeeID = Convert.ToInt64(dr["RespondentEmpID"]);
                    if (dr["RespondentRefNumber"] != DBNull.Value)
                        cDTO.ReferenceNumber = Convert.ToString(dr["RespondentRefNumber"]);
                    if (dr["Approved"] != DBNull.Value)
                        cDTO.IsApproved = Convert.ToBoolean(dr["Approved"]);
                    if (dr["CreatedBy"] != DBNull.Value)
                        cDTO.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                    if (dr["CreatedDate"] != DBNull.Value)
                        cDTO.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                    if (dr["IncidentDate"] != DBNull.Value)
                        cDTO.IncidentDate = Convert.ToDateTime(dr["IncidentDate"]);
                    if (dr["Status"] != DBNull.Value)
                        cDTO.Status = Convert.ToString(dr["Status"]);
                    if (dr["Charges"] != DBNull.Value)
                        cDTO.Charge = dr["Charges"].ToString();
                    if(dr["Jt1"]!=DBNull.Value)
                        cDTO.Jt1 = dr["Jt1"].ToString();                  
                    if (dr["RespondentFName"] != DBNull.Value)
                        cDTO.RespondentFName = dr["RespondentFName"].ToString();
                    if (dr["RespondentLName"] != DBNull.Value)
                        cDTO.RespondentLName = dr["RespondentLName"].ToString();
                    if (dr["complaintStatus"] != DBNull.Value)
                        cDTO.ComplaintStatus = Convert.ToInt16(dr["complaintStatus"]);
                    if (dr["complaintStatusDate"] != DBNull.Value)
                        cDTO.ComplaintStatusDate = Convert.ToDateTime(dr["complaintStatusDate"]);
                    if (dr["Indicator"] != DBNull.Value)
                        cDTO.Indicator = Convert.ToString(dr["Indicator"]);
                    if (dr["AdvocacyCaseId"] != DBNull.Value)
                        cDTO.CaseID = Convert.ToInt64(dr["AdvocacyCaseId"]);
                    if (dr["IsProbation"] != DBNull.Value)
                        cDTO.IsProbation = Convert.ToBoolean(dr["IsProbation"]);
                    cList.Add(cDTO);
                }
                return cList;       

        }

        public List<ComplaintDTO> ComplaintListCombined(DataSet ds)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();          
                List<ComplaintDTO> cList = new List<ComplaintDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ComplaintDTO cDTO = new ComplaintDTO();
                    if (dr["ComplaintId"] != DBNull.Value)
                        cDTO.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                    if (dr["IndexNo"] != DBNull.Value)
                        cDTO.IndexNo = Convert.ToInt64(dr["IndexNo"]);
                    if (dr["ParentIndexNo"] != DBNull.Value)
                        cDTO.ParentIndexNo = Convert.ToInt64(dr["ParentIndexNo"]);
                     if (dr["IncidentDate"] != DBNull.Value)
                        cDTO.IncidentDate = Convert.ToDateTime(dr["IncidentDate"]);
                    if (dr["Status"] != DBNull.Value)
                        cDTO.Status = Convert.ToString(dr["Status"]);
                    if (dr["Charges"] != DBNull.Value)
                        cDTO.Charge = dr["Charges"].ToString();
                    if (dr["RespondentFName"] != DBNull.Value)
                        cDTO.RespondentFName = dr["RespondentFName"].ToString();
                    if (dr["RespondentLName"] != DBNull.Value)
                        cDTO.RespondentLName = dr["RespondentLName"].ToString();
                    if (dr["complaintStatusDate"] != DBNull.Value)
                        cDTO.ComplaintStatusDate = Convert.ToDateTime(dr["complaintStatusDate"]);
                    if (dr["AdvocacyCaseId"] != DBNull.Value)
                        cDTO.CaseID = Convert.ToInt64(dr["AdvocacyCaseId"]);
                    cList.Add(cDTO);
                }
                return cList;            
        }


        public List<ComplaintDTO> CombinedComplaintList(DataSet ds)
        {
            List<ComplaintDTO> complaintList = new List<ComplaintDTO>();
          
                List<ComplaintDTO> cList = new List<ComplaintDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ComplaintDTO cDTO = new ComplaintDTO();
                    if (dr["IndexNo"] != DBNull.Value)
                        cDTO.IndexNo = Convert.ToInt64(dr["IndexNo"]);               
                
                    cList.Add(cDTO);
                }
                return cList;         

        }
        /// <summary>
        /// This method returns the list of advocate complaints DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>

        public List<ComplaintAdvocacyDTO> ComplaintAdvocatcy(DataSet ds)
        {
           
                List<ComplaintAdvocacyDTO> cList = new List<ComplaintAdvocacyDTO>();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ComplaintAdvocacyDTO cDTO = new ComplaintAdvocacyDTO();

                    if (dr["AdvocacyCaseId"] != DBNull.Value)
                        cDTO.AdvocacyCaseId = Convert.ToInt64(dr["AdvocacyCaseId"]);

                    if (dr["ComplaintId"] != DBNull.Value)
                        cDTO.ComplaintId = Convert.ToInt64(dr["ComplaintId"]);

                    if (dr["CaseClassificationcd"] != DBNull.Value)
                        cDTO.CaseClassificationcd = Convert.ToInt64(dr["CaseClassificationcd"]);

                    if (dr["OriginationSourcecd"] != DBNull.Value)
                        cDTO.OriginationSourcecd = Convert.ToInt64(dr["OriginationSourcecd"]);

                    if (dr["SpecialStatus_cd"] != DBNull.Value)
                        cDTO.SpecialStatus_cd = Convert.ToInt64(dr["SpecialStatus_cd"]);

                    if (dr["TrialReturnStatuscd"] != DBNull.Value)
                        cDTO.TrialReturnStatuscd = Convert.ToInt64(dr["TrialReturnStatuscd"]);

                    if (dr["TrialReturnDate"] != DBNull.Value)
                        cDTO.TrialReturnDate = Convert.ToDateTime(dr["TrialReturnDate"]);

                    if (dr["ReviewStatuscd"] != DBNull.Value)
                        cDTO.ReviewStatuscd = Convert.ToInt64(dr["ReviewStatuscd"]);

                    if (dr["ReviewStatusDate"] != DBNull.Value)
                        cDTO.ReviewStatusDate = Convert.ToDateTime(dr["ReviewStatusDate"]);

                    if (dr["Actioncd"] != DBNull.Value)
                        cDTO.Actioncd = Convert.ToInt64(dr["Actioncd"]);

                    if (dr["ActionDate"] != DBNull.Value)
                        cDTO.ActionDate = Convert.ToDateTime(dr["ActionDate"]);

                    if (dr["isClosed"] != DBNull.Value)
                        cDTO.isClosed = Convert.ToInt16(dr["isClosed"]);

                    if (dr["ClosedDate"] != DBNull.Value)
                        cDTO.ClosedDate = Convert.ToDateTime(dr["ClosedDate"]);

                    if (dr["COBStatus_cd"] != DBNull.Value)
                        cDTO.COBStatus_cd = Convert.ToInt64(dr["COBStatus_cd"]);

                    if (dr["COBDate"] != DBNull.Value)
                        cDTO.COBDate = Convert.ToDateTime(dr["COBDate"]);

                    if (dr["FinalStatuscd"] != DBNull.Value)
                        cDTO.FinalStatuscd = Convert.ToInt16(dr["FinalStatuscd"]);

                    if (dr["CaseComment"] != DBNull.Value)
                        cDTO.CaseComment = Convert.ToString(dr["CaseComment"]);

                    if (dr["RecieveUser"] != DBNull.Value)
                        cDTO.RecieveUserName = Convert.ToString(dr["RecieveUser"]);

                    if (dr["RecieveDate"] != DBNull.Value)
                        cDTO.RecieveDate = Convert.ToDateTime(dr["RecieveDate"]);

                    if (dr["ModifiedUser"] != DBNull.Value)
                        cDTO.ModifiedUserName = Convert.ToString(dr["ModifiedUser"]);

                    if (dr["ModifiedDate"] != DBNull.Value)
                        cDTO.ModifiedDate = Convert.ToDateTime(dr["ModifiedDate"]);

                    if (dr["IsAvailable"] != DBNull.Value)
                        cDTO.IsAvailable = Convert.ToString(dr["IsAvailable"]);

                    if(dr["IsProbation"]!=DBNull.Value)
                        cDTO.IsProbation = Convert.ToBoolean(dr["IsProbation"]);

                    cList.Add(cDTO);
                }
                return cList;           
        }
        /// <summary>
        /// This method returns the list of advocate hearing DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<AdvocateHearingDTO> AdvocateHearing(DataSet ds)
        {            
                List<AdvocateHearingDTO> objAdvHearing = new List<AdvocateHearingDTO>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AdvocateHearingDTO advDTO = new AdvocateHearingDTO();

                    if (dr["AdvocateHearingID"] != DBNull.Value)
                        advDTO.AdvocateHearingID = Convert.ToInt64(dr["AdvocateHearingID"]);

                    if (dr["AdvocacyCaseID"] != DBNull.Value)
                        advDTO.AdvocacyCaseID = Convert.ToInt64(dr["AdvocacyCaseID"]);

                    if (dr["HearingDt"] != DBNull.Value)
                        advDTO.HearingDt = Convert.ToDateTime(dr["HearingDt"]);

                    if (dr["HearingStatusCd"] != DBNull.Value)
                        advDTO.HearingStatusCd = Convert.ToInt64(dr["HearingStatusCd"]);

                    if (dr["CalStatusCd"] != DBNull.Value)
                        advDTO.CalStatusCd = Convert.ToInt64(dr["CalStatusCd"]);

                    if (dr["HearingTypeCd"] != DBNull.Value)
                        advDTO.HearingTypeCd = Convert.ToInt64(dr["HearingTypeCd"]);

                    if (dr["HearingComments"] != DBNull.Value)
                        advDTO.HearingComments = Convert.ToString(dr["HearingComments"]);

                    if (dr["ModifiedBy"] != DBNull.Value)
                        advDTO.ModifiedBy = Convert.ToInt64(dr["ModifiedBy"]); // 16 --> 64 by Farkas 28Feb2020

                    if (dr["ModifiedDt"] != DBNull.Value)
                        advDTO.ModifiedDt = Convert.ToDateTime(dr["ModifiedDt"]);

                    objAdvHearing.Add(advDTO);
                }
                return objAdvHearing;
            
        }
        /// <summary>
        /// This method returns the list of advocate hearing DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<AdvocateAppealDTO> AdvocateAppeal(DataSet ds)
        {
              List<AdvocateAppealDTO> objAdvAppeal = new List<AdvocateAppealDTO>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AdvocateAppealDTO advDTO = new AdvocateAppealDTO();

                    if (dr["AdvocateAppealID"] != DBNull.Value)
                        advDTO.AdvocateAppealID = Convert.ToInt64(dr["AdvocateAppealID"]);

                    if (dr["AdvocacyCaseID"] != DBNull.Value)
                        advDTO.AdvocacyCaseID = Convert.ToInt64(dr["AdvocacyCaseID"]);

                    if (dr["AppealDt"] != DBNull.Value)
                        advDTO.AppealDt = Convert.ToDateTime(dr["AppealDt"]);

                    if (dr["AppealresultCd"] != DBNull.Value)
                        advDTO.AppealresultCd = Convert.ToInt64(dr["AppealresultCd"]);

                    if (dr["AppealComments"] != DBNull.Value)
                        advDTO.AppealComments = Convert.ToString(dr["AppealComments"]);

                    if (dr["AppealCd"] != DBNull.Value)
                        advDTO.AppealCd = Convert.ToInt64(dr["AppealCd"]);

                    if (dr["ModifiedBy"] != DBNull.Value)
                        advDTO.ModifiedBy = Convert.ToInt16(dr["ModifiedBy"]);

                    if (dr["ModifiedDt"] != DBNull.Value)
                        advDTO.ModifiedDt = Convert.ToDateTime(dr["ModifiedDt"]);

                    objAdvAppeal.Add(advDTO);
                }
                return objAdvAppeal;
           
        }

        /// <summary>
        /// This method returns the list of advocate penalty DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>

        public List<AdvocatePenaltyDTO> AdvocatePenalty(DataSet ds)
        {
           
                List<AdvocatePenaltyDTO> objAdvPenalty = new List<AdvocatePenaltyDTO>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AdvocatePenaltyDTO advDTO = new AdvocatePenaltyDTO();

                    if (dr["AdvocatePenaltyID"] != DBNull.Value)
                        advDTO.AdvocatePenaltyID = Convert.ToInt64(dr["AdvocatePenaltyID"]);

                    if (dr["AdvocacyCaseID"] != DBNull.Value)
                        advDTO.AdvocacyCaseID = Convert.ToInt64(dr["AdvocacyCaseID"]);

                    if (dr["PenaltyTypeCd"] != DBNull.Value)
                        advDTO.PenaltyTypeCd = Convert.ToInt64(dr["PenaltyTypeCd"]);

                    if (dr["DataValue"] != DBNull.Value)
                        advDTO.DataValue = Convert.ToString(dr["DataValue"]);

                    if (dr["SuspensionDays"] != DBNull.Value)
                        advDTO.SuspensionDays = Convert.ToInt16(dr["SuspensionDays"]);

                    if (dr["FineDays"] != DBNull.Value)
                        advDTO.FineDays = Convert.ToInt16(dr["FineDays"]);

                    if (dr["ApplyToTotal"] != DBNull.Value)
                        advDTO.ApplyToTotal = Convert.ToBoolean(dr["ApplyToTotal"]);

                    if (dr["Term"] != DBNull.Value)
                        advDTO.Term = Convert.ToInt16(dr["Term"]);

                    if (dr["Term_Desc"] != DBNull.Value)
                        advDTO.Term_Desc = Convert.ToString(dr["Term_Desc"]);
                    

                         if (dr["Value_Data_Type"] != DBNull.Value)
                             advDTO.Value_Data_Type = Convert.ToString(dr["Value_Data_Type"]);
                    
                    if (dr["Comments"] != DBNull.Value)
                        advDTO.Comments = Convert.ToString(dr["Comments"]);

                    objAdvPenalty.Add(advDTO);
                }
                return objAdvPenalty;
            
        }
        /// <summary>
        /// This method returns the list of advocate associate complaints DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<AdvocateAssociatedComplaintDTO> AdvocateAssociatedComplaint(DataSet ds)
        {            
                List<AdvocateAssociatedComplaintDTO> objAdvAssociatedComplaint = new List<AdvocateAssociatedComplaintDTO>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AdvocateAssociatedComplaintDTO advDTO = new AdvocateAssociatedComplaintDTO();

                    if (dr["AssociatedID"] != DBNull.Value)
                        advDTO.AssociatedID = Convert.ToInt64(dr["AssociatedID"]);

                    if (dr["AdvocacyCaseID"] != DBNull.Value)
                        advDTO.AdvocacyCaseID = Convert.ToInt64(dr["AdvocacyCaseID"]);

                    if (dr["AsscociatedComplaintID"] != DBNull.Value)
                        advDTO.AsscociatedCompaintID = Convert.ToInt64(dr["AsscociatedComplaintID"]);

                    objAdvAssociatedComplaint.Add(advDTO);
                }
                return objAdvAssociatedComplaint;
           
        }
        /// <summary>
        /// This method returns the list of advocate associate complaints DTO from the dataset.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns></returns>
        public List<AdvocateHistoryDto> AdvocateAssociatedHistory(DataSet ds)
        {
            
                List<AdvocateHistoryDto> objAdvAssociatedHistory = new List<AdvocateHistoryDto>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    AdvocateHistoryDto advDTO = new AdvocateHistoryDto();

                    if (dr["ComplaintId"] != DBNull.Value)
                        advDTO.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);

                    if (dr["IndexNo"] != DBNull.Value)
                        advDTO.ndexNo = Convert.ToInt64(dr["IndexNo"]);
                    if (dr["CreatedDate"] != DBNull.Value)
                        advDTO.VoilationDate = Convert.ToDateTime(dr["CreatedDate"]);
                    if (dr["Charges"] != DBNull.Value)
                        advDTO.Charges = Convert.ToString(dr["Charges"]);
                    if (dr["ChargesDesc"] != DBNull.Value)
                        advDTO.ChargesDesc = Convert.ToString(dr["ChargesDesc"]);
                    if (dr["Penalty"] != DBNull.Value)
                        advDTO.Penalty = Convert.ToString(dr["Penalty"]);

                    if (dr["ParentIndexNo"] != DBNull.Value)
                        advDTO.ParentIndexNo = Convert.ToInt64(dr["ParentIndexNo"]);
                    if (dr["JT1"] != DBNull.Value)
                        advDTO.JT1 = Convert.ToString(dr["JT1"]);
                    objAdvAssociatedHistory.Add(advDTO);
                }
                return objAdvAssociatedHistory;
            
        }
        public List<CombinedPenaltyDTO> CombinedPenalty(DataSet ds)
        {
             List<CombinedPenaltyDTO> objCombPenalty = new List<CombinedPenaltyDTO>();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CombinedPenaltyDTO combPenDTO = new CombinedPenaltyDTO();

                    if (dr["CombPenID"] != DBNull.Value)
                        combPenDTO.CombPenID = Convert.ToInt16(dr["CombPenID"]);

                    if (dr["ParentCombPenID"] != DBNull.Value)
                        combPenDTO.ParentCombPenID = Convert.ToInt64(dr["ParentCombPenID"]);

                    if (dr["ChildCombPenID"] != DBNull.Value)
                        combPenDTO.ChildCombPenID = Convert.ToInt16(dr["ChildCombPenID"]);

                    objCombPenalty.Add(combPenDTO);
                }
                return objCombPenalty;          

        }
    }
}
