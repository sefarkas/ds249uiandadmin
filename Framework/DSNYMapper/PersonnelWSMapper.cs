﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
 using DSNY.DSNYCP.DTO;
using System.Data;
namespace DSNY.DSNYCP.Mapper
{
   public class PersonnelWSMapper
    {
       public List<PersonnelWSDTO> WitnessListMapper(DataSet dsPersonnelWS)
        {            
                List<PersonnelWSDTO> pDTO = new List<PersonnelWSDTO>();
                if (dsPersonnelWS.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsPersonnelWS.Tables[0].Rows)
                    {
                        PersonnelWSDTO lDTO = new PersonnelWSDTO();
                        if (dr["FirstName"] != DBNull.Value)
                            lDTO.FirstName = Convert.ToString(dr["FirstName"]);
                        if (dr["MiddleName"] != DBNull.Value)
                            lDTO.MiddleName = Convert.ToString(dr["MiddleName"]);
                        if (dr["LastName"] != DBNull.Value)
                            lDTO.LastName = Convert.ToString(dr["LastName"]);
                        if (dr["ReferenceNo"] != DBNull.Value)
                            lDTO.ReferenceNo = Convert.ToString(dr["ReferenceNo"]);
                        if (dr["SSN"] != DBNull.Value)
                            lDTO.SsnNo = Convert.ToString(dr["SSN"]);
                        
                        pDTO.Add(lDTO);

                    }
                }
                return pDTO;            
        }
    }
}
