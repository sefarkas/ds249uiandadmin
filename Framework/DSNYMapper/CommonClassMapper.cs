﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.Mapper
{
    /// <summary>
    /// This class provides the functionality for the set of methods used for complaints.
    /// </summary>
 public  class CommonClassMapper
    {
     /// <summary>
        /// This method returns the list of complaint DTO from the dataset.
     /// </summary>
     /// <param name="dscomplaint"></param>
     /// <returns></returns>
        public List<ComplaintDTO> ComplaintMapper(DataSet dscomplaint)
        {

            List<ComplaintDTO> cdto = new List<ComplaintDTO>();
            foreach (DataRow dr in dscomplaint.Tables[0].Rows)
            {
                ComplaintDTO cList = new ComplaintDTO();
                if (dr["ComplaintId"] != DBNull.Value)
                    cList.ComplaintID = Convert.ToInt64(dr["ComplaintId"]); 

                    if (dr["IndexNo"] != DBNull.Value)
                        cList.IndexNo = Convert.ToInt64(dr["IndexNo"]);

                if (dr["LocationId"] != DBNull.Value)
                    cList.LocationID = Convert.ToInt64(dr["LocationId"]);
                if (dr["BoroughID"] != DBNull.Value)
                    cList.BoroughID = Convert.ToInt64(dr["BoroughID"]);
                if (dr["PromotionDate"] != DBNull.Value)
                    cList.PromotionDate = Convert.ToDateTime(dr["PromotionDate"]);
                if (dr["LocationName"] != DBNull.Value)
                    cList.LocationName = Convert.ToString(dr["LocationName"]);
                if (dr["RespondentEmpID"] != DBNull.Value)
                    cList.EmployeeID = Convert.ToInt64(dr["RespondentEmpID"]);
                if (dr["RespondentRefNumber"] != DBNull.Value)
                    cList.ReferenceNumber = Convert.ToString(dr["RespondentRefNumber"]);
                if (dr["TitleId"] != DBNull.Value)
                    cList.TitleID = Convert.ToInt64(dr["TitleId"]);
                if (dr["BadgeNumber"] != DBNull.Value)
                    cList.BadgeNumber = Convert.ToString(dr["BadgeNumber"]);
                if (dr["ComplaintActionID"] != DBNull.Value)
                    cList.ComplaintActionID = Convert.ToInt16(dr["ComplaintActionID"]);
                if (dr["IsSubmitted"] != DBNull.Value)
                    cList.IsSubmitted = Convert.ToBoolean(dr["IsSubmitted"]);
                if (dr["ChargesServedDate"] != DBNull.Value)
                    cList.ChargesServedDate = Convert.ToDateTime(dr["ChargesServedDate"]);
                if (dr["SuspensionOrderedByEmpId"] != DBNull.Value)
                    cList.SuspensionOrderedById = Convert.ToInt64(dr["SuspensionOrderedByEmpId"]);
                if (dr["SuspensionOrderedName"] != DBNull.Value)
                    cList.SuspensionOrderedByName = Convert.ToString(dr["SuspensionOrderedName"]);
                if (dr["SuspensionOrderedRefNo"] != DBNull.Value)
                    cList.SuspensionOrderedByRefNo = Convert.ToString(dr["SuspensionOrderedRefNo"]);
                if (dr["SuspensionOrderedDt"] != DBNull.Value)
                    cList.SuspensionOrderDate = Convert.ToDateTime(dr["SuspensionOrderedDt"]);
                if (dr["ChargesServedByEmpId"] != DBNull.Value)
                    cList.ChargesServedBy = Convert.ToInt64(dr["ChargesServedByEmpId"]);
                if (dr["ChargesServedByName"] != DBNull.Value)
                    cList.ChargesServedByName = Convert.ToString(dr["ChargesServedByName"]);
                if (dr["ChargesServedByRefNo"] != DBNull.Value)
                    cList.ChargesServedByRefNo = Convert.ToString(dr["ChargesServedByRefNo"]);

                if (dr["SuspensionLiftedByEmpId"] != DBNull.Value)
                    cList.SuspensionLiftedBy = Convert.ToInt64(dr["SuspensionLiftedByEmpId"]);
                if (dr["SuspensionLiftedByName"] != DBNull.Value)
                    cList.SuspensionLiftedByName = Convert.ToString(dr["SuspensionLiftedByName"]);
                if (dr["SuspensionLiftedByRefNo"] != DBNull.Value)
                    cList.SuspensionLiftedByRefNo = Convert.ToString(dr["SuspensionLiftedByRefNo"]);

                if (dr["SuspensionLiftedDt"] != DBNull.Value)
                    cList.SuspensionLifedDate = Convert.ToDateTime(dr["SuspensionLiftedDt"]);
                if (dr["RoutingLocation"] != DBNull.Value)
                    cList.RoutingLocation = Convert.ToString(dr["RoutingLocation"]);
                if (dr["IsProbation"] != DBNull.Value)
                    cList.IsProbation = Convert.ToBoolean(dr["IsProbation"]);
                if (dr["Approved"] != DBNull.Value)
                    cList.IsApproved = Convert.ToBoolean(dr["Approved"]);
                if (dr["ApprovedDate"] != DBNull.Value)
                    cList.ApprovedDate = Convert.ToDateTime(dr["ApprovedDate"]);
                if (dr["CreatedBy"] != DBNull.Value)
                    cList.CreatedBy = Convert.ToString(dr["CreatedBy"]);
                if (dr["CreatedDate"] != DBNull.Value)
                    cList.CreatedDate = Convert.ToDateTime(dr["CreatedDate"]);
                if (dr["Comments"] != DBNull.Value)
                    cList.Comments = Convert.ToString(dr["Comments"]);
                if (dr["SentDate"] != DBNull.Value)
                    cList.SentDate = Convert.ToDateTime(dr["SentDate"]);
                if (dr["complaintStatus"] != DBNull.Value)
                    cList.ComplaintStatus = Convert.ToInt16(dr["complaintStatus"]);
                if (dr["complaintStatusDate"] != DBNull.Value)
                    cList.ComplaintStatusDate = Convert.ToDateTime(dr["complaintStatusDate"]);
                if (dr["StreetNumber"] != DBNull.Value)
                    cList.StreetNumber = Convert.ToString(dr["StreetNumber"]);
                if (dr["StreetName"] != DBNull.Value)
                    cList.StreetName = Convert.ToString(dr["StreetName"]);
                if (dr["AptNo"] != DBNull.Value)
                    cList.AptNo = Convert.ToString(dr["AptNo"]);
                if (dr["City"] != DBNull.Value)
                    cList.City = Convert.ToString(dr["City"]);
                if (dr["State"] != DBNull.Value)
                    cList.State = Convert.ToString(dr["State"]);
                if (dr["Zip"] != DBNull.Value)
                    cList.ZipCode = Convert.ToString(dr["Zip"]);
                if (dr["zipSuffix"] != DBNull.Value)
                    cList.ZipSuffix = Convert.ToString(dr["zipSuffix"]);
                if (dr["PayCodeId"] != DBNull.Value)
                    cList.PayCodeID = Convert.ToInt16(dr["PayCodeId"]);
                if (dr["PayDesc"] != DBNull.Value)
                    cList.PayCodeDesc = Convert.ToString(dr["PayDesc"]);
                if (dr["ChartNumber"] != DBNull.Value)
                    cList.ChartNo = Convert.ToString(dr["ChartNumber"]);
                if (dr["VacationSchedule"] != DBNull.Value)
                    cList.VacationSchedule = Convert.ToString(dr["VacationSchedule"]);
                if (dr["RespondentFName"] != DBNull.Value)
                    cList.RespondentFName = dr["RespondentFName"].ToString();
                if (dr["RespondentLName"] != DBNull.Value)
                    cList.RespondentLName = dr["RespondentLName"].ToString();
                if (dr["RespondentDOB"] != DBNull.Value)
                    cList.RespondentDOB = Convert.ToDateTime(dr["RespondentDOB"]);
                if (dr["RespondentApptDt"] != DBNull.Value)
                    cList.RespondentAptDt = Convert.ToDateTime(dr["RespondentApptDt"]);
                if (dr["Title"] != DBNull.Value)
                    cList.TitleDesc = Convert.ToString(dr["Title"]);
                if (dr["IsProbation"] != DBNull.Value)
                    cList.IsProbation = Convert.ToBoolean(dr["IsProbation"]);
                cdto.Add(cList);
            }

            return cdto;
        }
     /// <summary>
     /// This method returns the list of witness DTO from the dataset.
     /// </summary>
     /// <param name="dsWitnessList"></param>
     /// <returns></returns>
        public List<WitnessDto> WitnessListMapper(DataSet dsWitnessList)
        {           
                List<WitnessDto> wDTO = new List<WitnessDto>();
                if (dsWitnessList.Tables.Count > 0)
                    {
                    foreach (DataRow dr in dsWitnessList.Tables[0].Rows)
                    {
                        WitnessDto lDTO = new WitnessDto();
                        if (dr["ComplaintId"] != DBNull.Value)
                            lDTO.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                        if (dr["FullName"] != DBNull.Value)
                            lDTO.PersonnelName = Convert.ToString(dr["FullName"]);
                        if (dr["RefNo"] != DBNull.Value)
                            lDTO.RefNo = Convert.ToString(dr["RefNo"]);
                        wDTO.Add(lDTO);
                       
                    }
                }
                return wDTO;           
        }
     /// <summary>
        /// This method returns the list of complaints DTO from the dataset.
     /// </summary>
     /// <param name="dsComplainants"></param>
     /// <returns></returns>
        public List<ComplainantDTO> ComplainantListMapper(DataSet dsComplainants)
        {
          
                List<ComplainantDTO> cDTO = new List<ComplainantDTO>();                
                if (dsComplainants.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsComplainants.Tables[0].Rows)
                    {
                        ComplainantDTO complainantList = new ComplainantDTO();
                        
                        complainantList.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                        if (dr["FullName"] != DBNull.Value)
                            complainantList.ComplainantName = Convert.ToString(dr["FullName"]);
                        if (dr["RefNo"] != DBNull.Value)
                            complainantList.RefNo = Convert.ToString(dr["RefNo"]);
                        if (dr["TitleID"] != DBNull.Value)
                            complainantList.TitleID = Convert.ToInt64(dr["TitleID"]);
                        if (dr["LocationID"] != DBNull.Value)
                            complainantList.LocationID = Convert.ToInt64(dr["LocationID"]);
                        if (dr["Statement"] != DBNull.Value)
                            complainantList.Statement = Convert.ToString(dr["Statement"]);
                        if (dr["OnDate"] != DBNull.Value)
                            complainantList.OnDate = Convert.ToDateTime(dr["OnDate"]);
                        if (dr["Text"] != DBNull.Value)
                            complainantList.Text = Convert.ToString(dr["Text"]);
                        if (dr["ThisDate"] != DBNull.Value)
                            complainantList.ThisDate = Convert.ToDateTime(dr["ThisDate"]);
                        if (dr["Category"] != DBNull.Value)
                            complainantList.Category = Convert.ToString(dr["Category"]);
                        if (dr["ReportedDate"] != DBNull.Value)
                            complainantList.ReportedDate = Convert.ToDateTime(dr["ReportedDate"]);
                        if (dr["FromLoc"] != DBNull.Value)
                            complainantList.FromLocation = Convert.ToString(dr["FromLoc"]);
                        if (dr["ReportedTime"] != DBNull.Value)
                            complainantList.ReportedTime = Convert.ToString(dr["ReportedTime"]);
                        if (dr["ReportingTime"] != DBNull.Value)
                            complainantList.ReportingTime = Convert.ToString(dr["ReportingTime"]);
                        if (dr["TelNo"] != DBNull.Value)
                            complainantList.TelephoneNo = Convert.ToString(dr["TelNo"]);
                        if (dr["LocationName"] != DBNull.Value)
                            complainantList.LocationName = Convert.ToString(dr["LocationName"]);
                        if (dr["Title"] != DBNull.Value)
                            complainantList.Title = Convert.ToString(dr["Title"]);
                        cDTO.Add(complainantList);
                        
                    }
                }
                return cDTO;            
        }
       /// <summary>
        /// This method returns the list of complaint history DTO from the dataset.
       /// </summary>
       /// <param name="dshistoryList"></param>
       /// <returns></returns>
        public List<ComplaintHistoryDTO> ComplaintHistoryMapper(DataSet dshistoryList)
        {            
           
                List<ComplaintHistoryDTO> hDTO = new List<ComplaintHistoryDTO>();
                if (dshistoryList.Tables.Count > 0)
                {
                    foreach (DataRow dr in dshistoryList.Tables[0].Rows)
                    {
                        ComplaintHistoryDTO historyList = new ComplaintHistoryDTO();
                        historyList.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);                    
                        if (dr["ComplaintId"] != DBNull.Value)
                            historyList.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                        if(dr["IndexNo"]!=DBNull.Value)
                            historyList.IndexNo = Convert.ToInt64(dr["IndexNo"]);
                        if (dr["CreatedDate"] != DBNull.Value)
                            historyList.VoilationDate = Convert.ToDateTime(dr["CreatedDate"]);
                        if (dr["Charges"] != DBNull.Value)
                            historyList.Voilations = Convert.ToString(dr["Charges"]);
                        if (dr["ChargesDesc"] != DBNull.Value)
                            historyList.VoilationDesc = Convert.ToString(dr["ChargesDesc"]);
                        if (dr["Penalty"] != DBNull.Value)
                            historyList.Penalty = Convert.ToString(dr["Penalty"]);
                        if (dr["RoutingLocation"] != DBNull.Value)
                            historyList.RoutingLocation = Convert.ToString(dr["RoutingLocation"]);
                                             
                        hDTO.Add(historyList);
                    }
                }
                return hDTO;           
        }
       /// <summary>
        /// This method returns the list of violation DTO from the dataset.
       /// </summary>
       /// <param name="dsViolations"></param>
       /// <returns></returns>

        public List<ViolationDTO> VoilationListMapper(DataSet dsViolations)
        {
          
                List<ViolationDTO> vDTO = new List<ViolationDTO>();             

                if (dsViolations.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsViolations.Tables[0].Rows)
                    {
                        ViolationDTO vList = new ViolationDTO();
                        vList.Charge = new ChargeDTO();
                        if (dr["ChargeID"] != DBNull.Value)
                            vList.ChargeID = Convert.ToInt64(dr["ChargeID"]);
                        if (dr["ComplaintId"] != DBNull.Value)
                            vList.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                        if (dr["IncidentLocationID"] != DBNull.Value)
                            vList.IncidentLocationID = Convert.ToInt64(dr["IncidentLocationID"]);
                        if (dr["IncidentType"] != DBNull.Value)
                            vList.IncidentType = Convert.ToString(dr["IncidentType"]);
                        if (dr["ReferenceNumber"] != DBNull.Value)                            
                            vList.Charge.ChargeCode = Convert.ToString(dr["ReferenceNumber"]);
                        if (dr["ChargeDesc"] != DBNull.Value)
                        {
                            vList.Charge.ChargeDescription = Convert.ToString(dr["ChargeDesc"]);
                            vList.VoilationDesc = vList.Charge.ChargeCode + " " + Convert.ToString(dr["ChargeDesc"]);
                        }

                        if (dr["OverRideChargeTypeId"] != DBNull.Value)
                        {
                            vList.OverRideVoilationDesc = Convert.ToString(dr["OverRideRefNo"]) + " " + Convert.ToString(dr["OverRideDesc"]);

                        }   
                        if (dr["LocationName"] != DBNull.Value)
                            vList.IncidentLocationName = Convert.ToString(dr["LocationName"]);
                        if (dr["PrimaryInd"] != DBNull.Value)
                            vList.PrimaryIndicator = Convert.ToBoolean(dr["PrimaryInd"]);


                        if (dr["IncidentDate"] != DBNull.Value)
                            vList.IncidentDate =Convert.ToDateTime(dr["IncidentDate"]);                     

                        vDTO.Add(vList);                       
                    }
                }
                return vDTO;
            
        }
        /// <summary>
        /// This method returns the list of incident DTO from the dataset.
        /// </summary>
        /// <param name="dsIncedents"></param>
        /// <returns></returns>
        public List<IncedentDTO> IncidentListMapper(DataSet dsIncedents)
        {            
             List<IncedentDTO> iDTO = new List<IncedentDTO>();    
              if (dsIncedents.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsIncedents.Tables[0].Rows)
                    {
                        IncedentDTO iList = new IncedentDTO();
                        if (dr["ComplaintId"] != DBNull.Value)
                            iList.ComplaintID = Convert.ToInt64(dr["ComplaintId"]);
                        if (dr["IncidentDate"] != DBNull.Value)
                            iList.IncedentDate = Convert.ToDateTime(dr["IncidentDate"]);
                        if (dr["ChargeID"] != DBNull.Value)
                            iList.ChargeID = Convert.ToInt64(dr["ChargeID"]);
                        iDTO.Add(iList);
                    
                    }
                }
                return iDTO;            
        }

             /// <summary>
        /// This method returns the list of complaints by districts DTO from the dataset.
        /// </summary>
        /// <param name="dsIncedents"></param>
        /// <returns></returns>

        public List<ComplaintByDistrictDTO> ComplaintDistrictMapper(DataSet dsIncedents)
        {            
                List<ComplaintByDistrictDTO> complaintDTO = new List<ComplaintByDistrictDTO>();               

                if (dsIncedents.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsIncedents.Tables[0].Rows)
                    {
                        ComplaintByDistrictDTO complaintList = new ComplaintByDistrictDTO();
                        if (dr["Borough"] != DBNull.Value)
                            complaintList.Borough = Convert.ToString(dr["Borough"]);
                        if (dr["District"] != DBNull.Value)
                            complaintList.District = Convert.ToString(dr["District"]);
                        if (dr["NoOfComplaints"] != DBNull.Value)
                            complaintList.NoOfComplaints = Convert.ToInt64(dr["NoOfComplaints"]);
                        complaintDTO.Add(complaintList);

                    }
                }
                return complaintDTO;          
        }
     /// <summary>
     /// This method Implement DTO for Pay Code Mapper
     /// </summary>
     /// <param name="dsIncedents"></param>
     /// <returns></returns>

        public List<PayCodeDTO> PayCodeMapper(DataSet dsIncedents)
        {           
                List<PayCodeDTO> payCodeDTO = new List<PayCodeDTO>();
                if (dsIncedents.Tables.Count > 0)
                {
                    foreach (DataRow dr in dsIncedents.Tables[0].Rows)
                    {
                        PayCodeDTO payCodeList = new PayCodeDTO();
                        if (dr["PayCodeId"] != DBNull.Value)
                            payCodeList.PayCodeId = Convert.ToInt16(dr["PayCodeId"]);
                        if (dr["PayCodedesc"] != DBNull.Value)
                            payCodeList.PayCodeDesc = Convert.ToString(dr["PayCodedesc"]);
                        payCodeDTO.Add(payCodeList);
                    }
                }
                return payCodeDTO;            
        }

    }
}
