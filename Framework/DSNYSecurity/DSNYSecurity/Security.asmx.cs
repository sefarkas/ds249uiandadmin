﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.DAL;


namespace DSNY.DSNYCP.Securities
{
    /// <summary>
    /// Summary description for Security
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Security : System.Web.Services.WebService
    {

        private DataAccess dataAccess;


        public enum Applications
        {
            None = 0,
            DS249 = 1,
            ADVOCATE = 2,
            BCAD = 3,
            SEO = 4,
            ADMIN = 5,
            MEDICAL = 6,
            EMPLOYEEAPPLICATION = 7,
            SAFETY=8,
            WEP=9,
            FLEETCONSOLIDATIONDSR=10,
        }

        public Security()
        {

            //Uncomment the following line if using designed components 
            //InitializeComponent(); 
        }

        [WebMethod]

        public List<UserDTO> AuthenticateUser(String UserID, String Password)
        {
            try
            {
                if (dataAccess == null)

                    dataAccess = new DataAccess();
                List<UserDTO> dsMembership = dataAccess.AuthenticateUser(UserID, Password);
                return dsMembership;
                //return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [WebMethod]
        public List<UserDTO> AuthenticatedUser(String UserID, String Password,  DSNY.DSNYCP.Securities.Security.Applications appname)
        {
            try
            {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<UserDTO> dsMembership = dataAccess.AuthenticatedUserForApplication(UserID, Password, appname);
                return dsMembership;
                //return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


            

       
        [WebMethod]
        public List<UserDTO> LoadUser(Int64 PersonalID)
        {
            try
            {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<UserDTO> usrMembership = dataAccess.GetUser(PersonalID);
                return usrMembership;
                

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public List<MembershipDTO> GetMembership(Int64 PersonalID)
        {

            try
            {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<MembershipDTO> dsMembership = dataAccess.GetMembership(PersonalID);
                return dsMembership;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public List<MembershipDTO> GetMembershipList()
        {

            try
            {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<MembershipDTO> dsMembership = dataAccess.GetMembership();
                return dsMembership;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [WebMethod]
        public List<RoleDTO> GetRoles(int membershipID)
        {

            try
            {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<RoleDTO> dsRoles = dataAccess.GetRoles(membershipID);
                return dsRoles;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        [WebMethod]
        public List<MembershipDTO> GetMembershipRoles(Int64 MembershipID, Int64 PersonalID)
        {
            try
            {
                if (dataAccess == null)
                    dataAccess = new DataAccess();
                List<MembershipDTO> dsMembershipRoles = dataAccess.GetMembershipRoles(MembershipID, PersonalID);
                return dsMembershipRoles;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public bool ChangePassword(string username,string password)
        {
            bool passwordReset;
            if (dataAccess == null)
                dataAccess = new DataAccess();

            passwordReset = dataAccess.ResetPassword(username, password, false);
            return passwordReset;
        }
    }
}
