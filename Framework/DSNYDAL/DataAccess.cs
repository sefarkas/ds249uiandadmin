﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Web;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.Mapper;

namespace DSNY.DSNYCP.DAL
{
    /// <summary>
    /// This class provides the functionality for the below modules with the list of methods
    /// COMPLAINTS
    /// PERSONNEL
    /// BCAD COMPLAINTS
    /// ADVOCATE COMPLAINTS
    /// SECURITY
    /// EMPLOYEE INFO
    /// ADMIN
    /// </summary>

    public class DataAccess
    {
        #region WCF

        public List<PersonnelWSDTO> GetPersonelDataWS(int pageNo, int pageSize, string firstName, string middleName, string lastName, string referenceNo, string ssnNo)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                List<PersonnelWSDTO> personnelWSDTO = new List<PersonnelWSDTO>();
                PersonnelWSMapper complaintMapper = new PersonnelWSMapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetPersonellDetailWS);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PageNo", DbType.Int16, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PageSize", DbType.Int16, pageSize);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FirstName", DbType.String, firstName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MiddleName", DbType.String, middleName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@LastName", DbType.String, lastName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ReferenceNo", DbType.String, referenceNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SSNo", DbType.String, ssnNo);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                personnelWSDTO = complaintMapper.WitnessListMapper(DSNYCPdr);
                return personnelWSDTO;            
        }
        #endregion

        #region COMPLAINTS
        /// <summary>
        /// This method gets the list of complaint with respect to complaint ID 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>      
        public List<ComplaintDTO> GetComplaint(Int64 id)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                List<ComplaintDTO> complaintDTO = new List<ComplaintDTO>();
                CommonClassMapper complaintMapper = new CommonClassMapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplaintById);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, id);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                complaintDTO = complaintMapper.ComplaintMapper(DSNYCPdr);
                return complaintDTO;            
           
        }
        /// <summary>
        /// This method searches the complaint  with respect to search criteria and returns to the DTO
        /// </summary>
        /// <param name="SearchText"></param>
        /// <returns></returns>        
        public List<ComplaintDTO> SearchComplaintId(String searchText)
        {
                List<ComplaintDTO> complaint = new List<ComplaintDTO>();
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd;
                ComplaintDS249Mapper complaintMapper = new ComplaintDS249Mapper();
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SearchComplaints);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SearchText", DbType.String, searchText);
                DataSet dsDSNYCP = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                complaint = complaintMapper.ComplaintList(dsDSNYCP);
                return complaint;
                       
        }
        /// <summary>
        /// This method gets the list of complaint history to the DTO with respect to ID and complaint ID
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>        
        public List<ComplaintHistoryDTO> GetComplaintHistory(Int64 id, Int64 complaintId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                List<ComplaintHistoryDTO> hDTO = new List<ComplaintHistoryDTO>();
                CommonClassMapper cMapper = new CommonClassMapper();            
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplaintHistory);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmpID", DbType.Int64, id);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                hDTO = cMapper.ComplaintHistoryMapper(DSNYCPdr);
                return hDTO;      
           
        }
      

        /// <summary>
        /// This method gets the list of complaint to the DTO with respect to personal ID 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>        
        public List<ComplaintDTO> GetComplaints(Int16 personalId, int pageNo, int maximumRows, ref Int64 rowsCount)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();
            ComplaintDS249Mapper complaintMapperDS249 = new ComplaintDS249Mapper();
            DataSet DSNYCPds = null;           
                if ((pageNo == -3) && (maximumRows == -3))
                {
                    DbCommand DSNYCPCmd;
                    DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplaintsForAdmin);
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@CreatedBy", DbType.Int64, personalId);
                    DSNYCPds = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                }
                else
                {
                    if (pageNo != 0)
                        pageNo -= 1;
                    DbCommand DSNYCPCmd;
                    DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplaints);
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@CreatedBy", DbType.Int64, personalId);
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);                    
                    sqlDatabase.AddOutParameter(DSNYCPCmd, "@rowsCount", DbType.Int64, 64);
                    DSNYCPCmd.CommandTimeout = 0;
                    DSNYCPds = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                    rowsCount = Convert.ToInt64(DSNYCPCmd.Parameters["@rowsCount"].Value);
                }
                complaintListDTO = complaintMapperDS249.ComplaintListDS249(DSNYCPds);
                return complaintListDTO;          
        }
        /// <summary>
        /// This method gets the list of complaint to the DTO with respect to personal ID 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>       
        public List<ComplaintDTO> GetSearchComplaints(int createdBy, int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string strValue, ref Int32 rowCount)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            if (pageNo != 0)
                pageNo -= 1;
                List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplaintsBySearch);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CreatedBy", DbType.Int64, createdBy);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@numericValue", DbType.Int64, numericValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@chargesValue", DbType.String, decimalValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@complaintDate", DbType.String, dateValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@stringValue", DbType.String, strValue);
                sqlDatabase.AddOutParameter(DSNYCPCmd, "@RowCount", DbType.Int64, 64);
                DSNYCPCmd.CommandTimeout = 0; 
                DataSet DSNYCPds = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                rowCount = Convert.ToInt32(DSNYCPCmd.Parameters["@RowCount"].Value);
                ComplaintDS249Mapper complaintMapperDS249 = new ComplaintDS249Mapper();
                complaintListDTO = complaintMapperDS249.ComplaintListDS249(DSNYCPds);
                return complaintListDTO;            
        }
        /// <summary>
        /// This method gets the count of the list of complaint 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>        
        public int GetComplaintsListCount(Int16 personalId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplaintsCount);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CreatedBy", DbType.Int16, personalId);
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);           
        }

        public int SaveErrorLog(string Url, string RequestType, string UserID, string ErrorMessage, string StackTrace, int? AppID)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                List<AppErrorDTO> AppErrorLogListDTO = new List<AppErrorDTO>();           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveError);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Url", DbType.String, Url);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RequestType", DbType.String, RequestType);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@UserID", DbType.String, UserID);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ErrorMessage", DbType.String, ErrorMessage);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@StackTrace", DbType.String, StackTrace);
                object o = sqlDatabase.ExecuteScalar(DSNYCPCmd);
                return Convert.ToInt32(o);            
        }
        /// <summary>
        /// This method gets the count of the list of complaint 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public int GetSearchComplaintsListCount(Int16 personalId, int numericValue, string decimalValue, string dateValue, string strValue)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetSearchComplaintsCount);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CreatedBy", DbType.Int64, personalId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@numericValue", DbType.Int64, numericValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@chargesValue", DbType.String, decimalValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@complaintDate", DbType.String, dateValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@stringValue", DbType.String, strValue);
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);            
          
        }
        /// <summary>
        /// This method Saves the list of complaint with respect to complaint ID to the DTO
        /// </summary>
        /// <param name="complaint"></param>
        /// <returns></returns>        
        public bool SaveComplaint(ComplaintDTO complaint, Int64 personID)
        {
            Database db = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                  Int64 id = complaint.ComplaintID;
                    Int64 indexNo = complaint.IndexNo;
                    SaveComplaint(ref id, ref indexNo, complaint.LocationID, complaint.BoroughID, complaint.PromotionDate,
                                            complaint.EmployeeID, complaint.ReferenceNumber,
                                            complaint.TitleID, complaint.BadgeNumber,
                                            complaint.ChargesServedByRefNo, complaint.ChargesServedDate,
                                            complaint.SuspensionOrderedByRefNo, complaint.SuspensionOrderDate,
                                            complaint.SuspensionLiftedByRefNo, complaint.SuspensionLifedDate,
                                            complaint.RoutingLocation, complaint.IsProbation,
                                            complaint.IsApproved, complaint.ApprovedDate,                                         
                                            complaint.Comments, personID.ToString(),
                                            complaint.StreetNumber, complaint.StreetName,
                                            complaint.AptNo, complaint.City, complaint.State,
                                            complaint.ZipCode, complaint.ZipSuffix, complaint.PayCodeID,
                                            complaint.VacationSchedule, complaint.ChartNo,
                                            complaint.RespondentFName, complaint.RespondentMName,
                                            complaint.RespondentLName, complaint.RespondentDOB,
                                            complaint.RespondentAptDt, db, transaction);
                    if (complaint.Witnesses.Count > 0)
                    {
                        foreach (WitnessDto witness in complaint.Witnesses)
                        {
                            SaveWitness(id, witness.RefNo, witness.PersonnelName, witness.UniqueID, db, transaction);
                        }
                    }
                    if (complaint.Complainants.Count > 0)
                    {
                        foreach (ComplainantDTO complainant in complaint.Complainants)
                        {
                            SaveComplainant(id, complainant.RefNo, complainant.ComplainantName, complainant.UniqueID,
                                complainant.LocationID, complainant.TitleID, complainant.Statement, complainant.OnDate,
                                complainant.Text, complainant.ThisDate, complainant.Category, complainant.ReportedDate,
                                complainant.FromLocation, complainant.ReportedTime, complainant.ReportingTime, complainant.TelephoneNo,
                                db, transaction);
                        }
                    }
                    if (complaint.Voilations.Count > 0)
                    {
                        ChargeDTO charge;
                        foreach (ViolationDTO violation in complaint.Voilations)
                        {
                            Int64 violationID = -1;
                            charge = new ChargeDTO();
                            charge = violation.Charge;
                            SaveViolation(id, charge.ChargeCode, violation.UniqueID,
                                                violation.IncidentLocationID, violation.IncidentType,
                                                ref violationID, db, transaction);
                            violation.ChargeID = violationID;

                            if (violation.Incidents != null)
                            {
                                if (violation.Incidents.Count > 0)
                                {
                                    int i = 0;

                                    foreach (IncedentDTO incident in violation.Incidents)
                                    {
                                        SaveIncident(id, i, incident.IncedentDate, violationID, db, transaction);
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    transaction.Commit();
                    complaint.ComplaintID = id;
                    complaint.IndexNo = indexNo;                
                return true;
            }
        }
        /// <summary>
        /// This function returns true if the list of complaints are saved succesfully to the list.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="IndexNo"></param>
        /// <param name="LocationID"></param>
        /// <param name="RespondentEmpID"></param>
        /// <param name="RespondentRefNo"></param>
        /// <param name="TitleID"></param>
        /// <param name="BadgeNo"></param>
        /// <param name="ChargesServeredByRefNo"></param>
        /// <param name="ChargesServeredDt"></param>
        /// <param name="SuspensionOrderRefNo"></param>
        /// <param name="SuspensionOrderDt"></param>
        /// <param name="SuspensionLiftedByRefNo"></param>
        /// <param name="SuspensionLiftedDt"></param>
        /// <param name="RoutingLocation"></param>
        /// <param name="IsProbation"></param>
        /// <param name="IsApproved"></param>
        /// <param name="ApprovedDt"></param>
        /// <param name="Comments"></param>
        /// <param name="CreatedBy"></param>
        /// <param name="StreetNumber"></param>
        /// <param name="StreetName"></param>
        /// <param name="AptNo"></param>
        /// <param name="City"></param>
        /// <param name="State"></param>
        /// <param name="Zip"></param>
        /// <param name="ZipPrefix"></param>
        /// <param name="PayCodeID"></param>
        /// <param name="VacationSchedule"></param>
        /// <param name="ChartNo"></param>
        /// <param name="RespondentFName"></param>
        /// <param name="RespondentMName"></param>
        /// <param name="RespondentLName"></param>
        /// <param name="RespondentDOB"></param>
        /// <param name="RespondentAptDt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>       
        public bool SaveComplaint(ref Int64 complaintId, ref Int64 indexNo, Int64 locationId, Int64 boroughId, DateTime? promotionDate
                                    , Int64 respondentEmployeeId
                                    , String respondentReferenceNo, Int64 titleId, String badgeNo
                                    , String chargesServedByReferenceNo, DateTime chargesServedDate
                                    , String suspensionOrderReferenceNo, DateTime suspensionOrderDate
                                    , String suspensionLiftedByReferenceNo, DateTime suspensionLiftedDate
                                    , String routingLocation, Boolean isProbation
                                    , Boolean isApproved, DateTime approvedDate, String comments
                                    , String createdBy, String streetNumber, String streetName
                                    , String apartmentNo, String city, String state, String zip, String zipPrefix
                                    , Int16 payCodeId, String vacationSchedule, String chartNo
                                    , String respondentFirstName, String respondentMiddleName
                                    , String respondentLastName, DateTime respondentDateOfBirth
                                    , DateTime respondentApartmentDate,
                                    Database sqlDatabase, DbTransaction transaction)
        {
           
                Boolean isNew = false;
                if (complaintId == -1)
                    isNew = true;
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@LocationID", DbType.Int64, locationId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@BoroughID", DbType.Int64, boroughId);
                if (promotionDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@PromotionDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@PromotionDate", DbType.DateTime, promotionDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmployeeID", DbType.Int64, respondentEmployeeId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RefNo", DbType.String, respondentReferenceNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@TitleID", DbType.Int64, titleId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@BadgeNo", DbType.String, badgeNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ChargesServeredByRefNo", DbType.String, chargesServedByReferenceNo);

                if (chargesServedDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChargesServeredDt", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChargesServeredDt", DbType.DateTime, chargesServedDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SuspensionOrderRefNo", DbType.String, suspensionOrderReferenceNo);
                if (suspensionOrderDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@SuspensionOrderDt", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@SuspensionOrderDt", DbType.DateTime, suspensionOrderDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SuspensionLiftedByRefNo", DbType.String, suspensionLiftedByReferenceNo);
                if (suspensionLiftedDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@SuspensionLiftedDt", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@SuspensionLiftedDt", DbType.DateTime, suspensionLiftedDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RoutingLocation", DbType.String, routingLocation);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@IsProbation", DbType.Boolean, isProbation);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@IsApproved", DbType.Boolean, isApproved);
                if (isApproved == true)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ApprovedDt", DbType.DateTime, DateTime.Today);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ApprovedDt", DbType.DateTime, DBNull.Value);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CreatedBy", DbType.String, createdBy);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Comments", DbType.String, comments);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@StreetNumber", DbType.String, streetNumber);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@StreetName", DbType.String, streetName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AptNo", DbType.String, apartmentNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@City", DbType.String, city);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@State", DbType.String, state);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Zip", DbType.String, zip);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ZipSuffix", DbType.String, zipPrefix);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@VacationSchedule", DbType.String, vacationSchedule);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ChartNumber", DbType.String, chartNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PayCodeID", DbType.Int16, payCodeId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RespondentFName", DbType.String, respondentFirstName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RespondentMName", DbType.String, respondentMiddleName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RespondentLName", DbType.String, respondentLastName);
                if (respondentDateOfBirth == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@RespondentDOB", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@RespondentDOB", DbType.DateTime, respondentDateOfBirth);
                if (respondentApartmentDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@RespondentAptDt", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@RespondentAptDt", DbType.DateTime, respondentApartmentDate);
                sqlDatabase.AddOutParameter(DSNYCPCmd, "@IndexNo", DbType.Int64, 20);
                complaintId = Convert.ToInt64(sqlDatabase.ExecuteScalar(DSNYCPCmd, transaction));
                if (isNew)
                    indexNo = Convert.ToInt64(DSNYCPCmd.Parameters["@IndexNo"].Value);
                return true;            
        }
            
       


        /// <summary>
        /// This function returns true if the list of aproved complaints are saved succesfully with respect complaintId,status and modified user.
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="status"></param>
        /// <param name="modifiedBy"></param>
        /// <returns></returns>        
        public bool ReopenORCloseApproveComplaint(Int64 complaintId, Int16 status, String modifiedBy)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ReopenOrCloseApprovedComplaint);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@StatusCd", DbType.Int32, status);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@ModifiedBy", DbType.Int64, modifiedBy);
            sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
            return true;

        }

        

        /// <summary>
        /// This function returns true if the list of deleted complaints are undeleted saved succesfully  with respect complaintID and status.
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="status"></param>
        /// <returns></returns>       
        public bool DeleteUndeleteComplaint(Int64 complaintId, Int16 status)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.DeleteUndeleteComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Status", DbType.Int32, status);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }

        public bool ComplaintHistoryDetails(Int64 complaintID, String status, Int64 UserId, DateTime date)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ComplaintHistoryDetails);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintID);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Status", DbType.String, status);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@UserId", DbType.Int64, UserId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@date", DbType.DateTime, date);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }

        public bool AdminPersonDetails(Int64 UserId, String Status, Int64 personId)
        {

            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.AdminPersonLog);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@UserId", DbType.Int64, UserId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Changesmade", DbType.String, Status);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonId", DbType.Int64, personId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@date", DbType.DateTime, System.DateTime.Now);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;         
        }

        /// <summary>
        /// This method gets the list of Witnesses to the DTO with respect to complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>        
        public List<WitnessDto> LoadWitnesses(Int64 complaintId)
        {

            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                List<WitnessDto> lDTO = new List<WitnessDto>();
                CommonClassMapper clsMapper = new CommonClassMapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetWitnesses);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                lDTO = clsMapper.WitnessListMapper(DSNYCPdr);
                return lDTO;      
          
        }
        /// <summary>
        /// This method returns true if the list of Witnesses are saves sucessfully 
        /// with respect to complaint id, reffernce number and full name.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="RefNo"></param>
        /// <param name="FullName"></param>
        /// <param name="Cnt"></param>
        /// <returns></returns>        
        public bool SaveWitness(Int64 complaintId, String refNo, String fullName, int count)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveWitness);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FullName", DbType.String, fullName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RefNo", DbType.String, refNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;          
        }
        /// <summary>
        /// This method returns true if the list of Witnesses are saves sucessfully 
        /// with respect to complaint id, reffernce number and full name.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="RefNo"></param>
        /// <param name="FullName"></param>
        /// <param name="Cnt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>        
        public bool SaveWitness(Int64 complaintId,String referenceNo,String fullName,int count, Database sqlDatabase, DbTransaction transaction)
        {           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveWitness);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FullName", DbType.String, fullName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RefNo", DbType.String, referenceNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);
                return true;          
        }
        /// <summary>
        /// This method loads the list of complaint to the DTO with respect to complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>        
        public List<ComplainantDTO> LoadComplainants(Int64 complaintId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplainantDTO> cDTO = new List<ComplainantDTO>();
                CommonClassMapper cMapper = new CommonClassMapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplainants);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                cDTO = cMapper.ComplainantListMapper(DSNYCPdr);
                return cDTO;  
           
        }
        /// <summary>
        /// This method returns true if the list of complaints are saved sucessfully 
        /// with respect to below parameters
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="RefNo"></param>
        /// <param name="FullName"></param>
        /// <param name="Cnt"></param>
        /// <param name="LocID"></param>
        /// <param name="TitleID"></param>
        /// <param name="Statement"></param>
        /// <param name="onDate"></param>
        /// <param name="Text"></param>
        /// <param name="ThisDate"></param>
        /// <param name="Category"></param>
        /// <param name="ReportedDate"></param>
        /// <param name="FromLoc"></param>
        /// <param name="ReportedTime"></param>
        /// <param name="ReportingTime"></param>
        /// <param name="TelNo"></param>
        /// <returns></returns>        
        public bool SaveComplainant(Int64 complaintId,
                                    String referenceNo,
                                    String fullName,
                                    int count,
                                    Int64 locationId,
                                    Int64 titleId,
                                    String statement,
                                    DateTime onDate,
                                    String text,
                                    DateTime thisDate,
                                    String category,
                                    DateTime reportedDate,
                                    String fromLocation,
                                    String reportedTime,
                                    String reportingTime,
                                    String telephoneNo)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveComplainant);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FullName", DbType.String, fullName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RefNo", DbType.String, referenceNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@LocID", DbType.Int64, locationId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@TitleID", DbType.Int64, titleId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Statment", DbType.String, statement);
                if (onDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@onDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@onDate", DbType.DateTime, onDate);
                if (text == string.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@Text", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@Text", DbType.String, text);
                if (thisDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ThisDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ThisDate", DbType.DateTime, thisDate);
                if (category == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@Category", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@Category", DbType.String, category);

                if (reportedDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportedDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportedDate", DbType.DateTime, reportedDate);

                if (fromLocation == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@FromLoc", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@FromLoc", DbType.String, fromLocation);

                if (reportedTime == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportedTime", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportedTime", DbType.String, reportedTime);

                if (reportingTime == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportingTime", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportingTime", DbType.String, reportingTime);

                if (telephoneNo == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@TelNo", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@TelNo", DbType.String, telephoneNo);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the list of complaints are saved sucessfully 
        /// with respect to below parameters
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="RefNo"></param>
        /// <param name="FullName"></param>
        /// <param name="Cnt"></param>
        /// <param name="LocID"></param>
        /// <param name="TitleID"></param>
        /// <param name="Statement"></param>
        /// <param name="onDate"></param>
        /// <param name="Text"></param>
        /// <param name="ThisDate"></param>
        /// <param name="Category"></param>
        /// <param name="ReportedDate"></param>
        /// <param name="FromLoc"></param>
        /// <param name="ReportedTime"></param>
        /// <param name="ReportingTime"></param>
        /// <param name="TelNo"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>        
        public bool SaveComplainant(Int64 complaintId, String refNo, String fullName, int count, Int64 locationId,
                   Int64 titleId, String statement, DateTime onDate, String text, DateTime thisDate,
                                    String category, DateTime reportedDate, String fromLoc, String reportedTime,
                                    String reportingTime, String telNo, Database sqlDatabase, DbTransaction transaction)
        {
             DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveComplainant);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FullName", DbType.String, fullName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RefNo", DbType.String, refNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@LocID", DbType.Int64, locationId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@TitleID", DbType.Int64, titleId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Statment", DbType.String, statement);

                if (onDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@onDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@onDate", DbType.DateTime, onDate);
                if (text == string.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@Text", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@Text", DbType.String, text);
                if (thisDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ThisDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ThisDate", DbType.DateTime, thisDate);
                if (category == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@Category", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@Category", DbType.String, category);

                if (reportedDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportedDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportedDate", DbType.DateTime, reportedDate);

                if (fromLoc == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@FromLoc", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@FromLoc", DbType.String, fromLoc);

                if (reportedTime == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportedTime", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportedTime", DbType.String, reportedTime);

                if (reportingTime == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportingTime", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReportingTime", DbType.String, reportingTime);

                if (telNo == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@TelNo", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@TelNo", DbType.String, telNo);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);
                return true;           
        }
        /// <summary>
        /// This method returns true if the list of complaints are updated sucessfully 
        /// with respect to below parameters
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="Status"></param>
        /// <returns></returns>       
        public bool UpdateComplaintStatus(Int64 complaintId, Int16 status)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());          
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.UpdateComplaintStatus);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Status", DbType.Int16, status);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }



        public bool UpdateCombinedComplaints(String indexno, Int64 parentIndexNo,String uncmbIndexNo,Int64 uncmbParentIndexNo)
        {          
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.UpdateCombinedComplaints);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@IndexNo", DbType.String, indexno);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentIndexNo", DbType.Int64, parentIndexNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@UnCmbIndexNo", DbType.String, uncmbIndexNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@UnCmbParentIndexNo", DbType.Int64, uncmbParentIndexNo);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        
        public int CheckIsParent(Int64 IndexNo, ref Int64 IsChild)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());          
                int Count;
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.CheckIsParent);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@IndexNo", DbType.Int64, IndexNo);
                sqlDatabase.AddOutParameter(DSNYCPCmd, "@IsChild", DbType.Int64, 64);
                Count= (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);
                IsChild = Convert.ToInt64(DSNYCPCmd.Parameters["@IsChild"].Value);
                return Count;            
        }

         /// <summary>
        /// This method loads the list of Voilations to the DTO with respect to complaint ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>        
        public List<ViolationDTO> LoadViolations(Int64 complaintId)
        {
                List<ViolationDTO> vDTo = new List<ViolationDTO>();
                CommonClassMapper cMapper = new CommonClassMapper();
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetViolations);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                vDTo = cMapper.VoilationListMapper(DSNYCPdr);
                return vDTo;           
        }
        /// <summary>
        /// This method returns true if the list of Voilation are saved sucessfully with respect to
        /// complaintId, RefNo, Cnt, IncedentLocID, IncedentType &  ChargeID.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="RefNo"></param>
        /// <param name="Cnt"></param>
        /// <param name="IncedentLocID"></param>
        /// <param name="IncedentType"></param>
        /// <param name="ChargeID"></param>
        /// <returns></returns>        
        public bool SaveViolation(Int64 complaintId,String referenceNo,int count,Int64 incidentLocationId,String incidentType,ref Int64 chargeId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveViolation);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RefNo", DbType.String, referenceNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@LocID", DbType.Int64, incidentLocationId);
                if (incidentType == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@IncedentType", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@IncedentType", DbType.String, incidentType);
                chargeId = Convert.ToInt64(sqlDatabase.ExecuteScalar(DSNYCPCmd));
                return true;          
        }
        /// <summary>
        /// This method returns true if the list of Voilation are saved sucessfully with respect to
        /// complaintId, RefNo, Cnt, IncedentLocID, IncedentType &  ChargeID.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="RefNo"></param>
        /// <param name="Cnt"></param>
        /// <param name="IncedentLocID"></param>
        /// <param name="IncedentType"></param>
        /// <param name="ChargeID"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>        
        public bool SaveViolation(Int64 complaintId, String referenceNo, int count, Int64 incidentLocationId,String incidentType, ref Int64 chargeId,
                                    Database sqlDatabase, DbTransaction transaction)
        {
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveViolation);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RefNo", DbType.String, referenceNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@LocID", DbType.Int64, incidentLocationId);
                if (incidentType == String.Empty)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@IncedentType", DbType.String, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@IncedentType", DbType.String, incidentType);
                chargeId = Convert.ToInt64(sqlDatabase.ExecuteScalar(DSNYCPCmd, transaction));
                return true;           
        }
        /// <summary>
        /// This method loads the list of Incidents to the DTO with respect to complaint ID and ChargeID.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="ChargeID"></param>
        /// <returns></returns>       
        public List<IncedentDTO> LoadIncidents(Int64 complaintId, Int64 chargeId)
        {
                List<IncedentDTO> iDTO = new List<IncedentDTO>();
                CommonClassMapper cMapper = new CommonClassMapper();
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetIncidents);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ChargeID", DbType.Int64, chargeId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                iDTO = cMapper.IncidentListMapper(DSNYCPdr);
                return iDTO;           
        }
        /// <summary>
        /// This method returns true if the incident is saved sucessfully with respect to below parameters
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="Cnt"></param>
        /// <param name="IncidentDt"></param>
        /// <param name="ChargeID"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>       
        public bool SaveIncident(Int64 complaintId,int count,DateTime incidentDate,Int64 chargeId, Database sqlDatabase,DbTransaction transaction)
        {           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveIncident);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                if (incidentDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@IncidentDt", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@IncidentDt", DbType.DateTime, incidentDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ChargeID", DbType.Int64, chargeId);
                sqlDatabase.ExecuteScalar(DSNYCPCmd, transaction);
                return true;
           
        }
        /// <summary>
        /// This method returns true if Incident are saved sucessfully with respect to
        /// complaintId, RefNo, Cnt, IncedentLocID, IncedentType &  ChargeID.
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="Cnt"></param>
        /// <param name="IncidentDt"></param>
        /// <param name="ChargeID"></param>
        /// <returns></returns>        
        public bool SaveIncident(Int64 complaintId,int count,DateTime incidentDate,Int64 chargeId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveIncident);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                if (incidentDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@IncidentDt", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@IncidentDt", DbType.DateTime, incidentDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ChargeID", DbType.Int64, chargeId);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;          
        }
        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 documentId, Int64 parentId, string childId, string appName)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                List<UpLoadFileDTO> upLoadDTO = new List<UpLoadFileDTO>();
                UpLoadFileMapper uMapper = new UpLoadFileMapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.LoadComplaintAttachments);
                if (documentId != 0)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@DocumentID", DbType.Int64, documentId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@DocumentID", DbType.Int64, System.DBNull.Value);
                }
                if (parentId != 0)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, parentId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, System.DBNull.Value);
                }
                if (childId != string.Empty)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, childId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, System.DBNull.Value);
                }

                if (appName != string.Empty)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AppName", DbType.String, appName);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AppName", DbType.String, System.DBNull.Value);
                }
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                upLoadDTO = uMapper.ComplaintAttachmentsList(DSNYCPdr);
                return upLoadDTO;                 
        }
        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 documentId, Int64 parentId, Int64 LotId, Int64 inspectionRequestId, string childId, string appName)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DataSet DSNYCPdr = null;
                List<UpLoadFileDTO> upLoadDTO = new List<UpLoadFileDTO>();
                UpLoadFileMapper uMapper = new UpLoadFileMapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.LotCleaning_LoadAttachments);
                if (documentId != 0)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@DocumentID", DbType.Decimal, (decimal)documentId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@DocumentID", DbType.Decimal, System.DBNull.Value);
                }
                if (parentId != 0)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, parentId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, System.DBNull.Value);
                }
                if (LotId != 0)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@LotId", DbType.Int64, LotId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@LotId", DbType.Int64, System.DBNull.Value);
                }
                if (childId != string.Empty)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, childId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, System.DBNull.Value);
                }
                if (inspectionRequestId != 0)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@InspectRequestId", DbType.Int64, inspectionRequestId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@InspectRequestId", DbType.Int64, System.DBNull.Value);
                }
                if (appName != string.Empty)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AppName", DbType.String, appName);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AppName", DbType.String, System.DBNull.Value);
                }
                DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                upLoadDTO = uMapper.ComplaintAttachmentsListForLotCleaning(DSNYCPdr);
                return upLoadDTO;           
        }
        public List<UpLoadFileDTO> LoadComplaintAttachments(Int64 documentId, Int64 parentId, string childId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                List<UpLoadFileDTO> upLoadDTO = new List<UpLoadFileDTO>();
                UpLoadFileMapper uMapper = new UpLoadFileMapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.LoadComplaintAttachments);
                if (documentId != 0)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@DocumentID", DbType.Int64, documentId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@DocumentID", DbType.Int64, System.DBNull.Value);
                }
                if (parentId != 0)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, parentId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, System.DBNull.Value);
                }
                if (childId != string.Empty)
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, childId);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, System.DBNull.Value);
                }
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                upLoadDTO = uMapper.ComplaintAttachmentsListDS249(DSNYCPdr);
                return upLoadDTO;           
        }
        public bool DeleteSupportedDocument(Int64 parentId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.DeleteSupportedDocument);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, parentId);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }               
        public bool UploadFiles(long parentId, byte[] files, String contentType, String FileName, String child1)
        {           
                String fileExtension = FileName.Substring(FileName.Length - 3);
                if (!string.IsNullOrEmpty(fileExtension))
                    if (fileExtension.ToLower().ToString() == "pdf")
                        contentType = "application/pdf";
                    else if (fileExtension.ToLower().ToString() == "doc")
                        contentType = "application/msword";
                    else if (fileExtension.ToLower().ToString() == "xls")
                        contentType = "application/vnd.ms-excel";
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.InsertComplaintAttachments);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, parentId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Attachments", DbType.Binary, files);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AttachementName", DbType.String, FileName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ContentType", DbType.String, contentType);
                if (!child1.Equals(string.Empty))
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, child1);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, System.DBNull.Value);
                }
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        public bool UploadFiles(Int64 parentId, Int64 lotId, Int64 inspectionRequestId, String fileLocation, String applicationName, String contentType, String fileName, String chargeCode, String child1,
            DateTime imageDate, String whenTaken, String takenBy, String note)
        {
           
                String fileExtension = fileName.Substring(fileName.Length - 3);
                if (!string.IsNullOrEmpty(fileExtension))
                    if (fileExtension.ToLower().ToString() == "pdf")
                        contentType = "application/pdf";
                    else if (fileExtension.ToLower().ToString() == "doc")
                        contentType = "application/msword";
                    else if (fileExtension.ToLower().ToString() == "xls")
                        contentType = "application/vnd.ms-excel";
               
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.LotCleaning_InsertAttachments);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, parentId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@LotId", DbType.Int64, lotId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@InspectRequestId", DbType.Int64, inspectionRequestId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AttachmentLocation", DbType.String, fileLocation);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationName", DbType.String, applicationName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AttachementName", DbType.String, fileName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ContentType", DbType.String, contentType);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ImageDate", DbType.DateTime, imageDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@WhenTaken", DbType.String, whenTaken);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@TakenBy", DbType.String, takenBy);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Note", DbType.String, note);
                if (!child1.Equals(string.Empty))
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, child1);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, System.DBNull.Value);
                }
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }
        //dO
        public bool UploadFiles(long parentId, byte[] files, String applicationName, String contentType, String fileName, String child1)
        {           
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplaintAttachments);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentID", DbType.Int64, parentId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Attachments", DbType.Binary, files);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationName", DbType.String, applicationName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AttachementName", DbType.String, fileName);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ContentType", DbType.String, contentType);
                if (!child1.Equals(string.Empty))
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, child1);
                }
                else
                {
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildId1", DbType.String, System.DBNull.Value);
                }
                
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;       
           
        }
        public bool SaveCombinePenalties(long parentId, Int16 childCombPenId, Int16 combPenId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveCombinedPenalties);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentCombPenID", DbType.Int64, parentId);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@ChildCombPenID", DbType.Int16, childCombPenId);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@IsActive", DbType.String, "1");

            //Commented if loop, because combPenId is not nullable for the result of the condition is always true
            //if (combPenId != null)
            //{
            sqlDatabase.AddInParameter(DSNYCPCmd, "@CombPenID", DbType.Int16, combPenId);
            //}
            sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
            return true;
        }
        public bool DeleteCombinePenalties(long parentId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.DeleteCombinedPenalties);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ParentCombPenID", DbType.Int64, parentId);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;          
        }
        #endregion

        #region PERSONNEL
        /// <summary>
        /// This method gets the list of Personnel details to the DTO with respect to search ccriteria
        /// </summary>
        /// <param name="SearchString"></param>
        /// <returns></returns>        
        public List<PersonnelListDTO> GetPersonnelList(String searchValue)
        {
                List<PersonnelListDTO> pDTO = new List<PersonnelListDTO>();
                ComplaintDS249Mapper cMapper = new ComplaintDS249Mapper();
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SearchPersonnelList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SearchString", DbType.String, searchValue);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                pDTO = cMapper.PersonnelListMapper(DSNYCPdr);
                return pDTO;   

        }
        /// <summary>
        /// This method gets the list of Personnel details to the DTO
        /// </summary>
        /// <returns></returns>        
        public List<PersonnelDTO> GetPersonnelList()
        {
                List<PersonnelDTO> pDTO = new List<PersonnelDTO>();
                ComplaintDS249Mapper cMapper = new ComplaintDS249Mapper();
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetPersonnelList);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                pDTO = cMapper.PersonnelMapper(DSNYCPdr);
                return pDTO;       
            
        }
        /// <summary>
        ///  This method gets the list of Personnel details to the DTO w.r.t peronnel ID
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public List<PersonnelByIdDTO> GetPersonnel(Int64 id)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                List<PersonnelByIdDTO> personnelIDDTO = new List<PersonnelByIdDTO>();
                ComplaintDS249Mapper complaintMapper = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetPersonnel);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonnelID", DbType.Int64, id);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                personnelIDDTO = complaintMapper.PersonnelByIDMapper(DSNYCPdr);
                return personnelIDDTO;   

        }
        /// <summary>
        /// This method gets the list of Personnel details to the DTO w.r.t refernce number
        /// </summary>
        /// <param name="RefNumber"></param>
        /// <returns></returns>
        public List<PersonnelByRefNoDTO> GetPersonnel(String referenceNumber)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                List<PersonnelByRefNoDTO> personnelByRefNoDTO = new List<PersonnelByRefNoDTO>();
                ComplaintDS249Mapper cMapper = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetPersonnelByRefNumber);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ReferenceNo", DbType.String, referenceNumber);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                personnelByRefNoDTO = cMapper.PersonnelByRefNoMapper(DSNYCPdr);
                return personnelByRefNoDTO;  
        }

        #endregion

        #region COMMON
        /// <summary>
        /// This method returns the list of charges to the DTO w.r.t charge code
        /// </summary>
        /// <param name="ChargeCode"></param>
        /// <returns></returns>        
        public List<ChargeDTO> GetCharges(String chargeCode)
        {
                List<ChargeDTO> cDTO = new List<ChargeDTO>();
                ComplaintDS249Mapper cMapper = new ComplaintDS249Mapper();
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SearchChargeType);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ChargeCode", DbType.String, chargeCode);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                cDTO = cMapper.ChargeListMapper(DSNYCPdr);
                return cDTO;           
        }
        /// <summary>
        /// This method returns the list of charges to the DTO
        /// </summary>
        /// <returns></returns>        
        public List<ChargesWithoutParametersDTO> GetCharges()
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                List<ChargesWithoutParametersDTO> chargesDTO = new List<ChargesWithoutParametersDTO>();
                ComplaintDS249Mapper cMapper = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetChargeType);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                chargesDTO = cMapper.ChargeListWithoutParameterMapper(DSNYCPdr);
                return chargesDTO;       

        }
        /// <summary>
        /// This method returns the list of CodeList to the DTO with respect to category ID
        /// </summary>
        /// <param name="CategoryID"></param>
        /// <returns></returns>
        public List<CodeDTO> GetCodeList(Int16 categoryId)
        {
            List<CodeDTO> cDTO = new List<CodeDTO>();
            BCADMapper bMapper = new BCADMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetCodeList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CategoryID", DbType.Int16, categoryId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                cDTO = bMapper.CodeListMapper(DSNYCPdr);
                return cDTO;            
        }
        /// <summary>
        /// This method returns the list of BCAD CaseStatus to the DTO
        /// </summary>
        /// <returns></returns>
        public List<BCADStatusDTO> GetBCADCaseStatus()
        {
            List<BCADStatusDTO> bcadStatusDTO = new List<BCADStatusDTO>();
            BCADMapper bMapper = new BCADMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetBCADCaseStatus);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                bcadStatusDTO = bMapper.BCADStatutsListMapper(DSNYCPdr);
                return bcadStatusDTO;         
            
        }
        /// <summary>
        /// This method returns the list of Medical CaseStatus to the dataset
        /// </summary>
        /// <returns></returns>
        public List<MedicalStatusDTO> GetMedicalCaseStatus()
        {
            List<MedicalStatusDTO> bcadStatusDTO = new List<MedicalStatusDTO>();
            MedicalMapper mMapper = new MedicalMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetMedicalCaseStatus);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                bcadStatusDTO = mMapper.MedicalStatutsListMapper(DSNYCPdr);
                return bcadStatusDTO;            
        }
        /// <summary>
        /// This method returns the list of location to the dataset
        /// This method returns the list of location to the DTO
        /// </summary>
        /// <returns></returns>
        public List<LocationDTO> GetLocations()
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                ComplaintDS249Mapper locationListFunction = new ComplaintDS249Mapper();
                List<LocationDTO> locationDTO = new List<LocationDTO>();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetLocation);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                locationDTO = locationListFunction.GetLocationsMapper(DSNYCPdr);
                return locationDTO;          
        }
        /// <summary>
        /// This method gets the list of titles to the DTO
        /// </summary>
        /// <returns></returns>       
        public List<TitleDTO> GetTitles()
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                List<TitleDTO> titleDTO = new List<TitleDTO>();
                ComplaintDS249Mapper cMapper = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetTitles);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                titleDTO = cMapper.TitleMapper(DSNYCPdr);
                return titleDTO;           
        }
        /// <summary>
        /// This method gets the list of Boroughs to the DTO w.r.t search criteria
        /// </summary>
        /// <param name="SearchText"></param>
        /// <returns></returns>        
        public List<TitleListDTO> GetTitles(String searchText)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                List<TitleListDTO> titleDTO = new List<TitleListDTO>();
                ComplaintDS249Mapper complaintMapper = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SearchTitles);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SearchText", DbType.String, searchText);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                titleDTO = complaintMapper.TitleListMapper(DSNYCPdr);
                return titleDTO;            
        }
        /// <summary>
        /// This method gets the list of Boroughs to the DTO 
        /// </summary>
        /// <returns></returns>
        public List<BoroughDTO> GetBoroughs()
        {

            List<BoroughDTO> bDTO = new List<BoroughDTO>();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                ComplaintDS249Mapper boroughListFunction = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetBorough);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                bDTO = boroughListFunction.GetBoroughMapper(DSNYCPdr);
                return bDTO;
        }
        /// <summary>
        /// This method gets the list of complaint by districts to the DTO
        /// </summary>
        /// <returns></returns>
        public List<ComplaintByDistrictDTO> GetComplaintByDistrict()
        {
            List<ComplaintByDistrictDTO> complaintByDistrictDTO = new List<ComplaintByDistrictDTO>();
            CommonClassMapper cMapper = new CommonClassMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetComplaintByDistrict);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                complaintByDistrictDTO = cMapper.ComplaintDistrictMapper(DSNYCPdr);
                return complaintByDistrictDTO;    
           
        }
        #endregion

        #region BCAD Complaints
        /// <summary>
        /// This method returns the list of BCAD Complaints to the DTO 
        /// </summary>
        /// <returns></returns>        
        public List<ComplaintDTO> GetBCADComplaints(int pageNo, int maximumRows)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            if (pageNo != 0)
                pageNo -= 1;
            List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
            BCADMapper bcadComplaint = new BCADMapper();
            DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetBCADComplaints);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                cDTO = bcadComplaint.ComplaintListBCAD(DSNYCPdr);
                return cDTO;           
        }
        /// <summary>
        /// This method gets the list of complaint to the DTO with respect to personal ID 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>        
        public List<ComplaintDTO> GetBCADSearchComplaints(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string strValue)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            if (pageNo != 0)
                pageNo -= 1;
            List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
            DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetBCADComplaintsBySearch);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@numericValue", DbType.Int64, numericValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@chargesValue", DbType.String, decimalValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@complaintDate", DbType.String, dateValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@stringValue", DbType.String, strValue);
                DataSet DSNYCPds = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                BCADMapper bcadComplaint = new BCADMapper();
                cDTO = bcadComplaint.ComplaintListBCAD(DSNYCPds);
                return cDTO;           
        }
        /// <summary>
        /// Call stored proc to return medical complaints.
        /// </summary>
        /// <returns></returns>
        public List<ComplaintDTO> GetMedicalComplaints(int pageNo, int maximumRows)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            if (pageNo != 0)
                pageNo -= 1;
            List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
            MedicalMapper medicalComplaint = new MedicalMapper();           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetMedicalComplaints);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                cDTO = medicalComplaint.ComplaintListMedicalMapper(DSNYCPdr);
                return cDTO;            
           
        }
        /// <summary>
        /// This method gets the list of complaint to the DTO with respect to personal ID 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>       
        public List<ComplaintDTO> GetMedicalSearchComplaints(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string strValue)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            if (pageNo != 0)
                pageNo -= 1;
            List<ComplaintDTO> cDTO = new List<ComplaintDTO>();
            MedicalMapper medicalComplaint = new MedicalMapper();
              DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetMedicalComplaintsBySearch);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@numericValue", DbType.Int64, numericValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@chargesValue", DbType.String, decimalValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@complaintDate", DbType.String, dateValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@stringValue", DbType.String, strValue);
                DataSet DSNYCPds = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                cDTO = medicalComplaint.ComplaintListMedicalMapper(DSNYCPds);
                return cDTO;
            
        }
        /// <summary>
        /// This method returns the list of BCADComplaint to the dataset w.r.t ComplaintID
        /// This method returns the list of BCADComplaint to the DTO w.r.t ComplaintID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>       
        public List<BCADComplaintDTO> GetBCADComplaint(Int64 complaintId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<BCADComplaintDTO> bcadComplaintDTO = new List<BCADComplaintDTO>();
            BCADMapper bMapper = new BCADMapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetBCADComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                bcadComplaintDTO = bMapper.BCADComplaintMapper(DSNYCPdr);
                return bcadComplaintDTO;
                       
        }
        /// <summary>
        /// This method returns the list of MedicalComplaint to the dataset w.r.t ComplaintID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>        
        public List<MedicalComplaintDTO> GetMedicalComplaint(Int64 complaintId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<MedicalComplaintDTO> medicalComplaintDto = new List<MedicalComplaintDTO>();
            MedicalMapper mMapper = new MedicalMapper();
            DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetMedicalComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                medicalComplaintDto = mMapper.MedicalComplaintMapper(DSNYCPdr);
                return medicalComplaintDto;
           
        }
        /// <summary>
        /// This method returns the list of Advocate Complaint to the DTO w.r.t ComplaintID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>       
        public List<ComplaintAdvocacyDTO> GetAdvocateComplaint(Int64 complaintId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintAdvocacyDTO> advocateComplaintDto = new List<ComplaintAdvocacyDTO>();
            AdvocateMapper aMapper = new AdvocateMapper();            
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocateComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                advocateComplaintDto = aMapper.ComplaintAdvocatcy(DSNYCPdr);
                return advocateComplaintDto;            
        }

        /// <summary>
        /// This method returns true if the list of BCAD Complaint are created sucessfully 
        /// with respect to ComplaintID and User ID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="CreatedUser"></param>
        /// <returns></returns>        
        public bool CreateBCADComplaint(Int64 complaintId, Int16 createdUser)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());          
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.CreateBCADComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CreatedUser", DbType.Int16, createdUser);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;          
        }

        public bool CreateMedicalComplaint(Int64 complaintId, Int16 createdUser)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.CreateMedicalComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CreatedUser", DbType.Int16, createdUser);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the list of Advocate Complaint are opened sucessfully 
        /// with respect to Advocacy Case id, User ID and date.
        /// </summary>
        /// <param name="AdvocacyCaseId"></param>
        /// <param name="receivedUser"></param>
        /// <param name="receivedDate"></param>
        /// <returns></returns>        
        public bool OpenAdvocateComplaint(Int64 advocacyCaseId, Int16 receivedUser, DateTime receivedDate)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());          
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.OpenAdvocateComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseId", DbType.Int64, advocacyCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ReceivedUser", DbType.Int16, receivedUser);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ReceivedDate", DbType.DateTime, receivedDate);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the list of BCAD Complaint are set  sucessfully
        /// with respect to ComplaintID and StatusCD
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="StatusCD"></param>
        /// <returns></returns>        
        public bool SetBCADStatus(Int64 complaintId,
                                  Int16 statusCD)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SetBCADStatus);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@StatusCd", DbType.Int16, statusCD);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the list of Medical Complaint are set  sucessfully
        /// with respect to ComplaintID and StatusCD
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="StatusCD"></param>
        /// <returns></returns>       
        public bool SetMedicalStatus(Int64 complaintId, Int16 statusCD)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SetMedicalStatus);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@StatusCd", DbType.Int16, statusCD);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the list of Advocate Complaint are set  sucessfully
        /// with respect to ComplaintID ,StatusCD and  advocateCaseID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="StatusCD"></param>
        /// <param name="advocateCaseID"></param>
        /// <returns></returns>       
        public bool SetAdvocateCaseStatus(Int64 complaintId, Int16 statusCD, Int64 advocateCaseId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SetAdvocateCaseStatus);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@StatusCd", DbType.Int16, statusCD);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseId", DbType.Int64, advocateCaseId);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }
       
        /// <summary>
        /// This method returns true if the BCAD are returned to authour  sucessfully
        /// with respect to ComplaintID 
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>        
        public bool ReturnMedicalToAuthor(Int64 ComplaintID, Int64 personalID)
        {
              SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ReturnMedicalCaseToAuthor);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, ComplaintID);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonID", DbType.Int64, personalID);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;          
        }
        /// <summary>
        /// This method returns true if the BCAD are returned to authour  sucessfully
        /// with respect to ComplaintID 
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>        
        public bool ReturnBCADToAuthor(Int64 complaintId, Int64 personalID)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ReturnCaseToAuthor);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonID", DbType.Int64, personalID);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the BCAD are returned to Advocate  sucessfully
        /// with respect to ComplaintID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool ReturnBCADToAdvocate(Int64 complaintId, Int64 personalID)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ReturnCaseToAdvocate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonID", DbType.Int64, personalID);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }
        /// <summary>
        /// This method returns true if the BCAD are returned to Advocate  sucessfully
        /// with respect to ComplaintID
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <returns></returns>
        public bool ReturnMedicalToAdvocate(Int64 complaintID, Int64 personalID)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ReturnMedicalCaseToAdvocate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintID);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonID", DbType.Int64, personalID);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the Advocate are returned to authour  sucessfully
        /// with respect to ComplaintID and advocacy Case Id
        /// </summary>
        /// <param name="ComplaintID"></param>
        /// <param name="advocacyCaseId"></param>
        /// <returns></returns>       
        public bool ReturnAdvocateCaseToAuthor(Int64 complaintId, Int64 advocacyCaseId, Int64 personalID)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
              DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ReturnAdvocateCaseToAuthor);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseId", DbType.Int64, advocacyCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonID", DbType.Int64, personalID);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the Advocate are returned to BCAD  sucessfully
        /// with respect to ComplaintID and advocacy Case Id
        /// </summary>
        /// <param name="complaintID"></param>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>       
        public bool ReturnAdvocateToBCAD(Int64 complaintId, Int64 advocacyCaseId, Int64 personalID)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ReturnCaseToBCAD);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonID", DbType.Int64, personalID);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }
        /// <summary>
        /// This method returns true if the list of BCAD complaints are saved sucessfully 
        /// with respect to bcadComplaint
        /// </summary>
        /// <param name="bcadComplaint"></param>
        /// <returns></returns>      
        public bool SaveBCADComplaint(BCADComplaintDTO bcadComplaint)
        {
            Database db = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                 SaveBCADComplaint(bcadComplaint.BCADCaseID, bcadComplaint.FinalStatusCd, bcadComplaint.NonPenaltyResultCd,
                             bcadComplaint.AdjudicationBodyCd, bcadComplaint.AjudicationDate,
                             bcadComplaint.ModifiedUser, db, transaction);
                    if (bcadComplaint.Penalty.Count > 0)
                    {
                        Int16 cnt = 1;
                        foreach (BCADPenaltyDTO bcadPenalty in bcadComplaint.Penalty)
                        {
                            bcadPenalty.PenaltyGroupID = bcadComplaint.BCADCaseID;
                            SaveBCADPenaltyList(bcadPenalty.PenaltyGroupID,
                                                    bcadPenalty.PenaltyTypeCode,
                                                    bcadPenalty.Days,
                                                    bcadPenalty.Hours,
                                                    bcadPenalty.Minutes,
                                                    bcadPenalty.MoneyValue,
                                                    bcadPenalty.Notes,
                                                    cnt, db, transaction);
                            cnt++;
                        }
                    }
                    transaction.Commit();
                }               
                return true;        

        }
        /// <summary>
        /// This method returns true if the list of Medical complaints are saved sucessfully 
        /// with respect to bcadComplaint
        /// </summary>
        /// <param name="medicalComplaint"></param>
        /// <returns></returns>      
        public bool SaveMedicalComplaint(MedicalComplaintDTO medicalComplaint)
        {
            Database db = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                SaveMedicalComplaint(medicalComplaint.MedicalCaseID, medicalComplaint.FinalStatusCd, medicalComplaint.NonPenaltyResultCd,
                             medicalComplaint.AdjudicationBodyCd, medicalComplaint.AjudicationDate,
                             medicalComplaint.ModifiedUser, db, transaction);
                    if (medicalComplaint.Penalty.Count > 0)
                    {
                        Int16 cnt = 1;
                        foreach (MedicalPenaltyDTO medicalPenalty in medicalComplaint.Penalty)
                        {
                            medicalPenalty.PenaltyGroupID = medicalComplaint.MedicalCaseID;
                            SaveMedicalPenaltyList(medicalPenalty.PenaltyGroupID,
                                                    medicalPenalty.PenaltyTypeCode,
                                                    medicalPenalty.Days,
                                                    medicalPenalty.Hours,
                                                    medicalPenalty.Minutes,
                                                    medicalPenalty.MoneyValue,
                                                    medicalPenalty.Notes,
                                                    cnt, db, transaction);
                            cnt++;
                        }
                    }
                    transaction.Commit();
                }         
               
                return true;        

        }
        /// <summary>
        /// This method returns true if the list of BCAD complaints are saved sucessfully 
        /// with respect to below parameters
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <param name="FinalStatusCd"></param>
        /// <param name="NonPenaltyResultcd"></param>
        /// <param name="AdjudicationBodycd"></param>
        /// <param name="AdjudicationDate"></param>
        /// <param name="ModifiedUser"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>        
        public bool SaveBCADComplaint(Int64 bcadCaseId, Int16 finalStatusCD,Int16 nonPenaltyResultCD,Int16 adjudicationBodyCD,DateTime adjudicationDate,
                                      Int16 modifiedUser,Database sqlDatabase,DbTransaction transaction)
        {

                 DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveBCADComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@BCADCaseID", DbType.Int64, bcadCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@NonPenaltyResultCd", DbType.Int16, nonPenaltyResultCD);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdjudicationBodyCd", DbType.Int16, adjudicationBodyCD);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FinalStatusCd", DbType.Int16, finalStatusCD);
                if (adjudicationDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AdjudicationDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AdjudicationDate", DbType.DateTime, adjudicationDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ModifiedUser", DbType.Int16, modifiedUser);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }
        /// <summary>
        /// This method returns true if the list of BCAD complaints are saved sucessfully 
        /// with respect to below parameters
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <param name="FinalStatusCd"></param>
        /// <param name="NonPenaltyResultcd"></param>
        /// <param name="AdjudicationBodycd"></param>
        /// <param name="AdjudicationDate"></param>
        /// <param name="ModifiedUser"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public bool SaveMedicalComplaint(Int64 medicalCaseId,Int16 finalStatusCd,Int16 nonPenaltyResultCd,Int16 adjudicationBodyCD,DateTime adjudicationDate,
                                      Int16 modifiedUser,Database sqlDatabase,DbTransaction transaction)
        {          
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveMedicalComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MedicalCaseID", DbType.Int64, medicalCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@NonPenaltyResultCd", DbType.Int16, nonPenaltyResultCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdjudicationBodyCd", DbType.Int16, adjudicationBodyCD);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FinalStatusCd", DbType.Int16, finalStatusCd);
                if (adjudicationDate == DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AdjudicationDate", DbType.DateTime, DBNull.Value);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AdjudicationDate", DbType.DateTime, adjudicationDate);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ModifiedUser", DbType.Int16, modifiedUser);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;                      
        }

        /// <summary>
        /// This method loads the list of BCAD Penalty to the dataset with respect to BCAD CaseID 
        /// This method loads the list of BCAD Penalty to the DTO with respect to BCAD CaseID 
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <returns></returns>       
        public List<BCADPenaltyDTO> LoadBCADPenalty(Int64 bcadCaseId)
        {
            List<BCADPenaltyDTO> bcadPenaltyDTO = new List<BCADPenaltyDTO>();
            BCADMapper bMapper = new BCADMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetBCADPenaltyList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@BCADCaseID", DbType.Int64, bcadCaseId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                bcadPenaltyDTO = bMapper.BCADPenaltyListMapper(DSNYCPdr);
                return bcadPenaltyDTO;            
        }
        /// <summary>
        /// This method loads the list of BCAD Penalty to the dataset with respect to BCAD CaseID 
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <returns></returns>
        public List<MedicalPenaltyDTO> LoadMedicalPenalty(Int64 medicalCaseId)
        {
            List<MedicalPenaltyDTO> medicalPenaltyDTO = new List<MedicalPenaltyDTO>();
            MedicalMapper mMapper = new MedicalMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
             DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetMedicalPenaltyList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MedicalCaseID", DbType.Int64, medicalCaseId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                medicalPenaltyDTO = mMapper.MedicalPenaltyListMapper(DSNYCPdr);
                return medicalPenaltyDTO;           
           
        }
        /// <summary>
        /// This method returns true if the list of BCAD Penalty are saved sucessfully with respect to below parameters
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <param name="penaltyTypeCd"></param>
        /// <param name="Days"></param>
        /// <param name="Hours"></param>
        /// <param name="Minutes"></param>
        /// <param name="MoneyValue"></param>
        /// <param name="Notes"></param>
        /// <param name="Cnt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>        
        public bool SaveBCADPenaltyList(Int64 bcadCaseId,
                                    Int16 penaltyTypeCd,
                                    String days,
                                    String hours,
                                    String minutes,
                                    String moneyValue,
                                    String notes,
                                    int count,
                                    Database sqlDatabase,
                                    DbTransaction transaction
                                   )
        {

          
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveBCADPenaltyList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@BCADPenaltyID", DbType.Int64, bcadCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@penaltyTypeCd", DbType.Int16, penaltyTypeCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Days", DbType.String, days);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Hours", DbType.String, hours);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Minutes", DbType.String, minutes);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MoneyValue", DbType.String, moneyValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Notes", DbType.String, notes);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method returns true if the list of Medical Penalty are saved sucessfully with respect to below parameters
        /// </summary>
        /// <param name="medicalCaseId"></param>
        /// <param name="penaltyTypeCd"></param>
        /// <param name="Days"></param>
        /// <param name="Hours"></param>
        /// <param name="Minutes"></param>
        /// <param name="MoneyValue"></param>
        /// <param name="Notes"></param>
        /// <param name="Cnt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public bool SaveMedicalPenaltyList(Int64 medicalCaseId,Int16 penaltyTypeCd,String days,String hours,
                                    String minutes,String moneyValue,String notes,
                                    int count, Database sqlDatabase,DbTransaction transaction)
        {

                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveMedicalPenaltyList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MedicalPenaltyID", DbType.Int64, medicalCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@penaltyTypeCd", DbType.Int16, penaltyTypeCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Days", DbType.String, days);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Hours", DbType.String, hours);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Minutes", DbType.String, minutes);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MoneyValue", DbType.String, moneyValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Notes", DbType.String, notes);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
            
        }
        /// <summary>
        /// This method returns true if the list of BCAD Penalty are saved sucessfully with respect to below parameters
        /// </summary>
        /// <param name="BCADCaseID"></param>
        /// <param name="penaltyTypeCd"></param>
        /// <param name="Days"></param>
        /// <param name="Hours"></param>
        /// <param name="Minutes"></param>
        /// <param name="MoneyValue"></param>
        /// <param name="Notes"></param>
        /// <param name="Cnt"></param>
        /// <returns></returns>
        /// 
        public bool SaveBCADPenaltyList(Int64 bcadCaseId,Int16 penaltyTypeCd,String days,
                                 String hours,String minutes,String moneyValue,String notes,int count)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveBCADPenaltyList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@BCADPenaltyID", DbType.Int64, bcadCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@penaltyTypeCd", DbType.Int16, penaltyTypeCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Days", DbType.String, days);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Hours", DbType.String, hours);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Minutes", DbType.String, minutes);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MoneyValue", DbType.String, moneyValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Notes", DbType.String, notes);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;
           
        }
        /// <summary>
        /// Call Store proc "usp_SaveMedicalPenalty"
        /// </summary>
        /// <param name="MedicalCaseID"></param>
        /// <param name="penaltyTypeCd"></param>
        /// <param name="Days"></param>
        /// <param name="Hours"></param>
        /// <param name="Minutes"></param>
        /// <param name="MoneyValue"></param>
        /// <param name="Notes"></param>
        /// <param name="Cnt"></param>
        /// <returns></returns>
        public bool SaveMedicalPenaltyList(Int64 medicalCaseId,Int16 penaltyTypeCd,String days,String hours,String minutes,
                                   String moneyValue,String notes,int count)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveMedicalPenaltyList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MedicalPenaltyID", DbType.Int64, medicalCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@penaltyTypeCd", DbType.Int16, penaltyTypeCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Days", DbType.String, days);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Hours", DbType.String, hours);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Minutes", DbType.String, minutes);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MoneyValue", DbType.String, moneyValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Notes", DbType.String, notes);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;
           
        }

        #endregion

        #region Advocate Complaints
        /// <summary>
        /// This method returns the list of Advocate complaint to the DTO 
        /// </summary>
        /// <returns></returns>
        public List<ComplaintDTO> GetAdvocateComplaints(int pageNo, int maximumRows, ref Int64 rowsCount)
        {
            List<ComplaintDTO> cDTOAdvocate = new List<ComplaintDTO>();
            if (pageNo != 0)
                pageNo -= 1;
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocateComplaints);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);
                sqlDatabase.AddOutParameter(DSNYCPCmd, "@rowsCount", DbType.Int64, 64);
                DSNYCPCmd.CommandTimeout = 0;
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                rowsCount = Convert.ToInt64(DSNYCPCmd.Parameters["@rowsCount"].Value);
                AdvocateMapper complaintMapper = new AdvocateMapper();
                cDTOAdvocate = complaintMapper.ComplaintListAdvocate(DSNYCPdr);
                return cDTOAdvocate;          
        }

        #endregion

        #region Advocate Complaints
        /// <summary>
        /// This method returns the list of Advocate complaint to the DTO 
        /// </summary>
        /// <returns></returns>
        public List<ComplaintDTO> GetCombinedComplaints(int pageNo, Int64 employeeID, int maximumRows, Int64 complaintId, Int64 indexNo, ref Int64 rowsCount)
        {
            List<ComplaintDTO> cDTOAdvocate = new List<ComplaintDTO>();
            if (pageNo != 0)
                pageNo -= 1;
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetCombinedComplaints);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmpId", DbType.Int64, employeeID);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@IndexNO", DbType.Int64, indexNo);
                sqlDatabase.AddOutParameter(DSNYCPCmd, "@rowsCount", DbType.Int64, 64);
                DSNYCPCmd.CommandTimeout = 0;
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                rowsCount = Convert.ToInt64(DSNYCPCmd.Parameters["@rowsCount"].Value);
                AdvocateMapper complaintMapper = new AdvocateMapper();
                cDTOAdvocate = complaintMapper.ComplaintListCombined(DSNYCPdr);
                return cDTOAdvocate;
        }
        public List<ComplaintDTO> CheckCombinedComplaints(Int64 complaintId)
        {
            List<ComplaintDTO> cDTOAdvocate = new List<ComplaintDTO>();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.CheckCombinedComplaints);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                  DSNYCPCmd.CommandTimeout = 0;
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                AdvocateMapper complaintMapper = new AdvocateMapper();
                cDTOAdvocate = complaintMapper.CombinedComplaintList(DSNYCPdr);
                return cDTOAdvocate;            
        }
        /// <summary>
        /// This method gets the list of complaint to the DTO with respect to personal ID 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public List<ComplaintDTO> GetAdvocateSearchComplaints(int pageNo, int maximumRows, int numericValue, string decimalValue, string dateValue, string strValue, ref Int32 rowCount)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            if (pageNo != 0)
                pageNo -= 1;
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocateComplaintsBySearch);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pageNo", DbType.Int64, pageNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@maximumRows", DbType.Int64, maximumRows);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@numericValue", DbType.Int64, numericValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@chargesValue", DbType.String, decimalValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@complaintDate", DbType.String, dateValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@stringValue", DbType.String, strValue);
                sqlDatabase.AddOutParameter(DSNYCPCmd, "@RowCount", DbType.Int64, 64);
                DataSet DSNYCPds = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                rowCount = Convert.ToInt32(DSNYCPCmd.Parameters["@RowCount"].Value);
                AdvocateMapper complaintMapper = new AdvocateMapper();
                complaintListDTO = complaintMapper.ComplaintListAdvocate(DSNYCPds);
                return complaintListDTO;           
        }
        /// <summary>
        /// This method gets the count of the list of complaint 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public int LoadAdvocateListCount(Int16 personalId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();            
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocateComplaintsCount);
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);           
        }
        

        public int LoadCombineListCount(Int64 employeeID, Int64 complaintId, Int64 indexNo)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();
              DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetCombineListCount);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmpId", DbType.Int64, employeeID);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@IndexNO", DbType.Int64, indexNo); 
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);           
        }
        
        /// <summary>
        /// This method gets the count of the list of complaint 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>        
        public int LoadSearchAdvocateListCount(Int16 personalId, int numericValue, string decimalValue, string dateValue, string strValue)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();          
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetSearchAdvocateComplaintsCount);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@numericValue", DbType.Int64, numericValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@chargesValue", DbType.String, decimalValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@complaintDate", DbType.String, dateValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@stringValue", DbType.String, strValue);
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);           
        }
        /// <summary>
        /// This method gets the count of the list of complaint 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>
        public int LoadMedicalListCount(Int16 personalId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetMedicalComplaintsCount);
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);           
        }
        /// <summary>
        /// This method gets the count of the list of complaint 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>        
        public int LoadSearchMedicalListCount(Int16 personalId, int numericValue, string decimalValue, string dateValue, string strValue)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();          
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetSearchMedicalComplaintsCount);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@numericValue", DbType.Int64, numericValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@chargesValue", DbType.String, decimalValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@complaintDate", DbType.String, dateValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@stringValue", DbType.String, strValue);
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);           
        }
        /// <summary>
        /// This method gets the count of the list of complaint 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>        
        public int LoadBCADListCount(Int16 personalId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetBCADComplaintsCount);
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);           
        }
        /// <summary>
        /// This method gets the count of the list of complaint 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>    
        public int LoadSearchBCADListCount(Int16 personalId, int numericValue, string decimalValue, string dateValue, string stringValue)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<ComplaintDTO> complaintListDTO = new List<ComplaintDTO>();           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetSearchBCADComplaintsCount);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@numericValue", DbType.Int64, numericValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@chargesValue", DbType.String, decimalValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@complaintDate", DbType.String, dateValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@stringValue", DbType.String, stringValue);
                return (int)sqlDatabase.ExecuteScalar(DSNYCPCmd);           
        }
        /// <summary>
        /// This method returns the list of Advocate complaint to the DTO with respect to advocacy Case ID
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>
        public List<AdvocateAssociatedComplaintDTO> LoadAssociatedComplaints(Int64 advocacyCaseId)
        {
            List<AdvocateAssociatedComplaintDTO> advAssociatedComplaint = new List<AdvocateAssociatedComplaintDTO>();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
              DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocateAssociatedComplaintList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                AdvocateMapper complaintMapper = new AdvocateMapper();
                DataSet dsDSNYCP = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                //advAssociatedComplaint = complaintMapper.AdvocateAssociatedComplaint(dsDSNYCP);
                advAssociatedComplaint = complaintMapper.AdvocateAssociatedComplaint(dsDSNYCP);
                return advAssociatedComplaint;           
        }
        public List<AdvocateHistoryDto> LoadAssociatedHistory(Int64 employeeId, Int64 complaintId)
        {
            List<AdvocateHistoryDto> advAssociatedHistory = new List<AdvocateHistoryDto>();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocateHistory);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmpID", DbType.Int64, employeeId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                AdvocateMapper AdvHistoryMapper = new AdvocateMapper();
                DataSet dsDSNYCP = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                advAssociatedHistory = AdvHistoryMapper.AdvocateAssociatedHistory(dsDSNYCP);
                return advAssociatedHistory;            
        }
        /// <summary>
        /// This method returns the list of Advocate Hearing to the DTO with respect to advocacy Case ID
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>        
        public List<AdvocateHearingDTO> LoadAdvocateHearingList(Int64 advocacyCaseId)
        {
            List<AdvocateHearingDTO> advHearing = new List<AdvocateHearingDTO>();
            AdvocateMapper advMapper = new AdvocateMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocateHearingList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                DataSet dsDSNYCP = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                advHearing = advMapper.AdvocateHearing(dsDSNYCP);
                return advHearing;            
        }
        /// <summary>
        /// This method returns the list of Advocate Appeal to the DTO with respect to advocacy Case ID
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>        
        public List<AdvocateAppealDTO> LoadAdvocateAppealList(Int64 advocacyCaseId)
        {
            List<AdvocateAppealDTO> advAppeal = new List<AdvocateAppealDTO>();
            AdvocateMapper advMapper = new AdvocateMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocateAppealList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                DataSet dsDSNYCP = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                advAppeal = advMapper.AdvocateAppeal(dsDSNYCP);
                return advAppeal;            
        }
        /// <summary>
        /// This method returns the list of Advocate Appeal to the DTO with respect to advocacy Case ID
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>        
        public List<AdvocatePenaltyDTO> LoadAdvocatePenaltyList(Int64 advocacyCaseId)
        {
            List<AdvocatePenaltyDTO> advPenalty = new List<AdvocatePenaltyDTO>();
            AdvocateMapper advMapper = new AdvocateMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAdvocatePenaltyList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                DataSet dsDSNYCP = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                advPenalty = advMapper.AdvocatePenalty(dsDSNYCP);
                return advPenalty;           
        }
        /// <summary>
        /// This method returns the list of Advocate Appeal to the DTO with respect to advocacy Case ID
        /// </summary>
        /// <param name="advocacyCaseID"></param>
        /// <returns></returns>       
        public List<CombinedPenaltyDTO> LoadCombinedPenaltyList(Int64 advocacyCaseId)
        {
            List<CombinedPenaltyDTO> combPenalty = new List<CombinedPenaltyDTO>();
            AdvocateMapper advMapper = new AdvocateMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
              DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetCombinedPenaltyList);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                DataSet dsDSNYCP = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                combPenalty = advMapper.CombinedPenalty(dsDSNYCP);
                return combPenalty;           
        }
        /// <summary>
        /// This method returns true if the Advocate complaints are saved sucessfully with respect to advocacy Case ID
        /// </summary>
        /// <param name="advocateDTO"></param>
        /// <returns></returns>        
        public bool SaveAdvocateComplaint(ComplaintAdvocacyDTO DTO)
        {

            Database db = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
               
                    SaveAdvocateComplaint(DTO.AdvocacyCaseId, DTO.ComplaintId,
                                          DTO.SpecialStatus_cd, DTO.TrialReturnStatuscd,
                                          DTO.TrialReturnDate, DTO.ReviewStatuscd,
                                          DTO.ReviewStatusDate, DTO.Actioncd,
                                          DTO.ActionDate, DTO.ClosedDate, DTO.COBStatus_cd,
                                          DTO.COBDate, DTO.FinalStatuscd, DTO.CaseComment,
                                          DTO.ModifiedUser, DTO.ModifiedDate, db, transaction);
                    if (DTO.AssociatedComplaints.Count > 0)
                    {
                        Int16 cnt = 1;
                        foreach (AdvocateAssociatedComplaintDTO assComplaint in DTO.AssociatedComplaints)
                        {
                            assComplaint.AdvocacyCaseID = DTO.AdvocacyCaseId;
                            SaveAssociatedComplaints(assComplaint.AdvocacyCaseID, assComplaint.AsscociatedCompaintID, cnt, db, transaction);
                            cnt++;
                        }
                    }
                    if (DTO.Hearings.Count > 0)
                    {
                        Int16 cnt = 1;
                        foreach (AdvocateHearingDTO hearing in DTO.Hearings)
                        {
                            hearing.AdvocacyCaseID = DTO.AdvocacyCaseId;
                            hearing.ModifiedBy = DTO.ModifiedUser;
                            SaveHearings(hearing.AdvocacyCaseID, hearing.HearingDt, hearing.HearingStatusCd,
                                        hearing.CalStatusCd, hearing.HearingTypeCd, hearing.HearingComments,
                                        hearing.ModifiedBy, cnt, db, transaction);
                            cnt++;
                        }
                    }
                    if (DTO.Penaltys.Count > 0)
                    {
                        Int16 cnt = 1;
                        foreach (AdvocatePenaltyDTO penalty in DTO.Penaltys)
                        {
                            penalty.AdvocacyCaseID = DTO.AdvocacyCaseId;
                            SavePenalty(penalty.AdvocacyCaseID, penalty.PenaltyTypeCd, penalty.DataValue, penalty.SuspensionDays,
                                //penalty.DemotedRank, penalty.ACD, penalty.VacDays, penalty.CombPen, penalty.RandomTest, penalty.TimeServed,
                                        penalty.FineDays, penalty.ApplyToTotal, penalty.Term, penalty.Term_Desc,
                                        penalty.Comments, cnt, db, transaction);
                            cnt++;
                        }
                    }

                    if (DTO.Appeals.Count > 0)
                    {
                        Int16 cnt = 1;
                        foreach (AdvocateAppealDTO appeal in DTO.Appeals)
                        {
                            appeal.AdvocacyCaseID = DTO.AdvocacyCaseId;
                            appeal.ModifiedBy = DTO.ModifiedUser;
                            SaveAppeal(appeal.AdvocacyCaseID, appeal.AppealDt, appeal.AppealresultCd, appeal.AppealComments, appeal.AppealCd, appeal.ModifiedBy, cnt, db, transaction);
                            cnt++;
                        }
                    }
                    if (DTO.Charges.Count > 0)
                    {
                        foreach (ViolationDTO violation in DTO.Charges)
                        {
                            UpdateViolation(violation.ChargeID, violation.ComplaintID, violation.OverideCharge, violation.PrimaryIndicator, db, transaction);
                        }
                    }
                    transaction.Commit();
                }               
                return true;            
        }

        
        /// <summary>
        /// This method gets the list of Updated Violation with respect to ChargeID,complaint id,OverideCharge
        /// PrimaryIndicator,sqlDatabase and transaction oblect
        /// </summary>
        /// <param name="ChargeID"></param>
        /// <param name="ComplaintID"></param>
        /// <param name="OverideCharge"></param>
        /// <param name="PrimaryIndicator"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>       
        private void UpdateViolation(Int64 chargeId, Int64 complaintId, String overideCharge,
                                        Boolean primaryIndicator, Database sqlDatabase, DbTransaction transaction)
        {
            DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.UpdateCharges);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@ChargeID", DbType.Int64, chargeId);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@OverideCharge", DbType.String, overideCharge);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@PrimaryIndicator", DbType.Boolean, primaryIndicator);
            sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);
        }
        /// <summary>
        /// This method saves the list of Associated complaint with respect to below parameters
        /// </summary>
        /// <param name="AdvocacyCaseID"></param>
        /// <param name="AssociatedComplaintID"></param>
        /// <param name="Cnt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        private void SaveAssociatedComplaints(Int64 advocacyCaseId, Int64 associatedComplaintId, Int16 count,
                                                Database sqlDatabase, DbTransaction transaction)
        {           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveAssociatedComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AsscociatedComplaintID", DbType.Int64, associatedComplaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);           
        }
        /// <summary>
        /// This method saves the list of complaint hearing with respect to below parameters
        /// </summary>
        /// <param name="AdvocacyCaseID"></param>
        /// <param name="HearingDt"></param>
        /// <param name="HearingStatusCd"></param>
        /// <param name="CalStatusCd"></param>
        /// <param name="HearingTypeCd"></param>
        /// <param name="HearingComments"></param>
        /// <param name="ModifiedBy"></param>
        /// <param name="Cnt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>        
        private void SaveHearings(Int64 advocacyCaseId, DateTime? hearingDate, Int64 hearingStatusCd,
                                    Int64 calculateStatusCd, Int64 hearingTypeCd, String hearingComments,
                                    Int64 modifiedBy, Int16 count, Database sqlDatabase, DbTransaction transaction)
        {           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveAdvocateHearings);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                if (hearingDate != DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@HearingDt", DbType.DateTime, hearingDate);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@HearingDt", DbType.DateTime, DBNull.Value);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@HearingStatusCd", DbType.Int64, hearingStatusCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CalStatusCd", DbType.Int64, calculateStatusCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@HearingTypeCd", DbType.Int64, hearingTypeCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@HearingComments", DbType.String, hearingComments);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ModifiedBy", DbType.Int16, modifiedBy);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);            
        }
        /// <summary>
        /// This method saves the list of penalties with respect to below parameters
        /// </summary>
        /// <param name="AdvocacyCaseID"></param>
        /// <param name="PaneltyTypeCd"></param>
        /// <param name="DataValue"></param>
        /// <param name="SuspensionDays"></param>
        /// <param name="FineDays"></param>
        /// <param name="ApplyToTotal"></param>
        /// <param name="Comments"></param>
        /// <param name="Cnt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>        
        private void SavePenalty(Int64 advocacyCaseId, Int64 paneltyTypeCd, String dataValue, Int16 suspensionDays,
            //String DemotedRank, String ACD, Int16 VacDays, Boolean CombPen, Boolean RandomTest, Int16 TimeServered,
                                    Int16 fineDays, Boolean applyToTotal, Int16 term, string term_desc,
                                    String comments, Int16 count, Database sqlDatabase, DbTransaction transaction)
        {
            
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveAdvocatePenalty);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@penaltyTypeCd", DbType.Int64, paneltyTypeCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SuspensionDays", DbType.Int16, suspensionDays);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@DataValue", DbType.String, dataValue);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FineDays", DbType.Int16, fineDays);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplyToTotal", DbType.Boolean, applyToTotal);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Term", DbType.Int16, term);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Term_Desc", DbType.String, term_desc);                
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Comments", DbType.String, comments);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);
            
            
        }
        /// <summary>
        /// This method saves the list of appeals with respect to below parameters
        /// </summary>
        /// <param name="AdvocacyCaseID"></param>
        /// <param name="AppealDt"></param>
        /// <param name="AppealResultCd"></param>
        /// <param name="AppealComments"></param>
        /// <param name="AppealCd"></param>
        /// <param name="ModifiedBy"></param>
        /// <param name="Cnt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>        
        private void SaveAppeal(Int64 advocacyCaseId, DateTime? appealDate, Int64 appealResultCd,
                                String appealComments, Int64 appealCd, Int64 modifiedBy, Int16 count, Database sqlDatabase, DbTransaction transaction)
        {           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveAdvocateAppeal);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                if (appealDate != DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AppealDt", DbType.DateTime, appealDate);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@AppealDt", DbType.DateTime, DBNull.Value);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AppealResultCd", DbType.Int64, appealResultCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AppealComments", DbType.String, appealComments);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AppealCd", DbType.Int64, appealCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ModifiedBy", DbType.Int16, modifiedBy);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Cnt", DbType.Int16, count);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);          
        }
        /// <summary>
        /// This method saves the list of Advocate Complaint  with respect to below parameters
        /// </summary>
        /// <param name="AdvocacyCaseID"></param>
        /// <param name="ComplaintID"></param>
        /// <param name="SpecialStatusCd"></param>
        /// <param name="TrailReturnStatusCd"></param>
        /// <param name="TrailReturnDt"></param>
        /// <param name="ReviewStatudCd"></param>
        /// <param name="ReviewStatusDt"></param>
        /// <param name="ActionCd"></param>
        /// <param name="ActionDt"></param>
        /// <param name="ClosedDt"></param>
        /// <param name="COBStatusCd"></param>
        /// <param name="COBDate"></param>
        /// <param name="FinalStatusCd"></param>
        /// <param name="CaseComments"></param>
        /// <param name="ModifiedBy"></param>
        /// <param name="ModifiedDt"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>        
        private void SaveAdvocateComplaint(Int64 advocacyCaseId, Int64 complaintId, Int64 specialStatusCd,
                                            Int64 trailReturnStatusCd, DateTime trailReturnDate, Int64 reviewStatudCd,
                                            DateTime reviewStatusDate, Int64 actionCd, DateTime? actionDate, DateTime? closedDate,
                                            Int64 cobStatusCd, DateTime? cobDate, Int64 finalStatusCd, String caseComments,
                                            Int64 modifiedBy, DateTime modifiedDate, Database sqlDatabase, DbTransaction transaction)
        {
           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveAdvocateComplaint);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AdvocacyCaseID", DbType.Int64, advocacyCaseId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ComplaintID", DbType.Int64, complaintId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SpecialStatus_cd", DbType.Int64, specialStatusCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@TrialReturnStatuscd", DbType.Int64, trailReturnStatusCd);
                if (trailReturnDate != DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@TrialReturnDate", DbType.DateTime, trailReturnDate);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@TrialReturnDate", DbType.DateTime, DBNull.Value);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ReviewStatuscd", DbType.Int64, reviewStatudCd);
                if (reviewStatusDate != DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReviewStatusDate", DbType.DateTime, reviewStatusDate);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ReviewStatusDate", DbType.DateTime, DBNull.Value);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Actioncd", DbType.Int64, actionCd);
                if (actionDate != DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ActionDate", DbType.DateTime, actionDate);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ActionDate", DbType.DateTime, DBNull.Value);
                if (closedDate != DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ClosedDate", DbType.DateTime, closedDate);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@ClosedDate", DbType.DateTime, DBNull.Value);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@COBStatus_cd", DbType.Int64, cobStatusCd);
                if (cobDate != DateTime.MinValue)
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@COBDate", DbType.DateTime, cobDate);
                else
                    sqlDatabase.AddInParameter(DSNYCPCmd, "@COBDate", DbType.DateTime, DBNull.Value);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@FinalStatuscd", DbType.Int16, finalStatusCd);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@CaseComment", DbType.String, caseComments);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ModifiedUser", DbType.Int16, modifiedBy);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);            
           
        }

        #endregion

        #region Employee info
        /// <summary>
        /// This method gets the list of Employee information to the DTO with respect to refernce ID 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>        
        public List<EmployeeInfoDTO> LoadEmployeeInfo(Int64 id)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
           
                List<EmployeeInfoDTO> employeeInfoDTO = new List<EmployeeInfoDTO>();
                ComplaintDS249Mapper complaintMapper = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetEmployeeInfo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ReferenceNo", DbType.Int64, id);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                employeeInfoDTO = complaintMapper.EmployeesInfoMapper(DSNYCPdr);
                return employeeInfoDTO;
                      
        }

        /// <summary>
        /// This method gets the list of Employee information
        /// </summary>
        /// <returns></returns>        
        public List<EmployeeListDTO> LoadEmployeeList()
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<EmployeeListDTO> employeeListDTO = new List<EmployeeListDTO>();
            ComplaintDS249Mapper complaintMapper = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.LoadEmployeeList);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                employeeListDTO = complaintMapper.EmployeesListMapper(DSNYCPdr);
                return employeeListDTO;
        
        }
        /// <summary>
        /// This method gets the list of Employee images to the DTO with respect to refernce ID  
        /// and employee images data in bytes
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="EmployeeImage"></param>
        /// <returns></returns>        
        public List<PersonnelByImageDTO> LoadEmployeeImage(Int32 id, Byte[] employeeImage)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                List<PersonnelByImageDTO> personnelImageDTO = new List<PersonnelByImageDTO>();
                ComplaintDS249Mapper complaintMapper = new ComplaintDS249Mapper();
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetEmployeeImage);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ImageID", DbType.Int64, id);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Images", DbType.Binary, employeeImage);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                personnelImageDTO = complaintMapper.PersonnelByImageMapper(DSNYCPdr);
                return personnelImageDTO;
                      
        }

        #endregion

        #region Security
        /// <summary>
        /// This method returns true if the user is authenticated with respect to below parameters
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <param name="isTempPassword"></param>
        /// <param name="isAdmin"></param>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <returns></returns>
        public bool AuthenticateUser(ref Int64 personalId, ref Boolean isTempPassword,
                                       ref Boolean isAdmin, String userId, String password)
        {            
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.AuthenticateUser);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@user", DbType.String, userId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pwd", DbType.String, password);
                //PersonalID = Convert.ToInt64(sqlDatabase.ExecuteReader(DSNYCPCmd));
                IDataReader DSNYCPdr = sqlDatabase.ExecuteReader(DSNYCPCmd);
                SetUserAuthenticationDetails(DSNYCPdr, ref personalId, ref isTempPassword, ref isAdmin);
                return true;           
        }
        /// <summary>
        /// This method gets the list of Authenticated User to the DTO with respect to user id and password.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <returns></returns>        
        public List<UserDTO> AuthenticateUser(String userId, String password)
        {
            List<UserDTO> userDTO = new List<UserDTO>();
            SecurityMapper uMapper = new SecurityMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.AuthenticateUser);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@user", DbType.String, userId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pwd", DbType.String, password);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                userDTO = uMapper.UserListMapper(DSNYCPdr);
                return userDTO;           
           
        }


          /// <summary>
        /// This method gets the list of Authenticated Users roles to the DTO with respect to user id and password.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
          /// <param name="appname"></param>
        /// <returns></returns>        
        public List<UserDTO> AuthenticatedUserForApplication(String userId, String password, Enum appname)
        {
            List<UserDTO> userDTO = new List<UserDTO>();
            SecurityMapper uMapper = new SecurityMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.AuthenticatedUserForApplication);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@user", DbType.String, userId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pwd", DbType.String, password);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@appname", DbType.String, appname);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                userDTO = uMapper.UserRolesListMapper(DSNYCPdr);
                return userDTO;
           
            
        }
        /// <summary>
        /// This method gets the list of members to the DTO with respect to personnal id
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>        
        public List<MembershipDTO> GetMembership(Int64 personalId)
        {
            List<MembershipDTO> memberDTO = new List<MembershipDTO>();
            SecurityMapper mMapper = new SecurityMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetUserMembership);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonalID", DbType.Int64, personalId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                memberDTO = mMapper.GetMembershipMapper(DSNYCPdr);
                return memberDTO;            
        }
        /// <summary>
        /// This method gets the list of members to the DTO
        /// </summary>
        /// <returns></returns>        
        public List<MembershipDTO> GetMembership()
        {
            List<MembershipDTO> memberDTO = new List<MembershipDTO>();
            SecurityMapper mMapper = new SecurityMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetMembershipList);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                memberDTO = mMapper.GetMembersMapper(DSNYCPdr);
                return memberDTO;           
        }
        /// <summary>
        /// This method gets the list of members roles to the DTO with respect to membershipid and personnal id
        /// </summary>
        /// <param name="MembershipID"></param>
        /// <param name="PersonalID"></param>
        /// <returns></returns>       
        public List<MembershipDTO> GetMembershipRoles(Int64 membershipId, Int64 personalId)
        {
            List<MembershipDTO> membershipDTO = new List<MembershipDTO>();
            SecurityMapper mMapper = new SecurityMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetMembershipRoles);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MembershipID", DbType.String, membershipId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonalID", DbType.String, personalId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                membershipDTO = mMapper.MembershipListMapper(DSNYCPdr);
                return membershipDTO;
           
        }
        /// <summary>
        /// This method gets the list of members roles to the DTO
        /// </summary>
        /// <returns></returns>        
        public List<RoleDTO> GetRoles(int membershipID)
        {
            List<RoleDTO> RoleDTO = new List<RoleDTO>();
            SecurityMapper uMapper = new SecurityMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetRoles);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MembershipID", DbType.Int32, membershipID);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                RoleDTO = uMapper.RoleListMapper(DSNYCPdr);
                return RoleDTO;                      
        }
        /// <summary>
        /// This method provides the list of authenticated user details with respect to below parameters
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="PersonalID"></param>
        /// <param name="isTempPassword"></param>
        /// <param name="isAdmin"></param>       
        private void SetUserAuthenticationDetails(IDataReader dr, ref Int64 personalID, ref Boolean isTempPassword, ref Boolean isAdmin)
        {
            while (dr.Read())
            {
                if (dr["PersonalID"] != DBNull.Value)
                    personalID = Convert.ToInt64(dr["PersonalID"]);
                if (dr["isTempPassword"] != DBNull.Value)
                    isTempPassword = Convert.ToBoolean(dr["isTempPassword"]);
                if (dr["IsAdmin"] != DBNull.Value)
                    isAdmin = Convert.ToBoolean(dr["IsAdmin"]);
            }
        }
        /// <summary>
        /// This method provides a string with email and admin
        /// </summary>
        /// <param name="emailID"></param>
        /// <param name="isAdmin"></param>
        /// <returns></returns>        
        public String ValidateUserEmail(String emailId, ref Boolean isAdmin)
        {
            String userID = String.Empty;           
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ValidateUserEmail);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmailID", DbType.String, emailId);
                IDataReader DSNYCPdr = sqlDatabase.ExecuteReader(DSNYCPCmd);
                while (DSNYCPdr.Read())
                {
                    if (DSNYCPdr["UserID"] != DBNull.Value)
                        userID = Convert.ToString(DSNYCPdr["UserID"]);
                    if (DSNYCPdr["IsAdmin"] != DBNull.Value)
                        isAdmin = Convert.ToBoolean(DSNYCPdr["IsAdmin"]);
                }
                return userID;          
        }

        #endregion

        #region ADMIN
        /// <summary>
        /// This method gets the list of all personnels to the DTO with respect to the search criteria
        /// </summary>
        /// <param name="searchString"></param>
        /// <returns></returns>        
        public List<PersonnelDTO> GetAllPersonnel(String searchValue)
        {
            List<PersonnelDTO> personDTO = new List<PersonnelDTO>();
            SecurityMapper pMapper = new SecurityMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAllPersonals);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@SearchString", DbType.String, searchValue);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                personDTO = pMapper.PersonnelListMapper(DSNYCPdr);
                return personDTO;            
        }
        /// <summary>
        /// This method gets the list of allpay code list to the DTO 
        /// </summary>
        /// <returns></returns>        
        public List<PayCodeDTO> GetPayCodeList()
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            List<PayCodeDTO> payCodeDTO = new List<PayCodeDTO>();
            CommonClassMapper cMapper = new CommonClassMapper();           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetPayCodes);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                payCodeDTO = cMapper.PayCodeMapper(DSNYCPdr);
                return payCodeDTO;           
        }
        /// <summary>
        /// This method gets the list of Users to the DTO 
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <returns></returns>      
        public List<UserDTO> GetUser(Int64 personalId)
        {
            List<UserDTO> usrDTO = new List<UserDTO>();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            SecurityMapper securityMapper = new SecurityMapper();
             DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetUser);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonalID", DbType.Int64, personalId);
                DataSet dsDSNYCP = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                usrDTO = securityMapper.UserListMapper(dsDSNYCP);
                return usrDTO;            
        }
        /// <summary>
        /// This method saves the user name to a string
        /// </summary>
        /// <param name="User"></param>
        /// <returns></returns>       
        public String SaveUser(UserDTO user)
        {
            Database db = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
            String retVal = String.Empty;
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                try
                {
                    retVal = SaveUser( user.PersonalID, user.UserID, user.Password, user.IsTempPassword, user.EmailID, user.IsNew, user.IsAdmin, db, transaction );
                    if (user.UserMembership != null)
                    {
                        if (user.UserMembership.Count > 0)
                        {
                            Int16 k = 0;
                            foreach (MembershipDTO membership in user.UserMembership)
                            {
                                SaveUserMembership( membership.MembershipID, membership.PersonalID, k, db, transaction );

                                if (membership.Roles != null)
                                {
                                    if (membership.Roles.Count > 0)
                                    {
                                        Int16 i = 0;
                                        foreach (RoleDTO role in membership.Roles)
                                        {
                                            SaveMembershipRoles( role.MembershipID, role.PersonalID, role.RoleID, i, db, transaction );
                                            i++;
                                        }
                                    }
                                    else
                                    {
                                        //delete existing roles in database
                                        DeleteMembershipRoles( membership.MembershipID, membership.PersonalID, db, transaction );
                                    }

                                }
                                k++;
                            }
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
              
                return retVal;
            }

        }

        public bool HistoryMemberShipRoles(UserDTO user,Int64 userId)
        {

            Database db = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
            String retVal = String.Empty;
            using (DbConnection connection = db.CreateConnection())
            {
                connection.Open();
                DbTransaction transaction = connection.BeginTransaction();
                    if (user.UserMembership.Count == 0) 
                    {
                        SaveHistoryMembershipRoles(userId, 0,0, user.PersonalID, db, transaction);
                    }
                    if (user.UserMembership != null)
                    {
                        if (user.UserMembership.Count > 0)
                        {
                            Int16 k = 0;
                            foreach (MembershipDTO membership in user.UserMembership)
                            {
                   
                                if (membership.Roles != null)
                                {
                                    if (membership.Roles.Count > 0)
                                    {
                                        Int16 i = 0;
                                        foreach (RoleDTO role in membership.Roles)
                                        {
                                            long longRoleID = role.RoleID;
                                            SaveHistoryMembershipRoles(userId, role.MembershipID, longRoleID, role.PersonalID, db, transaction);
                                            i++;
                                        }
                                    }
                                }
                                k++;
                            }
                        }
                    }
                    transaction.Commit();
                
                return true;
            }
        }
        /// <summary>
        /// This method returns true if the User Membership are saved sucessfully with respect to below parameters
        /// </summary>
        /// <param name="MembershipID"></param>
        /// <param name="PersonalID"></param>
        /// <param name="k"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        private bool SaveUserMembership(Int64 membershipId, Int64 personalId, Int16 k, Database sqlDatabase, DbTransaction transaction)
        {            
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveUserMembership);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MembershipID", DbType.Int64, membershipId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@personalID", DbType.Int64, personalId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ID", DbType.Int16, k);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);
                return true;            
        }
        /// <summary>
        /// Given Store proc return the result 
        /// for the charges we want to hide.
        /// </summary>
        /// <returns></returns>
        public List<HideChargesDto> GetChargesTBLHideCharges(Int16 employeeId)
        {
            List<HideChargesDto> hideChargedDto = new List<HideChargesDto>();
            SecurityMapper uMapper = new SecurityMapper();
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetHideCharges);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmployeeId", DbType.Int64, employeeId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                hideChargedDto = uMapper.GetHideCharges(DSNYCPdr);
                return hideChargedDto;           
        }
        /// <summary>
        /// Save the charges which are not to be exposed to 
        /// every user but to particular people whose id will be stored
        /// </summary>
        /// <param name="referenceNo"></param>
        /// <param name="employeeId"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public bool SaveHideCharges(String referenceNo, Int64 employeeId)
        {
                 SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveHideCharges);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RefNo", DbType.String, referenceNo);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmployeeId", DbType.Int64, employeeId);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }
        /// <summary>
        /// Delete Hide Charges from table
        /// </summary>
        /// <param name="referenceNumber"></param>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        public bool DeleteHideCharges(Int16 viewChargesId)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.DeleteHideCharges);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@viewChargesId", DbType.Int32, viewChargesId);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }
        /// <summary>
        /// This method returns true if the User Membership Roles are saved sucessfully with respect to below parameters
        /// </summary>
        /// <param name="MembershipID"></param>
        /// <param name="PersonalID"></param>
        /// <param name="RoleID"></param>
        /// <param name="i"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>        
        private void SaveMembershipRoles(Int64 membershipId, Int64 personalId, Int64 roleId, Int16 i, Database sqlDatabase, DbTransaction transaction)
        {            
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveMembershipRoles);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MembershipID", DbType.Int64, membershipId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@personalID", DbType.Int64, personalId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RoleId", DbType.Int64, roleId); // 16 --> 64 by  Farkas 27Feb2020
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ID", DbType.Int16, i);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);              
        }

        private void DeleteMembershipRoles(Int64 membershipId, Int64 personalId, Database sqlDatabase, DbTransaction transaction)
        {
            DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.DeleteMembershipRoles);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@MembershipID", DbType.Int64, membershipId);
            sqlDatabase.AddInParameter(DSNYCPCmd, "@personalID", DbType.Int64, personalId);
            sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);
        }

        private void SaveHistoryMembershipRoles( Int64 userId,Int64 membershipId, Int64 roleId, Int64 personalId,Database sqlDatabase, DbTransaction transaction)
        {
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.HistoryMembershipRole);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@UserID", DbType.Int64, userId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@MembershipID", DbType.Int64, membershipId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RoleId", DbType.Int64, roleId); // 16  --> 64 by Farkas 27FEb2020
                sqlDatabase.AddInParameter(DSNYCPCmd, "@personalID", DbType.Int64, personalId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@date", DbType.DateTime, System.DateTime.Now);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd, transaction);           
        }
        /// <summary>
        /// This method saves the saved user name to a string
        /// </summary>
        /// <param name="PersonalID"></param>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <param name="isTempPwd"></param>
        /// <param name="emailID"></param>
        /// <param name="isNew"></param>
        /// <param name="isAdmin"></param>
        /// <param name="sqlDatabase"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>        
        public String SaveUser(Int64 personalId, String userId, String password,
            Boolean isTemp, String emailId, Boolean isNew, Boolean isAdmin
            , Database sqlDatabase, DbTransaction transaction)
        {          
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveUser);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@personalID", DbType.Int64, personalId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@user", DbType.String, userId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pwd", DbType.String, password);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@isTempPassword", DbType.Boolean, isTemp);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@EmailID", DbType.String, emailId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@IsNewUser", DbType.Boolean, isNew);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@IsAdmin", DbType.Boolean, isAdmin);
                return Convert.ToString(sqlDatabase.ExecuteScalar(DSNYCPCmd, transaction));           
        }
        /// <summary>
        /// This method returns true if the rest passsword is saved sucessfully with respect to UserID, pwd, temp password.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="Password"></param>
        /// <param name="isTempPwd"></param>
        /// <returns></returns>        
        public bool ResetPassword(String userId, String password, Boolean isTempPassword)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.ResetPassword);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@user", DbType.String, userId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@pwd", DbType.String, password);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@isTempPassword", DbType.Boolean, isTempPassword);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;            
        }
        /// <summary>
        /// This method returns true if the Deleted user is saved sucessfully with respect to personID
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>        
        public bool DeleteUser(Int64 personId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.DeleteUser);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonID", DbType.Int64, personId);
                sqlDatabase.ExecuteNonQuery(DSNYCPCmd);
                return true;           
        }
        /// <summary>
        /// This method saves new user name to a string with respect to personID
        /// </summary>
        /// <param name="personID"></param>
        /// <returns></returns>        
        public String GetNewUserId(Int64 personId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetNewUserId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@PersonalID", DbType.Int64, personId);
                return Convert.ToString(sqlDatabase.ExecuteScalar(DSNYCPCmd));            
            
        }
        #endregion
    }

}