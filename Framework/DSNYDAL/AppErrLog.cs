﻿using DT = DSNY.DSNYCP.DTO;
using System.Data.SqlClient;
using System.Data.Common;
using System.Data;
using System;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using DSNY.DSNYCP.Mapper;

namespace DSNY.DSNYCP.DAL
{
    public class AppErrLog
    {
        public String LogError(DT.AppErrorDTO appErr)
        {
            SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
            
               
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveError);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@Url", DbType.String, appErr.Url);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@RequestType", DbType.Int16, appErr.RequestType);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@UserID", DbType.String, appErr.UserID);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ErrorMessage", DbType.String, appErr.ErrorMessage);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@StackTrace", DbType.String, appErr.StackTrace);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@AppID", DbType.String, appErr.ErrorID);
                return Convert.ToString(sqlDatabase.ExecuteScalar(DSNYCPCmd));
                 
        }

    }

    
}
