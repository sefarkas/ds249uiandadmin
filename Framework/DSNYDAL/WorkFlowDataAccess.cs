﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Web;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using DSNY.DSNYCP.DTO;
namespace DSNY.DSNYCP.DAL
{
    /// <summary>
    /// This class provides the functionality for set of methods accessed by WorkFlow application
    /// </summary>
    public class WorkFlowDataAccess
    {
        /// <summary>
        /// This method gets the list of Map Workflow from the dataset with respect to userlist DTO
        /// </summary>
        /// <param name="userList"></param>
        /// <returns></returns>
         public List<WorkFlowUser> MapWorkflowDataSet(DataSet userList)
         {
            List<WorkFlowUser> userLi= new List<WorkFlowUser>();
            WorkFlowUser workflowUser;
            DataTable dtWorkflowUser = userList.Tables[0];
             for(int i=0;i<=dtWorkflowUser.Rows.Count-1;i++)
            {
                workflowUser = new WorkFlowUser();
                workflowUser.Email = Convert.ToString(dtWorkflowUser.Rows[i]["Email"]);
                workflowUser.UserId = Convert.ToString(dtWorkflowUser.Rows[i]["NTUserId"]);
                workflowUser.FirstName = Convert.ToString(dtWorkflowUser.Rows[i]["FirstName"]);
                workflowUser.LastName = Convert.ToString(dtWorkflowUser.Rows[i]["LastName"]);
                userLi.Add(workflowUser);   	 
            }
             return userLi;
        }
        /// <summary>
        /// This method gets the config value by string in the form of string
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
         public string GetConfigValueByKey(string key)
         {
             SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetConfigKey);
                 sqlDatabase.AddInParameter(DSNYCPCmd, "@Key", DbType.String, key);                 
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 DataTable dt = DSNYCPdr.Tables[0];
                 string retVal =string.Empty ;
                 if (dt.Rows[0][0] != System.DBNull.Value)
                 {
                     retVal = dt.Rows[0][0].ToString();
                 }
                 return retVal;                         
         }
        /// <summary>
         /// This method gets the list of Workflow Application DTO
        /// </summary>
        /// <returns></returns>
         public List<WorkflowApplicationDTO> GetAllApplications()
         {
                 SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());             
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAllApplications);
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 return MapWorkflowApplicationDataSet(DSNYCPdr);
            
         }
        /// <summary>
         /// This method gets the list of Workflow Application DTO to the dataset
        /// </summary>
        /// <param name="DSNYCPdr"></param>
        /// <returns></returns>
         private List<WorkflowApplicationDTO> MapWorkflowApplicationDataSet(DataSet DSNYCPdr)
         {
             List<WorkflowApplicationDTO> userLi = new List<WorkflowApplicationDTO>();
             WorkflowApplicationDTO workflowApplication;
             DataTable dtWorkflowUser = DSNYCPdr.Tables[0];
             for (int i = 0; i <= dtWorkflowUser.Rows.Count - 1; i++)
             {
                 workflowApplication = new WorkflowApplicationDTO();
                 workflowApplication.ApplicationId = Convert.ToInt32(dtWorkflowUser.Rows[i]["ApplicationId"]);
                 workflowApplication.ApplicationName  = Convert.ToString(dtWorkflowUser.Rows[i]["ApplicationName"]);
                 userLi.Add(workflowApplication);
             }
             return userLi;
         }

        /// <summary>
         /// This method gets all User By GroupID and Application ID w.r.t GroupID and Application ID 
        /// </summary>
        /// <param name="GroupId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
         public List<WorkFlowUser> GetAllUserByGroupApplication(int groupId, int applicationId)
        {
                SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());          
                DbCommand DSNYCPCmd;
                DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAllUserByGroupIdApplicationId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@GroupID", DbType.Int32, groupId);
                sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationId", DbType.Int32, applicationId);
                DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                return MapWorkflowDataSet(DSNYCPdr);        
            
        }
        /// <summary>
         /// This method gets all Total Approval Levels w.r.t GroupID and Application ID 
        /// </summary>
        /// <param name="GroupId"></param>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
         public int GetTotalApprovalLevels(int groupId, int applicationId)
         {
             SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetNextGroup);
                 sqlDatabase.AddInParameter(DSNYCPCmd, "@GroupID", DbType.Int32, groupId);
                 sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationId", DbType.Int32, applicationId);
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 DataTable dt = DSNYCPdr.Tables[0];
                 int retVal=0;
                 if (dt.Rows.Count > 0)
                 {
                     if (dt.Rows[0][0] != System.DBNull.Value)
                     {
                         retVal = Convert.ToInt32(dt.Rows[0][0]);
                     }
                 }
                 return retVal;                        
         }
        /// <summary>
        /// This method returns the int is the application is succesfully saved, based on the application name.
        /// </summary>
        /// <param name="ApplicationName"></param>
        /// <returns></returns>
         public int SaveApplication(string applicationName)
         {
             int applicationID = 0;
             Database sqlDatabase = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
             using (DbConnection connection = sqlDatabase.CreateConnection())
             {                
                     DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveWorkflowApplication);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationName", DbType.String, applicationName);
                     applicationID = Convert.ToInt32(sqlDatabase.ExecuteScalar(DSNYCPCmd));                
             }
             return applicationID;
             
         }

        /// <summary>
         /// This method returns the int is the application is succesfully saved w.r.t groupId, parent ID and Application ID 
        /// </summary>
        /// <param name="GroupName"></param>
        /// <param name="ApplicationId"></param>
        /// <param name="parentGroupId"></param>
        /// <returns></returns>
         public int SaveGroup(string groupName,int applicationId,int parentGroupId)
         {
             int groupId = 0;
             Database sqlDatabase = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
             using (DbConnection connection = sqlDatabase.CreateConnection())
             {
                     DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveWorkflowGroup);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@GroupName", DbType.String, groupName);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationId", DbType.Int32, applicationId);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@parentGroupId", DbType.Int32, parentGroupId);
                     groupId = Convert.ToInt32(sqlDatabase.ExecuteScalar(DSNYCPCmd));            
             }
             return groupId;
         }
         /// <summary>
         /// This method returns the int is the application is succesfully saved w.r.t groupId, Username,LastName and email
         /// </summary>
         /// <param name="UserName"></param>
         /// <param name="GroupId"></param>
         /// <param name="LastName"></param>
         /// <param name="NTID"></param>
         /// <param name="Email"></param>
         /// <returns></returns>
         public int SaveUser(string userName, int groupId,string lastName,string ntId,string emailAddress)
         {
             int newGroupId = 0;
             Database sqlDatabase = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
             using (DbConnection connection = sqlDatabase.CreateConnection())
             {                 
                     DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveWorkflowUsers);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@UserId", DbType.String, ntId);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@FirstName", DbType.String, userName);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@LastName", DbType.String, lastName);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@Email", DbType.String, emailAddress);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@GroupId", DbType.Int32, newGroupId);                    
                     newGroupId = Convert.ToInt32(sqlDatabase.ExecuteScalar(DSNYCPCmd));               
             }
             return newGroupId;
         }
        /// <summary>
         /// This method returns true if the user is deleted sucessfully with respect to User ID
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
         public bool  DeleteUser(int userId)
         {            
             Database sqlDatabase = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
             using (DbConnection connection = sqlDatabase.CreateConnection())
             {                
                     DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveWorkflowGroup);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@UserId", DbType.String, userId);
                     sqlDatabase.ExecuteScalar(DSNYCPCmd);                 
             }
             return true;
         }
        /// <summary>
         /// This method returns true if the group is deleted sucessfully with respect to group ID
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
         public bool DeleteGroup(int groupId)
         {            
             Database sqlDatabase = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
             using (DbConnection connection = sqlDatabase.CreateConnection())
             {                
                     DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.DeleteWorkflowGroup);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@groupId", DbType.Int32, groupId);
                     groupId = Convert.ToInt32(sqlDatabase.ExecuteScalar(DSNYCPCmd));                
             }
             return true;
         }                  
         
        /// <summary>
         /// This method returns true if the Application is deleted sucessfully with respect to Application ID
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
         public bool DeleteApplication(int applicationId)
         {             
             Database sqlDatabase = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
             using (DbConnection connection = sqlDatabase.CreateConnection())
             {               
                     DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.DeleteWorkflowApplication);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationId", DbType.Int32, applicationId);
                     sqlDatabase.ExecuteScalar(DSNYCPCmd);                
             }
             return true;
         }

        /// <summary>
         /// This method returns true if the Workflow Details is saved sucessfully with respect to employeeVacationId and workflowID
        /// </summary>
        /// <param name="employeeVacationId"></param>
        /// <param name="workflowID"></param>
        /// <returns></returns>
         public bool SaveWorkflowDetails(int employeeVacationId, string workflowId)
         {
             int EmployeeVacationId;
             Database sqlDatabase = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
             using (DbConnection connection = sqlDatabase.CreateConnection())
             {                 
                     DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveEmployeeVacationWorkflow);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@EmployeeVacationInfoId", DbType.Int32, employeeVacationId);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@WorkflowId", DbType.Int32, workflowId);
                     EmployeeVacationId = Convert.ToInt32(sqlDatabase.ExecuteScalar(DSNYCPCmd));                
             }
             return true;
         }
        /// <summary>
         /// This method returns true if the Workflow Approval Details is saved sucessfully with respect to workflow 
        /// </summary>
        /// <param name="workflow"></param>
        /// <returns></returns>
         public bool SaveWorkflowApprovalDetails(WorkflowDTO workflow)
         {
             int Id;
             Database sqlDatabase = DatabaseFactory.CreateDatabase("DSNYCPConnectionString");
             using (DbConnection connection = sqlDatabase.CreateConnection())
             {                
                     DbCommand DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.SaveWorkflowDetails);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@WokflowId", DbType.String, workflow.WorkflowId.ToString());
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@GroupId", DbType.Int32, workflow.GroupId);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@StatusId", DbType.Int32, workflow.StatusId);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@Description", DbType.String , workflow.Description);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@ApprvedDate", DbType.DateTime, System.DateTime.Now);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@ApprovedBy", DbType.String, workflow.ApprovedBy);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@CreateDate", DbType.DateTime, workflow.CreateDate);
                     sqlDatabase.AddInParameter(DSNYCPCmd, "@UserID", DbType.String, workflow.ApprovedBy);
                     Id = Convert.ToInt32(sqlDatabase.ExecuteScalar(DSNYCPCmd));                
             }
             return true;
         }

        /// <summary>
         /// This method gets the list of Workflow Groups to the dataset
        /// </summary>
        /// <returns></returns>
         public List<WorkflowGroupsDTO> GetAllGroups()
         {
                 SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());             
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAllApplicationGroup);
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 return MapWorkflowGroupDataSet(DSNYCPdr);             
         }
        /// <summary>
         /// This method gets the list of Workflow Groups to the DTO w.r.t Application ID
        /// </summary>
        /// <param name="ApplicationId"></param>
        /// <returns></returns>
         public List<WorkflowGroupsDTO> GetAllGroups(int applicationId)
         {
                 SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());             
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAllApplicationGroup);
                 sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationId", DbType.Int32 , applicationId);
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 return MapWorkflowGroupDataSet(DSNYCPdr);            
         }
        /// <summary>
         /// This method gets the list of Map Workflow Groups to the DTO w.r.t Application ID
        /// </summary>
        /// <param name="DSNYCPdr"></param>
        /// <returns></returns>
         private List<WorkflowGroupsDTO> MapWorkflowGroupDataSet(DataSet DSNYCPdr)
         {
             List<WorkflowGroupsDTO> userLi = new List<WorkflowGroupsDTO>();
             WorkflowGroupsDTO workflowGroups;
             DataTable dtWorkflowUser = DSNYCPdr.Tables[0];
             for (int i = 0; i <= dtWorkflowUser.Rows.Count - 1; i++)
             {
                 workflowGroups = new WorkflowGroupsDTO();
                 workflowGroups.GroupId  = Convert.ToInt32(dtWorkflowUser.Rows[i]["GroupId"]);
                 workflowGroups.GroupName = Convert.ToString(dtWorkflowUser.Rows[i]["GroupName"]);
                 userLi.Add(workflowGroups);
             }
             return userLi;
         }
        /// <summary>
         /// This method gets the list of ApplicationGroupDetails to the DTO
        /// </summary>
        /// <returns></returns>
         public List<WorkflowGroupsDTO> GetApplicationGroupDetails()
         {
                 SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAllApplicationGroup);
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 return MapWorkflowGroupDetailDataSet(DSNYCPdr);            
         }

        /// <summary>
         /// This method gets the list of Map Workflow Group Details to the DTO
       /// </summary>
        /// <param name="DSNYCPdr"></param>
        /// <returns></returns>
         private List<WorkflowGroupsDTO> MapWorkflowGroupDetailDataSet(DataSet DSNYCPdr)
         {
             List<WorkflowGroupsDTO> userLi = new List<WorkflowGroupsDTO>();
             WorkflowGroupsDTO workflowGroupDetails;
             DataTable dtWorkflowUser = DSNYCPdr.Tables[0];

             for (int i = 0; i <= dtWorkflowUser.Rows.Count - 1; i++)
             {
                 workflowGroupDetails = new WorkflowGroupsDTO();
                 if (dtWorkflowUser.Rows[i]["GroupId"] != System.DBNull.Value)
                 {
                     workflowGroupDetails.GroupId = Convert.ToInt32(dtWorkflowUser.Rows[i]["GroupId"]);
                 }
                 if (dtWorkflowUser.Rows[i]["GroupName"] != System.DBNull.Value)
                 {
                     workflowGroupDetails.GroupName = Convert.ToString(dtWorkflowUser.Rows[i]["GroupName"]);
                 }
                 workflowGroupDetails.Application = new WorkflowApplicationDTO();
                 if (dtWorkflowUser.Rows[i]["ApplicationId"] != System.DBNull.Value)
                 {
                     workflowGroupDetails.Application.ApplicationId = Convert.ToInt32(dtWorkflowUser.Rows[i]["ApplicationId"]);
                 }
                 if (dtWorkflowUser.Rows[i]["ApplicationName"] != System.DBNull.Value)
                 {
                     workflowGroupDetails.Application.ApplicationName = Convert.ToString(dtWorkflowUser.Rows[i]["ApplicationName"]);
                 }
                 if (dtWorkflowUser.Rows[i]["ParentGroupName"] != System.DBNull.Value)
                 {
                     workflowGroupDetails.ParentGroupName = Convert.ToString(dtWorkflowUser.Rows[i]["ParentGroupName"]);
                 }
                 userLi.Add(workflowGroupDetails);
             }
             return userLi;
         }
         /// <summary>
         /// This method gets the list of User Details to the DTO
         /// </summary>
         /// <returns></returns>
         public List<WorkFlowUser> GetUserDetails()
         {
                 SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAllUserDetails);
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 return MapWorkflowUserDetailDataSet(DSNYCPdr);             
         }
        /// <summary>
         /// This method gets the list of Map Workflow User Detail to the DTO
        /// </summary>
        /// <param name="DSNYCPdr"></param>
        /// <returns></returns>
         private List<WorkFlowUser> MapWorkflowUserDetailDataSet(DataSet DSNYCPdr)
         {
             List<WorkFlowUser> userLi = new List<WorkFlowUser>();
             WorkFlowUser workflowUser;
             DataTable dtWorkflowUser = DSNYCPdr.Tables[0];
             for (int i = 0; i <= dtWorkflowUser.Rows.Count - 1; i++)
             {
                 workflowUser = new WorkFlowUser();
                 if (dtWorkflowUser.Rows[i]["FirstName"] != System.DBNull.Value)
                 {
                     workflowUser.FirstName = Convert.ToString(dtWorkflowUser.Rows[i]["FirstName"]);
                 }
                 if (dtWorkflowUser.Rows[i]["LastName"] != System.DBNull.Value)
                 {
                     workflowUser.LastName = Convert.ToString(dtWorkflowUser.Rows[i]["LastName"]);
                 }
                 workflowUser.Group = new WorkflowGroupsDTO();
                 if(dtWorkflowUser.Rows[i]["GroupName"]!=System.DBNull.Value)
                 {
                 workflowUser.Group.GroupName = Convert.ToString(dtWorkflowUser.Rows[i]["GroupName"]);
                 }
                 workflowUser.Group.Application = new WorkflowApplicationDTO();
                 if (dtWorkflowUser.Rows[i]["ApplicationName"] != System.DBNull.Value)
                 {
                     workflowUser.Group.Application.ApplicationName = Convert.ToString(dtWorkflowUser.Rows[i]["ApplicationName"]);
                 }
                 if (dtWorkflowUser.Rows[i]["NTUserId"] != System.DBNull.Value)
                 {
                     workflowUser.UserId = Convert.ToString(dtWorkflowUser.Rows[i]["NTUserId"]);
                 }
                 userLi.Add(workflowUser);
             }
             return userLi;
         }
        /// <summary>
         /// This method returns true if the final approval is saved sucessfully with respect to  GroupId and applicationID
        /// </summary>
        /// <param name="GroupId"></param>
        /// <param name="applicationID"></param>
        /// <returns></returns>
         public bool IsFinalApproval(int groupId,int applicationId)
         {
                 SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());           
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetNextGroup);
                 sqlDatabase.AddInParameter(DSNYCPCmd, "@GroupID", DbType.Int32, groupId);
                 sqlDatabase.AddInParameter(DSNYCPCmd, "@ApplicationId", DbType.Int32, applicationId);
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 DataTable dt = DSNYCPdr.Tables[0];
                 int retVal = -1;
                 if (dt.Rows.Count > 0)
                 {
                     if (dt.Rows[0][0] != System.DBNull.Value)
                     {
                         retVal = Convert.ToInt32(dt.Rows[0][0]);
                     }
                 }
                 if (retVal == 0)
                     return true;
                 else
                     return false;          
         }
        /// <summary>
         /// This method gets the list of Authorized Requests with respect to groipId and AppId to the DTO
        /// </summary>
        /// <param name="groipId"></param>
        /// <param name="AppId"></param>
        /// <returns></returns>
         public List<int> GetAuthorizedRequests(int groupId, int appId)
         {

                 SqlDatabase sqlDatabase = new SqlDatabase(DBConnection.GetConnection());            
                 DbCommand DSNYCPCmd;
                 DSNYCPCmd = sqlDatabase.GetStoredProcCommand(SPName.GetAllUserDetails);
                 DataSet DSNYCPdr = sqlDatabase.ExecuteDataSet(DSNYCPCmd);
                 List<int> dd= new List<int>();
                 return dd;             
         }
    }
}