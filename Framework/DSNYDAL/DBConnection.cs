﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;


namespace DSNY.DSNYCP.DAL
{
    /// <summary>
    /// This class provides functionality to fetches the connection string from the web.config file and 
    /// establishes a connection to the database
    /// </summary>
    public class DBConnection
    {
        /// <summary>
        /// This static method fetches the connection string from the web.config file and 
        /// establishes a connection to the database
        /// </summary>
        /// <returns></returns>
        public static string GetConnection()
        {                
                ConnectionStringSettingsCollection connections = ConfigurationManager.ConnectionStrings;
                String dsnyConn = string.Empty;
                foreach (ConnectionStringSettings connection in connections)
                {
                    if (connection.Name == "DSNYCPConnectionString")
                        dsnyConn = connection.ConnectionString;
                }
                return dsnyConn;
        }

    }
}
