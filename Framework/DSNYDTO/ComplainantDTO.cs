﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using DSNY.DSNYCP.Proxys;

namespace DSNY.DSNYCP.DTO
{
    public class ComplainantDTO
    {
        //Proxy proxy;

        public ComplainantDTO()
        {
            uniqueID = 0;
            complainantID = 0;
            complaintID = -1;
            employeeID = -1;
            complainantName = String.Empty;
            statement = String.Empty;
            complainantType = String.Empty;
            locationID = -1;
            titleID = -1;
            refNo = String.Empty;
            locationName = String.Empty;
            title = String.Empty;

            //Variables for Complaint Statement.
            onDate = DateTime.MinValue;
            text = String.Empty;
            thisDate = DateTime.MinValue;
            category = String.Empty;
            reportedDate = DateTime.MinValue;
            fromLocation = String.Empty;
            reportedTime = String.Empty;
            reportingTime = String.Empty;
            telephone = String.Empty;
        }
        
        #region Private Variables

        private Int16 uniqueID;
        private Int16 complainantID;
        private Int64 complaintID;
        private Int64 employeeID;
        private String complainantName;
        private String statement;
        private String complainantType;
        private Int64 locationID;
        private Int64 titleID;
        private String refNo;

        private String locationName;
        private String title;

        // Variables for Complaint Statement.
        private DateTime onDate;
        private string text;
        private DateTime thisDate;
        private String category;
        private DateTime reportedDate;
        private String fromLocation;
        private String reportedTime;
        private String reportingTime;
        private String telephone;

        #endregion

        #region Public Property

        public Int16 UniqueID
        {
            get { return uniqueID; }
            set { uniqueID = value; }
        }
        
        public Int16 ComplainantID
        {
            get {return complainantID; }
            set { complainantID = value; }
        }

        public Int64 ComplaintID
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int64 EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public String ComplainantName
        {
            get { return complainantName; }
            set { complainantName = value; }
        }

        public String Statement
        {
            get { return statement; }
            set { statement = value; }
        }

        public String ComplaintType
        {
            get { return complainantType; }
            set { complainantType = value; }
        }

        public Int64 LocationID
        {
            get { return locationID; }
            set { locationID = value; }
        }

        public String LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public Int64 TitleID
        {
            get { return titleID; }
            set { titleID = value; }
        }

        public String Title
        {
            get { return title; }
            set { title = value; }
        }

        public String RefNo
        {
            get { return refNo;}
            set{refNo =value;}
        }


        //Property for Complaint Statement.

        public DateTime OnDate
        {
            get { return onDate; }
            set { onDate = value; }
        }

        public String Text
        {
            get { return text; }
            set { text = value; }
        }

        public DateTime ThisDate
        {
            get { return thisDate; }
            set { thisDate = value; }
        }

        public String Category
        {
            get { return category; }
            set { category = value;}
        }

        public DateTime ReportedDate
        {
            get { return reportedDate; }
            set { reportedDate = value; }
        }

        public String FromLocation
        {
            get { return fromLocation; }
            set { fromLocation = value; }
        }

        public String ReportedTime
        {
            get { return reportedTime; }
            set { reportedTime = value; }
        }

        public String ReportingTime
        {
            get { return reportingTime; }
            set { reportingTime = value; }
        }

        public String TelephoneNo
        {
            get { return telephone; }
            set { telephone = value; }
        }

        #endregion

       

    }
}
