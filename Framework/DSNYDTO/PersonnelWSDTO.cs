﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
   public class PersonnelWSDTO
    {
       public int PageNo { get; set; }
       public int PageSize { get; set; }
       public string FirstName { get; set; }
       public string MiddleName { get; set; }
       public string LastName { get; set; }
       public string ReferenceNo { get; set; }
       public string SsnNo { get; set; }
    }
}
