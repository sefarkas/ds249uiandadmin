﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class EmployeeAddressDTO
    {
        public Int64 EmployeeId { get; set;}
        public Int64 AddressId { get; set;}
        public bool Is_Active { get; set;}
    }
}
