﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class UpLoadFileDTO
    {
        public UpLoadFileDTO()
        {

        }
        private Int32 indexNo;
        private Int32 incidentId;
        private byte[] image;
         private string chargeCode;
        private Int64 documentID;
        private Int64 parentID;
        private string  childId;
        private string applicationName;
        private string attachmentName;
        private string contenttype;



        public Int32 IndexNo
        {
            get { return indexNo; }
            set { indexNo = value; }
        }
        public Int32 IncidentId
        {
            get { return incidentId; }
            set { incidentId = value; }
        }
      
        public byte[] Image
        {
            get { return image; }
            set { image = value; }
        }
        public string ChargeCode
        {
            get { return chargeCode; }
            set { chargeCode = value; }
        }
        
        public Int64 DocumentID
        {
            get { return documentID; }
            set { documentID = value; }
        }
        public Int64 ParentID
        {
            get { return parentID; }
            set { parentID = value; }
        }
        public string  ChildID
        {
            get { return childId; }
            set { childId = value; }
        }
        public string ApplicationName
        {
            get { return applicationName; }
            set { applicationName = value; }

        }
        public string AttachmentName 
        {
            get { return attachmentName; }
            set { attachmentName = value; }

        }
        public string ContentType
        {
            get { return contenttype; }
            set { contenttype = value; }

        }

        public String ImageLocation { get; set; }
        public DateTime ImageDate { get; set; }
        public String WhenTaken { get; set; }
        public String TakenBy { get; set; }
        public String Note { get; set; }
        public String CCUNo { get; set; }
        public String District { get; set; }
        public String Location { get; set; }
        public String BlockNo { get; set; }
        public String LotNo { get; set; }
        public String House1 { get; set; }
        public String House2 { get; set; }
        public String StreetAddress { get; set; }
        public String BoroCode { get; set; }
        public String CCUYear { get; set; }
        public String CrossStreetOne { get; set; }
        public String CrossStreettwo { get; set; }
        public String Block { get; set; }
        public String Used_House1 { get; set; }
        public String Used_House2 { get; set; }
        public String Used_StreetAddress { get; set; }
        public String Used_CrossStreetOne { get; set; }
        public String Used_CrossStreetTwo { get; set; }
        public String Zone { get; set; }
        public String BlockLoc { get; set; }
        public String UsedBlockLoc { get; set; }
        public Int64 InspectionRequestId { get; set; }
        public Int64 LotId { get; set; }
    }
}