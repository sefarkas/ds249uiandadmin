﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO.PMD
{
    [Serializable]
    public class DistrictDTO
    {
        #region Instance Variables 

        private string fieldName;
        private long id;
        private string name;

        #endregion

        #region Properties

        /// <summary>
        /// Field name.
        /// </summary>
        public string FieldName
        {
            get { return fieldName; }
            set { fieldName = value; }
        }

        /// <summary>
        /// District id.
        /// </summary>
        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        #endregion Properties
    }
}
