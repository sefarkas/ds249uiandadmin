﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.DTO.PMD;

namespace DSNY.DSNYCP.DTO.PMD
{
    /// <summary>
    /// A  data transfer object representing an Employee.
    /// </summary>
    [Serializable]
    public class EmployeeDTO
    {
        #region Instance Variables

        // Encapsulated by properties (Standard)
        private long id;

        // Encapsulated by properties (Business)
        private string badgeNumber;
        private DateTime birthDate;
        private string firstName;
        private string homePhoneNumber;
        private string lastName;
        private string middleInitial;
        private string payrollLocation;
        private string referenceNumber;
        private string socialSecurityNumber;        
        private string title;
        private string vacationSchedule;
        private string workLocation;

        #endregion Instance Variables

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public EmployeeDTO() {}

        /// <summary>
        /// Constructs an Employee an id.
        /// </summary>
        /// <param name="id">Employee identifer</param>
        public EmployeeDTO(long id)
        {
            this.id = id;
        }

        #endregion Constructors

        #region Business Properties

        public string BadgeNumber { get { return badgeNumber; } set { badgeNumber = value; } }

        public DateTime BirthDate { get { return birthDate; } set { birthDate = value; } }

        public string FirstName { get { return firstName; } set { firstName = value; } }

        public string HomePhoneNumber { get { return homePhoneNumber; } set { homePhoneNumber = value; } }

        public string LastName { get { return lastName; } set { lastName = value; } }

        public string MiddleInitial { get { return middleInitial; } set { middleInitial = value; } }

        public string PayrollLocation { get { return payrollLocation; } set { payrollLocation = value; } }

        public string ReferenceNumber { get { return referenceNumber; } set { referenceNumber = value; } }

        public string SocialSecurityNumber { get { return socialSecurityNumber; } set { socialSecurityNumber = value; } }

        public string Title { get { return title; } set { title = value; } }

        public string VacationSchedule { get { return vacationSchedule; } set { vacationSchedule = value; } }

        public string WorkLocation { get { return workLocation; } set { workLocation = value; } }

        #endregion Business Properties        

        #region Standard Properties

        /// <summary>
        /// Medical Evaluation identifier.
        /// </summary>
        public long Id { get { return id; } }

        #endregion Standard Properties
    }
}
