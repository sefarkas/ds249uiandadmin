﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO.PMD
{
    [Serializable]
    public class SeparationDTO
    {
        #region Instance Variables

        private string createdBy;
        private DateTime creationDate;
        private List<SeparationDetailsDTO> details;
        private long employeeId;
        private DateTime lastModifiedOn;
        private string lastModifiedBy;
        private int lastRegularDayWorked;
        private long id;
        private SeparationTypeDTO type;
        private int statusId;
        private WorkflowStatusDTO workflowStatus;

        #endregion Instance Variables

        #region Constructors

        public SeparationDTO()
        {
            type = new SeparationTypeDTO();
            workflowStatus = new WorkflowStatusDTO();
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Employee who created this separation record.
        /// </summary>
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Separation record creation date.
        /// </summary>
        public DateTime CreationDate
        {
            get { return creationDate; }
            set { creationDate = value; }
        }

        /// <summary>
        /// Additional details
        /// </summary>
        public List<SeparationDetailsDTO> Details
        {
            get { return details; }
            set { details = value; }
        }

        /// <summary>
        /// Employee identifier.
        /// </summary>
        public long EmployeeId
        {
            get { return employeeId; }
            set { employeeId = value; }
        }
        
        /// <summary>
        /// Separation identifier.
        /// </summary>
        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        /// <summary>
        /// Employee who modified this record last.
        /// </summary>
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        /// <summary>
        /// Date this record was last modified.
        /// </summary>
        public DateTime LastModifiedOn
        {
            get { return lastModifiedOn; }
            set { lastModifiedOn = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int LastRegularDayWorked
        {
            get { return lastRegularDayWorked; }
            set { lastRegularDayWorked = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

        /// <summary>
        /// Separation type.
        /// </summary>
        public string Type
        {
            get { return type.Type; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public int WorkflowId
        {
            get { return workflowStatus.StatusId; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string WorkflowStatus
        {
            get { return workflowStatus.Status; }
        }

        #endregion Properties
    }
}
