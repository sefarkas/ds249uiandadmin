﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO.PMD
{
    class SeparationApprovalDetailsDTO
    {
        #region Instance Variables

        private string approvedBy;
        private DateTime approvedOn;
        private long separationId;
        private SeparationTypeDTO type;
        private string value;

        #endregion Instance Variables

        #region Properties

        /// <summary>
        /// Employee who approved separation.
        /// </summary>
        public string ApprovedBy
        {
            get { return approvedBy; }
            set { approvedBy = value; }
        }

        /// <summary>
        /// Date approved.
        /// </summary>
        public DateTime ApprovedOn
        {
            get { return approvedOn; }
            set { approvedOn = value; }
        }
        
        /// <summary>
        /// Separation identifier.
        /// </summary>
        public long SeparationId
        {
            get { return separationId; }
            set { separationId = value; }
        }
        
        /// <summary>
        /// Separation type.
        /// </summary>
        public SeparationTypeDTO Type
        {
            get { return type; }
            set { type = value; }
        }
        
        /// <summary>
        /// Approval comments.
        /// </summary>
        public string Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        #endregion Properties

    }
}
