﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DSNY.DSNYCP.DTO.PMD
{
    /// <summary>
    /// A data transfer object representing a Medical Evaluation List Item.
    /// </summary>
    [Serializable]
    public class MedicalEvaluationListItemDTO
    {

        #region Instance Variables

        private string firstName;
        private long id;
        private DateTime lastModified;
        private string lastName;
        private string middleName;
        private String referenceNumber;
        private int statusId;

        #endregion Instance Variables

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MedicalEvaluationListItemDTO(long id, DateTime lastModified)
        {
            this.id = id;
            this.lastModified = lastModified;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Employee's first name.
        /// </summary>
        public String FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        /// <summary>
        /// Employee's last name.
        /// </summary>
        public String LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        /// <summary>
        /// Employee's middle name.
        /// </summary>
        public String MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }

        /// <summary>
        /// Interview Request identifier.
        /// </summary>
        public long Id
        {
            get { return id; }
        }

        /// <summary>
        /// Date the Interview Request was last modified.
        /// </summary>
        public DateTime LastModified
        {
            get { return lastModified; }
        }

        /// <summary>
        /// Reference number of the employee who created the Interview Request.
        /// </summary>
        public String ReferenceNumber
        {
            get { return referenceNumber; }
            set { referenceNumber = value; }
        }

        /// <summary>
        /// Status of the Interview Request.
        /// </summary>
        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

        #endregion Properties
    }
}
