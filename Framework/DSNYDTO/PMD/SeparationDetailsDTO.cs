﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO.PMD
{
    [Serializable]
    public class SeparationDetailsDTO
    {
        #region Instance Variables

        private string comments;
        private int days;
        private DateTime endDate;
        private DateTime startDate;

        #endregion Instance Variables

        #region Properties

        /// <summary>
        /// Number of separation days.
        /// </summary>
        public int Days
        {
            get { return days; }
            set { days = value; }
        }

        /// <summary>
        /// Date separation ends.
        /// </summary>
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        /// <summary>
        /// Date separation starts.
        /// </summary>
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }
        
        /// <summary>
        /// Comments
        /// </summary>
        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        #endregion Properties
    }
}
