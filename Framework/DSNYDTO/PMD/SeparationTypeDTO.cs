﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO.PMD
{
    [Serializable]
    public class SeparationTypeDTO
    {
        #region Instance Variables 

        private long id;
        private string type;

        #endregion

        #region Properties

        /// <summary>
        /// Separation type.
        /// </summary>
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        /// <summary>
        /// Separation id.
        /// </summary>
        public long Id
        {
            get { return id; }
            set { id = value; }
        }

        #endregion Properties
    }
}
