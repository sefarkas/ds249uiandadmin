﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO.PMD
{
    [Serializable]
    public class SeparationListItemDTO
    {
        #region Instance Variables

        private string createdBy;
        private DateTime dateCreated;
        private string firstName;
        private long id;
        private string lastName;
        private string middleName;
        private string payCode;
        private string payDescription;
        private string referenceNumber;
        private string status;
        private string type;
        private string workflowId;

        #endregion

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SeparationListItemDTO(long id)
        {
            this.id = id;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Separation author
        /// </summary>
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        /// <summary>
        /// Date the Separation was last created.
        /// </summary>
        public DateTime DateCreated
        {
            get { return dateCreated; }
            set { dateCreated = value; }
        }

        /// <summary>
        /// Employee's first name.
        /// </summary>
        public String FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        /// <summary>
        /// Separation identifier.
        /// </summary>
        public long Id
        {
            get { return id; }
        }

        /// <summary>
        /// Employee's last name.
        /// </summary>
        public String LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        /// <summary>
        /// Employee's middle name.
        /// </summary>
        public String MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }

        /// <summary>
        /// Employee's pay code.
        /// </summary>
        public String PayCode
        {
            get { return payCode; }
            set { payCode = value; }
        }

        /// <summary>
        /// Employee's pay description.
        /// </summary>
        public String PayDescription
        {
            get { return payDescription; }
            set { payDescription = value; }
        }

        /// <summary>
        /// Employee's reference number.
        /// </summary>
        public String ReferenceNumber
        {
            get { return referenceNumber; }
            set { referenceNumber = value; }
        }

        /// <summary>
        /// Status of the Separation.
        /// </summary>
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        /// <summary>
        /// Separation type
        /// </summary>
        public string Type
        {
            get { return type; }
            set { type = value; }
        }

        /// <summary>
        /// Workflow Id.
        /// </summary>
        public string WorkflowId
        {
            get { return workflowId; }
            set { workflowId = value; }
        }

        #endregion Properties
    }
}
