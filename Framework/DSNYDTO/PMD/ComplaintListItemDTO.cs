﻿using System;
using System.Collections.Generic;

namespace DSNY.DSNYCP.DTO.PMD
{
    /// <summary>
    /// A data transfer object representing a Complaint Item.
    /// </summary>
    [Serializable]
    public class ComplaintListItemDTO
    {
        #region Instance Variables

        private String callerName;
        private long id;
        private String logNumber;
        private DateTime submittedDate;
        private String submittedBy;

        #endregion Instance Variables

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="id">Complaint identifier</param>
        public ComplaintListItemDTO(long id)
        {
            this.id = id;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Name of caller.
        /// </summary>
        public String CallerName
        {
            get { return callerName; }
            set { callerName = value; }
        }

        /// <summary>
        /// Complaint identifier.
        /// </summary>
        public long Id
        {
            get { return id; }
        }

        /// <summary>
        /// Complaing log number.
        /// </summary>
        public String LogNumber
        {
            get { return logNumber; }
            set { logNumber = value; }
        }

        /// <summary>
        /// Date the Complaint was submitted into the system.
        /// </summary>
        public DateTime SubmittedDate
        {
            get { return submittedDate; }
            set { submittedDate = value; }
        }

        /// <summary>
        /// Emplopyee who entered the Complaint into the system.
        /// </summary>
        public String SubmittedBy
        {
            get { return submittedBy; }
            set { submittedBy = value; }
        }

        #endregion Properties
    }
}
