﻿using System;
using System.Collections.Generic;

namespace DSNY.DSNYCP.DTO.PMD
{
    /// <summary>
    /// An data transfer object representing a Complaint.
    /// </summary>
    [Serializable]
    public class ComplaintDTO
    {
        #region Instance Variables

        private String action;
        private String actionTakenBy;
        private String callerAddress;
        private String callerName;
        private String callerPhone;
        private String complaint;
        private DateTime date;
        private long id;
        private String logNumber;
        private List<ComplaintNamedEmployeeDTO> namedEmployees;
        private String submittedBy;
        private String type;
        private byte[] versionStamp;

        #endregion Instance Variables

        #region Constructors

        /// <summary>
        /// Constructors a Complaint.
        /// </summary>
        /// <param name="id">Complaint identifier</param>
        /// <param name="versionStamp">Version of the Complaint</param>
        public ComplaintDTO(long id,  byte[] versionStamp) 
        {
            this.id = id;
            this.versionStamp = versionStamp;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Action taken to address Complaint.
        /// </summary>
        public String Action
        {
            get { return action; }
            set { action = value; }
        }

        /// <summary>
        /// Person who performed the action.
        /// </summary>
        public String ActionTakenBy
        {
            get { return actionTakenBy; }
            set { actionTakenBy = value; }
        }

        /// <summary>
        /// Address of caller.
        /// </summary>
        public String CallerAddress
        {
            get { return callerAddress; }
            set { callerAddress = value; }
        }

        /// <summary>
        /// Name of caller.
        /// </summary>
        public String CallerName
        {
            get { return callerName; }
            set { callerName = value; }
        }

        /// <summary>
        /// Caller's phone number.
        /// </summary>
        public String CallerPhone
        {
            get { return callerPhone; }
            set { callerPhone = value; }
        }

        /// <summary>
        /// Complaint text.
        /// </summary>
        public String Complaint
        {
            get { return complaint; }
            set { complaint = value; }
        }

        /// <summary>
        /// Date of Complaint.s
        /// </summary>
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        /// <summary>
        /// Complaint identifer.
        /// </summary>
        public long Id
        {
            get { return id; }
        }

        /// <summary>
        /// Log number.
        /// </summary>
        public String LogNumber
        {
            get { return logNumber; }
            set { logNumber = value; }
        }

        /// <summary>
        /// Employees named in the Complaint.
        /// </summary>
        public List<ComplaintNamedEmployeeDTO> NamedEmployees
        {
            get { return namedEmployees; }
            set { namedEmployees = value; }
        }

        /// <summary>
        /// Id of user who submitted the complaint 
        /// </summary>
        public string SubmittedBy
        {
            get { return submittedBy; }
            set { submittedBy = value; }
        }

        /// <summary>
        /// Type of Complaint.
        /// </summary>
        public String Type
        {
            get { return type; }
            set { type = value; }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Retrieves the Complaint version.
        /// </summary>
        /// <returns>The version of the Complaint or null if this is a new Complaint.</returns>
        public byte[] GetVersionStamp()
        {
            return versionStamp;
        }

        #endregion Methods
    }
}
