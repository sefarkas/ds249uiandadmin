﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO.PMD
{
    /// <summary>
    /// An data transfer object representing a Medical Evaluation.
    /// </summary>
    [Serializable]
    public class MedicalEvaluationDTO
    {
        #region Instance Variables

        // Encapsulated by properties (Standard)
        private long id;
        private DateTime lastModified;
        private byte[] versionStamp;

        // Encapsulated by properties (Business)
        private DateTime appointmentDate;
        private DateTime arrivedAtClinic;
        private string chartDay;
        private TimeSpan departedForClinic;
        private TimeSpan departedFromClinic;
        private string districtResident;
        private string employeeBadgeNumber;
        private DateTime employeeBirthDate;
        private string employeeFirstName;
        private string employeeHomePhoneNumber;
        private string employeeLastName;
        private string employeeMiddleInitial;
        private string employeeReferenceNumber;
        private string employeeTitle;
        private string employeeSocialSecurityNumberLastFourDigits;
        private DateTime endingRestrictedDuty;
        private string mdaRenewal;
        private string mdaRenewed;
        private string medicalOfficerFirstName;
        private string medicalOfficerLastName;
        private string medicalOfficerMiddleInitial;
        private string medicalOfficerReferenceNumber;
        private string medicalRecommendation;
        private string medicalRecommendationForLimitedDuty;
        private string medicalRemarks;
        private string nightSigned;
        private string outOfUniform;
        private string payrollLocation;
        private string pmdEmployeeFirstName;
        private string pmdEmployeeLastName;
        private string pmdEmployeeMiddleInitial;
        private string pmdEmployeeReferenceNumber;
        private string pmdRemarks;
        private string reason;
        private string reportedSick;
        private DateTime reportedToLocation;
        private DateTime resumptionDate;
        private string scheduledVacationDue;
        private string shopSteward;
        private int statusId;
        private string supervisorFirstName;
        private string supervisorLastName;
        private string supervisorMiddleInitial;
        private string supervisorReferenceNumber;
        private string supervisorTitle;
        private string working;
        private string workLocation;

        #endregion Instance Variables

        #region Constructors

        /// <summary>
        /// Constructors a Medical Evaluation.
        /// </summary>
        /// <param name="id">Medical Evaluation identifier</param>
        /// <param name="lastModified">Date the Medical Evaluation was last modified</param>
        /// <param name="versionStamp">Version of the Medical Evaluation</param>
        public MedicalEvaluationDTO(long id, DateTime lastModified, byte[] versionStamp)
        {
            this.id = id;
            this.lastModified = lastModified;
            this.versionStamp = versionStamp;
        }

        #endregion Constructors

        #region Business Properties

        public DateTime AppointmentDate { get { return appointmentDate; } set { appointmentDate = value; } }

        public DateTime ArrivedAtClinic { get { return arrivedAtClinic; } set { arrivedAtClinic = value; } }

        public string ChartDay { get { return chartDay; } set { chartDay = value; } }

        public TimeSpan DepartedForClinic { get { return departedForClinic; } set { departedForClinic = value; } }

        public TimeSpan DepartedFromClinic { get { return departedFromClinic; } set { departedFromClinic = value; } }

        public string DistrictResident { get { return districtResident; } set { districtResident = value; } }

        public string EmployeeBadgeNumber { get { return employeeBadgeNumber; } set { employeeBadgeNumber = value; } }

        public DateTime EmployeeBirthDate { get { return employeeBirthDate; } set { employeeBirthDate = value; } }

        public string EmployeeFirstName { get { return employeeFirstName; } set { employeeFirstName = value; } }

        public string EmployeeHomePhoneNumber { get { return employeeHomePhoneNumber; } set { employeeHomePhoneNumber = value; } }

        public string EmployeeLastName { get { return employeeLastName; } set { employeeLastName = value; } }

        public string EmployeeMiddleInitial { get { return employeeMiddleInitial; } set { employeeMiddleInitial = value; } }

        public string EmployeeReferenceNumber { get { return employeeReferenceNumber; } set { employeeReferenceNumber = value; } }

        public string EmployeeTitle { get { return employeeTitle; } set { employeeTitle = value; } }

        public string EmployeeSocialSecurityNumberLastFourDigits { get { return employeeSocialSecurityNumberLastFourDigits; } set { employeeSocialSecurityNumberLastFourDigits = value; } }

        public DateTime EndingRestrictedDuty { get { return endingRestrictedDuty; } set { endingRestrictedDuty = value; } }

        public string MdaRenewal { get { return mdaRenewal; } set { mdaRenewal = value; } }

        public string MdaRenewed { get { return mdaRenewed; } set { mdaRenewed = value; } }

        public string MedicalOfficerFirstName { get { return medicalOfficerFirstName; } set { medicalOfficerFirstName = value; } }

        public string MedicalOfficerLastName { get { return medicalOfficerLastName; } set { medicalOfficerLastName = value; } }

        public string MedicalOfficerMiddleInitial { get { return medicalOfficerMiddleInitial; } set { medicalOfficerMiddleInitial = value; } }

        public string MedicalOfficerReferenceNumber { get { return medicalOfficerReferenceNumber; } set { medicalOfficerReferenceNumber = value; } }

        public string MedicalRecommendation { get { return medicalRecommendation; } set { medicalRecommendation = value; } }

        public string MedicalRecommendationForLimitedDuty { get { return medicalRecommendationForLimitedDuty; } set { medicalRecommendationForLimitedDuty = value; } }

        public string MedicalRemarks { get { return medicalRemarks; } set { medicalRemarks = value; } }

        public string NightSigned { get { return nightSigned; } set { nightSigned = value; } }

        public string OutOfUniform { get { return outOfUniform; } set { outOfUniform = value; } }

        public string PayrollLocation { get { return payrollLocation; } set { payrollLocation = value; } }

        public string PmdEmployeeFirstName { get { return pmdEmployeeFirstName; } set { pmdEmployeeFirstName = value; } }

        public string PmdEmployeeLastName { get { return pmdEmployeeLastName; } set { pmdEmployeeLastName = value; } }

        public string PmdEmployeeMiddleInitial { get { return pmdEmployeeMiddleInitial; } set { pmdEmployeeMiddleInitial = value; } }

        public string PmdEmployeeReferenceNumber { get { return pmdEmployeeReferenceNumber; } set { pmdEmployeeReferenceNumber = value; } }

        public string PmdRemarks { get { return pmdRemarks; } set { pmdRemarks = value; } }

        public string Reason { get { return reason; } set { reason = value; } }

        public string ReportedSick { get { return reportedSick; } set { reportedSick = value; } }

        public DateTime ReportedToLocation { get { return reportedToLocation; } set { reportedToLocation = value; } }

        public DateTime ResumptionDate { get { return resumptionDate; } set { resumptionDate = value; } }

        public string ScheduledVacationDue { get { return scheduledVacationDue; } set { scheduledVacationDue = value; } }

        public string ShopSteward { get { return shopSteward; } set { shopSteward = value; } }

        public int StatusId { get { return statusId; } set { statusId = value; } }

        public string SupervisorFirstName { get { return supervisorFirstName; } set { supervisorFirstName = value; } }

        public string SupervisorLastName { get { return supervisorLastName; } set { supervisorLastName = value; } }

        public string SupervisorMiddleInitial { get { return supervisorMiddleInitial; } set { supervisorMiddleInitial = value; } }

        public string SupervisorReferenceNumber { get { return supervisorReferenceNumber; } set { supervisorReferenceNumber = value; } }

        public string SupervisorTitle { get { return supervisorTitle; } set { supervisorTitle = value; } }

        public string Working { get { return working; } set { working = value; } }

        public string WorkLocation { get { return workLocation; } set { workLocation = value; } }

        #endregion Business Properties

        #region Standard Properties

        /// <summary>
        /// Medical Evaluation identifier.
        /// </summary>
        public long Id { get { return id; } }

        /// <summary>
        /// Medical Evaluation's last modification date
        /// </summary>
        public DateTime LastModified { get { return lastModified; } }

        #endregion Standard Properties

        #region Methods

        /// <summary>
        /// Retrieves the Medical Evaluation's version stamp (used to determine if a Medical Evaluation is out of date).
        /// </summary>
        /// <returns>The Medical Evaluation's version stamp.</returns>
        public byte[] GetVersionStamp()
        {
            return versionStamp;
        }

        #endregion Methods
    }
}
