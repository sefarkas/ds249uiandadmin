﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO.PMD
{
    /// <summary>
    /// An data transfer object representing an Interview Request.
    /// </summary>
    [Serializable]
    public class InterviewRequestDTO
    {
        #region Instance Variables

        // Encapsulated by properties
        private int? accruedTime = null;
        private string assignmentLocation;
        private string boroughComments;
        private string boroughOfficerFirstName;
        private string boroughOfficerLastName;
        private string boroughOfficerMiddleName;
        private string boroughOfficerReferenceNumber;
        private string boroughRecommendation;
        private string boroughSuggestedInterviewLocation;
        private string chartNumber;
        private string disposition;
        private string dispositionNotes;
        private string employeeFirstName;
        private string employeeMiddleInitial;
        private string employeeLastName;
        private string employeeReferenceNumber;
        private string employeeTitle;
        private long id;
        private DateTime interviewDateAndTime;
        private string interviewLocation;
        private DateTime lastModified;
        private int? numberOfAwols = null;
        private int? numberOfComplaints = null;
        private int? numberOfDaysLate = null;
        private int? numberOfLodiDays = null;
        private int? numberOfLodiIncidents = null;
        private int? numberOfSickDays = null;
        private int? numberOfSickLeaveIncidents = null;
        private string reason;
        private DateTime requestedAppointmentDate;
        private DateTime requestSubmissionDate;
        private DateTime requestBoroughSubmissionDate;
        private string selectedVacationBatches;
        private string shift;
        private int statusId;
        private byte[] versionStamp;

        #endregion Instance Variables

        #region Constructors

        /// <summary>
        /// Constructors an Interview Request.
        /// </summary>
        /// <param name="id">Interview Request identifier</param>
        /// <param name="lastModified">Date the Interview Request was last modified</param>
        /// <param name="versionStamp">Version of the Internet Request</param>
        public InterviewRequestDTO(long id,  DateTime lastModified, byte[] versionStamp) 
        {
            this.id = id;
            this.lastModified = lastModified;
            this.versionStamp = versionStamp;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Employee's accrued time.
        /// </summary>
        public int? AccruedTime
        {
            get { return accruedTime; }
            set { accruedTime = value; }
        }

        /// <summary>
        /// Employee's current assignment location.
        /// </summary>
        public string AssignmentLocation
        {
            get { return assignmentLocation; }
            set { assignmentLocation = value; }
        }

        /// <summary>
        /// Comments by borough officer.
        /// </summary>
        public string BoroughComments
        {
            get { return boroughComments; }
            set { boroughComments = value; }
        }

        /// <summary>
        /// Borough officer's first name.
        /// </summary>
        public string BoroughOfficerFirstName
        {
            get { return boroughOfficerFirstName; }
            set { boroughOfficerFirstName = value; }
        }

        /// <summary>
        /// Borough officer's last name.
        /// </summary>
        public string BoroughOfficerLastName
        {
            get { return boroughOfficerLastName; }
            set { boroughOfficerLastName = value; }
        }

        /// <summary>
        /// Borough officer's middle name.
        /// </summary>
        public string BoroughOfficerMiddleName
        {
            get { return boroughOfficerMiddleName; }
            set { boroughOfficerMiddleName = value; }
        }


        /// <summary>
        /// Borough officer's reference number.
        /// </summary>
        public string BoroughOfficerReferenceNumber
        {
            get { return boroughOfficerReferenceNumber; }
            set { boroughOfficerReferenceNumber = value; }
        }

        /// <summary>
        /// Borough officer's recommendation.
        /// </summary>        
        public string BoroughRecommendation
        {
            get { return boroughRecommendation; }
            set { boroughRecommendation = value; }
        }

        /// <summary>
        /// Suggested interview location.
        /// </summary>
        public string BoroughSuggestedInterviewLocation
        {
            get { return boroughSuggestedInterviewLocation; }
            set { boroughSuggestedInterviewLocation = value; }
        }

        /// <summary>
        /// Employee's chart number.
        /// </summary>
        public string ChartNumber
        {
            get { return chartNumber; }
            set { chartNumber = value; }
        }

        /// <summary>
        /// Interviewing officer's disposition.
        /// </summary>
        public string Disposition
        {
            get { return disposition; }
            set { disposition = value; }
        }

        /// <summary>
        /// Interviewing officer's disposition notes.
        /// </summary>
        public string DispositionNotes
        {
            get { return dispositionNotes; }
            set { dispositionNotes = value; }
        }

        /// <summary>
        /// Employee's first name.
        /// </summary>
        public string EmployeeFirstName
        {
            get { return employeeFirstName; }
            set { employeeFirstName = value; }
        }

        /// <summary>
        /// Employee's middle name.
        /// </summary>
        public string EmployeeMiddleInitial
        {
            get { return employeeMiddleInitial; }
            set { employeeMiddleInitial = value; }
        }

        /// <summary>
        /// Employee's last name.
        /// </summary>
        public string EmployeeLastName
        {
            get { return employeeLastName; }
            set { employeeLastName = value; }
        }

        /// <summary>
        /// Employee's reference number.
        /// </summary>
        public string EmployeeReferenceNumber
        {
            get { return employeeReferenceNumber; }
            set { employeeReferenceNumber = value; }
        }

        /// <summary>
        /// Employee's title.
        /// </summary>
        public string EmployeeTitle
        {
            get { return employeeTitle; }
            set { employeeTitle = value; }
        }

        /// <summary>
        /// Interview Request identifier.
        /// </summary>
        public long Id
        {
            get { return id; }
        }

        /// <summary>
        /// Interview date and time.
        /// </summary>
        public DateTime InterviewDateAndTime
        {
            get { return interviewDateAndTime; }
            set { interviewDateAndTime = value; }
        }

        /// <summary>
        /// Interview location.
        /// </summary>
        public string InterviewLocation
        {
            get { return interviewLocation; }
            set { interviewLocation = value; }
        }

        /// <summary>
        /// Interview Request's last modification date
        /// </summary>
        public DateTime LastModified
        {
            get { return lastModified; }
        }

        /// <summary>
        /// Employee's number of AWOLs.
        /// </summary>
        public int? NumberOfAwols
        {
            get { return numberOfAwols; }
            set { numberOfAwols = value; }
        }

        /// <summary>
        /// Complaints against employee.
        /// </summary>
        public int? NumberOfComplaints
        {
            get { return numberOfComplaints; }
            set { numberOfComplaints = value; }
        }

        /// <summary>
        /// Number of days the employee was late.
        /// </summary>
        public int? NumberOfDaysLate
        {
            get { return numberOfDaysLate; }
            set { numberOfDaysLate = value; }
        }

        /// <summary>
        /// Number of days the employee was on LODI.
        /// </summary>
        public int? NumberOfLodiDays
        {
            get { return numberOfLodiDays; }
            set { numberOfLodiDays = value; }
        }

        /// <summary>
        /// Number of days LODI incidents for the employee.
        /// </summary>
        public int? NumberOfLodiIncidents
        {
            get { return numberOfLodiIncidents; }
            set { numberOfLodiIncidents = value; }
        }

        /// <summary>
        /// Number of days the employee was sick.
        /// </summary>
        public int? NumberOfSickDays
        {
            get { return numberOfSickDays; }
            set { numberOfSickDays = value; }
        }

        /// <summary>
        /// Number of sick leave incidents for the employee.
        /// </summary>        
        public int? NumberOfSickLeaveIncidents
        {
            get { return numberOfSickLeaveIncidents; }
            set { numberOfSickLeaveIncidents = value; }
        }

        /// <summary>
        /// Reason for Interview Request.
        /// </summary>
        public String Reason
        {
            get { return reason; }
            set { reason = value; }
        }

        /// <summary>
        /// Requested interview appointment date.
        /// </summary>
        public DateTime RequestedAppointmentDate
        {
            get { return requestedAppointmentDate; }
            set { requestedAppointmentDate = value; }
        }

        /// <summary>
        /// Date the Interveiw Request was submitted.
        /// </summary>
        public DateTime RequestSubmissionDate
        {
            get { return requestSubmissionDate; }
            set { requestSubmissionDate = value; }
        }

        /// <summary>
        /// Date the Interveiw Request was submitted by the borough officer.
        /// </summary>
        public DateTime RequestBoroughSubmissionDate
        {
            get { return requestBoroughSubmissionDate; }
            set { requestBoroughSubmissionDate = value; }
        }

        /// <summary>
        /// Employee's selected vacation batches.
        /// </summary>
        public String SelectedVacationBatches
        {
            get { return selectedVacationBatches; }
            set { selectedVacationBatches = value; }
        }

        /// <summary>
        /// Employee' shift.
        /// </summary>
        public String Shift
        {
            get { return shift; }
            set { shift = value; }
        }

        /// <summary>
        /// Status of the Interview Request.
        /// </summary>
        public int StatusId
        {
            get { return statusId; }
            set { statusId = value; }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Retrieves the Interview Request version.
        /// </summary>
        /// <returns>The version of the Interview Request or null if this is a new Interview Request.</returns>
        public byte[] GetVersionStamp()
        {
            return versionStamp;
        }

        #endregion Methods
    }
}
