﻿using System;

namespace DSNY.DSNYCP.DTO.PMD
{
    /// <summary>
    /// An data transfer object representing a Employee named in a Complaint.
    /// </summary>
    [Serializable]
    public class ComplaintNamedEmployeeDTO
    {
        #region Instance Variables

        private long id;
        private long complaintId;
        private long employeeId;
        private DateTime created;
        private String employeeFullName;
        private String employeeReferenceNumber;

        #endregion Instance Variables

        #region Constructors

        /// <summary>
        /// Constructors a Complaint Named Employee.
        /// </summary>
        /// <param name="id">Complaint Named Employee identifier</param>
        public ComplaintNamedEmployeeDTO(long id) 
        {
            this.id = id;
        }

        #endregion Constructors

        #region Properties

        /// <summary>
        /// Complaint Named Employee identifier.
        /// </summary>
        public long Id
        {
            get { return id; }
        }

        /// <summary>
        /// Complaint identifier.
        /// </summary>
        public Int64 ComplaintId
        {
            get { return complaintId; }
            set { complaintId = value; }
        }

        /// <summary>
        /// Employee identifier.
        /// </summary>
        public Int64 EmployeeId
        {
            get { return employeeId; }
            set { employeeId = value; }
        }

        /// <summary>
        /// Date the employee was named in the complaint (via system, not text)
        /// </summary>
        public DateTime Created
        {
            get { return created; }
            set { created = value; }
        }

        /// <summary>
        /// Employee's full name.
        /// </summary>
        public String EmployeeFullName
        {
            get { return employeeFullName; }
            set { employeeFullName = value; }
        }

        /// <summary>
        /// Employee's reference number.
        /// </summary>
        public String EmployeeReferenceNumber
        {
            get { return employeeReferenceNumber; }
            set { employeeReferenceNumber = value; }
        }
        
        #endregion Properties
    }
}
