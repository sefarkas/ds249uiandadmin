﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class TitleListDTO
    {
        # region Variables
        private Int64 titleID;
        private string titleCode;
        private string titleDescription;
        # endregion

        # region Properties
        public Int64 TitleID
        {
            get { return titleID; }
            set { titleID = value; }
        }
        public string TitleCode
        {
            get { return titleCode; }
            set { titleCode = value; }
        }
        public string TitleDescription
        {
            get { return titleDescription; }
            set { titleDescription = value; }
        }
        # endregion
    }
}
