﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class RoleDTO
    {
        
         public RoleDTO()
        {
            Id = -1;
            membershipID = -1;
            role = String.Empty;
            roleID = -1;
            personalID = -1;
        }
        
        #region Private Variables

        private Int16 Id;
        private Int64 membershipID;
        private Int64 roleID;  // Int16 roleID; by Farkas 27Feb2020
        private String role;
        private Int64 personalID;

        #endregion

        #region Public Property

        public Int16 ID
        {
            get { return Id; }
            set { Id = value; }
        }

        public Int64 MembershipID
        {
            get { return membershipID; }
            set { membershipID = value; }
        }

        public Int64 PersonalID
        {
            get { return personalID; }
            set { personalID = value; }
        }

        public Int64 RoleID // Int16 RoleID by Farkas 27Feb2020
        {
            get { return roleID; }
            set { roleID = value; }
        }

        public String RoleDesc
        {
            get { return role; }
            set { role = value; }
        }

        #endregion
    }
}
