﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class AdvocateAssociatedComplaintDTO
    {
        #region Private Variables
        private Int64 _associatedID = -1;
        private Int64 _advocacyCaseID = -1;
        private Int64 _asscociatedCompaintID = -1;
        private Int16 _uniqueID = -1;
        #endregion

        public Int16 UniqueID
        {
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }

        public Int64 AssociatedID
        {
            get { return _associatedID; }
            set { _associatedID = value; }
        }

        public Int64 AdvocacyCaseID
        {
            get { return _advocacyCaseID; }
            set { _advocacyCaseID = value; }
        }
        public Int64 AsscociatedCompaintID
        {
            get { return _asscociatedCompaintID; }
            set { _asscociatedCompaintID = value; }
        }

    }
}
