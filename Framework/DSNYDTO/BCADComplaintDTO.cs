﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class BCADComplaintDTO
    {

        #region Private Variables

        private Int64 bcadCaseID = -1;
        private Int64 complaintID = -1;
        private Int16 createUser = 0;
        private DateTime createDate = DateTime.MinValue;
        private Int16 modifiedUser = 0;
        private DateTime modifiedDate = DateTime.MinValue;
        private Int16 finalStatusCd = 0;
        private Int16 nonPenaltyResultCd = 0;
        private Int16 adjudicationBodyCd = 0;
        private bool isProbation;
        private DateTime adjudicationDate = DateTime.MinValue;
        private String isAvailable = String.Empty;
        private List<BCADPenaltyDTO> penalty;
       
        #endregion


        #region Public Property

        public Int64 BCADCaseID
        {
            get { return bcadCaseID; }
            set { bcadCaseID = value; }
        }

        public Int64 ComplaintID
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int16 CreateUser
        {
            get { return createUser; }
            set { createUser = value; }
        }

        public DateTime CreateDate
        {
            get { return createDate; }
            set { createDate = value; }
        }

        public Int16 ModifiedUser
        {
            get { return modifiedUser; }
            set { modifiedUser = value; }
        }

        public DateTime ModifiedDate
        {
            get { return modifiedDate; }
            set { modifiedDate = value; }
        }

        public Int16 FinalStatusCd
        {
            get { return finalStatusCd; }
            set { finalStatusCd = value; }
        }

        public Int16 NonPenaltyResultCd
        {
            get { return nonPenaltyResultCd; }
            set { nonPenaltyResultCd = value; }
        }

        public Int16 AdjudicationBodyCd
        {
            get { return adjudicationBodyCd; }
            set { adjudicationBodyCd = value; }
        }

        public DateTime AjudicationDate
        {
            get { return adjudicationDate; }
            set { adjudicationDate = value; }
        }

        public String IsAvailable
        {
            get { return isAvailable; }
            set { isAvailable = value; }
        }

        public List<BCADPenaltyDTO> Penalty
        {
            get { return penalty; }
            set { penalty = value; }
        }

        public Boolean IsProbation
        {
            get { return isProbation; }
            set { isProbation = value; }
        }
        #endregion
    }
}
