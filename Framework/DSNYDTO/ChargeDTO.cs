﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using DSNY.DSNYCP.Proxys;

namespace DSNY.DSNYCP.DTO
{
    public class ChargeDTO
    {
        //Proxy proxy;

        public ChargeDTO()
        {

            chargeTypeID = 0;
            chargeCode = String.Empty;
            chargeDescription = String.Empty;
        }


        #region Private Variables
        
        private Int16 chargeTypeID;
        private String chargeCode;
        private String chargeDescription;
        private String referenceNumber;
        #endregion

        #region Public Property

        public Int16 ChargeTypeID
        {
            get { return chargeTypeID; }
            set { chargeTypeID = value; }
        }

        public String ChargeCode
        {
            get { return chargeCode; }
            set { chargeCode = value; }
        }

        public String ChargeDescription
        {
            get { return chargeDescription; }
            set { chargeDescription = value; }
        }
        public String ReferenceNumber
        {
            get { return referenceNumber; }
            set { referenceNumber = value; }
        }
        #endregion       
    }
}
