﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class LocationDTO
    {
        private Int64 locationID;
        private String locationName;
        private Int64 locationParentID;

        /// <summary>
        /// Public property
        /// </summary>
        public Int64 LocationID
        {
            get { return locationID; }
            set { locationID = value; }
        }

        public String LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        public Int64 LocationParentID
        {
            get { return locationParentID; }
            set { locationParentID = value; }
        }

        public bool IsActive { get; set; }
        public string LocationCode { get; set; }
    }

}
