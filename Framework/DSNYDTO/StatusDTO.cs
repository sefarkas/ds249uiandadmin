﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class StatusDTO
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string StatusCd { get; set; }
    }
}
