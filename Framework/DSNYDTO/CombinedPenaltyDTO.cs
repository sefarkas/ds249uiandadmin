﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class CombinedPenaltyDTO
    {
           
              private Int64 _parentCombPenID = -1;
              private Int16 _childCombPenID = -1;
              private Int16 _isActive = -1;
              private Int16 _combPenID = -1;

        


              public Int64 ParentCombPenID
              {
                  get { return _parentCombPenID; }
                  set { _parentCombPenID = value; }
              }
              public Int16 ChildCombPenID
              {
                  get { return _childCombPenID; }
                  set { _childCombPenID = value; }
              }
              public Int16 IsActive
              {
                  get { return _isActive; }
                  set { _isActive = value; }
              }
              public Int16 CombPenID
              {
                  get { return _combPenID; }
                  set { _combPenID = value; }
              }
    }
}
