﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class EmployeeStatusDTO
    {
        public Int64 EmployeeStatusID { get; set;}
        public Int64 StatusID { get; set;}
        public Int64 EmployeeID { get; set;}
        public DateTime EffectiveDate { get; set;}
        public bool IsActive { get; set;}
        public string Remarks { get; set;}
    }
}
