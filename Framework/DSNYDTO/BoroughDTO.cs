﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
 public  class BoroughDTO
    {
        
            private Int64 locationID = -1;
            private String locationName = String.Empty;
            private Int64 locationParentID = -1;

            /// <summary>
            /// Public property
            /// </summary>
            public Int64 LocationID
            {
                get { return locationID; }
                set { locationID = value; }
            }

            public String LocationName
            {
                get { return locationName; }
                set { locationName = value; }
            }

            public Int64 LocationParentID
            {
                get { return locationParentID; }
                set { locationParentID = value; }
            }
        }
    }

