﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    /// <summary>
    ///  Added dto PAYCODE DESCRIPTION
    /// </summary>
   public class PayCodeDTO
    {
       /// <summary>
       /// Default constructor to load default value
       /// </summary>
        public PayCodeDTO()
        {
            payCodeId = -1;
            payCodeDesc = string.Empty;
        }
        private Int16 payCodeId ;
        private String payCodeDesc;

       /// <summary>
       /// Declaration of Properties
       /// </summary>
        public Int16 PayCodeId
        {
            get { return payCodeId; }
            set { payCodeId = value; }
        }
        public string PayCodeDesc
        {
            get { return payCodeDesc; }
            set { payCodeDesc = value; }
        }
    }
}
