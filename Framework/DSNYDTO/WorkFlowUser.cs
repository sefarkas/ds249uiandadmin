﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    [Serializable]
    public class WorkFlowUser
    {
        private WorkflowGroupsDTO m_Group;

        public WorkflowGroupsDTO Group
        {
            get { return m_Group; }
            set { m_Group = value; }
        }


        public string GroupName
        {
            get { return m_Group.GroupName;}

        }

        public string ApplicationName
        {
            get { return m_Group.ApplicationName; }

        }

        public string UserName
        {
            get { return m_FirstName + " " + m_LastName; }
          
        }

        private string m_FirstName;

        public string FirstName
        {
            get { return m_FirstName; }
            set { m_FirstName = value; }
        }
        private string m_UserId;

        public string UserId
        {
            get { return m_UserId; }
            set { m_UserId = value; }
        }
        private string m_LastName;

        public string LastName
        {
            get { return m_LastName; }
            set { m_LastName = value; }
        }
        private string m_Email;

        public string Email
        {
            get { return m_Email; }
            set { m_Email = value; }
        }


    }
}
