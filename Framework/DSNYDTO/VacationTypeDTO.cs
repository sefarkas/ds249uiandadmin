﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    [Serializable]
    public class VacationTypeDTO
    {
        private long m_EmployeeVacationTypeId;

        public long EmployeeVacationTypeId
        {
            get { return m_EmployeeVacationTypeId; }
            set { m_EmployeeVacationTypeId = value; }
        }
        private string  m_EmployeeVacation;

        public string  EmployeeVacation
        {
            get { return m_EmployeeVacation; }
            set { m_EmployeeVacation = value; }
        }

    }   
}
