﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
   public class PersonnelDTO
    {
        #region Private Variables

        private String firstName;
        private String middleName;
        private String userName;
        private String ssn;
        private String lastName;
        private Int64 personnelID;
        private String referenceNumber;
        private bool isActive;
        private String personnelTitle;
        private String payCodeDescription;
        private String emailID;
        private bool isDeleted;
        private bool isAdmin;
        private Int64 personId;

        #endregion

        #region Public Property


        public String FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }


        public String MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }

        public String LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public String UserName
        {
            get { return userName; }
            set { userName = value; }
        }
        public String SSN
        {
            get { return ssn; }
            set { ssn = value; }
        }

        public Int64 PersonnelID
        {
            get { return personnelID; }
            set { personnelID = value; }
        }

        public String ReferenceNumber
        {
            get { return referenceNumber; }
            set { referenceNumber = value; }
        }
        
        public bool IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public String PersonnelTitle
        {
            get { return personnelTitle ; }
            set { personnelTitle = value; }
        }
        public String PayCodeDescription
        {
            get { return payCodeDescription; }
            set { payCodeDescription = value; }
        }
        public String EmailID
        {
            get { return emailID; }
            set { emailID = value; }
        }
        public bool IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }
        public bool IsAdmin
        {
            get { return isAdmin; }
            set { isAdmin = value; }
        }

        public Int64 PersonId
        {
            get { return personId; }
            set { personId = value; }
        }

        public DateTime? DOB { get; set; }
        public String Gender { get; set; }
        #endregion
    }
}
