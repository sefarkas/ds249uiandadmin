﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class ApplicationDTO
    {
        public ApplicationDTO()
        {
            personalID = -1;
            applicationID = -1;
            applicationDesc = String.Empty;
            roles = null;
            memberships = null;
        }

        #region Private Variables

        private Int64 personalID;
        private Int64 applicationID;
        private String applicationDesc;
        private Int16 Id;
        private Int16 roleID;
        private String role;
        private List<ApplicationRoleDTO> roles;
        private List<MembershipDTO> memberships;


        #endregion

        #region Public Property

        public Int64 PersonalID
        {
            get { return personalID; }
            set { personalID = value; }
        }

        public Int64 ApplicationID
        {
            get { return applicationID; }
            set { applicationID = value; }
        }

        public String ApplicationDesc
        {
            get { return applicationDesc; }
            set { applicationDesc = value; }
        }

        public List<MembershipDTO> Memberships
        {
            get { return memberships; }
            set { memberships = value; }
        }


        public List<ApplicationRoleDTO> Roles
        {
            get { return roles; }
            set { roles = value; }
        }
        public Int16 ID
        {
            get { return Id; }
            set { Id = value; }
        }
        public Int16 RoleID
        {
            get { return roleID; }
            set { roleID = value; }
        }

        public String RoleDesc
        {
            get { return role; }
            set { role = value; }
        }
        #endregion
    }
}
