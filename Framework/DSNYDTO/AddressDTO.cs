﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
   public class AddressDTO
    {

       public AddressDTO()
       {
           addressID = -1;
           addressTypeID = -1;
           streetNumber = string.Empty;
           streetName = string.Empty;
           aptNo = string.Empty;
           city = string.Empty;
           state = string.Empty;
           zipCode = string.Empty;
           zipSuffix = string.Empty;
       }

       private long addressID;
       private Int16 addressTypeID;
       private string streetNumber;
       private string streetName;
       private string aptNo;
       private string city;
       private string state;
       private string zipCode;
       private string zipSuffix;

       public long AddressID
       {
           get { return addressID; }
           set { addressID = value; }
       }
       public Int16 AddressTypeID
       {
           get { return addressTypeID; }
           set { addressTypeID = value; }
       }
       public String StreetNumber
       {
           get { return streetNumber; }
           set { streetNumber = value; }
       }
       public String StreetName
       {
           get { return streetName; }
           set { streetName = value; }
       }
       public String AptNo
       {
           get { return aptNo; }
           set { aptNo = value; }
       }
       public String City
       {
           get { return city; }
           set { city = value; }
       }
       public String State
       {
           get { return state; }
           set { state = value; }
       }

       public String ZipCode
       {
           get { return zipCode; }
           set { zipCode = value; }
       }

       public String ZipSuffix
       {
           get { return zipSuffix; }
           set { zipSuffix = value; }
       }

    }
}
