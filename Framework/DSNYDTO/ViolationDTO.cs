﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using DSNY.DSNYCP.Proxys;

namespace DSNY.DSNYCP.DTO
{

   public class ViolationDTO
    {

       //Proxy proxy;

       public ViolationDTO()
       {
           uniqueID = 0;
           chargeID = -1;
           complaintID = -1;
           voilationID = -1;
           voilationDesc = String.Empty;
           charge = null;
           overideCharge = String.Empty;
           primaryInd = false;
           incidentLocationID = -1;
           incidentLocationName = String.Empty;
           incidentType = String.Empty;
           incedentList = null;
           incidentDate = DateTime.MinValue;
       }

        #region Private Variables
            private Int64 chargeID;
            private Int16 uniqueID;            
            private Int64 complaintID;
            private Int64 voilationID;
            private String voilationDesc;    
            private ChargeDTO charge;
            private String overideCharge;
            private Boolean primaryInd;
            private Int64 incidentLocationID;
            private String incidentLocationName;
            private String incidentType;
            //private IncedentList incedentList;            
            private List<IncedentDTO> incedentList;
            private String overRideVoilationDesc;
            private DateTime incidentDate;
        #endregion


        #region Public Property

            public Int16 UniqueID
            {
                get { return uniqueID; }
                set { uniqueID = value; }
            }

            public Int64 ChargeID
            {
                get { return chargeID; }
                set { chargeID = value; }
            }
            public Int64 ComplaintID
            {
                get { return complaintID; }
                set { complaintID = value; }
            }
       
            public Int64 VoilationID
            {
                get { return voilationID; }
                set { voilationID = value; }
            }

            public String VoilationDesc
            {
                get { return voilationDesc; }
                set { voilationDesc = value; }
            }

            public ChargeDTO Charge
            {
                get { return charge; }
                set { charge = value; }
            }

            public String OverideCharge
            {
                get { return overideCharge; }
                set { overideCharge = value; }
            }

            public Boolean PrimaryIndicator
            {
                get { return primaryInd; }
                set { primaryInd = value; }
            }

            public Int64 IncidentLocationID
            {
                get { return incidentLocationID; }
                set { incidentLocationID = value; }
            }

            public String IncidentLocationName
            {
                get { return incidentLocationName; }
                set { incidentLocationName = value; }
            }
       
            public String IncidentType
            {
                get { return incidentType; }
                set { incidentType = value; }
            }

            public List<IncedentDTO> Incidents
            {
                get { return incedentList; }
                set { incedentList = value; }
            }

            public String OverRideVoilationDesc
            {
                get { return overRideVoilationDesc; }
                set { overRideVoilationDesc = value; }
            }

            public DateTime IncidentDate
            {
                get { return incidentDate; }
                set { incidentDate = value; }
            }

        #endregion

       
    }
}
