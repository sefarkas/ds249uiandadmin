﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
  public class BCADStatusDTO
    {

        #region Private Variables
      public BCADStatusDTO()
      {
          codeID = -1;
          codeName = string.Empty;
          codeDescription = string.Empty;
      }

        private Int16 codeID ;
        private String codeName;
        private String codeDescription;

        #endregion

        #region Public Property

        public Int16 CodeID
        {
            get { return codeID; }
            set { codeID = value; }
        }

        public String CodeName
        {
            get { return codeName; }
            set { codeName = value; }
        }

        public String CodeDescription
        {
            get { return codeDescription; }
            set { codeDescription = value; }
        }

        #endregion
    }
}
