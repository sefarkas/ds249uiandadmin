﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class DIFDTO
    {
        private long  m_PersonalId;

        public long  PersonalId
        {
            get { return m_PersonalId; }
            set { m_PersonalId = value; }
        }
        private string m_Relationship;

        public string Relationship
        {
            get { return m_Relationship; }
            set { m_Relationship = value; }
        }
        private DateTime m_FromDate;

        public DateTime FromDate
        {
            get { return m_FromDate; }
            set { m_FromDate = value; }
        }
        private DateTime m_ToDate;

        public DateTime ToDate
        {
            get { return m_ToDate; }
            set { m_ToDate = value; }
        }

        private string m_CreatedBy;

        public string CreatedBy
        {
            get { return m_CreatedBy; }
            set { m_CreatedBy = value; }
        }

        private DateTime m_CreatedDate;

        public DateTime CreatedDate
        {
            get { return m_CreatedDate; }
            set { m_CreatedDate = value; }
        }

        private int m_StatusID;

        public int StatusID
        {
            get { return m_StatusID; }
            set { m_StatusID = value; }
        }

        private string m_SignOffUser;

        public string SignOffUser
        {
            get { return m_SignOffUser; }
            set { m_SignOffUser = value; }
        }

        private DateTime m_SignOffDate;


        public DateTime SignOffDate
        {
            get { return m_SignOffDate; }
            set { m_SignOffDate = value; }
        }

        private Int64 m_DeathInFamilyId;

        public Int64 DeathInFamilyId
        {
            get { return m_DeathInFamilyId; }
            set { m_DeathInFamilyId = value; }
        }
    }
}
