﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class ComplaintAdvocacyDTO
    {
        #region Private Variable
        private Int64 _advocacyCaseId = -1;
        private Int64 _complaintId = -1;
        private Int64 _caseClassificationcd = -1;
        private Int64 _originationSourcecd = -1;
        private Int64 _specialStatus_cd = -1;
        private Int64 _trialReturnStatuscd = -1;
        private DateTime _trialReturnDate = DateTime.MinValue;
        private Int64 _reviewStatuscd = -1;
        private DateTime _reviewStatusDate = DateTime.MinValue;
        private Int64 _actioncd = -1;
        private DateTime _actionDate = DateTime.MinValue;
        private short _isClosed = -1;
        private DateTime _closedDate = DateTime.MinValue;
        private Int64 _cobStatus_cd = -1;
        private DateTime _cobDate = DateTime.MinValue;
        private Int16 _finalStatuscd = -1;
        private String _caseComment = String.Empty;
        private Int16 _recieveUser = -1;
        private String _receiveUserName = String.Empty;
        private DateTime _recieveDate = DateTime.MinValue;
        private Int16 _modifiedUser = -1;
        private String _modifiedUserName = String.Empty;
        private DateTime _modifiedDate = DateTime.MinValue;
        private String _isAvailable = String.Empty;
        private bool isProbation;
        private List<AdvocateAppealDTO> _appeals;
        private List<AdvocatePenaltyDTO> _penaltys;
        private List<CombinedPenaltyDTO> _CombPenaltys;
        private List<AdvocateAssociatedComplaintDTO> _assComplaints;
        private List<AdvocateHearingDTO> _hearings;
        private List<ViolationDTO> _charges;
        
        #endregion

        #region Public Properties
        public Int64 AdvocacyCaseId
        {
            get { return _advocacyCaseId; }
            set { _advocacyCaseId = value; }
        }
        public Int64 ComplaintId
        {
            get { return _complaintId; }
            set { _complaintId = value; }
        }
        public Int64 CaseClassificationcd
        {
            get { return _caseClassificationcd; }
            set { _caseClassificationcd = value; }
        }
        public Int64 OriginationSourcecd
        {
            get { return _originationSourcecd; }
            set { _originationSourcecd = value; }
        }
        public Int64 SpecialStatus_cd
        {
            get { return _specialStatus_cd; }
            set { _specialStatus_cd = value; }
        }
        public Int64 TrialReturnStatuscd
        {
            get { return _trialReturnStatuscd; }
            set { _trialReturnStatuscd = value; }
        }
        public DateTime TrialReturnDate
        {
            get { return _trialReturnDate; }
            set { _trialReturnDate = value; }
        }
        public Int64 ReviewStatuscd
        {
            get { return _reviewStatuscd; }
            set { _reviewStatuscd = value; }
        }
        public DateTime ReviewStatusDate
        {
            get { return _reviewStatusDate; }
            set { _reviewStatusDate = value; }
        }
        public Int64 Actioncd
        {
            get { return _actioncd; }
            set { _actioncd = value; }
        }
        public DateTime? ActionDate { get; set; }
       
        public Int16 isClosed
        {
            get { return _isClosed; }
            set { _isClosed = value; }
        }
        public DateTime? ClosedDate {get; set;}
       
        public Int64 COBStatus_cd
        {
            get { return _cobStatus_cd; }
            set { _cobStatus_cd = value; }
        }
        public DateTime? COBDate{get;set;}
     
        public Int16 FinalStatuscd
        {
            get { return _finalStatuscd; }
            set { _finalStatuscd = value; }
        }
        public String CaseComment
        {
            get { return _caseComment; }
            set { _caseComment = value; }
        }
        public Boolean IsProbation
        {
            get { return isProbation; }
            set { isProbation = value; }
        }
        public Int16 RecieveUser
        {
            get { return _recieveUser; }
            set { _recieveUser = value; }
        }
        public String RecieveUserName
        {
            get { return _receiveUserName; }
            set { _receiveUserName = value; }
        }
        public DateTime RecieveDate
        {
            get { return _recieveDate; }
            set { _recieveDate = value; }
        }
        public Int16 ModifiedUser
        {
            get { return _modifiedUser; }
            set { _modifiedUser = value; }
        }
        public String ModifiedUserName
        {
            get { return _modifiedUserName; }
            set { _modifiedUserName = value; }
        }
        public DateTime ModifiedDate
        {
            get { return _modifiedDate; }
            set { _modifiedDate = value; }
        }
        public String IsAvailable
        {
            get { return _isAvailable; }
            set { _isAvailable = value; }
        }

        public List<AdvocateAppealDTO> Appeals
        {
            get { return _appeals; }
            set { _appeals = value; }
        }

        public List<AdvocateAssociatedComplaintDTO> AssociatedComplaints
        {
            get { return _assComplaints; }
            set { _assComplaints = value; }
        }

        public List<AdvocateHearingDTO> Hearings
        {
            get { return _hearings; }
            set { _hearings = value; }
        }

        public List<AdvocatePenaltyDTO> Penaltys
        {
            get { return _penaltys; }
            set { _penaltys = value; }
        }

        public List<CombinedPenaltyDTO> CombPenaltys
        {
            get { return _CombPenaltys; }
            set { _CombPenaltys = value; }
        }


        public List<ViolationDTO> Charges
        {
            get { return _charges; }
            set { _charges = value; }
        }
        #endregion
    }
}
