﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class EmployeeVacationDetailsDTO
    {
        private VacationTypeDTO m_VacationType;

        public VacationTypeDTO VacationType
        {
            get { return m_VacationType; }
            set { m_VacationType = value; }
        }

        private DateTime m_FromDate;

        public DateTime FromDate
        {
            get { return m_FromDate; }
            set { m_FromDate = value; }
        }
        private DateTime m_ToDate;

        public DateTime ToDate
        {
            get { return m_ToDate; }
            set { m_ToDate = value; }
        }

        private int m_NoOfDays;

        public int NoOfDays
        {
            get { return m_NoOfDays; }
            set { m_NoOfDays = value; }
        }

        private string m_Value;

        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

    }
}
