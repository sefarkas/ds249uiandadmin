﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    /// <summary>
    /// DTO FOR COMPLAINT BY DISTRICT
    /// </summary>
  public class ComplaintByDistrictDTO
    {
        public ComplaintByDistrictDTO()
        {
            borough = string.Empty;
            district = string.Empty;
            noOfComplaints = -1;
        }
        private string borough;
        private string district;
        private Int64 noOfComplaints;

        public String Borough
        {
            get { return borough; }
            set { borough = value; }
        }

        public String District
        {
            get { return district; }
            set { district = value; }
        }

        public Int64 NoOfComplaints
        {
            get { return noOfComplaints; }
            set { noOfComplaints = value; }
        }
    }
}
