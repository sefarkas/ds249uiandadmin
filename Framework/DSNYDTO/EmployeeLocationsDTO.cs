﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class EmployeeLocationsDTO
    {
        public Int64 EmployeeLocationID {get;set;}
        public Int64 EmployeeID {get;set;}
        public Int64 EmployeeTitleID {get;set;}
        public Int64 LocationID {get;set;}
        public bool IsCurrent {get;set;}
    }
}
