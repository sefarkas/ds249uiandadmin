﻿
namespace DSNY.DSNYCP.DTO
{
    public class Common
    {
        #region Enum - Direction
        public enum OrderDirection
        {
            Asc,
            Desc
        }
        public enum DupOrNJ
        {
            Duplicate,
            NJ,
            NONE,
        }
        #endregion

        #region GroupBy Boro
        public class GroupByBoro
        {
            public long GroupCount { get; set; }
            public long BoroId { get; set; }
            public string BoroName { get; set; }
        }
        #endregion

        #region GroupBy Segment
        public class GroupBySegment
        {
            public long GroupCount { get; set; }
            public long SegmentId { get; set; }
        }
        #endregion
    }
}
