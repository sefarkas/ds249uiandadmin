﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
  public class CodeDTO
    {
      public CodeDTO()
      {
          codeID = -1;
          codeName = string.Empty;
          codeDescription = string.Empty;
          valueDataType = string.Empty;
      }

        #region Private Variables

        private Int16 codeID;
        private String codeName;
        private String codeDescription;
        private string valueDataType;

        #endregion

        #region Public Property

        public Int16 CodeID
        {
            get { return codeID; }
            set { codeID = value; }
        }

        public String CodeName
        {
            get { return codeName; }
            set { codeName = value; }
        }

        public String CodeDescription
        {
            get { return codeDescription; }
            set { codeDescription = value; }
        }
        public String ValueDataType
        {
            get { return valueDataType; }
            set { valueDataType = value; }
        }

        #endregion
    }
}
