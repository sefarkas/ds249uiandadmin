﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class ComplaintHistoryDTO
    {
        public ComplaintHistoryDTO()
        {
            voilationDate = DateTime.MinValue;
            complaintID = -1;
            voilations = string.Empty;
            voilationDesc = String.Empty;
            penalty = string.Empty;
            routingLocation = string.Empty;                     
        }

        #region Private Variables
        private DateTime voilationDate;
        private Int64 complaintID;
        private String voilations;
        private String voilationDesc;
        private String routingLocation;
        private String penalty;
        private Int64 indexNo;
             
        #endregion

        #region Public Property

       

        public DateTime VoilationDate
        {
            get { return voilationDate; }
            set { voilationDate = value; }
        }

        public String VoilationDateAsString
        {
            get { return voilationDate.ToShortDateString(); }
            
        }

        public Int64 ComplaintID
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public String Voilations
        {
            get { return voilations;}
            set { voilations = value; }
        }

        public String VoilationDesc
        {
            get { return voilationDesc; }
            set { voilationDesc = value; }
        }

        public String Penalty
        {
            get { return penalty; }
            set { penalty = value; }
        }

        public String RoutingLocation
        {
            get { return routingLocation; }
            set { routingLocation = value; }
        }

        public Int64 IndexNo
        {
            get { return indexNo; }
            set { indexNo = value; }
        }
        
        #endregion

        
    }
}
