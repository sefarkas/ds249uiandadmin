﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class EmployeeVacationApprovalDetailsDTO
    {
        private VacationTypeDTO m_VacationType;

        public VacationTypeDTO VacationType
        {
            get { return m_VacationType; }
            set { m_VacationType = value; }
        }

        private string m_Value;

        public string Value
        {
            get { return m_Value; }
            set { m_Value = value; }
        }

        private DateTime m_DateApproved;

        public DateTime DateApproved
        {
            get { return m_DateApproved; }
            set { m_DateApproved = value; }
        }
        private string m_SignOffUser;

        public string SignOffUser
        {
            get { return m_SignOffUser; }
            set { m_SignOffUser = value; }
        }

        private int m_EmployeeVacationInfoId;

        public int EmployeeVacationInfoId
        {
            get { return m_EmployeeVacationInfoId; }
            set { m_EmployeeVacationInfoId = value; }
        }
    }
}
