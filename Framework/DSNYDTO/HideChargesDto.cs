﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
  public class HideChargesDto
    {

      public HideChargesDto()
      {
          viewChargeId = -1;
          chargeTypeId = -1;
          employeeId = -1;
          chargeDescription = string.Empty;
          referenceNumber = string.Empty;
      }

        #region Private Variables

        private Int16 viewChargeId;
        private Int16 chargeTypeId;
        private Int16 employeeId;
        private String chargeDescription;
        private String referenceNumber;

        #endregion

        #region Public Property

        public Int16 ViewChargeId
        {
            get { return viewChargeId; }
            set { viewChargeId = value; }
        }

        public Int16 ChargeTypeId
        {
            get { return chargeTypeId; }
            set { chargeTypeId = value; }
        }

        public Int16 EmployeeId
        {
            get { return employeeId; }
            set { employeeId = value; }
        }
        public String ChargeDescription
        {
            get { return chargeDescription; }
            set { chargeDescription = value; }
        }

        public String ReferenceNumber
        {
            get { return referenceNumber; }
            set { referenceNumber = value; }
        }
        #endregion
    }
}
