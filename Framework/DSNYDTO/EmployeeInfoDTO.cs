﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class EmployeeInfoDTO
    {
        public EmployeeInfoDTO()
        {
            firstName = string.Empty;
            lastName = string.Empty;
            imageID = -1;
            images = null;
            referenceNo = -1;
        }

        # region variables
        private string firstName;
        private string lastName;
        private Int32 imageID;
        private Byte[] images;
        private Int32 referenceNo;
        # endregion
        # region properties
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
       
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public Int32 ImageID
        {
            get { return imageID; }
            set { imageID = value; }
        }
        public byte[] Images
        {
            get { return images; }
            set { images = value; }
        }

        public Int32 ReferenceNo
        {
            get { return referenceNo; }
            set { referenceNo = value; }
        }

        # endregion
    }
}
