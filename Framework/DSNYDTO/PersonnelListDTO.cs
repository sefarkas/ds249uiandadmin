﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
  public class PersonnelListDTO
    {
        #region Private Variables

        private String firstName;
        private String middleName;
        private String lastName;
        private Int64 personnelID;
        private String referenceNumber;

        #endregion

        #region Public Property


        public String FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }


        public String MiddleName
        {
            get { return middleName; }
            set { middleName = value; }
        }

        public String LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }


        public Int64 PersonnelID
        {
            get { return personnelID; }
            set { personnelID = value; }
        }

        public String ReferenceNumber
        {
            get { return referenceNumber; }
            set { referenceNumber = value; }
        }

        #endregion
    }
}
