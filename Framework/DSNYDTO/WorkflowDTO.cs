﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    [Serializable]
    public class WorkflowDTO
    {
        private int m_ApplicationId;
        public int ApplicationId
        {
            get { return m_ApplicationId; }
            set { m_ApplicationId = value; }
        }
        private Guid  m_WorkflowId;

        public Guid WorkflowId
        {
            get { return m_WorkflowId; }
            set { m_WorkflowId = value; }
        }
        private int m_GroupId;

        public int GroupId
        {
            get { return m_GroupId; }
            set { m_GroupId = value; }
        }
        private string m_UserId;

        public string UserId
        {
            get { return m_UserId; }
            set { m_UserId = value; }
        }
        private int m_StatusId;

        public int StatusId
        {
            get { return m_StatusId; }
            set { m_StatusId = value; }
        }
        private string m_Description;

        public string Description
        {
            get { return m_Description; }
            set { m_Description = value; }
        }
        private DateTime m_ApproveDate;

        public DateTime ApproveDate
        {
            get { return m_ApproveDate; }
            set { m_ApproveDate = value; }
        }
        private string m_ApprovedBy;

        public string ApprovedBy
        {
            get { return m_ApprovedBy; }
            set { m_ApprovedBy = value; }
        }
        private DateTime m_CreateDate;

        public DateTime CreateDate
        {
            get { return m_CreateDate; }
            set { m_CreateDate = value; }
        }

        
    }
}
