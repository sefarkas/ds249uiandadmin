﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
   public class EmployeeBadgesDTO
    {
        public Int64 EmployeeBadgeID {get;set;}
        public Int64 EmployeeTitleID {get;set;}
        public Int64 BadgeNo {get;set;}
        public bool IsCurrent { get; set; }     
    }
}
