﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class BCADPenaltyDTO
    {
        #region Private Variables

        private Int16 uniqueID = -1;
        private Int64 penaltyID = -1;
        private Int64 penaltyGroupID = -1;
        private Int16 penaltyTypeCode = -1;
        private String days = String.Empty;
        private String hours = String.Empty;
        private String minutes = String.Empty;
        private String moneyValue = String.Empty;
        private String notes = String.Empty;
        private int modifiedUser = -1;
        private DateTime modifiedDate = DateTime.MinValue;

        #endregion

        #region Public Property

        public Int16 UniqueID
        {
            get { return uniqueID; }
            set { uniqueID = value; }
        }

        public Int64 PenaltyID
        {
            get { return penaltyID; }
            set { penaltyID = value; }
        }

        public Int64 PenaltyGroupID
        {
            get { return penaltyGroupID; }
            set { penaltyGroupID = value; }
        }

        public Int16 PenaltyTypeCode
        {
            get { return penaltyTypeCode; }
            set { penaltyTypeCode = value; }
        }

        public String Days
        {
            get { return days; }
            set { days = value; }
        }

        public String Minutes
        {
            get { return minutes; }
            set { minutes = value; }
        }

        public String Hours
        {
            get { return hours; }
            set { hours = value; }
        }

        public String MoneyValue
        {
            get { return moneyValue; }
            set { moneyValue = value; }
        }

        public String Notes
        {
            get { return notes; }
            set { notes = value; }
        }

        public int ModifiedUser
        {
            get { return modifiedUser; }
            set { modifiedUser = value; }
        }

        public DateTime ModifiedDate
        {
            get { return modifiedDate; }
            set { modifiedDate = value; }
        }
        #endregion
    }
}
