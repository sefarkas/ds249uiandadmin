﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class AdvocatePenaltyDTO
    {

        private Int16 _uniqueID = -1;
        private Int64 _advocatePenaltyID = -1;
        private Int64 _advocacyCaseID = -1;
        private Int64 _penaltyTypeCd = -1;
        private String _dataValue = String.Empty;
        private Int16 _suspensionDays = -1;
        private Int16 _fineDays = -1;
        private Boolean _applyToTotal;
        private String _comments = String.Empty;
        private String _demotedRank;
        private String _acd;
        private Int16 _vacDays;
        private Boolean _combpen;
        private Boolean _randomTest;
        private Int16 _timeServed;
        private Int16 _term= -1;
        private String _term_desc = String.Empty;
        private String _value_data_type = String.Empty;


        public Int16 UniqueID
        {
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }
        public Int64 AdvocatePenaltyID
        {
            get { return _advocatePenaltyID; }
            set { _advocatePenaltyID = value; }
        }
        public Int64 AdvocacyCaseID
        {
            get { return _advocacyCaseID; }
            set { _advocacyCaseID = value; }
        }
        public Int64 PenaltyTypeCd
        {
            get { return _penaltyTypeCd; }
            set { _penaltyTypeCd = value; }
        }
        public String DataValue
        {
            get { return _dataValue; }
            set { _dataValue = value; }
        }
        public String Value_Data_Type
        {
            get { return _value_data_type; }
            set { _value_data_type = value; }
        }
        public Int16 SuspensionDays
        {
            get { return _suspensionDays; }
            set { _suspensionDays = value; }
        }
        public String Term_Desc
        {
            get { return _term_desc; }
            set { _term_desc = value; }
        }
        public Int16 Term
        {
            get { return _term; }
            set { _term = value; }
        }
        public Int16 FineDays
        {
            get { return _fineDays; }
            set { _fineDays = value; }
        }
        public Boolean ApplyToTotal
        {
            get { return _applyToTotal; }
            set { _applyToTotal = value; }
        }
        public String DemotedRank
        {
            get { return _demotedRank; }
            set { _demotedRank = value; }
        }
        public String ACD
        {
            get { return _acd; }
            set { _acd = value; }
        }
        public Int16 VacDays
        {
            get { return _vacDays; }
            set { _vacDays = value; }
        }
        public Boolean CombPen
        {
            get { return _combpen; }
            set { _combpen = value; }
        }
        public Boolean RandomTest
        {
            get { return _randomTest; }
            set { _randomTest = value; }
        }
        public Int16 TimeServed
        {
            get { return _timeServed; }
            set { _timeServed = value; }
        }

        public String Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }
    }
}
