﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using DSNY.BIT.DSNYCP.DSNYProxy;

namespace DSNY.DSNYCP.DTO
{
    public class ComplaintDTO
    {
        //Proxy proxy; 
        public ComplaintDTO()
        {

         complaintID=-1;
         indexNo=-1;
         jt1 = string.Empty; 
         locationID=-1;
         boroughID = -1;
         promotionDate = DateTime.MinValue;
         locationName=string.Empty;

         //************PENDING DOCUMENT************
         pendingDoc = string.Empty;

         employeeID=-1;
         refNumber=string.Empty;
         titleID=-1;
         badgeNumber=string.Empty;
         incidentDate=DateTime.MinValue;
         incidentType=string.Empty;
         parentIndexNo = -1;
         finalstatuscd = -1;       

         comments=string.Empty;
         complaintActionID=-1;
         incidentLocationID=-1;
         incidentLocationName=string.Empty;
         isSubmitted=false;
         suspensionOrderedBy=-1;
         suspensionOrderedByName=string.Empty;
         suspensionOrderedByRefNo=string.Empty;
         suspensionOrderedDt=DateTime.MinValue;
         chargesServedBy=-1;
         chargesServedByName=string.Empty;
         chargesServedByRefNo=string.Empty;
         chargesServedDate=DateTime.MinValue;
         suspensionLiftedBy=-1;
         suspensionLiftedByName=string.Empty;
         suspensionLiftedByRefNo=string.Empty;
         suspensionLiftedDt=DateTime.MinValue;
         routingLocation=string.Empty;
         isProbation=false;
         isApproved=false;
         approvedDate=DateTime.MinValue;
         createdBy=string.Empty;
         createdDate=DateTime.MinValue;
         status=string.Empty;
         chartNumber=string.Empty;
         payCodeID=-1;
         payCodeDesc=string.Empty;
         vacationSchedule=string.Empty;
         charge=string.Empty;
         chargeDesc=string.Empty;


             respondentFName=string.Empty;
         respondentMName=string.Empty;
         respondentLName=string.Empty;
         respondentDOB=DateTime.MinValue;
         respondentAptDt=DateTime.MinValue;
         titleDesc=string.Empty;
         complaintStatus=-1;
         complaintStatusDate=DateTime.MinValue;


        //Address Part

         streetNumber=string.Empty;
         streetName=string.Empty;
         aptNo=string.Empty;
         city=string.Empty;
         state=string.Empty;
         zip=string.Empty;
         zipSuffix=string.Empty;

        // Advocate / BCAD Variables      


         indicator=string.Empty;
         isDeleted=false;
         dataSource=string.Empty;
         sentDate=DateTime.MinValue;
        

        }

        #region Private Variables

        private Int64 complaintID;
        private Int64 indexNo;
        private String jt1;
        private Int64 locationID;
        private Int64 boroughID;
        private DateTime promotionDate;
        private String locationName;

        //************PENDING DOCUMENT************
        private String pendingDoc;

        private Int64 employeeID;
        private String refNumber;
        private Int64 titleID;
        private String badgeNumber;
        private DateTime incidentDate;
        private String incidentType;
        private Int64 parentIndexNo;
        private Int64 finalstatuscd;
       

        private List<ViolationDTO> voilations;
        private List<WitnessDto> witnessess;
        private List<ComplainantDTO> complainants;
        private List<IncedentDTO> incedents;
        private List<ComplaintHistoryDTO> historyList;
       
        private String comments;
        private int complaintActionID;
        private Int64 incidentLocationID;
        private String incidentLocationName;
        private bool isSubmitted;
        private Int64 suspensionOrderedBy;
        private String suspensionOrderedByName;
        private String suspensionOrderedByRefNo;
        private DateTime suspensionOrderedDt;
        private Int64 chargesServedBy;
        private String chargesServedByName;
        private String chargesServedByRefNo;
        private DateTime chargesServedDate;
        private Int64 suspensionLiftedBy;
        private String suspensionLiftedByName;
        private String suspensionLiftedByRefNo;
        private DateTime suspensionLiftedDt;
        private String routingLocation;
        private bool isProbation;
        private bool isApproved;
        private DateTime approvedDate;
        private String createdBy;
        private DateTime createdDate;
        private String status;
        private String chartNumber;
        private Int16 payCodeID;
        private String payCodeDesc;
        private String vacationSchedule;
        private String charge;
        private String chargeDesc;
       

        public String getChargeDesc()
        {
            return chargeDesc;
        }

        public void setChargeDesc(String value)
        {
            chargeDesc = value;
        }

        public String getComments()
        {
            return comments;
        }

        public void setComments(String value)
        {
            comments = value;
        }

        private String respondentFName;
        private String respondentMName;
        private String respondentLName;
        private DateTime respondentDOB;
        private DateTime respondentAptDt;
        private String titleDesc;
        private Int16 complaintStatus;
        private DateTime complaintStatusDate;


        //Address Part

        private String streetNumber;
        private String streetName;
        private String aptNo;
        private String city;
        private String state;
        private String zip;
        private String zipSuffix;

        // Advocate / BCAD Variables

        private Int64 caseID;


        private string indicator;
        private Boolean isDeleted;
        private string dataSource;
        private DateTime sentDate;
        

        #endregion

        #region Public Property

        public Int64 ComplaintID
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int64 IndexNo
        {
            get { return indexNo; }
            set { indexNo = value; }
        }
        
        public Int64 ParentIndexNo
        {
            get { return parentIndexNo; }
            set { parentIndexNo = value; }
        }
        
        public Int64 FinalStatusCD
        {
            get { return finalstatuscd; }
            set { finalstatuscd = value; }
        }

        public Int64 LocationID
        {
            get { return locationID; }
            set { locationID = value; }
        }
        public Int64 BoroughID
        {
            get { return boroughID; }
            set { boroughID = value; }
        }
        public DateTime? PromotionDate {get;set;}
       
        public String LocationName
        {
            get { return locationName; }
            set { locationName = value; }
        }

        //************PENDING DOCUMENT************
        public String PendingDoc
        {
            get { return pendingDoc; }
            set { pendingDoc = value; }
        }


        public Int64 EmployeeID
        {
            get { return employeeID; }
            set { employeeID = value; }
        }

        public String ReferenceNumber
        {
            get { return refNumber; }
            set { refNumber = value; }
        }

        public Int64 TitleID
        {
            get { return titleID; }
            set { titleID = value; }
        }

        public String BadgeNumber
        {
            get { return badgeNumber; }
            set { badgeNumber = value; }
        }

        public DateTime IncidentDate
        {
            get { return incidentDate; }
            set { incidentDate = value; }
        }

        public String IncidentType
        {
            get { return incidentType; }
            set { incidentType = value; }
        }
       
        public List<ViolationDTO> Voilations
        {
            get { return voilations; }
            set { voilations = value; }
        }

        public List<WitnessDto> Witnesses
        {
            get { return witnessess; }
            set { witnessess = value; }
        }

        public List<ComplainantDTO> Complainants
        {
            get { return complainants; }
            set { complainants = value; }
        }

        public List<IncedentDTO> Incedents
        {
            get { return incedents; }
            set { incedents = value; }
        }

        public List<ComplaintHistoryDTO> HistoryList
        {
            get { return historyList; }
            set { historyList = value; }
        }
       

        public String Comments
        {
            get { return comments; }
            set { comments = value; }
        }

        public int ComplaintActionID
        {
            get { return complaintActionID; }
            set { complaintActionID = value; }
        }

        public Int64 IncidentLocationID
        {
            get { return incidentLocationID; }
            set { incidentLocationID = value; }
        }

        public String IncidentLocation
        {
            get { return incidentLocationName; }
            set { incidentLocationName = value; }
        }

        public Boolean IsSubmitted
        {
            get { return isSubmitted; }
            set { isSubmitted = value; }
        }

        public DateTime ChargesServedDate
        {
            get { return chargesServedDate; }
            set { chargesServedDate = value; }
        }

        public Int64 ChargesServedBy
        {
            get { return chargesServedBy; }
            set { chargesServedBy = value; }
        }

        public String ChargesServedByName
        {
            get { return chargesServedByName; }
            set { chargesServedByName = value; }
        }

        public String ChargesServedByRefNo
        {
            get { return chargesServedByRefNo; }
            set { chargesServedByRefNo = value; }
        }

        public Int64 SuspensionOrderedById
        {
            get { return suspensionOrderedBy; }
            set { suspensionOrderedBy = value; }
        }

        public String SuspensionOrderedByName
        {
            get { return suspensionOrderedByName; }
            set { suspensionOrderedByName = value; }
        }

        public String SuspensionOrderedByRefNo
        {
            get { return suspensionOrderedByRefNo; }
            set { suspensionOrderedByRefNo = value; }
        }

        public DateTime SuspensionOrderDate
        {
            get { return suspensionOrderedDt; }
            set { suspensionOrderedDt = value; }
        }

        public Int64 SuspensionLiftedBy
        {
            get { return suspensionLiftedBy; }
            set { suspensionLiftedBy = value; }
        }

        public String SuspensionLiftedByName
        {
            get { return suspensionLiftedByName; }
            set { suspensionLiftedByName = value; }
        }

        public String SuspensionLiftedByRefNo
        {
            get { return suspensionLiftedByRefNo; }
            set { suspensionLiftedByRefNo = value; }
        }

        public DateTime SuspensionLifedDate
        {
            get { return suspensionLiftedDt; }
            set { suspensionLiftedDt = value; }
        }

        public String RoutingLocation
        {
            get { return routingLocation; }
            set { routingLocation = value; }
        }

        public Boolean IsProbation
        {
            get { return isProbation; }
            set { isProbation = value; }
        }

        public Boolean IsApproved
        {
            get { return isApproved; }
            set { isApproved = value; }
        }

        public DateTime ApprovedDate
        {
            get { return approvedDate; }
            set { approvedDate = value; }
        }

        public String CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        public String Status
        {
            get { return status; }
            set { status = value; }
        }

        public String Charge
        {
            get { return charge; }
            set { charge = value; }
        }

        public String ChargeDesc
        {
            get { return chargeDesc; }
            set { chargeDesc = value; }
        }

        public String RespondentFName
        {
            get { return respondentFName; }
            set { respondentFName = value; }
        }

        public String RespondentMName
        {
            get { return respondentMName; }
            set { respondentMName = value; }
        }

        public String RespondentLName
        {
            get { return respondentLName; }
            set { respondentLName = value; }
        }

        public DateTime RespondentDOB
        {
            get { return respondentDOB; }
            set { respondentDOB = value; }
        }

        public DateTime RespondentAptDt
        {
            get { return respondentAptDt; }
            set { respondentAptDt = value; }
        }


        public String TitleDesc
        {
            get { return titleDesc; }
            set { titleDesc = value; }
        }

        public Int16 ComplaintStatus
        {
            get { return complaintStatus; }
            set { complaintStatus = value; }
        }

        public DateTime ComplaintStatusDate
        {
            get { return complaintStatusDate; }
            set { complaintStatusDate = value; }
        }

        public String FullName
        {
            get
            {
                if (respondentFName != string.Empty || respondentLName != string.Empty)
                    return RespondentFName.Trim() + " " + respondentMName.Trim() + " " + respondentLName.Trim();
                else
                    return String.Empty;
            }
        }

        public String InitialAndLastName
        {
            get
            {
                if (respondentFName != string.Empty || respondentLName != string.Empty)
                    return RespondentFName.Trim().Substring(0, 1) + ". " + respondentLName.Trim();
                else
                    return String.Empty;
            }
        }
        //Address

        public String StreetNumber
        {
            get { return streetNumber; }
            set { streetNumber = value; }
        }

        public String StreetName
        {
            get { return streetName; }
            set { streetName = value; }
        }

        public String AptNo
        {
            get { return aptNo; }
            set { aptNo = value; }
        }

        public String City
        {
            get { return city; }
            set { city = value; }
        }

        public String State
        {
            get { return state; }
            set { state = value; }
        }

        public String ZipCode
        {
            get { return zip; }
            set { zip = value; }
        }

        public String ZipSuffix
        {
            get { return zipSuffix; }
            set { zipSuffix = value; }
        }

        public String ChartNo
        {
            get { return chartNumber; }
            set { chartNumber = value; }
        }

        public String VacationSchedule
        {
            get { return vacationSchedule; }
            set { vacationSchedule = value; }
        }

        public Int16 PayCodeID
        {
            get { return payCodeID; }
            set { payCodeID = value; }
        }

        public String PayCodeDesc
        {
            get { return payCodeDesc; }
            set { payCodeDesc = value; }
        }

        public Int64 CaseID
        {
            get { return caseID; }
            set { caseID = value; }
        }



        public String Indicator
        {
            get { return indicator; }
            set { indicator = value; }
        }

        public Boolean IsDeleted
        {
            get { return isDeleted; }
            set { isDeleted = value; }
        }

        public String DataSource
        {
            get { return dataSource; }
            set { dataSource = value; }
        } 
     

        public DateTime SentDate
        {
            get { return sentDate; }
            set { sentDate = value; }
        }


        public String Jt1
        {
            get { return jt1; }
            set { jt1 = value; }
        }

        #endregion

        
    }
}
