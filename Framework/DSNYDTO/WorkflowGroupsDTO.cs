﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    [Serializable]
    public class WorkflowGroupsDTO
    {
        private WorkflowApplicationDTO m_Application;

        public WorkflowApplicationDTO Application
        {
            get { return m_Application; }
            set { m_Application = value; }
        }

        

        public string ApplicationName
        {
            get { return Application.ApplicationName; }
            
        }

        public int ApplicationId
        {
            get { return Application.ApplicationId ; }

        }
        private int m_GroupId;

        public int GroupId
        {
            get { return m_GroupId; }
            set { m_GroupId = value; }
        }
        private string m_GroupName;

        public string GroupName
        {
            get { return m_GroupName; }
            set { m_GroupName = value; }
        }

        private string m_ParentGroup;

        public string ParentGroupName
        {
            get { return m_ParentGroup; }
            set { m_ParentGroup = value; }
        }
    }
}
