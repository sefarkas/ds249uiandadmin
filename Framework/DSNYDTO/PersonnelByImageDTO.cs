﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class PersonnelByImageDTO
    {

        public PersonnelByImageDTO()
        {
            firstName = string.Empty;
            middleName = string.Empty;
            lastName = string.Empty;
            ssn = string.Empty;
            gender = string.Empty;
            dob = DateTime.MinValue;
            personnelID = -1;
            referenceNo = string.Empty;
            appointmentDate = DateTime.MinValue;
            payCodeID = -1;
            payCodeDescription = string.Empty;
            isBCAD = String.Empty;
            chartNumber = string.Empty;
            dsnyHireDate = DateTime.MinValue;
            nycHireDate = DateTime.MinValue;
            phoneNumber = string.Empty;
            leaveStatusCode = -1;
            probationCode = -1;
            probationStartDate = DateTime.MinValue;
            probationEndDate = DateTime.MinValue;
            vacationSchedule = string.Empty;
            badgeNumber = String.Empty;
            titleID = -1;
            isActive = false;
            personnelTitle = string.Empty;
            userName = string.Empty;
            emailID = string.Empty;
           
        }

        #region Private Variables
        /// <summary>
        /// Private Variables
        /// </summary>

        private Int32 imageID;
        private string firstName;
        private string middleName;
        private string lastName;

        private string ssn;
        private string gender;
        private DateTime dob;
        private Int64 personnelID;
        private string referenceNo;
        private DateTime appointmentDate;
        private Int16 payCodeID;
        private string payCodeDescription;
        private string isBCAD;
        private string chartNumber;
        private DateTime dsnyHireDate;
        private DateTime nycHireDate;
        private string phoneNumber;
        private Int16 leaveStatusCode;
        private Int16 probationCode;
        private DateTime probationStartDate;
        private DateTime probationEndDate;
        private string vacationSchedule;
        private string badgeNumber;
        private Int64 titleID;
        private Boolean isActive;
        private AddressDTO address;
        private string personnelTitle;

        private string userName;
        private string emailID;
        #endregion

        #region Public Property
        /// <summary>
        /// Public Property
        /// </summary>

        public Int32 ImageID
        {
            get { return imageID; }
            set { imageID = value; }
        }

        public String FirstName
        {
            get { return firstName.Trim(); }
            set { firstName = value; }
        }
        /// <summary>
        /// This method returns the last name of the user
        /// </summary>
        public String MiddleName
        {
            get { return middleName.Trim(); }
            set { middleName = value; }
        }
        /// <summary>
        /// This method returns the last name of the user
        /// </summary>
        public String LastName
        {
            get { return lastName.Trim(); }
            set { lastName = value; }
        }

        public String SSN
        {
            get { return ssn; }
            set { ssn = value; }
        }

        public DateTime DOB
        {
            get { return dob; }
            set { dob = value; }
        }

        public String Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        public Int64 PersonnelID
        {
            get { return personnelID; }
            set { personnelID = value; }
        }

        public string ReferenceNo
        {
            get { return referenceNo; }
            set { referenceNo = value; }
        }

        public DateTime AppointmentDate
        {
            get { return appointmentDate; }
            set { appointmentDate = value; }
        }


        public Int16 PayCodeID
        {
            get { return payCodeID; }
            set { payCodeID = value; }
        }

        public string PayCodeDescription
        {
            get { return payCodeDescription; }
            set { payCodeDescription = value; }
        }

        public string IsBCAD
        {
            get { return isBCAD; }
            set { isBCAD = value; }
        }

        public string ChartNumber
        {
            get { return chartNumber; }
            set { chartNumber = value; }
        }

        public DateTime DSNYHireDate
        {
            get { return dsnyHireDate; }
            set { dsnyHireDate = value; }
        }
        public DateTime NYCHireDate
        {
            get { return nycHireDate; }
            set { nycHireDate = value; }
        }
        public string PhoneNumber
        {
            get { return phoneNumber; }
            set { phoneNumber = value; }
        }

        public Int16 LeaveStatusCode
        {
            get { return leaveStatusCode; }
            set { leaveStatusCode = value; }
        }
        public Int16 ProbationCode
        {
            get { return probationCode; }
            set { probationCode = value; }
        }
        public DateTime ProbationStartDate
        {
            get { return probationStartDate; }
            set { probationStartDate = value; }
        }
        public DateTime ProbationEndDate
        {
            get { return probationEndDate; }
            set { probationEndDate = value; }
        }
        public string VacationSchedule
        {
            get { return vacationSchedule; }
            set { vacationSchedule = value; }
        }
        public string BadgeNumber
        {
            get { return badgeNumber; }
            set { badgeNumber = value; }
        }
        public Int64 TitleID
        {
            get { return titleID; }
            set { titleID = value; }
        }
        public AddressDTO Address
        {
            get { return address; }
            set { address = value; }
        }
        public Boolean IsActive
        {
            get { return isActive; }
            set { isActive = value; }
        }

        public string PersonnelTitle
        {
            get { return personnelTitle; }
            set { personnelTitle = value; }
        }
        #endregion
    }
}
