﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace DSNY.DSNYCP.DTO
{
    public class UserDTO
    {

       

        public UserDTO()
        {
            personalID = -1;
            userID = String.Empty;
            password = String.Empty;
            emailID = String.Empty;
            userMembership = null;
            lastLoginDateTime = DateTime.MinValue;
            lastLogin = DateTime.MinValue;
            isNew = false;
            roles = string.Empty;
            roleID = string.Empty;
            membershipID = string.Empty;
            lastname = string.Empty;
            firstname = string.Empty;

        }

        #region Private Variables

        private Int64 personalID;
        private String userID;
        private String password;
        private String emailID;
        private Boolean isTempPassword;
        private Boolean isAdmin;
        private List<MembershipDTO> userMembership;
        private DateTime lastLoginDateTime;
        private DateTime lastLogin;
        private Boolean isNew;
        private String roles;
        private String roleID;
        private String membershipID;
        private String lastname;
        private String firstname;
        #endregion

        #region Public Property


        public Int64 PersonalID
        {
            get { return personalID; }
            set { personalID = value; }
        }

        public String UserID
        {
            get { return userID; }
            set { userID = value; }
        }

        public String Password
        {
            get { return password; }
            set { password = value; }
        }

        public String EmailID
        {
            get { return emailID; }
            set { emailID = value; }
        }

        public Boolean IsTempPassword
        {
            get { return isTempPassword; }
            set { isTempPassword = value; }
        }

        public Boolean IsAdmin
        {
            get { return isAdmin; }
            set { isAdmin = value; }
        }

        public List<MembershipDTO> UserMembership
        {
            get { return userMembership; }
            set { userMembership = value; }
        }

        public DateTime LastLoginDateTime
        {
            get { return lastLoginDateTime; }
            set { lastLoginDateTime = value; }
        }
        public DateTime LastLogin
        {
            get { return lastLogin; }
            set { lastLogin = value; }
        }
        public Boolean IsNew
        {
            get { return isNew; }
            set { isNew = value; }
        }

        public String Roles
        {
            get { return roles; }
            set { roles = value; }
        }
        public String RoleID
        {
            get { return roleID; }
            set { roleID = value; }
        }


        public String MembershipID
        {
            get { return membershipID; }
            set { membershipID = value; }
        }
        public String FirstName
        {
            get { return firstname; }
            set { firstname = value; }
        }
        public String LastName
        {
            get { return lastname; }
            set { lastname = value; }
        }

        #endregion


        //#region Public Method

        //public Boolean AuthenticateUser()
        //{
        //    if (proxy == null)
        //        proxy = new Proxy();
        //    return proxy.AuthenticateUser(this);
        //}


        //#endregion
    }
}
