﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using DSNY.DSNYCP.Proxys;

namespace DSNY.DSNYCP.DTO
{
    public class IncedentDTO
    {
       // Proxy proxy;
        public IncedentDTO()
        {
            uniqueID = 0;
            complaintID = -1;
            incedentDate = DateTime.MinValue;
            serialNo = 0;
            chargeID = -1;
            count = 0;
        }

        #region Private Variables
            private Int16 uniqueID;    
            private Int64 complaintID;
            private DateTime incedentDate;
            private int serialNo;
            private Int64 chargeID;
            private Int16 count;
        #endregion

        #region Public Property

            public Int16 UniqueID
            {
                get { return uniqueID; }
                set { uniqueID = value; }
            }
    
        public Int64 ComplaintID
            {
                get { return complaintID; }
                set { complaintID = value; }
            }

            public Int16 Count
            {
                get { return count; }
                set { count = value; }
            }

            public DateTime IncedentDate
            {
                get { return incedentDate; }
                set { incedentDate = value; }
            }

            public int SerialNo
            {
                get { return serialNo; }
                set { serialNo = value; }
            }

            public Int64 ChargeID
            {
                get { return chargeID; }
                set { chargeID = value; }
            }
        #endregion

        
    }
}
