﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
   [Serializable]
    public class WorkflowStatusDTO
    {
        private int m_statusId;

        public int StatusId
        {
            get { return m_statusId; }
            set { m_statusId = value; }
        }
        private string m_Status;

        public string Status
        {
            get { return m_Status; }
            set { m_Status = value; }
        }
    }
}
