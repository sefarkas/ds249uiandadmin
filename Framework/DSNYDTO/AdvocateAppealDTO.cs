﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class AdvocateAppealDTO
    {
        private Int64 _uniqueID = -1; // 16 --> 64 by Farkas 28Feb2020
        private Int64 _advocateAppealID = -1;
        private Int64 _advocacyCaseID = -1;
        private DateTime _appealDt = DateTime.MinValue;
        private Int64 _appealresultCd = -1;
        private String _appealComments = String.Empty;
        private Int64 appealCd = -1;
        private Int64 _modifiedBy = -1; // 16 --> 64 by Farkas 28Feb2020
        private DateTime _modifiedDt = DateTime.MinValue;        

        public Int64 UniqueID
        { // 16 --> 64 by Farkas 28Feb2020
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }

        public Int64 AdvocateAppealID
        {
            get { return _advocateAppealID; }
            set { _advocateAppealID = value; }
        }
        public Int64 AdvocacyCaseID
        {
            get { return _advocacyCaseID; }
            set { _advocacyCaseID = value; }
        }
        public DateTime? AppealDt { get; set; }
       
        public Int64 AppealresultCd
        {
            get { return _appealresultCd; }
            set { _appealresultCd = value; }
        }
        public String AppealComments
        {
            get { return _appealComments; }
            set { _appealComments = value; }
        }

        public Int64 AppealCd
        {
            get { return appealCd; }
            set { appealCd = value; }
        }

        public Int64 ModifiedBy
        {// 16 --> 64 by Farkas 28Feb2020
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        public DateTime ModifiedDt
        {
            get { return _modifiedDt; }
            set { _modifiedDt = value; }
        }
       
    }
}
