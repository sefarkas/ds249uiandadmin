﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class MembershipDTO
    {
        public MembershipDTO()
        {
            personalID = -1;
            membershipID = -1;
            membership = String.Empty;
            roles = null;
        }

        #region Private Variables

        private Int64 personalID;
        private Int64 membershipID;
        private String membership;
        private Int64 Id; // 16 -> 64 by Farkas 27Feb2020
        private Int64 roleID; // Int16 roleID; by Farkas 27Fe2020
        private String role;
        private List<RoleDTO> roles;


        #endregion

        #region Public Property

        public Int64 PersonalID
        {
            get { return personalID; }
            set { personalID = value; }
        }

        public Int64 MembershipID
        {
            get { return membershipID; }
            set { membershipID = value; }
        }

        public String MembershipDesc
        {
            get { return membership; }
            set { membership = value; }
        }

        public List<RoleDTO> Roles
        {
            get { return roles;}
            set { roles = value; }
        }
        public Int64 ID  // 16 -> 64 by Farkas 27Feb2020
        {
            get { return Id; }
            set { Id = value; }
        }
        public Int64 RoleID // Int16 RoleID by Farkas 27Fe2020 
        {
            get { return roleID; }
            set { roleID = value; }
            /*
             * The ID in table [dbo].[tblMembershipRoles] is larger than 
             * the C# System.Int16 maximum value, i.e., 32.767.  Current 
             * max ID has a value of 60,964.
             * T-SQL and C# have different value ranges for “integers”.  
             * */
        }

        public String RoleDesc
        {
            get { return role; }
            set { role = value; }
        }
        #endregion
    }
}
