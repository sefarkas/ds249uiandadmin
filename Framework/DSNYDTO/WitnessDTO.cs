﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using DSNY.DSNYCP.Proxys;

namespace DSNY.DSNYCP.DTO
{
    public class WitnessDto
    {

        //Proxy proxy;

        public WitnessDto()
        {
            uniqueID = 0;
            witnessID = -1;
            complaintID = -1;
            personnelID = -1;
            personnelName = String.Empty;
            refNo = String.Empty;
        }

        #region Private Variables

        private Int16 uniqueID;
        private Int64 witnessID;
        private Int64 complaintID;
        private Int64 personnelID;
        private String personnelName;
        private String refNo;

        #endregion

        #region Public Property

        public Int16 UniqueID
        {
            get { return uniqueID; }
            set { uniqueID = value; }
        }
        public Int64 WitnessID
        {
            get { return witnessID; }
            set { witnessID = value; }
        }

        public Int64 ComplaintID
        {
            get { return complaintID; }
            set { complaintID = value; }
        }

        public Int64 PersonnelID
        {
            get { return personnelID;}
            set { personnelID = value; }
        }

        public String PersonnelName
        {
            get { return personnelName; }
            set { personnelName = value; }
        }

        public String RefNo
        {
            get { return refNo; }
            set { refNo = value; }
        }

        #endregion

       
    }
}
