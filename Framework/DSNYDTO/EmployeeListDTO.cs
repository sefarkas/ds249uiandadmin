﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
   public class EmployeeListDTO
    {
        public EmployeeListDTO()
        {
            firstName = string.Empty;
            lastName = string.Empty;
            imageID = -1;            
            referenceNo = -1;
        }

        # region variables
        private string firstName;
        private string lastName;
        private Int32 imageID;
      
        private Int32 referenceNo;
        # endregion
        # region properties
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public Int32 ImageID
        {
            get { return imageID; }
            set { imageID = value; }
        }
       
        public Int32 ReferenceNo
        {
            get { return referenceNo; }
            set { referenceNo = value; }
        }

        # endregion
    }
}
