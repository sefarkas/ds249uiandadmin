﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class AdvocateHearingDTO
    {
        private Int64 _uniqueID = -1;
        private Int64 _advocateHearingID = -1;
        private Int64 _advocacyCaseID = -1;
        private DateTime _hearingDt = DateTime.MinValue;
        private Int64 _hearingStatusCd = -1;
        private Int64 _calStatusCd = -1;
        private Int64 _hearingTypeCd = -1;
        private String _hearingComments = String.Empty;
        private Int64 _modifiedBy = -1; // 16 --> 64 by Farkas 28Fe2020
        private DateTime _modifiedDt = DateTime.MinValue;


        public Int64 UniqueID
        { // 16 --> 64 by Farkas 28Feb2020
            get { return _uniqueID; }
            set { _uniqueID = value; }
        }

        public Int64 AdvocateHearingID
        {
            get { return _advocateHearingID; }
            set { _advocateHearingID = value; }
        }
        public Int64 AdvocacyCaseID
        {
            get { return _advocacyCaseID; }
            set { _advocacyCaseID = value; }
        }
        public DateTime? HearingDt {get;set;}
     
        public Int64 HearingStatusCd
        {
            get { return _hearingStatusCd; }
            set { _hearingStatusCd = value; }
        }
        public Int64 CalStatusCd
        {
            get { return _calStatusCd; }
            set { _calStatusCd = value; }
        }
        public Int64 HearingTypeCd
        {
            get { return _hearingTypeCd; }
            set { _hearingTypeCd = value; }
        }
        public String HearingComments
        {
            get { return _hearingComments; }
            set { _hearingComments = value; }
        }
        public Int64 ModifiedBy
        { // 16 --> 64 by Farkas 28Feb2020
            get { return _modifiedBy; }
            set { _modifiedBy = value; }
        }
        public DateTime ModifiedDt
        {
            get { return _modifiedDt; }
            set { _modifiedDt = value; }
        }


    }
}
