﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class ApplicationRoleDTO
    {

        public ApplicationRoleDTO()
        {
            Id = -1;
            applicationID = -1;
            role = String.Empty;
            roleID = -1;
            personalID = -1;
        }

        #region Private Variables

        private Int16 Id;
        private Int64 applicationID;
        private Int16 roleID;
        private String role;
        private Int64 personalID;

        #endregion

        #region Public Property

        public Int16 ID
        {
            get { return Id; }
            set { Id = value; }
        }

        public Int64 ApplicationID
        {
            get { return applicationID; }
            set { applicationID = value; }
        }

        public Int64 PersonalID
        {
            get { return personalID; }
            set { personalID = value; }
        }

        public Int16 RoleID
        {
            get { return roleID; }
            set { roleID = value; }
        }

        public String RoleDesc
        {
            get { return role; }
            set { role = value; }
        }

        #endregion
    }
}
