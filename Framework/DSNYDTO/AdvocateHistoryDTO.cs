﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    public class AdvocateHistoryDto
    {
       
            private Int64 _indexNo = -1;
            private Int64 _complaintID = -1;
            private DateTime _voilationDate = DateTime.MinValue;
            private String _charges = String.Empty;
            private String _chargesDesc = String.Empty;
            private String _penalty = String.Empty;
            private Int64 _parentIndexNo = -1;
            private String _jt1 = String.Empty;       
        /// <summary>
        /// Public property
        /// </summary>
        public Int64 ndexNo
        {
            get { return _indexNo; }
            set { _indexNo = value; }
        }

        public Int64 ParentIndexNo
        {
            get { return _parentIndexNo; }
            set { _parentIndexNo = value; }
        }

        public Int64 ComplaintID
        {
            get { return _complaintID; }
            set { _complaintID = value; }
        }
        public DateTime VoilationDate
        {
            get { return _voilationDate; }
            set { _voilationDate = value; }
        }
        public String Charges
        {
            get { return _charges; }
            set { _charges = value; }
        }
        public String ChargesDesc
        {
            get { return _chargesDesc; }
            set { _chargesDesc = value; }
        }
        public String Penalty
        {
            get { return _penalty; }
            set { _penalty = value; }
        }
        public String JT1
        {
            get { return _jt1; }
            set { _jt1 = value; }
        }
    }
}
