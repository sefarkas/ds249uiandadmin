﻿using System;

namespace DSNY.DSNYCP.DTO
{
    [Serializable]
    public class AppErrorDTO
    {
        public long ErrorID { get; set; }
        public string Url { get; set; }
        public string RequestType { get; set; }
        public string UserID { get; set; }
        public string ErrorMessage { get; set; }
        public string StackTrace { get; set; }
    }
}
