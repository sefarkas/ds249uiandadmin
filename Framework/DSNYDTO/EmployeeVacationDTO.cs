﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    [Serializable]
    public class EmployeeVacationDTO
    {
        public EmployeeVacationDTO()
        {
            m_SeperationType = new SeperationDTO();
            m_StatusType = new WorkflowStatusDTO();

        }

        private long m_EmpvacationId;

        public long EmpvacationId
        {
            get { return m_EmpvacationId; }
            set { m_EmpvacationId = value; }
        }

        private long m_EmployeeId;

        public long EmployeeId
        {
            get { return m_EmployeeId; }
            set { m_EmployeeId = value; }
        }

        public  SeperationDTO m_SeperationType;

        public string WorkflowStatus
        {
            get { return m_StatusType.Status; }
           
        }
        public string EmployeeSeperation
        {
            get { return m_SeperationType.SeperationType; }

        }
       
        private int m_LastRegularDayWorked;

        public int LastRegularDayWorked
        {
            get { return m_LastRegularDayWorked; }
            set { m_LastRegularDayWorked = value; }
        }
        private DateTime m_CreatedDate;

        public DateTime CreatedDate
        {
            get { return m_CreatedDate; }
            set { m_CreatedDate = value; }
        }
        private string m_CreatedBy;

        public string CreatedBy
        {
            get { return m_CreatedBy; }
            set { m_CreatedBy = value; }
        }
        private DateTime m_ModifiedDate;

        public DateTime ModifiedDate
        {
            get { return m_ModifiedDate; }
            set { m_ModifiedDate = value; }
        }
        private string m_ModifiedBy;

        public string ModifiedBy
        {
            get { return m_ModifiedBy; }
            set { m_ModifiedBy = value; }
        }
        private DateTime m_SignOffDate;

        public DateTime SignOffDate
        {
            get { return m_SignOffDate; }
            set { m_SignOffDate = value; }
        }
        private string m_SignOffUser;

        public string SignOffUser
        {
            get { return m_SignOffUser; }
            set { m_SignOffUser = value; }
        }
        private Boolean m_IsDeleted;

        public Boolean IsDeleted
        {
            get { return m_IsDeleted; }
            set { m_IsDeleted = value; }
        }
        private string  m_WorkflowId;

        public string WorkflowId
        {
            get { return m_WorkflowId; }
            set { m_WorkflowId = value; }
        }
        private int m_StatusId;

        public WorkflowStatusDTO  m_StatusType;
        public int StatusId
        {
            get { return m_StatusId; }
            set { m_StatusId = value; }
        }

        private List<EmployeeVacationDetailsDTO> m_EmployeeVacationDetails;

        public List<EmployeeVacationDetailsDTO> EmployeeVacationDetails
        {
            get { return m_EmployeeVacationDetails; }
            set { m_EmployeeVacationDetails = value; }
        }

    }
}
