﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DTO
{
    [Serializable]
    public class SeperationDTO
    {
        private string m_SeperationType;

        public string SeperationType
        {
            get { return m_SeperationType; }
            set { m_SeperationType = value; }
        }
        private int m_SeperationTypeId;

        public int SeperationTypeId
        {
            get { return m_SeperationTypeId; }
            set { m_SeperationTypeId = value; }
        }
    }
}
