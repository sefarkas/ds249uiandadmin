﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DSNY.DSNYCP.DAL
{
    /// <summary>
    /// This method provides the names for the stored procedures that are accessed by this application from the database.
    /// </summary>
    public sealed class SPName
    {
        #region Private Constructor

        /// <summary>
        /// Required so compiler does not automatically add a default constructor.
        /// </summary>
        private SPName() { }

        #endregion Private Constructor

        /// <summary>
        /// FETCH
        /// </summary>
        /// 

        #region
        public const String GetPersonellDetailWS = "usp_GetPersonDetailsWS";
        #endregion
        public const String SaveError = "usp_SaveError";
        public const String GetComplaintById = "usp_GetComplaint";
        public const String GetComplaints = "usp_GetComplaints";
        public const String GetComplaintsForAdmin = "usp_GetComplaintsForAdmin";
        public const String GetComplaintsBySearch = "usp_GetComplaintsBySearch";
        public const String GetAdvocateComplaintsBySearch = "usp_GetAdvocateComplaintsBySearch";        
        public const String GetComplaintsCount = "usp_GetComplaintsCount";
        public const String GetSearchComplaintsCount = "usp_GetComplaintsCountSearch";
        public const String GetSearchComplaints = "usp_GetSearchComplaints";
        public const String GetPersonnel = "usp_GetPersonnel";
        public const String GetEmployees = "usp_GetEmployees";
        public const String GetInspectionInfoByCCU = "usp_LotCleaning_GetInspectionInfoByCCU";
        public const String GetComplaintAttachments = "usp_InsertComplaintAttachments";
        #region EmpApp
        public const String GetEmployeeRapInfo = "usp_GetEmployeeRapInfo";
        public const String GetEmployeeProfile = "usp_GetEmployeeProfile";
        public const String GetEmployeeUniformList = "usp_LotCleaning_GetEmployeeUniformList";
        public const String GetEmployeeCivilianList = "usp_LotCleaning_GetEmployeeCivilianlist";
        public const String GetTitleitemList = "usp_GetTitleitemList";
        public const String GetEmployeeSeniorityList = "usp_GetEmployeeSeniorityList";        
        public const String GetSeniorityListForShiftForLocation = "usp_GetSeniorityListForShiftForLocation";
        public const String DeleteEmployeeRapInfo = "usp_DeleteEmployeeRapInfo";
        public const String DeleteEmployeeComplaintInfo = "usp_DeleteEmployeeComplaintInfo";
        public const String GetEmployeeComplaintInfo = "usp_GetEmployeeComplaintInfo";
        public const String GetSnowBattalionList = "usp_LotCleaning_GetSnowBattalion";
        public const String UpdateSnowBattalionInfoInfo = "usp_UpdateSnowbatalionInfo";
        public const String InsertUpdateEmployeeRapInfo = "usp_InsertOrUpdateEmployeeRapInfo";
        public const String InsertUpdateEmployeeComplaintInfo = "usp_InsertOrUpdateEmployeeComplaintInfo";
        public const String GetEmployeeTitleInfoByTitleid = "usp_GetEmployeeTitleInfoByTitleId";
#endregion
        public const String GetEmployeeImage = "LoadEmployeeImages";
        public const String GetEmployeeInfo = "GetEmplyeesInfo";
        public const String LoadEmployeeList = "GetEmployeePicsMapping";
        public const String GetPersonnelList = "usp_GetPersonnelListForComplaints";
        public const String SearchPersonnelList = "usp_SearchPersonnelList";
        public const String SearchChargeType = "usp_SearchCharges";
        public const String GetChargeType = "usp_GetCharges";
        public const String GetCodeList = "usp_GetCodeList";
        public const String GetPersonnelByRefNumber = "usp_GetPersonneByRefNumber";
        public const String GetLocation = "usp_GetDistrictList";
        public const String GetTitles = "GetTitleList";
        public const String SearchTitles = "usp_SearchTitleList";
        public const String GetBorough = "usp_GetBoroughList";
        public const String GetWitnesses = "usp_GetWitnessList";
        public const String GetViolations = "usp_GetVoilationList";
        public const String GetComplainants = "usp_GetComplainantList";
        public const String GetIncidents = "usp_IncidentList";
        public const String GetComplaintHistory = "usp_GetComplaintHistory";
        public const String AuthenticateUser = "usp_AuthenticateUser";
        public const String AuthenticatedUserForApplication = "usp_AuthenticatedUserForApplication";
        public const String ValidateUserEmail = "usp_ValidateUserEmail";
        public const String UpdateComplaintStatus = "usp_UpdateComplaintStatus";
        public const String UpdateCombinedComplaints = "usp_SaveCombinedComplaints";
        public const String LoadComplaintAttachments = "usp_LoadComplaintAttachments";
        public const String InsertComplaintAttachments = "usp_InsertComplaintAttachments";
       
        public const String SaveCombinedPenalties = "usp_SaveCombinePenalty";
        public const String DeleteCombinedPenalties = "usp_deleteCombinedPenalty";
        public const String DeleteSupportedDocument = "usp_deleteSupportedDocument";

        public const String LotCleaning_LoadAttachments = "usp_LotCleaningLoadAttachments";
        public const String LotCleaning_InsertAttachments = "usp_LotCleaningInsertAttachments";
        public const String LotCleaning_DeleteDocuments = "usp_LotCleaningDeleteSupportedDocument";
        public const String LotCleaning_GetAttachmentLocation = "usp_LotCleaningGetAttachmentLocations";

        /// <summary>
        /// BCAD
        /// </summary>
        public const String GetSearchBCADComplaintsCount = "usp_GetBCADComplaintsCountSearch";
        public const String GetBCADComplaintsBySearch = "usp_GetBCADComplaintsBySearch";
        public const String GetBCADComplaint = "usp_GetBCADComplaint";
        public const String GetBCADCaseStatus = "usp_GetBCADCaseStatus";
        public const String SetBCADStatus = "usp_ReopenBCADComplaint";
        public const String GetBCADPenaltyList = "usp_GetBCADPenaltyList";
        public const String SaveBCADPenaltyList = "usp_SaveBCADPenalty";
        public const String GetBCADComplaints = "usp_GetBCADComplaints";
        public const String GetBCADComplaintsCount = "usp_GetBCADComplaintsCount";
        public const String SaveBCADComplaint = "usp_UpdateBCADComplaint";
        public const String ReturnCaseToAdvocate = "usp_ReturnCaseToAdvocate";
        public const String ReturnCaseToAuthor = "usp_ReturnCaseToAuthor";

        
        /// <summary>
        /// ADVOCATE
        /// </summary>
        public const String GetAdvocateComplaints = "usp_GetAdvocateComplaints";
        public const String GetCombinedComplaints = "usp_GetCombinedComplaints";
        public const String CheckCombinedComplaints = "usp_CheckCombinedComplaints";

        public const String CheckIsParent = "usp_CheckIsParent";
        public const String GetAdvocateComplaintsCount = "usp_GetAdvocateComplaintsCount";

        public const String GetCombineListCount = "usp_GetCombineComplaintsCount";
        public const String GetSearchAdvocateComplaintsCount = "usp_GetAdvocateComplaintsCountSearch"; 
        public const String OpenAdvocateComplaint = "usp_OpenAdvocateComplaint";
        public const String GetAdvocateComplaint = "usp_GetAdvocateComplaint";
        public const String SetAdvocateCaseStatus = "usp_SetAdvocateCaseStatus";
        public const String ReturnAdvocateCaseToAuthor = "usp_ReturnAdvocateCaseToAuthor";
        public const String ReturnCaseToBCAD = "usp_ReturnCaseToBCAD";
        public const String GetAdvocateAssociatedComplaintList = "usp_GetAdvocateAssociatedComplaintList";
        public const String GetAdvocateHistory = "usp_GetAdvocateHistory";
        public const String GetAdvocateHearingList = "usp_GetAdvocateHearingList";
        public const String GetAdvocateAppealList = "usp_GetAdvocateAppealList";
        public const String GetAdvocatePenaltyList = "usp_GetAdvocatePenaltyList";
        public const String GetCombinedPenaltyList = "usp_GetCombinedPenaltyList";
        public const String SearchComplaints = "usp_SearchComplaintID";

        /// <summary>
        /// Medical
        /// </summary>

        public const String GetMedicalComplaints = "usp_GetMedicalComplaints";        
        public const String GetMedicalComplaintsCount = "usp_GetMedicalComplaintsCount";
        public const String GetSearchMedicalComplaintsCount = "usp_GetMedicalComplaintsCountSearch";
        public const String GetMedicalComplaintsBySearch = "usp_GetMedicalComplaintsBySearch";
        public const String CreateMedicalComplaint = "usp_CreateMedicalComplaint";
        public const String SaveMedicalPenaltyList = "usp_SaveMedicalPenalty";
        public const String GetMedicalPenaltyList = "usp_GetMedicalPenaltyList";
        public const String GetMedicalComplaint = "usp_GetMedicalComplaint";
        public const String SetMedicalStatus = "usp_ReopenMedicalComplaint";
        public const String ReturnMedicalCaseToAuthor = "usp_ReturnMedicalCaseToAuthor";
        public const String ReturnMedicalCaseToAdvocate = "usp_ReturnMedicalCaseToAdvocate";
        public const String SaveMedicalComplaint = "usp_UpdateMedicalComplaint";
        public const String GetMedicalCaseStatus = "usp_GetMedicalCaseStatus";
                
        /// <summary>
        /// SAVE
        /// </summary>
        public const String SaveComplaint = "usp_SaveComplaint";
        public const String SaveWitness = "usp_SaveWitness";
        public const String SaveComplainant = "usp_SaveComplainant";
        public const String SaveViolation = "usp_Savevoilation";
        public const String SaveIncident = "usp_SaveIncident";
        public const String CreateBCADComplaint = "usp_CreateBCADComplaint";
        public const String SaveAdvocateAppeal = "usp_SaveAdvocateAppeal";
        public const String SaveAdvocateHearings = "usp_SaveAdvocateHearing";
        public const String SaveAdvocatePenalty = "usp_SaveAdvocatePenalty";
        public const String SaveAssociatedComplaint = "usp_SaveAssociatedComplaints";
        public const String SaveAdvocateComplaint = "usp_SaveAdvocateComplaint";
        public const String UpdateCharges = "usp_UpdateCharges";
        public const String ReopenOrCloseApprovedComplaint = "usp_ReopenOrCloseApprovedComplaint";
        public const String DeleteUndeleteComplaint = "usp_DeleteUnDeleteDS249Complaint";
        public const String ComplaintHistoryDetails = "";
        public const String AdminPersonLog= "usp_AdminPersonLog";


        /// <summary>
        /// REPORTS
        /// </summary>
        public const String GetComplaintByDistrict = "usp_GetComplaintsByDistrict";
    

        /// <summary>
        /// ADMIN
        /// </summary>
        public const String GetAllPersonals = "usp_GetAllEmployees";
        public const String GetPayCodes = "GetPayCodeList";
        public const String SaveUser = "usp_SaveUser";
        public const String ResetPassword = "usp_ResetPassword";
        public const String DeleteUser = "usp_DeleteUser";
        public const String GetNewUserId = "usp_GetNewUserID";


        /// <summary>
        /// SECURITY
        /// </summary>
        public const String GetMembershipRoles = "usp_GetMembershipRoles";
        public const String GetRolesMembershipAccess = "usp_GetRolesMembershipAccess";
        public const String GetApplicationRoleMemberships = "usp_GetApplicationRoleMemberships";
        public const String SaveMembershipRoles = "usp_SaveMembershipRoles";
        public const String HistoryMembershipRole = "usp_HistoryMembershipRole";
        public const String SaveApplicationMembershipRoles = "usp_SaveApplicationMembershipRoles";

        public const String GetUserMembership = "usp_getUserMembership";
        public const String SaveUserMembership = "usp_SaveUserMembership";
        public const String SaveUserRoleMembership = "usp_SaveUserRoleMembership";

        public const String GetMembershipList = "usp_getMembershipList";
        public const String GetUser = "usp_GetUser";
        public const String GetUserEmployeeApp = "usp_GetUserEmployeeApp";
        public const String GetRoles = "usp_GetRoles";

        public const String GetAccessRoles = "usp_GetAccessRoles";
        public const String GetApplicationRoles = "usp_getDefaultNewRoles";
        public const String GetApplicationRole = "usp_GetApplicationRoles";
        public const String GetApplication = "usp_getApplication";
        public const String getApplicationList = "usp_getApplicationList";
        public const String getApplicationMemberList = "usp_getApplicationMembershipList";
        public const String getApplicationMemberships = "usp_getApplicationMemberships";

        public const String GetHideCharges = "usp_GetChargesTblHideCharges";
        public const String SaveHideCharges = "usp_SaveChargesTblHideCharges";
        public const String DeleteHideCharges = "usp_DeleteChargesTblHideCharges";


        public const String SaveUserApplication = "usp_SaveUserApplication";
        public const String SaveRolesMembershipAccess = "usp_SaveRolesMembershipAccess";



        /// <summary>
        /// WORKFLOW
        /// </summary>
        public const string GetAllUserByGroupIdApplicationId = "usp_Workflow_GetAllUserDetailsByGroupIdApplicationId";
        public const string GetNextGroup = "usp_Workflow_GetNextGroup";
        public const string GetConfigKey = "usp_Workflow_GetConfigKey";
        public const string SaveWorkflowApplication = "usp_Workflow_SaveApplication";
        public const string SaveWorkflowGroup = "usp_Workflow_SaveGroup";
        public const string DeleteWorkflowGroup = "usp_Workflow_DeleteGroup";
        public const string DeleteWorkflowApplication = "usp_Workflow_DeleteApplication";
        public const string GetAllApplications = "usp_Workflow_GetALLApplications";
        public const string GetAllGroups = "usp_Workflow_GetALLGroups";
        public const string GetAllApplicationGroup = "usp_Workflow_GetGroupDetails";
        public const string GetAllUserDetails = "usp_Workflow_GetUserDetails";
        public const string SaveWorkflowUsers = "usp_Workflow_SaveUsers";
        
        /// <summary>
        /// DIF
        /// </summary>
        public const string SaveDif = "usp_PMD_SaveDIF";

        #region PMD

        /// <summary>
        /// PMD
        /// </summary>
        public const string GetAllSeparationList = "usp_PMD_GetAllSeperationList";
        public const string GetAllEmployeeVacationDetail = "usp_PMD_GetAllEmployeeVacationList";
        public const string GetAllVacationList = "usp_PMD_GetAllVacationList";
        public const string GetAllDifferentList = "usp_PMD_GetAllDIFList";
        public const string GetAllDifferentById = "usp_PMD_GetAllDIFById";
        public const string SaveEmployeeVacation = "usp_PMD_SaveEmployeeVacation";
        public const string SaveEmployeeVacationDetails = "usp_PMD_SaveEmployeeVacationDetails";
        public const string SaveEmployeeVacationWorkflow = "usp_PMD_SaveEmployeeVacationWorkflow";
        public const string ApproveEmployeeVacation = "usp_PMD_ApproveEmployeeVacation";
        public const string GetEmployeeVacationInfo = "usp_PMD_GetEmpVacationInfoById";
        public const string GetEmployeeVacationInfoDetails = "usp_PMD_GetEmpVacationDetailById";
        public const string GetEmployeeVacationApprovalDetails = "usp_PMD_GetEmpVacationApprovalDetailById";
        public const string UpdateEmployeeVacationInfo = "usp_PMD_UpdateEmployeeVacation";
        public const string SaveWorkflowDetails = "usp_PMD_SaveWorkflowDetails";

        #region Complaints

        public const string PmdCreateComplaint = "usp_PMD_CreateComplaint";
        public const string PmdDeleteComplaintNamedEmployee = "usp_PMD_DeleteComplaintNamedEmployee";
        public const string PmdGetComplaintById = "usp_PMD_GetComplaintById";
        public const string PmdGetComplaintList = "usp_PMD_GetComplaintList";
        public const string PmdGetComplaintNamedEmployees = "usp_PMD_GetComplaintNamedEmployees";
        public const string PmdGetComplaintTotalCount = "usp_PMD_GetComplaintTotalCount";
        public const string PmdSaveComplaintNamedEmployee = "usp_PMD_SaveComplaintNamedEmployee";
        public const string PmdTouchComplaint = "usp_PMD_TouchComplaint";

        #endregion Complaints

        #region Interview Requests

        public const string PmdCreateInterviewRequest = "usp_PMD_CreateInterviewRequest";    
        public const string PmdGetInterviewRequestById = "usp_PMD_GetInterviewRequestById";
        public const string PmdGetInterviewRequestList = "usp_PMD_GetInterviewRequestList";
        public const string PmdGetInterviewRequestTotalCount = "usp_PMD_GetInterviewRequestTotalCount";
        public const string PmdUpdateInterviewRequest = "usp_PMD_UpdateInterviewRequest";

        #endregion Interview Requests

        #region General

        public const string PmdGetDisctricts = "usp_PMD_GetDistricts";
        public const string PmdGetEmployeeByRefNum = "usp_PMD_GetEmployeeByRefNum";

        #endregion General

        #region Medical Evaluations

        public const string PmdCreateMedicalEvaluation = "usp_PMD_CreateMedicalEvaluation";
        public const string PmdGetMedicalEvaluationById = "usp_PMD_GetMedicalEvaluationById";
        public const string PmdGetMedicalEvaluationList = "usp_PMD_GetMedicalEvaluationList";
        public const string PmdGetMedicalEvaluationTotalCount = "usp_PMD_GetMedicalEvaluationTotalCount";
        public const string PmdUpdateMedicalEvaluation = "usp_PMD_UpdateMedicalEvaluation";

        #endregion Medical Evaluations

        #region Separations

        public const string PmdGetSeparationList = "usp_PMD_GetSeparationList";
        public const string PmdGetSeparationTotalCount = "usp_PMD_GetSeparationTotalCount";
        public const string PmdGetSeparationTypes = "usp_PMD_GetSeparationTypes";

        #endregion Separations

        #endregion PMD

            /// <summary>   
        /// LOTCLEANING
        /// </summary>
        public const string GetCleaningLookup = "usp_LotCleaning_GetLookupValues";
        public const string GetInspectorValue = "usp_LotCleaning_GetInspectorsValues";
        public const string SaveLotCleaningList = "usp_LotCleaning_SaveRequest";
        public const string GetLotCleaningList = "usp_LotCleaning_GetRequest"; //
        public const string VoidLotCleaningRequest = "usp_LotCleaning_VoidRequest";
        public const string UpdateLotCleaningRequest = "usp_LotCleaning_UpdateRequest";

        public const string GetLotCleaningLocation = "usp_LotCleaning_GetLocation";
        public const string SaveLotCleaningLocation = "usp_LotCleaning_SaveLocation";
        public const string UpdateLotCleaningLocation = "usp_LotCleaning_UpdateLocation";

        public const string GetLotCleaningLoad = "usp_LotCleaning_GetLoad";
        public const string SaveLotCleaningLoad = "usp_LotCleaning_SaveLoad";
        public const string UpdateLotCleaningLoad = "usp_LotCleaning_UpdateLoad";
        //Activity
        public const string GetLotCleaningActivity = "usp_LotCleaning_GetActivity";
        public const string SaveLotCleaningActivity = "usp_LotCleaning_SaveActivity";
        //Activity Detail
        public const string GetLotCleaningActDetail = "usp_LotCleaning_GetActDetail";
        public const string SaveLotCleaningActDetail = "usp_LotCleaning_SaveActDetail";

        //usp_LotCleaning_SaveLot
        public const string GetLotCleaningLot = "usp_LotCleaning_Getlot";
        public const string SaveLotCleaningLot = "usp_LotCleaning_SaveLot";
        public const string UpdateLotCleaningLot = "usp_LotCleaning_UpdateLot";

        public const string GetInspectorslist = "usp_LotCleaning_GetInspectorslist";

        //Added by shamala
        //to delete membership roles
        public const string DeleteMembershipRoles = "usp_DeleteMembershipRoles";
    }
}
