﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.DTO.PMD;

namespace DSNY.DSNYCP.IInterfaces
{
    public interface IDSNYPmdService
    {
        #region General

        IList<DistrictDTO> GetDistricts();

        EmployeeDTO GetEmployeeByRefNum(string rererenceNumber);

        #endregion General

        #region Complaints

          DSNY.DSNYCP.DTO.PMD.ComplaintDTO GetComplaintById(long id);

          IList<ComplaintListItemDTO> GetComplaintList(int startRow, int maxRows, string sortExpression);

          long GetComplaintTotalCount();

          bool SaveComplaint(DSNY.DSNYCP.DTO.PMD.ComplaintDTO dto);

          #endregion Complaints

        #region Diffs

          DIFDTO GetDIFById(int id);

          List<DIFDTO> GetDIFInfo();

          int SaveDif(DIFDTO difDto);

          #endregion Diffs

        #region Interview Requests

          InterviewRequestDTO GetInterviewRequestById(long id);

          IList<InterviewRequestListItemDTO> GetInterviewRequestList(int startRow, int maxRows, string sortExpression);

          long GetInterviewRequestTotalCount();

          bool SaveInterviewRequest(InterviewRequestDTO dto);

          #endregion Interview Requests

        #region Medical Evaluations

          MedicalEvaluationDTO GetMedicalEvaluationById(long id);

          IList<MedicalEvaluationListItemDTO> GetMedicalEvaluationList(int startRow, int maxRows, string sortExpression);
     
          long GetMedicalEvaluationTotalCount();

          bool SaveMedicalEvaluation(MedicalEvaluationDTO dto);

          #endregion Medical Evaluations

        #region Separations 

          IList<SeparationListItemDTO> GetSeparationList(int startRow, int maxRows, string sortExpression);

          long GetSeparationTotalCount();

          IList<SeparationTypeDTO> GetSeparationTypes();

          #endregion Separations


      #region PMD Separations - old

      List<SeperationDTO> GetAllSeperationList();

      #endregion PMD Separations - old

      #region PMD Vacations

      int ApproveVacationInfo(List<EmployeeVacationApprovalDetailsDTO> employeeDetail, int employeeVacationInfoId, DateTime dateApproved, string approvedBy);
      
      List<EmployeeVacationApprovalDetailsDTO> GetEmployeeVacationApprovalDetails(int employeeVacationInformationId);
      
      List<EmployeeVacationDTO> GetEmployeeVacationDetails();
      
      EmployeeVacationDTO GetEmployeeVacationInfo(int employeeVacationInformationId);
      
      List<EmployeeVacationDetailsDTO> GetEmployeeVacationInfoDetails(int employeeVacationInformationId);
      
      int SaveEmployeeVacationDetails(EmployeeVacationDTO employeeVacation);
      
      bool SaveEmployeeVacaitonWorkflow(int employeeVacationId, string workflowId, int applicationId);
      
      bool UpdateEmployeeVacationInfo(int employeeVacationId, int statusId);

      #endregion PMD Vacation
    }
}
