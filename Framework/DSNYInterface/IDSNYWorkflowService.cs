﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DSNY.DSNYCP.DTO;
namespace DSNY.DSNYCP.IInterfaces
{
 public interface IDSNYWorkflowService
    {
        List<WorkFlowUser> GetALLUserByGroupIDApllicationID(int groupId, int applicationId);
        int GetTotalApprovalLevels(int groupId, int applicationId);
        string GetConfigValueBykey(string key);
        bool SaveWorkflow(WorkflowDTO workflow);
        int SaveUser(string userName, int groupId,string lastName,string ntId,string email);
         
    }
}
