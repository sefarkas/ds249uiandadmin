﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.IInterfaces
{

    public interface IService
    {
        List<ComplaintDTO> LoadComplaint(Int64 id);
        List<ComplaintDTO> SearchComplaintId(String searchText);
        List<ComplaintHistoryDTO> LoadComplaintHistory(Int64 id, Int64 complaintId);
        List<ComplaintDTO> LoadCombinedComplaints(int pageno,Int64 id, int Maxrows, Int64 complaintId,Int64 indexNo,ref Int64 rowsCount);
        List<ComplaintDTO> LoadComplaintList(Int16 personalId, int pageNo, int maximumRows, ref Int64 rowsCount);
        List<PersonnelByImageDTO> LoadEmployeeImage(Int32 id, Byte[] employeeImage);
        List<PersonnelByIdDTO> LoadPersonnel(Int64 id);
        List<PersonnelByRefNoDTO> LoadPersonnel(String refNo);

        List<PersonnelListDTO> LoadPersonnelList(String searchValue);
        List<EmployeeInfoDTO> LoadEmployeeInfo(Int64 id);
        List<PersonnelDTO> LoadPersonnelList();

        List<EmployeeListDTO> LoadEmployeeList();
        List<ChargeDTO> LoadCharges(String chargeCode);
        List<ChargesWithoutParametersDTO> LoadCharges();
        List<CodeDTO> LoadCodeList(Int16 categoryId);
        List<BCADStatusDTO> LoadBCADCaseStatus();
        List<LocationDTO> LoadLocations();
        List<BoroughDTO> LoadBoroughs();
        List<TitleDTO> LoadTitles();
        List<TitleListDTO> LoadTitles(String searchText);
        List<WitnessDto> LoadWitnesses(Int64 complaintId);
        List<ComplainantDTO> LoadComplainants(Int64 complaintId);
        List<ViolationDTO> LoadViolations(Int64 complaintId);
        List<IncedentDTO> LoadIncidents(Int64 complaintId, Int64 chargeId);
        List<ComplaintDTO> LoadBCADComplaints(int pageNo, int maximumRows);
       // List<ComplaintDTO> LoadCombinedComplaints(int pageNo, int maximumRows);
        List<BCADPenaltyDTO> LoadBCADPenalty(Int64 bcadCaseId);
        List<AdvocateAssociatedComplaintDTO> LoadAssociatedComplaints(Int64 advocacyCaseId);
        List<AdvocateHearingDTO> LoadAdvocateHearingList(Int64 advocacyCaseId);
        List<AdvocateAppealDTO> LoadAdvocateAppealList(Int64 advocacyCaseId);
        List<AdvocatePenaltyDTO> LoadAdvocatePenaltyList(Int64 advocacyCaseId);
        List<CombinedPenaltyDTO> LoadCombinedPenaltyList(Int64 advocacyCaseID);
        bool SaveComplaint(ComplaintDTO complaint, Int64 personID);
        bool ReopenORCloseApproveComplaint(ComplaintDTO complaint);
        bool DeleteUndeleteComplaint(Int64 complaintId, Int16 status);
        bool SaveWitness(WitnessDto witness);
        bool SaveComplainant(ComplainantDTO complainant);
        bool SaveVoilation(ViolationDTO violation);
        bool SaveIncident(IncedentDTO incident);
        bool UpdateComplaintStatus(Int64 complaintId, Int16 status);
        bool CreateNewBCAD(BCADComplaintDTO bcadComplaint);
        bool OpenAdvocateComplaint(Int64 advocacyCaseId, Int16 receivedUser, DateTime receivedDate);
        List<BCADComplaintDTO> GetBCADComplaint(Int64 complaintId, BCADComplaintDTO bcadComplaint);        
        List<ComplaintAdvocacyDTO> GetAdvocateComplaint(Int64 complaintId);
        bool SetBCADStatus(long complaintId, short statusCode);
        bool SetAdvocateCaseStatus(Int64 complaintId, Int16 statusCode, Int64 advocateCaseId);
        bool ReturnBCADToAuthor(Int64 complaintId, Int64 PersonalID);
        bool ReturnAdvocateCaseToAuthor(Int64 complaintId, Int64 advocacyCaseId, Int64 PersonalID);
        bool ReturnBCADToAdvocate(Int64 complaintId, Int64 PersonalID);
        bool ReturnAdvocateToBCAD(Int64 complaintId, Int64 advocacyCaseId, Int64 PersonalID);
        bool SaveBCADComplaint(BCADComplaintDTO bcadComplaint);
        bool SaveBCADPenaltyList(BCADPenaltyDTO bcadPenalty);
        bool SaveAdvocateComplaint(ComplaintAdvocacyDTO advocate);

        List<ComplaintByDistrictDTO> GetComplaintByDistrict();
        Boolean AuthenticateUser(UserDTO user);
        UserDTO ValidateUserEmail(String emailId);

        List<PersonnelDTO> LoadAllPersonnel(String searchValue);
        List<PayCodeDTO> LoadPayCodeList();
        String SaveUser(UserDTO user);
        Boolean ResetPassword(UserDTO user);
        Boolean DeleteUser(Int64 personId);
        String GetNewUserID(Int64 personId);

        //Added Medical Functions
         List<MedicalStatusDTO> LoadMedicalCaseStatus();
         List<ComplaintDTO> LoadMedicalComplaints(int pageNo, int maximumRows);
         List<MedicalPenaltyDTO> LoadMedicalPenalty(Int64 medicalCaseID);
         bool CreateNewMedical(MedicalComplaintDTO medicalComplaint);
         List<MedicalComplaintDTO> GetMedicalComplaint(Int64 ComplaintID, MedicalComplaintDTO medicalComplaint);

         bool SetMedicalStatus(long complaintID, short statusCode);
         bool ReturnMedicalToAuthor(Int64 ComplaintID, Int64 PersonalID);
         bool ReturnMedicalToAdvocate(Int64 ComplaintID, Int64 PersonalID);
         bool SaveMedicalComplaint(MedicalComplaintDTO medicalComplaint);
         bool SaveMedicalPenaltyList(MedicalPenaltyDTO medicalPenalty);

         List<HideChargesDto> GetHideCharges(Int16 employeeId);
         bool SaveHideCharges(String referenceNo, Int64 employeeId);
         bool DeleteHideCharges(Int16 viewChargesId);
    }

}
