<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.DS249.Advocate_CaseManagement" CodeBehind="CaseManagement.aspx.cs" %>

<%@ Register Src="../UserControl/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="CRB" Namespace="DSNY.Web.UI.WebControls" Assembly="GroupRadioButton" %>
<%@ Register Src="~/UserControl/PenaltyControl.ascx" TagName="AdvPenalty" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/AppealsControl.ascx" TagName="AdvAppeal" TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/HearingsControl.ascx" TagName="AdvHearing" TagPrefix="uc3" %>
<%@ Register Src="~/UserControl/AdvocateAssociatedcomplaints.ascx" TagName="AdvAssComplaint"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        function EnableOverRideCharges(chkID, txtid) {

            var chkBoxID = document.getElementById(chkID);
            var txtBoxId = document.getElementById(txtid);
            if (chkBoxID.checked == true) {
                txtBoxId.disabled = false;
            }
            else {
                txtBoxId.disabled = true;
            }
        }
        function OpenPrint(IndexNo) {
            if (IndexNo != "") {
                var URL = "Print.aspx?Id=" + IndexNo;
                var dialogResults = window.open(URL, '');
            }
            else {
                alert("ComplaintID is Blank Or Missing");
            }
            return false;
        }
    </script>
    <style type="text/css">
        .style1
        {
            font-size: 0.6em;
            width: 60px;
            text-align: left;
            border-left: 2px solid #1e951f;
        }
        .style2
        {
            height: 24px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <table id="caseManagementTable" class="tableCase" style="height: 30px">
            <tr>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList"
                                    ShowMessageBox="true" ShowSummary="false" EnableClientScript="true" Enabled="true" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lbMessage" runat="server" CssClass="feedbackStatus"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                        runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                        <Animations>                                
                                                <OnUpdated>
                                                    <FadeOut Duration="5.0" Fps="24" />
                                                </OnUpdated>
                        </Animations>
                    </cc1:UpdatePanelAnimationExtender>
                </td>
            </tr>
            <tr>
                <td class="styleHeading">
                    <div>
                        <span>ADVOCATE CASE MANAGEMENT</span>
                    </div>
                </td>
            </tr>
        </table>
        <table width="900px">
            <tr>
                <td align="left">
                    <asp:ImageButton ID="btnCaseList" 
                        ImageUrl="~/includes/img/ds249_btn_complaint.png" runat="server" CausesValidation="false"
                        OnClick="btnCaseList_Click" />
                </td>
                <td align="center">
                    <asp:ImageButton ID="ibPrint" runat="server" ImageUrl="~/includes/img/ds249_btn_print_static.png"
                        CausesValidation="false" />
                </td>
                <td align="right">
                    <asp:ImageButton ID="btnSave" ImageUrl="~/includes/img/ds249_btn_save_static.png"
                        runat="server" OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
        <table class="tableCase" id="searchTable">
            <tr>
                <td style="width: 60%">
                    &nbsp;
                </td>
                <td class="tdCaseSearch">
                    CASE SEARCH
                </td>
                <td>
                    <asp:TextBox ID="txtCaseSearch" runat="server" AutoPostBack="true" CausesValidation="false"
                        OnTextChanged="txtCaseSearch_TextChanged"></asp:TextBox>
                    <cc1:AutoCompleteExtender ID="txtCaseSearch_AutoCompleteExtender" runat="server"
                        ServiceMethod="GetCompletionListCaseSearch" DelimiterCharacters="" CompletionInterval="1000"
                        MinimumPrefixLength="1" CompletionSetCount="12" EnableCaching="true" CompletionListCssClass="ChargeCompletionList"
                        Enabled="True" ServicePath="" UseContextKey="True" TargetControlID="txtCaseSearch">
                    </cc1:AutoCompleteExtender>
                    <asp:Image ID="imgCaseSrch" ImageUrl="~/includes/img/ds249_i_mini_btn.png"
                        runat="server" CssClass="dashImg" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="uPanel" runat="server">
            <ContentTemplate>
                <table class="tableCase" id="tblCase1" border="0" cellspacing="0px" cellpadding="0px"
                    width="850px">
                    <tbody>
                        <tr>
                            <td class="tdCase">
                                &nbsp
                            </td>
                            <td class="tdCaseId">
                                <table id="tblCaseId">
                                    <tr>
                                        <td class="tdLabeling">
                                            CASE ID
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCaseID" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRecDate">
                                <table id="tblRecDate">
                                    <tr>
                                        <td class="tdLabeling">
                                            RECEIVE DATE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRecDate" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRecBy">
                                <table id="tblRecBy">
                                    <tr>
                                        <td class="tdLabeling">
                                            RECEIVE BY
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRecBy" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="tableCase" id="tblCase2" border="0" cellspacing="0px" cellpadding="0px">
                    <tbody>
                        <tr>
                            <td class="tdCompIdx">
                                <table id="tblCaseStatus">
                                    <tr>
                                        <td class="tdLabeling">
                                            CASE STATUS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="margin-left: 40px">
                                            <asp:DropDownList ID="ddlCaseStatus" runat="server" Font-Size="0.9em" 
                                                Font-Names="Arial">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdCompIdx">
                                <table id="tblCompIdx">
                                    <tr>
                                        <td class="tdLabeling">
                                            COMPLAINT INDEX
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCompIdx" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdModDate">
                                <table id="tblModifiedDate">
                                    <tr>
                                        <td class="tdLabeling">
                                            MODIFIED DATE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtModDate" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdModBy">
                                <table id="tblModBy">
                                    <tr>
                                        <td class="tdLabeling">
                                            MODIFIED BY
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtModBy" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblCase3" class="tableCase" style="border-top: 2px solid #1e951f; border-left: 2px solid #1e951f;
                    border-right: 2px solid #1e951f" border="0" cellspacing="0px" cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdCaseStatusTxt">
                                CASE STATUS
                            </td>
                        </tr>
                        <tr>
                            <td class="tdCaseStatusTxt">
                                <asp:TextBox ID="txtCaseStatus" runat="server" Height="74px" Width="900px" MaxLength="300"
                                    Wrap="true" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblComplaintDetails1" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdCompDetails1" nowrap="nowrap">
                            </td>
                            <td class="tdName1">
                                <table id="tblName">
                                    <tr>
                                        <td class="tdLabeling">
                                            NAME
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtName" runat="server" Width="315px" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdTitle1">
                                <table id="tblTitle">
                                    <tr>
                                        <td class="tdLabeling">
                                            TITLE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td nowrap="nowrap">
                                            <asp:TextBox ID="txtTitle" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>                                        

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblComplaintDetails2" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdDistrict">
                                <table id="tblDistrict">
                                    <tr>
                                        <td class="tdLabeling">
                                            DISTRICT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtDistrict" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                        
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRefNo">
                                <table id="tblRefNo">
                                    <tr>
                                        <td class="tdLabeling">
                                            REFERENCE NO.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRefNo" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdSSNo">
                                <table id="tblSocialSecurity">
                                    <tr>
                                        <td class="tdLabeling">
                                            SOCIAL SECURITY NUMBER
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtSSNo" runat="server" Font-Size="0.9em" Font-Names="Arial" MaxLength="9"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdAppoint">
                                <table id="tblAppoint">
                                    <tr>
                                        <td class="tdLabeling">
                                            APPOINTMENT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtAppointment" runat="server" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>                                       
                                    </tr>
                                </table>
                            </td>

                             <td class="tdAppoint">
                                <table id="Table1">                                    
                                    <tr>                                       
                                        <td nowrap="nowrap">&nbsp;&nbsp;
                                        <asp:CheckBox ID="chkProbation" runat="server" Text="Employee Under Probation"
                                                        SkinID="" Enabled="false" Font-Bold="true" />
                                        </td>
                                    </tr>
                                </table>
                            </td>

                        </tr>
                    </tbody>
                </table>
                <table id="tblComplaintDetails3" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdComplainantName">
                                <table id="tblComplainantName">
                                    <tr>
                                        <td class="tdLabeling">
                                            COMPLAINANT NAME
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptCompName" runat="server">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCompName" runat="server" Width="350px" Text='<%#Bind("ComplainantName")%>'
                                                        CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdComplainantTitle">
                                <table id="tblComplainantTitle">
                                    <tr>
                                        <td class="tdLabeling">
                                            TITLE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptCompTitle" runat="server">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCompTitle" runat="server" Width="200px" Text='<%#Bind("Title")%>'
                                                        CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdComplainantDistrict">
                                <table id="tblComplainantDistrict">
                                    <tr>
                                        <td class="tdLabeling">
                                            DISTRICT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptCompDistrict" runat="server">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCompDist" runat="server" Text='<%#Bind("LocationName")%>' CssClass="txtBox"
                                                        ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblCharges1" class="tableCase" border="0" cellspacing="0px" cellpadding="0px">
                    <tbody>
                        <tr>
                            <td class="tdInstruction">
                                TO ADD CHANGE A CHARGE,CLICK IN THE OVERRIDE CHECK BOX AND SPECIFY THE CORRECT CHARGE
                                UNDER OVERRIDE CHARGE
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="tblCharges2" border="0" cellspacing="0px" cellpadding="0px" width="930px">
                                    <tr>
                                        <td class="style1">
                                            PRIMARY
                                        </td>
                                        <td class="tdCharges" align="left">
                                            INCIDENT DATE
                                        </td>
                                        <td class="tdCharges">
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHARGES
                                        </td>
                                        <td class="tdChanges">
                                            <asp:Label ID="lblChanges" Text="CHANGES?" runat="server"></asp:Label>
                                        </td>
                                        <td class="tdOverRide">
                                            OVERRIDE WITH CHARGES
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-left: 2px solid #1e951f; border-right: 2px solid #1e951f">
                                <asp:Repeater ID="rptCharges" runat="server" OnItemDataBound="rptCharges_ItemDataBound">
                                    <ItemTemplate>
                                        <table id="tblCharges3" border="0" cellspacing="0px" cellpadding="0px" width="920px">
                                            <tr>
                                                <td>
                                                    <asp:HiddenField ID="hdnChargeID" runat="server" Value='<%#Eval("ChargeID") %>' />
                                                    <asp:HiddenField ID="hdnPrimary" runat="server" Value='<%#Eval("PrimaryIndicator") %>' />
                                                    <CRB:GroupRadioButton ID="rbPrimary" runat="server" GroupName="Primary" />
                                                </td>
                                                <td class="tdTxtIncidentDate" align="center">
                                                    &nbsp;&nbsp;<asp:TextBox ID="txtIncidentDate" runat="server" CssClass="txtBox" Width="100px"
                                                        Text='<%#DataBinder.Eval(Container.DataItem,"IncidentDate","{0:d}")%>' ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td class="tdTxtCharges">
                                                    <asp:TextBox ID="txtCharges" runat="server" CssClass="txtBox" Width="300px" Text='<%#Bind("ViolationDescription")%>'
                                                        ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td class="tdChkBox">
                                                    <asp:CheckBox ID="chkBox" runat="server" />
                                                </td>
                                                <td class="tdTxtOverRide">
                                                    <asp:TextBox ID="txtOverRideCharges" runat="server" Text='<%#Eval("OverRideViolationDescription")%>'
                                                        Width="300px" Enabled="false"></asp:TextBox>
                                                    <cc1:AutoCompleteExtender ID="txtOverRideCharges_AutoCompleteExtender" runat="server"
                                                        ServiceMethod="GetCharges" DelimiterCharacters="" CompletionInterval="1000" MinimumPrefixLength="1"
                                                        CompletionSetCount="12" EnableCaching="true" Enabled="True" ServicePath="" UseContextKey="True"
                                                        TargetControlID="txtOverRideCharges" CompletionListCssClass="CompletionList"
                                                        CompletionListHighlightedItemCssClass="HighlightedItem" CompletionListItemCssClass="ListItem">
                                                    </cc1:AutoCompleteExtender>
                                                    <asp:Image ID="imgCharges" runat="server" ImageUrl="~/includes/img/ds249_i_mini_btn.png" />
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtComplaintID" runat="server" CssClass="txtBox" Width="0px" Text='<%#Bind("ComplaintID")%>'
                                                        ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:ImageButton ID="ImgView" runat="server" ImageUrl="~/includes/img/ds249_btn_view_static.png" />
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Width="400px" Visible="false">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvUploadFile" runat="server" AutoGenerateColumns="False" BorderColor="#235705"
                                                EmptyDataText="No File Uploaded" HeaderStyle-BackColor="#a4d49a" HeaderStyle-Font-Bold="true"
                                                Width="380px" DataKeyNames="IndexNo" OnRowDataBound="gvUploadFile_RowDataBound">
                                                <EmptyDataRowStyle CssClass="fromdate" />
                                                <Columns>
                                                    <asp:BoundField DataField="ComplaintID" DataFormatString="" HeaderText="File" Visible="true">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="false"
                                                            Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="IndexNo" DataFormatString="" HeaderText="File">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="false"
                                                            Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="IncidentID" DataFormatString="" HeaderText="File" Visible="true">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="false"
                                                            Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ChargeCode" DataFormatString="" HeaderText="ChargeID"
                                                        Visible="true">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="false"
                                                            Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FileName" DataFormatString="" HeaderText="File">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Font-Bold="false"
                                                            Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="View">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="ViewButton" ImageUrl="~/includes/img/ds249_btn_view_static.png"
                                                                runat="server" CommandName="View" CausesValidation="false" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BackColor="#A4D49A" Font-Bold="True" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnOK" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <div align="center">
                                        <asp:Button ID="btnOK" runat="server" Text="OK" CausesValidation="false" Visible="false" />
                                    </div>
                                </asp:Panel>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblStatus1" class="tableCase" border="0" cellspacing="0px" cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdStatus">
                                &nbsp
                            </td>
                            <td style="border-left: 2px solid #1e951f; border-top: 2px solid #1e951f">
                                <table id="tblAction">
                                    <tr>
                                        <td class="tdAction">
                                            ACTION
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlAction" runat="server" AppendDataBoundItems="true" AutoPostBack="true"
                                                onselectedindexchanged="ddlAction_SelectedIndexChanged">
                                                <asp:ListItem Value="-1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="border-left: 2px solid #1e951f; border-top: 2px solid #1e951f; border-right: 2px solid #1e951f">
                                <table id="tblActionDate">
                                    <tr>
                                        <td class="tdActionDate">
                                            ACTION DATE&nbsp;<label style="font-size: 1.0em">(Month/Day/Year:xx/xx/xxxx)</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtActionDt" runat="server" Width="160px"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtActionDt_CalendarExtender" runat="server" TargetControlID="txtActionDt"
                                                PopupButtonID="imgActCal" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image ID="imgActCal" AlternateText="Calender" ImageUrl="~/includes/img/ds249_mini_cal_btn.png"
                                                runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblStatus2" class="tableCase" border="0" cellspacing="0px" cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdReviewStatus">
                                <table id="tblReviewStatus">
                                    <tr>
                                        <td>
                                            REVIEW STATUS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlReviewStatus" runat="server" AppendDataBoundItems="true">
                                                <asp:ListItem Value="-1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>                           
                            <td class="tdTrialStatus">
                                <table id="tblTrialStatus">
                                    <tr>
                                        <td>
                                            TRIAL RETURN STATUS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlTrailReturnStatus" runat="server" 
                                                AppendDataBoundItems="true" AutoPostBack="true" 
                                                onselectedindexchanged="ddlTrailReturnStatus_SelectedIndexChanged">
                                                <asp:ListItem Value="-1">--Select--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblAssCompHearings1" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="0px">
                    <tbody>
                        <tr>
                            <td class="tdAssComp">
                                &nbsp;
                            </td>
                            <td class="tdHearings">
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblAssCompHearings2" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="0px">
                    <tbody>
                        <tr>
                            <td class="tdTxtAssComp" valign="top">
                                <table id="tblAssComp">
                                    <tr>
                                        <td valign="top" class="style2">
                                            <asp:ImageButton ID="btnAddNewAssComp" ImageUrl="~/includes/img/addnew.gif"
                                                runat="server" CausesValidation="true" OnClick="btnAddNewAssComp_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <asp:PlaceHolder ID="pnAssComplaints" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdPnlHearingsStatus" valign="top">
                                <table id="tblPnlHearings">
                                    <tr>
                                        <td valign="top">
                                            <asp:ImageButton ID="btnAddNewHearing" ImageUrl="~/includes/img/addnew.gif"
                                                runat="server" CausesValidation="false" OnClick="btnAddNewHearing_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="pnHearing" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblPenaltyAppeals1" class="tableCase" border="0" cellspacing="0px" cellpadding="0px">
                    <tbody>
                        <tr>
                            <td class="tdPenaltyAdvocate">
                                &nbsp
                            </td>
                            <td class="tdAppeals">
                                &nbsp
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblPenaltyAppeals2" class="tableCase" border="0" cellspacing="0px" cellpadding="0px">
                    <tbody>
                        <tr>
                            <td class="tdAboutPenalty">
                                <table id="tblAboutPnlPenalty">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:ImageButton ID="ibAddPenalty" ImageUrl="~/includes/img/addnew.gif" runat="server"
                                                    CausesValidation="false" OnClick="btnAddNewPlty_Click" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:RadioButton ID="rdCombPnlty" runat="server" Text="COMBINED COMPLAINTS" OnCheckedChanged="rdCombPnlty_CheckedChanged"
                                                    AutoPostBack="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:PlaceHolder ID="pnPenalty" runat="server"></asp:PlaceHolder>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="tdTotal">
                                                &nbsp
                                                <asp:Label ID="lblTotalDays" runat="server" Text="TOTAL DAYS" Visible="True"></asp:Label>
                                                <asp:TextBox ID="txtTotal" runat="server" CssClass="txtBox" Font-Bold="true" ForeColor="Black"
                                                    ReadOnly="True" Visible="True"></asp:TextBox>
                                            </td>
                                            <td>
                                                &nbsp&nbsp
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="tdAboutAppeals">
                                <table id="tblAboutPnlAppeals">
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnAddNewAppeal" ImageUrl="~/includes/img/addnew.gif"
                                                runat="server" CausesValidation="false" OnClick="btnAddNewAppeal_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:PlaceHolder ID="PnAppeal" runat="server"></asp:PlaceHolder>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <%--Change--%>
                        <%--Change--%>
                    </tbody>
                </table>

                <table id="tblFinal1" class="tableCase" border="0" cellspacing="0px" cellpadding="0px">
                    <tbody>
                        <tr class="bordergreen">
                            <td colspan="4" class="bordergreen">
                                <table width="100%">
                                    <tr>
                                        <td style="width: 50%;" class="style7">
                                            <h4>
                                                HISTORY SECTION</h4>
                                            <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" BorderColor="#235705"
                                             OnRowDataBound="gvHistory_RowDataBound" EmptyDataText="No History Found" HeaderStyle-BackColor="#a4d49a" HeaderStyle-Font-Bold="true"
                                                Width="915px">
                                                <EmptyDataRowStyle CssClass="fromdate" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="INDEX NO">
                                                        <ItemTemplate>
                                                            <a href="#" id="lkIndex" style="font-size: 1.0em;">
                                                                <%# Eval("IndexNo")%></a>
                                                        </ItemTemplate>
                                                        <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em"
                                                            HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px" ForeColor="Black"
                                                            Font-Underline="False" Wrap="False" Width="80px" />
                                                        <ItemStyle BorderColor="#235705" BackColor="#A4D49A" BorderWidth="1px" Font-Size="1.0em"
                                                            HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="ComplaintID" DataFormatString="" HeaderText="ComplaintID"
                                                        Visible="false">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="150px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                            Font-Bold="false" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="CreatedDate" DataFormatString="{0:d}" HeaderText="INCIDENT DATE">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="150px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                            Font-Bold="false" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Charges" HeaderText="CHARGES">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="80px" Wrap="False" />
                                                        <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="80px"
                                                            Font-Bold="false" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ChargesDesc" HeaderText="CHARGES DESC" Visible="false">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="180px" Wrap="False" />
                                                        <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="180px"
                                                            Font-Bold="false" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Penalty" DataFormatString="" HeaderText="PENALTY">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="350px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="left" VerticalAlign="Middle" Width="450px"
                                                            Font-Bold="false" Wrap="true" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ParentIndexNo" HeaderText="PARENT INDEX NO">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="120" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="left" VerticalAlign="Middle" Width="450px"
                                                            Font-Bold="false" Wrap="true" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="JT1" HeaderText="JT1">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="120" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="left" VerticalAlign="Middle" Width="450px"
                                                            Font-Bold="false" Wrap="true" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle BackColor="#A4D49A" Font-Bold="True" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>                           
                                
        



                            </td>
                        </tr>
                        <tr>
                            <td class="tdFinal" style="height: 25px">
                                &nbsp
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblFinal2" class="tableCase" border="0" cellspacing="0px" cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdAboutFinal">
                                <table id="tblCOBFinal">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table id="tblCOBStatus">
                                                    <tbody>
                                                        <tr>
                                                            <td class="tdLabeling">
                                                                COB STATUS
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="ddlCOBStatus" runat="server" Width="220px" AppendDataBoundItems="true">
                                                                    <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td>
                                                <table id="tblCOBDate">
                                                    <tbody>
                                                        <tr>
                                                            <td class="tdLabeling">
                                                                COB DATE&nbsp;<label style="font-size: 1.0em">(Month/Day/Year:xx/xx/xxxx)</label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtCOBDt" runat="server"></asp:TextBox>
                                                                <cc1:CalendarExtender ID="txtCOBDt_CalendarExtender" runat="server" TargetControlID="txtCOBDt"
                                                                    CssClass="MyCalendar" PopupButtonID="imgCOBCal" PopupPosition="TopRight">
                                                                </cc1:CalendarExtender>
                                                                <asp:Image ID="imgCOBCal" ImageUrl="~/includes/img/ds249_mini_cal_btn.png"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td style="border: 2px solid #1e951f">
                                <table id="tblFinalClosed">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table id="tblCaseClosedDate">
                                                    <tr>
                                                        <td class="tdLabeling">
                                                            CASE CLOSED DATE&nbsp;<label style="font-size: 1.0em">(Month/Day/Year:xx/xx/xxxx)</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox ID="txtCloseDate" runat="server"></asp:TextBox>
                                                            <cc1:CalendarExtender ID="txtCloseDate_CalendarExtender" runat="server" TargetControlID="txtCloseDate"
                                                                PopupButtonID="imgFinalCal" PopupPosition="TopRight" CssClass="MyCalendar">
                                                            </cc1:CalendarExtender>
                                                            <asp:Image ID="imgFinalCal" ImageUrl="~/includes/img/ds249_mini_cal_btn.png"
                                                                runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                      
                    </tbody>
                </table>
                <table id="tblCancelSave" width="930px" style="margin-left: 30px; margin-right: 10px">
                    <tbody>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td style="width: 73%">
                                &nbsp;
                            </td>
                            <td>
                                <input id="IsDirty" name="hiddenIsDirty" value="false" type="hidden" />
                                <asp:HiddenField ID="hdnValue" runat="server" />
                            </td>
                            <td>
                                <asp:HiddenField ID="hdnText" runat="server" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
    <asp:Panel ID="pnlCombPnlty" runat="server" Style="display: none; padding-left: 5px;"
        Width="500px" Height="400px">
        <asp:Button ID="btnShowPopup" runat="server" Style="visibility: hidden" />
        <cc1:ModalPopupExtender ID="modalCombinedPntly" runat="server" BackgroundCssClass="modalBackground"
            PopupControlID="pnlCombPnlty" TargetControlID="btnShowPopup" />
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>               
                <div id="div" style="width: 450px; overflow-y: auto; overflow-x: auto; word-break: break-all;"
                    runat="server">
                </div>
                <table style="width: 400px"  class="tableCase" align="center" bgcolor="#FFFFFF" >
                    <tr>
                   <td>
                            <asp:label ID="lblSaveMsg"  runat="server" Text=""  Font-Size="Small" CssClass="feedbackStatus"  Font-Bold="True"  />
                        </td>     
                
                 </tr>  
                    <tr>
                        <td>
                            <uc1:Pager ID="Pager1" runat="server" />
                            <asp:GridView ID="gvAdvocate" runat="server" Width="500" AutoGenerateColumns="False"
                                BorderColor="#235705" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a"
                                AllowSorting="True" PageSize="20" AllowPaging="True" OnRowCommand="gvAdvocate_RowCommand"
                                OnRowDataBound="gvAdvocate_RowDataBound" OnSorting="gvAdvocate_Sorting" OnPageIndexChanging="gvAdvocate_PageIndexChanging"
                                OnDataBound="gvAdvocate_DataBound" AlternatingRowStyle-BackColor="#ededed" EmptyDataText="Sorry. No Complaints Found.">
                                <EmptyDataRowStyle CssClass="fromdate" />
                                <Columns>                               
                                  
                                    <asp:BoundField HeaderText="Incident Date" DataField="IncidentDate" SortExpression="IncidentDate"
                                        DataFormatString="{0:MMM dd, yyyy}" HtmlEncode="false">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="150px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Case ID" DataField="CaseID" SortExpression="CaseID">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="100px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Index #" DataField="IndexNo" SortExpression="IndexNo">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="70px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="140px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>
                                  
                                    <asp:BoundField HeaderText="Name (F. Last)" DataField="FirstName" SortExpression="FirstName">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="150px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>
                                   
                                    <asp:BoundField HeaderText="Charges" DataField="Charge" SortExpression="Charge">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="90px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>

                                   <asp:BoundField HeaderText="PIndexNo" DataField="ParentIndexNO" SortExpression="ParentIndexNO">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="70px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>                               
                                       <asp:TemplateField HeaderText="Combine/UnCombine" >
                                        <ItemTemplate>
                                            <asp:CheckBox runat="server" ID="chkCombine"
                                            
                                            Checked='<%# subCombineCheck((DataBinder.Eval(Container.DataItem, "ParentIndexNO") + "")) %>'
                                             AutoPostBack="true" />                    

                                         </ItemTemplate>


                                       <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="90px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                        
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveCmb" runat="server" Text="Save" OnClick="btnSaveCmb_Click" />
                            <asp:Button ID="btnCancel" CausesValidation="false" runat="server" Text="Cancel/Close"
                                OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                    
                </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
