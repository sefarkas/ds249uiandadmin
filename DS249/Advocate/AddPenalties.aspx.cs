﻿using System.Collections.Generic;
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;
using DSNY.DSNYCP.ClassHierarchy;
using System.Data;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.DS249.Advocate
{ 

    public partial class AddPenalties : System.Web.UI.Page
    {
        Complaint complaint;        
        CombinedPenaltyList CombPenList;
       

        int i_AddPenaltyRow = 0;
        DataTable dt = new DataTable();
        private long m_ParentID;

        public long ParentID
        {
            get { return m_ParentID; }
            set { m_ParentID = value; }
        }
        private string m_RefNo;

        public string RefNo
        {
            get { return m_RefNo; }
            set { m_RefNo = value; }
        }
        private bool is_New;

        public bool Is_New
        {
            get { return is_New; }
            set { is_New = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {            
            string onclick = "javascript:self.close();return false;";
            CancelButton.Attributes.Add("onclick", onclick);
            if (Request.QueryString["ParentID"] != null)
            {
                ParentID = Convert.ToInt64(Request.QueryString["ParentID"]);
            }

            if (Request.QueryString["RefNo"] != null)
            {
                RefNo = Convert.ToString(Request.QueryString["RefNo"]);
            }

            if (Request.QueryString["Mode"] != null)
            {
                if (Request.QueryString["Mode"] == "NEW")
                {
                    Is_New = true;
                }
                else
                {
                    Is_New = false;
                }
            }     


            if (!Page.IsPostBack)
            {
                if (Is_New == false)
                {
                  
                    BindGrid();
                }
                else
                {
                    LoadFromSession();
                }

            }
        }

        private void BindGrid()
        {
           
                ParentID = Convert.ToInt64(Request.QueryString["ParentID"]);
                complaint = new Complaint();
                complaint.Load(ParentID, RequestSource.DS249);
            if (Session["DT_Penalty"] != null)
                {
                    DataTable loadedDt = (DataTable)Session["DT_Penalty"];
                    if (loadedDt.Rows.Count > 0)
                    {
                        gvAddPenalties.DataSource = loadedDt;
                        gvAddPenalties.DataBind();
                    }
               }
                
                else if (complaint.ComplaintId != -1)
                {
                    gvAddPenalties.DataSource = complaint.HistoryList.List;
                    gvAddPenalties.DataBind();
                }
           
        }

        private void LoadFromSession()
        {
            List<Complaint> Complaint;
            if (Session["Complaint"] != null)
            {
                Complaint = (List<Complaint>)Session["Complaint"];
                if (Complaint.Count > 0)
                {

                    gvAddPenalties.DataSource = complaint.HistoryList.List;
                    gvAddPenalties.DataBind();
                }
            }
            else
            {
                lblError.Text = "No Data found.";
            }
        }

       protected void chkchkStstatus_CheckedChanged(object sender, EventArgs e)
        {          
            CheckBox chk;            
            dt.Columns.Add(new DataColumn("Add", typeof(string)));
            dt.Columns.Add(new DataColumn("IndexNo", typeof(string)));
            dt.Columns.Add(new DataColumn("CombPenID", typeof(string)));
                            for (int i = 0; i < gvAddPenalties.Rows.Count; i++)
                            {
                                DataRow dataRow = dt.NewRow(); 
				                chk = (CheckBox)gvAddPenalties.Rows[i].Cells[0].FindControl("chkchkStstatus");

                                if (chk.Checked == true)
                                {
                                    dataRow["Add"] = "Y";               
                                   
                                }
                                dataRow["IndexNo"] = gvAddPenalties.Rows[i].Cells[1].Text;
                                dataRow["CombPenID"] = gvAddPenalties.Rows[i].Cells[2].Text;
                                dt.Rows.Add(dataRow);
                            }
                            Session["DT_Penalty"] = dt;       
                           
            
       }

       protected void gvAddPenalties_RowDataBound(object sender, GridViewRowEventArgs e)
       {           
           CombPenList = new CombinedPenaltyList(); 
           if (e.Row.RowType == DataControlRowType.DataRow)
           {

               if (Session["DT_Penalty"] != null)
               {
                   DataTable loadedDt = (DataTable)Session["DT_Penalty"];

                   if (loadedDt.Rows[i_AddPenaltyRow].ItemArray[0].ToString() == "Y")
                       {                           
                           CheckBox chk = (CheckBox)e.Row.FindControl("chkchkStstatus");
                           chk.Checked = true;
                       }
                   e.Row.Cells[1].Text = loadedDt.Rows[i_AddPenaltyRow].ItemArray[1].ToString();
                   e.Row.Cells[2].Text = loadedDt.Rows[i_AddPenaltyRow].ItemArray[2].ToString();
                  
                   i_AddPenaltyRow++;
               
               }
               else if (CombPenList.Load(ParentID))
               {
                   if (CombPenList.List.Count > 0)
                   {
                       for (int i = 0; i < CombPenList.List.Count; i++)
                       {
                           if (e.Row.Cells[1].Text == Convert.ToString(CombPenList.List[i].ChildCombPenID))
                           {
                               CheckBox chk = (CheckBox)e.Row.FindControl("chkchkStstatus");
                               chk.Checked = true;
                              
                               e.Row.Cells[1].Text = Convert.ToString(CombPenList.List[i].ChildCombPenID);
                               if (e.Row.Cells[2].Text != null)
                               {
                                   e.Row.Cells[2].Text = Convert.ToString(CombPenList.List[i].CombPenID);
                                  
                               }
                               else
                                   e.Row.Cells[2].Text=null;
                               
                           }
                            
                       }
                   }
               }
           }        
       }
       }       
 }

        


