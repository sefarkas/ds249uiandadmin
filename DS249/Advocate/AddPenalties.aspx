﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddPenalties.aspx.cs" Inherits="DSNY.DSNYCP.DS249.Advocate.AddPenalties" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
  <link href="../includes/css/styles1.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/CaseManagementStyle.css" rel="stylesheet" type="text/css" />
<link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
<link href="../includes/css/DashboardStyle.css" rel="stylesheet" type="text/css" />
<%--<link href="../includes/css/ModalPopUp.css" rel="stylesheet" type="text/css" />--%>
    <title></title>
    <style type="text/css">
        
.modalPopup {
	background-color: #fff;
	border: 1px solid #1e951f;
	clear:both;
	color:#000000;
	font-family:Arial;
	font-weight:bold;
	font-size:12px;
	line-height:15pt;
	padding: 30px;
	text-align: center;
}

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <td>
                 &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                        <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
        <asp:GridView ID="gvAddPenalties" runat="server" AutoGenerateColumns="False" 
            BorderColor="#235705" DataKeyNames="IndexNo" EmptyDataText="Error occured while loading the data." 
            OnRowDataBound="gvAddPenalties_RowDataBound" HeaderStyle-BackColor="#a4d49a" HeaderStyle-Font-Bold="true" 
           Width="200px">
            <EmptyDataRowStyle CssClass="fromdate" />
            <Columns>
                <asp:TemplateField HeaderText="Add" SortExpression="Indicator">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chkchkStstatus" AutoPostBack="true" OnCheckedChanged="chkchkStstatus_CheckedChanged" />
                    </ItemTemplate>
                     <HeaderStyle BorderColor="#235705" BorderWidth="2px" 
                    Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em" Font-Underline="False" 
                    ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle" 
                    Wrap="False" />
                    <ItemStyle CssClass="dashLineStyle" />
                </asp:TemplateField>
                <asp:BoundField DataField="IndexNo" DataFormatString="" HeaderText="Index Number">
                <HeaderStyle BorderColor="#235705" BorderWidth="2px" 
                    Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em" Font-Underline="False" 
                    ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle" 
                    Wrap="False" />
                <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Bold="false" 
                    Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em" 
                    HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                </asp:BoundField>
                <asp:BoundField DataField="" DataFormatString="" HeaderText="ID">
                <HeaderStyle BorderColor="#235705" BorderWidth="2px" 
                    Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em" Font-Underline="False" 
                    ForeColor="Black" HorizontalAlign="Center" VerticalAlign="Middle" 
                    Wrap="False" />
                <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Bold="false" 
                    Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em" 
                    HorizontalAlign="Center" VerticalAlign="Middle" Wrap="False" />
                </asp:BoundField>
                 
                </Columns>
            <HeaderStyle BackColor="#A4D49A" Font-Bold="True" />
        </asp:GridView>
 </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                <asp:ImageButton ID="CancelButton" runat="server" 
                ImageUrl="~/includes/img/ds249_btn_cancel.png" CausesValidation="true" />
                <asp:HiddenField ID="hdnCombPenUniqueID" Visible="false" runat="server" />
               </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>