﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provides the functionality for events and methods to print Approval Info.
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class Advocates_PrintAdvocate : System.Web.UI.Page
    {

        #region Page Level Variables

        Complaint complaint;
        Personnel personnel;           
        Boolean pageTwo = false;
        int VoilationDisplay = 4;

        #endregion
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
                lbSuspensionByDt.Attributes.CssStyle.Add("TEXT-ALIGN", "right");
                lbSuspensionLiftedDt.Attributes.CssStyle.Add("TEXT-ALIGN", "right");
                lbStatement.Attributes.CssStyle.Add("TEXT-ALIGN", "justify");
                LoadComplaintData();           
        }

        /// <summary>
        /// This method loads the advocate complaints 
        /// </summary>
        private void LoadComplaintData()
        {
           
                Int64 ID = Convert.ToInt64(Request.QueryString["Id"]);
                complaint = new Complaint();
                complaint.Load(ID, RequestSource.DS249);
                BindComplaintData();
                personnel = new Personnel();
                personnel.Load(complaint.EmployeeId);
                BindPersonnelData();
                PrintWitness();
                PrintComplainant();
                PrintVoilations(false);
                if (pageTwo == true)
                    Panel1.Visible = true;
                else
                    Panel1.Visible = false;
                if (complaint.ComplaintStatus >= 4)
                {
                    lbHeader.Text = "sanitation<br>COMPLAINT&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DS 249&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(12-04)";
                    lbHeader2.Text = lbHeader.Text;
                    lbHeader.Font.Size = FontUnit.Large;
                    lbHeader2.Font.Size = FontUnit.Large;
                    pnSOR.Visible = true;
                }
                else
                {
                    lbHeader.Text = "Non Official Use Only";
                    lbHeader2.Text = lbHeader.Text;
                    divPage1.Attributes.Add
                        ("style", "background-image: url('..\\includes\\img\\Watermark.JPG'); background-repeat: no-repeat;background-position:center;");
                    divPage2.Attributes.Add("style", "background-image: url('..\\includes\\img\\Watermark.JPG'); background-repeat: no-repeat;background-position:center;");
                }
            
        }
        /// <summary>
        /// This method returns true if the user print violation form is succesfully created
        /// </summary>
        /// <param name="more"></param>
        private void PrintVoilations(bool more)
        {

            HtmlTable table1 = new HtmlTable();

                table1.CellPadding = 3;
                table1.CellSpacing = 3;

                HtmlTableRow row;
                HtmlTableCell cell;

                row = new HtmlTableRow();

                cell = new HtmlTableCell();
                cell.InnerHtml = "VIOLATIONS";
                cell.Width = "58%";
                row.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerHtml = "";
                cell.Width = "2%";
                row.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerHtml = "LOCATIONS";
                cell.Width = "10%";
                row.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerHtml = "";
                cell.Width = "2%";
                row.Cells.Add(cell);

                cell = new HtmlTableCell();
                cell.InnerHtml = "INCIDENT DATE";
                cell.Width = "50%";
                row.Cells.Add(cell);

                table1.Rows.Add(row);

                int i = 1;
                foreach (Voilation voilation in complaint.Violations.List)
                {
                    if (!more)
                    {
                        if (i > VoilationDisplay) break;
                    }

                    if (more && i <= VoilationDisplay)
                    {
                        i++;
                        continue;
                    }

                    row = new HtmlTableRow();


                    cell = new HtmlTableCell();
                    cell.InnerHtml = voilation.ViolationDescription.ToString();
                    row.Cells.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerHtml = String.Empty;
                    row.Cells.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerHtml = voilation.IncidentLocationName.ToString();
                    row.Cells.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerHtml = String.Empty;
                    row.Cells.Add(cell);

                    cell = new HtmlTableCell();
                    cell.InnerHtml = voilation.ParseIncidentDateAsString.ToString();   
                    row.Cells.Add(cell);

                    table1.Rows.Add(row);
                    i++;
                }

                if (more)
                    pnVoilations2.Controls.Add(table1);
                else
                    pnVoilations.Controls.Add(table1);

                if (complaint.Violations.List.Count > VoilationDisplay && !more)
                {
                    pageTwo = true;
                    PrintVoilations(true);
                }
           
        }
        /// <summary>
        /// This method will list the witnesses
        /// </summary>
        private void PrintWitness()
        {
            
                int i = 1;
                String strWitness = string.Empty;
                foreach (Witness witness in complaint.Witnesses.List)
                {
                    switch (i)
                    {
                        case 1:
                            lbWitness1.Text = witness.PersonnelName.ToString();
                            break;
                        case 2:
                            lbWitness2.Text = witness.PersonnelName.ToString();
                            break;
                        case 3:
                            lbWitness3.Text = witness.PersonnelName.ToString();
                            break;
                    }

                    if (i > 3)
                    {
                        pageTwo = true;
                        lbWitnessp2.Text += witness.PersonnelName.ToString() + "<br>";
                    }
                    i++;
                }
           
        }
        /// <summary>
        /// This method will print the complaints
        /// </summary>
        private void PrintComplainant()
        {
           
                int i = 0;
                HtmlTable table1 = new HtmlTable();
                HtmlTableRow row;
                HtmlTableCell cell;

                table1.CellPadding = 3;
                table1.CellSpacing = 3;
                table1.Width = "100%";
                table1.Height = "30px";

                foreach (Complainant complainant in complaint.Complainants.List)
                {
                    if (i == 0)
                    {
                        lbComplainantName.Text = complainant.ComplainantName.ToString();
                        lbComplainantTitle.Text = complainant.Title.ToString();
                        lbComplainantLocation.Text = complainant.LocationName.ToString();
                        lbComplainant.Text = complainant.ComplainantName.ToString();

                        String statement = String.Format("[{0}] [{1}]  {2}", complainant.LocationName.ToString(), complainant.Title.ToString(), complainant.Statement.ToString());

                        if (statement.Length > 800 && statement.IndexOf(" ", 800) != -1)
                        {
                            lbStatement.Text = TruncateAtWord(statement, 800) + "(Continued on Page 2)";
                            int start = statement.IndexOf(" ", 800);
                            if (start == -1)
                                start = 801;
                            pageTwo = true;
                            row = new HtmlTableRow();

                            cell = new HtmlTableCell();
                            cell.Width = "20%";
                            cell.VAlign = "top";
                            cell.InnerHtml = String.Empty;
                            row.Cells.Add(cell);

                            cell = new HtmlTableCell();
                            cell.Width = "80%";
                            cell.VAlign = "top";
                            cell.Attributes.Add("TEXT-ALIGN", "justify");
                            cell.InnerHtml = statement.Substring(start);
                            row.Cells.Add(cell);
                            table1.Rows.Add(row);
                        }
                        else
                            lbStatement.Text = String.Format("[{0}] [{1}]  {2}", complainant.LocationName.ToString(), complainant.Title.ToString(), complainant.Statement.ToString());
                    }
                    else
                    {
                        pageTwo = true;
                        row = new HtmlTableRow();

                        cell = new HtmlTableCell();
                        cell.Width = "20%";
                        cell.VAlign = "top";
                        cell.InnerHtml = complainant.ComplainantName.ToString();
                        row.Cells.Add(cell);

                        cell = new HtmlTableCell();
                        cell.Width = "80%";
                        cell.VAlign = "top";
                        cell.Attributes.Add("TEXT-ALIGN", "justify");
                        cell.InnerHtml = String.Format("[{0}] [{1}]  {2}", complaint.LocationName.ToString(), complainant.Title.ToString(), complainant.Statement.ToString());
                        row.Cells.Add(cell);
                        table1.Rows.Add(row);
                    }
                    i++;
                }
                if (pageTwo == true)
                {
                    pnComplainantP2.Controls.Add(table1);
                }            
        }
        /// <summary>
        /// This method will bind the personnel data
        /// </summary>
        private void BindPersonnelData()
        {            
                lbFName.Text = complaint.RespondentFirstName.ToString();
                lbMName.Text = complaint.RespondentMiddleName.ToString();
                lbLName.Text = complaint.RespondentLastName.ToString();
                if (complaint.RespondentDob != DateTime.MinValue)
                    lbDOB.Text = complaint.RespondentDob.ToShortDateString();
                lbRefNo.Text = complaint.ReferenceNumber.ToString();
                if (complaint.RespondentAptDate != DateTime.MinValue)
                    lbApptDt.Text = complaint.RespondentAptDate.ToShortDateString();
                lbPayroll2.Text = complaint.PayCodeDescription.ToString();
                lbFName2.Text = complaint.RespondentFirstName.ToString();
                lbMName2.Text = complaint.RespondentMiddleName.ToString();
                lbLName2.Text = complaint.RespondentLastName.ToString();
                if (complaint.RespondentDob != DateTime.MinValue)
                    lbDOB2.Text = complaint.RespondentDob.ToShortDateString();
                lbRefNo2.Text = complaint.ReferenceNumber.ToString();
                ////lbBadge.Text = complaint.BadgeNumber.ToString();
                //lbTitle.Text = complaint.TitleDescription.ToString();
                //lbBadge2.Text = complaint.BadgeNumber.ToString();
                //lbTitle2.Text = complaint.TitleDescription.ToString();
           
        }
        /// <summary>
        /// This method will bind the complaint data
        /// </summary>
        private void BindComplaintData()
        {            
                if (complaint.ComplaintStatus >= 4)
                    lbIndexNo.Text = complaint.IndexNo.ToString();
                lbDate.Text = complaint.CreatedDate.ToShortDateString();
                lbDistrict.Text = complaint.LocationName.ToString();         
                lblPromotionDate.Text = String.Format("{0:MM/dd/yyyy}", complaint.PromotionDate);
                if (complaint.ComplaintStatus >= 4)
                    lbIndex2.Text = complaint.IndexNo.ToString();
                lbDate2.Text = complaint.CreatedDate.ToShortDateString();
                lbDistrict2.Text = complaint.LocationName.ToString();
                if (complaint.Comments.Length > 250)
                {

                    lbComments.Text = TruncateAtWord(complaint.Comments, 250) + "(Continued on Page 2)";
                    int start = complaint.Comments.IndexOf(" ", 250);
                    lbComments2.Text = complaint.Comments.Substring(start);
                    pageTwo = true;
                }
                else
                    lbComments.Text = complaint.Comments.ToString();

                if (complaint.ChargesServedByName != String.Empty)
                {
                    lbChargesServedBy.Text = complaint.ChargesServedByName.ToString();
                    lbServerBy1.Text = complaint.ChargesServedByName.ToString();
                    lbServerBy2.Text = complaint.ChargesServedByName.ToString();

                }
                if (complaint.ChargesServedDate != DateTime.MinValue)
                {
                    lbChargesServerDt.Text = complaint.ChargesServedDate.ToShortDateString();
                    lbServerDt1.Text = complaint.ChargesServedDate.ToShortDateString();
                    lbServerDt2.Text = complaint.ChargesServedDate.ToShortDateString();
                }

                if (complaint.SuspensionOrderedByName != String.Empty)
                {
                    lbSuspensionBy.Text = complaint.SuspensionOrderedByName.ToString();

                }

                if (complaint.SuspensionOrderDate != DateTime.MinValue)
                    lbSuspensionByDt.Text = complaint.SuspensionOrderDate.ToShortDateString();
                if (complaint.SuspensionLiftedByName != String.Empty)
                {
                    lbSuspensionLiftedBy.Text = complaint.SuspensionLiftedByName.ToString();
                }
                if (complaint.SuspensionLiftedDate != DateTime.MinValue)
                    lbSuspensionLiftedDt.Text = complaint.SuspensionLiftedDate.ToShortDateString();
              
                    chkProbation.Checked = complaint.IsProbation;

                lbApt.Text = complaint.AptNo.ToString();
                lbAddress.Text = complaint.StreetNumber.ToString() + ' ' + complaint.StreetName.ToString();
                lbBorough.Text = complaint.City.ToString();
                lbZip.Text = complaint.ZipCode.ToString();

                //lbVSchedule.Text = complaint.VacationSchedule;
                lbChartNo.Text = complaint.ChartNo;
                lbPayroll.Text = complaint.PayCodeDescription;
                lbPayroll2.Text = complaint.PayCodeDescription;           
        }

        /// <summary>
        /// This method will return the string to validate the search criteria
        /// </summary>
        /// <param name="input"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public string TruncateAtWord(string input, int length)
        {
            if (input == null || input.Length < length)
                return input;
            int iNextSpace = input.IndexOf(" ", length);
            return string.Format("{0}...", input.Substring(0, (iNextSpace > 0) ? iNextSpace : length).Trim());
        }


        /// <summary>
        /// This method will Associate the Incident for Voilation
        /// </summary>
        private void AssociateIncidentToVoilation()
        {
            IncedentList lstIncident;
            IncedentList incidentList=new IncedentList();
            VoilationList voilationList=new VoilationList();     
                foreach (Voilation voilation in voilationList.List)
                {
                    if (voilation != null)
                    {
                        Int16 i = 1;
                        lstIncident = new IncedentList();
                        foreach (Incedent incident in incidentList.List)
                        {
                            if (incident.ChargeId == voilation.ChargeId)
                            {
                                incident.UniqueId = i;
                                lstIncident.List.Add(incident);
                                i++;
                            }
                        }
                        i = (Int16)(i - 1);
                        lstIncident.NewUniqueId = i;
                        voilation.Incidents = lstIncident;
                    }
                }            
        }

    }
}