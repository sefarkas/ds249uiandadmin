﻿


using System.Collections.Generic;
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;
using System.Data;
using DSNY.DSNYCP.ClassHierarchy;
using DSNY.Web.UI.WebControls;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls.WebParts;
using System.Text.RegularExpressions;
using System.Data.Sql;
using System.Configuration;

/// <summary>
/// This class provides the functionality for events and methods for the Approval case Management page.
/// </summary>
/// 

namespace DSNY.DSNYCP.DS249
{
    public partial class Advocate_CaseManagement : System.Web.UI.Page
    {

        #region Page Level Variable
        /// <summary>
        /// Page Level Variable
        /// </summary>
        CodeList codeList;
        AdvocateAssociatedComplaintList assComplaints;
        AdvocateHearingList hearings;
        AdvocatePenaltyList penaltys;
        AdvocateAppealList appeals;
        VoilationList voilationList;
        ComplainantList complainantList;
        ComplaintAdvocacy advocateComplaint;
        Complaint complaint;
        ComplaintList complaintList;
        ComplaintAdvocacy complaintAdvocacy;
        User user;
        UserControl_AdvocateAssociatedcomplaints AssComplaintsControl;
        UserControl_PenaltyControl PenaltyControl;
        UserControl_AppealsControl AppealControl;
        UserControl_HearingsControl HearingControl;
        Int64 ComplaintID;
        Int64 EmployeeID;
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";


        #endregion

        protected int? TotalPages
        {
            get { return (int?)ViewState["tp"]; }
            set { ViewState["tp"] = value; }
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!this.DesignMode)
                Pager1.OnPageChange += delegate(int nextPage, string pagerQueryKey)
                {
                    PopulateGrid(nextPage);

                };
        }

        /// <summary>
        /// This method sorts the databound items in a gridview
        /// </summary>
        /// 

        protected void gvHistory_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string Desc = e.Row.Cells[5].Text.Trim();

                if (Desc == ",")
                {
                    e.Row.Cells[5].Text = "";
                }
                else if (Desc.Substring(0, 1) == ",")
                {
                    e.Row.Cells[5].Text = Desc.Substring(1, Desc.Length - 1);
                }
                else if (Desc.Contains("nbsp") == false)
                {
                    if (Desc.EndsWith(",") == true)
                    {
                        Desc = Desc.Remove(Desc.Length - 1, 1);
                    }
                    Desc += ".";
                    e.Row.Cells[5].Text = Desc;
                }
            }
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        #region LoadDropdowns
        /// <summary>
        /// Load the data in the Dropdowns
        /// </summary>
        private void LoadDropDownLists()
        {
            LoadAdvocateCasestatus();
            LoadStatusAction();
            LoadReviewstatus();
            LoadTrialReturnstatus();
            LoadCOBStatus();

        }

        private void LoadAdvocateCasestatus()
        {
            codeList = new CodeList();
            codeList.Load(13);

            System.Web.UI.WebControls.ListItem item;
            foreach (Code code in codeList.CList)
            {
                item = new System.Web.UI.WebControls.ListItem();
                // Condition is added
                // because user select Send to Advocate. on advocate screen
                // and it causing issue as button is not available
                if (code.CodeId.ToString() != "132")
                {
                    item.Value = code.CodeId.ToString();
                    item.Text = code.CodeName.ToString();
                    ddlCaseStatus.Items.Add(item);
                }
            }
        }

        private void LoadStatusAction()
        {

            codeList = new CodeList();
            codeList.Load(5);
            System.Web.UI.WebControls.ListItem item;
            foreach (Code code in codeList.CList)
            {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlAction.Items.Add(item);
            }

        }

        private void LoadReviewstatus()
        {
            codeList = new CodeList();
            codeList.Load(4);
            System.Web.UI.WebControls.ListItem item;
            foreach (Code code in codeList.CList)
            {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlReviewStatus.Items.Add(item);
            }

        }

        private void LoadTrialReturnstatus()
        {
            codeList = new CodeList();
            codeList.Load(8);

            System.Web.UI.WebControls.ListItem item;
            foreach (Code code in codeList.CList)
            {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlTrailReturnStatus.Items.Add(item);
            }
        }

        private void LoadCOBStatus()
        {
            codeList = new CodeList();
            codeList.Load(17);
            System.Web.UI.WebControls.ListItem item;
            foreach (Code code in codeList.CList)
            {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlCOBStatus.Items.Add(item);
            }

        }

        #endregion

        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnCaseList.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
            btnCaseList.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");

            ibPrint.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_print_roll.png'");
            ibPrint.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_print_static.png'");


            btnSave.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_save_roll.png'");
            btnSave.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_save_static.png'");



            user = (User)Session["clsUser"];
            if (user != null)
            {
                if (!user.IsInMemberShip(GroupName.ADVOCATE))
                    Response.Redirect("~/UI/UnAuthorized.aspx");
                else
                {
                    user.CurrentMembership = GroupName.ADVOCATE;
                    SecurityUtility.AttachRolesToUser(user);
                }

                if (!User.IsInRole("Read"))
                    Response.Redirect("~/UI/UnAuthorized.aspx");
            }
            else
                Response.Redirect("~/UI/Login.aspx");

            if (Session["IsAuthenticated"] == null)
            {
                Response.Redirect("~/UI/login.aspx?Session=False");
            }
            if (Session["IsAuthenticated"].ToString() == "false")
            {
                Response.Redirect("~/UI/login.aspx?Session=False");
            }
            lbMessage.Text = String.Empty;
            initializeUserControls();
            if (Session["Mode"].ToString() == "EDIT")
            {
                ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
            }
            if (Session["Mode"].ToString() == "VIEW")
            {
                ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
            }

            if (!IsPostBack)
            {
                LoadDropDownLists();
                LoadComplaintData();
                LoadComplaintHistory();
            }
            if (Session["MODE"].ToString() == "VIEW")
            {
                if (ddlCaseStatus.SelectedItem.Value == "96")
                {

                    ChangeControlStatus(true);
                    ddlCaseStatus.Enabled = true;
                    rdCombPnlty.Enabled = false;
                }
                else
                {
                    ChangeControlStatus(true);
                    ddlCaseStatus.Enabled = true;
                    rdCombPnlty.Enabled = false;
                }
            }

            if (Session["MODE"].ToString() == "EDIT")
            {
                if (ddlCaseStatus.SelectedItem.Value == "96")
                {
                    ChangeControlStatus(true);
                    ddlCaseStatus.Enabled = true;
                }
                else
                {

                    ChangeControlStatus(true);
                    ddlCaseStatus.Enabled = true;
                }

                CheckCombined();
            }
        }

        #region UserControls
        /// <summary>
        /// This method intialise the usercontrols that are used in this page
        /// </summary>
        private void initializeUserControls()
        {
            initializePenaltyControl();
            initializeAppealControl();
            initializeHearingControl();
            initializeAssComplaintControl();
        }

        #region Penalty Control
        /// <summary>
        /// This method intialise the Penalty usercontrols that are used in this page
        /// </summary>
        private void initializePenaltyControl()
        {
            if (Session["advocatePenaltyList"] == null)
            {
                penaltys = new AdvocatePenaltyList();
                Session["advocatePenaltyList"] = penaltys;
                penaltys.AddPenalty();
            }
            else
                penaltys = (AdvocatePenaltyList)Session["advocatePenaltyList"];

            if (penaltys.List.Count == 0)
            {
                penaltys = new AdvocatePenaltyList();
                Session["advocatePenaltyList"] = penaltys;
                penaltys.AddPenalty();
            }
            CreateAdvocatePenaltyControl();
            txtTotal.Text = CalTotal().ToString();
        }

        /// <summary>
        /// This method creates n number of control from penaltys list with the AdvocatePenalty user Control
        /// </summary>
        private void CreateAdvocatePenaltyControl()
        {
            int i = 1;
            pnPenalty.Controls.Clear();
            foreach (AdvocatePenalty advocatePenalty in penaltys.List)
            {
                PenaltyControl = (UserControl_PenaltyControl)LoadControl("~/UserControl/PenaltyControl.ascx");
                PenaltyControl.ID = "ucAdvocatePenalty" + advocatePenalty.UniqueId;
                if (i > 1)
                {
                    ((ImageButton)PenaltyControl.FindControl("btnRemove")).Visible = true;

                }
                PenaltyControl.RemoveUserControl += this.HandleRemovePenaltyControl;
                PenaltyControl.CalculateTotal += this.TotalPenalty;
                PopulatePenaltyControl(advocatePenalty, PenaltyControl);
                Session["AdvocatePenalty"] = advocatePenalty;
                pnPenalty.Controls.Add(PenaltyControl);
                i++;
            }
        }
        /// <summary>
        /// This method integrates the list of controls to AdvocatePenalty user Control
        /// </summary>
        private void AddPenaltyControl()
        {
            penaltys.AddPenalty();
            CreateAdvocatePenaltyControl();
        }

        /// <summary>
        /// This method removes the controls from the user controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemovePenaltyControl(object sender, EventArgs e)
        {
            ImageButton buttn = sender as ImageButton;
            UserControl_PenaltyControl AdvocatePenaltyControl = (UserControl_PenaltyControl)buttn.Parent;
            Int64 UniqueID = Convert.ToInt64(((HiddenField)AdvocatePenaltyControl.FindControl("hdnPenaltyUniqueID")).Value);
            penaltys.RemovePenalty(UniqueID);
            pnPenalty.Controls.Remove(AdvocatePenaltyControl);
            txtTotal.Text = CalTotal().ToString();
        }
        /// <summary>
        ///  This method populates the controls to the user controls
        /// </summary>
        /// <param name="advocatePenalty"></param>
        /// <param name="ctrPnlty"></param>
        private void PopulatePenaltyControl(AdvocatePenalty advocatePenalty, Control ctrPnlty)
        {
            ((HiddenField)ctrPnlty.FindControl("hdnPenaltyUniqueID")).Value = advocatePenalty.UniqueId.ToString();
            ((TextBox)ctrPnlty.FindControl("txtPltyValue")).Text = advocatePenalty.DataValue.ToString();
            if (advocatePenalty.Term.ToString() == "-1")
            {
                ((TextBox)ctrPnlty.FindControl("txtTerm")).Text = "";
            }
            else
            {
                ((TextBox)ctrPnlty.FindControl("txtTerm")).Text = advocatePenalty.Term.ToString();
            }
            ((DropDownList)ctrPnlty.FindControl("ddlPtlyTerm_Desc")).SelectedValue = advocatePenalty.Term_Desc.ToString();

            ((TextBox)ctrPnlty.FindControl("txtpltycomments")).Text = advocatePenalty.PenaltyComments.ToString();

            ((CheckBox)ctrPnlty.FindControl("chkboxAdd")).Checked = advocatePenalty.ApplyToTotal;
            if (advocatePenalty.ApplyToTotal == true && advocatePenalty.Term_Desc == "DAYS")
            {
                ((CheckBox)ctrPnlty.FindControl("chkboxAdd")).Checked = true;
                ((CheckBox)ctrPnlty.FindControl("chkboxAdd")).Enabled = true;
            }
            else
            {
                ((CheckBox)ctrPnlty.FindControl("chkboxAdd")).Checked = false;
                ((CheckBox)ctrPnlty.FindControl("chkboxAdd")).Enabled = false;
            }

        }

        #endregion

        #region Appeal Control

        private void AddAppealControl()
        {
            appeals.AddAppeal();
            CreateAppealControl();

        }

        private void CreateAppealControl()
        {

            int i = 1;
            PnAppeal.Controls.Clear();
            foreach (AdvocateAppeal advocateAppeal in appeals.List)
            {
                AppealControl = (UserControl_AppealsControl)LoadControl("~/UserControl/AppealsControl.ascx");
                AppealControl.ID = "ucAppeal" + advocateAppeal.UniqueId;
                if (i > 1)
                {
                    ((ImageButton)AppealControl.FindControl("btnRemove")).Visible = true;
                }
                AppealControl.RemoveUserControl += this.HandleRemoveAppealCntrl;
                PopulateAppealData(advocateAppeal, AppealControl);
                PnAppeal.Controls.Add(AppealControl);
                i++;
            }
        }

        public void HandleRemoveAppealCntrl(object sender, EventArgs e)
        {
            ImageButton buttn = sender as ImageButton;
            UserControl_AppealsControl AppealControl = (UserControl_AppealsControl)buttn.Parent;
            Int64 UniqueID = Convert.ToInt64(((HiddenField)AppealControl.FindControl("hdnAppealUniqueID")).Value);
            appeals.RemoveAppeal(UniqueID);
            PnAppeal.Controls.Remove(AppealControl);
        }

        private void initializeAppealControl()
        {
            if (Session["advocateAppealList"] == null)
            {
                appeals = new AdvocateAppealList();
                Session["advocateAppealList"] = appeals;
                appeals.AddAppeal();
            }
            else
                appeals = (AdvocateAppealList)Session["advocateAppealList"];
            if (appeals.List.Count == 0)
            {
                appeals = new AdvocateAppealList();
                Session["advocateAppealList"] = appeals;
                appeals.AddAppeal();
            }
            CreateAppealControl();
        }

        private void PopulateAppealData(AdvocateAppeal advocateAppeal, Control ctrAppeal)
        {
            ((HiddenField)ctrAppeal.FindControl("hdnAppealUniqueID")).Value = advocateAppeal.UniqueId.ToString();
            if (advocateAppeal.AppealDate != DateTime.MinValue && advocateAppeal.AppealDate != null)
                ((TextBox)ctrAppeal.FindControl("txtAppealDate")).Text = Convert.ToDateTime(advocateAppeal.AppealDate).ToShortDateString();
            else
                ((TextBox)ctrAppeal.FindControl("txtAppealDate")).Text = null;
            ((TextBox)ctrAppeal.FindControl("txtAppealcommnts")).Text = advocateAppeal.AppealComments.ToString();
        }

        #endregion

        #region Hearing Control

        private void initializeHearingControl()
        {

            if (Session["AdvocateHearingList"] == null)
            {
                hearings = new AdvocateHearingList();
                Session["AdvocateHearingList"] = hearings;
                hearings.AddHearing();
            }
            else
                hearings = (AdvocateHearingList)Session["AdvocateHearingList"];
            if (hearings.List.Count == 0)
            {
                hearings = new AdvocateHearingList();
                Session["AdvocateHearingList"] = hearings;
                hearings.AddHearing();
            }
            CreateHearingControl();
        }

        private void CreateHearingControl()
        {
            int i = 1;
            pnHearing.Controls.Clear();
            foreach (AdvocateHearing hearing in hearings.List)
            {
                HearingControl = (UserControl_HearingsControl)LoadControl("~/UserControl/HearingsControl.ascx");
                HearingControl.ID = "ucHearing" + hearing.UniqueId;
                if (i > 1)
                {
                    ((ImageButton)HearingControl.FindControl("btnRemove")).Visible = true;
                }
                HearingControl.RemoveUserControl += this.HandleRemoveHearingCntrl;
                PopulateHearingData(hearing, HearingControl);
                pnHearing.Controls.Add(HearingControl);
                i++;
            }
        }

        private void PopulateHearingData(AdvocateHearing hearing, Control crtHearing)
        {
            ((HiddenField)crtHearing.FindControl("hdnHearingUniqueID")).Value = hearing.UniqueId.ToString();
            if ((hearing.HearingDate != DateTime.MinValue) && hearing.HearingDate != null)
                ((TextBox)crtHearing.FindControl("txtHearingDt")).Text = Convert.ToDateTime(hearing.HearingDate).ToShortDateString();
            ((TextBox)crtHearing.FindControl("txtHearingComments")).Text = hearing.HearingComments.ToString();

        }

        public void HandleRemoveHearingCntrl(object sender, EventArgs e)
        {
            ImageButton buttn = sender as ImageButton;
            UserControl_HearingsControl HearingControl = (UserControl_HearingsControl)buttn.Parent;
            Int64 UniqueID = Convert.ToInt64(((HiddenField)HearingControl.FindControl("hdnHearingUniqueID")).Value);
            hearings.RemoveHearing(UniqueID);
            pnHearing.Controls.Remove(HearingControl);
        }

        private void AddHearingControl()
        {
            hearings.AddHearing();
            CreateHearingControl();
        }


        #endregion

        #region Associated Complaints

        private void initializeAssComplaintControl()
        {
            if (Session["AssComplaintList"] == null)
            {
                assComplaints = new AdvocateAssociatedComplaintList();
                Session["AssComplaintList"] = assComplaints;
                assComplaints.AddAssociatedComplaint();
            }
            else
                assComplaints = (AdvocateAssociatedComplaintList)Session["AssComplaintList"];
            if (assComplaints.List.Count == 0)
            {
                assComplaints = new AdvocateAssociatedComplaintList();
                Session["AssComplaintList"] = assComplaints;
                assComplaints.AddAssociatedComplaint();
            }
            CreateAssComplaintControl();
        }

        private void CreateAssComplaintControl()
        {
            int i = 1;
            pnAssComplaints.Controls.Clear();
            foreach (AdvocateAssociatedComplaint assComplaint in assComplaints.List)
            {
                AssComplaintsControl = (UserControl_AdvocateAssociatedcomplaints)LoadControl("~/UserControl/AdvocateAssociatedcomplaints.ascx");
                AssComplaintsControl.ID = "ucAssComplaint" + assComplaint.UniqueId;
                if (i > 1)
                {
                    ((ImageButton)AssComplaintsControl.FindControl("btnRemove")).Visible = true;
                }
                AssComplaintsControl.RemoveUserControl += this.HandleRemoveAssComplaintsControl;
                PopulateAssociatedComplaintData(assComplaint, AssComplaintsControl);
                pnAssComplaints.Controls.Add(AssComplaintsControl);
                i++;
            }
        }

        private void PopulateAssociatedComplaintData(AdvocateAssociatedComplaint assComplaint, Control crtAssComplaint)
        {

            ((HiddenField)crtAssComplaint.FindControl("hdnAssCompUniqueID")).Value = assComplaint.UniqueId.ToString();
            if (assComplaint.AssociatedComplaintId != -1)
                ((TextBox)crtAssComplaint.FindControl("txtcomplaints")).Text = assComplaint.AssociatedComplaintId.ToString();

        }

        public void HandleRemoveAssComplaintsControl(object sender, EventArgs e)
        {
            ImageButton buttn = sender as ImageButton;
            UserControl_AdvocateAssociatedcomplaints AssComplaintControl = (UserControl_AdvocateAssociatedcomplaints)buttn.Parent;
            Int64 UniqueID = Convert.ToInt64(((HiddenField)AssComplaintControl.FindControl("hdnAssCompUniqueID")).Value);
            assComplaints.RemoveAssociatedComplaint(UniqueID);
            pnAssComplaints.Controls.Remove(AssComplaintControl);
        }

        private void AddAssComplaintControl()
        {
            assComplaints.AddAssociatedComplaint();
            CreateAssComplaintControl();
        }


        #endregion

        #endregion

        #region Private Methods

        private void LoadComplaintHistory()
        {
            AdvocateHistory complaintList = new AdvocateHistory();
            EmployeeID = (Int64)Session["RespEmployeeId"];
            ComplaintID = (Int64)Session["ComplaintID"];
            List<AdvocateHistory> objAdvocateHistory = complaintList.Load(EmployeeID, ComplaintID);
            DataTable dt = MergeSearchData(objAdvocateHistory);
            DataView dv;
            dv = new DataView(dt);
            gvHistory.DataSource = dv;
            gvHistory.DataBind();
        }

        public DataTable MergeSearchData(List<AdvocateHistory> complaintList)
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("ComplaintID", Type.GetType("System.Int64"));
            dt.Columns.Add("IndexNo", Type.GetType("System.Int64"));
            dt.Columns.Add("CreatedDate", Type.GetType("System.DateTime"));
            dt.Columns.Add("Charges", Type.GetType("System.String"));
            dt.Columns.Add("ChargesDesc", Type.GetType("System.String"));
            dt.Columns.Add("Penalty", Type.GetType("System.String"));
            dt.Columns.Add("ParentIndexNo", Type.GetType("System.String"));
            dt.Columns.Add("Jt1", Type.GetType("System.String"));

            DataRow dr;

            foreach (AdvocateHistory complaint in complaintList)
            {

                dr = dt.NewRow();

                dr["ComplaintID"] = complaint.ComplaintID;
                dr["IndexNo"] = complaint.ndexNo;
                dr["CreatedDate"] = complaint.VoilationDate;
                dr["Charges"] = complaint.Charges;
                dr["ChargesDesc"] = complaint.ChargesDesc;
                dr["Penalty"] = complaint.Penalty;
                dr["JT1"] = complaint.JT1;
                if (complaint.ParentIndexNo == -1)
                {
                    dr["ParentIndexNo"] = "";
                }
                else
                {
                    dr["ParentIndexNo"] = Convert.ToString(complaint.ParentIndexNo);
                }
                dt.Rows.Add(dr);

            }
            return dt;
        }

        private void DisplayButtonBasedOnPermission()
        {
            if (!User.IsInRole("Write"))
            {
                btnSave.Visible = false;
            }

            if (!User.IsInRole("Print"))
            {
                ibPrint.Visible = false;
            }

        }

        public void TotalPenalty(object sender, EventArgs e)
        {
            txtTotal.Text = CalTotal().ToString();
        }

        private double CalTotal()
        {
            double total = 0;
            foreach (AdvocatePenalty advocatePenalty in penaltys.List)
            {
                if (advocatePenalty.ApplyToTotal == true && advocatePenalty.Term_Desc == "DAYS")
                {
                    total += advocatePenalty.Term;
                }
            }
            if (total < 0)
                total = 0;
            return total;
        }
        private void LoadComplaintData()
        {

            Int64 ID = Convert.ToInt64(Request.QueryString["Id"]);
            complaint = new Complaint();
            complaint.Load(ID, RequestSource.Advocate);
            if (complaint.ComplaintId != -1)
            {

                ComplaintID = complaint.ComplaintId;
                EmployeeID = complaint.EmployeeId;
                Session["RespEmployeeId"] = EmployeeID;
                Session["ComplaintID"] = ComplaintID;
                Session["IndexNo"] = complaint.IndexNo;

                Session["AdvocateComplaint"] = complaint.ComplaintAdvocacy;

                penaltys = new AdvocatePenaltyList();
                penaltys = complaint.ComplaintAdvocacy.PenaltyList;
                penaltys.NewUniqueId = Convert.ToInt16(penaltys.List.Count);
                Session["AdvocatePenaltyList"] = penaltys;

                assComplaints = new AdvocateAssociatedComplaintList();
                assComplaints = complaint.ComplaintAdvocacy.AssociatedComplaints;
                assComplaints.NewUniqueId = Convert.ToInt16(assComplaints.List.Count);
                Session["AssComplaintList"] = assComplaints;


                hearings = new AdvocateHearingList();
                hearings = complaint.ComplaintAdvocacy.HearingList;
                hearings.NewUniqueId = Convert.ToInt16(hearings.List.Count);
                Session["AdvocateHearingList"] = hearings;


                appeals = new AdvocateAppealList();
                appeals = complaint.ComplaintAdvocacy.AppealList;
                appeals.NewUniqueId = Convert.ToInt16(appeals.List.Count);
                Session["advocateAppealList"] = appeals;

                BindAdvocateComplaint();

                BindPersonnelData();

                complainantList = new ComplainantList();
                complainantList = complaint.Complainants;
                Session["AdvocateComplainantList"] = complaint.Complainants;
                BindComplaintantData();

                voilationList = new VoilationList();
                voilationList = complaint.Violations;
                Session["voilationList"] = complaint.Violations;
                BindCharges();

                initializeUserControls();
            }
            else
            {
                Response.Redirect("Dashboard.aspx");
            }
        }

        private void BindAdvocateComplaint()
        {
            advocateComplaint = complaint.ComplaintAdvocacy;

            if (advocateComplaint.FinalStatusCD != 0)
                ddlCaseStatus.Items.FindByValue(advocateComplaint.FinalStatusCD.ToString()).Selected = true;

            hdnValue.Value = ddlCaseStatus.SelectedItem.Value;
            hdnText.Value = ddlCaseStatus.SelectedItem.ToString();

            if (!string.IsNullOrEmpty(advocateComplaint.AdvocacyCaseId.ToString()))
                txtCaseID.Text = advocateComplaint.AdvocacyCaseId.ToString();

            if (advocateComplaint.RecieveDate != DateTime.MinValue)
                txtRecDate.Text = advocateComplaint.RecieveDate.ToShortDateString();

            if (!String.IsNullOrEmpty(advocateComplaint.RecieveUserName))
                txtRecBy.Text = advocateComplaint.RecieveUserName;

            if (!String.IsNullOrEmpty(complaint.IndexNo.ToString()))
                txtCompIdx.Text = complaint.IndexNo.ToString();

            if (advocateComplaint.ModifiedDate != DateTime.MinValue)
                txtModDate.Text = advocateComplaint.ModifiedDate.ToShortDateString();

            if (!String.IsNullOrEmpty(advocateComplaint.ModifiedUserName))
                txtModBy.Text = advocateComplaint.ModifiedUserName;

            if (!String.IsNullOrEmpty(advocateComplaint.CaseComment))
                txtCaseStatus.Text = advocateComplaint.CaseComment;

            if (advocateComplaint.ActionCD != -1)
                if (ddlAction.Items.IndexOf(ddlAction.Items.FindByValue(advocateComplaint.ActionCD.ToString())) != -1)
                    ddlAction.Items.FindByValue(advocateComplaint.ActionCD.ToString()).Selected = true;

            if (advocateComplaint.ActionDate != DateTime.MinValue && advocateComplaint.ActionDate != null)
                txtActionDt.Text = Convert.ToDateTime(advocateComplaint.ActionDate).ToShortDateString();
            else
                advocateComplaint.ActionDate = null;

            if (advocateComplaint.ReviewStatusCD != -1)
                if (ddlReviewStatus.Items.IndexOf(ddlReviewStatus.Items.FindByValue(advocateComplaint.ReviewStatusCD.ToString())) != -1)
                    ddlReviewStatus.Items.FindByValue(advocateComplaint.ReviewStatusCD.ToString()).Selected = true;

            if (advocateComplaint.TrialReturnStatusCD != -1)
                if (ddlTrailReturnStatus.Items.IndexOf(ddlTrailReturnStatus.Items.FindByValue(advocateComplaint.TrialReturnStatusCD.ToString())) != -1)
                    ddlTrailReturnStatus.Items.FindByValue(advocateComplaint.TrialReturnStatusCD.ToString()).Selected = true;

            if (advocateComplaint.CobStatus_CD != -1)
                if (ddlCOBStatus.Items.IndexOf(ddlCOBStatus.Items.FindByValue(advocateComplaint.CobStatus_CD.ToString())) != -1)
                    ddlCOBStatus.Items.FindByValue(advocateComplaint.CobStatus_CD.ToString()).Selected = true;

            if (advocateComplaint.CobDate != DateTime.MinValue && advocateComplaint.CobDate != null)
                txtCOBDt.Text = Convert.ToDateTime(advocateComplaint.CobDate).ToShortDateString();
            else
                txtCOBDt.Text = null;

            if ((advocateComplaint.ClosedDate == DateTime.MinValue))
                txtCloseDate.Text = null;
            else
                txtCloseDate.Text = Convert.ToDateTime(advocateComplaint.ClosedDate).ToShortDateString();

            if (advocateComplaint.ClosedDate == null)
                txtCloseDate.Text = null;

            if (advocateComplaint.IsProbation || !advocateComplaint.IsProbation)
                chkProbation.Checked = advocateComplaint.IsProbation;
            else
                chkProbation.Checked = false;

        }

        private void BindPersonnelData()
        {
            txtName.Text = complaint.FullName.ToString();
            txtRefNo.Text = complaint.ReferenceNumber.ToString();
            if (complaint.RespondentAptDate != DateTime.MinValue)
                txtAppointment.Text = complaint.RespondentAptDate.ToShortDateString();
            txtTitle.Text = complaint.TitleDescription.ToString();
        }

        private void BindComplaintantData()
        {

            rptCompName.DataSource = complainantList.List;
            rptCompName.DataBind();
            rptCompTitle.DataSource = complainantList.List;
            rptCompTitle.DataBind();
            rptCompDistrict.DataSource = complainantList.List;
            rptCompDistrict.DataBind();
        }

        private void BindCharges()
        {
            rptCharges.DataSource = voilationList.List;
            rptCharges.DataBind();
        }

        private void SaveAdvocateComplaint()
        {
            Type cstype = this.GetType();
            if (advocateComplaint == null)
                advocateComplaint = (ComplaintAdvocacy)Session["AdvocateComplaint"];
            if (ddlCaseStatus.SelectedItem.Value == "131")
            {
                advocateComplaint.ReturnToAuthor(user.PersonalId);
                ClientScript.RegisterStartupScript(cstype, @"message", @"<script>alert('Case Returned to Author!');document.location ='Dashboard.aspx';</script>");
            }
            else if (ddlCaseStatus.SelectedItem.Value == "130")
            {
                advocateComplaint.ReturnToBcad(user.PersonalId);
                ClientScript.RegisterStartupScript(cstype,@"message", @"<script>alert('Case Sent to BCAD!');document.location ='Dashboard.aspx';</script>");
            }
            else
            {
                UpdateAdvocateComplaint();
                if (advocateComplaint.Save())
                {
                    LoadComplaintHistory();

                    lbMessage.Text = "Complaint Saved Successfully.";
                    ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
                }
                else
                    lbMessage.Text = "Complaint Could Not Be Saved.";
            }

        }

        private void UpdateAdvocateComplaint()
        {
            advocateComplaint.ActionCD = Convert.ToInt64(ddlAction.SelectedItem.Value);
            if (txtActionDt.Text != string.Empty)
                advocateComplaint.ActionDate = Convert.ToDateTime(txtActionDt.Text);
            else
                advocateComplaint.ActionDate = null;

            advocateComplaint.CaseComment = txtCaseStatus.Text;

            if (txtCloseDate.Text != String.Empty)
                advocateComplaint.ClosedDate = Convert.ToDateTime(txtCloseDate.Text);
            else
                advocateComplaint.ClosedDate = null;
            if (txtCOBDt.Text != String.Empty)
                advocateComplaint.CobDate = Convert.ToDateTime(txtCOBDt.Text);
            else
                advocateComplaint.CobDate = null;
            advocateComplaint.CobStatus_CD = Convert.ToInt64(ddlCOBStatus.SelectedItem.Value);
            advocateComplaint.FinalStatusCD = Convert.ToInt16(ddlCaseStatus.SelectedItem.Value);
            advocateComplaint.ModifiedUser = Convert.ToInt16(Session["UserEmpID"]);
            advocateComplaint.ReviewStatusCD = Convert.ToInt64(ddlReviewStatus.SelectedItem.Value);
            advocateComplaint.TrialReturnStatusCD = Convert.ToInt64(ddlTrailReturnStatus.SelectedItem.Value);
            advocateComplaint.AssociatedComplaints = assComplaints;
            advocateComplaint.HearingList = hearings;
            advocateComplaint.PenaltyList = penaltys;
            advocateComplaint.AppealList = appeals;
            advocateComplaint.ChargeList = AssembleChargeList();

        }


        private VoilationList AssembleChargeList()
        {
            voilationList = (VoilationList)Session["voilationList"];
            foreach (RepeaterItem rItem in rptCharges.Items)
            {
                Int64 chargeID = Convert.ToInt64(((HiddenField)rItem.FindControl("hdnChargeID")).Value);
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.ChargeId == chargeID; });
                voilation.OverRideViolationDescription = ((TextBox)rItem.FindControl("txtOverRideCharges")).Text.ToString();
                voilation.PrimaryIndicator = ((GroupRadioButton)rItem.FindControl("rbPrimary")).Checked;
                voilation.Incidentdate = Convert.ToDateTime(((TextBox)rItem.FindControl("txtIncidentDate")).Text);
            }
            return voilationList;
        }

        private void ChangeControlStatus(bool status)
        {

            if (Session["Mode"].ToString() == "NEW")
            {
                ibPrint.Enabled = status;
            }
            foreach (Control c in uPanel.Controls)
            {
                foreach (Control ctrl in c.Controls)
                {
                    if (ctrl is TextBox)
                        ((TextBox)ctrl).Enabled = status;
                    else if (ctrl is Button)
                        ((Button)ctrl).Enabled = status;
                    else if (ctrl is RadioButton)
                        ((RadioButton)ctrl).Enabled = status;
                    else if (ctrl is ImageButton)
                        ((ImageButton)ctrl).Visible = status;
                    else if (ctrl is System.Web.UI.WebControls.Image)

                        ((System.Web.UI.WebControls.Image)ctrl).Visible = status;

                    else if (ctrl is CheckBox)

                        ((CheckBox)ctrl).Enabled = status;

                    else if (ctrl is DropDownList)
                        ((DropDownList)ctrl).Enabled = status;

                    else if (ctrl is HyperLink)
                        ((HyperLink)ctrl).Enabled = status;

                    else if (ctrl is Repeater)
                    {
                        foreach (Control rc in ctrl.Controls)
                        {
                            foreach (Control ri in rc.Controls)
                            {
                                if (ri is TextBox)
                                    ((TextBox)ri).Enabled = status;
                                else if (ri is RadioButton)
                                    ((RadioButton)ri).Enabled = status;
                                else if (ri is CheckBox)
                                    ((CheckBox)ri).Enabled = status;
                            }
                        }
                    }
                    else if (ctrl is PlaceHolder)
                    {
                        if (ctrl.ID == "pnPenalty")
                        {
                            foreach (UserControl_PenaltyControl control in ctrl.Controls)
                            {
                                control.Enable = status;
                            }
                        }
                        if (ctrl.ID == "PnAppeal")
                        {
                            foreach (UserControl_AppealsControl control in ctrl.Controls)
                            {
                                control.Enable = status;
                            }
                        }
                        if (ctrl.ID == "pnHearing")
                        {
                            foreach (UserControl_HearingsControl control in ctrl.Controls)
                            {
                                control.Enable = status;
                            }
                        }

                        if (ctrl.ID == "pnAssComplaints")
                        {
                            foreach (UserControl_AdvocateAssociatedcomplaints control in ctrl.Controls)
                            {
                                control.Enable = status;
                            }
                        }
                    }
                }
            }

        }
        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string[] SearchComplaints(string prefixText, int count)
        {
            ArrayList arrEmp = new ArrayList();
            ComplaintList cList = new ComplaintList();
            cList.Load(prefixText);
            if (cList.List.Count > 0)
            {
                foreach (Complaint complaint in cList.List)
                {
                    arrEmp.Add(complaint.IndexNo.ToString());
                }
            }
            return (string[])arrEmp.ToArray(typeof(string));
        }

        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string[] GetCharges(string prefixText, int count)
        {
            return (string[])Common.GetCharges(prefixText, count);
        }
        #endregion
        /// <summary>
        /// This method loads the data to the datareapeter control
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        protected void rptCharges_ItemDataBound(object source, RepeaterItemEventArgs e)
        {

            CheckBox chk = (CheckBox)e.Item.FindControl("chkBox");
            TextBox txtOverRide = (TextBox)e.Item.FindControl("txtOverRideCharges");
            chk.Attributes.Add("OnClick", "javascript:return EnableOverRideCharges('" + chk.ClientID + "','" + txtOverRide.ClientID + "');");
            String chkPrimary = ((HiddenField)e.Item.FindControl("hdnPrimary")).Value;
            if (chkPrimary == "True")
                ((GroupRadioButton)e.Item.FindControl("rbPrimary")).Checked = true;
            else
                ((GroupRadioButton)e.Item.FindControl("rbPrimary")).Checked = false;
            //Add code
            ImageButton View = (ImageButton)e.Item.FindControl("ImgView");
            Int64 chargeID = Convert.ToInt64(((HiddenField)e.Item.FindControl("hdnChargeID")).Value);
            Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.ChargeId == chargeID; });
            TextBox txtCompID = (TextBox)e.Item.FindControl("txtComplaintID");
            // string onclick = "javascript:window.open('Upload.aspxId=" + txtCompID.Text + "&indexId=0&IncidentId=0&ChargeCode=" + chargeID.ToString() + "','mywindow','status=1','toolbar=1','width=800','height=1000');";
            //string onclick = "javascript:window.open('Upload.aspx?ChildID1=" + hdnChargeRefNo.Value.ToString() + "&Mode=" + Session["Mode"].ToString() + "&ParentID=" + complaint.ComplaintId + "','UpLoad','scrollbars=yes,resizable=yes,width=400,height=200');return false;";
            string onclick = "javascript:window.open('../Complaints/Upload.aspx?ChildID1=" + voilation.Charge.ChargeCode + "&Mode=" + Session["Mode"].ToString() + "&ParentID=" + voilation.ComplaintId + "&PageFrom=Advocate" + "','UpLoad','scrollbars=yes,resizable=yes,width=400,height=200');return false;";
            View.Attributes.Add("OnClick", onclick);
            txtCompID.Visible = false;
            TextBox txtCharges = (TextBox)e.Item.FindControl("txtCharges");
            TextBox txtIncidentDate = (TextBox)e.Item.FindControl("txtIncidentDate");
            String refNo = ExtractRefNo(txtCharges.Text);
            Int64 Id = Convert.ToInt64(txtCompID.Text);
        }
        protected void gvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton viewFile = (ImageButton)e.Row.Cells[5].FindControl("ViewButton");
                string onclick = "javascript:window.open('../Complaints/ViewDocument.aspx?Id=" + e.Row.Cells[0].Text + "&indexId=" + e.Row.Cells[1].Text + "&IncidentId=" + e.Row.Cells[2].Text + "&ChargeCode=" + e.Row.Cells[3].Text + "','mywindow','status=1','toolbar=1','width=800','height=1000');";
                viewFile.Attributes.Add("onclick", onclick);
                ImageButton View = (ImageButton)e.Row.FindControl("ViewButton");
                View.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_view_roll.png'");
                View.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_view_static.png'");
            }
        }
        private String ExtractRefNo(String text)
        {
            if (text != String.Empty)
            {
                return text.Substring(0, text.IndexOf(" ")).Trim();
            }
            else
            {
                return string.Empty;
            }

        }

        /// <summary>
        /// This button click event intialises the the user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewAppeal_Click(object sender, ImageClickEventArgs e)
        {
            AddAppealControl();

        }
        /// <summary>
        /// This button click event intialises the the user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewPlty_Click(object sender, ImageClickEventArgs e)
        {
            AddPenaltyControl();
        }
        /// <summary>
        /// This button click event will redirect the page to the dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCaseList_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }
        /// <summary>
        /// This button click event will redirect the page to the dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCase_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }
        /// <summary>
        /// This button click event will redirect the page to the dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibCancel_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       

        protected void txtCaseSearch_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// This button click event intialises the the user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewAssComp_Click(object sender, ImageClickEventArgs e)
        {
            AddAssComplaintControl();
        }
        /// <summary>
        /// This button click event intialises the the user control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewHearing_Click(object sender, ImageClickEventArgs e)
        {
            AddHearingControl();
        }
        /// <summary>
        /// This button click event will save the advocate complaint to the database 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {
            SaveAdvocateComplaint();
        }

        protected void btnSaveCmb_Click(object sender, EventArgs e)
        {
            rdCombPnlty.Checked = false;
            UpdateCombinedComplaints();
            lblSaveMsg.Text = "Combine/Uncombine Complaints Saved Successfully";
            LoadComplaintHistory();

        }

        /// <summary>
        /// This button click event will save the advocate complaint to the database 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void UpdateCombinedComplaints()
        {

            CheckBox chk;
            DataTable dt = new DataTable();
            String[] indexno = new String[gvAdvocate.Rows.Count];
            String cmbIndx = string.Empty;
            string unCmbIndx = string.Empty;
            Int64 cmbParentIndexNo = 0;
            Int64 uncmbParentIndexNo = 0;
            complaint = new Complaint();
            for (int i = 0; i < gvAdvocate.Rows.Count; i++)
            {
                chk = (CheckBox)gvAdvocate.Rows[i].Cells[0].FindControl("chkCombine");

                if (chk.Checked == true)
                {
                    indexno[i] = (gvAdvocate.Rows[i].Cells[2].Text);
                    cmbParentIndexNo = (Int64)Session["IndexNo"];
                    cmbIndx = indexno[i] + ',' + cmbIndx;

                }
                else if (chk.Checked == false)
                {
                    indexno[i] = gvAdvocate.Rows[i].Cells[2].Text;
                    uncmbParentIndexNo = 0;
                    unCmbIndx = indexno[i] + ',' + unCmbIndx;
                }
            }

            //Combined complaints

            if (cmbIndx.Length > 0)
            {
                cmbIndx = cmbIndx.Substring(0, cmbIndx.Length - 1);
            }

            //Uncombined complaints

            if (unCmbIndx.Length > 0)
            {
                unCmbIndx = unCmbIndx.Substring(0, unCmbIndx.Length - 1);
            }
            complaint.SaveCombinedComplaints(cmbIndx, cmbParentIndexNo, unCmbIndx, uncmbParentIndexNo);
        }

        protected void CheckCombined()
        {

            Int64 ParentIndexCount;
            Int64 IsChild = 0;
            complaint = new Complaint();
            ParentIndexCount = complaint.CheckIsParent((Int64)Session["IndexNo"], ref IsChild);

            //If Action is calendar or O.a.t.h then enable combine complaint
            if (IsChild == 0)
            {

                rdCombPnlty.Enabled = false;
                ddlAction.Enabled = false;
                return;
            }

            if (((ParentIndexCount == 0) && (ddlAction.SelectedItem.Value == "39" || ddlAction.SelectedItem.Value == "40" || ddlAction.SelectedItem.Value == "41" || ddlAction.SelectedItem.Value == "44" || ddlAction.SelectedItem.Value == "46")) || (ParentIndexCount > 0))
            {
                rdCombPnlty.Enabled = true;
                if (ParentIndexCount > 0)
                {
                    ddlAction.Enabled = false;
                }
                else
                {
                    ddlAction.Enabled = true;
                }

            }

            else if ((ddlAction.SelectedItem.Value != "46" && ddlAction.SelectedItem.Value != "44" && ddlAction.SelectedItem.Value != "39" && ddlAction.SelectedItem.Value != "40" && ddlAction.SelectedItem.Value != "41"))// && ddlAction.SelectedItem.Value != "-1")
            {
                rdCombPnlty.Enabled = false;
                ddlAction.Enabled = true;
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            rdCombPnlty.Checked = false;
            LoadComplaintHistory();
            modalCombinedPntly.Hide();
        }

        protected void rdCombPnlty_CheckedChanged(object sender, EventArgs e)
        {
            lblSaveMsg.Text = "";
            int pageNo = 0;
            PopulateGrid(pageNo);
            modalCombinedPntly.Show();
        }

        /// <summary>
        /// Populates the data in the gridview
        /// </summary>

        private void PopulateGrid(int pageNo)
        {
            Int64 complaintId, indexNo;
            Int64 rowsCount = 0;
            int maximumRows = Convert.ToInt32(ConfigurationManager.AppSettings["MaximumRows"]);
            EmployeeID = Convert.ToInt64(Session["RespEmployeeId"]);
            complaintId = Convert.ToInt64(Session["ComplaintID"]);
            indexNo = Convert.ToInt64(Session["IndexNo"]);
            DataTable dt = MergeData(pageNo, EmployeeID, maximumRows, complaintId, indexNo, ref rowsCount);
            DataView dv = new DataView(dt);
            ViewState["gridData"] = dt;
            gvAdvocate.DataSource = dv;
            gvAdvocate.DataBind();
            TotalPages = Convert.ToInt32(rowsCount);
            Pager1.TotalResults = this.TotalPages.Value;
            Pager1.BindPager();
            if (dt.Rows.Count == 0)
            {
                btnSaveCmb.Enabled = false;
            }
            if ((TotalPages / 50) <= 1)
            {
                Pager1.Visible = false;
            }

        }
        /// <summary>
        /// Merge the data from the datatable to the colums of the datagrid
        /// </summary>
        /// <returns></returns>
        private DataTable MergeData(int pageNo, Int64 employeeID, int maximumRows, Int64 complaintId, Int64 indexNo, ref Int64 rowsCount)
        {

            LoadCombinedComplaints(pageNo, EmployeeID, maximumRows, complaintId, indexNo, ref rowsCount);
            DataTable dt = new DataTable();
            dt.Columns.Add("IncidentDate", Type.GetType("System.DateTime"));
            dt.Columns.Add("CaseId", Type.GetType("System.Int64"));
            dt.Columns.Add("IndexNo", Type.GetType("System.Int64"));
            dt.Columns.Add("Status", Type.GetType("System.String"));
            dt.Columns.Add("ParentIndexNo", Type.GetType("System.Int64"));
            dt.Columns.Add("Charge", Type.GetType("System.String"));
            dt.Columns.Add("LocationName", Type.GetType("System.String"));
            dt.Columns.Add("FirstName", Type.GetType("System.String"));
            DataRow dr;
            foreach (Complaint complaint in complaintList.List)
            {
                dr = dt.NewRow();
                dr["IncidentDate"] = complaint.IncidentDate.ToShortDateString();
                dr["IndexNo"] = complaint.IndexNo.ToString();
                dr["CaseID"] = complaint.CaseId;
                dr["Status"] = complaint.Status;
                dr["ParentIndexNo"] = complaint.ParentIndexNo.ToString();
                dr["Charge"] = complaint.Charge.Trim();
                dr["LocationName"] = complaint.LocationName;
                dr["FirstName"] = complaint.InitialAndLastName.Trim();
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /// <summary>
        /// Loads the data of the advocates
        /// </summary>
        private void LoadCombinedComplaints(int pageNo, Int64 employeeID, int maximumRows, Int64 complaintId, Int64 indexNo, ref Int64 rowsCount)
        {
            complaintList = new ComplaintList();
            complaintList.LoadCombinedComplaints(RequestSource.Advocate, pageNo, employeeID, maximumRows, complaintId, indexNo, ref rowsCount);
        }



        /// <summary>
        ///     For each GridView row is databound, the GridView's RowDataBound event is fired
        ///     </summary>
        ///     <param name="sender"></param>
        ///   <param name="e"></param>
        protected void gvAdvocate_DataBound(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// This is a Page Index Change event on a datagrid, that fires Occurs when one of the pager buttons is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvAdvocate_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        /// <summary>
        /// The RowCommand event is raised when a button is clicked in the GridView control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvAdvocate_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "Open")
            {

                complaintAdvocacy = new ComplaintAdvocacy
                {
                    AdvocacyCaseId = Convert.ToInt64(e.CommandArgument.ToString()),
                    ReceiveUser = Convert.ToInt16(Session["UserEmpID"]),
                    RecieveDate = DateTime.Now
                };

                if (complaintAdvocacy.OpenCase())
                {
                    PopulateGrid(gvAdvocate.PageIndex);
                    lbMessage.Text = String.Format("Complaint {0} Successfully Received.", e.CommandArgument.ToString());
                }
                else
                {
                    lbMessage.Text = String.Format("Failed to Receive Complaint {0}", e.CommandArgument.ToString());
                }
            }

        }


        /// <summary>
        /// This event is fired when a data row is bound to data in a GridView control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvAdvocate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.Header)
            //    {
            //       // e.Row.Cells[9].Visible = false;
            //        e.Row.Cells[2].Visible = false;
            //    }
            if (e.Row.Cells.Count > 2)
            {
                //string PIndexNo = e.Row.Cells[6].Text.ToString().Trim();
                if (e.Row.Cells[6].Text == "-1")
                {
                    e.Row.Cells[6].Text = "";
                }
            }
        }


        protected bool subCombineCheck(string str)
        {
            str = str.Trim();
            if (str.Length > 3)
                return true;
            else
                return false;
        }


        /// <summary>
        /// Occurs when the hyperlink to sort a column is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvAdvocate_Sorting(object sender, GridViewSortEventArgs e)
        {
            string sortExpression = e.SortExpression;

            if (GridViewSortDirection == SortDirection.Descending)
            {
                GridViewSortDirection = SortDirection.Ascending;
                SortGridView(sortExpression, ASCENDING);
            }
            else
            {
                GridViewSortDirection = SortDirection.Descending;
                SortGridView(sortExpression, DESCENDING);
            }

        }

        /// <summary>
        /// This method sorts the data from the dataview based on the search criteria
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="direction"></param>
        private void SortGridView(string sortExpression, string direction)
        {
            DataView dv = new DataView((DataTable)ViewState["gridData"])
            {
                Sort = sortExpression + direction
            };
            Session["DV"] = dv;
            gvAdvocate.DataSource = dv;
            gvAdvocate.DataBind();
        }

        protected void ddlAction_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlAction.SelectedItem.Value != "46" && ddlAction.SelectedItem.Value != "44" && ddlAction.SelectedItem.Value != "39" && ddlAction.SelectedItem.Value != "40" && ddlAction.SelectedItem.Value != "41")
            {
                rdCombPnlty.Enabled = false;
            }

            if ((ddlAction.SelectedItem.Value == "39") || (ddlTrailReturnStatus.SelectedItem.Value == "72"))
            {
                ddlCaseStatus.SelectedItem.Value = "130";
                ddlCaseStatus.SelectedItem.Text = "Send to BCAD";
                ddlCaseStatus.Enabled = false;
            }
            else
            {
                ddlCaseStatus.SelectedItem.Value = hdnValue.Value;
                ddlCaseStatus.SelectedItem.Text = hdnText.Value;
            }
        }

        protected void ddlTrailReturnStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((ddlTrailReturnStatus.SelectedItem.Value == "72") || (ddlAction.SelectedItem.Value == "39"))
            {
                ddlCaseStatus.SelectedItem.Value = "130";
                ddlCaseStatus.SelectedItem.Text = "Send to BCAD";
                ddlCaseStatus.Enabled = false;
            }
            else
            {
                ddlCaseStatus.SelectedItem.Value = hdnValue.Value;
                ddlCaseStatus.SelectedItem.Text = hdnText.Value;
            }
        }

    }



}