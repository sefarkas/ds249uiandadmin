﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
     Inherits="DSNY.DSNYCP.DS249.Advocate_DashBoard" CodeBehind="DashBoard.aspx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<%@ Register src="../UserControl/Pager.ascx" tagname="Pager" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 125px;
        }
        #Panel1 TABLE
        {
            border: #060F40 2px solid;
            color: #060F40;
            background: #ffffcc;
        }
        .pnReports
        {
            border: #060F40 2px solid;
            color: #060F40;
            background: #ffffcc;
        }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <asp:UpdatePanel ID="upButtons" runat="server">
            <ContentTemplate>
                <table width="955px" border="0" cellpadding="0px" cellspacing="0px" align="center">
                    <tr>
                        <td class="headertext" align="left">
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="82" />
                            ADVOCATE
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="25" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Image runat="server" SkinId="skidClear" width="15" />
                            <asp:ImageButton ID="btnRef" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                                runat="server" OnClick="btnRef_Click" CausesValidation="False" />
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="597" />
                            <asp:ImageButton ID="btnReport" ImageUrl="~/includes/img/ds249_btn_report_static.png"
                                runat="server" CausesValidation="False" OnClientClick="JavaScript:return false;" />
                            <cc1:PopupControlExtender ID="btnReport_PopupControlExtender" runat="server" 
                                Enabled="True" ExtenderControlID="" TargetControlID="btnReport" PopupControlID="pnReportsMenu"
                                Position="Bottom">
                            </cc1:PopupControlExtender>
                        </td>
                    </tr>
                </table>
                <asp:Image runat="server" SkinId="skidClear" height="10" width="25" />
                <table width="955px" border="0" cellpadding="0px" cellspacing="0px">
                    <tr>
                        <td width="33%">
                        </td>
                        <td class="searchCase" align="left">
                            SEARCH FOR COMPLAINTS&nbsp;&nbsp;
                        </td>
                        <td class="style1" align="left">
                            <asp:TextBox ID="txtSearch" runat="server" Width="172px" Height="17px" CssClass="inputFieldStyle"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnSearch" ImageUrl="~/includes/img/ds249_btn_search_static.png"
                                runat="server" OnClick="btnSearch_Click" />
                            <asp:ImageButton ID="btnSearchCancel" ImageUrl="~/includes/img/btn_cancel.gif"
                                runat="server" OnClick="btnSearchCancel_Click" CausesValidation="False" Visible="false" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table style="width: 832px;" border="0" cellpadding="0px" cellspacing="0px" 
            align="center">
            <tr>
                <td class="dashStatuBar" height="20" align="center">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label ID="lbMessage" CssClass="feedbackStatus" Width="50%" runat="server"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                        runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                        <Animations>                                
                        <OnUpdated>
                            <FadeOut Duration="5.0" Fps="24" />
                        </OnUpdated>
                        </Animations>
                    </cc1:UpdatePanelAnimationExtender>
                </td>
            </tr>
        </table>
        <table width="832" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td>
                    <asp:Image runat="server" SkinId="skidTopCap" /></td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upGrid" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="btnRef" EventName="Click" />
                        </Triggers>
                        <ContentTemplate>
                                                
                            <uc1:Pager ID="Pager1" runat="server" />                                           
                            <asp:GridView ID="gvAdvocate" runat="server" Width="832" AutoGenerateColumns="False"
                                BorderColor="#235705" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a"
                                AllowSorting="True" PageSize="50" 
                                AllowPaging="True" OnRowCommand="gvAdvocate_RowCommand" OnRowDataBound="gvAdvocate_RowDataBound"
                                OnSorting="gvAdvocate_Sorting"
                                OnDataBound="gvAdvocate_DataBound" AlternatingRowStyle-BackColor="#ededed" EmptyDataText="Sorry. No Complaints Found.">
                                <EmptyDataRowStyle CssClass="fromdate" />
                               
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnDataSource" ImageUrl="~/includes/img/btn_SourceIndicator.PNG"
                                                runat="server" Visible="true" CommandArgument='<%#Eval("DataSource") %>' />
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                         <div align="center">
                                            <asp:ImageButton ID="btnOpen" ImageUrl="~/includes/img/ds249_btn_dash_received_static.png" runat="server"
                                                CommandName="Open" Visible="false" CommandArgument='<%#Eval("CaseID") %>' OnClientClick="javascript:window.scrollTo(0,0); return confirm('Do you want to open the case?');" />
                                            <asp:ImageButton ID="btnEdit" ImageUrl="~/includes/img/ds249_btn_edit_static.png"
                                                runat="server" CommandName="Edit" Visible="false" CommandArgument='<%#Eval("ComplaintID") %>' />
                                            <asp:ImageButton ID="btnView" ImageUrl="~/includes/img/ds249_btn_view_static.png"
                                                runat="server" CommandName="View" Visible="false" CommandArgument='<%#Eval("ComplaintID") %>' />
                                            <asp:Label ID="lbAdvocate" CssClass="dashHeaderTxt" Visible="false" runat="server"
                                                Text="Restricted" Font-Names="Arial,Verdana,sans-serif" Font-Size="0.9em"></asp:Label>
                                        </div>
                                        </ItemTemplate>
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="70px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Data source" DataField="DataSource" SortExpression="DataSource">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="25px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>                   

                                    <asp:BoundField HeaderText="Date Routed" DataField="ComplaintStatusDate" SortExpression="ComplaintStatusDate"
                                                        DataFormatString="{0:MMM dd, yyyy}" HtmlEncode="false">
                                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                            Width="120px" />
                                                        <ItemStyle CssClass="dashLineStyle" />
                                     </asp:BoundField>

                                     <asp:BoundField HeaderText="Incident Date" DataField="IncidentDate" SortExpression="IncidentDate"
                                        DataFormatString="{0:MMM dd, yyyy}" HtmlEncode="false">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="120px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>                                                 


                                    <asp:BoundField HeaderText="Case ID" DataField="CaseID" SortExpression="CaseID">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="80px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>                                                           


                                    <asp:BoundField HeaderText="Index #" DataField="IndexNo" SortExpression="IndexNo">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="70px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="140px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="District" DataField="LocationName" SortExpression="LocationName">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="180px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Name (F. Last)" DataField="FirstName" SortExpression="FirstName">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="150px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>                                     

                                    <asp:BoundField HeaderText="Name (F. Last)" DataField="LastName" SortExpression="LastName">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="150px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="Charges" DataField="Charge" SortExpression="Charge">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="90px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>

                                    <asp:BoundField HeaderText="Reference #" DataField="RefNo" SortExpression="RefNo">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="90px" />
                                        <ItemStyle CssClass="dashRefLineStyle" />
                                    </asp:BoundField>

                                     <asp:BoundField HeaderText="Jt1" DataField="Jt1" SortExpression="Jt1">
                                        <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                            Width="70px" />
                                        <ItemStyle CssClass="dashLineStyle" />
                                    </asp:BoundField>

                                </Columns>
                                                               
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image runat="server" SkinId="skidBottomCap" />
                </td>
            </tr>
        </table>
    </center>
    <asp:Panel ID="pnReportsMenu" runat="server" CssClass="pnReports">
        <table>
            <tr>
                <td align="left">
                    <a href="../Reports/AdvocateReports.aspx?Id=1">Breakdown By Work Location</a>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <a href="../Reports/AdvocateReports.aspx?Id=2">Annual Disposition Report</a>
                    <asp:HiddenField ID="hdnImagePath" runat="server" />
                </td>
            </tr>

             <tr>
                <td align="left">
                    <a href="../Reports/AdvocateReports.aspx?Id=3">Complaints/Charges</a>
                    <asp:HiddenField ID="HiddenField2" runat="server" />
                </td>
            </tr>
            <tr>
        
                <td align="left">
                    <a href="../Reports/AdvocateReports.aspx?Id=4">Attorney Case Load</a>
                    <asp:HiddenField ID="HiddenField1" runat="server" />
                </td>
                </tr>
                <tr>
               
                  <td align="left">
                    <a href="../Reports/AdvocateReports.aspx?Id=5">Monthly Report</a>
                    <asp:HiddenField ID="HiddenField3" runat="server" />
                </td>
            </tr>
              <tr>
               
                  <td align="left">
                    <a href="../Reports/AdvocateReports.aspx?Id=6">Advocate DE Info</a>
                    <asp:HiddenField ID="HiddenField4" runat="server" />
                </td>
            </tr>
              <tr>
               
                  <td align="left">
                    <a href="../Reports/AdvocateReports.aspx?Id=7">Complaint History Info</a>
                    <asp:HiddenField ID="HiddenField5" runat="server" />
                </td>
                </tr>
                <tr>
                <td align="left">
                    <a href="../Reports/AdvocateReports.aspx?Id=8">Probation Employees</a>
                    <asp:HiddenField ID="HiddenField6" runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
