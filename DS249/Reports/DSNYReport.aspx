<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.DS249.Reports_DSNYReport" CodeBehind="DSNYReport.aspx.cs" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
    <style type="text/css">
        .pnReports
        {
            border: #060F40 2px solid;
            color: #060F40;
            background: #ffffcc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 900px; height: 30px;" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top">
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                at <a href="mailto:itservicedesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                    itServiceDesk@dsny.nyc.gov</a></span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="styleHeading">
                <div>
                    <span>DS-249 REPORTS </span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 900px; height: 50px" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td class="tdNewComplaint" align="center" width="50%">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                    OnClick="ImageButton1_Click" CausesValidation="False" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td class="searchCase" width="40%">
                <asp:ImageButton ID="ibReports" runat="server" ImageUrl="~/includes/img/ds249_btn_report_static.png"
                    OnClientClick="JavaScript:return false;" Height="32px" />
                <cc1:PopupControlExtender ID="ibReports_PopupControlExtender" runat="server" 
                    Enabled="True" ExtenderControlID="" TargetControlID="ibReports" PopupControlID="pnReportsMenu"
                    Position="Bottom">
                </cc1:PopupControlExtender>
            </td>
            <td width="10%">
            </td>
        </tr>
    </table>
    <table style="width: 900px;" border="0" cellpadding="0px" cellspacing="0px" align="center">
        <tr>
            <td colspan="2" align="center">
                <rsweb:ReportViewer ID="DS249ReportViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                    Height="519px" ProcessingMode="Remote" Width="920px" ShowPrintButton="true"> </rsweb:ReportViewer>                   
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel ID="pnReportsMenu" runat="server" CssClass="pnReports">
        <table>
            <tr>
                <td>
                    <a href="../Reports/DSNYReport.aspx?Id=1">Charges By Locations</a>
                </td>
            </tr>
<%--            <tr>
                <td>
                    <a href="../Reports/DSNYReport.aspx?Id=2">Distribution of Charges</a>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <a href="../Reports/DSNYReport.aspx?Id=3">Max # of Charges</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="../Reports/DSNYReport.aspx?Id=4">Complaints By Boros</a>
                </td>
            </tr>
            <tr>
                <td>
                    <a href="../Reports/DSNYReport.aspx?Id=5">Complaints_Report</a>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <a href="../Reports/DSNYReport.aspx?Id=6">Complaint_By_Title</a>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <a href="../Reports/DSNYReport.aspx?Id=7">Complaints_with_1.4_1.5_Charges</a>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <a href="../Reports/DSNYReport.aspx?Id=8">Employee_Privileges</a>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <a href="../Reports/DSNYReport.aspx?Id=9">30_60_90_Days_Report</a>
                </td>
            </tr>      
             <tr>
                <td align="left">
                    <a href="../Reports/DSNYReport.aspx?Id=10">Probation Employees</a>
                </td>
            </tr>          
        </table>
    </asp:Panel>
</asp:Content>
