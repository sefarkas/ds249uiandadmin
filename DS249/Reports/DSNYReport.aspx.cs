﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using DSNY.DSNYCP.ClassHierarchy;
using DSNY.DSNYCP.DS249.Reports;
using Microsoft.Reporting.WebForms;
namespace DSNY.DSNYCP.DS249
{
    public partial class Reports_DSNYReport : System.Web.UI.Page
    {
        User user;
        protected void Page_Load(object sender, EventArgs e)
        {
           user = (User)Session["clsUser"];
                if (user != null)
                {
                    SecurityUtility.AttachRolesToUser(user);
                    if (!User.IsInRole("Report"))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                }
                else
                    Response.Redirect("../UI/Login.aspx");                          

                if (Session["IsAuthenticated"] == null)
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }
                if (Session["IsAuthenticated"].ToString() == "false")
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }

                int reportNo = Convert.ToInt16(Request.QueryString["Id"]);
                Uri reportURL = new Uri(ConfigurationManager.AppSettings["ReportURL"]);
                DS249ReportViewer.ServerReport.ReportServerUrl = reportURL;
                if (!IsPostBack)
                {
                    DS249ReportViewer.ServerReport.ReportServerUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ReportURL"].ToString());
                    DS249ReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;                 
                    ReportServerCredentials rptCredentials = new ReportServerCredentials();
                    DS249ReportViewer.ServerReport.ReportServerCredentials = rptCredentials;
                    string ds249ReportPath = Convert.ToString(ConfigurationManager.AppSettings["DS249ReportPath"]);
                    switch (reportNo)
                    {
                        case 1:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "Complaints By Locations";
                            break;
                        case 2:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "Distribution of Charges across Complaints";
                            break;
                        case 3:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "People with max Number of Charges";
                            break;
                        case 4:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "Complaints_By_Boros";
                            break;
                        case 5:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "Complaints_Report";
                            break;
                        case 6:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "complaint_by_title";
                            break;
                        case 7:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "Complaints_with_1.4_1.5_Charges";
                            break;
                        case 8:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "Employee_Privileges";
                            break;
                        case 9:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "30_60_90_Days_Report";
                            break;
                        case 10:
                            DS249ReportViewer.ServerReport.ReportPath = ds249ReportPath + "Probation Employees";                           
                            DS249ReportViewer.ServerReport.SetParameters(new ReportParameter("RoutingLocation", RequestSource.DS249.ToString()));
                            DS249ReportViewer.ServerReport.Refresh(); 
                            break;  
                    }
                }           
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Complaints/Dashboard.aspx");
        }
    }
}