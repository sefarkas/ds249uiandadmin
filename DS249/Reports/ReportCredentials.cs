﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Security.Principal;
using Microsoft.Reporting.WebForms;

namespace DSNY.DSNYCP.DS249.Reports
{
    public class ReportServerCredentials : IReportServerCredentials
    {

        #region IReportServerCredentials Members



        public bool GetFormsCredentials(out System.Net.Cookie authCookie, out string userName, out string password, out string authority)
        {

            authCookie = null;

            userName = null;

            password = null;

            authority = null;

            //Not using form credentials 

            return false;

        }



        public WindowsIdentity ImpersonationUser
        {

            get { return null; }

        }



        public System.Net.ICredentials NetworkCredentials
        {

            get
            {

                string userName = ConfigurationManager.AppSettings["ReportServerUsername"];

                string password = ConfigurationManager.AppSettings["ReportServerPassword"];

                string domain = ConfigurationManager.AppSettings["ReportServerDomain"];

                return new System.Net.NetworkCredential(userName, password, domain);

            }

        }



        #endregion

    }


}