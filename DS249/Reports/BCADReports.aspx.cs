﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using System.Configuration;
using Microsoft.Reporting.WebForms;

namespace DSNY.DSNYCP.DS249.Reports
{
    public partial class BCADReports : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = (User)Session["clsUser"];
            if (user != null)
            {
                SecurityUtility.AttachRolesToUser(user);

                if (!User.IsInRole("Report"))
                    Response.Redirect("../UI/UnAuthorized.aspx");
            }
            else
                Response.Redirect("../UI/Login.aspx");

            if (Session["IsAuthenticated"] == null || Session["IsAuthenticated"].ToString() == "false")
            {
                Response.Redirect("../UI/login.aspx?Session=False");
            }

            if (!IsPostBack)
            {
                BCADReportViewer.ServerReport.ReportServerUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ReportURL"].ToString());
                BCADReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                ReportServerCredentials rptCredentials = new ReportServerCredentials();
                BCADReportViewer.ServerReport.ReportServerCredentials = rptCredentials;
                string bcadReportPath = Convert.ToString(ConfigurationManager.AppSettings["BCADReportPath"]);
                string ds249ReportPath = Convert.ToString(ConfigurationManager.AppSettings["DS249ReportPath"]);
                int reportNo = Convert.ToInt16(Request.QueryString["Id"]);
                switch (reportNo)
                {
                    case 1:
                        BCADReportViewer.ServerReport.ReportPath = bcadReportPath + "BCADOpenedComplaints";
                        break;
                    case 2:
                        BCADReportViewer.ServerReport.ReportPath = ds249ReportPath + "Probation Employees";
                        BCADReportViewer.ServerReport.SetParameters(new ReportParameter("RoutingLocation", RequestSource.BCAD.ToString()));
                        BCADReportViewer.ServerReport.Refresh(); 
                        break; 
                }
            }

        }
        protected void btnBCADDashboard_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../BCAD/Dashboard.aspx");
        }
    }
}