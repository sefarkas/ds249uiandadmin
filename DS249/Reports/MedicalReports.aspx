﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MedicalReports.aspx.cs" 
 MasterPageFile="~/MasterPage.master" Inherits="DSNY.DSNYCP.DS249.Reports.MedicalReports" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:content id="Content1" contentplaceholderid="head" runat="Server">
    <%--included for IE8-Date calender control--%>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
    <style type="text/css">
        .pnReports
        {
            border: #060F40 2px solid;
            color: #060F40;
            background: #ffffcc;
        }
    </style>
</asp:content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="Server">
    <table style="width: 900px; height: 30px;" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top">
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                    itServiceDesk@dsny.nyc.gov</a></span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="styleHeading">
                <div>
                    <span>MEDICAL REPORTS </span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 900px; height: 50px" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" width="100%">
        <tr>
            <td class="tdNewComplaint" align="left" width="50%">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:ImageButton ID="btnMedicalDashboard" runat="server" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                    OnClick="btnMedicalDashboard_Click" CausesValidation="False" />
                &nbsp;&nbsp;
            </td>
            <td class="searchCase" align="right" width="40%">
                <asp:ImageButton ID="ibReports" runat="server" ImageUrl="~/includes/img/ds249_btn_report_static.png"
                    OnClientClick="JavaScript:return false;" />
                <cc1:PopupControlExtender ID="ibReports_PopupControlExtender" runat="server" 
                    Enabled="True" ExtenderControlID="" TargetControlID="ibReports" PopupControlID="pnReportsMenu"
                    Position="Bottom">
                </cc1:PopupControlExtender>
            </td>
            <td width="10%">
            </td>
        </tr>
    </table>
    <table style="width: 900px;" border="0" cellpadding="0px" cellspacing="0px" align="center">
        <tr>
            <td colspan="2" align="center">
                <rsweb:ReportViewer ID="MedicalReportViewer" runat="server" Width="920px" Font-Names="Verdana"
                    Font-Size="8pt" Height="519px" ProcessingMode="Remote">
                   
                </rsweb:ReportViewer>
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel ID="pnReportsMenu" runat="server" CssClass="pnReports">
        <table>            
             <tr>
                <td align="left">
                    <a href="../Reports/MedicalReports.aspx?Id=1">Probation Employees</a>
                </td>
            </tr>   
           
        </table>
    </asp:Panel>
</asp:content>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE8">
    <style type="text/css">
        .pnReports
        {
            border: #060F40 2px solid;
            color: #060F40;
            background: #ffffcc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 900px; height: 30px;" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top">
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                    itServiceDesk@dsny.nyc.gov</a></span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="styleHeading">
                <div>
                    <span>MEDICAL REPORTS </span>
                </div>
            </td>
        </tr>
    </table>
    <table style="width: 900px; height: 50px" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td class="tdNewComplaint" align="center" width="50%">
                <asp:ImageButton ID="btnMedicalDashboard" runat="server" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                    OnClick="btnMedicalDashboard_Click" CausesValidation="False" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            </td>
            <td class="searchCase" width="40%">
                <asp:ImageButton ID="ibReports" runat="server" ImageUrl="~/includes/img/ds249_btn_report_static.png"
                    OnClientClick="JavaScript:return false;" />
                <cc1:PopupControlExtender ID="ibReports_PopupControlExtender" runat="server" 
                    Enabled="True" ExtenderControlID="" TargetControlID="ibReports" PopupControlID="pnReportsMenu"
                    Position="Bottom">
                </cc1:PopupControlExtender>
            </td>
            <td width="10%">
            </td>
        </tr>
    </table>
    <table style="width: 900px;" border="0" cellpadding="0px" cellspacing="0px" align="center">
        <tr>
            <td colspan="2" align="center">
                <rsweb:ReportViewer ID="MedicalReportViewer" runat="server" Font-Names="Verdana" Font-Size="8pt"
                    Height="519px" ProcessingMode="Remote" Width="920px" ShowPrintButton="true">  </rsweb:ReportViewer>                   
            </td>
        </tr>
    </table>
    <br />
    <asp:Panel ID="pnReportsMenu" runat="server" CssClass="pnReports">
        <table>
            <tr>
                <td>
                    <a href="../Reports/MedicalReports.aspx?Id=1">Probation Employees</a>
                </td>
            </tr>         
        </table>
    </asp:Panel>
</asp:Content>
--%>