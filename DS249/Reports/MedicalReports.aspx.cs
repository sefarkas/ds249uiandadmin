﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using System.Configuration;
using Microsoft.Reporting.WebForms;


namespace DSNY.DSNYCP.DS249.Reports
{
    public partial class MedicalReports : System.Web.UI.Page
    {
        User user;
        protected void Page_Load(object sender, EventArgs e)
        {
           user = (User)Session["clsUser"];
                if (user != null)
                {
                    SecurityUtility.AttachRolesToUser(user);
                    if (!User.IsInRole("Report"))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                }
                else
                    Response.Redirect("../UI/Login.aspx");                      

                if (Session["IsAuthenticated"] == null)
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }
                if (Session["IsAuthenticated"].ToString() == "false")
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }
                int reportNo = Convert.ToInt16(Request.QueryString["Id"]);
                Uri reportURL = new Uri(ConfigurationManager.AppSettings["ReportURL"]);
                MedicalReportViewer.ServerReport.ReportServerUrl = reportURL;
                if (!IsPostBack)
                {
                    MedicalReportViewer.ServerReport.ReportServerUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ReportURL"].ToString());
                    MedicalReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;                 
                    ReportServerCredentials rptCredentials = new ReportServerCredentials();
                    MedicalReportViewer.ServerReport.ReportServerCredentials = rptCredentials;
                    string medicalReportPath = Convert.ToString(ConfigurationManager.AppSettings["MedicalReportPath"]);
                    string ds249ReportPath = Convert.ToString(ConfigurationManager.AppSettings["DS249ReportPath"]);
                    switch (reportNo)
                    {
                        case 1:
                            MedicalReportViewer.ServerReport.ReportPath = ds249ReportPath + "Probation Employees";
                            MedicalReportViewer.ServerReport.SetParameters(new ReportParameter("RoutingLocation", RequestSource.Medical.ToString()));
                            MedicalReportViewer.ServerReport.Refresh(); 
                            break;                        
                    }
                }           
        }
        protected void btnMedicalDashboard_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Medical/Dashboard.aspx");
        }
    }
}