﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using DSNY.DSNYCP.ClassHierarchy;
using Microsoft.Reporting.WebForms;
using System.Security.Principal;
using System.Net;
using DSNY.DSNYCP.DS249.Reports;

namespace DSNY.DSNYCP.DS249
{
    public partial class Reports_AdvocateReports : System.Web.UI.Page
    {
        User user;
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            user = (User)Session["clsUser"];
            if (user != null)
            {
                SecurityUtility.AttachRolesToUser(user);

                if (!User.IsInRole("Report"))
                    Response.Redirect("../UI/UnAuthorized.aspx");
            }
            else
                Response.Redirect("../UI/Login.aspx");

            if (Session["IsAuthenticated"] == null)
            {
                Response.Redirect("../UI/login.aspx?Session=False");
            }
            if (Session["IsAuthenticated"].ToString() == "false")
            {
                Response.Redirect("../UI/login.aspx?Session=False");
            }

            int reportNo = Convert.ToInt16(Request.QueryString["Id"]);

            if (!IsPostBack)
            {
                AdvocateReportViewer.ServerReport.ReportServerUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["ReportURL"].ToString());
                AdvocateReportViewer.ProcessingMode = Microsoft.Reporting.WebForms.ProcessingMode.Remote;
                ReportServerCredentials rptCredentials = new ReportServerCredentials();
                AdvocateReportViewer.ServerReport.ReportServerCredentials = rptCredentials;
                string advocateReportPath = Convert.ToString(ConfigurationManager.AppSettings["AdvocateReportPath"]);
                string ds249ReportPath = Convert.ToString(ConfigurationManager.AppSettings["DS249ReportPath"]);
                switch (reportNo)
                {
                    case 1:
                        AdvocateReportViewer.ServerReport.ReportPath = advocateReportPath + "Breakdown By Work Location";
                        break;
                    case 2:
                        AdvocateReportViewer.ServerReport.ReportPath = advocateReportPath + "Annual Disposition Report";
                        break;
                    case 3:
                        AdvocateReportViewer.ServerReport.ReportPath = advocateReportPath + "ComplaintsCharges";
                        break;
                    case 4:
                        AdvocateReportViewer.ServerReport.ReportPath = advocateReportPath + "AttorneyComplaints_Boros";
                        break;
                    case 5:
                        AdvocateReportViewer.ServerReport.ReportPath = advocateReportPath + "MonthlyReport";
                        break;
                    case 6:
                        AdvocateReportViewer.ServerReport.ReportPath = advocateReportPath + "RptGetAdvocateDEInfo";
                        break;
                    case 7:
                        AdvocateReportViewer.ServerReport.ReportPath = advocateReportPath + "RptGetRoutingHistory";
                        break;
                    case 8:
                        AdvocateReportViewer.ServerReport.ReportPath = ds249ReportPath + "Probation Employees";
                        AdvocateReportViewer.ServerReport.SetParameters(new ReportParameter("RoutingLocation", RequestSource.Advocate.ToString()));
                        AdvocateReportViewer.ServerReport.Refresh(); 
                        break;
                }
            }

        }
        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Advocate/Dashboard.aspx");
        }
    }
}


