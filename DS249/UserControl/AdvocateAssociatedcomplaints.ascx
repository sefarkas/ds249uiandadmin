﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.UserControl_AdvocateAssociatedcomplaints"
    CodeBehind="AdvocateAssociatedcomplaints.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script language="javascript" type="text/javascript">

    function PerformPostBack(sender, e) {
        $get('ctl00$ContentPlaceHolder1$Button1').click();
    }
</script>
<table style="border-bottom-style: none; border-bottom-width: thin; width: 100%;">
    <tr>
        <td>
            <asp:TextBox ID="txtcomplaints" runat="server" Width="200px" OnTextChanged="txtcomplaints_TextChanged"
                Wrap="False" EnableViewState="False" CssClass="inputFieldStyle"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtcomplaints_TextBoxWatermarkExtender" runat="server"
                Enabled="True" TargetControlID="txtcomplaints" WatermarkText="Enter First 3 Numbers"
                WatermarkCssClass="WaterMark">
            </cc1:TextBoxWatermarkExtender>
            <asp:Image ID="imgIntelAssComp" ImageUrl="~/includes/img/ds249_i_mini_btn.png"
                runat="server" />
            <asp:ImageButton ID="btnRemove" ImageUrl="~/includes/img/ds249_mini_-_btn.png"
                Visible="false" runat="server" CausesValidation="false" OnClick="btnRemove_Click" />
            <cc1:AutoCompleteExtender ID="txtcomplaints_AutoCompleteExtender" runat="server"
                DelimiterCharacters="" CompletionInterval="1000" MinimumPrefixLength="3" CompletionSetCount="12"
                EnableCaching="true" Enabled="True" ServicePath="" UseContextKey="True" ServiceMethod="SearchComplaints"
                CompletionListCssClass="CompletionList" CompletionListHighlightedItemCssClass="HighlightedItem"
                CompletionListItemCssClass="ListItem" OnClientItemSelected="PerformPostBack"
                TargetControlID="txtcomplaints">
            </cc1:AutoCompleteExtender>
            <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Enter Only Numeric Value in Associated Complaints"
                Type="Integer" Operator="DataTypeCheck" ControlToValidate="txtcomplaints">!</asp:CompareValidator>
            <br />
            <asp:HiddenField ID="hdnAssCompUniqueID" runat="server" />
        </td>
    </tr>
</table>
