﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Text;




/// <summary>
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>

namespace DSNY.DSNYCP.DS249
{
    
    public partial class UserControl_VoilationControl : System.Web.UI.UserControl
    {
   
        protected System.Web.UI.WebControls.Label lblIncedentLocation;
        protected System.Web.UI.WebControls.Label lblChargeType;
       

        Complaint complaint;
        LocationList locationList;
        public event EventHandler RemoveUserControl;
        public event EventHandler CreateDateControl;
        public event EventHandler CreateNewControl;
        VoilationList voilationList;      
        bool docFound = false;
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                txtCharge.Enabled = value;
                txtEndDt.Enabled = value;
                txtSingleDt.Enabled = value;
                ddlChargeType.Enabled = value;
                ddlIncedentLocation.Enabled = value;
                txtStartDt.Enabled = value;
                btnRemove.Visible = value;
                ibAddDt.Visible = value;
                supnCal.Visible = false;
                frmCal.Visible = false;
                toCal.Visible = false;
            }

        }
        /// <summary>
        /// This is a boolean function that enables print button display as per the role membership
        /// </summary>
        public Boolean PartialEnable
        {
            set
            {
                txtCharge.Enabled = value;
                ddlChargeType.Enabled = value;
                ddlIncedentLocation.Enabled = value;

            }

        }
        /// <summary>
        /// This event is fired when the page is initiated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {            
                loadLocationList();
                loadIncidentLocation(); 
        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {            
                btnUpload.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_upload_sm_roll.png'");
                btnUpload.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_upload_sm.png'");
                string onclick = string.Empty;
                Button1.Style.Add("display", "none");
                if (Request.QueryString["Mode"] == "VIEW" || Request.QueryString["Mode"] == "APPROVE" || Request.QueryString["Mode"] == "CLOSE")
                    voilationList = (VoilationList)Session["voilationListView"];
                else
                    voilationList = (VoilationList)Session["voilationList"];
                DropdownSelect();

                if (Session["Mode"] != null && Session["Mode"].ToString() == "EDIT")
                    uploadButton();                     
                
                if (!Page.IsPostBack)
                {
                    complaint = new Complaint();                                      
                    List<UploadFile> upLoadDTO = new List<UploadFile>();                  
                }           

        }
        protected void Button1_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// This method will load the values in a drop down.
        /// </summary>
        private void DropdownSelect()
        {            
                if (voilationList.List != null)
                {
                    Voilation voilation;
                    voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == Convert.ToInt16(hdnUniqueID.Value); });
                    if (voilation != null)
                    {
                        if (voilation.IncidentLocationId != 0)
                            ddlIncedentLocation.Items.FindByValue(voilation.IncidentLocationId.ToString()).Selected = true;                            
                        lblIncedentLocation.Text = ddlIncedentLocation.SelectedItem.Text.ToString();
                        if (voilation.IncidentType.ToString() != String.Empty)
                            ddlChargeType.Items.FindByValue(voilation.IncidentType.ToString()).Selected = true;                        
                        lblChargeType.Text = ddlChargeType.SelectedItem.Text.ToString();
                    }
                }
        }
        /// <summary>
        /// This method loads the set of location list
        /// </summary>
        private void loadLocationList()
        {
            
                locationList = new LocationList();
                if (Session["LocationList"] == null)
                {
                    locationList.Load();
                    Session["LocationList"] = locationList.List;
                }
                else
                {
                    locationList.List = (List<LocationDS>)Session["LocationList"];
                }
           
        }
        /// <summary>
        /// This method loads the set of incedent ocation list
        /// </summary>
        private void loadIncidentLocation()
        {
            ListItem item;           
                foreach (LocationDS location in locationList.List)
                {
                item = new ListItem
                {
                    Value = location.LocationId.ToString(),
                    Text = location.LocationName.ToString()
                };
                ddlIncedentLocation.Items.Add(item);
                }
           
        }
        /// <summary>
        /// Raise this event so the parent page can handle it 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {           

                if (RemoveUserControl != null)
                {                    
                    RemoveUserControl(sender, e);                
                }           
        }
        /// <summary>
        /// This method updates violation complants to the list
        /// </summary>
        /// 
        private void UpdateVoilation()
        {

            Incedent incident;
            Charge charge = new Charge();           
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == Convert.ToInt64(hdnUniqueID.Value); });
                if (docFound == false)
                {                    
                    if (txtCharge.Text.Length < 10)
                        txtCharge.Text = "";
                    charge.ChargeCode = ExtractRefNo(txtCharge.Text);
                    charge.ChargeDescription = txtCharge.Text;
                }
                else
                {
                    charge.ChargeCode = hdnChargeRefNo.Value.ToString();
                    charge.ChargeDescription = hdnOldChargeDesc.Value;
                    txtCharge.Text = hdnOldChargeDesc.Value;
                    
                }
                voilation.Charge = charge;
                voilation.IncidentLocationId = Convert.ToInt64(ddlIncedentLocation.SelectedItem.Value);
                voilation.IncidentType = Convert.ToString(ddlChargeType.SelectedItem.Value);
                IncedentList lstIncident;
                lstIncident = new IncedentList();
                switch (voilation.IncidentType)
                {
                    case "S":
                        incident = new Incedent();
                        if (incident.ChargeId == -1)
                            incident.ChargeId = voilation.UniqueId;
                        if (txtSingleDt.Text != String.Empty)
                        {


                            incident.IncidentDate = Convert.ToDateTime(txtSingleDt.Text);
                            txtSingleDt.Text = String.Format("{0:MM/dd/yyyy}", incident.IncidentDate);

                        }
                        lstIncident.List.Add(incident);
                        voilation.Incidents = lstIncident;
                        break;
                    case "M":
                        break;
                    case "R":
                        incident = new Incedent();
                        if (incident.ChargeId == -1)
                            incident.ChargeId = voilation.UniqueId;
                        if (txtStartDt.Text != String.Empty)
                            incident.IncidentDate = Convert.ToDateTime(txtStartDt.Text);
                        else
                            incident.IncidentDate = DateTime.MinValue;
                        lstIncident.List.Add(incident);
                        incident = new Incedent();
                        if (incident.ChargeId == -1)
                            incident.ChargeId = voilation.UniqueId;
                        if (txtEndDt.Text != String.Empty)
                            incident.IncidentDate = Convert.ToDateTime(txtEndDt.Text);
                        else
                            incident.IncidentDate = DateTime.MinValue;
                        lstIncident.List.Add(incident);
                        voilation.Incidents = lstIncident;

                        break;
                    default:
                        break;
                }
                uploadButton();         

        }

        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, violation complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtCharge_TextChanged(object sender, EventArgs e)
        {
             string preValue = hdnChargeRefNo.Value;
                
                btnUpload.Enabled = true;
                String refNo = ExtractRefNo(txtCharge.Text);
                hdnNewChargeRefNo.Value = refNo;
                if (refNo != String.Empty)
                {
                    if (refNo != preValue)
                    {
                        List<UploadFile> upLoadDTO = new List<UploadFile>();
                        if (Session["upLoadDTO"] != null)
                            upLoadDTO = (List<UploadFile>)Session["upLoadDTO"];
                        {
                            if (upLoadDTO.Count != 0)
                            {
                                foreach (UploadFile upFile in upLoadDTO)
                                {
                                    if ((Session["upLoadDTO"]) != null)
                                    {


                                        for (int i = 0; i <= upLoadDTO.Count - 1; i++)
                                        {
                                            UploadFile upLoadValue = new UploadFile();                                            

                                            if (upLoadDTO[i].ChildID == preValue)
                                            {
                                                
                                                docFound = true;
                                                break;
                                            }

                                        }
                                        if (docFound == true)
                                        {
                                            hdnChargeRefNo.Value = preValue;
                                           
                                            UpdateVoilation();
                                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SomestartupScript", " alert('There are document(s) attached to the current charge,  Please delete these document(s) prior to changing the charges.');", true);
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                hdnChargeRefNo.Value = refNo;
                hdnOldChargeDesc.Value = txtCharge.Text;
                UpdateVoilation();              
        }

        
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, violation complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSingleDt_TextChanged(object sender, EventArgs e)
        {           
                UpdateVoilation();            
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, violation complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtStartDt_TextChanged(object sender, EventArgs e)
        {           
                UpdateVoilation();            
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, violation complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEndDt_TextChanged(object sender, EventArgs e)
        {            
                UpdateVoilation();           
        }

        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, violation complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlIncedentLocation_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                UpdateVoilation();
            
        }
        /// <summary>
        /// This is a button click event which is used to create the new violation list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibAddDt_Click(object sender, ImageClickEventArgs e)
        {
           
                if (CreateDateControl != null)
                {
                    CreateDateControl(sender, e);
                }
           
        }

        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, violation complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlChargeType_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                UpdateVoilation();

                switch (ddlChargeType.SelectedItem.Value)
                {
                    case "S":
                        phSingle.Visible = true;
                        phMultiple.Visible = false;
                        phRange.Visible = false;
                        break;
                    case "M":
                        phSingle.Visible = false;
                        phMultiple.Visible = true;
                        phRange.Visible = false;
                        if (CreateNewControl != null)
                        {
                            CreateNewControl(sender, e);                        
                            
                        }
                        break;
                    case "R":
                        phSingle.Visible = false;
                        phMultiple.Visible = false;
                        phRange.Visible = true;
                        break;
                    default:
                        phSingle.Visible = false;
                        phMultiple.Visible = false;
                        phRange.Visible = false;
                        break;
                }
            
        }
        /// <summary>
        /// This method returns string, based on the search criteria
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private String ExtractRefNo(String text)
        {
           
                if (text != String.Empty)
                {
                    return text.Substring(0, text.IndexOf(" ")).Trim();
                }
                else
                {
                    return string.Empty;
                }           

        }        

        protected void uploadButton()
        {
            Complaint complaint = new Complaint();
            complaint = (Complaint)Session["Complaint"];
            Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == Convert.ToInt64(hdnUniqueID.Value); });
            string x = string.Empty;
            Incedent incident;
            incident = new Incedent();
            string p1=hdnChargeRefNo.Value.ToString();
            string p2 = Session["Mode"].ToString();
             Int64 p3=0;
            Int16 p4=0;
            if (complaint != null)
            {
               p3 = complaint.ComplaintId;
                p4 = voilation.UniqueId;
            }           

            btnUpload.Attributes.Add("Onclick", "javascript:return OpenComfirmation('" + p1 + "','" + p2 + "','" + p3 + "','" + p4 + "');");
        }

        protected void btnUpload_Click(object sender, ImageClickEventArgs e)
        {
                
            uploadButton();
            uploadButton();
        }

       


    }
}