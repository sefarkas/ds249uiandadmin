﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.UserControl_AppealsControl"
    CodeBehind="AppealsControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style7
    {
        width: 242px;
        height: 27px;
    }
    .style8
    {
        height: 27px;
    }
    .style9
    {
        width: 242px;
        height: 15px;
    }
    .style10
    {
        height: 15px;
    }
    .style11
    {
        width: 112px;
    }
</style>
<table width="100%" style="border-bottom-style: none; border-bottom-width: thin;
    height: 56px;">
    <tr>
        <td class="style7">
            DATE<br />
            (Month/Date/Year:xx/xx/xxxx)<br />
            <asp:TextBox ID="txtAppealDate" runat="server" AutoPostBack="true" CausesValidation="true"
                CssClass="inputField1Style" ontextchanged="txtAppealDate_TextChanged"></asp:TextBox>
            <cc1:CalendarExtender ID="txtAppealDate_CalendarExtender" runat="server" Enabled="True"
                TargetControlID="txtAppealDate" PopupButtonID="supnCal" CssClass="MyCalendar">
            </cc1:CalendarExtender>
            <asp:Image id="supnCal" runat="server" SkinId="skidMiniCalButton" /><br />
            <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Incident Date Entered Is Incorrect"
                Type="Date" Operator="DataTypeCheck" ControlToValidate="txtAppealDate"> !</asp:CompareValidator>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAppealDate"
                ValidationGroup="Approved" ErrorMessage="Please Enter Date">!</asp:RequiredFieldValidator>
        </td>
        <td valign="top" class="style11">
            APPEAL
            <br />
            <br />
            <asp:DropDownList ID="ddlAppeal" runat="server" OnSelectedIndexChanged="ddlAppeal_SelectedIndexChanged"
                AutoPostBack="True" CssClass="inputField1Style">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td valign="top" class="style8">
            RESULT<br />
            <br />
            <asp:DropDownList ID="ddlResult" runat="server" OnSelectedIndexChanged="ddlResult_SelectedIndexChanged"
                AutoPostBack="True" CssClass="inputField1Style">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2" class="style9">
            <asp:HiddenField ID="hdnAppealUniqueID" runat="server" />
            <asp:TextBox ID="txtAppealcommnts" runat="server" Width="309px" OnTextChanged="txtAppeals_TextChanged"
                Wrap="False" EnableViewState="False" CssClass="inputFieldStyle" AutoPostBack="True"></asp:TextBox>
        </td>
        <td class="style10">
            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/includes/img/ds249_mini_-_btn.png"
                OnClick="btnRemove_Click" Visible="false" CausesValidation="False" />
        </td>
    </tr>
</table>
