﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.UserControl_WitnessControl"
    CodeBehind="WitnessControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
<link href="../includes/css/DashboardStyle.css" rel="stylesheet" type="text/css" />--%>
<style type="text/css">
    .borderless
    {
        border: none;
        background-color: white;
    }
    .label
    {
        font-family: Arial;
        font-size: 1.3em;
        margin: 0px 20px 0px 3px;
        _margin-left: 5px;
        font-weight: normal;
    }
</style>
<script language="javascript" type="text/javascript">

    function PerformPostBack(sender, e) {        

        $get('ctl00$ContentPlaceHolder1$Button1').click();
    }
</script>
<table>
    <tr>
        <td class="borderless">
            <asp:HiddenField ID="hdnUniqueID" runat="server" />
            <asp:HiddenField ID="hdnPersonnelID" runat="server" />
            <asp:HiddenField ID="hdnRefNo" runat="server" />
            <asp:TextBox ID="txtWitness" runat="server" Width="220px" OnTextChanged="txtWitness_TextChanged"
                Wrap="False" EnableViewState="False"></asp:TextBox>
            <asp:Label ID="lblWitness" runat="server" class="labelStyle1" Visible="false"></asp:Label>
            <cc1:TextBoxWatermarkExtender ID="txtWitness_TextBoxWatermarkExtender" runat="server"
                Enabled="True" TargetControlID="txtWitness" WatermarkCssClass="WaterMark" WatermarkText="Enter First 2 Characters of the Name">
            </cc1:TextBoxWatermarkExtender>
            <cc1:AutoCompleteExtender ID="txtWitness_AutoCompleteExtender" runat="server" CompletionInterval="1000"
                MinimumPrefixLength="2" CompletionSetCount="12" EnableCaching="true" Enabled="True"
                ServicePath="" UseContextKey="True" ServiceMethod="SearchEmployee" TargetControlID="txtWitness"
                CompletionListCssClass="CompletionList" CompletionListHighlightedItemCssClass="HighlightedItem"
                CompletionListItemCssClass="ListItem" OnClientItemSelected="PerformPostBack">
            </cc1:AutoCompleteExtender>
            <asp:Image ID="imgIntelAssComp" ImageUrl="~/includes/img/ds249_i_mini_btn.png"
                runat="server" />
            <asp:ImageButton ID="btnRemove" ImageUrl="~/includes/img/ds249_mini_-_btn.png"
                Visible="false" runat="server" CausesValidation="false" OnClick="btnRemove_Click" /> 
                     <asp:Button ID="Button1" runat="server" onclick="Button1_Click" Text="Button" 
                                                        CausesValidation="False" />         
        </td>
    </tr>
</table>
