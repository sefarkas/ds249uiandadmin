﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.UserControl_PenaltyControl"
    CodeBehind="PenaltyControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    #tblPltyHours
    {
        width: 68px;
    }
    .style15
    {
        width: 190px;
    }
    .style16
    {
        width: 60px;
    }
    .style18
    {
        width: 94px;
    }
</style>
<table>
    <tr>
        <td class="style15">
            &nbsp;<asp:Label ID="lblDisposition" runat="server" Text="DISPOSITION TYPE"></asp:Label>
        </td>
        <td class="style16" nowrap>
            <asp:Label ID="lblPltyValue" runat="server" Text="VALUE" Visible="False"></asp:Label>
            <asp:Label ID="lblTerm" runat="server" Text="TIME" Visible="False"></asp:Label>
        </td>
        <td>
            <asp:Label ID="lblTerm_Desc" runat="server" Text="TERM" Visible="False"></asp:Label>
        </td>
        <td>
            <asp:Label ID="lblPltyAdd" runat="server" Text="ADD?" Visible="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style15">
            <asp:DropDownList ID="ddlPltyType" runat="server" Font-Size="0.9em" Font-Names="Arial"
                Width="215px" OnSelectedIndexChanged="ddlPltyType_SelectedIndexChanged" AutoPostBack="True">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="style16" nowrap>
            <asp:TextBox ID="txtPltyValue" runat="server" Font-Size="0.9em" Font-Names="Arial"
                Width="40px" AutoPostBack="True" OnTextChanged="txtPltyValue_TextChanged" Visible="False"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RqPtlyValue" runat="server" ControlToValidate="txtPltyValue"
                ErrorMessage="Value Required" Text="*" Visible="False"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RgExpValdPtlyValue" runat="server" ControlToValidate="txtPltyValue"
                ValidationExpression="^\d{1,4}(\.\d{1,2})?$" ErrorMessage="Value Should Be Of Format 9999.99"
                Text="*" Width="10px " Visible="False"></asp:RegularExpressionValidator>
            <asp:TextBox ID="txtTerm" runat="server" Font-Size="0.9em" Font-Names="Arial" Width="40px"
                AutoPostBack="True" OnTextChanged="txtTerm_TextChanged" Visible="False"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RqValdTerm" runat="server" ControlToValidate="txtTerm"
                ErrorMessage="Time Required" Text="*" Visible="False"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RgExpValdTerm" runat="server" ControlToValidate="txtTerm"
                ErrorMessage="Enter Two Digit Number For Time" MaximumValue="99" MinimumValue="1"
                Text="*" Type="Integer" Visible="False"></asp:RangeValidator>
        </td>
        <td>
            <asp:DropDownList ID="ddlPtlyTerm_Desc" runat="server" Width="70" Font-Size="0.9em"
                Font-Names="Arial" AutoPostBack="true" OnSelectedIndexChanged="ddlPtlyTerm_Desc_SelectedIndexChanged"
                Visible="False">
                <asp:ListItem Value="HOURS">HOURS</asp:ListItem>
                <asp:ListItem Value="DAYS">DAYS</asp:ListItem>
                <asp:ListItem Value="WEEKS">WEEKS</asp:ListItem>
                <asp:ListItem Value="MONTHS">MONTHS</asp:ListItem>
                <asp:ListItem Value="YEARS">YEARS</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:CheckBox ID="chkboxAdd" runat="server" Width="20px" Font-Size="Small" Height="37px"
                AutoPostBack="True" OnCheckedChanged="chkboxAdd_CheckedChanged" Visible="True" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:TextBox ID="txtpltycomments" runat="server" Width="210px" Wrap="False" Visible="false"
                CssClass="inputFieldStyle" AutoPostBack="True" OnTextChanged="txtpltycomments_TextChanged"
                MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RqFldComments" runat="server" ControlToValidate="txtpltycomments"
                ErrorMessage="Comments Required" Text="*" Visible="False"></asp:RequiredFieldValidator>
            <asp:HiddenField ID="hdnPenaltyUniqueID" runat="server" />
        </td>
        <td class="style18">
        </td>
        <td>
            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/includes/img/ds249_mini_-_btn.png"
                OnClick="btnRemove_Click" Visible="False" CausesValidation="False" />
        </td>
    </tr>
</table>
