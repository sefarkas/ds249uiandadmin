﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using DSNY.DSNYCP.DTO;
/// <summary>
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>
namespace DSNY.DSNYCP.DS249
{
  
    public partial class UserControl_HearingsControl : System.Web.UI.UserControl
    {
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, DS249 complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public event EventHandler RemoveUserControl;       
        CodeList codeList;
        AdvocateHearingList hearingList;
        AdvocateHearing hearing;
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                txtHearingDt.Enabled = value;
                supnCal.Visible = false;
                ddlcalstatus.Enabled = value;
                ddlhringtype.Enabled = value;
                ddlhringstatus.Enabled = value;
                btnRemove.Visible = value;
                txtHearingComments.Enabled = value;
            }

        }
       
        /// <summary>
        /// This event is fired when the page is initiated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {           
                LoadhrngCalstatus();
                LoadhrngType();
                Loadhrngstatus();     
        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            hearingList = (AdvocateHearingList)Session["AdvocateHearingList"];
            DropdownSelect();
        }

        #region Private Methods
        /// <summary>
        /// This method loads the hiring status as per the status in the code list.
        /// </summary>
        private void LoadhrngCalstatus()
        {
              codeList = new CodeList();

                if (Session["Calstatus"] == null)
                {
                    codeList.Load(6);
                    Session["Calstatus"] = codeList.CList;
                }
                else
                {
                    codeList.CList = (List<Code>)Session["Calstatus"];
                }

                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlcalstatus.Items.Add(item);

                }           
        }
        /// <summary>
        /// This method loads the hiring type as per the status in the code list.
        /// </summary>
        private void LoadhrngType()
        {
             codeList = new CodeList();

                if (Session["Type"] == null)
                {
                    codeList.Load(10);
                    Session["Type"] = codeList.CList;
                }
                else
                {
                    codeList.CList = (List<Code>)Session["Type"];
                }

                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlhringtype.Items.Add(item);

                }
           
        }
        /// <summary>
        /// This method loads the hiring status as per the status in the code list.
        /// </summary>
        private void Loadhrngstatus()
        {
              codeList = new CodeList();
                if (Session["Status"] == null)
                {
                    codeList.Load(7);
                    Session["Status"] = codeList.CList;
                }
                else
                {
                    codeList.CList = (List<Code>)Session["Status"];
                }
                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlhringstatus.Items.Add(item);
                }            
        }
        /// <summary>
        /// This method will load the values in a drop down.
        /// </summary>
        private void DropdownSelect()
        {
          
                if (hearingList != null)
                {
                    if (hearingList.List != null)
                    {
                        hearing = hearingList.List.Find(delegate(AdvocateHearing p) { return p.UniqueId == Convert.ToInt16(hdnHearingUniqueID.Value); });
                        if (hearing != null)
                        {
                            if (hearing.CalStatusCD != -1)
                             if (ddlcalstatus.Items.IndexOf(ddlcalstatus.Items.FindByValue(hearing.CalStatusCD.ToString())) != -1) 
                            {
                                ddlcalstatus.Items.FindByValue(hearing.CalStatusCD.ToString()).Selected = true;
                            }
                            if (hearing.HearingStatusCD != -1)
                             if (ddlhringstatus.Items.IndexOf(ddlhringstatus.Items.FindByValue(hearing.HearingStatusCD.ToString())) != -1) 
                            {
                                ddlhringstatus.Items.FindByValue(hearing.HearingStatusCD.ToString()).Selected = true;
                            }
                            if (hearing.HearingTypeCD != -1)
                                if (ddlhringtype.Items.IndexOf(ddlhringtype.Items.FindByValue(hearing.HearingTypeCD.ToString())) != -1) 
                            {
                                ddlhringtype.Items.FindByValue(hearing.HearingTypeCD.ToString()).Selected = true;
                            }
                            //Hearing Type="CANCEL" 

                            if (hearing.HearingTypeCD == (long)DS249Enums.HearingType.CANCEL)
                            {
                                ddlhringstatus.Enabled = false;
                                ddlhringstatus.ClearSelection();
                            }

                            else
                            {
                                ddlhringstatus.Enabled = true;
                                ddlhringstatus.SelectedItem.Value = hearing.HearingStatusCD.ToString();
                            }
                        }
                    }
                }           
        }

        /// <summary>
        /// This method loads updated hearing complants to the form
        /// </summary>
        private void UpdateHearing()
        {                        
                hearing = hearingList.List.Find(delegate(AdvocateHearing p) { return p.UniqueId == Convert.ToInt16(hdnHearingUniqueID.Value); });
                hearing.CalStatusCD = Convert.ToInt64(ddlcalstatus.SelectedItem.Value);
                hearing.HearingStatusCD = Convert.ToInt64(ddlhringstatus.SelectedItem.Value);
                hearing.HearingTypeCD = Convert.ToInt64(ddlhringtype.SelectedItem.Value);
                //saving heearing date
                //Code reviewed by Gill and Bhalla

                if (txtHearingDt.Text != String.Empty)
                    hearing.HearingDate = Convert.ToDateTime(txtHearingDt.Text);
                else
                    hearing.HearingDate = null;             
           
        }

        #endregion

        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {            
                if (RemoveUserControl != null)
                {
                    RemoveUserControl(sender, e);
                }           
        }
        /// <summary>
        /// This is a text change event that gets updated hearing complaints to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlcalstatus_SelectedIndexChanged(object sender, EventArgs e)
        {           
                UpdateHearing();            
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, hearing complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlhringtype_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                UpdateHearing();
                // Included hearing type "CANCEL" 
                //If hearing type "CANCEL" is selected then the user is not allowed to select hearing status 
                long hearingCode = (long)DS249Enums.HearingType.CANCEL; 

                if (ddlhringtype.SelectedItem.Value==hearingCode.ToString())                    
                {
                    ddlhringstatus.Enabled = false;
                    ddlhringstatus.ClearSelection();
                }
                else
                {
                    ddlhringstatus.Enabled = true;                    
                }           
           
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, hearing complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlhringstatus_SelectedIndexChanged(object sender, EventArgs e)
        {            
                UpdateHearing();           
            
        }

        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, hearing complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void hearingcomments_TextChanged(object sender, EventArgs e)
        {           
                UpdateHearing();         
            
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, hearing complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSingleDt_TextChanged(object sender, EventArgs e)
        {
             UpdateHearing();        
           
        }
    }
}
