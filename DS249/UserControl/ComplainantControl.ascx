﻿<%@ Control Language="C#" AutoEventWireup="true"   Inherits="DSNY.DSNYCP.DS249.UserControl_ComplainantControl" Codebehind="ComplainantControl.ascx.cs" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<%--<link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
 <link href="../includes/css/DashboardStyle.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .style8
        {
            width: 34%;
        }
        .style9
        {
            width: 240px;
        }
        .label { font-family:Arial; font-size:1em; margin:0px 20px 0px 2px; _margin-left:5px; font-weight:normal; }
        .style10
        {
            font-family: Arial;
            font-size: 1em;
            margin: 0px 20px 0px 2px;
            _margin-left: 5px;
            font-weight: normal;
            width: 240px;
        }
    </style>

    <script lang="javascript" type="text/javascript">

        function PerformPostBack(sender, e) {
            //__doPostBack('ctl00_ctl00_MasterContentPlaceHolder_CustomerContentPlaceHolder_THangarFormView_HangarNumberTextBoxAdd', 'TextChanged');

            $get('ctl00$ContentPlaceHolder1$Button1').click();
        }
        
        function DisableLink() {
            alert("Hello");
        }
    </script>
<table width="100%">
    <tr class="docLineStyle">
    <td width="30%">
        <asp:Label ID="lblVwName" runat="server" Visible="false" Text="COMPLAINANT NAME" class="lable1" Font-Bold="True"></asp:Label></td>
    <td width="30%">
        <asp:Label ID="lblVwLocation" Text="LOCATION" runat="server" Visible="False" class="lable1" Font-Bold="True"></asp:Label>
    </td>
    <td width="40%">
        <asp:Label ID="lblVwTitle" runat="server" Text="TITLE" Visible="False" class="lable1" Font-Bold="True"></asp:Label>
    </td>
    </tr>
    <tr class="docLineStyle">
    <td >
    <asp:Label ID="lblComplainantName" runat="server" class="labelStyle" Visible="False" ></asp:Label>
    </td>
    <td >
    <asp:Label ID="lblComplainantLoc" runat="server" class="labelStyle" Visible="False" 
            ></asp:Label>
    </td>
    <td>
    <asp:Label ID="lblComplainantTitle" runat="server" class="labelStyle" Visible="False"></asp:Label>
    </td>
    </tr>
    </table>
    <table  width="100%" style="border-bottom-style: solid; border-bottom-width: thin;"><tr class="docLineStyle">
    <td >
            <asp:Label ID="lblVwStatement" runat="server" Text="STATEMENT" 
            Visible="False" class="lable1" Font-Bold="True"></asp:Label>
    </td>
    </tr>
    
     <tr class="docLineStyle">
    <td width="100%">
        <asp:Label ID="lblComplainantStmt" runat="server" class="labelStyle" 
            Visible="False" Width="810px" ></asp:Label>     
    </td>
    </tr>
   </table>
   <table  width="100%" style="border-bottom-style: solid; border-bottom-width: thin;">
    <tr>
        <td>
            <asp:Label ID="lblName" runat="server" Text="COMPLAINANT NAME" Font-Bold="True"></asp:Label>
        </td>
        <td class="style9">
            <asp:Label ID="lblStatement" runat="server" Text="STATEMENT" Font-Bold="True"></asp:Label>
        </td>
        <td style="width: 65%;" class="labelBoldStyle" align="right">
            <asp:ImageButton ID="btnRemoveComplainant" ImageUrl="~/includes/img/ds249_mini_-_btn.png"
                Visible="false" runat="server" CausesValidation="False" OnClick="btnRemoveComplainant_Click"
                Style="height: 20px" />
        </td>
    </tr>
    <tr>
        <td class="style8">
            <asp:HiddenField ID="hdnComplainantUniqueID" runat="server" />
            <asp:HiddenField ID="hdnComplainantRefNo" runat="server" />
            <asp:HiddenField ID="hdnComplainantTitleID" runat="server" />
            <asp:TextBox ID="txtComplainantName" runat="server" Height="17px" Width="220px" OnTextChanged="txtComplainantName_TextChanged"
                CssClass="inputFieldStyle"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtComplainantName_TextBoxWatermarkExtender" runat="server"
                Enabled="True" TargetControlID="txtComplainantName" WatermarkCssClass="WaterMark"
                WatermarkText="Enter First 2 Characters of the Name">
            </cc1:TextBoxWatermarkExtender>
            <cc1:AutoCompleteExtender ID="txtComplainantName_AutoCompleteExtender" runat="server"
                CompletionInterval="1000" MinimumPrefixLength="2" CompletionSetCount="12" EnableCaching="true"
                Enabled="True" ServicePath="" UseContextKey="True" ServiceMethod="SearchEmployee"
                OnClientItemSelected="PerformPostBack" TargetControlID="txtComplainantName" CompletionListCssClass="CompletionList"
                CompletionListHighlightedItemCssClass="HighlightedItem" CompletionListItemCssClass="ListItem">
            </cc1:AutoCompleteExtender>
            <asp:Image ID="imgIntelAssComp" ImageUrl="~/includes/img/ds249_i_mini_btn.png" runat="server" /> 
        </td>
        <td rowspan="5" colspan="2" >
            <asp:Image runat="server" ID="ibTemplate" alt="Template" ImageUrl="~/includes/img/ds249_btn_template_static.png" />
            <br />
            <asp:TextBox ID="txtComplainantStmt" runat="server" Height="138px" TextMode="MultiLine"
                Width="480px" OnTextChanged="txtComplainantStmt_TextChanged" AutoPostBack="True"
                CssClass="inputFieldStyle"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td class="style8">
            <asp:Label ID="lblLocation" runat="server" Text="LOCATION" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style8">
            <asp:DropDownList ID="ddlComplainantLoc" runat="server" Width="300px" AppendDataBoundItems="true"
                AutoPostBack="True" OnSelectedIndexChanged="ddlComplainantLoc_SelectedIndexChanged"
                CssClass="inputFieldStyle" Height="25px">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td class="style8">
            <asp:Label ID="lblTitle" runat="server" Text="TITLE" Font-Bold="True"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="style8">
            <asp:TextBox ID="txtComplaintantTitle" runat="server" Height="18px" Width="220px"
                OnTextChanged="txtComplaintantTitle_TextChanged" CssClass="inputFieldStyle"></asp:TextBox>
            <cc1:TextBoxWatermarkExtender ID="txtComplaintantTitle_TextBoxWatermarkExtender" WatermarkCssClass="WaterMark" WatermarkText="Enter First 2 Characters of the Title"
                runat="server" Enabled="True" TargetControlID="txtComplaintantTitle">
            </cc1:TextBoxWatermarkExtender>
            <cc1:AutoCompleteExtender ID="txtComplaintantTitle_AutoCompleteExtender" runat="server"
                CompletionInterval="1000" MinimumPrefixLength="2" CompletionSetCount="12" EnableCaching="true"
                Enabled="True" ServicePath="" ServiceMethod="GetTitles" OnClientItemSelected="PerformPostBack"
                TargetControlID="txtComplaintantTitle" CompletionListCssClass="CompletionList"
                CompletionListHighlightedItemCssClass="HighlightedItem" CompletionListItemCssClass="ListItem">
            </cc1:AutoCompleteExtender>
            <asp:Image ID="imgIntelAssComp0" ImageUrl="~/includes/img/ds249_i_mini_btn.png" runat="server" />
             <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" CausesValidation="False" />
            <br />
            <asp:DropDownList ID="ddlComplainantTitle" runat="server" Height="22px" Width="262px"
                AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="ddlComplainantTitle_SelectedIndexChanged"
                CssClass="inputFieldStyle" Visible="False">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
            <br />
        </td>
    </tr>
</table>
 