﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using System.Text;
using System.IO;
using System.Collections;
/// <summary>
/// 
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UserControl_PenaltyControl : System.Web.UI.UserControl
    {
        public event EventHandler RemoveUserControl;
        public event EventHandler CalculateTotal;
        AdvocatePenaltyList advocatePenaltyList;
        AdvocatePenalty advocatePenalty;        
        CodeList codeList;    
        Code Code;
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                ddlPltyType.Enabled = value;
                txtPltyValue.Enabled = value;             
                txtTerm.Enabled = value;
                ddlPtlyTerm_Desc.Enabled = value;  
                txtpltycomments.Enabled = value;
                btnRemove.Visible = value;
            }

        }
        /// <summary>
        /// This event is fired when the page is initiated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            
                LoadAdvocatePnltyType();
                if (Session["AdvocatePenalty"] != null)
                {
                    advocatePenalty = (AdvocatePenalty)Session["AdvocatePenalty"];
                    ChangeControlVisibility(advocatePenalty.Value_Data_Type);
                }               
            
        }

        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            advocatePenaltyList = (AdvocatePenaltyList)Session["advocatePenaltyList"];
            SelectDropdown();
            if (!IsPostBack)
            {
                if (Session["AdvocatePenalty"] != null)
                {
                    Session["AdvocatePenalty"] = null;
                }
            }

        }
        /// <summary>
        /// This method loads updated hearing complants to the form
        /// </summary>
        private void UpdatePenalty()
        {       

                Page.Validate();
                advocatePenalty = advocatePenaltyList.List.Find(delegate(AdvocatePenalty p) { return p.UniqueId == Convert.ToInt16(hdnPenaltyUniqueID.Value); });
                 advocatePenalty.ApplyToTotal = Convert.ToBoolean(chkboxAdd.Checked);   

                advocatePenalty.PenaltyTypeCD = Convert.ToInt64(ddlPltyType.SelectedItem.Value);              
                if (ddlPltyType.SelectedItem.Value == "76" ||
                    ddlPltyType.SelectedItem.Value == "77" ||
                   ddlPltyType.SelectedItem.Value == "171" ||
                   //Added to resolve TFS #6427
                   ddlPltyType.SelectedItem.Value == "86" ||
                   ddlPltyType.SelectedItem.Value == "174")
                    advocatePenalty.Value_Data_Type = "T";               

                else if (ddlPltyType.SelectedItem.Value == "78" && ddlPltyType.SelectedItem.Text.ToUpper() == "FINED")
                    advocatePenalty.Value_Data_Type = "T";
                else if (ddlPltyType.SelectedItem.Value != "78" && ddlPltyType.SelectedItem.Text.ToUpper() == "FINED")
                    advocatePenalty.Value_Data_Type = "M";            
                else if( ddlPltyType.SelectedItem.Value == "81")
                    advocatePenalty.Value_Data_Type = "F";
                else
                    advocatePenalty.Value_Data_Type = "N";
                advocatePenalty.DataValue = txtPltyValue.Text;
                advocatePenalty.PenaltyComments = Convert.ToString(txtpltycomments.Text);
                if(txtTerm.Text.Trim()!="")                
                {
                    advocatePenalty.Term = Convert.ToInt16(txtTerm.Text);
                }

                advocatePenalty.Term_Desc = ddlPtlyTerm_Desc.SelectedValue;     
        }
        /// <summary>
        /// This method finds the value from a penalty type dropdown list
        /// </summary>
        private void SelectDropdown()
        {
               if (advocatePenaltyList != null)
                {
                    if (advocatePenaltyList.List != null)
                    {
                        advocatePenalty = advocatePenaltyList.List.Find(delegate(AdvocatePenalty a) { return a.UniqueId == Convert.ToInt16(hdnPenaltyUniqueID.Value); });
                        if (advocatePenalty != null)
                        {
                            if (advocatePenalty.PenaltyTypeCD != -1)
                            {
                                if (ddlPltyType.Items.IndexOf(ddlPltyType.Items.FindByValue(advocatePenalty.PenaltyTypeCD.ToString())) != -1)
                                    ddlPltyType.Items.FindByValue(advocatePenalty.PenaltyTypeCD.ToString()).Selected = true;                                
                                ChangeControlVisibilityAndValues(advocatePenalty,advocatePenalty.Value_Data_Type);                            
                            }
                        }
                    }
                }          
        }

        public void ChangeControlVisibilityAndValues(AdvocatePenalty advocateValue,string PenaltyType)
        {
           
                switch (PenaltyType)
                {
                    case "N":
                        txtpltycomments.Visible = false;
                        RqFldComments.Visible = false;
                        lblTerm.Visible = false;
                        txtTerm.Visible = false;
                         RgExpValdTerm.Visible = false;                       
                        RqValdTerm.Visible = false;
                        lblTerm_Desc.Visible = false;
                        ddlPtlyTerm_Desc.Visible = false;
                        txtPltyValue.Visible = false;
                        lblPltyValue.Visible = false;
                        RgExpValdPtlyValue.Visible = false;                       
                        RqPtlyValue.Enabled = false;
                         lblPltyAdd.Enabled = false;
                        chkboxAdd.Enabled = false;
                        chkboxAdd.Checked = false;
                        break;
                    case "T":
                        txtpltycomments.Visible = false;
                        RqFldComments.Visible = false;
                        lblTerm.Visible = true;
                        txtTerm.Visible = true;
                        txtTerm.Text = Convert.ToString(advocateValue.Term);
                        RqValdTerm.Enabled = true;
                        RgExpValdTerm.Enabled = true; 
                         RqValdTerm.Visible = true;
                        RgExpValdTerm.Visible = true; 
                        lblTerm_Desc.Visible = true;
                        ddlPtlyTerm_Desc.Visible = true;
                        if(!string.IsNullOrEmpty(Convert.ToString(advocateValue.Term_Desc)))
                        ddlPtlyTerm_Desc.SelectedValue = Convert.ToString(advocateValue.Term_Desc);
                        txtPltyValue.Visible = false;
                        lblPltyValue.Visible = false;
                        RgExpValdPtlyValue.Visible = false;
                        RqPtlyValue.Enabled = false;
                        if (ddlPtlyTerm_Desc.SelectedItem.Value == "DAYS")
                        {
                            lblPltyAdd.Enabled = true;
                            chkboxAdd.Enabled = true;
                        }
                        else
                        {
                            lblPltyAdd.Enabled = false;
                            chkboxAdd.Enabled = false;
                        }
                       
                        chkboxAdd.Checked = advocateValue.ApplyToTotal;                        
                        break;
                    case "F":
                        txtpltycomments.Visible = true;
                        txtpltycomments.Text = Convert.ToString(advocateValue.PenaltyComments);
                        RqFldComments.Visible = true;
                        RqFldComments.Enabled = true;
                        txtPltyValue.Visible = false;
                        lblPltyValue.Visible = false;
                        RqPtlyValue.Visible = false;
                        RgExpValdPtlyValue.Visible = false;
                        lblTerm.Visible = false;
                        txtTerm.Visible = false;
                         RgExpValdTerm.Visible = false;                      
                        RqValdTerm.Visible = false;
                        lblTerm_Desc.Visible = false;
                        ddlPtlyTerm_Desc.Visible = false;  
                        lblPltyAdd.Enabled = false;
                        chkboxAdd.Enabled = false;
                        chkboxAdd.Checked = false;
                        break;
                    case "M":
                        txtpltycomments.Visible = false;
                        RqFldComments.Visible = false;
                        txtPltyValue.Visible = true;
                        txtPltyValue.Text = Convert.ToString(advocateValue.DataValue);
                        lblPltyValue.Visible = true;
                        RgExpValdPtlyValue.Enabled = true;
                        RqPtlyValue.Enabled = true;
                        RgExpValdPtlyValue.Visible = true;
                        RqPtlyValue.Visible = true;
                        lblTerm.Visible = false;
                        txtTerm.Visible = false;
                        RgExpValdTerm.Visible = false;                        
                        RqValdTerm.Visible = false;
                        lblTerm_Desc.Visible = false;
                        ddlPtlyTerm_Desc.Visible = false;
                         lblPltyAdd.Enabled = false;
                        chkboxAdd.Enabled = false;
                        chkboxAdd.Checked = false;                                                                
                        break;
                }           
        }        


        /// <summary>
        /// This is a text change event that gets updated penalty complaints to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPltyType_SelectedIndexChanged(object sender, EventArgs e)
        {           
                UpdatePenalty();               
                Code = (Code)ViewState["CodeList"];
                Code.CodeId.ToString();
                foreach (Code code in codeList.CList)
                {
                    if (ddlPltyType.SelectedValue  == code.CodeId.ToString())
                    {
                        ChangeControlVisibility(code.ValueDataType);
                    }
                }     
        }       
               
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, penalty complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPltyHours_TextChanged(object sender, EventArgs e)
        {            
                UpdatePenalty();           
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, penalty complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPltyMnts_TextChanged(object sender, EventArgs e)
        {          
                UpdatePenalty();       
           
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, penalty complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void pltycomments_TextChanged(object sender, EventArgs e)
        {           
                UpdatePenalty();           
        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {
              if (RemoveUserControl != null)
                {
                    RemoveUserControl(sender, e);
                }           
        }
        /// <summary>
        /// This method loads the Advocate penalty status as per the status in the code list.
        /// </summary>
        private void LoadAdvocatePnltyType()
        {           
                codeList = new CodeList();
                codeList.Load(9);            
                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ViewState["CodeList"] = code;              
                    ddlPltyType.Items.Add(item);
                }           
        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtpltycomments_TextChanged(object sender, EventArgs e)
        {           
                UpdatePenalty();           
        }
        /// <summary>
        /// This is a checkbox change event that gets updated advocate penalty complaints to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkboxAdd_CheckedChanged(object sender, EventArgs e)
        {           
                UpdatePenalty();
                if (CalculateTotal != null)
                {
                    CalculateTotal(sender, e);
                }           
        }
        /// <summary>
        /// This is a checkbox change event that gets updated advocate penalty complaints to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void ChangeControlVisibility(string PenaltyType)
        {
           
                switch (PenaltyType)
                {
                    case "N":                        
                        txtpltycomments.Visible = false;
                        RqFldComments.Visible = false;
                        lblTerm.Visible = false;
                        txtTerm.Visible =false;
                        RgExpValdTerm.Visible = false;
                        RqValdTerm.Enabled = false;                      
                        lblTerm_Desc.Visible = false;
                        ddlPtlyTerm_Desc.Visible = false;
                        txtPltyValue.Visible = false;
                        lblPltyValue.Visible = false;   
                        RgExpValdPtlyValue.Visible = false;
                        RqPtlyValue.Enabled = false;   
                         lblPltyAdd.Enabled = false;
                        chkboxAdd.Enabled = false;
                        chkboxAdd.Checked = false;                       
                        break;
                    case "T":
                        txtpltycomments.Visible = false;
                        RqFldComments.Visible = false;
                        lblTerm.Visible = true;
                        txtTerm.Visible =true;
                        RqValdTerm.Enabled = true;
                        RgExpValdTerm.Enabled  = true;
                        RqValdTerm.Visible = true;
                        RgExpValdTerm.Visible  = true;
                        lblTerm_Desc.Visible = true;
                        ddlPtlyTerm_Desc.Visible = true;
                        txtPltyValue.Visible = false;
                        lblPltyValue.Visible = false;                           
                        RgExpValdPtlyValue.Visible = false;
                        RqPtlyValue.Visible  = false;
                         lblPltyAdd.Enabled = true;
                         if (ddlPtlyTerm_Desc.SelectedItem.Value == "DAYS")
                         {
                             lblPltyAdd.Enabled = true;
                             chkboxAdd.Enabled = true;
                         }
                         else
                         {
                             lblPltyAdd.Enabled = false;
                             chkboxAdd.Enabled = false;
                         }
                       
                        chkboxAdd.Checked = false;                         
                       
                        break;
                    case "F":                         
                        txtpltycomments.Visible = true;
                        RqFldComments.Visible = true;
                        RqFldComments.Enabled = true;
                        txtPltyValue.Visible = false;
                        lblPltyValue.Visible = false;
                        RqPtlyValue.Visible  = false;
                        RgExpValdPtlyValue.Visible = false;
                        lblTerm.Visible = false;
                        txtTerm.Visible =false;
                        RgExpValdTerm.Visible = false;
                        RqValdTerm.Visible = false;
                        lblTerm_Desc.Visible = false;
                        ddlPtlyTerm_Desc.Visible = false;     
                         lblPltyAdd.Enabled = false;
                        chkboxAdd.Enabled = false;
                        chkboxAdd.Checked = false;
                        break;
                    case "M":                          
                        txtpltycomments.Visible = false;
                        RqFldComments.Visible = false;
                        txtPltyValue.Visible = true;
                        lblPltyValue.Visible = true;
                        RgExpValdPtlyValue.Enabled = true;
                        RqPtlyValue.Enabled = true;
                        RgExpValdPtlyValue.Visible = true;
                        RqPtlyValue.Visible = true;
                        lblTerm.Visible = false;
                        txtTerm.Visible =false;
                         RgExpValdTerm.Visible = false;
                         RqValdTerm.Enabled = false;
                        lblTerm_Desc.Visible = false;
                        ddlPtlyTerm_Desc.Visible = false;  
                         lblPltyAdd.Enabled = false;
                        chkboxAdd.Enabled = false;
                        chkboxAdd.Checked = false;
                       
                        break;
                }
           
        }        
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// and updates the advocate penalty complaint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>        
        protected void txtSuspDays_TextChanged(object sender, EventArgs e)
        {           
                UpdatePenalty();
                if (CalculateTotal != null)
                {
                    CalculateTotal(sender, e);
                }           
        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// and updates the advocate penalty complaint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPltyFineDays_TextChanged(object sender, EventArgs e)
        {
           
             UpdatePenalty();
                if (CalculateTotal != null)
                {
                    CalculateTotal(sender, e);
                }
           
            
        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// and updates the advocate penalty complaint
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPltyValue_TextChanged(object sender, EventArgs e)
        {
            try
            {
                UpdatePenalty();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void txtTerm_TextChanged(object sender, EventArgs e)
        {            
                UpdatePenalty();           
           
        }

        protected void ddlPtlyTerm_Desc_SelectedIndexChanged(object sender, EventArgs e)
        {           
                if (ddlPtlyTerm_Desc.SelectedItem.Value == "DAYS")
                {
                    chkboxAdd.Checked = false;
                    chkboxAdd.Enabled = true;
                }
                else
                {
                    chkboxAdd.Checked = false;
                    chkboxAdd.Enabled = false;                
                }
                UpdatePenalty();
            }
          
    }
}