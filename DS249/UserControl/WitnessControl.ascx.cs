﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;


/// <summary>
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UserControl_WitnessControl : System.Web.UI.UserControl
    {
        public event EventHandler RemoveUserControl;
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                txtWitness.Enabled = value;
                btnRemove.Visible = value;
            }

        }
        /// <summary>
        /// This is a boolean function that enables print button display as per the role membership
        /// </summary>
        public Boolean PartialEnable
        {
            set
            {
                txtWitness.Enabled = value;
            }
        }

        WitnessList witnessList;

        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Button1.Style.Add("display", "none");
                if (Request.QueryString["Mode"] == "VIEW")
                    witnessList = (WitnessList)Session["witnessListView"];
                else
                    witnessList = (WitnessList)Session["witnessList"];           
        }

        /// <summary>
        /// Raise this event so the parent page can handle it 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {           
                if (RemoveUserControl != null)
                {
                    RemoveUserControl(sender, e);
                }           
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, witness complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtWitness_TextChanged(object sender, EventArgs e)
        {           
                String refNo = Common.ExtractNumbers(txtWitness.Text);
                if (refNo != String.Empty)
                {
                    txtWitness.Text = txtWitness.Text.Replace(refNo, "").Trim();
                    hdnRefNo.Value = refNo;
                }
                UpdateWitness();            
        }
        /// <summary>
        /// This method updates witness complants to the list
        /// </summary>
        private void UpdateWitness()
        {           
            Witness witness = witnessList.List.Find(delegate(Witness p) { return p.UniqueId == Convert.ToInt64(hdnUniqueID.Value); });
            witness.PersonnelName = txtWitness.Text.ToString();
            witness.RefNo = hdnRefNo.Value.ToString();           
        }
        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}