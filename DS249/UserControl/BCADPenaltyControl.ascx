﻿<%@ Control Language="C#" AutoEventWireup="true"  Inherits="DSNY.DSNYCP.DS249.UserControl_BCADPenaltyControl" Codebehind="BCADPenaltyControl.ascx.cs" %>

<table id="tblAboutPenalty" border="0" cellspacing="0px" cellpadding="0px">
    <tbody>
    <tr>
    <td>
     <table id="tblPenalty">
      <tbody>
       <tr style="vertical-align:top;"><td class="tdLabeling"><asp:Label ID="lblPenalty" runat="server" Text="PENALTY TYPE"></asp:Label></td></tr>
       <tr style="height: 40px; vertical-align:top;"><td><asp:DropDownList ID="ddlPltyType" runat="server" Font-Size="0.9em" 
               Font-Names="Arial" Width="210px" 
               onselectedindexchanged="ddlPltyType_SelectedIndexChanged" 
               AutoPostBack="True">
               <asp:ListItem Value="0">[SELECT]</asp:ListItem>
               </asp:DropDownList></td></tr>
      </tbody>
     </table>
    </td>
    <td>
     <table id="tblPltyDays">
     <tbody>
      <tr><td class="tdSubLabeling"><asp:Label ID="lblPltyDays" runat="server" Text="DAYS"></asp:Label>
          </td></tr>
      <tr style="height: 40px; vertical-align :top;"><td>
          <asp:TextBox ID="txtPltyDays" runat="server" Font-Size="0.9em" 
              Font-Names="Arial" Width="40px" ontextchanged="txtPltyDays_TextChanged" 
              AutoPostBack="True"></asp:TextBox> 
              <asp:CompareValidator ID="CompareValidator2" runat="server"
                ErrorMessage="Enter Only Numeric Value" Type="Integer" Operator="DataTypeCheck"  
                ControlToValidate="txtPltyDays"> !</asp:CompareValidator>       
          </td></tr>
     </tbody>
     </table>
    </td>
    <td>
     <table id="tblPltyHours">
      <tbody>
       <tr><td class="tdSubLabeling"><asp:Label ID="lblPltyHours" runat="server" Text="HOURS"></asp:Label></td></tr>
       <tr style="height: 40px; vertical-align:top;"><td><asp:TextBox ID="txtPltyHours" runat="server" Font-Size="0.9em" 
               Font-Names="Arial" Width="40px" ontextchanged="txtPltyHours_TextChanged"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator1" runat="server"
                ErrorMessage="Enter Only Numeric Value" Type="Integer" Operator="DataTypeCheck"  
                ControlToValidate="txtPltyHours"> !</asp:CompareValidator>     
               
               </td></tr>
      </tbody>
     </table>
    </td>
    <td>
     <table id="tblPltyMnts">
      <tbody>
       <tr><td class="tdSubLabeling"><asp:Label ID="lblPltyMnts" runat="server" Text="MINUTES"></asp:Label></td></tr>
       <tr style="height: 40px; vertical-align:top;"><td><asp:TextBox ID="txtPltyMnts" runat="server" Width="40px" Font-Size="0.9em" 
               Font-Names="Arial" ontextchanged="txtPltyMnts_TextChanged"></asp:TextBox>
               <asp:CompareValidator ID="CompareValidator3" runat="server"
                ErrorMessage="Enter Only Numeric Value" Type="Integer" Operator="DataTypeCheck"  
                ControlToValidate="txtPltyMnts"> !</asp:CompareValidator>
               </td></tr>
      </tbody>
     </table>
    </td>
    <td>
     <table id="tblPltyMoney">
     <tbody>
      <tr><td class="tdSubLabeling">&nbsp;&nbsp;&nbsp;<asp:Label ID="lblPltyMoney" runat="server" Text="MONETARY"></asp:Label>
          </td></tr>
      <tr style="height: 40px; vertical-align:top;"><td><asp:Label ID="lblDollar" runat="server" Text="$ "></asp:Label>
          <asp:TextBox ID="txtMonetary" runat="server" Font-Size="0.9em" 
              Font-Names="Arial" Width="60px" ontextchanged="txtMonetary_TextChanged"></asp:TextBox><br />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
              ErrorMessage="Invalid Format" Font-Size="0.5em" ControlToValidate="txtMonetary" ValidationExpression="^(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$"></asp:RegularExpressionValidator><br />
              <asp:RangeValidator ID="rngMonetary" Enabled="false" 
                            Runat="server"
                            ControlToValidate="txtMonetary" 
                            Type="Integer"
                            ErrorMessage="Amount must be between $25 and $200"
                            MaximumValue="200" 
                            MinimumValue="25"
                            Font-Size="0.5em"></asp:RangeValidator>

             </td></tr>
     </tbody>
     </table>
    </td>
    <td>
     <table id="tblPltyNotes">
     <tbody>
     <tr><td class="tdSubLabeling"><asp:Label ID="lblPltyNotes" runat="server" Text="NOTES"></asp:Label>
         </td></tr>
     <tr style="height: 40px; vertical-align:top;">
      <td><asp:TextBox ID="txtPltyNotes" runat="server" Width="310px" MaxLength="300" 
              Font-Size="0.9em" Font-Names="Arial" ontextchanged="txtPltyNotes_TextChanged"></asp:TextBox>
              <br /><asp:HiddenField ID="hdnUniqueID" runat="server" />
              </td>
      <td><asp:ImageButton ID="btnRemovePenalty" ImageUrl="~/includes/img/ds249_mini_-_btn.png" 
              runat="server" CausesValidation="false" Visible="false" 
              onclick="btnRemovePenalty_Click"/></td>
     </tr>
     </tbody>
    </table>
    </td>
    </tr>      
    </tbody>
    </table>
   