﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UserControl_AppealsControl : System.Web.UI.UserControl
    {
        public event EventHandler RemoveUserControl;    
        CodeList codeList;
        AdvocateAppealList appeals;
        AdvocateAppeal appeal;
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                txtAppealDate.Enabled = value;
                supnCal.Visible = false;
                ddlResult.Enabled = value;
                txtAppealcommnts.Enabled = value;
                btnRemove.Visible = value;
                ddlAppeal.Enabled = value;
            }

        }
        /// <summary>
        /// This event is fired when the page is initiated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Init(object sender, EventArgs e)
        {           
                LoadAppealResult();
                LoadAppeal();                  

        }
        /// <summary>
        /// This method updated the appeals to the list
        /// </summary>
        private void LoadAppeal()
        {           
                codeList = new CodeList();                
                codeList.Load(11);
                
                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlAppeal.Items.Add(item);
                }          
           
        }
        /// <summary>
        /// This method updated the penalties to the list
        /// </summary>
        private void LoadAppealResult()
        {
           
                codeList = new CodeList();

                if (Session["AppealResult"] == null)
                {
                    codeList.Load(12);
                    Session["AppealResult"] = codeList.CList;
                }
                else
                {
                    codeList.CList = (List<Code>)Session["AppealResult"];
                }

                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlResult.Items.Add(item);
                }          
        }
        /// <summary>
        /// This method updated the penalties to the list
        /// </summary>
        private void UpdateAppeal()
        {           
                appeal = appeals.List.Find(delegate(AdvocateAppeal p) { return p.UniqueId == Convert.ToInt16(hdnAppealUniqueID.Value); });
                appeal.AppealresultCD = Convert.ToInt64(ddlResult.SelectedItem.Value);
                appeal.AppealCD = Convert.ToInt64(ddlAppeal.SelectedItem.Value);
                if (txtAppealDate.Text != String.Empty)
                    appeal.AppealDate = Convert.ToDateTime(txtAppealDate.Text);
                else
                    appeal.AppealDate = null;
                appeal.AppealComments = txtAppealcommnts.Text;          
        }
        /// <summary>
        /// This method will load the values in a drop down.
        /// </summary>
        private void DropdownSelect()
        {
             if (appeals != null)
                {
                    if (appeals.List != null)
                    {
                        appeal = appeals.List.Find(delegate(AdvocateAppeal p) { return p.UniqueId == Convert.ToInt16(hdnAppealUniqueID.Value); });
                        if (appeal != null)
                        {
                            if (appeal.AppealresultCD != -1)
                                if (ddlResult.Items.IndexOf(ddlResult.Items.FindByValue(appeal.AppealresultCD.ToString())) != -1) 
                            {
                                ddlResult.Items.FindByValue(appeal.AppealresultCD.ToString()).Selected = true;
                            }
                            if (appeal.AppealCD != -1)
                                if (ddlAppeal.Items.IndexOf(ddlAppeal.Items.FindByValue(appeal.AppealCD.ToString())) != -1) 
                            {
                                ddlAppeal.Items.FindByValue(appeal.AppealCD.ToString()).Selected = true;
                            }
                        }
                    }                
            }
           
        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            appeals = (AdvocateAppealList)Session["advocateAppealList"];
            DropdownSelect();
        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {
              if (RemoveUserControl != null)
                {
                    RemoveUserControl(sender, e);
                }            
           
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, appealed are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlResult_SelectedIndexChanged(object sender, EventArgs e)
        {           
              UpdateAppeal();   

        }
        /// <summary>
        /// This is a  button click event where the associated complaints are updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibAddDt_Click(object sender, ImageClickEventArgs e)
        {           
                UpdateAppeal();           
        }
        /// <summary>
        /// This is a  text change event where the associated complaints are updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        protected void txtAppeals_TextChanged(object sender, EventArgs e)
        {         
                UpdateAppeal();            
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, appealed are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlAppeal_SelectedIndexChanged(object sender, EventArgs e)
        {           
                UpdateAppeal();            
        }

        protected void txtAppealDate_TextChanged(object sender, EventArgs e)
        {
            UpdateAppeal();
        }
    }
}