﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;

/// <summary>
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UserControl_BCADPenaltyControl : System.Web.UI.UserControl
    {
        public event EventHandler RemoveUserControl;
        MedicalPenaltyList medicalPenaltyList;
        BCADPenaltyList bcadPenaltyList;
        CodeList codeList;
        BcadPenalty bcadPenalty;
        MedicalPenalty medicalPenalty;
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                ddlPltyType.Enabled = value;
                btnRemovePenalty.Visible = value;
                txtMonetary.Enabled = value;
                txtPltyDays.Enabled = value;
                txtPltyHours.Enabled = value;
                txtPltyNotes.Enabled = value;
                txtPltyMnts.Enabled = value;
            }
        }
        /// <summary>
        /// This event is fired when the page is initiated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {  
                
             LoadBCADPenaltyType();
          
        }

        public enum ControlType
        {
            BCAD,
            Medical
        }

        public ControlType ControlLocation { get; set; }

        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
                bcadPenaltyList = (BCADPenaltyList)Session["bcadPenaltyList"];
                medicalPenaltyList = (MedicalPenaltyList)Session["medicalPenaltyList"];            
                DropdownSelect();
                ValidatePenalty();            
           
        }
        private void ValidatePenalty()
        {
            if (ddlPltyType.SelectedItem.Text == "FINED")
            {
                rngMonetary.Enabled = true;
                txtPltyDays.Text = "";
                txtPltyDays.Enabled = false;
                txtPltyHours.Text="";
                txtPltyHours.Enabled = false;
                txtPltyMnts.Text="";
                txtPltyMnts.Enabled = false;
            }
            else
            {
                rngMonetary.Enabled = false;
                txtPltyDays.Enabled = true;
                txtPltyHours.Enabled = true;
                txtPltyMnts.Enabled = true;
            }
        }

        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, BCAD Penalty are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void ddlPltyType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ControlLocation == ControlType.Medical)
            {
                ValidatePenalty();
            }
                UpdateBCADPenalty();
               
        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemovePenalty_Click(object sender, ImageClickEventArgs e)
        {           
                if (RemoveUserControl != null)
                {
                    RemoveUserControl(sender, e);
                }                    
        }

        /// <summary>
        /// This method updated the penalties to the list
        /// </summary>
        private void UpdateBCADPenalty()
        {           
                if (bcadPenaltyList != null)
                {
                    if (bcadPenaltyList.PenaltyList != null)
                    {
                        bcadPenalty = bcadPenaltyList.PenaltyList.Find(delegate(BcadPenalty p) { return p.UniqueId == Convert.ToInt16(hdnUniqueID.Value); });
                        if (bcadPenalty != null)
                        {                            
                            bcadPenalty.PenaltyTypeCode = Convert.ToInt16(ddlPltyType.SelectedItem.Value);
                            bcadPenalty.Days = Convert.ToString(txtPltyDays.Text);
                            bcadPenalty.Hours = Convert.ToString(txtPltyHours.Text);
                            bcadPenalty.Minutes = Convert.ToString(txtPltyMnts.Text);
                            bcadPenalty.Notes = Convert.ToString(txtPltyNotes.Text);
                            bcadPenalty.MoneyValue = Convert.ToString(txtMonetary.Text);
                        }
                    }
                }
                if (medicalPenaltyList != null)
                {
                    if (medicalPenaltyList.PenaltyList != null)
                    {
                        medicalPenalty = medicalPenaltyList.PenaltyList.Find(delegate(MedicalPenalty p) { return p.UniqueId == Convert.ToInt16(hdnUniqueID.Value); });
                        if (medicalPenalty != null)
                        {
                            medicalPenalty.PenaltyTypeCode = Convert.ToInt16(ddlPltyType.SelectedItem.Value);
                            medicalPenalty.Days = Convert.ToString(txtPltyDays.Text);
                            medicalPenalty.Hours = Convert.ToString(txtPltyHours.Text);
                            medicalPenalty.Minutes = Convert.ToString(txtPltyMnts.Text);
                            medicalPenalty.Notes = Convert.ToString(txtPltyNotes.Text);
                            medicalPenalty.MoneyValue = Convert.ToString(txtMonetary.Text);                    

                        }
                    }
                }                
           
        }
        /// <summary>
        /// This method updated the penalties to the list
        /// </summary>
        private void LoadBCADPenaltyType()
        {            
              codeList = new CodeList();
                //The below line is commented to populate the MEdical and BCAD drop down seperatlly TFS id 7000  
                if (ControlLocation== ControlType.BCAD)
                    codeList.Load(20);
                else
                    if (ControlLocation == ControlType.Medical)
                        codeList.Load(21);             

                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlPltyType.Items.Add(item);
                }           
        }
        /// <summary>
        /// This method will load the values in a drop down.
        /// </summary>
        private void DropdownSelect()
        {
            if (bcadPenaltyList != null)
                {
                    if (bcadPenaltyList.PenaltyList != null)
                    {
                        bcadPenalty = bcadPenaltyList.PenaltyList.Find(delegate(BcadPenalty p) { return p.UniqueId == Convert.ToInt16(hdnUniqueID.Value); });
                        if (bcadPenalty != null)
                        {
                            if (bcadPenalty.PenaltyTypeCode != 0)
                            {
                                ddlPltyType.Items.FindByValue(bcadPenalty.PenaltyTypeCode.ToString()).Selected = true;
                            }
                        }
                    }
                }
                if (medicalPenaltyList != null)
                {
                    if (medicalPenaltyList.PenaltyList != null)
                    {

                        medicalPenalty = medicalPenaltyList.PenaltyList.Find(delegate(MedicalPenalty p) { return p.UniqueId == Convert.ToInt16(hdnUniqueID.Value); });
                        if (medicalPenalty != null)
                        {
                            if (medicalPenalty.PenaltyTypeCode != 0)
                            {
                                ddlPltyType.Items.FindByValue(medicalPenalty.PenaltyTypeCode.ToString()).Selected = true;
                            }
                        }
                    }
                }           
        }
        /// <summary>
        /// This is a text change event that gets updated BCAD penalties to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPltyDays_TextChanged(object sender, EventArgs e)
        {
            UpdateBCADPenalty();
        }
        /// <summary>
        /// This is a text change event that gets updated BCAD penalties to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPltyHours_TextChanged(object sender, EventArgs e)
        {
            UpdateBCADPenalty();
        }
        /// <summary>
        /// This is a text change event that gets updated BCAD penalties to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPltyMnts_TextChanged(object sender, EventArgs e)
        {
            UpdateBCADPenalty();
        }
        /// <summary>
        /// This is a text change event that gets updated BCAD penalties to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtMonetary_TextChanged(object sender, EventArgs e)
        {
            UpdateBCADPenalty();
        }
        /// <summary>
        /// This is a text change event that gets updated BCAD penalties to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPltyNotes_TextChanged(object sender, EventArgs e)
        {
            UpdateBCADPenalty();
        }
    }
}