﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UserControl_AdvocateAssociatedcomplaints : System.Web.UI.UserControl
    {

        public event EventHandler RemoveUserControl;
        AdvocateAssociatedComplaintList assComplaintList;
        AdvocateAssociatedComplaint assComplaint;
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                txtcomplaints.Enabled = value;
                btnRemove.Visible = value;
            }

        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            assComplaintList = (AdvocateAssociatedComplaintList)Session["AssComplaintList"];

        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemove_Click(object sender, ImageClickEventArgs e)
        {
             if (RemoveUserControl != null)
                {
                    RemoveUserControl(sender, e);
                }
                     
        }
        /// <summary>
        /// This method is used to update the associated complaints
        /// </summary>
        private void UpdateAssComplaint()
        {
                Page.Validate();
                assComplaint = assComplaintList.List.Find(delegate(AdvocateAssociatedComplaint p) { return p.UniqueId == Convert.ToInt16(hdnAssCompUniqueID.Value); });
                if (txtcomplaints.Text != String.Empty)
                    assComplaint.AssociatedComplaintId = Convert.ToInt64(txtcomplaints.Text);
         
        }
        /// <summary>
        /// This is a  text change event where the associated complaints are updated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtcomplaints_TextChanged(object sender, EventArgs e)
        {
            UpdateAssComplaint();
        }
    }
}
