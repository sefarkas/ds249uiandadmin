﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.UserControl_IncidentDateControl"
    CodeBehind="IncidentDateControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
<link href="../includes/css/DashboardStyle.css" rel="stylesheet" type="text/css" />--%>
<table>
    <tr>
        <td>
            <asp:HiddenField ID="hdnChargeID" runat="server" />
            <asp:HiddenField ID="hdnUniqueID" runat="server" />
            <asp:TextBox ID="txtIncidentDt" runat="server" Width="113px" AutoPostBack="True"
                OnTextChanged="txtIncidentDt_TextChanged" Wrap="False" EnableViewState="False"
                Height="19px" CausesValidation="true" CssClass="inputField1Style"></asp:TextBox>
            <cc1:CalendarExtender ID="txtIncidentDt_CalendarExtender" runat="server" Enabled="True"
                TargetControlID="txtIncidentDt" PopupButtonID="supnCal" CssClass="MyCalendar"
                Format="MM/dd/yyyy">
            </cc1:CalendarExtender>
            <asp:Image id="imgDT" runat="server" SkinId="skidMiniCalButton" />
            <asp:ImageButton ID="btnRemoveDate" ImageUrl="~/includes/img/ds249_mini_-_btn.png"
                Visible="false" runat="server" />
            <br />
            <span class="label8Style">(i.e. Month/Date/Year; xx/xx/xxxx)</span>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtIncidentDt"
                ErrorMessage="Please Enter Incident Date">!</asp:RequiredFieldValidator>
        </td>
    </tr>
</table>
