﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pager.ascx.cs" Inherits="DSNY.DSNYCP.DS249.UserControl_Pager" %>
<table style="border-right-style: solid; border-right-width: thin; border-right-color: Black;
    border-left-style: solid; border-left-width: thin; border-left-color: Black;"
    width="100%">
    <tr>
        <td align="left">
            <asp:ImageButton ID="btnMoveFirst" ImageUrl="~/includes/img/arrow_l_multi.png"
                ToolTip="Move First" runat="server" OnClick="btnMoveFirst_Click" />
        </td>
        <td align="left">
            <asp:ImageButton ID="btnMovePrev" ImageUrl="~/includes/img/arrow_l_one.png"
                ToolTip="Move Previous" runat="server" OnClick="btnMovePrev_Click" />
        </td>
        <td align="left">
            <asp:Label ID="Label1" CssClass="commentsText" runat="server" Font-Bold="true" Text="Page"></asp:Label>
        </td>
        <td align="left">
            <asp:LinkButton ID="L1" runat="server" OnClick="L1_Click" ForeColor="#009933"></asp:LinkButton>
            <asp:LinkButton ID="L2" runat="server" OnClick="L2_Click" ForeColor="#009933"></asp:LinkButton>
            <asp:LinkButton ID="L3" runat="server" OnClick="L3_Click" ForeColor="#009933"></asp:LinkButton>
            <asp:LinkButton ID="L4" runat="server" OnClick="L4_Click" ForeColor="#009933"></asp:LinkButton>
            <asp:LinkButton ID="L5" runat="server" OnClick="L5_Click" ForeColor="#009933"></asp:LinkButton>
            <asp:LinkButton ID="L6" runat="server" OnClick="L6_Click" ForeColor="#009933"></asp:LinkButton>
            <asp:LinkButton ID="L7" runat="server" OnClick="L7_Click" ForeColor="#009933"></asp:LinkButton>
            <asp:LinkButton ID="L8" runat="server" OnClick="L8_Click" ForeColor="#009933"></asp:LinkButton>
        </td>
        <td align="left">
            <asp:Label ID="lblTotalPages" CssClass="commentsText" runat="server" Font-Bold="true"
                Text=""></asp:Label>
        </td>
        <td align="left">
            <asp:ImageButton ID="btnMoveNext" ImageUrl="~/includes/img/arrow_r_one.png"
                ToolTip="Move Next" runat="server" OnClick="btnMoveNext_Click" />
        </td>
        <td align="left">
            <asp:ImageButton ID="btnMoveLast" ImageUrl="~/includes/img/arrow_r_multi.png"
                ToolTip="Move Last" runat="server" OnClick="btnMoveLast_Click" />
        </td>
        <td>
        </td>
        <td align="right">
            <asp:Label ID="lblJumptoPage" CssClass="commentsText" runat="server" Font-Bold="true"
                Text=""></asp:Label>
            <asp:TextBox ID="txtJumptoPage" runat="server" Width="44px"></asp:TextBox>
        </td>
    </tr>
</table>
