﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.UserControl_HearingsControl"
    CodeBehind="HearingsControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<style type="text/css">
    .style10
    {
        width: 250px;
    }
    .style11
    {
        width: 149px;
    }
</style>
<table>
    <tr>
        <td class="style11">
            CAL STATUS
        </td>
        <td class="style10" nowrap>
            HEARING DATE<br />
            (Month/Date/Year: xx/xx/xxxx)
        </td>
        <td>
            TYPE
        </td>
        <td>
            STATUS
        </td>
    </tr>
    <tr>
        <td class="style11">
            <asp:DropDownList ID="ddlcalstatus" runat="server" Font-Size="0.9em" Font-Names="Arial"
                Width="125px" OnSelectedIndexChanged="ddlcalstatus_SelectedIndexChanged" AutoPostBack="True">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td class="style10">
            <asp:TextBox ID="txtHearingDt" runat="server" AutoPostBack="true" CausesValidation="true"
                CssClass="inputField1Style" Width="75px" OnTextChanged="txtSingleDt_TextChanged"></asp:TextBox>
            <cc1:CalendarExtender ID="txtHearingDt_CalendarExtender" runat="server" Enabled="True"
                TargetControlID="txtHearingDt" PopupButtonID="supnCal" CssClass="MyCalendar">
            </cc1:CalendarExtender>
            <asp:Image id="supnCal" runat="server" SkinId="skidMiniCalButton" />
        </td>
        <td>
            <asp:DropDownList ID="ddlhringtype" runat="server" Font-Size="0.9em" Font-Names="Arial"
                Width="160px" OnSelectedIndexChanged="ddlhringtype_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:DropDownList ID="ddlhringstatus" runat="server" Font-Size="0.9em" Font-Names="Arial"
                Width="145px" OnSelectedIndexChanged="ddlhringstatus_SelectedIndexChanged">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <asp:TextBox ID="txtHearingComments" runat="server" Width="319px" Wrap="False" EnableViewState="False"
                CssClass="inputFieldStyle" OnTextChanged="hearingcomments_TextChanged"></asp:TextBox>
            <asp:HiddenField ID="hdnHearingUniqueID" runat="server" />
        </td>
        <td>
            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/includes/img/ds249_mini_-_btn.png"
                OnClick="btnRemove_Click" Visible="false" CausesValidation="False" />
        </td>
    </tr>
</table>
