﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UserControl_IncidentDateControl : System.Web.UI.UserControl
    {

        public event EventHandler RemoveDateControl;
        public event EventHandler UpdateDateControl;
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                txtIncidentDt.Enabled = value;
                btnRemoveDate.Visible = value;
                imgDT.Visible = false;
            }

        }
        /// <summary>
        /// This event is fired when the page is initiated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            btnRemoveDate.Click += new ImageClickEventHandler(this.btnRemoveDate_Click);
        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {


        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, incedent data complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtIncidentDt_TextChanged(object sender, EventArgs e)
        {
           
                if (UpdateDateControl != null)
                    UpdateDateControl(sender, e);           
        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemoveDate_Click(object sender, ImageClickEventArgs e)
        {
             if (RemoveDateControl != null)
                    RemoveDateControl(sender, e);         
        }

    }
}