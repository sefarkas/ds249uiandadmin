﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provides the functionality to use a repeated control accross the pages
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UserControl_ComplainantControl : System.Web.UI.UserControl
    {
        public event EventHandler RemoveUserControl;
        ComplainantList complainantList;
        LocationList locationList;     
        /// <summary>
        /// This is a boolean function that enables button display as per the role membership
        /// </summary>
        public Boolean Enable
        {
            set
            {
                txtComplainantName.Enabled = value;
                txtComplainantStmt.Enabled = value;
                ddlComplainantLoc.Enabled = value;
                btnRemoveComplainant.Visible = value;
                txtComplaintantTitle.Enabled = value;
                ibTemplate.Visible = value;
            }
        }
        /// <summary>
        /// This is a boolean function that enables print button display as per the role membership
        /// </summary>
        public Boolean PartialEnable
        {
            set
            {
                txtComplainantName.Enabled = value;
                txtComplainantStmt.Enabled = value;
                ddlComplainantLoc.Enabled = value;
                txtComplaintantTitle.Enabled = value;
                ibTemplate.Visible = value;

            }
        }
        /// <summary>
        /// This event is fired when the page is initiated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
                loadLocationList();
                loadLocation();           
        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Button1.Style.Add("display", "none");
                if (Request.QueryString["Mode"] == "VIEW" || Request.QueryString["Mode"] == "APPROVE" || Request.QueryString["Mode"] == "CLOSE")
                {
                    complainantList = (ComplainantList)Session["ComplainantListView"];
                    ibTemplate.Visible = true;
                }
                else
                    complainantList = (ComplainantList)Session["ComplainantList"];
                DropdownSelect();

                if (Request.QueryString["Mode"] != "VIEW")
                {
                }
                ibTemplate.Attributes.Add("onClick", String.Format("OpenStatement({0});", txtComplainantStmt.ClientID));           
        }
        /// <summary>
        /// This is a button click event which is fired when the value is not null
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemoveComplainant_Click(object sender, ImageClickEventArgs e)
        {
                if (RemoveUserControl != null)
                {
                    RemoveUserControl(sender, e);
                }            
        }
        /// <summary>
        /// This is a text change event that gets updated DS249 complaints to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtComplainantName_TextChanged(object sender, EventArgs e)
        {            
                String refNo = Common.ExtractNumbers(txtComplainantName.Text);
                if (refNo != String.Empty)
                {
                    txtComplainantName.Text = txtComplainantName.Text.Replace(refNo, "").Trim();
                    hdnComplainantRefNo.Value = refNo;
                    UpdateComplainant();
                }           
        }
        /// <summary>
        /// This method updated DS249 complants to the list
        /// </summary>
        private void UpdateComplainant()
        {
                Complainant complainant = complainantList.List.Find(delegate(Complainant p) { return p.UniqueId == Convert.ToInt64(hdnComplainantUniqueID.Value); });
                complainant.ComplainantName = txtComplainantName.Text.ToString();
                complainant.RefNo = hdnComplainantRefNo.Value.ToString();
                complainant.TitleId = Convert.ToInt64(hdnComplainantTitleID.Value);
                complainant.LocationId = Convert.ToInt64(ddlComplainantLoc.SelectedItem.Value);
                complainant.Statement = txtComplainantStmt.Text; 

        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, DS249 complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlComplainantLoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateComplainant();
           
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, DS249 complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlComplainantTitle_SelectedIndexChanged(object sender, EventArgs e)
        {            
                UpdateComplainant();            
        }
        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, DS249 complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtComplaintantTitle_TextChanged(object sender, EventArgs e)
        {
             String TitleID = Common.ExtractNumbers(txtComplaintantTitle.Text);
                if (TitleID != String.Empty)
                {
                    txtComplaintantTitle.Text = txtComplaintantTitle.Text.Replace(TitleID, "").Trim();
                    hdnComplainantTitleID.Value = TitleID;
                    UpdateComplainant();
                }           
        }

        /// <summary>
        /// This is a  drop down list Selected Index Changed event when changed, DS249 complaints are updated.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtComplainantStmt_TextChanged(object sender, EventArgs e)
        { 
            UpdateComplainant();           
        }
        /// <summary>
        /// This method loads the set of location list
        /// </summary>
        private void loadLocationList()
        {
                locationList = new LocationList();
                    if (Session["LocationList"] == null)
                    {
                        locationList.Load();
                        Session["LocationList"] = locationList.List;
                    }
                    else
                    {
                        locationList.List = (List<LocationDS>)Session["LocationList"];
                    }                                        
           
        }
        /// <summary>
        /// This method loads the set of location list
        /// </summary>
        private void loadLocation()
        {
            ListItem item;
                foreach (LocationDS location in locationList.List)
                {
                item = new ListItem
                {
                    Value = location.LocationId.ToString(),
                    Text = location.LocationName.ToString()
                };
                ddlComplainantLoc.Items.Add(item);
                }          
        }
        /// <summary>
        /// This method loads the set of location title list
        /// </summary>
        private void loadTitleList()
        {
            TitleList titleList=new TitleList();
             if (Session["TitleList"] == null)
                {
                    titleList.Load();
                    Session["TitleList"] = titleList.List;
                }
                else
                {
                    titleList.List = (List<Title>)Session["TitleList"];
                }                               
           
        }
        /// <summary>
        /// This method loads the set of location title list
        /// </summary>
        private void loadTitle()
        {           
                ListItem item;
                TitleList titleList = new TitleList();
                foreach (Title title in titleList.List)
                {
                item = new ListItem
                {
                    Value = title.TitleId.ToString(),
                    Text = title.TitleDescription.ToString()
                };
                ddlComplainantTitle.Items.Add(item);
                }           
        }
        /// <summary>
        /// This method will load the values in a drop down.
        /// </summary>
        private void DropdownSelect()
        {
              if (complainantList != null)
                {
                    if (complainantList.List != null)
                    {
                        Complainant complainant;
                        complainant = complainantList.List.Find(delegate(Complainant p) { return p.UniqueId == Convert.ToInt16(hdnComplainantUniqueID.Value); });
                        if (complainant != null)
                        {
                            if (complainant.LocationId.ToString() != String.Empty)
                                ddlComplainantLoc.Items.FindByValue(complainant.LocationId.ToString()).Selected = true;
                            lblComplainantLoc.Text = ddlComplainantLoc.SelectedItem.Text.ToString();

                        }
                    }
                }            
        }


        protected void Button1_Click(object sender, EventArgs e)
        {

        }

    }
}