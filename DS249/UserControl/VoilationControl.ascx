﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.UserControl_VoilationControl"
    CodeBehind="VoilationControl.ascx.cs" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%--<link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
<link href="../includes/css/DashboardStyle.css" rel="stylesheet" type="text/css" />--%>
<script language="javascript" type="text/javascript">

    var vcHasResult = 0;
    var vcGetttingResult = 0;


    function PerformPostBack(sender, e) {
        $get('ctl00$ContentPlaceHolder1$Button1').click();
    }

    // Function for Employee Search
    function vcShownev(source, e) {
        vcHasResult = 1;
        vcGetttingResult = 0;
        $get('<%=lbvcSearchMsg.ClientID%>').innerText = "";
    }

    function vcPopulatingev(source, e) {
        vcGetttingResult = 1;
        vcHasResult = 0;
        $get('<%=lbvcSearchMsg.ClientID%>').innerText = "Retreiving..";
    }

    function vcOnhiding(source, e) {

        if (vcGetttingResult == 1) {
            if (vcHasResult == 0) {
                $get('<%=lbvcSearchMsg.ClientID%>').innerText = "No Match Found";

            }
        }
    }


    function OpenViewDocument() {

        var URL = "Upload.aspx";
        var dialogResults = window.open(URL, '');


    }

    function OpenPrint(IndexNo) {
        if (IndexNo != "") {
            var URL = "Print.aspx?Id=" + IndexNo;
            var dialogResults = window.open(URL, '');
        }
        else {
            alert("ComplaintID is Blank Or Missing");
        }
        return false;
    }

    //    ctl00_ContentPlaceHolder1_chkSuppDoc
    //    function OpenComfirmation(p1,p2,p3,p4) {
    //        if (confirm("Are you sure you want to attach the documents?")) {
    //            if (confirm("Attach the document only when you route the complaint to the Advocate.\n Please confirm.")) {
    //                alert(document.getElementById("ctl00_ContentPlaceHolder1_chkSuppDoc"));
    //                debugger
    //                var chkbox = document.getElementById("ctl00_ContentPlaceHolder1_chkSuppDoc");
    //                if (chkbox.style.visibility == "") {
    //                    chkbox.style.visibility = "visible";
    //                }
    //                
    ////                if (document.getElementById('ctl00_ContentPlaceHolder1_chkSuppDoc').style.visibility = "") {
    ////                    document.getElementById('ctl00_ContentPlaceHolder1_chkSuppDoc').style.visibility = "visible";
    ////                    }
    //                var stringURL = "Upload.aspx?ChildID1=" + p1 + "&Mode=" + p2 + "&ParentID=" + p3 + "&CountID=" + p4;
    //                window.open(stringURL, "UpLoad", "scrollbars=yes,resizable=yes,width=400,height=200,titlebar=no");

    //               
    //                }
    //            }
    //        }

    //************PENDING DOCUMENT************

    function OpenComfirmation(p1, p2, p3, p4) {
//        if (confirm("Are you sure you want to attach the documents?")) {
            if (confirm("Attach the document only when you route the complaint to the Advocate.\n Please confirm.")) {
                var stringURL = "Upload.aspx?ChildID1=" + p1 + "&Mode=" + p2 + "&ParentID=" + p3 + "&CountID=" + p4;
                window.open(stringURL, "UpLoad", "scrollbars=yes,resizable=yes,width=400,height=200");
//            }
        }
    }

  
    
</script>
<table width="100%" style="border-bottom-style: solid; border-bottom-width: thin;">
    <tr class="docAltLineStyle">
        <td class="labelBoldStyleOth" colspan="2" style="width: 60%;">
            <b>CHARGES: </b>
            <asp:Label ID="lblMandatoryCharge" runat="server" Visible="true" Text="*" ForeColor="Red"
                Font-Bold="true"></asp:Label><br />
            <asp:HiddenField ID="hdnUniqueID" runat="server" />
            <asp:HiddenField ID="hdnComplaintID" runat="server" />
            <asp:HiddenField ID="hdnChargeRefNo" runat="server" />
            <asp:HiddenField ID="hdnNewChargeRefNo" runat="server" />
            <asp:HiddenField ID="hdnOldChargeDesc" runat="server" />
            <asp:TextBox ID="txtCharge" runat="server" Height="16px" Width="466px" AutoCompleteType="Disabled"
                OnTextChanged="txtCharge_TextChanged" CssClass="inputField1Style"></asp:TextBox>
            <asp:Label ID="lblCharge" runat="server" class="labelStyle" Visible="false"></asp:Label>
            <asp:Image ID="imgIntelAssComp" ImageUrl="~/includes/img/ds249_i_mini_btn.png"
                runat="server" />
            <cc1:TextBoxWatermarkExtender ID="txtCharge_TextBoxWatermarkExtender" runat="server"
                Enabled="True" TargetControlID="txtCharge" WatermarkCssClass="WaterMark" WatermarkText="Enter Charge Code Eg. 1.4">
            </cc1:TextBoxWatermarkExtender>
            <cc1:AutoCompleteExtender ID="txtCharge_AutoCompleteExtender" runat="server" CompletionInterval="1000"
                MinimumPrefixLength="1" CompletionSetCount="12" EnableCaching="true" Enabled="True"
                ServicePath="" UseContextKey="True" ServiceMethod="GetCharges" OnClientItemSelected="PerformPostBack"
                TargetControlID="txtCharge" CompletionListCssClass="CompletionList" CompletionListHighlightedItemCssClass="HighlightedItem"
                CompletionListItemCssClass="ListItem" OnClientPopulating="vcPopulatingev" OnClientPopulated="vcShownev"
                OnClientHiding="vcOnhiding">
            </cc1:AutoCompleteExtender>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCharge"
                ErrorMessage="Please select the charges">!</asp:RequiredFieldValidator>
                  <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" CausesValidation="False" />
            <asp:Label ID="lbvcSearchMsg" runat="server" Text="" Font-Bold="True" Font-Size="Small"></asp:Label>
            <%--<cc1:ModalPopupExtender ID="mpe" runat="server" TargetControlID="btnUpload" PopupControlID="Panel1"
                DropShadow="true" OkControlID="OkButton" CancelControlID="CancelButton" BackgroundCssClass="modalBackground" />--%>
        </td>
        <td align="right">
            <asp:ImageButton ID="btnRemove" runat="server" ImageUrl="~/includes/img/ds249_mini_-_btn.png"
                OnClick="btnRemove_Click" Visible="False" CausesValidation="False" />
            <asp:ImageButton ID="btnUpload" runat="server" CausesValidation="False" ImageUrl="~/includes/img/ds249_btn_upload_sm.png"
                OnClick="btnUpload_Click" Enabled="False" />
            <%--    <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" BackColor="LightGray" 
                Width="400px" >
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                 <ContentTemplate>
                        <asp:FileUpload ID="FileUpload1" runat="server" Style="width: 217px" />
                 </ContentTemplate>
                   <%-- <Triggers>
                        <asp:PostBackTrigger ControlID="btnAttach" />
                    </Triggers>--%>
            <%--  <asp:GridView ID="gvUploadFile" runat="server" AutoGenerateColumns="False" BorderColor="#235705"
                                                    EmptyDataText="No File Uploaded" 
                                                    HeaderStyle-BackColor="#a4d49a" HeaderStyle-Font-Bold="true"
                                                    Width="380px" DataKeyNames="IndexNo"
                                                    OnRowDeleting="gvUploadFile_RowDeleting"
                                                    OnRowDataBound="gvUploadFile_RowDataBound">
                                                    <EmptyDataRowStyle CssClass="fromdate" />
                                                    <Columns>  
                                                    <asp:BoundField DataField="ComplaintID" DataFormatString="" HeaderText="File" Visible="true">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="IndexNo" DataFormatString="" HeaderText="File">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="IncidentID" DataFormatString="" HeaderText="File" Visible="true">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ChargeCode" DataFormatString="" HeaderText="ChargeID" Visible="true">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FileName" DataFormatString="" HeaderText="File">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        
                                                        <asp:TemplateField HeaderText="View">
                                                           <ItemTemplate>
                                                           <asp:ImageButton ID="ViewButton" ImageUrl="~/includes/img/ds249_btn_view_static.png"
                                                                    runat="server" CommandName="View" CausesValidation="false"/>
                                                           </ItemTemplate>                                                         
                                                         </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Delete">
                                                           <ItemTemplate>
                                                           <asp:ImageButton ID="deleteButton" ImageUrl="~/includes/img/ds249_btn_clear_static.png"
                                                                    runat="server" CommandName="Delete" CausesValidation="false"  />
                                                           </ItemTemplate>                                                         
                                                         </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#A4D49A" Font-Bold="True" />
                                                </asp:GridView>--%>
        </td>
    </tr>
    <tr class="docAltLineStyle">
        <td class="labelBoldStyleOth" valign="top">
            <b>LOCATION:</b><asp:Label ID="lblMandatoryLoc" runat="server" Visible="true" Text="*"
                ForeColor="Red" Font-Bold="true"></asp:Label><br />
            <asp:DropDownList ID="ddlIncedentLocation" runat="server" AppendDataBoundItems="true"
                OnSelectedIndexChanged="ddlIncedentLocation_SelectedIndexChanged" AutoPostBack="True"
                CssClass="inputField1Style">
                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblIncedentLocation" runat="server" class="labelStyle" Visible="false"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlIncedentLocation"
                ErrorMessage="Select Incident Location" InitialValue="-1">!</asp:RequiredFieldValidator>
        </td>
        <td class="labelBoldStyleOth" valign="top">
            <b>INCIDENT TYPE:</b><asp:Label ID="lblMandatoryIncident" runat="server" Visible="true"
                Text="*" ForeColor="Red" Font-Bold="true"></asp:Label><br />
            <asp:DropDownList ID="ddlChargeType" runat="server" OnSelectedIndexChanged="ddlChargeType_SelectedIndexChanged"
                AutoPostBack="True" CssClass="inputField1Style">
                <asp:ListItem Value="S">Single</asp:ListItem>
                <asp:ListItem Value="M">Multiple</asp:ListItem>
                <asp:ListItem Value="R">Date Range</asp:ListItem>
            </asp:DropDownList>
            <asp:Label ID="lblChargeType" runat="server" class="labelStyle" Visible="false"></asp:Label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlChargeType"
                ErrorMessage="Select Incident Type">!</asp:RequiredFieldValidator>
        </td>
        <td class="labelBoldStyleOth">
            <asp:PlaceHolder ID="phSingle" runat="server" Visible="false"><b>INCIDENT DATE:<asp:Label
                ID="Label1" runat="server" Visible="true" Text="*" ForeColor="Red" Font-Bold="true"></asp:Label></b><br />
                <asp:TextBox ID="txtSingleDt" runat="server" OnTextChanged="txtSingleDt_TextChanged"
                    AutoPostBack="true" CausesValidation="true" CssClass="inputField1Style"></asp:TextBox><asp:RequiredFieldValidator
                        ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtSingleDt" ErrorMessage="Select Incident Date">!</asp:RequiredFieldValidator>
                <asp:Label ID="lblDate" runat="server" class="labelStyle" Visible="false"></asp:Label>
                <cc1:CalendarExtender ID="txtSingleDt_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtSingleDt" PopupButtonID="supnCal" CssClass="MyCalendar" Format="MM/dd/yyyy">
                </cc1:CalendarExtender>
                <asp:Image id="supnCal" runat="server" SkinId="skidMiniCalButton" /><br />
                <asp:Label ID="lblDate1" runat="server" Visible="true" Text="(i.e. Month/Date/Year; xx/xx/xxxx)"
                    ForeColor="Black"></asp:Label>
                <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Incident Date Entered Is Incorrect"
                    Type="Date" Operator="DataTypeCheck" ControlToValidate="txtSingleDt"> !</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtSingleDt"
                    ValidationGroup="Approved" ErrorMessage="Please Enter Incident Date">!</asp:RequiredFieldValidator>
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phMultiple" runat="server" Visible="false">MUTIPLE INCIDENT DATE:
                <asp:ImageButton ID="ibAddDt" runat="server" ImageUrl="~/includes/img/ds249_mini_plus_btn.png"
                    OnClick="ibAddDt_Click" />
            </asp:PlaceHolder>
            <asp:PlaceHolder ID="phRange" runat="server" Visible="false">FROM:<br />
                <asp:TextBox ID="txtStartDt" runat="server" OnTextChanged="txtStartDt_TextChanged"
                    AutoPostBack="true" CausesValidation="true" CssClass="inputField1Style"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtStartDt"
                    ErrorMessage="Select To Date">!</asp:RequiredFieldValidator>
                <cc1:CalendarExtender ID="txtStartDt_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtStartDt" PopupButtonID="frmCal" CssClass="MyCalendar" Format="MM/dd/yyyy">
                </cc1:CalendarExtender>
                <asp:Image id="frmCal" runat="server" SkinId="skidMiniCalButton" />
                <br />
                <asp:Label ID="lblDate2" runat="server" Visible="true" Text="(i.e. Month/Date/Year; xx/xx/xxxx)"
                    ForeColor="Black"></asp:Label>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Incident Date Entered Is Incorrect"
                    Type="Date" Operator="DataTypeCheck" ControlToValidate="txtStartDt"> !</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtStartDt"
                    ValidationGroup="Approved" ErrorMessage="Please Enter Incident Date (From)">!</asp:RequiredFieldValidator>
                <br />
                TO:
                <br />
                <asp:TextBox ID="txtEndDt" runat="server" OnTextChanged="txtEndDt_TextChanged" AutoPostBack="true"
                    CausesValidation="true" CssClass="inputField1Style"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtEndDt"
                    ErrorMessage="Select end date">!</asp:RequiredFieldValidator>
                <cc1:CalendarExtender ID="txtEndDt_CalendarExtender" runat="server" Enabled="True"
                    TargetControlID="txtEndDt" PopupButtonID="toCal" CssClass="MyCalendar" Format="MM/dd/yyyy">
                </cc1:CalendarExtender>
                <asp:Image id="toCal" runat="server" SkinId="skidMiniCalButton" />
                <br />
                <asp:Label ID="lblDate3" runat="server" Visible="true" Text="(i.e. Month/Date/Year; xx/xx/xxxx)"
                    ForeColor="Black"></asp:Label>
                <asp:CompareValidator ID="cmpEndDate" runat="server" ErrorMessage="To date cannot be less than start date"
                    ControlToCompare="txtStartDt" ControlToValidate="txtEndDt" Operator="GreaterThanEqual"
                    Type="Date">!</asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEndDt"
                    ValidationGroup="Approved" ErrorMessage="Please Enter Incident Date (To)">!</asp:RequiredFieldValidator>
            </asp:PlaceHolder>
        </td>
    </tr>
</table>
