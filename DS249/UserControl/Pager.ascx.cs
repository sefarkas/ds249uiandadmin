﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;


namespace DSNY.DSNYCP.DS249
{
    public partial class UserControl_Pager : System.Web.UI.UserControl
	{
		public delegate void PageChange(int nextPage,string pagerQueryKey);
		public event PageChange OnPageChange;

		const string _PAGER_QUERY_KEY = "pg";
                
        public int PageSize
		{
			get
			{
				if (ViewState["PageSize"] == null)
				{
					ViewState["PageSize"] = 50;
				}
				return (int)ViewState["PageSize"];
			}
			set
			{
				ViewState["PageSize"] = value;
			}
		}
                
        public int TotalResults 
		{
			get
			{
				if (ViewState["TotalResults"] == null)
				{
					ViewState["TotalResults"] = 0;
				}
				return (int)ViewState["TotalResults"];
			}
			set
			{
				ViewState["TotalResults"] = value;
			}
		}

		public int? TotalPages
		{
			get
			{
				if (ViewState["TotalPages"] == null)
				{
					if ((TotalResults % PageSize) == 0)
					{
						ViewState["TotalPages"] = TotalResults / PageSize;
					}
					else
					{
						ViewState["TotalPages"] = (TotalResults / PageSize) + 1;
					}
				}
				return (int?)ViewState["TotalPages"];
			}
		}
        
		public int CurrentPage
		{
			get
			{
				if (ViewState["CurrentPage"] == null)
				{
					if (!String.IsNullOrEmpty(Request.QueryString[_PAGER_QUERY_KEY]))
					{
						int key;
						if (int.TryParse(Request.QueryString[_PAGER_QUERY_KEY], out key))
						{
							ViewState["CurrentPage"] = key;
						}
						else
						{
							ViewState["CurrentPage"] = 0;
						}
					}
					else
					{
						ViewState["CurrentPage"] = 0;
					}
				}
				return (int)ViewState["CurrentPage"];
			}
			set
			{
				ViewState["CurrentPage"] = value;
			}
		}

        public void InitializeVisibility()
        {
            L1.Enabled = true;
            L2.Enabled = true;
            L3.Enabled = true;
            L4.Enabled = true;
            L5.Enabled = true;
            L6.Enabled = true;
            L7.Enabled = true;
            L8.Enabled = true;

            L1.Visible = true;
            L2.Visible = true;
            L3.Visible = true;
            L4.Visible = true;
            L5.Visible = true;
            L6.Visible = true;
            L7.Visible = true;
            L8.Visible = true;

            btnMoveFirst.Enabled = btnMovePrev.Enabled = true;
            btnMoveLast.Enabled = btnMoveNext.Enabled = true;
        }

        public void SetColor()
        {
            if (this.CurrentPage == 0)
            {
                L1.ForeColor = System.Drawing.Color.Black;
            }
            else
            {
                if (this.CurrentPage.ToString() == L1.Text)
                    L1.ForeColor = System.Drawing.Color.Black;
                else
                    L1.ForeColor = System.Drawing.Color.Green;
                
                if (this.CurrentPage.ToString() == L2.Text)
                    L2.ForeColor = System.Drawing.Color.Black;
                else
                    L2.ForeColor = System.Drawing.Color.Green;
                
                if (this.CurrentPage.ToString() == L3.Text)
                    L3.ForeColor = System.Drawing.Color.Black;
                else
                    L3.ForeColor = System.Drawing.Color.Green;


                if (this.CurrentPage.ToString() == L4.Text)
                    L4.ForeColor = System.Drawing.Color.Black;
                else
                    L4.ForeColor = System.Drawing.Color.Green;


                if (this.CurrentPage.ToString() == L5.Text)
                    L5.ForeColor = System.Drawing.Color.Black;
                else
                    L5.ForeColor = System.Drawing.Color.Green;


                if (this.CurrentPage.ToString() == L6.Text)
                    L6.ForeColor = System.Drawing.Color.Black;
                else
                    L6.ForeColor = System.Drawing.Color.Green;


                if (this.CurrentPage.ToString() == L7.Text)
                    L7.ForeColor = System.Drawing.Color.Black;
                else
                    L7.ForeColor = System.Drawing.Color.Green;

                if (this.CurrentPage.ToString() == L8.Text)
                    L8.ForeColor = System.Drawing.Color.Black;
                else
                    L8.ForeColor = System.Drawing.Color.Green;
            }
        }

		public void BindPager()
		{
            ViewState["TotalPages"] = null;
            lblTotalPages.Text = String.Format("of {0}", this.TotalPages.ToString());
            lblJumptoPage.Text = "Jump to Page Number: ";

            InitializeVisibility();                      

            if (this.CurrentPage == 0)
            {
                L1.Text = Convert.ToString(this.CurrentPage + 1);
                L2.Text = Convert.ToString(this.CurrentPage + 2);
                L3.Text = Convert.ToString(this.CurrentPage + 3);
                L4.Text = Convert.ToString(this.CurrentPage + 4);
                L5.Text = Convert.ToString(this.CurrentPage + 5);
                L6.Text = Convert.ToString(this.CurrentPage + 6);
                L7.Text = Convert.ToString(this.CurrentPage + 7);
                L8.Text = Convert.ToString(this.CurrentPage + 8);
            }
            else
            {
                L1.Text = Convert.ToString(this.CurrentPage + 0);
                L2.Text = Convert.ToString(this.CurrentPage + 1);
                L3.Text = Convert.ToString(this.CurrentPage + 2);
                L4.Text = Convert.ToString(this.CurrentPage + 3);
                L5.Text = Convert.ToString(this.CurrentPage + 4);
                L6.Text = Convert.ToString(this.CurrentPage + 5);
                L7.Text = Convert.ToString(this.CurrentPage + 6);
                L8.Text = Convert.ToString(this.CurrentPage + 7);
            }

            SetColor();

			if (this.TotalPages > 0)
			{
				if (this.CurrentPage == 0 || this.CurrentPage == 1)
				{
                    btnMoveFirst.Enabled = btnMovePrev.Enabled = false;
                    btnMoveLast.Enabled = btnMoveNext.Enabled = true;
				}
				else if (this.CurrentPage == this.TotalPages)
				{
                    btnMoveFirst.Enabled = btnMovePrev.Enabled = true;
                    btnMoveLast.Enabled = btnMoveNext.Enabled = false;
				}
				else
				{
                    btnMoveFirst.Enabled = btnMovePrev.Enabled = true;
                    btnMoveLast.Enabled = btnMoveNext.Enabled = true;
				}             
			}
			else
			{
                btnMoveFirst.Enabled = btnMovePrev.Enabled = btnMoveLast.Enabled = btnMoveNext.Enabled = false;
			}

            if (this.TotalPages < 8)
            {
                if (this.TotalPages == 1) { L2.Visible = false; L3.Visible = false; L4.Visible = false; L5.Visible = false; L6.Visible = false; L7.Visible = false; L8.Visible = false; }
                if (this.TotalPages == 2) { L3.Visible = false; L4.Visible = false; L5.Visible = false; L6.Visible = false; L7.Visible = false; L8.Visible = false; }
                if (this.TotalPages == 3) { L4.Visible = false; L5.Visible = false; L6.Visible = false; L7.Visible = false; L8.Visible = false; }
                if (this.TotalPages == 4) { L5.Visible = false; L6.Visible = false; L7.Visible = false; L8.Visible = false; }
                if (this.TotalPages == 5) { L6.Visible = false; L7.Visible = false; L8.Visible = false; }
                if (this.TotalPages == 6) { L7.Visible = false; L8.Visible = false; }
                if (this.TotalPages == 7) { L8.Visible = false; }
            }
          
            if (this.TotalPages.ToString() == L1.Text) { L2.Visible = false; L3.Visible = false; L4.Visible = false; L5.Visible = false; L6.Visible = false; L7.Visible = false; L8.Visible = false; btnMoveLast.Enabled = btnMoveNext.Enabled = false; }
            if (this.TotalPages.ToString() == L2.Text) { L3.Visible = false; L4.Visible = false; L5.Visible = false; L6.Visible = false; L7.Visible = false; L8.Visible = false; btnMoveLast.Enabled= false; }
            if (this.TotalPages.ToString() == L3.Text) { L4.Visible = false; L5.Visible = false; L6.Visible = false; L7.Visible = false; L8.Visible = false; btnMoveLast.Enabled = false; }
            if (this.TotalPages.ToString() == L4.Text) { L5.Visible = false; L6.Visible = false; L7.Visible = false; L8.Visible = false; btnMoveLast.Enabled = btnMoveNext.Enabled = false; }
            if (this.TotalPages.ToString() == L5.Text) { L6.Visible = false; L7.Visible = false; L8.Visible = false; btnMoveLast.Enabled = false; }
            if (this.TotalPages.ToString() == L6.Text) { L7.Visible = false; L8.Visible = false; btnMoveLast.Enabled = false; }
            if (this.TotalPages.ToString() == L7.Text) { L8.Visible = false; btnMoveLast.Enabled = false; }       

		}

       
		protected void btnMoveFirst_Click(object sender, ImageClickEventArgs e)
		{
            if (this.CurrentPage == 8 || this.CurrentPage == 7 || this.CurrentPage == 6 || this.CurrentPage == 5 || this.CurrentPage == 4 || this.CurrentPage == 3 || this.CurrentPage == 2 || this.CurrentPage == 1)
            {
                this.CurrentPage -= 8;
            }
            else
            {
                this.CurrentPage = 1;
            }

            raisePageChangeEvent();			
		}

		protected void btnMovePrev_Click(object sender, ImageClickEventArgs e)
		{  
            this.CurrentPage -= 1;
            raisePageChangeEvent();                        
		}

		protected void btnMoveNext_Click(object sender, ImageClickEventArgs e)
		{            
            this.CurrentPage += 1;
            raisePageChangeEvent();            
		}

		protected void btnMoveLast_Click(object sender, ImageClickEventArgs e)
		{

            if (this.CurrentPage > this.TotalPages - 8)
            {
                this.CurrentPage = (int)this.TotalPages;
            }
            else if (this.CurrentPage == (this.TotalPages - 8) || this.CurrentPage == (this.TotalPages - 7) || this.CurrentPage == (this.TotalPages - 6) || this.CurrentPage == (this.TotalPages - 5) || this.CurrentPage == (this.TotalPages - 4) || this.CurrentPage == (this.TotalPages - 3) || this.CurrentPage == (this.TotalPages - 2) || this.CurrentPage == (this.TotalPages - 1))
            {
                this.CurrentPage += 8;
            }
            else
            {
                this.CurrentPage = (int)this.TotalPages;
            }

            raisePageChangeEvent();
		}
                                                                                                                                       
		private void raisePageChangeEvent()
		{
			if (OnPageChange != null)
			{
				OnPageChange(this.CurrentPage,_PAGER_QUERY_KEY);
			}
		}

        protected void L1_Click(object sender, EventArgs e)
        {
            this.CurrentPage = Convert.ToInt32(L1.Text);            
            raisePageChangeEvent();
        }

        protected void L2_Click(object sender, EventArgs e)
        {
            this.CurrentPage = Convert.ToInt32(L2.Text);            
            raisePageChangeEvent();
        }

        protected void L3_Click(object sender, EventArgs e)
        {
            this.CurrentPage = Convert.ToInt32(L3.Text);            
            raisePageChangeEvent();
        }

        protected void L4_Click(object sender, EventArgs e)
        {
            this.CurrentPage = Convert.ToInt32(L4.Text);            
            raisePageChangeEvent();
        }

        protected void L5_Click(object sender, EventArgs e)
        {
            this.CurrentPage = Convert.ToInt32(L5.Text);            
            raisePageChangeEvent();
        }

        protected void L6_Click(object sender, EventArgs e)
        {
            this.CurrentPage = Convert.ToInt32(L6.Text);            
            raisePageChangeEvent();
        }
        
        protected void L7_Click(object sender, EventArgs e)
        {
            this.CurrentPage = Convert.ToInt32(L7.Text);            
            raisePageChangeEvent();
        }     
    
        protected void L8_Click(object sender, EventArgs e)
        {
            this.CurrentPage = Convert.ToInt32(L8.Text); 
            raisePageChangeEvent();
        }
    }
}