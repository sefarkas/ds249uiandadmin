﻿using System.Collections.Generic;
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.DS249
{
    /// <summary>
    /// This class provides the functionality for events and methods for the Medical case management.
    /// </summary>
    public partial class MedicalCaseManagement : System.Web.UI.Page
    {

        #region Page Variables

        MedicalCodeList codeList;               
        ComplainantList complainantList;
        Complaint complaint;      
        LocationList boroughList;
        MedicalPenaltyList medicalPenaltyList;       
        MedicalComplaint medicalComplaint;       
        UserControl_BCADPenaltyControl PenaltyControl;
        User user;

        #endregion
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {            
                ibPrint.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_print_roll.png'");
                ibPrint.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_print_static.png'");

                btnCaseList.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
                btnCaseList.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");

                btnSave.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_save_roll.png'");
                btnSave.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_save_static.png'");

                user = (User)Session["clsUser"];
                if (user != null)
                {
                    if (!user.IsInMemberShip(GroupName.MEDICAL))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                    else
                    {
                        user.CurrentMembership = GroupName.MEDICAL;
                        SecurityUtility.AttachRolesToUser(user);
                    }

                    if (!User.IsInRole("Read"))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                }
                else
                    Response.Redirect("../UI/Login.aspx");

                DisplayButtonBasedOnPermission();
                initializeUserControls();

                if (Session["Mode"].ToString() == "EDIT" || Session["MODE"].ToString() == "VIEW")
                {
                    ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
                }
                if (!IsPostBack)
                {
                    LoadDropDownLists();
                    LoadComplaintData();
                }
                if (Session["MODE"].ToString() == "VIEW")
                {

                    //***Changes*** 2.0.9 AFTER
                    ChangeControlStatus(true);
                    ddlCaseStatus.Enabled = true;
                }           
        }


        #region Private Methods


        #region UserControl
        /// <summary>
        /// This method initialises the user control
        /// </summary>
        private void initializeUserControls()
        {               
                initializePenaltyControl();           
        }
        /// <summary>
        /// This method initialises the user control
        /// </summary>
        private void initializePenaltyControl()
        {
            if (Session["medicalPenaltyList"] == null)
                {
                    medicalPenaltyList = new MedicalPenaltyList();
                    Session["medicalPenaltyList"] = medicalPenaltyList;
                    medicalPenaltyList.AddMedicalPenalty();
                }
                else
                    medicalPenaltyList = (MedicalPenaltyList)Session["medicalPenaltyList"];

                if (medicalPenaltyList.PenaltyList.Count == 0)
                {
                    medicalPenaltyList = new MedicalPenaltyList();
                    Session["medicalPenaltyList"] = medicalPenaltyList;
                    medicalPenaltyList.AddMedicalPenalty();
                }
                CreatePenaltyControl();           
        }
        /// <summary>
        /// Add penalty object in the list.
        /// </summary>
        private void AddPenaltyControl()
        {
           
                medicalPenaltyList.AddMedicalPenalty();
                CreatePenaltyControl();
            
        }
        /// <summary>
        /// Create list of controls from the penalty list in the user control 
        /// </summary>
        private void CreatePenaltyControl()
        {
            int i = 1;
                pnPenalty.Controls.Clear();

                foreach (MedicalPenalty medicalPenalty in medicalPenaltyList.PenaltyList)
                {
                    PenaltyControl = (UserControl_BCADPenaltyControl)LoadControl("~/UserControl/BCADPenaltyControl.ascx");
                    PenaltyControl.ID = "ucPenalty" + medicalPenalty.UniqueId;
                    PenaltyControl.ControlLocation = UserControl_BCADPenaltyControl.ControlType.Medical;
                    if (i > 1)
                    {
                        
                        ((ImageButton)PenaltyControl.FindControl("btnRemovePenalty")).Visible = true;
                    }                    

                    PenaltyControl.RemoveUserControl += this.HandleRemovePenaltyControl;
                    PopulatePenaltyData(medicalPenalty, PenaltyControl);
                    pnPenalty.Controls.Add(PenaltyControl);
                    i++;
                }
            
        }
        /// <summary>
        /// Bind the BCAD penalty values to the user control 
        /// </summary>
        /// <param name="bcadPenalty"></param>
        /// <param name="ctrPenalty"></param>
        private void PopulatePenaltyData(MedicalPenalty medicalPenalty, Control ctrPenalty)
        {
                ((HiddenField)ctrPenalty.FindControl("hdnUniqueID")).Value = medicalPenalty.UniqueId.ToString();
                ((TextBox)ctrPenalty.FindControl("txtPltyDays")).Text = medicalPenalty.Days.ToString();
                ((TextBox)ctrPenalty.FindControl("txtPltyHours")).Text = medicalPenalty.Hours.ToString();
                ((TextBox)ctrPenalty.FindControl("txtPltyMnts")).Text = medicalPenalty.Minutes.ToString();
                ((TextBox)ctrPenalty.FindControl("txtMonetary")).Text = medicalPenalty.MoneyValue.ToString();
                ((TextBox)ctrPenalty.FindControl("txtPltyNotes")).Text = medicalPenalty.Notes.ToString();           

        }

        /// <summary>
        /// This handles delete event fired from the user control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemovePenaltyControl(object sender, EventArgs e)
        {            
                ImageButton but = sender as ImageButton;
                UserControl_BCADPenaltyControl PenaltyControl = (UserControl_BCADPenaltyControl)but.Parent;
                Int64 UniqueID = Convert.ToInt64((((HiddenField)PenaltyControl.FindControl("hdnUniqueID")).Value));
                medicalPenaltyList.RemoveMedicalPenalty(UniqueID);
                pnPenalty.Controls.Remove(PenaltyControl);           
        }

        #endregion
        /// <summary>
        /// Display the permisions based on the membership roles
        /// </summary>
        private void DisplayButtonBasedOnPermission()
        {
            if (!User.IsInRole("Write"))
            {
                btnSave.Visible = false;
            }
            if (!User.IsInRole("Print"))
            {
                ibPrint.Visible = false;
            }

        }
        /// <summary>
        /// This method saves the BCAD complaints to the database
        /// </summary>
        private void SaveMedicalComplaint()
        {
          
                user = (User)Session["clsUser"];
                if (medicalComplaint == null)
                    medicalComplaint = (MedicalComplaint)Session["MEDICALComplaint"];

                //The below code is to resolve TFS#7643/8010/7643
                Type cstype = this.GetType();

                for (int i = 0; i < medicalComplaint.Penalty.PenaltyList.Count; i++)
                {

                    if (medicalComplaint.Penalty.PenaltyList[i].PenaltyTypeCode == 183
                        && !string.IsNullOrWhiteSpace(medicalComplaint.Penalty.PenaltyList[i].MoneyValue))
                    {

                      
                        if (Convert.ToInt32(medicalComplaint.Penalty.PenaltyList[i].MoneyValue) < 25)
                        {
                            ClientScript.RegisterStartupScript(cstype, @"message", @"<script>alert('Amount must be a minimum of $25.');</script>");
                            return;
                        }

                        else if (Convert.ToInt32(medicalComplaint.Penalty.PenaltyList[i].MoneyValue) > 200)
                        {
                            ClientScript.RegisterStartupScript(cstype, @"message", @"<script>alert('Amount must be between $25 and $200');</script>");
                            return;
                        }

                    }
                 
                    if (medicalComplaint.Penalty.PenaltyList[i].PenaltyTypeCode == 183
                       && string.IsNullOrWhiteSpace(medicalComplaint.Penalty.PenaltyList[i].MoneyValue))
                    {
                        ClientScript.RegisterStartupScript(cstype, @"message", @"<script>alert('Amount must be a minimum of $25.');</script>");
                        return;
                    }
                }                

                if (ddlCaseStatus.SelectedItem.Value == "131")
                {

                    medicalComplaint.ReturnToAuthor(user.PersonalId);
                    ClientScript.RegisterStartupScript(cstype, @"message", @"<script>alert('Case Returned to Author!');document.location ='Dashboard.aspx';</script>");
                }

                else if (ddlCaseStatus.SelectedItem.Value == "132")
                {
                    medicalComplaint.ReturnToAdvocate(user.PersonalId);
                    ClientScript.RegisterStartupScript(cstype, @"message", @"<script>alert('Case Send to Advocacy!');document.location ='Dashboard.aspx';</script>");
                }

                else
                {
                    medicalComplaint.FinalStatusCD = Convert.ToInt16(ddlCaseStatus.SelectedItem.Value.ToString());
                    if (txtAdjudDate.Text != String.Empty)
                        medicalComplaint.AjudicationDate= Convert.ToDateTime(txtAdjudDate.Text);
                    medicalComplaint.NonPenaltyResultCD = Convert.ToInt16(ddlNonPltyRslt.SelectedItem.Value.ToString());
                    medicalComplaint.ModifiedUser = Convert.ToInt16(Session["UserEmpID"]);
                    medicalComplaint.ModifiedDate = DateTime.Now;
                    medicalComplaint.Penalty = medicalPenaltyList;                 

                    if (medicalComplaint.Save())
                    {
                        String script = "<script>alert('Case Saved Successfully!'); document.location ='Dashboard.aspx';</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "message", script, false);
                        ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
                    }
                    else
                    {
                        lbMessage.Text = "Error Occured. Unable to Save Complaint Data";
                        lbMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }           

        }
        /// <summary>
        /// Loads the complaint data to the form
        /// </summary>
        private void LoadComplaintData()
        {           
                Int64 ID = Convert.ToInt64(Request.QueryString["Id"]);
                complaint = new Complaint();
                complaint.Load(ID, RequestSource.Medical);
                if (complaint.ComplaintId != -1)
                {
                    BindComplaintData();
                    Session["MedicalComplaint"] = complaint.MedicalComplaint;

                    medicalPenaltyList = new MedicalPenaltyList();
                    medicalPenaltyList = complaint.MedicalComplaint.Penalty;
                    medicalPenaltyList.NewUniqueId = Convert.ToInt16(medicalPenaltyList.PenaltyList.Count);
                    Session["medicalPenaltyList"] = medicalPenaltyList;

                    BindMedicalComplaint();

                    BindPersonnelData();

                    complainantList = new ComplainantList();
                    complainantList = complaint.Complainants;
                    BindComplaintantData();
                    
                    BindVoilations();

                    initializePenaltyControl();
                }
                else
                {
                    Response.Redirect("Dashboard.aspx");
                }
            
        }
        /// <summary>
        /// This method binds the BCAD complaint data to the form
        /// </summary>
        private void BindMedicalComplaint()
        {
            medicalComplaint = complaint.MedicalComplaint;

            if (ddlCaseStatus.Items.IndexOf(ddlCaseStatus.Items.FindByValue(medicalComplaint.FinalStatusCD.ToString())) != -1) 
            ddlCaseStatus.Items.FindByValue(medicalComplaint.FinalStatusCD.ToString()).Selected = true;

            if (ddlNonPltyRslt.Items.IndexOf(ddlNonPltyRslt.Items.FindByValue(medicalComplaint.NonPenaltyResultCD.ToString())) != -1) 
            ddlNonPltyRslt.Items.FindByValue(medicalComplaint.NonPenaltyResultCD.ToString()).Selected = true;

            if (medicalComplaint.AjudicationDate != DateTime.MinValue)
                txtAdjudDate.Text = medicalComplaint.AjudicationDate.ToShortDateString();
            chkProbation.Checked = medicalComplaint.IsProbation;
        }
        /// <summary>
        /// This method binds the voilation data to the form
        /// </summary>
        private void BindVoilations()
        {           
                rptViolations.DataSource = complaint.Violations.List;
                rptViolations.DataBind();           
        }

        /// <summary>
        /// This method binds the complaint data to the form
        /// </summary>
        private void BindComplaintantData()
        {
             rptCompName.DataSource = complainantList.List;
                rptCompName.DataBind();
                rptCompTitle.DataSource = complainantList.List;
                rptCompTitle.DataBind();
                rptCompDistrict.DataSource = complainantList.List;
                rptCompDistrict.DataBind();
                rptCompStmt.DataSource = complainantList.List;
                rptCompStmt.DataBind();           
        }
        /// <summary>
        /// This method binds the complaint data to the form
        /// </summary>
        private void BindComplaintData()
        {
            
                txtCreatDt.Text = complaint.CreatedDate.ToShortDateString();
                txtIdxNo.Text = complaint.IndexNo.ToString();
                txtGnrlCmts.Text = complaint.Comments.ToString();
                txtDivision.Text = complaint.LocationName.ToString();
                LoadBoroughByLocation();
            
        }
        /// <summary>
        /// This method loads the list of Borough w.r.t location
        /// </summary>
        private void LoadBoroughByLocation()
        {
            boroughList = new LocationList();
                boroughList.Load(true);
                LocationDS borough = boroughList.List.Find(delegate(LocationDS q) { return q.LocationId == Convert.ToInt64(complaint.BoroughId); });
                    if (borough != null)
                    {
                        String Borough = borough.LocationName.ToString();
                        txtBorough.Text = Borough;                    
                   }
                else
                    txtBorough.Text = "";           
        }
        /// <summary>
        /// This method binds the personnel data to the form
        /// </summary>
        private void BindPersonnelData()
        {           
                txtRespName.Text = complaint.FullName.ToString();
                txtRespRefNo.Text = complaint.ReferenceNumber.ToString();
                if (complaint.RespondentAptDate != DateTime.MinValue)
                    txtRespAppointment.Text = complaint.RespondentAptDate.ToShortDateString();
                txtRespVacSch.Text = complaint.VacationSchedule.ToString();
                txtChartDayNo.Text = complaint.ChartNo.ToString();
                txtBadge.Text = complaint.BadgeNumber.ToString();
                txtRespTitle.Text = complaint.TitleDescription.ToString();
                txtPayLoc.Text = complaint.PayCodeDescription.ToString();    
           
        }
        /// <summary>
        /// This method loads the drop down list values
        /// </summary>
        private void LoadDropDownLists()
        {
            LoadMedicalCaseStatus();
            LoadNonPenaltyResults();
            LoadAdjudicationBody();
        }
        /// <summary>
        /// This method Load Non-Penalty Results to the form
        /// </summary>
        private void LoadNonPenaltyResults()
        {           
                codeList = new MedicalCodeList();
                codeList.Load(18);                
                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                if (code.CodeId.ToString() != "135" && code.CodeName.ToString() != "WARN LETTER")
                        ddlNonPltyRslt.Items.Add(item);
                }            
        }
        /// <summary>
        /// This method Load Adjudication Body to the form
        /// </summary>
        private void LoadAdjudicationBody()
        {
             codeList = new MedicalCodeList();
                codeList.Load(19);        
                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
            }           
        }

        /// <summary>
        /// This method Load BCAD Case Status
        /// </summary>
        private void LoadMedicalCaseStatus()
        {           
                codeList = new MedicalCodeList();
                codeList.Load();                
                 System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlCaseStatus.Items.Add(item);
                }           
        }
        /// <summary>
        /// This method returns true if the controls in the form are enabled based on the membership roles
        /// </summary>
        /// <param name="status"></param>
        private void ChangeControlStatus(bool status)
        {
            
                if (Session["Mode"].ToString() == "NEW")
                {
                    ibPrint.Enabled = status;
                }

                foreach (Control c in upComplaint.Controls)
                {
                    foreach (Control ctrl in c.Controls)
                    {
                        if (ctrl is TextBox)

                            ((TextBox)ctrl).Enabled = status;

                        else if (ctrl is Button)

                            ((Button)ctrl).Enabled = status;

                        else if (ctrl is RadioButton)

                            ((RadioButton)ctrl).Enabled = status;

                        else if (ctrl is ImageButton)

                            ((ImageButton)ctrl).Visible = status;
                        else if (ctrl is System.Web.UI.WebControls.Image)

                            ((System.Web.UI.WebControls.Image)ctrl).Visible = status;

                        else if (ctrl is CheckBox)

                            ((CheckBox)ctrl).Enabled = status;

                        else if (ctrl is DropDownList)

                            ((DropDownList)ctrl).Enabled = status;

                        else if (ctrl is HyperLink)
                            ((HyperLink)ctrl).Enabled = status;
                        else if (ctrl is PlaceHolder)
                        {
                            if (ctrl.ID == "pnPenalty")
                            {
                                foreach (UserControl_BCADPenaltyControl control in ctrl.Controls)
                                {
                                    control.Enable = status;
                                }
                            }
                        }
                    }
                }           

        }


        #endregion

        #region Page Events
        /// <summary>
        /// This button click event will redirect the page to the dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCaseList_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }       
        /// <summary>
        /// This click event will save the BCAD complaint data to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {           
                if (Convert.ToInt16(ddlCaseStatus.SelectedItem.Value) == 0)
                {
                    lbMessage.Text = "Please Select Appropriate [Case Status] Option !";
                    lbMessage.ForeColor = System.Drawing.Color.Green;
                }
                else
                SaveMedicalComplaint();           
        }
        /// <summary>
        /// This is a dropdown list change event that Allows the user to Reopen the Case.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCaseStatus_SelectedIndexChanged(object sender, EventArgs e)
        {           
                if (ddlCaseStatus.SelectedItem.Value == "96" && Session["Mode"].ToString() == "VIEW")
                {
                    if (medicalComplaint == null)
                        medicalComplaint = (MedicalComplaint)Session["MedicalComplaint"];
                    medicalComplaint.FinalStatusCD = Convert.ToInt16(ddlCaseStatus.SelectedItem.Value);
                    medicalComplaint.SetMedicalStatus();
                    Session["Mode"] = "EDIT";
                    ScriptManager.RegisterStartupScript(ddlCaseStatus, this.GetType(), "msg", String.Format("<script>alert('Case Reopened!');document.location ='MedicalCaseManagement.aspx?ID={0}';</script>", medicalComplaint.ComplaintId), false);
                }            
        }
        /// <summary>
        /// This event allows the user to create a new penalty 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewPenalty_Click(object sender, ImageClickEventArgs e)
        {           
                AddPenaltyControl();            
        }
        #endregion
    }
}