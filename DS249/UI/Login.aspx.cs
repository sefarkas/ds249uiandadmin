﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DSNY.DSNYCP.ClassHierarchy;



namespace DSNY.DSNYCP.DS249
{
    /// <summary>
    /// This method provides functionality to login authenticated user to the website.
    /// </summary>
    public partial class Login : System.Web.UI.Page
    {
        LocationList locationList;
        LocationList boroughList;
        TitleList titleList;
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

           if ((string)Session["IsAuthenticated"] == "true")
                {
                    Session["IsAuthenticated"] = "false";
                    Session["UserRoles"] = null;
                    Session.RemoveAll();
                    FormsAuthentication.SignOut();
                }
                           
                Session["IsAuthenticated"] = "false";
                Session["UserRoles"] = null;
                txtUserName.Focus();
                if (Request.QueryString["Session"] == "False")
                {
                    lMessage.Text = "Session Expired! Please Login Again";
                    lMessage.ForeColor = System.Drawing.Color.Red;
                }

                Page.Form.DefaultButton = btnLogin.UniqueID;
                if (IsPostBack)
                {
                
                loadLocationList();
                loadBoroughList();
                loadTitleList();
                }
            }          

        
        /// <summary>
        /// This button click event allows the user to redirect to the forgot password page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ForgotPasswordLinkButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("ForgotPassword.aspx");
        }

        /// <summary>
        /// This method allows the user to login to the website.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {
            User user = new User
            {
                UserId = txtUserName.Text.Trim(),
                Password = txtPassword.Text.Trim()
            };
            if (user.AuthenticateUser())
            {
                if (user.PersonalId != -1)
                {
                    Session["IsAuthenticated"] = "true";
                    Session["UserEmpID"] = user.PersonalId;
                    Session["User"] = txtUserName.Text;
                    Session["clsUser"] = user;                   
                    SecurityUtility.RedirectFromLoginPage(txtUserName.Text.Trim(), "Dummy", true);
                }
                else
                {
                    txtUserName.Text = String.Empty;
                    lMessage.Text = "Login Failed. The UserName/Password Entered is Incorrect.";
                    lMessage.ForeColor = System.Drawing.Color.Red;
                }
                
            }
            else
            {
                txtUserName.Text = String.Empty;
                lMessage.Text = "Login Failed. The UserName/Password Entered is Incorrect.";
                lMessage.ForeColor = System.Drawing.Color.Red;
               
            }
        }

        private void loadLocationList()
        {          
                locationList = new LocationList();

                if (Session["LocationList"] == null)
                {
                    locationList.Load();
                    Session["LocationList"] = locationList.List;
                }
                else
                {
                    locationList.List = (List<LocationDS>)Session["LocationList"];
                }
         }
           


        private void loadBoroughList()
        { 

                boroughList = new LocationList();
                if (Session["BoroughList"] == null)
                {
                    boroughList.Load(true);
                    Session["BoroughList"] = boroughList.List;
                }
                else
                {
                    boroughList = new LocationList();
                    boroughList.Load(true);
                }
            }
                
        private void loadTitleList()
        {           
                titleList = new TitleList();
                if (Session["TitleList"] == null)
                {
                    titleList.Load();
                    Session["TitleList"] = titleList.List;
                }
                else
                {
                    titleList.List = (List<Title>)Session["TitleList"];
                }   
          
        }
    }
}