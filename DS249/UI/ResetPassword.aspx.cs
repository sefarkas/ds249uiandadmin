﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provide functionality to reset the password
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UI_ResetPassword : System.Web.UI.Page
    {
        /// <summary>
        /// This method redirect the user to logout page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            SignInIDTextBox.Text = Convert.ToString(Session["User"]);
        }
        /// /// <summary>
        /// This method provide functionality to reset the password
        /// </summary>
        protected void ResetPassword_Click(object sender, ImageClickEventArgs e)
        {
            User usr = (User)Session["clsUser"];           
                usr.UserId = SignInIDTextBox.Text.Trim();
                usr.Password = PasswordTextBox.Text.Trim();
                usr.IsTempPassword = false;
                usr.ResetPassword();
                Common.RedirectToDefaultPage();
            }           
           
        }
    }
