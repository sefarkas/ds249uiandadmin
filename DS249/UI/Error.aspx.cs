﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DSNY.DSNYCP.DS249
{
    public partial class UI_Error : System.Web.UI.Page
    {
        //User user;
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["errorID"] != null)
            {
                lblErrorID.Text = "Error ID : "+ Session["errorID"].ToString();
            }
        }
    }
}