﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFIle = "~/MasterPage.master" 
    Inherits="DSNY.DSNYCP.DS249.Login" Title="DSNY : DS-249 COMPLAINTS" CodeBehind="Login.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" type="text/javascript">

        $.when($.ready).then(function () 
        {
            hideLoader();
        });


        $("form").live("submit", function ()
        {
            // Strongly recommended: Hide loader after 20 seconds, even if the page hasn't finished loading
            setTimeout(hideLoader, 60 * 1000);
            ShowProgress();
        });

    </script>    


</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style=" top: 120px; margin-top: 10px">
        <table width="832px" border="0" cellspacing="0" cellpadding="2" align="center">
            <tr>
                <td colspan="3" class="headertext">
                    DSNY: DS-249 COMPLAINTS<br />
                </td>
            </tr>
        </table>
        <table  width="832px" border="0" cellspacing="0" cellpadding="2" align="center">
            <tr>
                <td colspan="3" align="center">
                    <table class="loginBox" cellpadding="14" align="center">
                        <tr>
                            <td>
                                PLEASE SIGN IN BELOW WITH
                                <br />
                                YOUR ID AND PASSWORD
                            <br />
                            <br />
                          </td>

                        </tr>
                        <tr class="loginBoxInternal" align="left">
                            <td>
                                <asp:Label ID="lMessage" runat="server" Font-Size="Small"></asp:Label>
                                <br />
                                Sign in ID*<br />
                                <asp:TextBox ID="txtUserName" runat="server" Width="275"></asp:TextBox><br />
                                <br />
                                Password*<br />
                                <asp:TextBox ID="txtPassword" runat="server" Width="275" TextMode="Password"></asp:TextBox><br />
                                <br />
                                <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/includes/img/login.png"
                                    OnClick="btnLogin_Click" />
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="ForgotPasswordLinkButton" runat="server" Text="Forgot your password?"
                                    OnClick="ForgotPasswordLinkButton_Click" CausesValidation="false"></asp:LinkButton>
                                <br />
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Version: <span style="font-family: Arial; font-size: 10pt">
                                    3.0.1<br />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;06/30/2012</span>
                            </td>
                        </tr>
                    </table>
                    <table class="loginBoxLower" align="center">
                        <tr>
                            <td>
                                Need Help? Please contact IT Service Desk <br />at (212) 291-1111 or email us <br />at itServiceDesk@dsny.nyc.gov
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>    
</asp:Content>
    