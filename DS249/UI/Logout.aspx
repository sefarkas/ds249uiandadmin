﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  Inherits="DSNY.DSNYCP.DS249.Logout" Title="Logout" Codebehind="Logout.aspx.cs" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="815" border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td colspan="3" class="headertext">
                LOG OUT<br />               
            </td>
        </tr>
        <tr>
            <td colspan="3" class="headertext">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table class="loginBox" cellpadding="14">
                    <tr>
                        <td>
                            You have successfully logged out
                        </td>
                        <br />
                        <br />
                    </tr>
                    <tr align="center">
                        <td align="center">
                            Click here to
                            <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/includes/img/btn_login.gif"
                                OnClick="btnLogin_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

