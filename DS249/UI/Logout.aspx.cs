﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
/// <summary>
/// This class provide functionality to clear the session and redirect the user to logout page
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class Logout : System.Web.UI.Page
    {
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["IsAuthenticated"] = "false";
            Session["UserRoles"] = null;
            Session.RemoveAll();
            FormsAuthentication.SignOut();
        }

        /// <summary>
        /// This method redirect the user to logout page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../UI/Login.aspx");
        }
    }
}