﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provides the functionality to send the temp password to the user. 
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UI_ForgotPassword : System.Web.UI.Page
    {
        /// <summary>
        /// Define default min and max password lengths.
        /// Define supported password characters divided into groups.
        //  You can add (or remove) characters to (from) these groups.
        /// </summary>

        private static int DEFAULT_MIN_PASSWORD_LENGTH = 8;
        private static int DEFAULT_MAX_PASSWORD_LENGTH = 10;


        private static string PASSWORD_CHARS_LCASE = "abcdefgijkmnopqrstwxyz";
        private static string PASSWORD_CHARS_UCASE = "ABCDEFGHJKLMNPQRSTWXYZ";
        private static string PASSWORD_CHARS_NUMERIC = "23456789";

       
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ForgotPasswordTable.Visible = true;
                confirmationtable.Visible = false;
            }
        }
        /// <summary>
        /// This method resets the password and send the information to the user via email
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResetPasswordButton_Click(object sender, ImageClickEventArgs e)
        {
            String rndmPass = String.Empty;
            User usr = new User();
            Admin adminSvc = new Admin();


            usr.EmailId = EmailIDTextBox.Text.Trim();
            usr.ValidateUserEmail();
            if (usr.UserId != String.Empty)
            {
                rndmPass = GenerateRandomPassword(8, 8);
                usr.Password = rndmPass;
                usr.IsTempPassword = true;
                usr.Save(false);

                adminSvc.SendResetPasswordEmail(usr.UserId, usr.Password, usr.EmailId);
                ForgotPasswordTable.Visible = false;
                confirmationtable.Visible = true;
                ConfirmationLabel.Text = "Temporary Password has been emailed to " + usr.EmailId;

            }
            else
            {
                lMessage.Text = "Please contact you Admin to create an account for you.";
            }
        }

        /// <summary>
        /// Generates a random password.
        /// Randomly generated password.
        /// The length of the generated password will be determined at
        /// random. It will be no shorter than the minimum default and
        /// no longer than maximum default.
        /// </summary>
        /// <returns>
        private string Generate()
        {
            return GenerateRandomPassword(DEFAULT_MIN_PASSWORD_LENGTH,
                            DEFAULT_MAX_PASSWORD_LENGTH);
        }

        /// <summary>
        /// Generates a random password of the exact length.
        /// </summary>
        /// <param name="length">
        /// Exact password length.
        /// </param>
        /// <returns>
        /// Randomly generated password.
        /// </returns>
        private string Generate(int length)
        {
            return GenerateRandomPassword(length, length);
        }

        /// <summary>
        /// Generates a random password.
        /// Minimum password length.
        /// Maximum password length.
        /// Randomly generated password.
        /// The length of the generated password will be determined at
        /// random and it will fall with the range determined by the
        /// function parameters.
        /// This function also provides the following
        /// This array will hold password characters.
        /// Allocate appropriate memory for the password.
        /// Index of the next character to be added to password.
        /// Index of the next character group to be processed.
        /// Index which will be used to track not processed character groups.
        /// Index of the last non-processed character in a group.
        /// </remarks>
        private string GenerateRandomPassword(int minLength,
                                      int maxLength)
        {

            if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                return null;


            char[][] charGroups = new char[][] 
        {
            PASSWORD_CHARS_LCASE.ToCharArray(),
            PASSWORD_CHARS_UCASE.ToCharArray(),
            PASSWORD_CHARS_NUMERIC.ToCharArray(),
            
        };


            int[] charsLeftInGroup = new int[charGroups.Length];


            for (int i = 0; i < charsLeftInGroup.Length; i++)
                charsLeftInGroup[i] = charGroups[i].Length;

            int[] leftGroupsOrder = new int[charGroups.Length];

            for (int i = 0; i < leftGroupsOrder.Length; i++)
                leftGroupsOrder[i] = i;


            byte[] randomBytes = new byte[4];


            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);


            int seed = (randomBytes[0] & 0x7f) << 24 |
                        randomBytes[1] << 16 |
                        randomBytes[2] << 8 |
                        randomBytes[3];


            Random random = new Random(seed);


            char[] password = null;


            if (minLength < maxLength)
                password = new char[random.Next(minLength, maxLength + 1)];
            else
                password = new char[minLength];


            int nextCharIdx;


            int nextGroupIdx;


            int nextLeftGroupsOrderIdx;


            int lastCharIdx;


            int lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

            for (int i = 0; i < password.Length; i++)
            {


                if (lastLeftGroupsOrderIdx == 0)
                    nextLeftGroupsOrderIdx = 0;
                else
                    nextLeftGroupsOrderIdx = random.Next(0,
                                                         lastLeftGroupsOrderIdx);

                nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];

                lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;

                if (lastCharIdx == 0)
                    nextCharIdx = 0;
                else
                    nextCharIdx = random.Next(0, lastCharIdx + 1);

                password[i] = charGroups[nextGroupIdx][nextCharIdx];

                if (lastCharIdx == 0)
                    charsLeftInGroup[nextGroupIdx] =
                                              charGroups[nextGroupIdx].Length;
                else
                {
                    if (lastCharIdx != nextCharIdx)
                    {
                        char temp = charGroups[nextGroupIdx][lastCharIdx];
                        charGroups[nextGroupIdx][lastCharIdx] =
                                    charGroups[nextGroupIdx][nextCharIdx];
                        charGroups[nextGroupIdx][nextCharIdx] = temp;
                    }
                    charsLeftInGroup[nextGroupIdx]--;
                }

                if (lastLeftGroupsOrderIdx == 0)
                    lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;
                else
                {
                    if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                    {
                        int temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                        leftGroupsOrder[lastLeftGroupsOrderIdx] =
                                    leftGroupsOrder[nextLeftGroupsOrderIdx];
                        leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                    }
                    lastLeftGroupsOrderIdx--;
                }
            }

            return new string(password);
        }
        /// <summary>
        /// This method redirect the user to login-in page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnLogin_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../UI/Login.aspx");
        }
    }
}