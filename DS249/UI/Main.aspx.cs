﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// This class provide functionality to redirect the user based on their membership roles to their websites
/// </summary>

namespace DSNY.DSNYCP.DS249
{
    public partial class UI_Default : System.Web.UI.Page
    {
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            string script = "$(document).ready(function () { $('[id*=btnSubmit]').click(); });";

            // Define the name and type of the client scripts on the page.
            String csname1 = "load";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            // Check to see if the startup script is already registered.
            if (!cs.IsStartupScriptRegistered( cstype, csname1 ))
            {
                cs.RegisterStartupScript( cstype, csname1, script, true );
            }

            Common.RedirectToDefaultPage();
        }
    }
}