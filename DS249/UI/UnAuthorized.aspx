<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  Inherits="DSNY.DSNYCP.DS249.UnAuthorized" Title="Untitled Page" Codebehind="UnAuthorized.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table width="900px" border="0" cellspacing="0" cellpadding="2" class="tableMargin" align="center">
        <tr>
            <td colspan="3" class="headertext">
                NO AUTHORIZATION<br />
                <asp:Image runat="server" ImageUrl="~/includes/img/border_top.gif" />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <table class="" cellpadding="14" align="center">
                    <tr>
                        <td align="right">
                           
                        </td>
                        <br />
                        <br />
                    </tr>
                    <tr class="loginBoxInternal" align="left">
                        <td>
                            <table class="loginBox" cellpadding="14">
                                <tr>
                                    <td>
                                        Sorry. You are not authorized to view this page.
                                    </td>
                                    <br />
                                    <br />
                                </tr>
                                <tr class="loginBoxInternal" align="center">
                                    <td>
                                        Click here to go back to <a href="#" onclick="javascript:window.navigate('login.aspx');">login</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table class="loginBoxLower" align="center">
                    <tr>
                        <td>
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                    itServiceDesk@dsny.nyc.gov</a></span>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

