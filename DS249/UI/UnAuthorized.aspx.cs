﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
/// <summary>
/// This class provides functionality for unauthorised users to redirect to logout page. 
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class UnAuthorized : System.Web.UI.Page
    {
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ImageButton btnLogout;

            btnLogout = (ImageButton)Master.FindControl("btnlogout");
            btnLogout.Visible = false;

        }
       
    }
}