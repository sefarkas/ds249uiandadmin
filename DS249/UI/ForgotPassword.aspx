﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.DS249.UI_ForgotPassword" Title="DSNY : DS-249 FORGOT PASSWORD"
    CodeBehind="ForgotPassword.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="832px" border="0" cellspacing="0" cellpadding="2" align="center">
        <tr>
            <td colspan="3" class="headertext">
                DSNY: FORGOT PASSWORD
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="3" align="center">
                <br />
                <table id="ForgotPasswordTable" runat="server" class="loginBox" cellpadding="14"
                    align="center">
                    <tr>
                        <td>
                            PLEASE ENTER YOUR DSNY EMAIL ID
                        </td>
                    </tr>
                    <tr class="loginBoxInternal" align="left">
                        <td>
                            <asp:Label ID="lMessage" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
                            <br />
                            <br />
                            Email ID*<br />
                            <asp:TextBox ID="EmailIDTextBox" runat="server" Width="275"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="EmailRequiredFieldValidator" runat="server" ErrorMessage="Please Enter Email ID."
                                ControlToValidate="EmailIDTextBox"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regEmail" ControlToValidate="EmailIDTextBox"
                                Text="Invalid Email" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                runat="server" />
                            <br />
                            <br />
                            <asp:ImageButton ID="ResetPasswordButton" runat="server" ImageUrl="~/includes/img/btn_reset.gif"
                                OnClick="ResetPasswordButton_Click" />
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
                <table id="confirmationtable" runat="server" class="loginBox" cellpadding="14" visible="false">
                    <tr>
                        <td>
                            <asp:Label ID="ConfirmationLabel" runat="server" Font-Size="Small"></asp:Label>
                            <br />
                            <br />
                        </td>
                    </tr>
                    <tr align="center">
                        <td align="center">
                            Click here to
                            <asp:ImageButton ID="btnLogin" runat="server" ImageUrl="~/includes/img/btn_login.gif"
                                OnClick="btnLogin_Click" />
                            Screen.
                        </td>
                    </tr>
                </table>
                <table class="loginBoxLower" align="center">
                    <tr>
                        <td>
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at 212.291.1111 or email us
                                at <a href="mailto:itServiceDesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                    itServiceDesk@dsny.nyc.gov</a></span>

                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
