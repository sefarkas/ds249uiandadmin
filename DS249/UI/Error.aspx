﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.DS249.UI_Error" CodeBehind="Error.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="width: 900px; height: 30px;" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td>
                <table>
                    <tr>
                        <td valign="top">
                            <span class="infoStyleGreen1">Need Help? </span><span class="infoStyleGreen1"></span>
                            <span class="infoStyleGreen2">Please contact IT Service Desk at (212) 291-1111 or email us
                                at <a href="mailto:itServicedesk@dsny.nyc.gov" title="Send us an email" class="infoStyleGreen1">
                                    itServicedesk@dsny.nyc.gov</a></span>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="styleHeading">
                <div>
                    <span>ERROR</span></div>
            </td>
        </tr>
    </table>
    <table align="center" width="80%">
        <tr>
            <td class="todate">
                <br />
                <br />
                Opps...!!! An unexpected response has occured In a program. Please contact IT Service Desk with <br />
                <br />             
                <asp:Label ID="lblErrorID" runat="server"></asp:Label>
            </td>
        </tr>
       
    </table>
</asp:Content>
