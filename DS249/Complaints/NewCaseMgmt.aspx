﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.DS249.Complaints_NewCaseMgmt" Theme="DSNY" CodeBehind="NewCaseMgmt.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .tdNonEdit
        {
            width: 25%;
            border-bottom: solid 1px;
            background-color: Silver;
        }
        
        .tdEdit
        {
            width: 25%;
            border-bottom: solid 1px;
        }
        
        .innerTable
        {
            width: 95%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <table style="width: 955px">
            <!-- Application title & document flow-->
            <tr>
                <td style="width: 100%">
                    <table class="innerTable">
                        <tr>
                            <td align="left">
                                Application Tabs
                            </td>
                            <td align="right">
                                Document Flow
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Page Title -->
            <tr>
                <td style="width: 100%">
                    <table class="innerTable">
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label23" runat="server" Text="DS249 COMPLAINT :: NEW COMPLAINT" SkinID="HeaderLabel"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:Label ID="Label16" runat="server" Text="EMPLOYEE SEARCH" SkinID="frmLabel"></asp:Label>&nbsp;&nbsp;
                                <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Control Buttons -->
            <tr>
                <td style="width: 100%">
                    <table class="innerTable">
                        <tr>
                            <td align="left">
                                <asp:ImageButton ID="ibComplaintList" runat="server" ImageUrl="~/includes/img/btn_caselist.gif" />
                                <asp:ImageButton ID="ibApprove" runat="server" ImageUrl="~/includes/img/btn_approve.gif" />
                            </td>
                            <td align="right">
                                <asp:ImageButton ID="ibSave" runat="server" ImageUrl="~/includes/img/btn_save.gif" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Main Form table -->
            <tr>
                <td>
                    <table class="innerTable" cellpadding="5px" cellspacing="0px">
                        <tr>
                            <td colspan="4" style="background-color: Green">
                                &nbsp;
                            </td>
                        </tr>
                        <tr style="background-color: Silver">
                            <td>
                                &nbsp;
                            </td>
                            <td colspan="2" style="height: 25px">
                                <asp:Label ID="lbMessage" runat="server" Text="Feedback Status Message" SkinID="ErrorLabel"></asp:Label>
                            </td>
                            <td align="right">
                                <asp:ImageButton ID="ibEdit" runat="server" ImageUrl="~/includes/img/btn_edit.gif" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background-color: Green">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label1" runat="server" Text="DATE" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbDate" runat="server" Text="10/14/2009"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label2" runat="server" Text="INDEX NO." SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbIndexNo" runat="server" Text="XXXXX"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label3" runat="server" Text="BADGE" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbBadge" runat="server" Text="030562"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label5" runat="server" Text="REFERENCE NO." SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="Label6" runat="server" Text="494517"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label4" runat="server" Text="NAME(LAST)" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbLName" runat="server" Text="HOLSTER"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label8" runat="server" Text="(FIRST)" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbFName" runat="server" Text="KEVIN"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label10" runat="server" Text="MI" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbMName" runat="server" Text="S"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label12" runat="server" Text="DATE OF BIRTH" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbDob" runat="server" Text="10/14/1976"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label7" runat="server" Text="STREET NO." SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbStreetNo" runat="server" Text="20-698"></asp:Label>
                            </td>
                            <td colspan="2" align="left" class="tdNonEdit">
                                <asp:Label ID="Label11" runat="server" Text="STREET NAME" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbStreetName" runat="server" Text="LAKE BALDWIN DR."></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label14" runat="server" Text="APT" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbApt" runat="server" Text="4B"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label9" runat="server" Text="APPOINTMENT DATE" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbApptDate" runat="server" Text="02/16/1986"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label15" runat="server" Text="&nbsp;" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbVacationSch" runat="server" Text="23, 26, 25, 29"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label17" runat="server" Text="PAYROLL LOCATION" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbPayLoc" runat="server" Text="S0117"></asp:Label>
                            </td>
                            <td align="left" class="tdNonEdit">
                                <asp:Label ID="Label19" runat="server" Text="CHART DAY NO." SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:Label ID="lbChartDay" runat="server" Text="O-004"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" class="tdEdit">
                                <asp:Label ID="Label13" runat="server" Text="DISTRICT / DIVISION" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:DropDownList ID="ddlDistrict" runat="server" SkinID="dropdowns">
                                </asp:DropDownList>
                            </td>
                            <td colspan="2" align="left" class="tdEdit">
                                <asp:Label ID="Label18" runat="server" Text="BOROUGH" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:DropDownList ID="ddlBorough" runat="server" SkinID="dropdowns">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="bggreen2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Charge Control-->
            <tr>
                <td colspan="4">
                    <table class="innerTable" cellpadding="5px" cellspacing="0px">
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label20" runat="server" Text="RULES AND ORDERS VIOLATED AS FOLLOWS"
                                    SkinID="frmLabel"></asp:Label>
                                <asp:ImageButton ID="ibAddCharge" runat="server" ImageUrl="~/includes/img/btn_addV2.gif" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder ID="phVoilations" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="4" align="left">
                                                <asp:Label ID="Label26" runat="server" Text="CHARGES" SkinID="frmLabel"></asp:Label>
                                                <br />
                                                <asp:TextBox ID="TextBox1" runat="server" Width="850px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" class="tdEdit">
                                                <asp:Label ID="Label27" runat="server" Text="LOCATION" SkinID="frmLabel"></asp:Label>
                                                <br />
                                                <asp:DropDownList ID="DropDownList1" runat="server" SkinID="ddSingle">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" class="tdEdit">
                                                <asp:Label ID="Label28" runat="server" Text="INCIDENT TYPE" SkinID="frmLabel"></asp:Label><br />
                                                <asp:DropDownList ID="DropDownList2" runat="server" SkinID="ddSingle">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" class="tdEdit">
                                                <asp:Label ID="Label29" runat="server" Text="INCIDENT DATE" SkinID="frmLabel"></asp:Label><br />
                                                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                            </td>
                                            <td class="tdEdit">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Comments -->
            <tr>
                <td colspan="4">
                    <table class="innerTable" cellpadding="5px" cellspacing="0px">
                        <tr>
                            <td colspan="4" align="left">
                                <asp:Label ID="Label30" runat="server" Text="COMMENTS" SkinID="frmLabel"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtComments" runat="server" Width="895px"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Complainant Control -->
            <tr>
                <td colspan="4">
                    <table class="innerTable" cellpadding="5px" cellspacing="0px">
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label21" runat="server" Text="COMPLAINANT'S STATEMENT(SPECIFY ACT WHICH CONSTITUTE VOILATIONS)"
                                    SkinID="frmLabel"></asp:Label>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/includes/img/btn_addV2.gif" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:PlaceHolder ID="pnComplainant" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:Label ID="Label32" runat="server" Text="NAME" SkinID="frmLabel"></asp:Label>
                                                <br />
                                                <asp:TextBox ID="TextBox5" runat="server" Width="425PX"></asp:TextBox>
                                            </td>
                                            <td align="left" class="tdEdit">
                                                <asp:Label ID="Label33" runat="server" Text="LOCATION" SkinID="frmLabel"></asp:Label><br />
                                                <asp:DropDownList ID="DropDownList4" runat="server" SkinID="ddSingle">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="left" class="tdEdit">
                                                <asp:Label ID="Label34" runat="server" Text="TITLE" SkinID="frmLabel"></asp:Label><br />
                                                <asp:DropDownList ID="DropDownList5" runat="server" SkinID="ddSingle">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" align="left">
                                                <br />
                                                <asp:Label ID="Label35" runat="server" Text="STATEMENT" SkinID="frmLabel"></asp:Label>
                                            </td>
                                            <td colspan="2" align="right">
                                                <br />
                                                TEMPLATE &nbsp; SPELL CHECK
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="left">
                                                <asp:TextBox ID="TextBox4" runat="server" Width="895px" TextMode="MultiLine"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <!-- Witness Control & Probation Check box -->
            <tr>
                <td colspan="4">
                    <table class="innerTable" cellpadding="5px" cellspacing="0px">
                        <tr>
                            <td align="left" style="width: 50%">
                                <asp:Label ID="Label22" runat="server" Text="WITNESS" SkinID="frmLabel"></asp:Label>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/includes/img/btn_addV2.gif" />
                            </td>
                            <td rowspan="2" align="left">
                                <asp:CheckBox ID="cbProbation" runat="server" Text="CHECK IF EMPLOYEE UNDER PROBATION"
                                    SkinID="frmCheckBox" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%">
                                <asp:PlaceHolder ID="pnWitnesses" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="TextBox3" runat="server" Width="425PX"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:PlaceHolder>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" class="bggreen2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="innerTable" cellpadding="5px" cellspacing="0px">
                        <tr>
                            <td align="left">
                                <asp:Label ID="Label24" runat="server" Text="HISTORY ::" SkinID="frmLabel"></asp:Label>
                                <asp:Label ID="lbHistoryName" runat="server" Text="KEVIN HOLSTER" SkinID="frmLabel"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <!-- Gridview for History Section -->
                                <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" BorderColor="#235705"
                                    EmptyDataText="No Voilation History Found" HeaderStyle-BackColor="#a4d49a" HeaderStyle-Font-Bold="true"
                                    Width="900px">
                                    <EmptyDataRowStyle CssClass="fromdate" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="INDEX NO">
                                            <ItemTemplate>
                                                <a href id="lkIndex" href="#" onclick="OpenModalWindow('ViewComplaintDetails.aspx?ID=<%# Eval("ComplaintID")%>&Mode=VIEW');return false;"
                                                    style="font-size: 1.0em;">
                                                    <%# Eval("ComplaintID")%></a>
                                            </ItemTemplate>
                                            <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em"
                                                HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px" ForeColor="Black"
                                                Font-Underline="False" Wrap="False" Width="80px" />
                                            <ItemStyle BorderColor="#235705" BackColor="#A4D49A" BorderWidth="1px" Font-Size="1.0em"
                                                HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ComplaintID" DataFormatString="" HeaderText="INDEX NO">
                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                VerticalAlign="Middle" Width="150px" Wrap="False" />
                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                Font-Bold="false" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="VoilationDateAsString" DataFormatString="" HeaderText="VOILATION DATE">
                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                VerticalAlign="Middle" Width="150px" Wrap="False" />
                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                Font-Bold="false" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Voilations" HeaderText="CHARGES">
                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                VerticalAlign="Middle" Width="80px" Wrap="False" />
                                            <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="80px"
                                                Font-Bold="false" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="VoilationDesc">
                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                VerticalAlign="Middle" Width="180px" Wrap="False" />
                                            <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="180px"
                                                Font-Bold="false" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Penalty" DataFormatString="" HeaderText="PENALTY">
                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                VerticalAlign="Middle" Width="180px" Wrap="False" />
                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="180px"
                                                Font-Bold="false" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="ibViewHistory" runat="server" ImageUrl="~/includes/img/btn_view.gif" />
                                            </ItemTemplate>
                                            <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em"
                                                HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px" ForeColor="Black"
                                                Font-Underline="False" Wrap="False" Width="80px" />
                                            <ItemStyle BorderColor="#235705" BackColor="#A4D49A" BorderWidth="1px" Font-Size="1.0em"
                                                HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <HeaderStyle BackColor="#A4D49A" Font-Bold="True" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="innerTable" cellpadding="5px" cellspacing="0px">
                        <tr style="background-color: Silver">
                            <td>
                                &nbsp;
                            </td>
                            <td colspan="2" style="height: 25px">
                                <asp:Label ID="Label25" runat="server" Text="Feedback Status Message" SkinID="ErrorLabel"></asp:Label>
                            </td>
                            <td align="right">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 100%">
                    <table class="innerTable">
                        <tr>
                            <td align="left">
                                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/includes/img/btn_caselist.gif" />
                                <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/includes/img/btn_approve.gif" />
                            </td>
                            <td align="right">
                                <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/includes/img/btn_save.gif" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table class="innerTable" cellspacing="0px">
                        <tr style="background-color: Silver">
                            <td colspan="4" style="background-color: Green">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </center>
</asp:Content>
