﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  Inherits="DSNY.DSNYCP.DS249.Complaints_ApprovalConfirm" Codebehind="ApprovalConfirm.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            height: 18px;
        }
        .style2
        {
            width: 516px;
        }
    </style>
    
    <script language="javascript">
        function OpenPrint(IndexNo) {
            if (IndexNo != "") {
                var URL = "Print.aspx?Id=" + IndexNo;
               
                var dialogResults = window.open(URL, '');
            }
            else {
                alert("ComplaintID is Blank Or Missing");
            }
            return false;
        }
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <table style="width: 900px; height: 30px" class="tableMargin" border="0" cellpadding="0px"
    cellspacing="0px">
     <tr>
       <td>
        <table>
         <tr>
        <td valign="top">
          &nbsp;
        </td>
        <td>&nbsp;</td>
             </tr>
         </table>
         </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
   <tr>                                          
    <td class="styleHeading"><div><span>DS-249 COMPLAINT (01-09) </span></div></td>    
   </tr>
    <tr><td>&nbsp;</td></tr>
</table>
    <table id="form1:panel0" class="tableMargin" border="0" cellpadding="0" cellspacing="0" width="900px">
        <tbody>
            <tr>
                <td>
                    <table style="width: 401px">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:ImageButton ID="ibList" runat="server" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                                        OnClick="ibList_Click" />
                                    <asp:ImageButton ID="ibNew" runat="server" ImageUrl="~/includes/img/ds249_btn_new_static.png"
                                        OnClick="ibNew_Click" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <table border="0" width="300px">
                        <tbody>
                            <tr>
                                <td>
                                    &nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="tableMargin" border="0" cellpadding="0" cellspacing="0" width="900px">
    <tr>
            <td colspan="2">
             &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" style="background-image:url('..\includes\img\table_top.jpg'); background-repeat: repeat-x; font-family: Arial, Helvetica, sans-serif; color: #FFFFFF; font-weight: bold; font-size: small;" 
                class="style1">
             SUBMITTED SUCESSFULLY
            </td>
        </tr>
        <tr>
            <td colspan="2">
             &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2">
             &nbsp;
            </td>
        </tr>
        <tr>
            <td class="style2" 
                style="font-weight: bold; font-family: Arial, Helvetica, sans-serif; font-size: small; color: #000000">
            <font font-weight="bold">Confirmation Number: </font><asp:Label ID="lbIndexNo" runat="server" Text=""></asp:Label>
            </td>
            <td>
             <asp:ImageButton ID="ibPrint" runat="server" ImageUrl="~/includes/img/ds249_btn_print_static.png"
                                                CausesValidation="False" />
            </td>
        </tr>
    </table>    
    </asp:Content>

