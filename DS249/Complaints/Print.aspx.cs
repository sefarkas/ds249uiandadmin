﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;
using System.Globalization;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provides the functionality for events and methods for the DS249 complaints print page.
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class Complaints_Test : System.Web.UI.Page
    {       

        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            htmltopdf();
        }      

        /// <summary>
        /// This method converts the html content to pdf format.
        /// </summary>
        private void htmltopdf()
        {
            
            String ID = Request.QueryString["id"];
            PdfConverter pdfConverter = new PdfConverter();
            pdfConverter.LicenseKey = "MhkAEgAAEgMCAgMSBBwCEgEDHAMAHAsLCws=";
            pdfConverter.ScriptsEnabled = true;
            pdfConverter.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfConverter.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfConverter.PdfDocumentOptions.ShowHeader = false;
            pdfConverter.PdfDocumentOptions.ShowFooter = true;
            pdfConverter.PdfDocumentOptions.LeftMargin = 10;
            pdfConverter.PdfDocumentOptions.RightMargin = 10;
            pdfConverter.PdfDocumentOptions.TopMargin = 15;
            pdfConverter.PdfDocumentOptions.BottomMargin = 0;
            pdfConverter.PdfFooterOptions.DrawFooterLine = false;
            pdfConverter.PdfFooterOptions.PageNumberText = "Page";
            pdfConverter.PdfFooterOptions.ShowPageNumber = true;
            pdfConverter.PdfFooterOptions.PageNumberTextFontName = "verdana";
            String URL = String.Format("{0}{1}{2}printcomplaint.aspx?Id={3}", Request.Url.GetLeftPart(UriPartial.Authority), Request.Url.Segments[0], Request.Url.Segments[1], ID);
            byte[] downloadBytes = pdfConverter.GetPdfFromUrlBytes(URL);            
            Response.ClearHeaders();
            Response.ClearContent();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Length", downloadBytes.Length.ToString(
            CultureInfo.CurrentCulture));
            Response.AddHeader("Content-Disposition", "inline");
            Response.BinaryWrite(downloadBytes);
            Response.End();
        }

    }







}