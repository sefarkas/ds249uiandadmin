<%@ Page Title="Complaint Listing" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
     Inherits="DSNY.DSNYCP.DS249.Complaints_Dashboard" CodeBehind="Dashboard.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register src="~/UserControl/Pager.ascx" tagname="Pager" tagprefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .style1
        {
            width: 125px;
        }
        #Panel1 TABLE
        {
            border: #060F40 2px solid;
            color: #060F40;
            background: #ffffcc;
            text-align:left;
        }
        .pnReports
        {
            border: #060F40 2px solid;
            color: #060F40;
            background: #ffffcc;
        }
        .inputFieldStyle
        {
            margin-left: 0px;
        }
    </style>

    <script language="javascript" type="text/javascript">


        $.when($.ready).then(function () 
        {
            hideLoader();
        });


        $("form").live("click", function ()
        {
            // Strongly recommended: Hide loader after 20 seconds, even if the page hasn't finished loading
            setTimeout(hideLoader, 1 * 1000);
            ShowProgress();
        });
    </script>    


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>

        <asp:UpdatePanel ID="upButtons" runat="server">
            <ContentTemplate>
                <table width="955px" border="0" cellpadding="0px" cellspacing="0px" align="center">
                    <tr>
                        <td class="headertext" align="left">
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="82" />
                            Complaints
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="25" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Image runat="server" SkinId="skidClear" width="15" />
                            <asp:ImageButton ID="btnRef" runat="server" Width="160" CausesValidation="False"
                                ImageUrl="~/includes/img/ds249_btn_complaint.png" OnClick="btnRef_Click" />
                            <asp:Image runat="server" SkinId="skidClear" width="15" />
                            <asp:ImageButton ID="btnNew" ImageUrl="~/includes/img/ds249_btn_new_static.png"
                                runat="server" OnClick="btnNew_Click" CausesValidation="False" />
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="416" />                            
                            <asp:ImageButton ID="ibReports" runat="server" ImageUrl="~/includes/img/ds249_btn_report_static.png"
                                OnClientClick="JavaScript:return false;" />                           
                            <cc1:PopupControlExtender ID="ibReports_PopupControlExtender" runat="server" 
                                Enabled="True" ExtenderControlID="" PopupControlID="pnReportsMenu" Position="Bottom"
                                TargetControlID="ibReports">
                            </cc1:PopupControlExtender>
                        </td>
                    </tr>
                </table>
                <asp:Image runat="server" SkinId="skidClear" height="10" width="25" />
                <table width="955px" border="0" cellpadding="0px" cellspacing="0px">
                    <tr>
                        <td width="33%">
                        </td>
                        <td class="searchCase" align="left">
                            SEARCH FOR COMPLAINTS&nbsp;&nbsp;
                        </td>
                        <td class="style1" align="left">
                            <asp:TextBox ID="txtSearch" runat="server" Width="172px" Height="17px" 
                                CssClass="inputFieldStyle" ontextchanged="txtSearch_TextChanged"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnSearch" ImageUrl="~/includes/img/ds249_btn_search_static.png"
                                runat="server" OnClick="btnSearch_Click" />                            
                            <asp:ImageButton ID="btnSearchCancel" ImageUrl="~/includes/img/btn_cancel.gif"
                                runat="server" OnClick="btnSearchCancel_Click" CausesValidation="False" Visible="false" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table width="832" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
                <td class="dashStatuBar" height="20" align="center">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label ID="lbMessage" runat="server" CssClass="feedbackStatus" Width="50%"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                        runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                        <Animations>                                
                        <OnUpdated>
                            <FadeOut Duration="5.0" Fps="24" />
                        </OnUpdated>
                        </Animations>
                    </cc1:UpdatePanelAnimationExtender>
                </td>
            </tr>
            <tr>
                <td>

                    <table width="832" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                            <td>
                                <asp:Image runat="server" SkinID="skidTopCap" /></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upGrid">

                                    <ProgressTemplate>
                                        Please wait ...
                                    </ProgressTemplate>

                                </asp:UpdateProgress>
                                <!-- update panel -->
                                <asp:UpdatePanel ID="upGrid" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnRef" EventName="Click" />
                                    </Triggers>
                                    <ContentTemplate>

                                        <uc1:Pager ID="Pager1" runat="server" />
                                        <asp:GridView ID="gvComplaints" runat="server" Width="832px" AutoGenerateColumns="False"
                                            BorderColor="#235705" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a"
                                            AllowSorting="True" PageSize="50"
                                            AllowPaging="True" OnRowCommand="gvComplaints_RowCommand" OnRowDataBound="gvComplaints_RowDataBound"
                                            OnSorting="gvComplaints_Sorting"
                                            OnDataBound="gvComplaints_DataBound"
                                            OnRowCreated="gvComplaints_RowCreated" AlternatingRowStyle-BackColor="#ededed"
                                            EmptyDataText="Sorry. No Complaints Found."
                                            CellPadding="0">
                                            <EmptyDataRowStyle CssClass="fromdate" />

                                            <Columns>
                                                <asp:TemplateField HeaderText="!" SortExpression="Indicator">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgAlert" runat="server" Height="16px" />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="25px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Date" DataField="CreatedDate" SortExpression="CreatedDate"
                                                    DataFormatString="{0:MMM dd, yyyy}" HtmlEncode="false">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="95px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Index #" DataField="IndexNo" SortExpression="IndexNo">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="65px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ComplaintStatus" Visible="true">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="0px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="PrintButton" ImageUrl="~/includes/img/ds249_btn_dash_route_static.png"
                                                            runat="server" CommandName="Route" CausesValidation="false" Visible="false" CommandArgument='<%#Eval("ComplaintID") %>' />
                                                        <asp:ImageButton ID="EditButton" ImageUrl="~/includes/img/ds249_btn_edit_static.png"
                                                            runat="server" CommandName="Edit" CausesValidation="false" Visible="false" CommandArgument='<%#Eval("ComplaintID") %>' />
                                                        <asp:ImageButton ID="ViewButton" ImageUrl="~/includes/img/ds249_btn_view_static.png"
                                                            runat="server" CommandName="View" CausesValidation="false" Visible="false" CommandArgument='<%#Eval("ComplaintID") %>' />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="dashHeaderTxt" Font-Size="0.9em" Font-Names="Verdana, Geneva, sans-serif"
                                                        ForeColor="White" Font-Underline="False" Wrap="False" Width="80px" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="District" DataField="LocationName" SortExpression="LocationName">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="160px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Name(F. Last)" DataField="InitialAndLastName" SortExpression="LastName">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="120" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="NAME (F. Last)" DataField="LastName" SortExpression="LastName"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="0" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Charges">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbCharge" runat="server" Text="Label">
                                                        </asp:Label>
                                                        <div class="container">
                                                            <asp:Image ID="imgStatus" runat="server" ImageUrl="~/includes/img/ds249_mini_plus_btn.png" CssClass="image" />
                                                            <div class="overlay">
                                                                <asp:Image ID="imgStatusRO" runat="server" ImageUrl="~/includes/img/ds249_mini_plus_roll_btn.png" CssClass="imagero overlay" />
                                                            </div>
                                                        </div>
                                                        <!-- PopupControlExtender is looking to attach javascript to the roll-over button image to show a list of charges too big for the grid-cell -->
                                                        <cc1:PopupControlExtender ID="pceStatus" runat="server" DynamicServiceMethod="GetChargesList"
                                                            DynamicContextKey='<%#Eval("ComplaintID") %>' DynamicControlID="Panel1" TargetControlID="imgStatusRO"
                                                            PopupControlID="Panel1" Position="Bottom">
                                                        </cc1:PopupControlExtender>
                                                        <!-- second extender is necessary for IE11 because css opacity and hover are not respected in IE11 -->
                                                        <cc1:PopupControlExtender ID="PopupControlExtender1" runat="server" DynamicServiceMethod="GetChargesList"
                                                            DynamicContextKey='<%#Eval("ComplaintID") %>' DynamicControlID="Panel1" TargetControlID="imgStatus"
                                                            PopupControlID="Panel1" Position="Bottom">
                                                        </cc1:PopupControlExtender>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="dashHeaderTxt" Font-Size="0.9em" Font-Names="Verdana, Geneva, sans-serif"
                                                        ForeColor="White" Font-Underline="False" Wrap="False" Width="90px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Charge" HeaderText="CHARGES" SortExpression="CHARGES">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="0" />
                                                    <ItemStyle CssClass="dashLineRight" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Reference #" DataField="RefNo" SortExpression="RefNo">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="95px" />
                                                    <ItemStyle CssClass="dashRefLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Author" DataField="CreatedBy" SortExpression="CreatedBy">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="90px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Route" DataField="RoutingLocation" SortExpression="RoutingLocation">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="20px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="" DataField="Indicator" SortExpression="Indicator">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="20px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Route" SortExpression="RoutingLocation">
                                                    <ItemTemplate>
                                                        <asp:Image ID="imgRoute" runat="server" Height="16px" />
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="25px" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ComplaintID" />
                                                <asp:BoundField HeaderText="JT1" DataField="Jt1">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="65px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image runat="server" SkinID="skidBottomCap" />
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
        </table>
           </center>
        <asp:Panel ID="Panel1" runat="server">
        </asp:Panel>
        <asp:Panel ID="pnReportsMenu" runat="server" CssClass="pnReports">
            <table>
                <tr align="left">
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=1">Charges By Locations</a>
                    </td>
                </tr>
<%--                <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=2">Distribution of Charges</a>
                    </td>
                </tr>--%>
                <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=3">Max # of Charges</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=4">Complaints By Boros</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=5">Complaints_Report</a>
                    </td>
                </tr>
                 <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=6">Complaint_By_Title</a>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=7">Complaints_with_1.4_1.5_Charges</a>
                    </td>
                </tr>
                 <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=8">Employee_Privileges</a>
                    </td>
                </tr>
                 <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=9">30_60_90_Days_Report</a>
                    </td>
                </tr>    
                <tr>
                    <td align="left">
                        <a href="../Reports/DSNYReport.aspx?Id=10">Probation Employees</a>
                    </td>
                </tr>                
                  <tr>
                <td align="left">
                    <a href="../Reports/DSNYReport.aspx?Id=11">Pending complaints</a>
                </td>
            </tr>          
              <tr>
                <td align="left">
                    <a href="../Reports/DSNYReport.aspx?Id=12">BCAD_ADVOCATE_OPEN_Complaints</a>
                </td>
            </tr>        
            </table>
        </asp:Panel>
 
</asp:Content>

