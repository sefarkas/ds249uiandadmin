﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using DSNY.DSNYCP.ClassHierarchy;
/// <summary>
/// This class provides the functionality to route the complaint to the Borough approver
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class Complaints_RouteComplaint : System.Web.UI.Page
    {

        Personnel personnel;
        Complaint complaint;
        User user;
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            ImageButton1.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
            ImageButton1.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");

            ibPrint.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_print_roll.png'");
            ibPrint.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_print_static.png'");

            ImageButton7.Attributes.Add("onmouseout", "this.src='../includes/img/submit_btn.png'");
            ImageButton7.Attributes.Add("onmouseover", "this.src='../includes/img/submit_over_btn.png'");

            btnView.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_lg_view_roll.png'");
            btnView.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_lg_view_static.png'");

            Button1.Style.Add("display", "none");
            lbMessage.Text = String.Empty;

            user = (User)Session["clsUser"];
            if (user != null)
            {
                if (!user.IsInMemberShip(GroupName.DS249))
                    Response.Redirect("../UI/UnAuthorized.aspx");
                else
                {
                    user.CurrentMembership = GroupName.DS249;
                    SecurityUtility.AttachRolesToUser(user);
                }

                if (!User.IsInRole("Read") || !User.IsInRole("BoroApprover"))
                    Response.Redirect("../UI/UnAuthorized.aspx");
            }
            else
                Response.Redirect("../UI/Login.aspx");

            if (Session["IsAuthenticated"] == null)
            {
                Response.Redirect("../UI/login.aspx?Session=False");
            }
            if (Session["IsAuthenticated"].ToString() == "false")
            {
                Response.Redirect("../UI/login.aspx?Session=False");
            }

            if (!IsPostBack)
            {
                complaint = new Complaint();
                Session["Complaint"] = complaint;
                LoadComplaintData();
                lbDate.Text = complaint.CreatedDate.ToShortDateString();
            }

            ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
            String URL = String.Format("ViewComplaintDetails.aspx?ID={0}&Mode=VIEW", Request.QueryString["Id"].ToString());
            btnView.Attributes.Add("onclick", String.Format("return OpenModalWindow('{0}')", URL));
            DisplayButtonBasedOnPermission();
        }
        /// <summary>
        /// This method displays the button based on the membership role permissions
        /// </summary>
        private void DisplayButtonBasedOnPermission()
        {
            if (User.IsInRole("Print"))
                ibPrint.Visible = true;
            else
                ibPrint.Visible = false;
        }

        /// <summary>
        /// This method loads the DS249 complaints
        /// </summary>
        private void LoadComplaintData()
        {           
                Int64 ID = Convert.ToInt64(Request.QueryString["Id"]);
                complaint.Load(ID, RequestSource.DS249);
                if (complaint.ComplaintId != -1)
                {
                    BindComplaintData();
                    personnel = new Personnel();
                    personnel.Load(complaint.EmployeeId);
                    Session["Personnel"] = personnel;
                    BindPersonnelData();
                }
                else
                {
                    Response.Redirect("Dashboard.aspx");
                }           
        }

        /// <summary>
        /// This method binds the DS249 personnel data 
        /// </summary>
        private void BindPersonnelData()
        {
                hdnEmployeeID.Value = personnel.PersonnelId.ToString();
                lbFName.Text = personnel.FirstName.ToString();
                lbMName.Text = personnel.MiddleName.ToString();
                lbLName.Text = personnel.LastName.ToString();
                if (personnel.Dob != DateTime.MinValue)
                    lbDob.Text = personnel.Dob.ToShortDateString();
                lbRefNo.Text = personnel.ReferenceNumber.ToString();
                lbBadge.Text = personnel.BadgeNumber.ToString();
           
        }
        /// <summary>
        /// This method binds the DS249 complaint data 
        /// </summary>
        private void BindComplaintData()
        {
            
                lbIndexNo.Text = complaint.IndexNo.ToString();
                if (complaint.RoutingLocation.Trim() != String.Empty)
                    if (complaint.RoutingLocation.Trim() != "P")
                {
                    ddlRoutingLocation.Items.FindByValue(complaint.RoutingLocation.ToString()).Selected = true;
                    ddlRoutingLocation.Enabled = false;
                }

                if (complaint.ChargesServedByName != String.Empty)
                {
                    txtChargesServedBy.Text = complaint.ChargesServedByName.ToString();
                    hdnChargesServedByRefNo.Value = complaint.ChargesServedByRefNo.ToString();
                }
                if (complaint.ChargesServedDate != DateTime.MinValue)
                    txtChargesServedDt.Text = complaint.ChargesServedDate.ToShortDateString();

                if (complaint.SuspensionOrderedByName != String.Empty)
                {
                    txtSuspendedBy.Text = complaint.SuspensionOrderedByName.ToString();
                    hdnSuspendedByRefNo.Value = complaint.SuspensionOrderedByRefNo.ToString();
                }

                if (complaint.SuspensionOrderDate != DateTime.MinValue)
                    txtSuspendedDt.Text = complaint.SuspensionOrderDate.ToShortDateString();
                if (complaint.SuspensionLiftedByName != String.Empty)
                {
                    txtSuspensionBy.Text = complaint.SuspensionLiftedByName.ToString();
                    hdnSuspensionByRefNo.Value = complaint.SuspensionLiftedByRefNo.ToString();
                }
                if (complaint.SuspensionLiftedDate != DateTime.MinValue)
                    txtSuspensionDt.Text = complaint.SuspensionLiftedDate.ToShortDateString();
           
        }

        /// <summary>
        /// This method search fro the details of the employee based on the search criteria
        /// </summary>
        /// <param name="prefixText"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string[] SearchEmployee(string prefixText, int count)
        {            
                ArrayList arrEmp = new ArrayList();
                PersonnelList pList = new PersonnelList();
                pList.Load(prefixText);
                if (pList.List.Count > 0)
                {
                    foreach (Personnel personnel in pList.List)
                    {
                        arrEmp.Add(personnel.ByLastName + " " + personnel.ReferenceNumber);
                    }
                }
                return (string[])arrEmp.ToArray(typeof(string));           
        }
        /// <summary>
        /// This method saves the complaint information to the database.
        /// </summary>
        private void SaveComplaint()
        {           
                if (complaint == null)
                {
                    complaint = (Complaint)Session["Complaint"];
                    UpdateComplaint();
                    if (complaint.Save(RequestSource.DS249,user.PersonalId) == true)
                    {
                        ddlRoutingLocation.Enabled = false;
                        lbMessage.Text = "Complaint Information is Saved";
                        lbMessage.ForeColor = System.Drawing.Color.Green;
                    }
                }   
        }

        /// <summary>
        /// This method updates the complaint information to the database.
        /// </summary>
        private void UpdateComplaint()
        {           
                complaint.ChargesServedByRefNo = Convert.ToString(hdnChargesServedByRefNo.Value);
                if (txtChargesServedDt.Text != String.Empty)
                    complaint.ChargesServedDate = Convert.ToDateTime(txtChargesServedDt.Text);
                else
                    complaint.ChargesServedDate = DateTime.MinValue;
                complaint.SuspensionLiftedByRefNo = Convert.ToString(hdnSuspensionByRefNo.Value);
                if (txtSuspensionDt.Text != String.Empty)
                    complaint.SuspensionLiftedDate = Convert.ToDateTime(txtSuspensionDt.Text);
                else
                    complaint.SuspensionLiftedDate = DateTime.MinValue;
                complaint.SuspensionOrderedByRefNo = Convert.ToString(hdnSuspendedByRefNo.Value);
                if (txtSuspendedDt.Text != String.Empty)
                    complaint.SuspensionOrderDate = Convert.ToDateTime(txtSuspendedDt.Text);
                else
                    complaint.SuspensionOrderDate = DateTime.MinValue;
                complaint.RoutingLocation = Convert.ToString(ddlRoutingLocation.SelectedItem.Value);           
        }

        /// <summary>
        /// This is a text change event that fires when the value in a text box is changes 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void txtChargesServedBy_TextChanged(object sender, EventArgs e)
        {            
                String refNo = Common.ExtractNumbers(txtChargesServedBy.Text);
                if (refNo != String.Empty)
                {
                    txtChargesServedBy.Text = txtChargesServedBy.Text.Replace(refNo, "");
                    hdnChargesServedByRefNo.Value = refNo;
                }
                else
                {
                    hdnChargesServedByRefNo.Value = String.Empty;
                    txtChargesServedBy.Text = String.Empty;
                }           

        }
        /// <summary>
        /// This is a text change event that fires when the value in a text box is changes 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSuspendedBy_TextChanged(object sender, EventArgs e)
        {           
                String refNo = Common.ExtractNumbers(txtSuspendedBy.Text);
                if (refNo != String.Empty)
                {
                    txtSuspendedBy.Text = txtSuspendedBy.Text.Replace(refNo, "");
                    hdnSuspendedByRefNo.Value = refNo;
                }
                else
                {
                    hdnSuspendedByRefNo.Value = String.Empty;
                    txtSuspendedBy.Text = String.Empty;
                }           
        }
        /// <summary>
        /// This is a text change event that fires when the value in a text box is changes 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSuspensionBy_TextChanged(object sender, EventArgs e)
        {            
                String refNo = Common.ExtractNumbers(txtSuspensionBy.Text);
                if (refNo != String.Empty)
                {
                    txtSuspensionBy.Text = txtSuspensionBy.Text.Replace(refNo, "");
                    hdnSuspensionByRefNo.Value = refNo;
                }
                else
                {
                    hdnSuspensionByRefNo.Value = String.Empty;
                    txtSuspensionBy.Text = String.Empty;
                }          
        }
        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("dashboard.aspx");
        }
        /// <summary>
        /// This button click events that redirects thier respective module
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton7_Click(object sender, ImageClickEventArgs e)
        {            
                SaveComplaint();
                complaint.ComplaintStatus = 5;
                complaint.UpdateComplaintStatus();
                String routing = String.Empty; ;

                switch (complaint.RoutingLocation)
                {
                    case "A":
                        routing = "Advocate";
                        break;
                    case "B":
                        routing = "BCAD";
                        break;
                    case "M":
                        routing = "Medical";
                        break;
                    default:
                        break;
                }
               ScriptManager.RegisterStartupScript(ImageButton7, this.GetType(), "msg", String.Format("<script>alert('Complaint {0} Sent to {1}!');document.location ='Dashboard.aspx';</script>", complaint.IndexNo, routing), false);            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
        
    }
}