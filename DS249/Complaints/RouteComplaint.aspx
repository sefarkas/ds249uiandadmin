﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  Inherits="DSNY.DSNYCP.DS249.Complaints_RouteComplaint" Codebehind="RouteComplaint.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="../includes/css/styles1.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/CaseManagementStyle.css" rel="stylesheet" type="text/css" />      
    
    <script language=javascript type="text/javascript">

        function PerformPostBack(sender, e) {           
           
            $get('ctl00$ContentPlaceHolder1$Button1').click();
        }

        function OpenModalWindow(URL) {            
            window.showModalDialog(URL, 'Complaint History', 'dialogWidth:950px;dialogHeight:770px;scroll:yes');
            return false;
        }
       
        hasResult1 = 0;
        getttingResult1 = 0;

        // Function for Employee Search
        function shownev1() {
            hasResult1 = 1;
            getttingResult1 = 0;
            $get("ctl00_ContentPlaceHolder1_lbSearchMsg2").innerText = "";
        }

        function populatingev1() {
            getttingResult1 = 1;
            hasResult1 = 0;
            $get("ctl00_ContentPlaceHolder1_lbSearchMsg2").innerText = "Retreiving..";
        }

        function onhiding1() {

            if (getttingResult1 == 1) {
                if (hasResult1 == 0) {
                    $get("ctl00_ContentPlaceHolder1_lbSearchMsg2").innerText = "No Match Found";

                }
            }
        }


        hasResult2 = 0;
        getttingResult2 = 0;
        // Function for Employee Search
        function shownev2() {
            hasResult2 = 1;
            getttingResult2 = 0;
            $get("ctl00_ContentPlaceHolder1_lbSearchMsg3").innerText = "";
        }

        function populatingev2() {
            getttingResult2 = 1;
            hasResult2 = 0;
            $get("ctl00_ContentPlaceHolder1_lbSearchMsg3").innerText = "Retreiving..";
        }

        function onhiding2() {

            if (getttingResult2 == 1) {
                if (hasResult2 == 0) {
                    $get("ctl00_ContentPlaceHolder1_lbSearchMsg3").innerText = "No Match Found";

                }
            }
        }


        hasResult3 = 0;
        getttingResult3 = 0;
        // Function for Employee Search
        function shownev3() {
            hasResult3 = 1;
            getttingResult3 = 0;
            $get("ctl00_ContentPlaceHolder1_lbSearchMsg4").innerText = "";
        }

        function populatingev3() {
            getttingResult3 = 1;
            hasResult3 = 0;
            $get("ctl00_ContentPlaceHolder1_lbSearchMsg4").innerText = "Retreiving..";
        }

        function onhiding3() {

            if (getttingResult3 == 1) {
                if (hasResult3 == 0) {
                    $get("ctl00_ContentPlaceHolder1_lbSearchMsg4").innerText = "No Match Found";

                }
            }
        }

        function validatePage() {
            //Executes all the validation controls associated with group1 validaiton Group1.
            window.scrollTo(0, 0);
            var flag = Page_ClientValidate('Approve');
            if (flag)
            //Executes all the validation controls which are not associated with any validation group. 
                flag = Page_ClientValidate();
            return flag;
        }

        function OpenPrint(IndexNo) {
            if (IndexNo != "") {
                var URL = "Print.aspx?Id=" + IndexNo;                
                var dialogResults = window.open(URL, '');
            }
            else {
                alert("ComplaintID is Blank Or Missing");
            }
            return false;
        }
        
    </script>
    <style type="text/css">
        .style1
        {
            width: 743px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
<table style="width: 900px; height: 30px;" class="tableMargin" border="0" cellpadding="0px"
    cellspacing="0px" align="center" >
    <tr>
    <td>
     <table style="width: 900px; height: 30px;" class="tableMargin" border="0" cellpadding="0px"
    cellspacing="0px" align="center" >
    
    <tr><td>&nbsp;</td></tr>
   <tr>                                          
    <td class="styleHeading"><div><span>DS-249 COMPLAINT ROUTING </span></div></td>    
   </tr>
</table>
    </td>
    </tr>
    
    
    <tr>
    <td>
    <table style="width: 900px; height: 50px" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr>
            <td class="style1" align="left">
                <table border="0" style="width: 118%" >
                    <tbody>
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                    <ContentTemplate>
                                        &nbsp;&nbsp;
                                        <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                                            CausesValidation="False" onclick="ImageButton1_Click" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:ImageButton ID="ImageButton7" runat="server" 
                                            ImageUrl="~/includes/img/submit_btn.png" onclick="ImageButton7_Click" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:ImageButton ID="ibPrint" runat="server" ImageUrl="~/includes/img/ds249_btn_print_static.png"
                                            CausesValidation="false"/>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:ImageButton ID="btnView" runat="server" 
                                            ImageUrl="~/includes/img/ds249_btn_lg_view_static.png" 
                                            style="width: 160px" />
                                        <asp:Button ID="Button1" runat="server" Text="Button" 
                                            CausesValidation="False" onclick="Button1_Click" Width="16px" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>              
        </tr>
        <tr>
        <td align="center">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                    <br />
                        <asp:Label ID="lbMessage" runat="server" Width="80%" CssClass="feedbackStatus"></asp:Label>
                        
                    </ContentTemplate>
                </asp:UpdatePanel>
                <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                    runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                    <Animations>                                
                        <OnUpdated>
                            <FadeOut Duration="5.0" Fps="24" />
                        </OnUpdated>
                    </Animations>
                </cc1:UpdatePanelAnimationExtender>
            </td>
        </tr>
    </table>
    </td>
    </tr>
    
    
    <tr>
    <td>
     <asp:UpdatePanel ID="upMain" runat="server">
    <ContentTemplate>
    
    <table style="width: 900px; height: 50px" class="tableMargin" border="0" cellpadding="0px"
        cellspacing="0px" align="center">
        <tr class="bordergreen">
            <td class="tdReadOnly">
             <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                               DATE
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Label ID="lbDate" runat="server" Class="labelStyle"  cssClass="inputFieldStyle"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>            
            </td>            
            <td class="tdReadOnly">
             <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                               INDEX NO.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Label ID="lbIndexNo" runat="server" Class="labelStyle"  cssClass="inputFieldStyle"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>             
            </td>
            
            <td class="tdReadOnly">
             <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                               BADGE
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbBadge" runat="server" Class="labelStyle"  cssClass="inputFieldStyle"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>  
            </td>
            <td class="tdReadOnly">
            <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                               REFERENCE NO.
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <asp:Label ID="lbRefNo" runat="server" Class="labelStyle"  cssClass="inputFieldStyle"></asp:Label>
                                <asp:HiddenField ID="hdnEmployeeID" runat="server" />
                               
                            </td>
                        </tr>
                    </tbody>
                    
                </table>  
            </td>
        </tr>
        <tr>
        <td colspan="4" class="bordergreen">
        <hr />
        </td>
        </tr>
        <tr class="bordergreen">
            <td class="tdReadOnly">
                <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                                NAME (LAST)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Label ID="lbLName" runat="server" Class="labelStyle"  cssClass="inputFieldStyle"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>           
            </td>
            
            <td class="tdReadOnly">
                <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                                (FIRST)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                  <asp:Label ID="lbFName" runat="server" Class="labelStyle"  cssClass="inputFieldStyle"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>           
            </td>     
             <td class="tdReadOnly">             
             <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                               (MI)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                  <asp:Label ID="lbMName" runat="server" Class="labelStyle"  cssClass="inputFieldStyle"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>  
            </td>
            <td class="tdReadOnly">
             <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                               DATE OF BIRTH
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Label ID="lbDob" runat="server" Class="labelStyle"  cssClass="inputFieldStyle"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
        <td colspan="4" class="bggreen2 bordergreen">&nbsp;</td>
        </tr>
        <tr class="bordergreen">
            <td colspan="2" class="bordergreen">
                <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                                RESPONDENT SERVED WITH CHARGES BY
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hdnChargesServedByRefNo" runat="server" />
                                <asp:TextBox ID="txtChargesServedBy" runat="server" Width="220px" AutoPostBack="false"
                                    OnTextChanged="txtChargesServedBy_TextChanged" CssClass="inputFieldStyle"></asp:TextBox>
                                <cc1:textboxwatermarkextender id="txtChargesServedBy_TextBoxWatermarkExtender" runat="server"
                                    enabled="True" targetcontrolid="txtChargesServedBy" watermarkcssclass="WaterMark"
                                    watermarktext="Enter First 2 Characters of the Name">
                                                                                    </cc1:textboxwatermarkextender>
                                <cc1:autocompleteextender id="txtChargesServedBy_AutoCompleteExtender" runat="server"
                                    delimitercharacters="" completioninterval="1000" minimumprefixlength="2" completionsetcount="12"
                                    enablecaching="true" enabled="True" servicepath="" usecontextkey="True" servicemethod="SearchEmployee"
                                    targetcontrolid="txtChargesServedBy" completionlistcssclass="CompletionList"
                                    completionlisthighlighteditemcssclass="HighlightedItem" completionlistitemcssclass="ListItem"
                                    onclientitemselected="PerformPostBack" onclientpopulating="populatingev1" onclientpopulated="shownev1"
                                    onclienthiding="onhiding1">
                                                                                    </cc1:autocompleteextender>
                                <asp:Image runat="server" SkinId="skidMiniBtn"  height="24" width="24" class="infoIconStyle" />
                                <br />
                                <asp:Label ID="lbSearchMsg2" runat="server" Text="" Font-Bold="True" Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="2" class="bordergreen">
                <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                                DATE
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtChargesServedDt" runat="server" CssClass="inputFieldStyle"></asp:TextBox>
                                <cc1:calendarextender id="txtChargesServedDt_CalendarExtender" runat="server" format="MM/dd/yyyy"
                                    enabled="True" targetcontrolid="txtChargesServedDt" popupbuttonid="SrvCal" cssclass="MyCalendar">
                                                                                    </cc1:calendarextender>
                                <asp:Image id="SrvCal" runat="server" SkinId="skidMiniCalButton" />
                                (i.e. Month/Date/Year; xx/xx/xxxx)
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Charges Served By Date Incorrect"
                                    Type="Date" Operator="DataTypeCheck" ControlToValidate="txtChargesServedDt">!</asp:CompareValidator>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr class="bordergreen">
            <td colspan="2" class="bordergreen">
                <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                                SUSPENDED BY ORDER OF
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hdnSuspendedByRefNo" runat="server" />
                                <asp:TextBox ID="txtSuspendedBy" runat="server" Width="220px" AutoPostBack="false"
                                    OnTextChanged="txtSuspendedBy_TextChanged" CssClass="inputFieldStyle"></asp:TextBox>
                                <cc1:textboxwatermarkextender id="txtSuspendedBy_TextBoxWatermarkExtender" runat="server"
                                    enabled="True" targetcontrolid="txtSuspendedBy" watermarkcssclass="WaterMark"
                                    watermarktext="Enter First 2 Characters of the Name">
                                                                                    </cc1:textboxwatermarkextender>
                                <cc1:autocompleteextender id="txtSuspendedBy_AutoCompleteExtender" runat="server"
                                    delimitercharacters="" completioninterval="1000" minimumprefixlength="2" completionsetcount="12"
                                    enablecaching="true" enabled="True" servicepath="" usecontextkey="True" servicemethod="SearchEmployee"
                                    targetcontrolid="txtSuspendedBy" completionlistcssclass="CompletionList" completionlisthighlighteditemcssclass="HighlightedItem"
                                    completionlistitemcssclass="ListItem" onclientitemselected="PerformPostBack"
                                    onclientpopulating="populatingev2" onclientpopulated="shownev2" onclienthiding="onhiding2">
                                                                                    </cc1:autocompleteextender>
                                <asp:Image runat="server" SkinId="skidMiniBtn"  height="24" width="24" class="infoIconStyle" />
                                <br />
                                <asp:Label ID="lbSearchMsg3" runat="server" Text="" Font-Bold="True" Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="2" class="bordergreen">
                <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                                DATE
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtSuspendedDt" runat="server" CssClass="inputFieldStyle"></asp:TextBox>
                                <cc1:calendarextender id="txtSuspendedDt_CalendarExtender" runat="server" format="MM/dd/yyyy"
                                    enabled="True" targetcontrolid="txtSuspendedDt" popupbuttonid="supCal" cssclass="MyCalendar">
                                                                                    </cc1:calendarextender>
                                <asp:Image id="supCal" runat="server" SkinId="skidMiniCalButton" />
                                (i.e. Month/Date/Year; xx/xx/xxxx)
                                <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="Suspended Date Incorrect"
                                    Type="Date" Operator="DataTypeCheck" ControlToValidate="txtSuspendedDt">!</asp:CompareValidator>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr class="bordergreen">
            <td colspan="2" class="bordergreen">
                <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                                SUSPENSION LIFTED BY ORDER OF
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:HiddenField ID="hdnSuspensionByRefNo" runat="server" />
                                <asp:TextBox ID="txtSuspensionBy" runat="server" Width="220px" AutoPostBack="false"
                                    OnTextChanged="txtSuspensionBy_TextChanged" CssClass="inputFieldStyle"></asp:TextBox>
                                <cc1:textboxwatermarkextender id="txtSuspensionBy_TextBoxWatermarkExtender" runat="server"
                                    enabled="True" targetcontrolid="txtSuspensionBy" watermarkcssclass="WaterMark"
                                    watermarktext="Enter First 2 Characters of the Name">
                                                                                    </cc1:textboxwatermarkextender>
                                <cc1:autocompleteextender id="txtSuspensionBy_AutoCompleteExtender" runat="server"
                                    delimitercharacters="" completioninterval="1000" minimumprefixlength="2" completionsetcount="12"
                                    enablecaching="true" enabled="True" servicepath="" usecontextkey="True" servicemethod="SearchEmployee"
                                    targetcontrolid="txtSuspensionBy" completionlistcssclass="CompletionList" completionlisthighlighteditemcssclass="HighlightedItem"
                                    completionlistitemcssclass="ListItem" onclientitemselected="PerformPostBack"
                                    onclientpopulating="populatingev3" onclientpopulated="shownev3" onclienthiding="onhiding3">
                                                                                    </cc1:autocompleteextender>
                                <asp:Image runat="server" SkinId="skidMiniBtn"  height="24" width="24" class="infoIconStyle" />
                                <br />
                                <asp:Label ID="lbSearchMsg4" runat="server" Text="" Font-Bold="True" Font-Size="Small"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td colspan="2" class="bordergreen">
                <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                                DATE
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtSuspensionDt" runat="server" CssClass="inputFieldStyle"></asp:TextBox>
                                <cc1:calendarextender id="txtSuspensionDt_CalendarExtender" runat="server" format="MM/dd/yyyy"
                                    enabled="True" targetcontrolid="txtSuspensionDt" popupbuttonid="supnCal" cssclass="MyCalendar">
                                                                                    </cc1:calendarextender>
                                <asp:Image id="supnCal" runat="server" SkinId="skidMiniCalButton" />
                                <span class="contextHelpStyle">(i.e. Month/Date/Year; xx/xx/xxxx)</span>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="Suspension Lifted Date Incorrect"
                                    Type="Date" Operator="DataTypeCheck" ControlToValidate="txtSuspensionDt">!</asp:CompareValidator>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
        <td colspan="4" class="bggreen2 bordergreen">&nbsp;</td>
        </tr>
        
        <tr >
        <td colspan="2" class="bordergreen" >
         <table border="0" width="90%">
                    <tbody>
                        <tr>
                            <td class="labelBoldStyle">
                               ROUTING DOCUMENT
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DropDownList ID="ddlRoutingLocation" runat="server" Width="177px" CssClass="inputFieldStyle">
                                    <asp:ListItem Value="">[SELECT]</asp:ListItem>
                                    <asp:ListItem Value="A">Advocate</asp:ListItem>
                                    <asp:ListItem Value="B">BCAD</asp:ListItem>
                                    <asp:ListItem Value="M">Medical</asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlRoutingLocation"
                                    ErrorMessage="Please Select Routing Location" SetFocusOnError="True">!</asp:RequiredFieldValidator>  
                                <cc1:ValidatorCalloutExtender ID="RequiredFieldValidator1_ValidatorCalloutExtender" 
                                    runat="server" Enabled="True" TargetControlID="RequiredFieldValidator1">
                                </cc1:ValidatorCalloutExtender>
                            </td>
                        </tr>
                    </tbody>
                </table>       
        </td>
        <td colspan="2" class="bordergreen">&nbsp;</td>
        </tr>
        
         <tr>
        <td colspan="4">&nbsp;</td>
        </tr>
        <tr class="bordergreen">
            <td class="labelBoldStyle">
                &nbsp;</td>
            <td class="labelBoldStyle">
            </td>
            <td class="labelBoldStyle">
                
            </td>
            <td class="labelBoldStyle">
                &nbsp;</td>
        </tr>
        
        
    </table>
    </ContentTemplate>
    </asp:UpdatePanel>
    </td>
    </tr>
    
    </table>
      
    
    
   
    </center>
</asp:Content>

