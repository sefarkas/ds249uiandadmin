﻿using System.Collections.Generic;
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;
using System.Data;
using DSNY.DSNYCP.ClassHierarchy;
using DSNY.DSNYCP.DS249.Complaints;

namespace DSNY.DSNYCP.DS249
{
    public partial class Complaints_ViewComplaintDetails : System.Web.UI.Page
    {
        #region Control Variables
        protected System.Web.UI.WebControls.Label lblSuspendedBy;
        protected System.Web.UI.WebControls.Label lblChargesServedDt;
        protected System.Web.UI.WebControls.Label lblChargesServedBy;
        protected System.Web.UI.WebControls.Label lblComments;
        protected System.Web.UI.WebControls.Label lblLocation;
        protected System.Web.UI.WebControls.Label lblSuspendedDt;
        protected System.Web.UI.WebControls.Label lblSuspensionDt;
        protected System.Web.UI.WebControls.Label lblSuspensionBy;
        protected System.Web.UI.WebControls.Label lblFirstName;
        protected System.Web.UI.WebControls.Label lblMName;
        protected System.Web.UI.WebControls.Label lblLastName;
        protected System.Web.UI.WebControls.Label lblFullName;
        protected System.Web.UI.WebControls.Label lblDOB;
        protected System.Web.UI.WebControls.Label lblRefNumber;
        protected System.Web.UI.WebControls.Label lblApptDate;
        protected System.Web.UI.WebControls.Label lblBadge;
        protected System.Web.UI.WebControls.Label lblStreetNumber;
        protected System.Web.UI.WebControls.Label lblStreetName;
        protected System.Web.UI.WebControls.Label lblAptNo;
        protected System.Web.UI.WebControls.Label lblCity;
        protected System.Web.UI.WebControls.Label lblState;
        protected System.Web.UI.WebControls.Label lblZip;
        protected System.Web.UI.WebControls.Label lblZipPrefix;
        protected System.Web.UI.WebControls.Label lblVacationSchedule;
        protected System.Web.UI.WebControls.Label lblChartDay;
        protected System.Web.UI.WebControls.Label lblPayrollLoc;
        protected System.Web.UI.WebControls.Label lbltypeDate;
        protected System.Web.UI.WebControls.Label lblRouteType;
        protected System.Web.UI.WebControls.Label lblBorough;



        //protected System.Web.UI.WebControls.Panel UpdatePanel3;

        #endregion

        #region Page Level Variables
        
        LocationList locationList;
        LocationList boroughList;
        WitnessList witnessList;
        VoilationList voilationList;
        ComplainantList complainantList;
        IncedentList incidentList;
        UserControl_WitnessControl WitnessControl;
        UserControl_ComplainantControl ComplainantControl;
        UserControl_VoilationControl VoilationControl;
        UserControl_IncidentDateControl IncidentDateControl;
        Complaint complaint;
        User user;
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {           
                user = (User)Session["clsUser"];
                if (user != null)
                {
                    if (!user.IsInMemberShip(GroupName.DS249))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                    else
                    {
                        user.CurrentMembership = GroupName.DS249;
                        SecurityUtility.AttachRolesToUser(user);
                    }

                    if (!User.IsInRole("Read")) 
                        Response.Redirect("../UI/UnAuthorized.aspx");
                }
                else
                    Response.Redirect("../UI/Login.aspx");


                ibComplaintList.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
                ibComplaintList.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");

                ibPrint.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_print_roll.png'");
                ibPrint.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_print_static.png'");

                ibRoute.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_route_roll.png'");
                ibRoute.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_route_static.png'");

                ibclose.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_close_roll.png'");
                ibclose.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_close_static.png'");


                Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                loadLocationList();
                loadBoroughList();
                loadDropdownList();
                complaint = new Complaint();
                LoadComplaintData();
                if (Request.QueryString["Mode"] == "VIEW")
                {
                    ibComplaintList.Visible = false;
                    ibclose.Visible = true;
                    ibPrint.Visible = true;
                    lbHeader.Text = "VIEW COMPLAINT";
                    //Implemented the following to disable the Date Controls on View Complaint if the Incident DateType is "DateRange" or "Multiple"
                    //Author-Shiva lakshmi
                    //Reviewed BY-Gill,Dipesh
                    ChangeControlStatus(false);
                }
                else if (Request.QueryString["Mode"] == "APPROVE" && complaint.RoutingLocation!="P")
                {
                    ibComplaintList.Visible = true;
                    ibRoute.Visible = true;
                    ibclose.Visible = false;
                    ibPrint.Visible = true;
                    lbHeader.Text = "APPROVED COMPLAINT";
                    //Implemented the following to disable the Date Controls on View Complaint if the Incident DateType is "DateRange" or "Multiple"
                    //Author-Shiva lakshmi
                    //Reviewed BY-Gill,Dipesh
                    ChangeControlStatus(false);
                }
                
                else if (Request.QueryString["Mode"] == "CLOSE")
                {
                    ibComplaintList.Visible = true;
                    ibRoute.Visible = false;
                    ibclose.Visible = false;
                    ibPrint.Visible = true;
                    lbHeader.Text = "VIEW COMPLAINT";
                    //Implemented the following to disable the Date Controls on View Complaint if the Incident DateType is "DateRange" or "Multiple"
                    //Author-Shiva lakshmi
                    //Reviewed BY-Gill,Dipesh
                    ChangeControlStatus(false);
                }

                ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
                DisplayButtonBasedOnPermission();            
        }

        #region PRIVATE METHODS

        #region USER CONTROLS

        private void initializeUserControls()
        {            
                initializeWitnessControl();
                initializeComplainantControl();
                initializeVoilationControl();         

        }

        #region Witness User Control

        private void initializeWitnessControl()
        {
           if (witnessList.List.Count == 0)
                {
                    witnessList = new WitnessList();
                    witnessList.AddWitness();
                }
                CreateWitnessControl();                       
        }

        /// <summary>
        /// This handles delete event fired from the user control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemoveWitnessControl(object sender, EventArgs e)
        {
            ImageButton but = sender as ImageButton;
            UserControl_WitnessControl WitnessControl = (UserControl_WitnessControl)but.Parent;
            Int64 UniqueID = Convert.ToInt64((((HiddenField)WitnessControl.FindControl("hdnUniqueID")).Value));
            witnessList.RemoveWitness(UniqueID);
            pnWitnesses.Controls.Remove(WitnessControl);
        }

        private void CreateWitnessControl()
        {
            int i = 1;
                pnWitnesses.Controls.Clear();

                foreach (Witness witness in witnessList.List)
                {
                    WitnessControl = (UserControl_WitnessControl)LoadControl("~/UserControl/WitnessControl.ascx");
                    WitnessControl.ID = "ucWitness" + witness.UniqueId;


                    if (i > 1)
                    {
                        ((ImageButton)WitnessControl.FindControl("btnRemove")).Visible = true;
                    }

                    WitnessControl.RemoveUserControl += this.HandleRemoveWitnessControl;
                    PopulateWitnessData(witness, WitnessControl);
                    pnWitnesses.Controls.Add(WitnessControl);
                    i++;
                }
           
        }


        private void PopulateWitnessData(Witness witness, Control ctrWitness)
        {           
                ((HiddenField)ctrWitness.FindControl("hdnUniqueID")).Value = witness.UniqueId.ToString();
                ((TextBox)ctrWitness.FindControl("txtWitness")).Visible = false;
                ((Image)ctrWitness.FindControl("imgIntelAssComp")).Visible = false;
                ((Label)ctrWitness.FindControl("lblWitness")).Visible = true;
                ((TextBox)ctrWitness.FindControl("txtWitness")).Text = witness.PersonnelName.ToString();
                ((Label)ctrWitness.FindControl("lblWitness")).Text = witness.PersonnelName.ToString();
                ((HiddenField)ctrWitness.FindControl("hdnRefNo")).Value = witness.RefNo.ToString();
                ((HiddenField)ctrWitness.FindControl("hdnPersonnelID")).Value = witness.PersonnelId.ToString();        

        }

        private void AddWitnessControl()
        {           
                witnessList.AddWitness();
                CreateWitnessControl();            

        }
        #endregion

        #region Complanaint User Control

        private void initializeComplainantControl()
        {            
                if (complainantList.List.Count == 0)
                {
                    complainantList = new ComplainantList();
                    complainantList.AddComplainant();
                }
                CreateComplainantControl();           
        }

        private void CreateComplainantControl()
        {          
                int i = 1;
                pnComplainant.Controls.Clear();
                foreach (Complainant complainant in complainantList.List)
                {
                    ComplainantControl = (UserControl_ComplainantControl)LoadControl("~/UserControl/ComplainantControl.ascx");
                    ComplainantControl.ID = "ucComplainant" + complainant.UniqueId;
                    if (i > 1)
                    {
                        ((ImageButton)ComplainantControl.FindControl("btnRemoveComplainant")).Visible = true;
                    }
                    ComplainantControl.RemoveUserControl += this.HandleRemoveComplainantControl;
                    PopulateComplainantData(complainant, ComplainantControl);
                    ComplainantControl.Enable = false;
                    pnComplainant.Controls.Add(ComplainantControl);
                    i++;
                }           
        }

        /// <summary>
        /// This handles delete event fired from the user control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemoveComplainantControl(object sender, EventArgs e)
        {           
                ImageButton but = sender as ImageButton;
                UserControl_ComplainantControl ComplainantControl = (UserControl_ComplainantControl)but.Parent;
                Int64 UniqueID = Convert.ToInt64((((HiddenField)ComplainantControl.FindControl("hdnComplainantUniqueID")).Value));
                complainantList.RemoveComplainant(UniqueID);
                pnComplainant.Controls.Remove(ComplainantControl);         
           
        }

        private void PopulateComplainantData(Complainant complainant, Control crtComplainant)
        {           
                ((Label)crtComplainant.FindControl("lblTitle")).Visible = false;
                ((Label)crtComplainant.FindControl("lblLocation")).Visible = false;
                ((Label)crtComplainant.FindControl("lblName")).Visible = false;
                ((Label)crtComplainant.FindControl("lblStatement")).Visible = false;

                ((Image)crtComplainant.FindControl("imgIntelAssComp")).Visible = false;
                ((Image)crtComplainant.FindControl("imgIntelAssComp0")).Visible = false;

                ((TextBox)crtComplainant.FindControl("txtComplaintantTitle")).Visible = false;
                ((TextBox)crtComplainant.FindControl("txtComplainantName")).Visible = false;
                ((TextBox)crtComplainant.FindControl("txtComplainantStmt")).Visible = false;
                ((DropDownList)crtComplainant.FindControl("ddlComplainantLoc")).Visible = false;

                ((Label)crtComplainant.FindControl("lblVwTitle")).Visible = true;
                ((Label)crtComplainant.FindControl("lblVwLocation")).Visible = true;
                ((Label)crtComplainant.FindControl("lblVwName")).Visible = true;
                ((Label)crtComplainant.FindControl("lblVwStatement")).Visible = true;

                ((Label)crtComplainant.FindControl("lblComplainantTitle")).Visible = true;
                ((Label)crtComplainant.FindControl("lblComplainantName")).Visible = true;
                ((Label)crtComplainant.FindControl("lblComplainantStmt")).Visible = true;
                ((Label)crtComplainant.FindControl("lblComplainantLoc")).Visible = true;

                ((HiddenField)crtComplainant.FindControl("hdnComplainantUniqueID")).Value = complainant.UniqueId.ToString();
                ((TextBox)crtComplainant.FindControl("txtComplaintantTitle")).Text = complainant.Title.ToString();

                ((Label)crtComplainant.FindControl("lblComplainantTitle")).Text = complainant.Title.ToString();

                ((HiddenField)crtComplainant.FindControl("hdnComplainantTitleID")).Value = complainant.TitleId.ToString();

                ((TextBox)crtComplainant.FindControl("txtComplainantName")).Text = complainant.ComplainantName.ToString();
                ((Label)crtComplainant.FindControl("lblComplainantName")).Text = complainant.ComplainantName.ToString();
                ((TextBox)crtComplainant.FindControl("txtComplainantStmt")).Text = complainant.Statement.ToString();
                ((Label)crtComplainant.FindControl("lblComplainantStmt")).Text = complainant.Statement.ToString();
                ((HiddenField)crtComplainant.FindControl("hdnComplainantRefNo")).Value = complainant.RefNo.ToString();
            
        }

        private void AddComplainantControl()
        {          
                complainantList.AddComplainant();
                CreateComplainantControl();           
        }

        private void AddVoilationControl()
        {           
                voilationList.Addviolation();
                CreateVoilationControl();           
        }

        #endregion

        #region Voilation User Control
        private void initializeVoilationControl()
        {           
                if (voilationList.List.Count == 0)
                {
                    voilationList = new VoilationList();
                    voilationList.Addviolation();
                }
                CreateVoilationControl();           
        }

        private void CreateVoilationControl()
        {
            
                int i = 1;
                phVoilations.Controls.Clear();
                foreach (Voilation voilation in voilationList.List)
                {
                    VoilationControl = (UserControl_VoilationControl)LoadControl("~/UserControl/VoilationControl.ascx");
                    VoilationControl.ID = "ucVolilation" + voilation.UniqueId;
                    if (i > 1)
                    {
                        ((ImageButton)VoilationControl.FindControl("btnRemove")).Visible = true;
                    }

                    VoilationControl.RemoveUserControl += this.HandleRemoveVoilationControl;
                    VoilationControl.CreateDateControl += this.AddDateControl;
                    VoilationControl.CreateNewControl += this.InitializeDateControl;
                    PopulateVoilationData(voilation, VoilationControl);
                     VoilationControl.Enable = false;
                    phVoilations.Controls.Add(VoilationControl);
                    i++;
                }           
        }

        private void PopulateVoilationData(Voilation voilation, Control crtVoilation)
        {
            Charge charges = voilation.Charge;
                ((HiddenField)crtVoilation.FindControl("hdnUniqueID")).Value = voilation.UniqueId.ToString();
                ((HiddenField)crtVoilation.FindControl("hdnComplaintID")).Value = voilation.ComplaintId.ToString();
                if (charges != null)
                {
                    ((TextBox)crtVoilation.FindControl("txtCharge")).Text = charges.ChargeDescription.ToString();

                    ((TextBox)crtVoilation.FindControl("txtCharge")).Visible = false;
                    ((TextBox)crtVoilation.FindControl("txtSingleDt")).Visible = false;
                    ((DropDownList)crtVoilation.FindControl("ddlIncedentLocation")).Visible = false;
                    ((DropDownList)crtVoilation.FindControl("ddlChargeType")).Visible = false;
                    ((Image)crtVoilation.FindControl("imgIntelAssComp")).Visible = false;

                    ((Label)crtVoilation.FindControl("lblMandatoryCharge")).Visible = false;
                    ((Label)crtVoilation.FindControl("lblMandatoryLoc")).Visible = false;
                    ((Label)crtVoilation.FindControl("lblMandatoryIncident")).Visible = false;
                    ((Label)crtVoilation.FindControl("Label1")).Visible = false;

                    ((Label)crtVoilation.FindControl("lblDate1")).Visible = false;
                    ((Label)crtVoilation.FindControl("lblDate2")).Visible = false;
                    ((Label)crtVoilation.FindControl("lblDate3")).Visible = false;

                    ((ImageButton)crtVoilation.FindControl("btnUpload")).Visible = false;
                    ((Label)crtVoilation.FindControl("lblCharge")).Text = charges.ChargeDescription.ToString();
                    ((Label)crtVoilation.FindControl("lblCharge")).Visible = true;
                    ((Label)crtVoilation.FindControl("lblIncedentLocation")).Visible = true;
                    ((Label)crtVoilation.FindControl("lblChargeType")).Visible = true;
                    ((Label)crtVoilation.FindControl("lblDate")).Visible = true;
                    ((HiddenField)crtVoilation.FindControl("hdnChargeRefNo")).Value = charges.ChargeCode.ToString();                     

                }

                IncedentList incidents = voilation.Incidents;
                if (incidents != null)
                {
                    if (incidents.List.Count > 0)
                    {
                        switch (voilation.IncidentType)
                        {
                            case "S":
                                foreach (Incedent incident in incidents.List)
                                {
                                    PlaceHolder pHolder = (PlaceHolder)crtVoilation.FindControl("phSingle");
                                    pHolder.Visible = true;
                                    if (incident.IncidentDate != DateTime.MinValue)
                                        ((TextBox)pHolder.FindControl("txtSingleDt")).Text = Convert.ToString(incident.IncidentDate.ToShortDateString());
                                    ((Label)pHolder.FindControl("lblDate")).Text = Convert.ToString(incident.IncidentDate.ToShortDateString());

                                }
                                break;
                            case "R":
                                int cnt = 1;
                                foreach (Incedent incident in incidents.List)
                                {
                                    PlaceHolder pHolder = (PlaceHolder)crtVoilation.FindControl("phRange");
                                    pHolder.Visible = true;
                                    if (cnt == 1)
                                    {
                                        if (incident.IncidentDate != DateTime.MinValue)
                                            ((TextBox)pHolder.FindControl("txtStartDt")).Text = Convert.ToString(incident.IncidentDate.ToShortDateString());
                                        ((Label)pHolder.FindControl("lblDate")).Text = Convert.ToString(incident.IncidentDate.ToShortDateString());
                                    }
                                    else
                                    {
                                        if (incident.IncidentDate != DateTime.MinValue)
                                            ((TextBox)pHolder.FindControl("txtEndDt")).Text = Convert.ToString(incident.IncidentDate.ToShortDateString());
                                    }
                                    cnt++;
                                }
                                break;
                            case "M":
                                if (incidents != null)
                                {
                                    if (incidents.List.Count > 0)
                                    {
                                        PlaceHolder pHolder = (PlaceHolder)crtVoilation.FindControl("phMultiple");
                                        pHolder.Visible = true;
                                        CreateIncidentDtControl(crtVoilation, incidents);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (voilation.IncidentType)
                        {
                            case "S":
                                PlaceHolder pSHolder = (PlaceHolder)crtVoilation.FindControl("phSingle");
                                pSHolder.Visible = true;
                                break;
                            case "R":
                                PlaceHolder pRHolder = (PlaceHolder)crtVoilation.FindControl("phRange");
                                pRHolder.Visible = true;
                                break;
                            default:
                                break;
                        }
                    }
                }           
        }

        /// <summary>
        /// This handles delete event fired from the user control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemoveVoilationControl(object sender, EventArgs e)
        {            
                ImageButton but = sender as ImageButton;
                UserControl_VoilationControl VoilationControl = (UserControl_VoilationControl)but.Parent;
                Int64 UniqueID = Convert.ToInt64((((HiddenField)VoilationControl.FindControl("hdnUniqueID")).Value));
                voilationList.Removevoilation(UniqueID);
                phVoilations.Controls.Remove(VoilationControl);            
        }


        #region Incident Control

        public void InitializeDateControl(object sender, EventArgs e)
        {          
                UserControl_VoilationControl VoilationControl;
                DropDownList but = sender as DropDownList;
                VoilationControl = (UserControl_VoilationControl)but.Parent;

                Int64 UniqueID = Convert.ToInt64((((HiddenField)VoilationControl.FindControl("hdnUniqueID")).Value));
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == UniqueID; });
                incidentList = new IncedentList();
                incidentList.AddIncident();
                voilation.Incidents = incidentList;
                CreateIncidentDtControl(VoilationControl, incidentList);           
        }

        public void AddDateControl(object sender, EventArgs e)
        {            
                UserControl_VoilationControl VoilationControl;
                ImageButton but1 = sender as ImageButton;
                VoilationControl = (UserControl_VoilationControl)but1.Parent.Parent;
                Int64 UniqueID = Convert.ToInt64((((HiddenField)VoilationControl.FindControl("hdnUniqueID")).Value));
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == UniqueID; });
                if (voilation.Incidents == null)
                {
                    incidentList = new IncedentList();
                }
                else
                    incidentList = voilation.Incidents;
                incidentList.AddIncident();
                voilation.Incidents = incidentList;
                CreateIncidentDtControl(VoilationControl, incidentList);           
        }


        private void CreateIncidentDtControl(Control Voilation, IncedentList incidentList)
        {
           
                int i = 1;
                PlaceHolder ph = (PlaceHolder)Voilation.FindControl("phMultiple");
                ph.Visible = true;               

                List<Control> obj = new List<Control>();
                foreach (Control crt in ph.Controls)
                {
                    if (crt is UserControl_IncidentDateControl)
                    {
                        obj.Add(crt);
                    }
                }

                foreach (Control crt in obj)
                {
                    ph.Controls.Remove(crt);
                }
                
                
                foreach (Incedent incident in incidentList.List)
                {
                    IncidentDateControl = (UserControl_IncidentDateControl)LoadControl("~/UserControl/IncidentDateControl.ascx");
                    IncidentDateControl.ID = "ucIncidentDt" + incident.UniqueId;
                   

                    if (i > 1)
                    {
                        ((ImageButton)IncidentDateControl.FindControl("btnRemoveDate")).Visible = true;
                    }

                    IncidentDateControl.RemoveDateControl += this.HandleRemoveDateControl;
                    IncidentDateControl.UpdateDateControl += this.HandleUpdateDateControl;               

                    if (incident.IncidentDate != DateTime.MinValue)
                        ((TextBox)IncidentDateControl.FindControl("txtIncidentDt")).Text = incident.IncidentDate.ToShortDateString();
                    ((HiddenField)IncidentDateControl.FindControl("hdnUniqueID")).Value = incident.UniqueId.ToString();
                
                    ph.Controls.Add(IncidentDateControl);                    
                    i++;
                }            
        }

        public void HandleUpdateDateControl(object sender, EventArgs e)
        {            
                Incedent incident;
                TextBox txtBox = sender as TextBox;
                UserControl_IncidentDateControl IncidentControl = (UserControl_IncidentDateControl)txtBox.Parent;
                PlaceHolder ph = (PlaceHolder)IncidentControl.Parent;                
                UserControl_VoilationControl VoilationCrtl = (UserControl_VoilationControl)ph.Parent;
                Int64 VoilationUniqueID = Convert.ToInt64((((HiddenField)VoilationCrtl.FindControl("hdnUniqueID")).Value));

                
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == VoilationUniqueID; });

                IncedentList incidentLst = voilation.Incidents;
                incidentLst.List.Clear();

                foreach (Control crt in ph.Controls)
                {
                    if (crt is UserControl_IncidentDateControl)
                    {
                    incident = new Incedent
                    {
                        UniqueId = Convert.ToInt16(((HiddenField)crt.FindControl("hdnUniqueID")).Value)
                    };
                    if (((TextBox)crt.FindControl("txtIncidentDt")).Text != String.Empty)
                            incident.IncidentDate = Convert.ToDateTime(((TextBox)crt.FindControl("txtIncidentDt")).Text);
                        incidentLst.List.Add(incident);
                    }
                }
                voilation.Incidents = incidentLst;            
        }

        public void HandleRemoveDateControl(object sender, EventArgs e)
        {
           
                ImageButton but = sender as ImageButton;
                UserControl_IncidentDateControl IncidentControl = (UserControl_IncidentDateControl)but.Parent;

                PlaceHolder ph = (PlaceHolder)IncidentControl.Parent;

                
                UserControl_VoilationControl VoilationCrtl = (UserControl_VoilationControl)ph.Parent;
                Int64 VoilationUniqueID = Convert.ToInt64((((HiddenField)VoilationCrtl.FindControl("hdnUniqueID")).Value));

                
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == VoilationUniqueID; });
                IncedentList incidentLst = voilation.Incidents;

                Int64 UniqueID = Convert.ToInt64((((HiddenField)IncidentControl.FindControl("hdnUniqueID")).Value));
                incidentLst.RemoveIncident(UniqueID);

                ph.Controls.Remove(IncidentControl);
            
        }
        #endregion

        #endregion

        #endregion

        #region Populate Dropdowns

        private void loadDropdownList()
        {           
                loadLocation();
                loadBorough();
           
        }

        private void loadLocationList()
        {
          
                locationList = new LocationList();

                if (Session["LocationList"] == null)
                {
                    locationList.Load();
                    Session["LocationList"] = locationList.List;
                }
                else
                {
                    locationList.List = (List<LocationDS>)Session["LocationList"];
                }           
        
        }

        private void loadBoroughList()
        { 
            boroughList = new LocationList();
                if (Session["BoroughList"] == null)
                {
                    boroughList.Load(true);
                    Session["BoroughList"] = boroughList.List;
                }
                else
                {
                    boroughList = new LocationList();
                    boroughList.Load(true);
                }          
        }

        private void loadLocation()
        {          
                ListItem item;
                foreach (LocationDS location in locationList.List)
                {
                item = new ListItem
                {
                    Value = location.LocationId.ToString(),
                    Text = location.LocationName.ToString()
                };
                ddlLocation.Items.Add(item);
                }          
        }

        private void loadBorough()
        {           
                ListItem item;
                foreach (LocationDS location in boroughList.List)
                {
                item = new ListItem
                {
                    Value = location.LocationId.ToString(),
                    Text = location.LocationName.ToString()
                };
                ddlBorough.Items.Add(item);
                }           
        }



        #endregion

        #region Method Helper

        private void DisplayButtonBasedOnPermission()
        {
            if (User.IsInRole("Print"))
            {
                ibPrint.Visible = true;
            }
            else
            {
                ibPrint.Visible = false;
            }
        }

        private void LoadComplaintData()
        {           
                Int64 ID = Convert.ToInt64(Request.QueryString["Id"]);                
                complaint.Load(ID, RequestSource.DS249);
                if (complaint.ComplaintId != -1)
                {
                    BindComplaintData();
                    witnessList = new WitnessList();
                    witnessList = complaint.Witnesses;
                    witnessList.NewUniqueId = Convert.ToInt16(witnessList.List.Count);
                    Session["witnessListView"] = witnessList;
                    complainantList = new ComplainantList();
                    complainantList = complaint.Complainants;
                    complainantList.NewUniqueId = Convert.ToInt16(complainantList.List.Count);
                    Session["ComplainantListView"] = complainantList;

                    voilationList = new VoilationList();
                    voilationList = complaint.Violations;
                    voilationList.NewUniqueId = Convert.ToInt16(voilationList.List.Count);
                    Session["voilationListView"] = voilationList;                    
                    initializeUserControls();
                    LoadComplaintHistory();
                }
                else
                {
                    Response.Redirect("Dashboard.aspx");
                }           
        }

        private void LoadComplaintHistory()
        {            
                gvHistory.DataSource = complaint.HistoryList.List;
                gvHistory.DataBind();                           
           
        }
                    
        private void AssociateIncidentToVoilation()
        {
            IncedentList lstIncident;           
                foreach (Voilation voilation in voilationList.List)
                {
                    if (voilation != null)
                    {
                        Int16 i = 1;
                        lstIncident = new IncedentList();
                        foreach (Incedent incident in incidentList.List)
                        {
                            if (incident.ChargeId == voilation.ChargeId)
                            {
                                incident.UniqueId = i;
                                lstIncident.List.Add(incident);
                                i++;
                            }
                        }
                        i = (Int16)(i - 1);
                        lstIncident.NewUniqueId = i;
                        voilation.Incidents = lstIncident;
                    }
                }           
        }

        private void BindPersonnelData()
        {
            Personnel personnel=new Personnel();
                hdnEmployeeID.Value = personnel.PersonnelId.ToString();
                lblDate.Text = DateTime.Now.ToShortDateString();           
        }

        private void BindComplaintData()
        {           
                lblIndexNumber.Text = complaint.IndexNo.ToString();
                ddlLocation.Items.FindByValue(complaint.LocationId.ToString()).Selected = true;
                lblLocation.Text = ddlLocation.SelectedItem.Text.ToString();
                LoadBoroughByLocation();
                lblComments.Text = complaint.Comments.ToString();
                if (complaint.RoutingLocation.ToString().Trim() != "P")
                {
                    ddlRoutingLocation.Items.FindByValue(complaint.RoutingLocation.ToString().Trim()).Selected = true;
                }
                chkProbation.Checked = complaint.IsProbation;

                if (complaint.ChargesServedByName != String.Empty)
                {
                    hdnChargesServedByRefNo.Value = complaint.ChargesServedByRefNo.ToString();
                    lblChargesServedBy.Text = complaint.ChargesServedByName.ToString();
                }
                if (complaint.ChargesServedDate != DateTime.MinValue)
                    lblChargesServedDt.Text = complaint.ChargesServedDate.ToShortDateString();

                if (complaint.SuspensionOrderedByName != String.Empty)
                {
                    hdnSuspendedByRefNo.Value = complaint.SuspensionOrderedByRefNo.ToString();
                    lblSuspendedBy.Text = complaint.SuspensionOrderedByName.ToString();
                }

                if (complaint.SuspensionOrderDate != DateTime.MinValue)
                    lblSuspendedDt.Text = complaint.SuspensionOrderDate.ToShortDateString();
                if (complaint.SuspensionLiftedDate != DateTime.MinValue)
                    lblSuspensionDt.Text = complaint.SuspensionLiftedDate.ToShortDateString();

                if (complaint.SuspensionLiftedByName != String.Empty)
                {
                    hdnSuspensionByRefNo.Value = complaint.SuspensionLiftedByRefNo.ToString();
                    lblSuspensionBy.Text = complaint.SuspensionLiftedByName.ToString();
                }

                hdnEmployeeID.Value = complaint.EmployeeId.ToString();

                lblDate.Text = complaint.CreatedDate.ToShortDateString();
                lblFirstName.Text = complaint.RespondentFirstName.ToString();
                lblMName.Text = complaint.RespondentMiddleName.ToString();
                lblLastName.Text = complaint.RespondentLastName.ToString();
                lblFullName.Text = lblFirstName.Text + " " + lblLastName.Text;
                if (complaint.RespondentDob != DateTime.MinValue)
                    lblDOB.Text = complaint.RespondentDob.ToShortDateString();
                lblRefNumber.Text = complaint.ReferenceNumber.ToString();
                if (complaint.RespondentAptDate != DateTime.MinValue)
                    lblApptDate.Text = complaint.RespondentAptDate.ToShortDateString();
                //lblBadge.Text = complaint.BadgeNumber.ToString();               

                lblStreetNumber.Text = complaint.StreetNumber;
                lblStreetName.Text = complaint.StreetName;
                lblAptNo.Text = complaint.AptNo;
                lblCity.Text = complaint.City;
                lblState.Text = complaint.State;
                lblZip.Text = complaint.ZipCode;
                lblZipPrefix.Text = complaint.ZipSuffix;

                //lblVacationSchedule.Text = complaint.VacationSchedule;
                lblChartDay.Text = complaint.ChartNo;
                lblPayrollLoc.Text = complaint.PayCodeDescription;
                lblLocation.Text = complaint.LocationName.ToString();                                
                if (complaint.SentDate != DateTime.MinValue)
                    lbltypeDate.Text = complaint.SentDate.ToShortDateString();
                BindRouteDocumentData();                
            
        }
        /// <summary>
        /// This method loads the list of Borough w.r.t location
        /// </summary>
        private void LoadBoroughByLocation()
        {            
                boroughList = new LocationList();
                boroughList.Load(true);
                LocationDS borough = boroughList.List.Find(delegate(LocationDS q) { return q.LocationId == Convert.ToInt64(complaint.BoroughId); });
                if (borough != null)
                {
                    String Borough = borough.LocationName.ToString();
                    lblBorough.Text = Borough;
                }
                else
                    lblBorough.Text = "";           
        }
        private void BindRouteDocumentData()
        {           
                lblIndexNumber.Text = complaint.IndexNo.ToString();
                if (complaint.RoutingLocation.Trim() != String.Empty && complaint.RoutingLocation.Trim() != "P")
                {
                    ddlRoutingLocation.Items.FindByValue(complaint.RoutingLocation.ToString()).Selected = true;
                    ddlRoutingLocation.Enabled = false;
                    lblRouteType.Text = ddlRoutingLocation.SelectedItem.Text.ToString();
                }           
        }
        
        private void clearForm()
        {

        }
                
        protected void gvHistory_RowDataBound(Object sender, GridViewRowEventArgs e)
        {            
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string strPenDesc = e.Row.Cells[5].Text.Trim();

                    if (strPenDesc == ",")
                    {
                        e.Row.Cells[5].Text = "";
                    }
                    else if (strPenDesc.Substring(0, 1) == ",")
                    {
                        e.Row.Cells[5].Text = strPenDesc.Substring(1, strPenDesc.Length - 1);
                    }
                    else if (strPenDesc.Contains("nbsp") == false)
                    {
                        if (strPenDesc.EndsWith(",") == true)
                        {
                            strPenDesc = strPenDesc.Remove(strPenDesc.Length - 1, 1);
                        }
                        strPenDesc += ".";
                        e.Row.Cells[5].Text = strPenDesc;
                    }


                    if (Request.QueryString["Mode"] == "VIEW" || Request.QueryString["Mode"] == "CLOSE" || Request.QueryString["Mode"] == "APPROVE")
                    {
                        e.Row.Cells[0].Visible = false;
                    }
                    else
                    {
                        LinkButton lkBtn = (LinkButton)e.Row.FindControl("LinkButton1");
                        String URL = String.Format("CaseManagement.aspx?ID={0}&Mode=VIEW", e.Row.Cells[1].Text.ToString());
                        lkBtn.Attributes.Add("onClick", String.Format("OpenModalWindow('{0}')", URL));
                        e.Row.Cells[1].Visible = false;
                    }
                    string Desc = e.Row.Cells[4].Text.ToString();
                    Desc = Desc.Replace(", ", "\n");
                    e.Row.Cells[3].Attributes.Add("title", Desc);
                    e.Row.Cells[4].Visible = false;
                }
                
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[4].Visible = false;

                    if (Request.QueryString["Mode"] == "VIEW" || Request.QueryString["Mode"] == "CLOSE" || Request.QueryString["Mode"] == "APPROVE")
                    {
                        e.Row.Cells[0].Visible = false;
                    }
                    else
                        e.Row.Cells[1].Visible = false;
                }           
        }

        private void ChangeControlStatus(bool status)
        {
            
                foreach (Control c in UpdatePanel1.Controls)
                {
                    foreach (Control ctrl in c.Controls)
                    {
                        
                        if (ctrl is TextBox)

                            ((TextBox)ctrl).Enabled = status;

                        else if (ctrl is Button)

                            ((Button)ctrl).Enabled = status;

                        else if (ctrl is RadioButton)

                            ((RadioButton)ctrl).Enabled = status;

                        else if (ctrl is ImageButton)

                            ((ImageButton)ctrl).Visible = status;
                        else if (ctrl is Image)

                            ((Image)ctrl).Visible = status;

                        else if (ctrl is CheckBox)

                            ((CheckBox)ctrl).Enabled = status;
                        
                        else if (ctrl is DropDownList)

                            ((DropDownList)ctrl).Enabled = status;

                        else if (ctrl is HyperLink)
                            ((HyperLink)ctrl).Enabled = status;
                        else if (ctrl is PlaceHolder)
                        {
                            if (ctrl.ID == "pnWitnesses")
                            {
                                foreach (UserControl_WitnessControl control in ctrl.Controls)
                                {
                                    control.Enable = status;
                                }
                            }

                            if (ctrl.ID == "pnComplainant")
                            {
                                foreach (UserControl_ComplainantControl control in ctrl.Controls)
                                {
                                    control.Enable = status;
                                }
                            }


                            if (ctrl.ID == "phVoilations")
                            {
                                foreach (UserControl_VoilationControl control in ctrl.Controls)
                                {
                                    control.Enable = status;
                                    foreach (Control child in control.Controls)
                                    {
                                        if (child.ID == "phMultiple")
                                        {
                                            if (child.Controls.Count > 2)
                                            {
                                                foreach (Control ctrIncident in child.Controls)
                                                {
                                                    if (ctrIncident is UserControl_IncidentDateControl)
                                                        ((UserControl_IncidentDateControl)ctrIncident).Enable = status;
                                                }
                                            }
                                        }

                                        //Implemented the follwoing to disable the Date Controls on View Complaint if the Incident DateType is "DateRange"
                                        //Author-Shiva lakshmi
                                        //Reviewed BY-Gill
                                        if (child.ID == "phRange")
                                        {
                                            if (child.Controls.Count > 2)
                                            {
                                                foreach (Control ctrIncident in child.Controls)
                                                {
                                                    if (ctrIncident is UserControl_IncidentDateControl)
                                                        ((UserControl_IncidentDateControl)ctrIncident).Enable = status;
                                                }
                                            }
                                        }



                                    }

                                }

                            }
                            
                        }
                    
                    }
                }           
            
        }

        #endregion

        #endregion

        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            String URL = String.Format("RouteComplaint.aspx?Id={0}", complaint.ComplaintId.ToString());
            Session["Mode"] = "EDIT";
            Response.Redirect(URL);
        }
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }

        protected void ibPrint_Click(object sender, ImageClickEventArgs e)
        {

        }

    }
}