﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="DSNY.DSNYCP.DS249.Complaints.Upload" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
  <link href="../includes/css/styles1.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/CaseManagementStyle.css" rel="stylesheet" type="text/css" />
<link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
<link href="../includes/css/DashboardStyle.css" rel="stylesheet" type="text/css" />
<%--<link href="../includes/css/ModalPopUp.css" rel="stylesheet" type="text/css" />--%>
    <title></title>
    <style type="text/css">
        
.modalPopup {
	background-color: #fff;
	border: 1px solid #1e951f;
	clear:both;
	color:#000000;
	font-family:Arial;
	font-weight:bold;
	font-size:12px;
	line-height:15pt;
	padding: 30px;
	text-align: center;
}

    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:Label ID="Label1" runat="server" Text="Supported Documents" class="styleHeading"></asp:Label>        
        <br />
        <br />  
        <table>
            <tr>
                <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" Style="width: 217px" />
                 &nbsp;<asp:ImageButton ID="btnAttach" runat="server" ImageUrl="~/includes/img/ds249_btn_attach.png" CausesValidation="false"
                        OnClick="btnAttach_Click" />
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                        <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                    </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                <asp:GridView ID="gvUploadFile" runat="server" AutoGenerateColumns="False" BorderColor="#235705"
                                                    EmptyDataText="No File Uploaded" 
                                                    HeaderStyle-BackColor="#a4d49a" HeaderStyle-Font-Bold="true"
                                                    Width="380px" DataKeyNames="IndexNo"
                                                    OnRowDeleting="gvUploadFile_RowDeleting"
                                                    OnRowDataBound="gvUploadFile_RowDataBound">
                                                    <EmptyDataRowStyle CssClass="fromdate" />
                                                    <Columns>  
                                                    <asp:BoundField DataField="ComplaintID" DataFormatString="" HeaderText="ComplaintID" 
                                                            Visible="true">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="IndexNo" DataFormatString="" HeaderText="#">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="IncidentID" DataFormatString="" HeaderText="IncidentID" 
                                                            Visible="true">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ChildID" DataFormatString="" 
                                                            HeaderText="Charges" Visible="true">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="AttachmentName" DataFormatString="" 
                                                            HeaderText="File">
                                                            <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                VerticalAlign="Middle"  Wrap="False" />
                                                            <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" 
                                                                Font-Bold="false" Wrap="False" />
                                                        </asp:BoundField>
                                                        
                                                        <asp:TemplateField HeaderText="View">
                                                           <ItemTemplate>
                                                           <asp:ImageButton ID="ViewButton" ImageUrl="~/includes/img/ds249_btn_view_static.png"
                                                                    runat="server" CommandName="View" CausesValidation="false" 
                                                                  />
                                                           </ItemTemplate>                                                         
                                                         </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="Delete">
                                                           <ItemTemplate>
                                                           <asp:ImageButton ID="deleteButton" ImageUrl="~/includes/img/ds249_btn_clear_static.png"
                                                                    runat="server" CommandName="Delete" CausesValidation="false"  />
                                                           </ItemTemplate>                                                         
                                                         </asp:TemplateField>
                                                    </Columns>
                                                    <HeaderStyle BackColor="#A4D49A" Font-Bold="True" />
                                                </asp:GridView>
                
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="center">
                <asp:ImageButton ID="CancelButton" runat="server" 
                ImageUrl="~/includes/img/ds249_btn_close_static.png" CausesValidation="false"/>
               </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
