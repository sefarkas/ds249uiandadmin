﻿using System.Collections.Generic;
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;
using System.Data;
using DSNY.DSNYCP.DTO;
using DSNY.DSNYCP.ClassHierarchy;

/// <summary>
/// This class provides the functionality for events and methods for the DS249 Case management page.
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class Complaints_CaseManagement : System.Web.UI.Page
    {
        #region Page Level Variables
       

        Personnel personnel;
        LocationList locationList;
        LocationList boroughList;      
        WitnessList witnessList;
        VoilationList voilationList;
        ComplainantList complainantList;
        IncedentList incidentList;
        UserControl_WitnessControl WitnessControl;
        UserControl_ComplainantControl ComplainantControl;
        UserControl_VoilationControl VoilationControl;
        UserControl_IncidentDateControl IncidentDateControl;
        Complaint complaint;
       
       
        ComplaintHistoryList complaintHistory;
        User user;
       

        #endregion
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        ArrayList ItemList = new ArrayList();
        protected void Page_Load(object sender, EventArgs e)
        {           
            ImageButton1.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
            ImageButton1.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");
                      
            ibSave.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_save_roll.png'");
            ibSave.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_save_static.png'");
           
            //************PENDING DOCUMENT************
            string save;
            save = "S";
            ibSave.Attributes.Add("Onclick", "javascript:CloseComfirmation('" + save + "');");

            ibPrint.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_print_roll.png'");
            ibPrint.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_print_static.png'");

            ibSubmit.Attributes.Add("onmouseover", "this.src='../includes/img/submit_over_btn.png'");
            ibSubmit.Attributes.Add("onmouseout", "this.src='../includes/img/submit_btn.png'");
           

            ibReject.Attributes.Add("onmouseover", "this.src='../includes/img/PMD_btn_return_roll.png'");
            ibReject.Attributes.Add("onmouseout", "this.src='../includes/img/PMD_btn_return_static.png'");


            //************PENDING DOCUMENT************
            string approve;
            approve = "A";
            ibApprove.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_approve_roll.png'");
            ibApprove.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_approve_static.png'");
            ibApprove.Attributes.Add("Onclick", "javascript:return CloseComfirmation('" + approve + "');");   


            Button1.Style.Add("display", "none");
                user = (User)Session["clsUser"];
                if (user != null)
                {
                    if (!user.IsInMemberShip(GroupName.DS249))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                    else
                    {
                        user.CurrentMembership = GroupName.DS249;
                        SecurityUtility.AttachRolesToUser(user);
                    }

                    if (!User.IsInRole("Read"))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                }
                else
                    Response.Redirect("../UI/Login.aspx");

                if (Session["IsAuthenticated"] == null)
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }

                if (Session["IsAuthenticated"].ToString() == "false")
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }

                DisplayButtonBasedOnPermission();
                lbMessage.Text = String.Empty;                
                loadLocationList();
                loadBoroughList();
                initializeUserControls();

                if (Session["Mode"].ToString() == "EDIT")
                {
                    ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));

                }           
             

                if (!IsPostBack)
                {
                  
                    loadDropdownList();

                    complaint = new Complaint();            
                   

                    if (Session["Mode"].ToString() == "NEW")
                    {
                        Session["Complaint"] = complaint;
                        ChangeControlStatus(false);
                        lbHeader.Text = "NEW COMPLAINT";

                    }

                    if (Session["Mode"].ToString() == "EDIT")
                    {

                        lbHeader.Text = "EDIT COMPLAINT";
                        Session["Complaint"] = complaint;
                        LoadComplaintData();
                        lblDate.Text = complaint.CreatedDate.ToShortDateString();                         

                        List<UploadFile> upLoadDTO;
                        UploadFile uf = new UploadFile();
                        upLoadDTO = uf.LoadComplaintAttachments(0, complaint.ComplaintId, string.Empty);
                        Session["upLoadDTO"] = upLoadDTO;
                    }
                }

                /*This if condition is to fix the visibility issue on dropdown (on the page e.g. district dropdown)
           selection change*/

                //************PENDING DOCUMENT************

                if (hdnPendingValue.Value.ToLower() == "p")
                {
                    SupCheckBox.Attributes.Remove("style");
                }
                else
                {
                    SupCheckBox.Attributes.Add("style", "visibility: hidden;");
                }                               
           
        }

        #region PRIVATE METHODS

        #region USER CONTROLS
        /// <summary>
        /// This method initialised the user controls that are to be used.
        /// </summary>
        private void initializeUserControls()
        {
                initializeVoilationControl();
                initializeComplainantControl();
                initializeWitnessControl();            
        }

        #region Witness User Control
        /// <summary>
        /// This method initialised the Witness user controls.
        /// </summary>
        private void initializeWitnessControl()
        {
                if (Session["witnessList"] == null)
                {
                    witnessList = new WitnessList();
                    Session["witnessList"] = witnessList;
                    witnessList.AddWitness();
                }
                else
                    witnessList = (WitnessList)Session["witnessList"];

                if (witnessList.List.Count == 0)
                {
                    witnessList = new WitnessList();
                    Session["witnessList"] = witnessList;
                    witnessList.AddWitness();
                }
               CreateWitnessControl();   
        }

        /// <summary>
        /// This method handles delete event fired from the user control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemoveWitnessControl(object sender, EventArgs e)
        {
                ImageButton but = sender as ImageButton;
                UserControl_WitnessControl WitnessControl = (UserControl_WitnessControl)but.Parent;                
                Int64 UniqueID = Convert.ToInt64((((HiddenField)WitnessControl.FindControl("hdnUniqueID")).Value));
                witnessList.RemoveWitness(UniqueID);
                pnWitnesses.Controls.Remove(WitnessControl);
           
        }
        /// <summary>
        /// This method creates WitnessControl user control 
        /// </summary>
        private void CreateWitnessControl()
        {
              int i = 1;
                pnWitnesses.Controls.Clear();
                foreach (Witness witness in witnessList.List)
                {
                    WitnessControl = (UserControl_WitnessControl)LoadControl("~/UserControl/WitnessControl.ascx");
                    WitnessControl.ID = "ucWitness" + witness.UniqueId;
                    if (i > 1)
                    {
                        ((ImageButton)WitnessControl.FindControl("btnRemove")).Visible = true;
                    }

                    WitnessControl.RemoveUserControl += this.HandleRemoveWitnessControl;
                    PopulateWitnessData(witness, WitnessControl);
                    pnWitnesses.Controls.Add(WitnessControl);
                    i++;
                }            
        }

        /// <summary>
        /// This method populate WitnessControl user control 
        /// </summary>
        /// <param name="witness"></param>
        /// <param name="ctrWitness"></param>
        private void PopulateWitnessData(Witness witness, Control ctrWitness)
        {           
                ((HiddenField)ctrWitness.FindControl("hdnUniqueID")).Value = witness.UniqueId.ToString();
                ((TextBox)ctrWitness.FindControl("txtWitness")).Text = witness.PersonnelName.ToString();
                ((HiddenField)ctrWitness.FindControl("hdnRefNo")).Value = witness.RefNo.ToString();
                ((HiddenField)ctrWitness.FindControl("hdnPersonnelID")).Value = witness.PersonnelId.ToString();    
           

        }
        /// <summary>
        /// This method Add WitnessControl user control 
        /// </summary>
        private void AddWitnessControl()
        {           
                witnessList.AddWitness();
                CreateWitnessControl();            
        }
        #endregion

        #region Complanaint User Control
        /// <summary>
        /// This method Load New ComplainantList and assign it to complaint object
        /// </summary>
        private void initializeComplainantControl()
        {
                if (Session["ComplainantList"] == null)
                {
                    complainantList = new ComplainantList();
                    Session["ComplainantList"] = complainantList;
                    complainantList.AddComplainant();
                }
                else
                    complainantList = (ComplainantList)Session["ComplainantList"];

                if (complainantList.List.Count == 0)
                {
                    complainantList = new ComplainantList();
                    Session["ComplainantList"] = complainantList;
                    complainantList.AddComplainant();
                }
                CreateComplainantControl();            
        }
        /// <summary>
        /// Add an event handler to this control to raise an event when the delete button is clicked 
        /// on the user control and Finally, add the user control to the panel 
        /// </summary>
        private void CreateComplainantControl()
        {           
                int i = 1;
                pnComplainant.Controls.Clear();
                foreach (Complainant complainant in complainantList.List)
                {
                    ComplainantControl = (UserControl_ComplainantControl)LoadControl("~/UserControl/ComplainantControl.ascx");
                    ComplainantControl.ID = "ucComplainant" + complainant.UniqueId;
                    if (i > 1)
                    {
                        ((ImageButton)ComplainantControl.FindControl("btnRemoveComplainant")).Visible = true;
                    }
                    ComplainantControl.RemoveUserControl += this.HandleRemoveComplainantControl;
                    PopulateComplainantData(complainant, ComplainantControl);
                    pnComplainant.Controls.Add(ComplainantControl);
                    i++;
                }                
        }

        /// <summary>
        /// This handles delete event fired from the user control 
        /// Get the user control that fired this event, and remove it
        /// Remove that Complainant from the ComplainantList.
        /// Remove the Control from the Container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        public void HandleRemoveComplainantControl(object sender, EventArgs e)
        {
                ImageButton but = sender as ImageButton;
                UserControl_ComplainantControl ComplainantControl = (UserControl_ComplainantControl)but.Parent;
                Int64 UniqueID = Convert.ToInt64((((HiddenField)ComplainantControl.FindControl("hdnComplainantUniqueID")).Value));
                complainantList.RemoveComplainant(UniqueID);
                pnComplainant.Controls.Remove(ComplainantControl);           
        }
        /// <summary>
        /// This method populate ComplainantData user control 
        /// </summary>
        /// <param name="complainant"></param>
        /// <param name="crtComplainant"></param>
        private void PopulateComplainantData(Complainant complainant, Control crtComplainant)
        {            
                ((HiddenField)crtComplainant.FindControl("hdnComplainantUniqueID")).Value = complainant.UniqueId.ToString();
                ((TextBox)crtComplainant.FindControl("txtComplainantName")).Text = complainant.ComplainantName.ToString();
                ((TextBox)crtComplainant.FindControl("txtComplaintantTitle")).Text = complainant.Title.ToString();
                ((HiddenField)crtComplainant.FindControl("hdnComplainantTitleID")).Value = complainant.TitleId.ToString();
                ((TextBox)crtComplainant.FindControl("txtComplainantStmt")).Text = complainant.Statement.ToString();
                ((HiddenField)crtComplainant.FindControl("hdnComplainantRefNo")).Value = complainant.RefNo.ToString();            
        }
        /// <summary>
        /// This method Add Add Complainant object in the list.
        /// </summary>
        private void AddComplainantControl()
        {           
                complainantList.AddComplainant();
                CreateComplainantControl();            
        }
        /// <summary>
        /// Add Voilation Object in the list.
        /// </summary>
        private void AddVoilationControl()
        {           
                voilationList.Addviolation();
                CreateVoilationControl();            
        }

        #endregion

        #region Voilation User Control
        /// <summary>
        /// This method initializes the Voilation User Control
        /// </summary>
        private void initializeVoilationControl()
        {           
                if (Session["voilationList"] == null)
                {
                    voilationList = new VoilationList();
                    Session["voilationList"] = voilationList;
                    voilationList.Addviolation();
                }
                else
                    voilationList = (VoilationList)Session["voilationList"];
                if (voilationList.List.Count == 0)
                {
                    voilationList = new VoilationList();
                    Session["voilationList"] = voilationList;
                    voilationList.Addviolation();
                }
                CreateVoilationControl();           
        }
        /// <summary>
        /// This method creates the Voilation User Control
        /// Add an event handler to this control to raise an event when the delete button is clicked 
        /// on the user control 
        /// Finally, add the user control to the panel 
        /// </summary>
        private void CreateVoilationControl()
        {            
                int i = 1;
                phVoilations.Controls.Clear();
                foreach (Voilation voilation in voilationList.List)
                {
                    VoilationControl = (UserControl_VoilationControl)LoadControl("~/UserControl/VoilationControl.ascx");
                    VoilationControl.ID = "ucVolilation" + voilation.UniqueId;
                    if (i > 1)
                    {
                        ((ImageButton)VoilationControl.FindControl("btnRemove")).Visible = true;
                    }

                    VoilationControl.RemoveUserControl += this.HandleRemoveVoilationControl;
                    VoilationControl.CreateDateControl += this.AddDateControl;
                    VoilationControl.CreateNewControl += this.InitializeDateControl;
                    PopulateVoilationData(voilation, VoilationControl);

                    phVoilations.Controls.Add(VoilationControl);
                    i++;
                }            
        }      
        
        private void PopulateVoilationData(Voilation voilation, Control crtVoilation)
        {           
                Charge charges = voilation.Charge;
                ((HiddenField)crtVoilation.FindControl("hdnUniqueID")).Value = voilation.UniqueId.ToString();
                ((HiddenField)crtVoilation.FindControl("hdnComplaintID")).Value = voilation.ComplaintId.ToString();
                if (charges != null)
                {
                    ((HiddenField)crtVoilation.FindControl("hdnOldChargeDesc")).Value = "";
                    ((TextBox)crtVoilation.FindControl("txtCharge")).Text = charges.ChargeDescription.ToString();
                      ((HiddenField)crtVoilation.FindControl("hdnOldChargeDesc")).Value = charges.ChargeDescription.ToString();
                    ((HiddenField)crtVoilation.FindControl("hdnChargeRefNo")).Value = charges.ChargeCode.ToString();
                                 ((ImageButton)crtVoilation.FindControl("btnUpload")).Enabled = true;

                    ((ImageButton)crtVoilation.FindControl("btnUpload")).Attributes.Add("Onclick", "javascript:window.open('Upload.aspx?ChildID1=" + charges.ChargeCode.ToString() + "&Mode=" + Session["Mode"].ToString() + "&ParentID=" + voilation.ComplaintId.ToString() + "','UpLoad','scrollbars=yes,resizable=yes,width=400,height=200').focus();return false;");

                }
                
                IncedentList incidents = voilation.Incidents;



                if (incidents != null)
                {
                    
                    if (incidents.List.Count > 0)
                    {

                        switch (voilation.IncidentType)
                        {
                            case "S":
                                foreach (Incedent incident in incidents.List)
                                {
                                    PlaceHolder pHolder = (PlaceHolder)crtVoilation.FindControl("phSingle");
                                    pHolder.Visible = true;
                                    if (incident.IncidentDate != DateTime.MinValue)                                        
                                        ((TextBox)pHolder.FindControl("txtSingleDt")).Text = String.Format("{0:MM/dd/yyyy}", incident.IncidentDate);
                                        ((TextBox)pHolder.FindControl("txtSingleDt")).Visible = true;
                                }
                                break;
                            case "R":
                                int cnt = 1;
                                foreach (Incedent incident in incidents.List)
                                {
                                    PlaceHolder pHolder = (PlaceHolder)crtVoilation.FindControl("phRange");
                                    pHolder.Visible = true;
                                    if (cnt == 1)
                                    {
                                        if (incident.IncidentDate != DateTime.MinValue)
                                            ((TextBox)pHolder.FindControl("txtStartDt")).Text = Convert.ToString(incident.IncidentDate.ToShortDateString());
                                    }
                                    else
                                    {
                                        if (incident.IncidentDate != DateTime.MinValue)
                                            ((TextBox)pHolder.FindControl("txtEndDt")).Text = Convert.ToString(incident.IncidentDate.ToShortDateString());
                                    }
                                    cnt++;
                                }
                                break;
                            case "M":
                                if (incidents != null)
                                {
                                    if (incidents.List.Count > 0)
                                    {
                                        PlaceHolder pHolder = (PlaceHolder)crtVoilation.FindControl("phMultiple");
                                        pHolder.Visible = true;
                                        CreateIncidentDtControl(crtVoilation, incidents);
                                    }
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    else
                    {
                        switch (voilation.IncidentType)
                        {
                            case "S":
                                PlaceHolder pSHolder = (PlaceHolder)crtVoilation.FindControl("phSingle");
                                pSHolder.Visible = true;
                                break;
                            case "R":
                                PlaceHolder pRHolder = (PlaceHolder)crtVoilation.FindControl("phRange");
                                pRHolder.Visible = true;
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    PlaceHolder pHolder = (PlaceHolder)crtVoilation.FindControl("phSingle");
                    pHolder.Visible = true;
                }           
        }


        /// <summary>
        //This handles delete event fired from the user control 
        //Get the user control that fired this event, and remove it        
        //Remove that Witness from the WitnessList.
        //Remove the Control from the Container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemoveVoilationControl(object sender, EventArgs e)
        {
            
                bool removePannel=false;
                ImageButton but = sender as ImageButton;
                UserControl_VoilationControl VoilationControl = (UserControl_VoilationControl)but.Parent;       

                Int64 UniqueID = Convert.ToInt64((((HiddenField)VoilationControl.FindControl("hdnUniqueID")).Value));
                string ChargeID = ((HiddenField)VoilationControl.FindControl("hdnChargeRefNo")).Value;              

                if (ChargeID == "")
                {
                    voilationList.Removevoilation(UniqueID);
                    phVoilations.Controls.Remove(VoilationControl);
                    return;
                }

                List<UploadFile> upLoadDTO = new List<UploadFile>();
                if (Session["upLoadDTO"] != null)
                    upLoadDTO = (List<UploadFile>)Session["upLoadDTO"];
               
                if (upLoadDTO.Count != 0)
                {
                    foreach (UploadFile upFile in upLoadDTO)
                    {
                        if ((Session["upLoadDTO"]) != null)
                        {
                            for (int i = 0; i <= upLoadDTO.Count - 1; i++)
                            {
                                if (upLoadDTO[i].ChildID == ChargeID)
                                {
                                    removePannel = true;
                                    break;
                                }
                            }
                            if (removePannel == true)
                            {
                                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SomestartupScript", " alert(' Please clear supporting documents related to this charges. ');", true);
                                return;
                            }
                            else
                            {
                                voilationList.Removevoilation(UniqueID);
                                phVoilations.Controls.Remove(VoilationControl);
                                return;
                            }

                            //if (upFile.PanelId == UniqueID)
                            //{
                            //    upLoadDTO.Remove(upFile);

                            //    Session["upLoadDTO"] = upLoadDTO;
                            //    voilationList.Removevoilation(UniqueID);
                            //    phVoilations.Controls.Remove(VoilationControl);
                            //    break;
                            //}
                        }

                        if ((upFile.ChildID == ChargeID) && (upFile.AttachmentName != null))
                        {
                            if ((Session["Pannel"]) != null)
                            {
                                if (Convert.ToInt64(((ArrayList)Session["Pannel"])[0]) == UniqueID)
                                    ((ArrayList)Session["Pannel"]).Remove(UniqueID);
                                voilationList.Removevoilation(UniqueID);
                                phVoilations.Controls.Remove(VoilationControl);
                                Session["Pannel"] = null;
                                Session.Remove("Pannel");
                                return;
                            }

                            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "SomestartupScript", " alert(' Please clear the supporting document by hitting the upload button ');", true);

                            return;
                        }
                        else if ((upFile.ChildID == ChargeID) && (upFile.AttachmentName == null))
                        {

                            voilationList.Removevoilation(UniqueID);
                            phVoilations.Controls.Remove(VoilationControl);
                            return;
                        }
                    }
                    foreach (UploadFile upFile in upLoadDTO)
                    {
                        if ((Session["Pannel"]) != null)
                        {
                            ((ArrayList)Session["Pannel"]).Remove(UniqueID);
                            voilationList.Removevoilation(UniqueID);
                            phVoilations.Controls.Remove(VoilationControl);
                            Session["Pannel"] = null;
                            Session.Remove("Pannel");

                            return;

                        }
                    }
                }
                else
                {
                    voilationList.Removevoilation(UniqueID);
                    phVoilations.Controls.Remove(VoilationControl);
                    return;
                }           
           
        }

        #region Incident Control
        /// <summary>
        /// This method AddIncidentControl to the list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void InitializeDateControl(object sender, EventArgs e)
        {           
                UserControl_VoilationControl VoilationControl;
                DropDownList but = sender as DropDownList;
                VoilationControl = (UserControl_VoilationControl)but.Parent;
                Int64 UniqueID = Convert.ToInt64((((HiddenField)VoilationControl.FindControl("hdnUniqueID")).Value));
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == UniqueID; });
                incidentList = new IncedentList();
                incidentList.AddIncident();
                voilation.Incidents = incidentList;
                CreateIncidentDtControl(VoilationControl, incidentList);            
        }
        /// <summary>
        /// This method AddIncidentControl to the list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AddDateControl(object sender, EventArgs e)
        {           
                UserControl_VoilationControl VoilationControl;
                ImageButton but1 = sender as ImageButton;
                VoilationControl = (UserControl_VoilationControl)but1.Parent.Parent;

                Int64 UniqueID = Convert.ToInt64((((HiddenField)VoilationControl.FindControl("hdnUniqueID")).Value));
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == UniqueID; });
                if (voilation.Incidents == null)
                {
                    incidentList = new IncedentList();
                }
                else
                    incidentList = voilation.Incidents;

                incidentList.AddIncident();
                voilation.Incidents = incidentList;
                CreateIncidentDtControl(VoilationControl, incidentList);            
        }

        /// <summary>
        /// Mannually Delete controls from ph. Other than the Add Button
        /// Mannual Removal of controls ends...
        /// Add an event handler to this control to raise an event when the delete button is clicked 
        /// on the user control 
        /// Finally, add the user control to the panel 
        /// </summary>
        /// <param name="Voilation"></param>
        /// <param name="incidentList"></param>
        private void CreateIncidentDtControl(Control Voilation, IncedentList incidentList)
        {
            
                int i = 1;
                PlaceHolder ph = (PlaceHolder)Voilation.FindControl("phMultiple");
                ph.Visible = true;
                List<Control> obj = new List<Control>();
                foreach (Control crt in ph.Controls)
                {
                    if (crt is UserControl_IncidentDateControl)
                    {
                        obj.Add(crt);
                    }
                }
                foreach (Control crt in obj)
                {
                    ph.Controls.Remove(crt);
                }
                foreach (Incedent incident in incidentList.List)
                {
                    IncidentDateControl = (UserControl_IncidentDateControl)LoadControl("~/UserControl/IncidentDateControl.ascx");
                    IncidentDateControl.ID = "ucIncidentDt" + incident.UniqueId;
                    if (i > 1)
                    {
                        ((ImageButton)IncidentDateControl.FindControl("btnRemoveDate")).Visible = true;
                    }
                    IncidentDateControl.RemoveDateControl += this.HandleRemoveDateControl;
                    IncidentDateControl.UpdateDateControl += this.HandleUpdateDateControl;
                    if (incident.IncidentDate != DateTime.MinValue)
                        ((TextBox)IncidentDateControl.FindControl("txtIncidentDt")).Text = incident.IncidentDate.ToShortDateString();
                    ((HiddenField)IncidentDateControl.FindControl("hdnUniqueID")).Value = incident.UniqueId.ToString();
                    ph.Controls.Add(IncidentDateControl);
                    i++;
                }            
        }
        /// <summary>
        /// This method..
        /// Get the reference of the Parent control and check which control is this by UniqueID.
        /// Get the voilation from the voilation List.
        /// Get the value of each control and add it to the Voilation class Incidents property
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleUpdateDateControl(object sender, EventArgs e)
        {
           
                Incedent incident;
                TextBox txtBox = sender as TextBox;
                UserControl_IncidentDateControl IncidentControl = (UserControl_IncidentDateControl)txtBox.Parent;
                PlaceHolder ph = (PlaceHolder)IncidentControl.Parent;
                UserControl_VoilationControl VoilationCrtl = (UserControl_VoilationControl)ph.Parent;
                Int64 VoilationUniqueID = Convert.ToInt64((((HiddenField)VoilationCrtl.FindControl("hdnUniqueID")).Value));
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == VoilationUniqueID; });
                IncedentList incidentLst = voilation.Incidents;
                incidentLst.List.Clear();
                foreach (Control crt in ph.Controls)
                {
                    if (crt is UserControl_IncidentDateControl)
                    {
                    incident = new Incedent
                    {
                        UniqueId = Convert.ToInt16(((HiddenField)crt.FindControl("hdnUniqueID")).Value)
                    };
                    if (((TextBox)crt.FindControl("txtIncidentDt")).Text != String.Empty)
                            if (Common.IsDate(((TextBox)crt.FindControl("txtIncidentDt")).Text))
                                incident.IncidentDate = Convert.ToDateTime(((TextBox)crt.FindControl("txtIncidentDt")).Text);

                        incidentLst.List.Add(incident);
                    }
                }
                voilation.Incidents = incidentLst;           
        }
        /// <summary>
        /// This method..
        /// Get the user control that fired this event, and remove it.
        /// Get the reference of the Parent control and check which control is this by UniqueID.
        /// Get the voilation from the voilation List.
        /// Remove that Incident from the IncidentList.
        /// Finally Remove the Control from the Container.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemoveDateControl(object sender, EventArgs e)
        { 
                ImageButton but = sender as ImageButton;
                UserControl_IncidentDateControl IncidentControl = (UserControl_IncidentDateControl)but.Parent;
                PlaceHolder ph = (PlaceHolder)IncidentControl.Parent;
                UserControl_VoilationControl VoilationCrtl = (UserControl_VoilationControl)ph.Parent;
                Int64 VoilationUniqueID = Convert.ToInt64((((HiddenField)VoilationCrtl.FindControl("hdnUniqueID")).Value));
                Voilation voilation = voilationList.List.Find(delegate(Voilation p) { return p.UniqueId == VoilationUniqueID; });
                IncedentList incidentLst = voilation.Incidents;
                Int64 UniqueID = Convert.ToInt64((((HiddenField)IncidentControl.FindControl("hdnUniqueID")).Value));
                incidentLst.RemoveIncident(UniqueID);
                ph.Controls.Remove(IncidentControl);            
        }
        #endregion

        #endregion

        #endregion

        #region Populate Dropdowns
        /// <summary>
        /// This method Populate the Location & Borough Dropdowns
        /// </summary>
        private void loadDropdownList()
        {           
                loadLocation();
                loadBorough();            
        }
        /// <summary>
        /// This method Populate the Location List
        /// </summary>
        private void loadLocationList()
        {           
                locationList = new LocationList();

                if (Session["LocationList"] == null)
                {
                    locationList.Load();
                    Session["LocationList"] = locationList.List;
                }
                else
                {
                    locationList.List = (List<LocationDS>)Session["LocationList"];

                }
            
        }
        /// <summary>
        /// This method Populate the Borough List
        /// </summary>
        private void loadBoroughList()
        {
            boroughList = new LocationList();
                if (Session["BoroughList"] == null)
                {
                    boroughList.Load(true);
                    Session["BoroughList"] = boroughList.List;
                }
                else
                {
                    boroughList = new LocationList();
                    boroughList.Load(true);
                }           
        }
        /// <summary>
        /// This method loads the Location List
        /// </summary>
        private void loadLocation()
        {         
                System.Web.UI.WebControls.ListItem item;
                foreach (LocationDS location in locationList.List)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = location.LocationId.ToString(),
                    Text = location.LocationName.ToString()
                };
                ddlLocation.Items.Add(item);
                }            
        }
        /// <summary>
        /// This method loads the Borough List
        /// </summary>
        private void loadBorough()
        {            
                System.Web.UI.WebControls.ListItem item;
                foreach (LocationDS location in boroughList.List)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = location.LocationId.ToString(),
                    Text = location.LocationName.ToString()
                };
                ddlBorough.Items.Add(item);
                }           
        }

        
        #endregion

        #region Method Helper
        /// <summary>
        /// This method displays the button based on DistrictApprover and BoroApprover
        /// </summary>
        private void DisplayButtonBasedOnPermission()
        {
            if (User.IsInRole("DistrictApprover") || User.IsInRole("BoroApprover"))
            {
                ibApprove.Visible = true;
                ibReject.Visible = true;
                ibSubmit.Visible = false;
            }
            else
            {
                ibSubmit.Visible = true;
                ibApprove.Visible = false;
                ibReject.Visible = false;
            }
            if (!User.IsInRole("Print"))
            {
                ibPrint.Visible = false;

            }

        }
        /// <summary>
        /// This method returns the employee based on the search criteria.
        /// </summary>
        /// <param name="prefixText"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string[] SearchEmployee(string prefixText, int count)
        {           
                ArrayList arrEmp = new ArrayList();
                PersonnelList pList = new PersonnelList();
                pList.Load(prefixText);
                if (pList.List.Count > 0)
                {
                    foreach (Personnel personnel in pList.List)
                    {
                        arrEmp.Add(personnel.ByLastName + " " + personnel.ReferenceNumber);
                    }
                }
                return (string[])arrEmp.ToArray(typeof(string));           
        }

        /// <summary>
        /// This method returns the charges based on the search criteria.
        /// </summary>
        /// <param name="prefixText"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string[] GetCharges(string prefixText, int count)
        {             
                ArrayList arrCharge = new ArrayList();
                ChargesList cList = new ChargesList();               
                cList.Load(prefixText);                
                if (cList.ChargeList.Count > 0)
                {                    
                    foreach (Charge charge in cList.ChargeList)
                    {
                        arrCharge.Add(charge.ChargeCode + " " + charge.ChargeDescription);                              
                    }
                }
                return (string[])arrCharge.ToArray(typeof(string));          

        }
        protected void ActivateFileUplaod()
        {
            ((Button)VoilationControl.FindControl("btnUpload")).Enabled = true;
        }
        /// <summary>
        /// This method returns the title based on the search criteria.
        /// </summary>
        /// <param name="prefixText"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        [System.Web.Script.Services.ScriptMethod]
        [System.Web.Services.WebMethod]
        public static string[] GetTitles(string prefixText, int count)
        {            
                ArrayList arrTitle = new ArrayList();
                TitleList cList = new TitleList();
                cList.Load(prefixText);

                if (cList.List.Count > 0)
                {
                    foreach (Title title in cList.List)
                    {
                        arrTitle.Add(title.TitleDescription + " " + title.TitleId);
                    }
                }
                return (string[])arrTitle.ToArray(typeof(string));            

        }

        /// <summary>
        /// This method...
        /// Populate Witnessess Data
        /// Remove all the List Classes and access from Complaint class.
        /// Populate Complainant Data
        /// Populate Voilations Data
        /// and finally Create User controls based on DB.
        /// </summary>
        private void LoadComplaintData()
        {
           
                Int64 ID = Convert.ToInt64(Request.QueryString["Id"]);
                complaint.Load(ID, RequestSource.DS249);
                if (complaint.ComplaintId != -1)
                {
                    BindComplaintData();

                    witnessList = new WitnessList();
                    witnessList = complaint.Witnesses;

                    witnessList.NewUniqueId = Convert.ToInt16(witnessList.List.Count);
                    Session["witnessList"] = witnessList;

                    complainantList = new ComplainantList();
                    complainantList = complaint.Complainants;
                    complainantList.NewUniqueId = Convert.ToInt16(complainantList.List.Count);
                    Session["ComplainantList"] = complainantList;


                    voilationList = new VoilationList();
                    voilationList = complaint.Violations;
                    voilationList.NewUniqueId = Convert.ToInt16(voilationList.List.Count);
                    Session["voilationList"] = voilationList;
                    initializeUserControls();
                    LoadComplaintHistory();

                }
                else
                {
                    Response.Redirect("Dashboard.aspx");
                }            
        }
        /// <summary>
        /// This method Load Complaint History
        /// </summary>
        private void LoadComplaintHistory()
        {            
                gvHistory.DataSource = complaint.HistoryList.List;
                gvHistory.DataBind();            
        }
        /// <summary>
        /// This method loads the Associate Incident for Voilation
        /// </summary>
        private void AssociateIncidentToVoilation()
        {
            IncedentList lstIncident;          

                foreach (Voilation voilation in voilationList.List)
                {
                    if (voilation != null)
                    {
                        Int16 i = 1;
                        lstIncident = new IncedentList();
                        foreach (Incedent incident in incidentList.List)
                        {
                            if (incident.ChargeId == voilation.ChargeId)
                            {
                                incident.UniqueId = i;
                                lstIncident.List.Add(incident);
                                i++;
                            } 
                        }
                        i = (Int16)(i - 1);
                        lstIncident.NewUniqueId = i;
                        voilation.Incidents = lstIncident;
                    }
                }           
        }
        /// <summary>
        /// This method laods the personnel data and Update Personnel Info to complaint Object if New Complaint
        /// </summary>
        private void BindPersonnelData()
        {            
                hdnEmployeeID.Value = personnel.PersonnelId.ToString();
                txtSearch.Text = personnel.FullName.ToString();
                lblDate.Text = DateTime.Now.ToShortDateString();
                txtFirstName.Text = personnel.FirstName.ToString();
                txtMName.Text = personnel.MiddleName.ToString();
                txtLastName.Text = personnel.LastName.ToString();
                if (personnel.Dob != DateTime.MinValue)
                    txtDOB.Text = personnel.Dob.ToShortDateString();
                txtRefNumber.Text = personnel.ReferenceNumber.ToString();
                if (personnel.DsnyHireDate != DateTime.MinValue)
                    txtApptDate.Text = String.Format("{0:MM/dd/yyyy}",personnel.DsnyHireDate);
                if (personnel.EffectiveDate != DateTime.MinValue)
                    txtPromotionDate.Text = String.Format("{0:MM/dd/yyyy}", personnel.EffectiveDate);
                Address address = personnel.PersonnelAddress;
                if (Session["Mode"].ToString() == "NEW")
                {
                    if (address != null)
                    {
                        txtAptNo.Text = address.AptNo.ToString();
                        txtStreetNumber.Text = address.StreetNumber.ToString();
                        txtStreetName.Text = address.StreetName.ToString();
                        txtCity.Text = address.City.ToString();
                        txtState.Text = address.State.ToString();
                        txtZip.Text = address.ZipCode.ToString();
                        txtZipPrefix.Text = address.ZipSuffix.ToString();
                    }
                    //txtVacationSchedule.Text = personnel.VacationSchedule.ToString();
                    txtChartDay.Text = personnel.ChartNumber.ToString();
                    txtPayrollLoc.Text = personnel.PayCodeDescription.ToString();
                }
                //txtBadge.Text = personnel.BadgeNumber.ToString();
                txtTitle.Text = personnel.PersonnelTitle.ToString();
                if (personnel != null)
                    UpdatePersonnelDataToComplaint();
                ChangeControlStatus(true);
                txtSearch.Enabled = true;            
        }
        /// <summary>
        /// This method laods the complaint data.
        /// </summary>
        private void BindComplaintData()
        {
           
                lblIndexNumber.Text = complaint.IndexNo.ToString();
                ddlLocation.Items.FindByValue(complaint.LocationId.ToString()).Selected = true;
                ddlBorough.Items.FindByValue(complaint.BoroughId.ToString()).Selected = true;
                if (complaint.PromotionDate != DateTime.MinValue)
                    txtPromotionDate.Text = String.Format("{0:MM/dd/yyyy}", complaint.PromotionDate);
                txtComments.Text = complaint.Comments.ToString();
                chkProbation.Checked = complaint.IsProbation;
                hdnEmployeeID.Value = complaint.EmployeeId.ToString();
                txtSearch.Text = complaint.FullName.ToString();
                txtSearch.Enabled = false;
                lblDate.Text = DateTime.Now.ToShortDateString();
                txtFirstName.Text = complaint.RespondentFirstName.ToString();
                txtMName.Text = complaint.RespondentMiddleName.ToString();
                txtLastName.Text = complaint.RespondentLastName.ToString();
                if (complaint.RespondentDob != DateTime.MinValue)
                    txtDOB.Text = complaint.RespondentDob.ToShortDateString();
                txtRefNumber.Text = complaint.ReferenceNumber.ToString();
                if (complaint.RespondentAptDate != DateTime.MinValue)               
                txtApptDate.Text=String.Format("{0:MM/dd/yyyy}", complaint.RespondentAptDate);
                //txtBadge.Text = complaint.BadgeNumber.ToString();
                txtTitle.Text = complaint.TitleDescription.ToString();
                txtStreetNumber.Text = complaint.StreetNumber;
                txtStreetName.Text = complaint.StreetName;
                txtAptNo.Text = complaint.AptNo;
                txtCity.Text = complaint.City;
                txtState.Text = complaint.State;
                txtZip.Text = complaint.ZipCode;
                txtZipPrefix.Text = complaint.ZipSuffix;
                //txtVacationSchedule.Text = complaint.VacationSchedule;
                txtChartDay.Text = complaint.ChartNo;
                txtPayrollLoc.Text = complaint.PayCodeDescription;

                //  ************PEDING DOCUMENT************
                //if (complaint.RoutingLocation == "P")
                //{                
                //    hdnPendingValue.Value = "P";                
                //}
                //    else
                //{
                //    hdnPendingValue.Value = "";
                //}
            
        }
        /// <summary>
        /// This method laods the complaint data.
        /// </summary>
        private void UpdatePersonnelDataToComplaint()
        {
             complaint = (Complaint)Session["Complaint"];
                complaint.EmployeeId = personnel.PersonnelId;
                complaint.ReferenceNumber = personnel.ReferenceNumber;
                complaint.TitleId = personnel.TitleId;
                complaint.BadgeNumber = personnel.BadgeNumber;
                complaint.PayCodeId = personnel.PayCodeId;
                complaint.RespondentFirstName = personnel.FirstName;
                complaint.RespondentLastName = personnel.LastName;            
        }

        private void clearForm()
        {

        }



        /// <summary>
        /// Find the control that caused the postback. 
        /// I got this code from http://www.ryanfarley.com/blog/archive/2005/03/11/1886.aspx
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public Control GetPostBackControl(Page page)
        {
            Control control = null;
            string ctrlname = page.Request.Params.Get("__EVENTTARGET");
            if ((ctrlname != null) & ctrlname != string.Empty)
            {
                control = page.FindControl(ctrlname);
            }
            else
            {
                foreach (string ctl in page.Request.Form)
                {
                    Control c = page.FindControl(ctl);
                    if (c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break; 
                    }
                }
            }
            return control;
        }
        /// <summary>
        /// This method returns true if the complaints are succesfully saved 
        /// </summary>
        /// <param name="Approved"></param>
        private void SaveComplaint(Boolean Approved)
        {            
                if (complaint == null)
                {
                    complaint = (Complaint)Session["Complaint"];
                    UpdateComplaint(Approved);
                    if (complaint.Save(RequestSource.DS249, user.PersonalId) == true)
                    {
                        if (Approved)
                            complaint.UpdateComplaintStatus();

                        if (!Approved)
                        {
                            SaveComplaintAttachments();
                        }
                        else
                        {
                            if (Session["upLoadDTO"] != null)
                            {
                                SaveComplaintAttachments();
                            }
                            else
                            {
                                List<UploadFile> upLoadDTO;
                                UploadFile uf = new UploadFile();
                                upLoadDTO = uf.LoadComplaintAttachments(0, complaint.ComplaintId , string.Empty);
                                Session["upLoadDTO"] = upLoadDTO;
                                SaveComplaintAttachments();
                            }
                        }                        
                        lblIndexNumber.Text = complaint.IndexNo.ToString();
                        ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", complaint.ComplaintId.ToString()));
                        lbMessage.Text = "Complaint Information is Saved";
                        lbMessage.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        lbMessage.Text = "Complaint Information Could Not Be Saved";
                        lbMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }         

        }
        private void SaveComplaintAttachments()
        {
            List<UploadFile> upLoadDTO = new List<UploadFile>();
            if (Session["upLoadDTO"] != null)
                upLoadDTO = (List<UploadFile>)Session["upLoadDTO"];
            Int64 ParentID = complaint.ComplaintId;             
            UploadFile upFiledoc = new UploadFile();
            if (upLoadDTO.Count > 0)
            {
                upFiledoc.DeleteSupportedDocument(ParentID);
                foreach (UploadFile upFile in upLoadDTO)
                {
                    complaint.UploadFiles(ParentID, upFile.Image, "DS249", upFile.ContentType, upFile.AttachmentName, upFile.ChildID);
                }

            }
            else
            {
                upFiledoc.DeleteSupportedDocument(ParentID);
            }         

        }


        /// <summary>
        /// This method saves the DS249 Rejected Complaint
        /// </summary>
        /// <param name="Rejected"></param>
        private void SaveRejectedComplaint(Boolean Rejected)
        {          
                if (complaint == null)
                {
                    complaint = (Complaint)Session["Complaint"];
                    UpdateRejectedComplaint(Rejected);
                    if (complaint.Save(RequestSource.DS249, user.PersonalId) == true)
                    {
                        if (Rejected)
                            complaint.UpdateComplaintStatus();
                    }
                    else
                    {
                        lbMessage.Text = "Complaint Information Could Not Be Updated";
                        lbMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }            
        }

        /// <summary>
        /// This method Updates the DS249 Rejected Complaint
        /// </summary>
        /// <param name="Approved"></param>
        private void UpdateComplaint(bool Approved)
        {
           
                complaint.LocationId = Convert.ToInt64(ddlLocation.SelectedItem.Value);
                complaint.BoroughId = Convert.ToInt64(ddlBorough.SelectedItem.Value);
                if (txtPromotionDate.Text != String.Empty)
                    complaint.PromotionDate = Convert.ToDateTime(txtPromotionDate.Text);
                complaint.Witnesses = (WitnessList)Session["witnessList"];
                complaint.Complainants = (ComplainantList)Session["ComplainantList"];
                complaint.Violations = (VoilationList)Session["voilationList"];
                
                complaint.RespondentFirstName = txtFirstName.Text;
                complaint.RespondentMiddleName = txtMName.Text;
                complaint.RespondentLastName = txtLastName.Text;
                if (txtApptDate.Text != String.Empty)
                    complaint.RespondentAptDate = Convert.ToDateTime(txtApptDate.Text);
                if (txtDOB.Text != String.Empty)
                    complaint.RespondentDob = Convert.ToDateTime(txtDOB.Text);
                complaint.Comments = Convert.ToString(txtComments.Text);
                complaint.IsProbation = chkProbation.Checked;
                complaint.IsApproved = Approved;
                if (Approved && User.IsInRole("DistrictApprover"))
                    complaint.ComplaintStatus = 3;
                if (Approved && User.IsInRole("BoroApprover"))
                    complaint.ComplaintStatus = 4;
                //complaint.PendingDoc = Convert.ToString(hdnPendingDocs.Value);
                
                //************PENDING DOCUMENT************
                if (hdnPendingValue.Value == "P")
                    complaint.RoutingLocation = "P";
                else
                    complaint.RoutingLocation = String.Empty;          
            
                complaint.CreatedBy = Session["UserEmpID"].ToString();
                complaint.StreetNumber = Convert.ToString(txtStreetNumber.Text);
                complaint.StreetName = Convert.ToString(txtStreetName.Text);
                complaint.AptNo = Convert.ToString(txtAptNo.Text);
                complaint.City = Convert.ToString(txtCity.Text);
                complaint.State = Convert.ToString(txtState.Text);
                complaint.ZipSuffix = Convert.ToString(txtZipPrefix.Text);
                complaint.ZipCode = Convert.ToString(txtZip.Text);
                complaint.ChartNo = Convert.ToString(txtChartDay.Text);
                //complaint.VacationSchedule = Convert.ToString(txtVacationSchedule.Text);           
        }
        /// <summary>
        /// This method loads the list of Borough w.r.t location
        /// </summary>
        private void LoadBoroughByLocation()
        {           
                LocationDS location = locationList.List.Find(delegate(LocationDS p) { return p.LocationId == Convert.ToInt64(ddlLocation.SelectedItem.Value); });
                if (location != null)
                {
                    LocationDS borough = boroughList.List.Find(delegate(LocationDS q) { return q.LocationId == Convert.ToInt64(location.LocationParentId); });
                    if (borough != null)
                    {
                        String Borough = borough.LocationName.ToString();
                        ddlBorough.ClearSelection();
                        ddlBorough.Items.FindByText(Borough).Selected = true;
                    }
                    else
                        ddlBorough.SelectedIndex = -1;
                }
                else
                    ddlBorough.SelectedIndex = -1;           
        }
        /// <summary>
        /// This method clears the session
        /// </summary>
        private void ClearSessions()
        {
             Session["Complaint"] = null;
                Session["Personnel"] = null;
                Session["witnessList"] = null;
                Session["ComplainantList"] = null;
                Session["voilationList"] = null;            
        }
        /// <summary>
        /// This method updates the Ds249 Rejected Complaint
        /// </summary>
        /// <param name="Rejected"></param>
        private void UpdateRejectedComplaint(bool Rejected)
        {
           
                complaint.LocationId = Convert.ToInt64(ddlLocation.SelectedItem.Value);
                complaint.Witnesses = (WitnessList)Session["witnessList"];
                complaint.Complainants = (ComplainantList)Session["ComplainantList"];
                complaint.Violations = (VoilationList)Session["voilationList"];
                
                complaint.RespondentFirstName = txtFirstName.Text;
                complaint.RespondentMiddleName = txtMName.Text;
                complaint.RespondentLastName = txtLastName.Text;
                if (txtApptDate.Text != String.Empty)
                    complaint.RespondentAptDate = Convert.ToDateTime(txtApptDate.Text);
                if (txtDOB.Text != String.Empty)
                    complaint.RespondentDob = Convert.ToDateTime(txtDOB.Text);
                complaint.Comments = Convert.ToString(txtComments.Text);
                complaint.IsProbation = chkProbation.Checked;
                complaint.IsApproved = false;
                if (Rejected && User.IsInRole("DistrictApprover"))
                    complaint.ComplaintStatus = 1;
                if (Rejected && User.IsInRole("BoroApprover"))
                    complaint.ComplaintStatus = 2;
                
                //complaint.PendingDoc = Convert.ToString(hdnPendingDocs.Value);

               // ************PENDING DOCUMENT************

                if (hdnPendingValue.Value == "P")
                    complaint.RoutingLocation = "P";
                else
                    complaint.RoutingLocation = String.Empty;


                complaint.CreatedBy = Session["UserEmpID"].ToString();

                complaint.StreetNumber = Convert.ToString(txtStreetNumber.Text);
                complaint.StreetName = Convert.ToString(txtStreetName.Text);
                complaint.AptNo = Convert.ToString(txtAptNo.Text);
                complaint.City = Convert.ToString(txtCity.Text);
                complaint.State = Convert.ToString(txtState.Text);
                complaint.ZipSuffix = Convert.ToString(txtZipPrefix.Text);
                complaint.ZipCode = Convert.ToString(txtZip.Text);
                complaint.ChartNo = Convert.ToString(txtChartDay.Text);
                //complaint.VacationSchedule = Convert.ToString(txtVacationSchedule.Text);           
        }
        /// <summary>
        /// This method changes the the ststus of the controls are per membership roles
        /// </summary>
        /// <param name="status"></param>
        private void ChangeControlStatus(bool status)
        {
            
                if (Session["Mode"].ToString() == "NEW")
                {
                    ibSave.Enabled = status;
                    ibPrint.Enabled = status;
                    ibApprove.Enabled = status;
                    ibReject.Enabled = status;
                    ibSubmit.Enabled = status;
                }


                foreach (Control c in upComplaint.Controls)
                {
                    foreach (Control ctrl in c.Controls)
                    {
                        if (ctrl is TextBox)
                        {
                            txtPromotionDate.Enabled = status;
                            imgPromotionDate.Visible = !status;
                            ((TextBox)ctrl).Enabled = status;
                        }
                        else if (ctrl is Button)

                            ((Button)ctrl).Enabled = status;

                        else if (ctrl is RadioButton)

                            ((RadioButton)ctrl).Enabled = status;

                        else if (ctrl is ImageButton)

                            ((ImageButton)ctrl).Visible = status;
                        else if (ctrl is System.Web.UI.WebControls.Image)

                            ((System.Web.UI.WebControls.Image)ctrl).Visible = status;

                        else if (ctrl is CheckBox)

                            ((CheckBox)ctrl).Enabled = status;

                        else if (ctrl is DropDownList)

                            ((DropDownList)ctrl).Enabled = status;
                        else if (ctrl is HyperLink)
                            ((HyperLink)ctrl).Enabled = status;
                        else if (ctrl is PlaceHolder)
                        {
                            if (ctrl.ID == "pnWitnesses")
                            {
                                foreach (UserControl_WitnessControl control in ctrl.Controls)
                                {
                                    control.PartialEnable = status;
                                }
                            }

                            if (ctrl.ID == "pnComplainant")
                            {
                                foreach (UserControl_ComplainantControl control in ctrl.Controls)
                                {
                                    control.PartialEnable = status;
                                }
                            }


                            if (ctrl.ID == "phVoilations")
                            {
                                foreach (UserControl_VoilationControl control in ctrl.Controls)
                                {
                                    control.PartialEnable = status;
                                    foreach (Control child in control.Controls)
                                    {
                                        if (child.ID == "phMultiple")
                                        {
                                            if (child.Controls.Count > 2)
                                            {
                                                foreach (Control ctrIncident in child.Controls)
                                                {
                                                    if (ctrIncident is UserControl_IncidentDateControl)
                                                        ((UserControl_IncidentDateControl)ctrIncident).Enable = status;
                                                }
                                            }
                                        }
                                    }

                                }

                            }
                        }


                    }
                }          

        }



        #endregion

        #endregion

        #region Page Control Events
        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Session["Pannel"] = null;
            Session.Remove("Pannel");
            Response.Redirect("Dashboard.aspx");
        }



        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
              String refNo = Common.ExtractNumbers(txtSearch.Text.Trim());
                if (refNo != String.Empty)
                {
                    clearForm();

                    txtSearch.Text = txtSearch.Text.Replace(refNo, "");
                    personnel = new Personnel();
                    personnel.Load(refNo.Trim());
                    if (personnel.PersonnelId != -1)
                    {
                        Session["Personnel"] = personnel;
                        BindPersonnelData();
                        
                        complaintHistory = new ComplaintHistoryList();
                        complaintHistory.Load(personnel.PersonnelId, complaint.ComplaintId);
                        gvHistory.DataSource = complaintHistory.List;
                        gvHistory.DataBind();
                        DisplayButtonBasedOnPermission();
                    }
                    else
                    {
                        lbMessage.Text = "Information for this Employee Not Found.";
                        lbMessage.ForeColor = System.Drawing.Color.Red;
                        txtSearch.Text = String.Empty;
                    }


                }
                else
                {
                    txtSearch.Text = String.Empty;
                    lbMessage.Text = "Ref No. is missing for this employee.";
                    lbMessage.ForeColor = System.Drawing.Color.Red;
                }           
        }

        protected void ddlLocation_SelectedIndexChanged(object sender, EventArgs e)
        {                         

                LoadBoroughByLocation();
           
        }

        //Add Witness Control
        protected void btnAddWitness_Click(object sender, ImageClickEventArgs e)
        {          
                //Handled at page load function.
                AddWitnessControl();           
        }

        protected void btnAddWit_Click(object sender, EventArgs e)
        {
            //Handled on page load.
        }

        protected void ibAddComplainant_Click(object sender, ImageClickEventArgs e)
        {          
                AddComplainantControl();            
        }


        protected void ibSave_Click(object sender, ImageClickEventArgs e)
        {
            if (voilationList.List.Count != 0)
            {
                for (int i = 0; i <= voilationList.List.Count - 1; i++)
                {
                    if (String.IsNullOrWhiteSpace(voilationList.List[i].Charge.ChargeCode) == true)
                    {
                        lbMessage.Text = "Please enter valid charges.";
                        lbMessage.Font.Size = 10;
                        lbMessage.ForeColor = System.Drawing.Color.Red;
                        return;
                    }
                }
            }
            Session["Pannel"] = null;
            Session.Remove("Pannel");
            SaveComplaint(false);
        }     
       

        protected void ImageButton6_Click(object sender, ImageClickEventArgs e)
        {           
                AddVoilationControl();          
            
        }


        protected void Button1_Click(object sender, EventArgs e)
        {

        }

        protected void gvHistory_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    string strPenDesc = e.Row.Cells[5].Text.Trim();

                    if (strPenDesc == ",")
                    {
                        e.Row.Cells[5].Text = "";
                    }
                    else if (strPenDesc.Substring(0, 1) == ",")
                    {
                        e.Row.Cells[5].Text = strPenDesc.Substring(1, strPenDesc.Length - 1);
                    }
                    else if (strPenDesc.Contains("nbsp") == false)
                    {
                        if (strPenDesc.EndsWith(",") == true)
                        {
                            strPenDesc = strPenDesc.Remove(strPenDesc.Length - 1, 1);
                        }
                        strPenDesc += ".";
                        e.Row.Cells[5].Text = strPenDesc;
                    }



                    if (Request.QueryString["Mode"] == "VIEW")
                    {
                        e.Row.Cells[0].Visible = false;
                    }
                    else
                    {
                         e.Row.Cells[1].Visible = false;
                    }
                    string Desc = e.Row.Cells[4].Text.ToString();
                    Desc = Desc.Replace(", ", "\n");
                    e.Row.Cells[3].Attributes.Add("title", Desc);
                    e.Row.Cells[4].Visible = false;
                }
                
                else if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[4].Visible = false;

                    if (Request.QueryString["Mode"] == "VIEW")
                    {
                        e.Row.Cells[0].Visible = false;
                    }
                    else
                        e.Row.Cells[1].Visible = false;
                }           
        }

        #endregion

        /// <summary>
        /// This button click event saves the data of the complaint to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibSubmit_Click(object sender, ImageClickEventArgs e)
        {            
                SaveComplaint(false);
                complaint.ComplaintStatus = 2;
                complaint.PendingDoc = Convert.ToString(hdnPendingDocs.Value);
                //************PENDING DOCUMENT************
                if (hdnPendingValue.Value == "P")
                complaint.RoutingLocation = "P";
                complaint.UpdateComplaintStatus();
                ScriptManager.RegisterStartupScript(ibSubmit, this.GetType(), "msg", String.Format("<script>alert('Complaint {0} Successfully Submitted!');document.location ='Dashboard.aspx';</script>", complaint.IndexNo), false);          
        }
        /// <summary>
        /// This button click event saves the data of the aaproved complaint to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibApprove_Click(object sender, ImageClickEventArgs e)
        {            
                if (voilationList.List.Count != 0)
                {
                    for (int i = 0; i <= voilationList.List.Count - 1; i++)
                    {
                        if (String.IsNullOrWhiteSpace(voilationList.List[i].Charge.ChargeCode) == true)
                        {
                            lbMessage.Text = "Please enter valid charges.";
                            lbMessage.Font.Size = 10;
                            lbMessage.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                }
                Session["Pannel"] = null;
                Session.Remove("Pannel");
                SaveComplaint(true);


                if (User.IsInRole("BoroApprover"))
                    Response.Redirect(String.Format("ViewComplaintDetails.aspx?Id={0}&Mode=APPROVE", complaint.ComplaintId.ToString()));
                else
                    ScriptManager.RegisterStartupScript(ibApprove, this.GetType(), "msg", String.Format("<script>alert('Complaint {0} Successfully Approved!');document.location ='Dashboard.aspx';</script>", complaint.IndexNo), false);


                //if (hdnPendingValue.Value == "P")
                //{
                //    Session["Mode"] = "EDIT";
                //}
                //else
                //{
                //    if (User.IsInRole("BoroApprover"))
                //        Response.Redirect(String.Format("ViewComplaintDetails.aspx?Id={0}&Mode=APPROVE", complaint.ComplaintId.ToString()));
                //    else
                //        ScriptManager.RegisterStartupScript(ibApprove, this.GetType(), "msg", String.Format("<script>alert('Complaint {0} Successfully Approved!');document.location ='Dashboard.aspx';</script>", complaint.IndexNo), false);
                //}
           }

        /// <summary>
        /// This button click event saves the rejected complaint data to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Reject_Click(object sender, ImageClickEventArgs e)
        {           
                Session["Pannel"] = null;
                Session.Remove("Pannel");
                SaveRejectedComplaint(true);
                ScriptManager.RegisterStartupScript(ibApprove, this.GetType(), "msg", String.Format("<script>alert('Complaint {0} Successfully Returned!');document.location ='Dashboard.aspx';</script>", complaint.IndexNo), false);
            
        }       

    }
}