﻿<%@ Page Language="C#" AutoEventWireup="true"  Inherits="DSNY.DSNYCP.DS249.Complaints_PrintComplaint" Codebehind="PrintComplaint.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">    
    <title>Print Complaint</title>   
    
    <style type="text/css">
        .SOR
        {
        	font-family:Helvetica; 
        	font-size:1.3em; 
        	font-weight:400; 
        	text-align:left;
        	margin-left:25px;
        	margin-right:25px;
        	letter-spacing:1;  
        	line-height: 1.6em;
        }
        .tdWidth
        {
        }
        
        .style2
        {
        height: 39px;
        }
        .style3
        {
            height: 35px;
        }
        .style4
        {
            height: 162px;
        }
        .style5
        {
            height: 53px;
        }
        .style6
        {
            height: 239px;
        }
        .style7
        {
            height: 46px;
        }
        .style8
        {
            height: 47px;
        }
        .style9
        {
            height: 43px;
        }
        .style12
        {
            height: 24px;
        }
        .style13
        {
            height: 35px;
        }
        .style16
        {
            width: 445px;
        }
        .style18
        {
            width: 20px;
        }
        .style20
        {
            height: 24px;
            width: 92px;
        }
        .style21
        {
            height: 39px;
            width: 92px;
        }
        .style22
        {
            height: 35px;
            width: 92px;
        }
        .style23
        {
            width: 50px;
        }
        .style29
        {
            height: 39px;
            width: 146px;
        }
        .style31
        {
            height: 39px;
            width: 106px;
        }
        .style32
        {
            width: 146px;
        }
        .style34
        {
            height: 119px;
        }
        .style36
        {
            height: 48px;
        }
        .style37
        {
            width: 184px;
        }
        .style38
        {
            height: 323px;
        }
        .style39
        {
            height: 340px;
        }
        .style40
        {
            height: 310px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div id="divPage1" runat="server">
        <table id="tblPage1" align="center" cellpadding="2px" cellspacing="0px" style="background-position: center center; width:98%; font-family: Verdana; vertical-align: top; font-size: 1em; text-align: left;font-style: normal;">
            <tr>
                <td class="tdWidth" valign="middle" style="border-right-style: none; border-bottom-style: none; border-width: 1px; border-color: #000000; font-size: x-large; font-family: Arial, Helvetica, sans-serif; font-weight: bold;" 
                    colspan="3">
                    <asp:Label ID="lbHeader" runat="server" Text="Label"></asp:Label>
                    </td>
               
               <td style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    class="tdWidth" valign="top">
                    DATE<br /><br />
                    <asp:Label ID="lbDate" runat="server"></asp:Label>
                    </td>
                     <td style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    class="tdWidth" valign="top">
                    PROMOTION DATE<br />
                    <br />
                    <asp:Label ID="lblPromotionDate" runat="server"></asp:Label>
                    </td>
                <td style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    class="tdWidth" valign="top">
                    INDEX NO.<br />
                    <br />
                     <asp:Label ID="lbIndexNo" runat="server" ></asp:Label><br /></td>
            </tr>
            <tr>
                <td class="style32" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    BOROUGH (PAYROLL)<br />
                    <asp:Label ID="lbPayroll" runat="server" ></asp:Label><br />
                </td>
                <td class="style12" colspan="2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    DISTRICT/DIVISION<br />
                    <br />
                    <asp:Label ID="lbDistrict" runat="server" ></asp:Label><br /></td>
                <td class="style20" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    SSN<br />
                    <br /><asp:Label ID="lbSSN" runat="server" ></asp:Label><br /></td>
                <td class="style12" colspan="2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    EMPLOYEE REFERENCE NO.<br />
                    <br />
                    <asp:Label ID="lbRefNo" runat="server" ></asp:Label><br /></td>
            </tr>
            <tr>
                <td class="style29" 
                    style="border-style: solid none none solid; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    NAME(LAST)<br />
                    <br />
                    <asp:Label ID="lbLName" runat="server" ></asp:Label><br />
                </td>
                <td class="style2" 
                    style="border-style: solid none none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    (FIRST)<br />
                    <br />
                    <asp:Label ID="lbFName" runat="server" ></asp:Label><br /></td>
                <td class="style31" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    (MI)<br />
                    <br />
                    <asp:Label ID="lbMName" runat="server" ></asp:Label><br /></td>
                <td class="style21" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    &nbsp;
                    <br />
                    <asp:Label ID="lbBadge" runat="server" ></asp:Label><br />
                </td>
                <td class="style2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    TITLE<br />
                    <br />
                    <asp:Label ID="lbTitle" runat="server" ></asp:Label></td>
                <td class="style2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    DATE OF BIRTH<br />
                    <br />
                    <asp:Label ID="lbDOB" runat="server" ></asp:Label><br /></td>
            </tr>
            <tr>
                <td class="style3" colspan="3" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    HOME ADDRESS<br />
                    <br />
                    <asp:Label ID="lbAddress" runat="server" ></asp:Label><br />
                </td>
                <td class="style22" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    APT<br />
                    <br />
                   <asp:Label ID="lbApt" runat="server" ></asp:Label><br />
                </td>
                <td class="style3" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    BOROUGH<br />
                    <br />
                    <asp:Label ID="lbBorough" runat="server" ></asp:Label><br />
                </td>
                <td class="style3" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    ZIP CODE<br />
                    <br />
                    <asp:Label ID="lbZip" runat="server" ></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="style13" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    valign="top" colspan="2">
                    &nbsp;<br />
                </td>
                <td class="style13" colspan="2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    CHART DAY NO.<br />
                    <br /><asp:Label ID="lbChartNo" runat="server" ></asp:Label><br />
                </td>
                <td class="style3" colspan="1" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    APPOINTMENT<br />
                    <br />
                    <asp:Label ID="lbApptDt" runat="server" ></asp:Label><br />
                </td>
                  <td class="style3" colspan="1" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top" nowrap="nowrap"> 
                      <br />
                     <asp:CheckBox ID="chkProbation" runat="server" Text="Employee Under Probation"
                                                        SkinID="" Font-Bold="true"/>                               <br />
                </td>


            </tr>
           
            <tr>
                <td valign="top" class="style6" colspan="6" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;">
                    RULES AND ORDERS VIOLATED AS FOLLOWS (LIST RULE NOS.)<br />
                    <asp:Panel ID="pnVoilations" runat="server">
                    </asp:Panel>                    
               </td>
            </tr>
             <tr>
                <td valign="top" class="style5" colspan="6" 
                     style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;">
                    GENERAL COMMENTS:<asp:Label ID="lbComments" runat="server" ></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td class="style4" valign="top" colspan="6" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;">
                    COMPLAINANT STATEMENT (SPECIFIC ACTS WHICH CONSTITUTE VIOLATIONS)<br />
                    <table style="width:100%; height: 141px;">
                        <tr>
                            <td valign="top" class="style37" >                            
                                <asp:Label ID="lbComplainant" runat="server"></asp:Label>
                            </td>
                            <td valign="top">
                                <asp:Label ID="lbStatement" runat="server" Height="210px" 
                                    style="margin-left: 3px; " Width="719px"></asp:Label>
                              </td>
                        </tr>
                         
                    </table>
                    </td>
            </tr>
            <tr>
                <td valign="top" class="style7" colspan="5" 
                    
                    style="border-style: solid none none solid; border-width: 1px; border-color: #000000;">
                    RESPONDENT SERVES WITH CHARGES AND STATEMENT OF RIGHTS BY<br />
                    <br />
                   <asp:Label ID="lbChargesServedBy" runat="server" ></asp:Label>
                </td>
                <td valign="top" class="style7" 
                    
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;">
                    DATE<br />
                    <br />
                    <asp:Label ID="lbChargesServerDt" runat="server" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="3" 
                    style="border-style: solid none none solid; border-width: 1px; border-color: #000000;" 
                    class="style8">
                    ACKNOWLEDGEMENT OF RECEIPT OF CHARGES
                    <br />
                    AND STATEMENT OF RIGHTS</td>
                <td valign="top" colspan="2" 
                    style="border-style: solid none none none; border-width: 1px; border-color: #000000;" 
                    class="style8">
                    RESPONDENT&#39;S SIGNATURE<br />
                    <br />
                    <br />
                </td>
                <td valign="top" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    class="style8">
                    DATE<br />
                </td>
            </tr>
            <tr>
                <td valign="top" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    colspan="3">
                    <table>
                        <tr>
                            <td valign="top" class="style18">
                                <table style="border: medium solid #000000; width: 27px;">
                                    <tr>
                                        <td valign="top" class="style23">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="style16" 
                                style="font-size: 12pt; font-family: verdana">
                                <table>
                                    <tr>
                                        <td colspan="2">
                                            Respondent REFUSED to Sign and REFUSED Copy
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:70%;">
                                        Served By<br />
                                            <asp:Label ID="lbServerBy1" runat="server"></asp:Label>
                                        </td>
                                        <td style="width:30%;">
                                            Date<br />
                                            <asp:Label ID="lbServerDt1" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>                       
                    </table>
                </td>
                <td valign="top" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    colspan="3">
                     <table>
                        <tr>
                            <td valign="top">
                                <table style="border: medium solid #000000; width: 23px;">
                                    <tr>
                                        <td valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="style16" 
                                style="font-family: verdana; font-size: 12pt">
                                 <table>
                                    <tr>
                                        <td colspan="2">
                                            Respondent REFUSED to Sign and ACCEPTED Copy
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:70%;">
                                        Served By<br />
                                            <asp:Label ID="lbServerBy2" runat="server"></asp:Label>
                                        </td>
                                        <td style="width:30%;">
                                            Date<br />
                                            <asp:Label ID="lbServerDt2" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>                                
                            </td>
                        </tr>                       
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    colspan="3" class="style9">
                    COMPLAINANT&#39;S NAME<br />
                    <br />
                    <asp:Label ID="lbComplainantName" runat="server" ></asp:Label><br /></td>
                <td valign="top" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    colspan="3" class="style9">
                    COMPLAINANT&#39;S SIGNATURE</td>
            </tr>
            <tr>
                <td valign="top" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    colspan="3">
                    LOCATION<br />
                    <br />
                    <asp:Label ID="lbComplainantLocation" runat="server" ></asp:Label><br /></td>
                <td valign="top" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    colspan="3">
                    TITLE<br />
                    <br />
                    <asp:Label ID="lbComplainantTitle" runat="server" ></asp:Label><br /></td>
            </tr>
            <tr>
                 <td valign="top" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    colspan="3">
                    &nbsp;</td> 
                <td valign="top" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    colspan="3">
                    WITNESS(1)<br />
                    <br />
                    <asp:Label ID="lbWitness1" runat="server" ></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td valign="top" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    colspan="3">                    
                    <table>
                        <tr>
                            <td style="width: 70%;">
                                SUSPENDED BY ORDER OF<br />
                                <asp:Label ID="lbSuspensionBy" runat="server" ></asp:Label>
                            </td>
                            <td style="width: 30%;" align="center">
                                Date<br />
                                <asp:Label ID="lbSuspensionByDt" runat="server" Width="120px" ></asp:Label>
                            </td>
                        </tr>
                    </table>                    
                    </td>
                <td valign="top" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    colspan="3">
                    WITNESS(2)<br />
                    <br />
                    <asp:Label ID="lbWitness2" runat="server" ></asp:Label><br />
                </td>
            </tr>
            <tr>
                <td valign="top" style="border: 1px solid #000000" colspan="3">
                    
                    <table>
                        <tr>
                            <td style="width: 70%;">
                                SUSPENSION LIFTED BY ORDER OF<br />
                                <asp:Label ID="lbSuspensionLiftedBy" runat="server" ></asp:Label>
                            </td>
                            <td style="width: 30%;" align="center">
                                Date<br />
                                <asp:Label ID="lbSuspensionLiftedDt" runat="server" Height="18px" 
                        Width="120px" ></asp:Label>
                            </td>
                        </tr>
                    </table> 
                    
                    </td>
                <td valign="top" 
                    style="border-style: solid solid solid none; border-width: 1px; border-color: #000000;" 
                    colspan="3">
                    WITNESS(3)<br />
                    <br />
                    <asp:Label ID="lbWitness3" runat="server" ></asp:Label><br />
                </td>                
            </tr>
           
        </table>                
        </div>
         
        
        <div id="divPage2" runat="server" align="center">   
    <asp:Panel ID="Panel1" runat="server" BorderStyle="None" BorderColor="White">
   
        <table align="center" cellpadding="2px" cellspacing="0px" style="page-break-before:always;background-position: center center; width:98%; font-family: Verdana; vertical-align: top; font-size: 1em; text-align: left; font-style: normal;">
            <tr>
                <td class="style36" valign="middle" style="border-right-style: none; border-bottom-style: none; border-width: 1px; border-color: #000000; font-size: x-large; font-family: Arial, Helvetica, sans-serif; font-weight: bold;" 
                    colspan="3">
                    <asp:Label ID="lbHeader2" runat="server" Text="Label"></asp:Label>
                    </td>
               
                <td style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    class="tdWidth" valign="top">
                     DATE<br /><br />
                     <asp:Label ID="lbDate2" runat="server"></asp:Label>
                    </td>
                     <td style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    class="style36" valign="top">
                    PROMOTION DATE<br />
                    <br />
                     <asp:Label ID="lblPromotiondate2" runat="server"></asp:Label>
                    </td>
                <td style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    class="style36" valign="top">
                    INDEX NO.<br />
                    <br />
                    <asp:Label ID="lbIndex2" runat="server"></asp:Label>
                    <br /></td>
            </tr>
            <tr>
                <td class="style32" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    BOROUGH (PAYROLL)<br />
                    <asp:Label ID="lbPayroll2" runat="server"></asp:Label>
                </td>
                <td class="style12" colspan="2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    DISTRICT/DIVISION<br />
                    <br />
                    <asp:Label ID="lbDistrict2" runat="server"></asp:Label>
                </td>
                <td class="style20" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    SSN<br />
                    <br />
                    <asp:Label ID="lbSSN2" runat="server"></asp:Label>
                    <br /></td>
                <td class="style12" colspan="2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    EMPLOYEE REFERENCE NO.<br />
                    <br />
                    <asp:Label ID="lbRefNo2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style29" 
                    style="border-style: solid none none solid; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    NAME(LAST)<br />
                    <br />
                    <asp:Label ID="lbLName2" runat="server"></asp:Label>
                </td>
                <td class="style2" 
                    style="border-style: solid none none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    (FIRST)<br />
                    <br />
                    <asp:Label ID="lbFName2" runat="server"></asp:Label>
                </td>
                <td class="style31" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    (MI)<br />
                    <br />
                    <asp:Label ID="lbMName2" runat="server"></asp:Label>
                    <br />
                    </td>
                <td class="style21" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    &nbsp;
                    <br />
                    <asp:Label ID="lbBadge2" runat="server"></asp:Label>
                    <br />
                </td>
                <td class="style2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    TITLE<br />
                    <br />
                    <asp:Label ID="lbTitle2" runat="server"></asp:Label>
                </td>
                <td class="style2" 
                    style="border-style: solid solid none none; border-width: 1px; border-color: #000000;" 
                    valign="top">
                    DATE OF BIRTH<br />
                    <br />
                    <asp:Label ID="lbDOB2" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td valign="top" class="style38" colspan="6" 
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;">
                    RULES AND ORDERS VIOLATED AS FOLLOWS (LIST RULE NOS.) CONTINUED FROM PAGE 1<br />
                   <asp:Panel ID="pnVoilations2" runat="server">
                </asp:Panel>
               </td>
                
            </tr>
             <tr>
                <td valign="top" class="style40" colspan="6" 
                     
                     style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;">
                    GENERAL COMMENTS CONTINUED FROM PAGE 1<br />
                    <br />
                    <asp:Label ID="lbComments2" runat="server" Height="126px" Width="899px"></asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td class="style39" valign="top" colspan="6" 
                    
                    style="border-style: solid solid none solid; border-width: 1px; border-color: #000000;">
                    COMPLAINANT STATEMENT (SPECIFIC ACTS WHICH CONSTITUTE VIOLATIONS) CONTINUED 
                    FROM PAGE 1<br />
                    <asp:Panel ID="pnComplainantP2" runat="server">
                    </asp:Panel>
                    </td>
            </tr>
        
            <tr>
                <td valign="top" style="border: 1px solid #000000" colspan="6" class="style34">
                    WITNESSES CONTINUED FROM PAGE 1<br />
                    <br />
                    <asp:Label ID="lbWitnessp2" runat="server" Height="66px" Width="883px"></asp:Label>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </td>
            </tr>
        </table>
 </asp:Panel>
 </div>

 <div id="divpage3" runat="server" align="center"> 
 <asp:Panel ID="Panel3" runat="server" BorderStyle="None" BorderColor="White">
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
 <table width="100%">
     <tr align="left">
                <td align="left" class="style6" colspan="6" 
                    style="border-style: solid solid solid solid; border-width: 1px; border-color: #000000;">
                    <br />
                    <asp:Label ID="lblHistory" runat="server" Visible="false"></asp:Label>   
                    <br />
                    <br />
                    <br />
                    <asp:Panel ID="pnHistory" runat="server">
                    </asp:Panel>                    
               </td>
      </tr>
     </table>
     </asp:Panel>
     </div>
 <asp:Panel ID="pnSOR" runat="server" Visible="false" > 
 
 <p style="font-family:Helvetica; font-size:1.6em; font-weight: bold; text-align: center;page-break-before:always;"><br /><br /><br />STATEMENT OF RIGHTS</p>
 <br />
<p class="SOR">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You are required to report to the Conference Room of the Office of Employment and
Disciplinary Matters at 44 Beaver Street, 11th Floor, New York, New York  10004 , at 08:00hrs on the date
you are scheduled to appear. You are required to appear either at the Conference or the DS Clinic.
If you do not appear at either place, you are to submit documentation to the DS Clinic substantiating
your inability to travel. Failure to submit documentation will subject you to further disciplinary action.
</p>
<p class="SOR">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Your failure to appear without cause may result in a hearing being held in your absence. By
failure to appear for your Conference without good cause, you would be waiving your right to be
present at the hearing to testify in your own behalf and to help your counsel in your defense.
</p>
<p class="SOR">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;In the event a disciplinary complaint is filed against you and you are a permanent competitive
civil service employee of the N.Y.C. Department of Sanitation the following rights are yours by virtue
of the New York City Administrative Code and any other applicable statute, regulation, order or
agreement that applies to your employment.
</p>
<p class="SOR">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You are entitled to a hearing on the disciplinary charges filed against you and to be
represented at such hearing by counsel or a representative of a certified union. It is your
responsibility to make arrangements to be represented and to meet with your representative prior to
the hearing to prepare your defense. You should be prepared at such hearing to present witnesses
and other proof you may have in your defense against these charges.
</p>
<p class="SOR">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;You are entitled to summon witnesses on your behalf at the disciplinary hearing. Your attorney
or representative must submit the names of all Department employees who you wish to appear as
witnesses to the Office of Employment and Disciplinary Matters. You must make your own
arrangements for the appearance at Hearing of any persons not employed by the Department who
you desire to have as witnesses. You will be permitted to examine or cross-examine all witnesses
who testify at such hearing.</p>

<p class="SOR">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;If you are found guilty of the charges, the penalty imposed on you shall be in compliance with
those outlined in the applicable statute, order or agreement that applies to your employment.</p><br />
<p style="margin-left:25px; margin-right:25px;">
----------------------------------
</p>
<p style="font-family:Verdana; font-size:.6em; line-height:1.3em; margin-left:25px; margin-right:25px;">*Certain Non-Competitive Employees who have five years or more of service may also be subject to Section 75 of the N.Y.S. Civil Service Law. Employees who believe
they are in this category should consult their union representative. Certain non-uniformed competitive employees and provisional employees may be subject to alternative,
Non-Section 75, disciplinary procedure. These employees should consult the currently effective bargaining agreement to determine their status in the disciplinary procedures.
</p>
<p style="font-family:Helvetica; font-size: large; font-weight: bold; font-style: normal; text-transform: capitalize; text-align: right; margin-left:25px; margin-right:25px;">DS 249 SOR&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 </p>
        </asp:Panel>
    </form>
</body>
</html>
