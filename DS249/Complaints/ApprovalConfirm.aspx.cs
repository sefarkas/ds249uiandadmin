﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// This class provides the functionality for events and methods for the DS249 confirmation page.
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class Complaints_ApprovalConfirm : System.Web.UI.Page
    {
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {            
                ibList.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
                ibList.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");
                ibNew.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_new_roll.png'");
                ibNew.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_new_static.png'");
                ibPrint.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_print_roll.png'");
                ibPrint.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_print_static.png'");
                lbIndexNo.Text = Request.QueryString["INo"].ToString();
                ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["INo"].ToString()));           
        }

        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibList_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }
        /// <summary>
        /// This button click events redirects the page to Casemanagement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ibNew_Click(object sender, ImageClickEventArgs e)
        {
                ClearSessions();
                Session["Mode"] = "NEW";
                Response.Redirect("CaseManagement.aspx");           
        }

        /// <summary>
        /// This method clears the session if available.
        /// </summary>
        private void ClearSessions()
        {            
                Session["Complaint"] = null;
                Session["Personnel"] = null;
                Session["witnessList"] = null;
                Session["ComplainantList"] = null;
                Session["voilationList"] = null;            
        }
    }
}