﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.Complaints_ComplainantStatement" 
            CodeBehind="ComplainantStatement.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Pragma" content="no-cache" />
    <link href="../includes/css/styles1.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/styles.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/CaseManagementStyle.css" rel="stylesheet" type="text/css" />
    <title></title>
    <base target="_self" />
    <script language="javascript" type="text/javascript">

        function populateParentStatement(sender, args) {
            if (!Page_ClientValidate('')) return false;
            if (args != false) {
                window.returnValue = document.getElementById("hdnStatement").value;
                window.close();
            }
        }

        function dummyFunction() {
            return true;
        }
        function cancelStatement(sender, args) {

            if (args != false) {
                window.returnValue = "CANCEL" + '$' + document.getElementById("hdnOrgType").value;
                window.close();
            }
        }
        function allownumbers(e) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            var reg = new RegExp("[0-9.]")
            if (key == 8) {
                keychar = String.fromCharCode(key);
            }
            if (key == 13) {
                key = 8;
                keychar = String.fromCharCode(key);
            }
            return reg.test(keychar);
        }
        function calculate(stTime, edTime) {
            var startTime = document.getElementById(stTime).value;
            var endTime = document.getElementById(edTime).value;
            if (startTime == endTime) {
                alert('The shift end time cannot be equal to shift start Time.');
                document.getElementById(edTime).value = '';
            }
            else if (endTime < startTime) {
                alert('The shift end time should be greater than ' + startTime);
                document.getElementById(edTime).value = '';
            }
        }


        function displayWeekdayDate(txtOnDate) {

            var displayDate = new Date(document.getElementById(txtOnDate).value);
            var displayWeek = displayDate.getDay();



            var weekday = new Array(7);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";

            var Weekday = weekday[displayWeek];
            var selectedDate = Weekday + ', ' + document.getElementById(txtOnDate).value;
            document.getElementById(txtOnDate).value = selectedDate;



        }

        function showOther(ddlId, OtherTittles) {
            debugger
            var ControlName = document.getElementById('ddlId').options[document.getElementById('ddlId').selectedIndex].value
            if (ControlName.value == 4)  //it depends on which value Selection do u want to hide or show your textbox 
            {
                document.getElementById(OtherTittles).style.display = 'vissible';

            }
            else {
                document.getElementById('MyTextBox').style.display = '';

            }
        }

        function Ordinal(txtIncident) {
            var s = 'th';
            var IncidentNumber = document.getElementById(txtIncident).value;
            if (IncidentNumber != '') {
                if (isNaN(IncidentNumber) == true) {
                    alert('Please enter valid incidents in between 1 to 20');
                    document.getElementById(txtIncident).value = '';
                    return false;
                }
                if (IncidentNumber > 20) {
                    alert('Indicent cannot be greater than 20');
                    document.getElementById(txtIncident).value = '';
                    return false;
                }
                else {
                    if (IncidentNumber == 1) s = 'st';
                    if (IncidentNumber == 2) s = 'nd';
                    if (IncidentNumber == 3) s = 'rd';
                    document.getElementById(txtIncident).value = IncidentNumber + s;
                }
            }
        }

        function SetOtherTTitles(txtTitle) {
            document.getElementById('txtTemp1').value = document.getElementById('txtTemp2').value
           = document.getElementById('txtTemp3').value = document.getElementById('txtTemp4').value
           = document.getElementById('txtTemp5').value = txtTitle.value;
        }

        function PrepareTStatement() {
            var txtTOnDate = document.getElementById('txtTOnDate');
            var ddlCatT1 = document.getElementById('ddlCatT1');
            var lblTemp1 = document.getElementById('lblTemp1');
            var txtT1Garage = document.getElementById('txtT1Garage');
            var txtTstartTime = document.getElementById('txtTstartTime');
            var txtTendTime = document.getElementById('txtTendTime');
            var ddlCatT2 = document.getElementById('ddlCatT2');
            var txtT2Garage = document.getElementById('txtT2Garage');
            var txtTtime = document.getElementById('txtTtime');
            var txtTtype = document.getElementById('txtTtype');
            var ddlCatT3 = document.getElementById('ddlCatT3');
            var ddlCatT4 = document.getElementById('ddlCatT4');
            var ddlCatT5 = document.getElementById('ddlCatT5');
            var txtTincidents = document.getElementById('txtTincidents');
            var templateStatementT = document.getElementById('hdnStatement');
            var txtTemp1 = document.getElementById('txtTemp1');
            var title = (txtTemp1 != null && ddlCatT1.options[ddlCatT1.selectedIndex].text == 'Other' ? txtTemp1.value : ddlCatT1.options[ddlCatT1.selectedIndex].text);


            templateStatementT.value = "On  " + txtTOnDate.value + ", " + title + " " +
                             lblTemp1.innerText + " was assigned to " + txtT1Garage.value + " on the " + txtTstartTime.value +
                             " to " + txtTendTime.value + " shift. " + title + " "
                             + lblTemp1.innerText + " called " + txtT2Garage.value + " at " + txtTtime.value + " requesting leave for " + txtTtype.value + ". "
                             + "Emergency leave was granted pending proof. " + title
                             + " " + lblTemp1.innerText + " failed to submit proof within 2 work days. As a result  " + title
                             + " " + lblTemp1.innerText
                            + " was marked absent and docked 8 hours pay. This is " + title
                             + " " + lblTemp1.innerText + "'s " + txtTincidents.value + " incident in a 12 month period.";


        }
        function SetOtherUTitles(txtTitle)
         {
            document.getElementById('txtUemp1').value = document.getElementById('txtUemp2').value
           = document.getElementById('txtUemp3').value = document.getElementById('txtUemp4').value
           = document.getElementById('txtUemp5').value = document.getElementById('txtUemp6').value = txtTitle.value;
        }




        function PrepareUStatement() {

            var txtUOnDate = document.getElementById('txtUOnDate');
            var ddlCatU1 = document.getElementById('ddlCatU1');
            var lblUemp1 = document.getElementById('lblUemp1');
            var txtU1Garage = document.getElementById('txtU1Garage');
            var txtUstartTime = document.getElementById('txtUstartTime');
            var txtUendTime = document.getElementById('txtUendTime');
            var ddlCatU2 = document.getElementById('ddlCatU2');
            var txtU2Garage = document.getElementById('txtU2Garage');
            var txtUtime = document.getElementById('txtUtime');
            var txtUtype = document.getElementById('txtUtype');
            var txtUEmp = document.getElementById('txtUEmp');
            var txtUincidents = document.getElementById('txtUincidents');
            var txtUemp1 = document.getElementById('txtUemp1');

            var title = (txtUemp1 != null && ddlCatU1.options[ddlCatU1.selectedIndex].text == 'Other' ? txtUemp1.value : ddlCatU1.options[ddlCatU1.selectedIndex].text);

            var templateStatementU = document.getElementById('hdnStatement');
            templateStatementU.value = "On " + txtUOnDate.value + ", " + title + " " +
                            lblUemp1.innerText + " was assigned to " + txtU1Garage.value
                            + " on the " + txtUstartTime.value + " to " + txtUendTime.value + " shift. " + title + " " +
                            lblUemp1.innerText + " called " + txtU2Garage.value + " at " + txtUtime.value
                            + " requesting leave for " + txtUtype.value + ". "
                            + "Emergency leave was granted pending proof. " + title + " " +
                            lblUemp1.innerText + "'s proof was deemed unsatisfactory by " + txtUEmp.value + ". " + title + " " +
                            lblUemp1.innerText + " was given 5 additional work days to submit satisfactory proof, which was also unsatisfactory or no additional proof was submitted. As a result " + title + " " +
                            lblUemp1.innerText + " was marked absent and docked 8 hours pay.  This is " + title + " " +
                            lblUemp1.innerText + "'s " + txtUincidents.value + " incident in a 12 month period. ";
        }

        function SetOtherQTitles(txtTitle) {
            document.getElementById('txtQemp1').value = document.getElementById('txtQemp2').value
           = document.getElementById('txtQemp3').value = document.getElementById('txtQemp4').value
          = txtTitle.value;
        }


        function SetOtherJTitles(txtTitle)
         {
             document.getElementById('txtJ1Category').value = document.getElementById('txtJ2Category').value = txtTitle.value;
         }

         function SetOtherPTitles(txtTitle)
          {            
             document.getElementById('txtP1Category').value = document.getElementById('txtP2Category').value = txtTitle.value;
         }


        function PrepareQStatement() {
            var txtQOnDate = document.getElementById('txtQOnDate');
            var ddlCatQ1 = document.getElementById('ddlCatQ1');
            var txtQemp1 = document.getElementById('txtQemp1');
            var lblQemp1 = document.getElementById('lblQemp1');
            var txtQGarage = document.getElementById('txtQGarage');
            var txtQstartTime = document.getElementById('txtQstartTime');
            var txtQendTime = document.getElementById('txtQendTime');
            var ddlDoQ = document.getElementById('ddlDoQ');
            var ddlOccurQ = document.getElementById('ddlOccurQ');
            var txtQincidents = document.getElementById('txtQincidents');
            var title = (txtQemp1 != null && ddlCatQ1.options[ddlCatQ1.selectedIndex].text == 'Other' ? txtQemp1.value : ddlCatQ1.options[ddlCatQ1.selectedIndex].text);
            var templateStatementU = document.getElementById('hdnStatement');
            templateStatementU.value =
            "On " + txtQOnDate.value + ", " + title + " " +
            lblQemp1.innerText + " was assigned to " + txtQGarage.value + " on the " + txtQstartTime.value
            + " to " + txtQendTime.value + " shift. " + title + " " +
            lblQemp1.innerText + " was a no call, no show. "
            + "As a result " + title + " " +
            lblQemp1.innerText + " was marked AWOL and docked 8 hours pay. "
            + "This " + ddlDoQ.options[ddlDoQ.selectedIndex].text + " occur " + ddlOccurQ.options[ddlOccurQ.selectedIndex].text + " a scheduled day off."
            + " This is " + title + " " +
            lblQemp1.innerText + "'s " + txtQincidents.value + " incident in a 12 month period.";
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="SMStmtPopup" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:Label ID="Label1" runat="server" Text="Complainant Statement (Specify Acts which Constitute Violations)"
            class="styleHeading"></asp:Label>
        <br />
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="97%">
                    <tr>
                        <td>
                            Select Type:<asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="">[SELECT]</asp:ListItem>
                                <asp:ListItem Value="A">No Doc. Cat A</asp:ListItem>
                                <asp:ListItem Value="B">No Doc. Cat B</asp:ListItem>
                                <asp:ListItem Value="C">No Doc. Cat C</asp:ListItem>
                                <asp:ListItem Value="D">Lateness</asp:ListItem>
                                <asp:ListItem Value="E">Not Home Visit</asp:ListItem>
                                <asp:ListItem Value="F">Not Home Telephone</asp:ListItem>
                                <asp:ListItem Value="G">No Doc. HVP Code</asp:ListItem>
                                <asp:ListItem Value="H">No Phone</asp:ListItem>
                                <asp:ListItem Value="I">Disconnected Phone</asp:ListItem>
                                <asp:ListItem Value="J">Tox Test</asp:ListItem>
                                <asp:ListItem Value="K">MVA</asp:ListItem>
                                <asp:ListItem Value="L">License</asp:ListItem>
                                <asp:ListItem Value="M">Guide man</asp:ListItem>
                                <asp:ListItem Value="N">Red Light</asp:ListItem>
                                <asp:ListItem Value="O">Seat Belt</asp:ListItem>
                                <asp:ListItem Value="P">Absent Without Authority</asp:ListItem>
                                <asp:ListItem Value="Q">1.4 AWOL</asp:ListItem>
                                <asp:ListItem Value="R">DSNY Trial Letter</asp:ListItem>
                                <asp:ListItem Value="S">Deemed Prejudicial</asp:ListItem>
                                <asp:ListItem Value="T">1.5 For No Proof</asp:ListItem>
                                <asp:ListItem Value="U">1.5 For Denied Proof</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:HiddenField ID="hdnEmpName" runat="server" />
                            <asp:HiddenField ID="hdnintTitle" runat="server" />
                            <asp:HiddenField ID="hdnstrTitleDesc" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList"
                                HeaderText="Please Correct the following Errors" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="pnTypeA" runat="server" Visible="false">
                                <table>
                                    <tr>
                                        <td>
                                            <br />
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtAOnDate" runat="server" AutoPostBack="true" OnTextChanged="txtAOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtAOnDate_CalendarExtender" runat="server" Enabled="True"
                                                Format="MM/dd/yyyy" TargetControlID="txtAOnDate" PopupButtonID="CalAOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalAOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator3" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtAOnDate"> !</asp:CompareValidator>
                                            , 
                                             <asp:DropDownList ID="ddlACategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlACategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorACategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlACategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtACategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtA" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtACategory">!</asp:RequiredFieldValidator>                                     <asp:Label ID="lbAEmpName" runat="server" Text="Employee Name"></asp:Label>
                                            a Category "A" employee, reported sick/ lodi and began medical leave. You were ordered
                                            to report to the DS Clinic on
                                            <asp:TextBox ID="txtAReportingDate" runat="server" AutoPostBack="true" OnTextChanged="txtAReportingDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtAReportingDate_CalendarExtender" runat="server" Enabled="True"
                                                Format="MM/dd/yyyy" TargetControlID="txtAReportingDate" PopupButtonID="CalARepDt"
                                                CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalARepDt" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator1" runat="server" SetFocusOnError="true"
                                                ErrorMessage="Reporting Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtAReportingDate"> !</asp:CompareValidator>
                                            and failed to report as required. You also failed to submit any / adequate medical
                                            documentation to substantiate the illness and the <span style="font-size: 12.0pt;
                                                font-family: Times New Roman;">inability </span>to travel.
                                            <br />
                                            <br />
                                            As of this date
                                            <asp:TextBox ID="txtAThisDate" runat="server" AutoPostBack="true" OnTextChanged="txtAThisDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtAThisDate_CalendarExtender" runat="server" Enabled="True"
                                                Format="MM/dd/yyyy" TargetControlID="txtAThisDate" PopupButtonID="CalAThisDt"
                                                CssClass="MyCalendar" PopupPosition="TopLeft">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalAThisDt" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator2" runat="server" SetFocusOnError="true"
                                                ErrorMessage="This Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtAThisDate"> !</asp:CompareValidator>
                                            , no / inadequate medical documentation has been received by the Medical Division
                                            for the above date of incident.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeB" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtBOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtBOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtBOnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtBOnDate" PopupButtonID="CalBOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalBOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator4" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtBOnDate"> !</asp:CompareValidator>
                                            ,
                                            <asp:DropDownList ID="ddlBCategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlBCategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorBCategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlBCategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtBCategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtB" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtBCategory">!</asp:RequiredFieldValidator>
                 
                                            <asp:Label ID="lbBEmpName" runat="server" Text="Label"></asp:Label>
                                            reported sick and began medical leave. As a Category "B" employee, employee is required
                                            to report to the DS Clinic on the second day of sick leave. Employee failed to report
                                            as required. Employee reported to the DS Clinic on
                                            <asp:TextBox ID="txtBReportedDate" runat="server" OnTextChanged="txtBReportedDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtBReportedDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtBReportedDate" PopupButtonID="CalBRepDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalBRepDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator5" runat="server" SetFocusOnError="true"
                                                ErrorMessage="Reported Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtBReportedDate"> !</asp:CompareValidator>
                                            and failed to present any/adequate medical documentation to substantiate the illness
                                            and / or inability to travel to the DS Clinic on/from
                                            <asp:TextBox ID="txtBFromLoc" runat="server" Height="18px" Width="186px" AutoPostBack="True"
                                                MaxLength="30" OnTextChanged="txtBFromLoc_TextChanged"></asp:TextBox>.
                                            <br />
                                            <br />
                                            As of this date
                                            <asp:TextBox ID="txtBThisDate" runat="server" AutoPostBack="True" OnTextChanged="txtBThisDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtBThisDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtBThisDate" PopupButtonID="CalBThisDt" CssClass="MyCalendar"
                                                PopupPosition="TopLeft">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalBThisDt" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator6" runat="server" SetFocusOnError="true"
                                                ErrorMessage="This Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtBThisDate"> !</asp:CompareValidator>
                                            , no / inadequate medical documentation has been received by the Medical Division
                                            for the above date of incident.
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeC" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtCOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtCOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtCOnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtCOnDate" PopupButtonID="CalCOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalCOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator7" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtCOnDate"> !</asp:CompareValidator>
                                            , 
                                            <asp:DropDownList ID="ddlCCategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCCategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorCCategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCCategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtCCategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtC" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtCCategory">!</asp:RequiredFieldValidator>
                 
                                            <asp:Label ID="lbCEmpName" runat="server" Text="Label"></asp:Label>
                                            reported sick / LODI and began medical leave. As a Category "C" employee, employee
                                            is required to report to the DS Clinic on the first day of medical leave or on the
                                            first day the DS Clinic is Open. Employee failed to report as required. Employee
                                            reported to the DS Clinic on
                                            <asp:TextBox ID="txtCReportedDate" runat="server" OnTextChanged="txtCReportedDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtCReportedDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtCReportedDate" PopupButtonID="CalCRepDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalCRepDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator8" runat="server" SetFocusOnError="true"
                                                ErrorMessage="Reported Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtCReportedDate"> !</asp:CompareValidator>
                                            and failed to present any/adequate medical documentation to substantiate the illness
                                            and / or inability to travel to the DS Clinic on/from
                                            <asp:TextBox ID="txtCFromLoc" runat="server" Height="18px" Width="186px" AutoPostBack="True"
                                                MaxLength="30" OnTextChanged="txtCFromLoc_TextChanged"></asp:TextBox>.
                                            <br />
                                            <br />
                                            As of this date
                                            <asp:TextBox ID="txtCThisDate" runat="server" AutoPostBack="True" OnTextChanged="txtCThisDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtCThisDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtCThisDate" PopupButtonID="CalCThisDt" CssClass="MyCalendar"
                                                PopupPosition="TopLeft">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalCThisDt" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator9" runat="server" SetFocusOnError="true"
                                                ErrorMessage="This Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtCThisDate"> !</asp:CompareValidator>
                                            , no / inadequate medical documentation has been received by the Medical Division
                                            for the above date of incident.
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeD" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtDOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtDOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtDOnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtDOnDate" PopupButtonID="CalDOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalDOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator10" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtDOnDate"> !</asp:CompareValidator>
                                            , 


                                            <asp:DropDownList ID="ddlDCategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlDCategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorDCategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlDCategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtDCategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtD" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtDCategory">!</asp:RequiredFieldValidator>
                 
                                            <asp:Label ID="lbDEmpName" runat="server" Text="Label"></asp:Label>, a category
                                            <asp:DropDownList ID="ddlCategory" runat="server" AutoPostBack="true">
                                                <asp:ListItem Value="A">A</asp:ListItem>
                                                <asp:ListItem Value="B">B</asp:ListItem>
                                                <asp:ListItem Value="C">C</asp:ListItem>
                                            </asp:DropDownList>
                                            employee, reported to the DS Clinic at
                                            <asp:TextBox ID="txtDReportedTime" runat="server" AutoPostBack="True" OnTextChanged="txtDReportedTime_TextChanged"></asp:TextBox>
                                            hours.<br />
                                            Employee failed to report at
                                            <asp:TextBox ID="txtDReportingTime" runat="server" AutoPostBack="True" OnTextChanged="txtDReportingTime_TextChanged"></asp:TextBox>
                                            &nbsp;hours as required by PAP 2007-04.
                                            <br />
                                            Employee is to be marked late
                                            <asp:TextBox ID="txtDText" runat="server" AutoPostBack="True" OnTextChanged="txtDText_TextChanged"></asp:TextBox>
                                            and docked the same by using Payroll Code 3613 in the PMS System and an entry is
                                            to be made in the absence and lateness log.
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeE" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtEOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtEOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtEOnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtEOnDate" PopupButtonID="CalEOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalEOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator11" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtEOnDate"> !</asp:CompareValidator>
                                            at
                                            <asp:TextBox ID="txtEReportingTime" runat="server" AutoPostBack="True" OnTextChanged="txtEReportingTime_TextChanged"></asp:TextBox>
                                            hours a sick leave visit was made to 


                                             <asp:DropDownList ID="ddlECategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlECategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorECategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlECategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtECategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtE" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtECategory">!</asp:RequiredFieldValidator>
                 
                                            <asp:Label ID="lbEEmpName" runat="server" Text="Label"></asp:Label>, a category
                                            <asp:DropDownList ID="ddlLodiA" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLodiA_SelectedIndexChanged">
                                                <asp:ListItem Value="A">A</asp:ListItem>
                                                <asp:ListItem Value="B">B</asp:ListItem>
                                                <asp:ListItem Value="C">C</asp:ListItem>
                                            </asp:DropDownList>
                                            employee, at
                                            <asp:TextBox ID="txtEFromLoc" runat="server" AutoPostBack="True" OnTextChanged="txtEFromLoc_TextChanged"></asp:TextBox>
                                            by
                                            <asp:TextBox ID="txtEText" runat="server" AutoPostBack="True" OnTextChanged="txtEText_TextChanged"></asp:TextBox>,
                                            an authorized employee of the Supervised Sick Leave Unit. The employee was not at
                                            home and did not call for or receive permission from the Home Visitation Program
                                            to be away from home while on medical leave.
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeF" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtFOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtFOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFOnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtFOnDate" PopupButtonID="CalFOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalFOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator12" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtFOnDate"> !</asp:CompareValidator>
                                            authorized personnel from the Supervised Sick Leave Unit called 
                                               <asp:DropDownList ID="ddlFCategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlFCategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorFCategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlFCategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtFCategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>
                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtF" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtFCategory">!</asp:RequiredFieldValidator>
             
                                           <asp:Label ID="lbFEmpName" runat="server" Text="Label"></asp:Label>
                                            at his/her reported telephone number
                                            <asp:TextBox ID="txtFTelNo" runat="server" AutoPostBack="True" OnTextChanged="txtFTelNo_TextChanged"></asp:TextBox>
                                            at various times and left instructions for the employee on his/her answering machine.
                                            The employee, on paid medical leave, failed to respond to the telephone call/s and
                                            failed to call for or receive permission from the Home Visitation Program to be
                                            away from home at the instance of each call.
                                            <br />
                                            As of this date,
                                            <asp:TextBox ID="txtFThisDate" runat="server" AutoPostBack="True" OnTextChanged="txtFThisDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtFThisDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtFThisDate" PopupButtonID="CalFThisDt" CssClass="MyCalendar"
                                                PopupPosition="TopLeft">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalFThisDt" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator13" runat="server" SetFocusOnError="true"
                                                ErrorMessage="This Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtFThisDate"> !</asp:CompareValidator>
                                            , no /inadequate medical documentation has been received by the Medical Division
                                            for the above date of incident
                                            <br />
                                            The employee was in category
                                            <asp:DropDownList ID="ddlCatF" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCatF_SelectedIndexChanged">
                                                <asp:ListItem Value="A">A</asp:ListItem>
                                                <asp:ListItem Value="B">B</asp:ListItem>
                                                <asp:ListItem Value="C">C</asp:ListItem>
                                            </asp:DropDownList>
                                            at the time of incident.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeG" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtGOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtGOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtGOnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtGOnDate" PopupButtonID="CalGOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalGOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator14" runat="server" SetFocusOnError="true"
                                                ErrorMessage="This Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtGOnDate"> !</asp:CompareValidator>
                                            
                                                <asp:DropDownList ID="ddlGCategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlGCategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorGCategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlGCategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtGCategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtG" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtGCategory">!</asp:RequiredFieldValidator>
                 

                                            <asp:Label ID="lbGEmpName" runat="server" Text="Label"></asp:Label>
                                            reported sick/lodi. The employee found not at home on a phone visit/home visit had
                                            called for authorization on
                                            <asp:TextBox ID="txtGReportedDate" runat="server" AutoPostBack="True" OnTextChanged="txtGReportedDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtGReportedDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtGReportedDate" PopupButtonID="CalGRepDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalGRepDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator15" runat="server" SetFocusOnError="true"
                                                ErrorMessage="Reported Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtGReportedDate"> !</asp:CompareValidator>
                                            to leave home and was given code
                                            <asp:TextBox ID="txtGText" runat="server" AutoPostBack="True" OnTextChanged="txtGText_TextChanged"></asp:TextBox>
                                            for
                                            <asp:DropDownList ID="ddlEmpCategory" runat="server" AutoPostBack="true">
                                                <asp:ListItem Value="0">SELECT</asp:ListItem>
                                                <asp:ListItem Value="1">DOCTOR</asp:ListItem>
                                                <asp:ListItem Value="2">PHYSICAL THERAPY</asp:ListItem>
                                                <asp:ListItem Value="3">FUNERAL</asp:ListItem>
                                                <asp:ListItem Value="4">GROCERY STORE</asp:ListItem>
                                                <asp:ListItem Value="5">DRUG STORE</asp:ListItem>
                                                <asp:ListItem Value="6">FAMILY EMERGENCY</asp:ListItem>
                                                <asp:ListItem Value="7">COURT /DEPT.TRIALS</asp:ListItem>
                                            </asp:DropDownList>
                                            .
                                            <br />
                                            As of this date
                                            <asp:TextBox ID="txtGThisDate" runat="server" OnTextChanged="txtGThisDate_TextChanged"
                                                AutoPostBack="true" CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtGThisDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtGThisDate" PopupButtonID="CalGThisDate" CssClass="MyCalendar"
                                                PopupPosition="TopLeft">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalGThisDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator16" runat="server" SetFocusOnError="true"
                                                ErrorMessage="This Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtGThisDate"> !</asp:CompareValidator>
                                            no/inadequate documentation has been received by the Medical Division for the above
                                            date of incident.
                                            <br />
                                            The employee was in category
                                            <asp:DropDownList ID="ddlCatG" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCatG_SelectedIndexChanged">
                                                <asp:ListItem Value="A">A</asp:ListItem>
                                                <asp:ListItem Value="B">B</asp:ListItem>
                                                <asp:ListItem Value="C">C</asp:ListItem>
                                            </asp:DropDownList>
                                            at the time of incident.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeH" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtHOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtHOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtHOnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtHOnDate" PopupButtonID="CalHOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalHOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator17" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtHOnDate"> !</asp:CompareValidator>
                                            , 
                                            <asp:DropDownList ID="ddlHCategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlHCategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorHCategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlHCategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtHCategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtH" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtHCategory">!</asp:RequiredFieldValidator>
                 



                                            <asp:Label ID="lbHEmpName" runat="server" Text="Label"></asp:Label>
                                            reported sick/LODI, began medical leave, and currently does not have a home telephone.
                                            An employee that does not have a home telephone must call the DS Clinic to report
                                            a medical leave absence on the first day of medical leave and must call the DS Clinic
                                            each day they are on medical leave, providing an update of their medical condition,
                                            until their first visit to the DS Clinic.
                                            <br />
                                            Employee failed to call as required on
                                            <asp:TextBox ID="txtHReportedDate" runat="server" AutoPostBack="True" OnTextChanged="txtHReportedDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtHReportedDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtHReportedDate" PopupButtonID="CalHRepDate" CssClass="MyCalendar"
                                                PopupPosition="TopLeft">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalHRepDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator18" runat="server" SetFocusOnError="true"
                                                ErrorMessage="Reported Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtHReportedDate"> !</asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeI" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            PAP 2007-04.
                                            <br />
                                            <br />
                                            On
                                            <asp:TextBox ID="txtIOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtIOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtIOnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtIOnDate" PopupButtonID="CalIOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalIOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator19" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtIOnDate"> !</asp:CompareValidator>
                                            authorized personnel from the Supervised Sick Leave Unit called
                                            
                                             <asp:DropDownList ID="ddlICategory" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlICategory_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorICategory" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlICategory">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtICategory" title="Enter Title" runat="server" Visible="false"   MaxLength="60" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtI" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtICategory">!</asp:RequiredFieldValidator>
                                                        
                                            
                                            <asp:Label ID="lbIEmpName" runat="server" Text="Label"></asp:Label>&nbsp;at his/her
                                            reported telephone number
                                            <asp:TextBox ID="txtITelNo" runat="server" AutoPostBack="True" OnTextChanged="txtITelNo_TextChanged"></asp:TextBox>at
                                            various times.
                                            <br />
                                            A message stating "This telephone number is not in service or is disconnected" was
                                            received. Employee must notify their Supervisor of any change of their telephone
                                            number.
                                            <br />
                                            While on paid leave, an employee shall remain accessible and available for telephone
                                            sick leave calls conducted by Supervised Sick Leave Unit personnel.
                                            <br />
                                            The employee was in category
                                            <asp:DropDownList ID="ddlCatI" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCatI_SelectedIndexChanged">
                                                <asp:ListItem Value="A">A</asp:ListItem>
                                                <asp:ListItem Value="B">B</asp:ListItem>
                                                <asp:ListItem Value="C">C</asp:ListItem>
                                            </asp:DropDownList>
                                            at the time of incident.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeJ" runat="server" Visible="false">
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtJ1OnDate" runat="server" AutoPostBack="True" OnTextChanged="txtJ1OnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtJ1OnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtJ1OnDate" PopupButtonID="CalJ1OnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalJ1OnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator20" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtJ1OnDate"> !</asp:CompareValidator>
                                          
                                           <asp:DropDownList ID="ddlJ1Category" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlJ1Category_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorJ1Category" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlJ1Category">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtJ1Category" title="Enter Title" runat="server" Visible="false"   MaxLength="60" 
                                         
                                           onchange= "SetOtherJTitles(this);"> </asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtJ1" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtJ1Category">!</asp:RequiredFieldValidator>
                 

                                            <asp:Label ID="lbJ1EmpName" runat="server" Text="Label"></asp:Label>
                                            reported to (the)
                                            <asp:TextBox ID="txtJ1FromLoc" runat="server" AutoPostBack="True" OnTextChanged="txtJ1FromLoc_TextChanged"></asp:TextBox>
                                            as ordered. A urine sample of toxicology testing was taken. These result were positive
                                            for
                                            <asp:TextBox ID="txtJ1Text" runat="server" AutoPostBack="True" OnTextChanged="txtJ1Text_TextChanged"></asp:TextBox>
                                            <br />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <br />
                                            On
                                            <asp:TextBox ID="txtJ2OnDate" runat="server" AutoPostBack="True"  CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtJ2OnDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtJ2OnDate" PopupButtonID="CalJ2OnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalJ2OnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator21" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtJ2OnDate"> !</asp:CompareValidator>                                                                                 

                                             <asp:DropDownList ID="ddlJ2Category" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlJ2Category_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorJ2Category" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlJ2Category">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtJ2Category" title="Enter Title" runat="server" Visible="false"   MaxLength="60"  onchange= "SetOtherJTitles(this);" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtJ2" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtJ2Category">!</asp:RequiredFieldValidator>
                 

                                            <asp:Label ID="lbJ2EmpName" runat="server" Text="Label"></asp:Label>
                                            reported to (the)
                                            <asp:TextBox ID="txtJ2FromLoc" runat="server" AutoPostBack="True" OnTextChanged="txtJ2FromLoc_TextChanged"></asp:TextBox>
                                            as ordered. A breathalyzer test was conducted under the supervision of
                                            <asp:TextBox ID="txtJ2Super" runat="server" AutoPostBack="True" OnTextChanged="txtJ2Super_TextChanged"></asp:TextBox>
                                            <br />
                                            The result was a positive BAC of
                                            <asp:TextBox ID="txtJ2Text" runat="server" AutoPostBack="True" OnTextChanged="txtJ2Text_TextChanged"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeK" runat="server" Visible="false">
                                MVA GO 2002-06 CODE OF CONDUCT, 5.1, 5.4, 5.11. 5.12
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            The Safety Unit has determined you were negligent on
                                            <asp:TextBox ID="txtKOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtKOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Enabled="True" TargetControlID="txtKOnDate"
                                                PopupButtonID="CalKOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalKOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator22" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtKOnDate"> !</asp:CompareValidator>
                                            at approximately
                                            <asp:TextBox ID="txtKHours" runat="server" AutoPostBack="True" OnTextChanged="txtKHours_TextChanged"></asp:TextBox>
                                            &nbsp;hours. While operating Vehicle code #
                                            <asp:TextBox ID="txtKVehicle" runat="server" AutoPostBack="True" OnTextChanged="txtKVehicle_TextChanged"></asp:TextBox>
                                            &nbsp;you were involved in a collision at&nbsp;
                                            <asp:TextBox ID="txtKLocation" runat="server" AutoPostBack="True" OnTextChanged="txtKLocation_TextChanged"></asp:TextBox>
                                            &nbsp;that resulted in property damage and / or personal injury. This is your chargeable
                                            accident in a 12 month period.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeL" runat="server" Visible="false">
                                License GO 2002-06 CODE OF CONDUCT 3.4
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtLOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtLOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" Enabled="True" TargetControlID="txtLOnDate"
                                                PopupButtonID="CalLOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalLOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator23" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtLOnDate"> !</asp:CompareValidator>
                                            the New York State Department of Motor Vehicles revoked / suspended your Commercial
                                            Driver’s License, Order No.
                                            <asp:TextBox ID="txtLOrderNo" runat="server" AutoPostBack="True" OnTextChanged="txtLOrderNo_TextChanged"></asp:TextBox>
                                            &nbsp;for DMV violation. You have not had in your possession a valid Commercial
                                            Driver’s License for over 10 days.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeM" runat="server" Visible="false">
                                Guide man GO 2002-06 CODE OF CONDUCT, 5.4, 5.11
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            The Safety Unit has determined you were negligent on
                                            <asp:TextBox ID="txtMOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtMOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender7" runat="server" Enabled="True" TargetControlID="txtMOnDate"
                                                PopupButtonID="CalMOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalMOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator28" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtMOnDate"> !</asp:CompareValidator>
                                            at approximately
                                            <asp:TextBox ID="txtMHours" runat="server" AutoPostBack="True" OnTextChanged="txtMHours_TextChanged"></asp:TextBox>
                                            <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="dummyFunction"
                                                ControlToValidate="txtMHours" ErrorMessage=""></asp:CustomValidator>
                                            &nbsp;hours. While guiding Vehicle code #
                                            <asp:TextBox ID="txtMVehicle" runat="server" AutoPostBack="True" OnTextChanged="txtMVehicle_TextChanged"></asp:TextBox>
                                            &nbsp; in a backwards direction you were involved in a collision at
                                            <asp:TextBox ID="txtMLocation" runat="server" AutoPostBack="True" OnTextChanged="txtMLocation_TextChanged"></asp:TextBox>
                                            &nbsp; that resulted in property damage and / or personal injury.
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeN" runat="server" Visible="false">
                                Red Light GO 2002-06 CODE OF CONDUCT, 5.4
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtNOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtNOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender3" runat="server" Enabled="True" TargetControlID="txtNOnDate"
                                                PopupButtonID="CalNOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalNOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator24" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtNOnDate"> !</asp:CompareValidator>
                                            at approximately
                                            <asp:TextBox ID="txtNHours" runat="server" AutoPostBack="True" OnTextChanged="txtNHours_TextChanged"></asp:TextBox>
                                            &nbsp;hrs. while operating vehicle code
                                            <asp:TextBox ID="txtNVehicle" runat="server" AutoPostBack="True" OnTextChanged="txtNVehicle_TextChanged"></asp:TextBox>
                                            &nbsp; you were observed by Safety Officer
                                            <asp:TextBox ID="txtNOfficer" runat="server" AutoPostBack="True" OnTextChanged="txtNOfficer_TextChanged"></asp:TextBox>
                                            &nbsp;passing a steady red signal at
                                            <asp:TextBox ID="txtNLocation" runat="server" AutoPostBack="True" OnTextChanged="txtNLocation_TextChanged"></asp:TextBox>
                                            &nbsp; I had a clear and unobstructed view of this violation. The signal was in
                                            proper working order.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeO" runat="server" Visible="false">
                                Seat Belt GO 2002-06 CODE OF CONDUCT, 5.11, GO 2009-07
                                <br />
                                THE USE OF SEATBELTS AND HARNESSES IN DEPARTMENT OF SANITATION VEHICLES.
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtOOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtOOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender4" runat="server" Enabled="True" TargetControlID="txtOOnDate"
                                                PopupButtonID="CalOOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalOOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator25" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtOOnDate"> !</asp:CompareValidator>
                                            at approximately hours
                                            <asp:TextBox ID="txtOHours" runat="server" AutoPostBack="True" OnTextChanged="txtOHours_TextChanged"></asp:TextBox>
                                            &nbsp;you were observed by
                                            <asp:TextBox ID="txtOSuper" runat="server" AutoPostBack="True" OnTextChanged="txtOSuper_TextChanged"></asp:TextBox>
                                            &nbsp; operating / occupying Vehicle Code No.
                                            <asp:TextBox ID="txtOVehicle" runat="server" AutoPostBack="True" OnTextChanged="txtOVehicle_TextChanged"></asp:TextBox>
                                            &nbsp;at
                                            <asp:TextBox ID="txtOLocation" runat="server" AutoPostBack="True" OnTextChanged="txtOLocation_TextChanged"></asp:TextBox>
                                            &nbsp; You were not wearing a seat belt at the time. This is a direct violation
                                            of the above orders and rules.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeP" runat="server" Visible="false">
                                Absent Without Authority
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtPWeek" runat="server" AutoPostBack="True" OnTextChanged="txtPWeek_TextChanged"></asp:TextBox>
                                            &nbsp;&nbsp;,<asp:TextBox ID="txtPOnDate" runat="server" AutoPostBack="True" OnTextChanged="txtPOnDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender5" runat="server" Enabled="True" TargetControlID="txtPOnDate"
                                                PopupButtonID="CalPOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalPOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator26" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtPOnDate"> !</asp:CompareValidator>
                                           
                                            <asp:DropDownList ID="ddlP1Category" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlP1Category_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorP1Category" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlP1Category">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtP1Category" title="Enter Title" runat="server" Visible="false"   MaxLength="60"  onchange= "SetOtherPTitles(this);" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtP1" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtP1Category">!</asp:RequiredFieldValidator>                                            





                                            <asp:Label ID="lbPEmp" runat="server" Text=""></asp:Label>
                                            &nbsp;was absent for his/her assigned shift, he did not call and had no authorization
                                            to be off.                                            
                                            
                                             <asp:DropDownList ID="ddlP2Category" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlP2Category_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>

                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorP2Category" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlP2Category">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtP2Category" title="Enter Title" runat="server" Visible="false"   MaxLength="60"  onchange= "SetOtherPTitles(this);" ></asp:TextBox>

                                               <asp:RequiredFieldValidator ID="RequiredFieldValidatortxtP2" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtP2Category">!</asp:RequiredFieldValidator>
                                            
                                            
                                            
                                            has been marked absent and docked 8 hours pay
                                            <br />
                                            This incident occurred
                                            <asp:TextBox ID="txtPWhen" runat="server" AutoPostBack="True" OnTextChanged="txtPWhen_TextChanged"></asp:TextBox>,
                                            <asp:TextBox ID="txtPReason" runat="server" AutoPostBack="True" OnTextChanged="txtPReason_TextChanged"></asp:TextBox>
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeQ" runat="server" Visible="false">
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtQOnDate" title="Enter WeekDay, Date" runat="server" 
                                                CausesValidation="false" Height="16px" Width="140px" onchange="Javascript:displayWeekdayDate('txtQOnDate');PrepareQStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator25" runat="server" ErrorMessage="Please select the date."
                                                Display="Dynamic" ControlToValidate="txtQOnDate">!</asp:RequiredFieldValidator>
                                            <cc1:CalendarExtender ID="CalendarExtender6" runat="server" Enabled="True" TargetControlID="txtQOnDate"
                                                PopupButtonID="CalQOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalQOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:DropDownList ID="ddlCatQ1" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatQ1_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator26" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatQ1">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtQemp1" title="Enter Title" runat="server" Visible="false" onchange= "SetOtherQTitles(this);PrepareQStatement();"
                                                MaxLength="60" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtQemp1">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblQemp1" runat="server" Text="Employee Name"></asp:Label><br />
                                            was assigned to
                                            <asp:TextBox ID="txtQGarage" title="Enter Gararge" MaxLength="50" runat="server"
                                                onchange= "PrepareQStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator27" runat="server" ErrorMessage="Please enter the garage."
                                                Display="Dynamic" ControlToValidate="txtQGarage">!</asp:RequiredFieldValidator>
                                            on the
                                            <asp:TextBox ID="txtQstartTime" title="Enter Start Time" runat="server" onchange= "PrepareQStatement();"
                                             Width="40"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditQstartTime" runat="server" MaskType="Time"
                                                Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                TargetControlID="txtQstartTime" AutoCompleteValue="00:00" UserTimeFormat="TwentyFourHour">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator6" runat="server" ControlToValidate="txtQstartTime"
                                                ControlExtender="MaskedEditQstartTime" Display="None" IsValidEmpty="false" EmptyValueMessage="Time is required"
                                                InvalidValueMessage="This time is invalid">
                                            </cc1:MaskedEditValidator>
                                            to
                                            <asp:TextBox ID="txtQendTime" title="Enter End Time" runat="server" 
                                             MaxLength="4" Width="40" onchange="PrepareQStatement();"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditQendTime" runat="server" MaskType="Time" Mask="99:99"
                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="txtQendTime"
                                                TargetControlID="txtQendTime" AutoCompleteValue="00:00" UserTimeFormat="TwentyFourHour">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator7" runat="server" ControlToValidate="txtQendTime"
                                                ControlExtender="MaskedEditQendTime" Display="None" IsValidEmpty="false" EmptyValueMessage="Time is required"
                                                InvalidValueMessage="This time is invalid">
                                            </cc1:MaskedEditValidator>
                                            shift.
                                            <br />
                                            <asp:DropDownList ID="ddlCatQ2" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatQ2_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtQemp2" title="Enter Title" runat="server" Visible="false" onchange= "SetOtherQTitles(this);PrepareQStatement();"
                                                MaxLength="60" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtQemp2">!</asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator28" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatQ2">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblQemp2" runat="server" Text="Employee Name"></asp:Label>
                                            was a no call, no show.<br />
                                            As a result
                                            <asp:DropDownList ID="ddlCatQ3" runat="server" title="Enter Title" AutoPostBack="true"
                                                Width="70" OnSelectedIndexChanged="ddlCatQ3_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtQemp3" title="Enter Title" runat="server" Visible="false" onchange= "SetOtherQTitles(this);PrepareQStatement();"
                                                MaxLength="60"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator24" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtQemp3">!</asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator29" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatQ3">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblQemp3" runat="server" Text="Employee Name"></asp:Label>
                                            was marked AWOL and docked 8 hours pay.
                                            <br />
                                            This
                                            <asp:DropDownList ID="ddlDoQ" title="Enter Type" runat="server" 
                                                onchange= "PrepareQStatement();">
                                                <asp:ListItem Value="A">did</asp:ListItem>
                                                <asp:ListItem Value="B">did not</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator30" runat="server" ErrorMessage="Please select the did or did not."
                                                Display="Dynamic" ControlToValidate="ddlDoQ">!</asp:RequiredFieldValidator>
                                            occur
                                            <asp:DropDownList ID="ddlOccurQ" title="Enter Occurrence" runat="server" onchange= "PrepareQStatement();">
                                                <asp:ListItem Value="A">before</asp:ListItem>
                                                <asp:ListItem Value="B">after</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator31" runat="server" ErrorMessage="Please select the occurance."
                                                Display="Dynamic" ControlToValidate="ddlOccurQ">!</asp:RequiredFieldValidator>
                                            a scheduled day off. This is
                                            <asp:DropDownList ID="ddlCatQ4" title="Enter Title" runat="server" AutoPostBack="true">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="txtQemp4" title="Enter Title" runat="server" Visible="false" 
                                                MaxLength="60" onchange= "SetOtherQTitles(this);PrepareQStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator34" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtQemp4">!</asp:RequiredFieldValidator>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator32" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatQ4">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblQemp4" runat="server" Text="Employee Name"></asp:Label>
                                            &nbsp;<asp:TextBox ID="txtQincidents" Width="30" runat="server" 
                                                onchange="Javascript:Ordinal('txtQincidents');PrepareQStatement();" 
                                                MaxLength="4" title="Enter Incident"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ErrorMessage="Please enter the incident."
                                                Display="Dynamic" ControlToValidate="txtQincidents">!</asp:RequiredFieldValidator>
                                            incident in a 12 month period.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeR" runat="server" Visible="false">
                                DSNY Trial room
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtTrialName" runat="server" AutoPostBack="True" OnTextChanged="txtTrialName_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender8" runat="server" Enabled="True" TargetControlID="txtTrialName"
                                                PopupButtonID="CalFOnDate1" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalFOnDate1" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator29" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtTrialName"> !</asp:CompareValidator>
                                            <asp:TextBox ID="txtTrialDate" runat="server" AutoPostBack="True" OnTextChanged="txtTrialDate_TextChanged"></asp:TextBox>
                                            &nbsp; was scheduled to appear at DSNY Trial room, located at 44 Beaver Street,NY
                                            at 0800.                                           
                                            <asp:TextBox ID="txtIncident" runat="server" AutoPostBack="True" OnTextChanged="txtIncident_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            &nbsp; was no call no show and failed to contact the Advocate's Office as to the
                                            reason for his/her absence or inability to travel to DSNY Trial room.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeS" runat="server" Visible="false">
                                Deemed Prejudicial
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            You have engaged in activities that are deemed to be prejudicial to the good order
                                            and discipline of this Department. Your activities and your arrest of
                                            <asp:TextBox ID="txtprejDate" runat="server" AutoPostBack="True" OnTextChanged="txtprejDate_TextChanged"
                                                CausesValidation="true"></asp:TextBox>
                                            <cc1:CalendarExtender ID="CalendarExtender9" runat="server" Enabled="True" TargetControlID="txtprejDate"
                                                PopupButtonID="CalprejDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalprejDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:CompareValidator ID="CompareValidator30" runat="server" SetFocusOnError="true"
                                                ErrorMessage="On Date Entered Is Incorrect" Type="Date" Operator="DataTypeCheck"
                                                ControlToValidate="txtprejDate"> !</asp:CompareValidator>
                                            for
                                            <asp:TextBox ID="txtprejName" runat="server" AutoPostBack="True" OnTextChanged="txtprejName_TextChanged"></asp:TextBox>
                                            has brought discredit to the City of New York.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnTypeT" runat="server" Visible="false">
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtTOnDate" title="Enter WeekDay, Date" runat="server" Width="140px"
                                                CausesValidation="false" Height="16px" 
                                                onchange="Javascript:displayWeekdayDate('txtTOnDate');PrepareTStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please select the date."
                                                Display="Dynamic" ControlToValidate="txtTOnDate">!</asp:RequiredFieldValidator>
                                            <cc1:CalendarExtender ID="CalendarExtender10" runat="server" Enabled="True" TargetControlID="txtTOnDate"
                                                PopupButtonID="CalTOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalTOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:DropDownList ID="ddlCatT1" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatT1_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator15" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatT1">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtTemp1" title="Enter Title" runat="server" Visible="false" onchange="SetOtherTTitles(this);PrepareTStatement();"
                                                MaxLength="60" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator35" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtTemp1">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblTemp1" runat="server" Text="Employee Name"></asp:Label>
                                            was assigned to<br />
                                            <asp:TextBox ID="txtT1Garage" title="Enter Gararge" runat="server" 
                                                MaxLength="50" onchange="PrepareTStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ErrorMessage="Please enter the garage."
                                                Display="Dynamic" ControlToValidate="txtT1Garage">!</asp:RequiredFieldValidator>
                                            on the
                                            <asp:TextBox ID="txtTstartTime" title="Enter Start Time" runat="server" onchange="PrepareTStatement();"
                                                MaxLength="4" Width="40"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditTstartTime" runat="server" MaskType="Time"
                                                Mask="99:99" MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                TargetControlID="txtTstartTime" AutoCompleteValue="00:00" UserTimeFormat="TwentyFourHour">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator4" runat="server" ControlToValidate="txtTstartTime"
                                                ControlExtender="MaskedEditTstartTime" Display="None" IsValidEmpty="false" EmptyValueMessage="Time is required"
                                                InvalidValueMessage="This time is invalid">
                                            </cc1:MaskedEditValidator>
                                            to
                                            <asp:TextBox ID="txtTendTime" title="Enter End Time" runat="server" Width="40" 
                                             MaxLength="4" onchange="PrepareTStatement();"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditTendTime" runat="server" MaskType="Time" Mask="99:99"
                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                TargetControlID="txtTendTime" AutoCompleteValue="00:00" UserTimeFormat="TwentyFourHour">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator5" runat="server" ControlToValidate="txtTendTime"
                                                ControlExtender="MaskedEditTendTime" Display="None" IsValidEmpty="false" EmptyValueMessage="Time is required"
                                                InvalidValueMessage="This time is invalid">
                                            </cc1:MaskedEditValidator>
                                            shift.<br />
                                            <asp:DropDownList ID="ddlCatT2" title="Enter Title" runat="server" 
                                            AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatT2_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatT2">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtTemp2" title="Enter Title" runat="server" Visible="false" onchange="SetOtherTTitles(this);PrepareTStatement();"
                                                MaxLength="60" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator36" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtTemp2">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblTemp2" runat="server" Text="Employee Name"></asp:Label>
                                            called
                                            <asp:TextBox ID="txtT2Garage" title="Enter Gararge" runat="server" 
                                                MaxLength="50" onchange="PrepareTStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator23" runat="server" ErrorMessage="Please enter the garage."
                                                Display="Dynamic" ControlToValidate="txtT2Garage">!</asp:RequiredFieldValidator>
                                            at
                                            <asp:TextBox ID="txtTtime" title="Enter Time" runat="server" Width="40" onchange="PrepareTStatement();"
                                             MaxLength="4"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MaskedEditTtime" runat="server" MaskType="Time" Mask="99:99"
                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                TargetControlID="txtTtime" AutoCompleteValue="00:00" UserTimeFormat="TwentyFourHour">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator3" runat="server" ControlToValidate="txtTtime"
                                                ControlExtender="MaskedEditTtime" Display="None" IsValidEmpty="false" EmptyValueMessage="Time is required"
                                                InvalidValueMessage="This time is invalid">
                                            </cc1:MaskedEditValidator>
                                            requesting leave for
                                            <asp:TextBox ID="txtTtype" title="Enter Resaon For Leave" runat="server" onchange="PrepareTStatement();"
                                                MaxLength="50" ></asp:TextBox>.
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator18" runat="server" ErrorMessage="Please enter the type."
                                                Display="Dynamic" ControlToValidate="txtTtype">!</asp:RequiredFieldValidator>
                                            Emergency leave was granted pending proof.<br />
                                            <asp:DropDownList ID="ddlCatT3" runat="server" title="Enter Title"  AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatT3_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatT3">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtTemp3" title="Enter Title" runat="server" Visible="false" onchange="SetOtherTTitles(this);PrepareTStatement();"
                                                MaxLength="60" ></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator37" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtTemp3">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblTemp3" runat="server" Text="Employee Name"></asp:Label>
                                            failed to submit proof within 2 work days.
                                           <br /> 
                                           
                                            As a result
                                            <asp:DropDownList ID="ddlCatT4" runat="server" AutoPostBack="true" 
                                                OnSelectedIndexChanged="ddlCatT4_SelectedIndexChanged" title="Enter Title">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator20" runat="server" 
                                                ControlToValidate="ddlCatT4" Display="Dynamic" 
                                                ErrorMessage="Please select the title.">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtTemp4" runat="server" MaxLength="60" 
                                                onchange="SetOtherTTitles(this);PrepareTStatement();" title="Enter Title" 
                                                Visible="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator38" runat="server" 
                                                ControlToValidate="txtTemp4" Display="Dynamic" Enabled="false" 
                                                ErrorMessage="Please enter the title.">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblTemp4" runat="server" Text="Employee Name"></asp:Label>
                                            &nbsp; was marked absent and docked 8 hours pay.<br /> This is
                                            <asp:DropDownList ID="ddlCatT5" runat="server" AutoPostBack="true" 
                                                OnSelectedIndexChanged="ddlCatT5_SelectedIndexChanged" title="Enter Title">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator21" runat="server" 
                                                ControlToValidate="ddlCatT5" Display="Dynamic" 
                                                ErrorMessage="Please select the title.">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtTemp5" runat="server" MaxLength="60" 
                                                onchange="SetOtherTTitles(this);PrepareTStatement();" title="Enter Title" 
                                                Visible="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator39" runat="server" 
                                                ControlToValidate="txtTemp5" Display="Dynamic" Enabled="false" 
                                                ErrorMessage="Please enter the title.">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblTemp5" runat="server" Text="Employee Name"></asp:Label>
                                            &nbsp;<asp:TextBox ID="txtTincidents" runat="server" MaxLength="4" 
                                                onchange="Javascript:Ordinal('txtTincidents'); PrepareTStatement();" 
                                                title="Enter Incident" Width="30"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator22" runat="server" 
                                                ControlToValidate="txtTincidents" Display="Dynamic" 
                                                ErrorMessage="Please enter the incident.">!</asp:RequiredFieldValidator>
                                            incident in a 12 month period.
                                            <br />                                           
                                            </td>
                                    </tr>
                                </table>
                               
                            </asp:Panel>
                            <asp:Panel ID="pnTypeU" runat="server" Visible="false">
                                <br />
                                <br />
                                <table>
                                    <tr>
                                        <td>
                                            On
                                            <asp:TextBox ID="txtUOnDate" title="Enter WeekDay, Date" runat="server" 
                                                CausesValidation="false" Height="16px" Width="140px" onchange="Javascript:displayWeekdayDate('txtUOnDate');PrepareUStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please select the date."
                                                Display="Dynamic" ControlToValidate="txtUOnDate">!</asp:RequiredFieldValidator>
                                            <cc1:CalendarExtender ID="CalendarExtender11" runat="server" Enabled="True" TargetControlID="txtUOnDate"
                                                PopupButtonID="CalUOnDate" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image id="CalUOnDate" runat="server" SkinId="skidMiniCalButton" />
                                            <asp:DropDownList ID="ddlCatU1" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatU1_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatU1">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUemp1" title="Enter Title" runat="server" Visible="false" onchange= "SetOtherUTitles(this); PrepareUStatement();"
                                                MaxLength="60"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator40" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtUemp1">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblUemp1" runat="server" Text="Employee Name"></asp:Label>
                                            was assigned to<br />
                                            <asp:TextBox ID="txtU1Garage" title="Enter Gararge" runat="server" MaxLength="50"
                                                onchange= "PrepareUStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please enter the garage."
                                                Display="Dynamic" ControlToValidate="txtU1Garage">!</asp:RequiredFieldValidator>
                                            on the
                                            <asp:TextBox ID="txtUstartTime" title="Enter Start Time" runat="server" 
                                                Width="40" onchange= "PrepareUStatement();" MaxLength="4"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="maskEditUstartTime" runat="server" MaskType="Time" Mask="99:99"
                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                TargetControlID="txtUstartTime" AutoCompleteValue="00:00" UserTimeFormat="TwentyFourHour">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator" runat="server" ControlToValidate="txtUstartTime"
                                                ControlExtender="maskEditUstartTime" Display="None" IsValidEmpty="false" EmptyValueMessage="Time is required"
                                                InvalidValueMessage="This time is invalid">
                                            </cc1:MaskedEditValidator>
                                            to
                                            <asp:TextBox ID="txtUendTime" title="Enter End Time" runat="server" 
                                                Width="40" onchange="PrepareUStatement();"
                                                MaxLength="4"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="maskEditUendTime" runat="server" MaskType="Time" Mask="99:99"
                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                TargetControlID="txtUendTime" AutoCompleteValue="00:00" UserTimeFormat="TwentyFourHour">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlToValidate="txtUendTime"
                                                ControlExtender="maskEditUendTime" Display="None" IsValidEmpty="false" EmptyValueMessage="Time is required"
                                                InvalidValueMessage="This time is invalid">
                                            </cc1:MaskedEditValidator>
                                            shift.<br />
                                            <asp:DropDownList ID="ddlCatU2" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatU2_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatU2">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUemp2" title="Enter Title" runat="server" Visible="false" 
                                                MaxLength="60" onchange= "SetOtherUTitles(this); PrepareUStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator41" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtUemp2">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblUemp2" runat="server" Text="Employee Name"></asp:Label>
                                            called
                                            <asp:TextBox ID="txtU2Garage" title="Enter Gararge" runat="server" MaxLength="50"
                                                onchange= "PrepareUStatement();"></asp:TextBox>
                                            at
                                            <asp:TextBox ID="txtUtime" title="Enter Time" runat="server" onchange= "PrepareUStatement();"
                                             MaxLength="4" Width="40"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="maskEditUtime" runat="server" MaskType="Time" Mask="99:99"
                                                MessageValidatorTip="true" OnFocusCssClass="MaskedEditFocus" OnInvalidCssClass="MaskedEditError"
                                                TargetControlID="txtUtime" AutoCompleteValue="00:00" UserTimeFormat="TwentyFourHour">
                                            </cc1:MaskedEditExtender>
                                            <cc1:MaskedEditValidator ID="MaskedEditValidator2" runat="server" ControlToValidate="txtUtime"
                                                ControlExtender="maskEditUtime" Display="Dynamic" IsValidEmpty="false" EmptyValueMessage="Time is required"
                                                InvalidValueMessage="This time is invalid">
                                            </cc1:MaskedEditValidator>
                                            requesting leave for
                                            <asp:TextBox ID="txtUtype" title="Enter Resaon For Leave" runat="server" 
                                                MaxLength="50" onchange= "PrepareUStatement();"></asp:TextBox>.
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please enter the type."
                                                Display="Dynamic" ControlToValidate="txtUtype">!</asp:RequiredFieldValidator>
                                            Emergency leave was granted pending proof.<br />
                                            <asp:DropDownList ID="ddlCatU3" runat="server" title="Enter Title" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatU3_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatU3">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUemp3" title="Enter Title" runat="server" Visible="false"  
                                                MaxLength="60" onchange= "SetOtherUTitles(this); PrepareUStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator42" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtUemp3">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblUemp3" runat="server" Text="Employee Name"></asp:Label>
                                           's proof was deemed unsatisfactory by
                                            <asp:TextBox ID="txtUEmp" title="Enter Name" runat="server" Width="80"
                                                MaxLength="50" onchange= "PrepareUStatement();"></asp:TextBox>.
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Please enter the employee name."
                                                Display="Dynamic" ControlToValidate="txtUEmp">!</asp:RequiredFieldValidator>
                                            <br />
                                            <asp:DropDownList ID="ddlCatU4" title="Enter Title" runat="server" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatU4_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatU4">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUemp4" title="Enter Title" runat="server" Visible="false"
                                                MaxLength="60" onchange= "SetOtherUTitles(this); PrepareUStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator43" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtUemp4">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblUemp4" runat="server" Text="Employee Name"></asp:Label>
                                            &nbsp;was given 5 additional work days to submit satisfactory proof, which was also
                                            unsatisfactory or no additional proof was submitted. As a result
                                            <asp:DropDownList ID="ddlCatU5" title="Enter Title" runat="server"  AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatU5_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatU5">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUemp5" title="Enter Title" runat="server" Visible="false" 
                                                MaxLength="60" onchange= "SetOtherUTitles(this); PrepareUStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator44" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtUemp5">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblUemp5" runat="server" Text="Employee Name"></asp:Label>
                                            &nbsp;was marked absent and docked 8 hours pay.<br />
                                            This is
                                            <asp:DropDownList ID="ddlCatU6" runat="server" title="Enter Title" AutoPostBack="true"
                                                OnSelectedIndexChanged="ddlCatU6_SelectedIndexChanged">
                                                <asp:ListItem Value="A">S/W</asp:ListItem>
                                                <asp:ListItem Value="B">Supv</asp:ListItem>
                                                <asp:ListItem Value="C">GS</asp:ListItem>
                                                <asp:ListItem Value="D">Other</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Please select the title."
                                                Display="Dynamic" ControlToValidate="ddlCatU6">!</asp:RequiredFieldValidator>
                                            <asp:TextBox ID="txtUemp6" title="Enter Title" runat="server" Visible="false" 
                                                MaxLength="60" onchange= "SetOtherUTitles(this); PrepareUStatement();"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator45" Enabled="false" runat="server"
                                                ErrorMessage="Please enter the title." Display="Dynamic" ControlToValidate="txtUemp6">!</asp:RequiredFieldValidator>
                                            <asp:Label ID="lblUemp6" runat="server" Text="Employee Name"></asp:Label>
                                            <asp:TextBox ID="txtUincidents" MaxLength="4" runat="server" 
                                                onchange="Javascript:Ordinal('txtUincidents');PrepareUStatement();" 
                                                Width="30" title="Enter Incident"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Please enter the incident."
                                                Display="Dynamic" ControlToValidate="txtUincidents">!</asp:RequiredFieldValidator>
                                            incident in a 12 month period.
                                            <br />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:HiddenField ID="hdnUniqueID" runat="server" />
                            <asp:HiddenField ID="hdnStatement" runat="server" />
                            <asp:HiddenField ID="hdnType" runat="server" />
                            <asp:HiddenField ID="hdnStmtID" runat="server" />
                            <asp:HiddenField ID="hdnOnDate" runat="server" />
                            <asp:HiddenField ID="hdnText" runat="server" />
                            <asp:HiddenField ID="hdnThisDate" runat="server" />
                            <asp:HiddenField ID="hdnReportedDate" runat="server" />
                            <asp:HiddenField ID="hdnFromLocation" runat="server" />
                            <asp:HiddenField ID="hdnReportedTime" runat="server" />
                            <asp:HiddenField ID="hdnReportingTime" runat="server" />
                            <asp:HiddenField ID="hdnOrgType" runat="server" />
                            <asp:HiddenField ID="hdnTelephoneNo" runat="server" />
                            <asp:ImageButton ID="ibCancel" runat="server" ImageUrl="~/includes/img/btn_cancel.gif"
                                OnClick="ibCancel_Click" OnClientClick="cancelStatement()" />
                            <asp:ImageButton ID="ibSubmit" runat="server" ImageUrl="~/includes/img/btn_submit.gif"
                                OnClick="ibSubmit_Click" OnClientClick="populateParentStatement()" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>


















































































































































































































































































































































