<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  Inherits="DSNY.DSNYCP.DS249.Complaints_ViewComplaintDetails" Codebehind="ViewComplaintDetails.aspx.cs" %>
<%@ Register Src="~/UserControl/WitnessControl.ascx" TagName="Witness" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/VoilationControl.ascx" TagName="Voilation" TagPrefix="vc1" %>
<%@ Register Src="~/UserControl/ComplainantControl.ascx" TagName="Witness" TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/IncidentDateControl.ascx" TagName="Witness" TagPrefix="uc3" %>
<%@ Register Src="~/UserControl/VoilationControl.ascx" TagName="Witness" TagPrefix="uc4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<%-- <link href="../includes/css/DashboardStyle.css" rel="stylesheet" type="text/css" />
    <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />--%>
 
     <base target="_self" />
      <meta http-equiv="Pragma" content="no-cache" />
    
    <script language="javascript" type="text/javascript">
        
        function OpenModalWindow(URL) {
            window.showModalDialog(URL, 'Modal window', 'dialogWidth:950px;dialogHeight:770px;scroll:yes');
            return false;
        }

        function OpenPrint(IndexNo) {
            if (IndexNo != "") {
                var URL = "Print.aspx?Id=" + IndexNo;               
                var dialogResults = window.open(URL, '');
            }
            else {
                alert("ComplaintID is Blank Or Missing");
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <table width="955px" border="0" cellpadding="0px" cellspacing="0px" align="center">
            <tr>
                <td class="headertext" align="left">
                    <asp:Image runat="server" SkinId="skidClear" height="10" width="82" />
                    DS249 
                    <asp:Label ID="lbHeader" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image runat="server" SkinId="skidClear" height="10" width="25" />
                </td>
            </tr>
            <tr>
                <td align="left">
                     <asp:Image runat="server" SkinId="skidClear" width="15" />
                    <asp:ImageButton ID="ibComplaintList" runat="server" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                        OnClick="ImageButton1_Click" CausesValidation="False" Visible="true" />
                    <asp:Image runat="server" SkinId="skidClear" width="25" />
                    <asp:ImageButton ID="ibRoute" runat="server" ImageUrl="~/includes/img/ds249_btn_route_static.png"
                        OnClick="ImageButton2_Click" Visible="False" />
                    <asp:Image runat="server" SkinId="skidClear" width="25" />
                    <asp:ImageButton ID="ibclose" runat="server" ImageUrl="~/includes/img/ds249_btn_close_static.png"
                        OnClientClick="javascript:window.close();" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:ImageButton ID="ibPrint" runat="server" 
                         ImageUrl="~/includes/img/ds249_btn_print_static.png" 
                         onclick="ibPrint_Click" />
                </td>
            </tr>
        </table>
        <asp:Image runat="server" SkinId="skidClear" height="10" width="25" />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table style="width: 832px;" border="0" cellpadding="0px" cellspacing="0px" align="center">
                    <tr>
                        <td>
                            <asp:Image runat="server" SkinId="skidTopCap" />
                        </td>
                    </tr>
                    <tr>
                        <td class="dashStatuBar" height="30">
                            <asp:Label ID="lbMessage" runat="server" Text="" CssClass="feedbackStatus"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table border="1" cellpadding="3" cellspacing="0" width="100%">
                                <tr class="docAltLineStyle">
                                    <td width="25%">
                                        <b>DATE</b><br />
                                        <asp:Label ID="lblDate" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                            Text=""></asp:Label>
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>INDEX NO</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblIndexNumber" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="25%">
                                        &nbsp;
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>REFERENCE NO</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblRefNumber" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docAltLineStyle">
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>NAME(LAST)</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblLastName" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                        <asp:HiddenField ID="hdnEmployeeID" runat="server" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>FIRST</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblFirstName" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>MI</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMName" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>DATE OF BIRTH</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblDOB" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docAltLineStyle">
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>STREET NO</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblStreetNumber" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" width="50%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>STREET NAME</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblStreetName" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>APT</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblAptNo" runat="server" Class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docAltLineStyle">
                                    <td colspan="2" width="50%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>CITY</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblCity" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        <table border="0" width="25%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>STATE</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblState" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>ZIP</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblZip" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label> 
                                                        <asp:Label ID="lblZipPrefix" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docAltLineStyle">
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>APPOINTMENT DATE</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblApptDate" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="25%">
                                        &nbsp;
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>PAYROLL LOCATION</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblPayrollLoc" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="25%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>CHART DAY NO</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblChartDay" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                            Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="2" width="50%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>DISTRICT / DIVISION</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblLocation" runat="server" class="labelStyle" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" width="50%">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <b>BOROUGH</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblBorough" runat="server" class="labelStyle" Text=""></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td colspan="4" class="dashBottom">
                                        <asp:Image runat="server" SkinId="skidClear" height="10" />
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="4" height="20" align="left">
                                        <b>RULES AND ORDERS VIOLATED AS FOLLOWS</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table id="form1:chargeItemsTable" style="border-top: 0px" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="phVoilations" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="4">
                                        <b>COMMENTS</b><br />
                                        <asp:Label ID="lblComments" runat="server" class="labelStyle" Height="80px" Width="100%"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="4" height="20">
                                        <b>COMPLAINANT&#39;S STATEMENT (SPECIFY ACTS WHICH CONSTITUTE VIOLATIONS)</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="pnComplainant" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="4">
                                        <b>WITNESSES</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <table style="border-top: 0px" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="50%">
                                                    <asp:PlaceHolder ID="pnWitnesses" runat="server"></asp:PlaceHolder>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkProbation" runat="server" 
                                                        Text="Check If Employee Under Probation" Enabled="False" Font-Bold="true"/>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="dashBottom">
                                        <asp:Image runat="server" SkinId="skidClear" height="15" />
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="4">
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="50%">
                                                    <table border="0">
                                                        <tbody>
                                                            <tr class="docLineStyle">
                                                                <td>
                                                                    <b>RESPONDENT SERVED WITH CHARGE BY </b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblChargesServedBy" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                                        Text=""></asp:Label>
                                                                    <asp:HiddenField ID="hdnChargesServedByRefNo" runat="server" />
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="50%">
                                                    <table border="0">
                                                        <tbody>
                                                            <tr class="docLineStyle">
                                                                <td>
                                                                    <b>DATE</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblChargesServedDt" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                                        Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docAltLineStyle">
                                    <td colspan="4">
                                        <table border="0" class="docAltLineStyle" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="50%">
                                                    <table border="0">
                                                        <tbody>
                                                            <tr class="docAltLineStyle">
                                                                <td>
                                                                    <b>SUSPENDED BY ORDER OF </b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:HiddenField ID="hdnSuspendedByRefNo" runat="server" />
                                                                    <asp:Label ID="lblSuspendedBy" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                                        Text="" Width="202px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="50%">
                                                    <table border="0">
                                                        <tbody>
                                                            <tr class="docAltLineStyle">
                                                                <td>
                                                                    <b>DATE</b>
                                                                </td>
                                                            </tr>
                                                            <tr class="docAltLineStyle">
                                                                <td>
                                                                    <asp:Label ID="lblSuspendedDt" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                                        Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="4">
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="50%">
                                                    <table border="0">
                                                        <tbody>
                                                            <tr class="docLineStyle">
                                                                <td>
                                                                    <b>SUSPENSION LIFTED BY ORDER OF </b>
                                                                </td>
                                                            </tr>
                                                            <tr class="docLineStyle">
                                                                <td>
                                                                    <asp:HiddenField ID="hdnSuspensionByRefNo" runat="server" />
                                                                    <asp:Label ID="lblSuspensionBy" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                                        Text="" Width="230px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="50%">
                                                    <table border="0">
                                                        <tbody>
                                                            <tr class="docLineStyle">
                                                                <td>
                                                                    <b>DATE</b>
                                                                </td>
                                                            </tr>
                                                            <tr class="docLineStyle">
                                                                <td>
                                                                    <asp:Label ID="lblSuspensionDt" runat="server" class="labelStyle" CssClass="inputFieldStyle"
                                                                        Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="dashBottom">
                                        <asp:Image runat="server" SkinId="skidClear" height="15" />
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="4">
                                        <table border="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td width="50%">
                                                    <table border="0">
                                                        <tbody>
                                                            <tr class="docLineStyle">
                                                                <td>
                                                                    <b>ROUTE DOCUMENT </b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblRouteType" runat="server" class="labelStyle" Width="199px"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td width="50%">
                                                    <table border="0">
                                                        <tbody>
                                                            <tr class="docLineStyle">
                                                                <td>
                                                                    <b>DATE</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lbltypeDate" runat="server" class="labelStyle"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="dashBottom">
                                        <asp:Image runat="server" SkinId="skidClear" height="15" />
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="4">
                                        <b>HISTORY ::
                                            <asp:Label ID="lblFullName" runat="server"></asp:Label>
                                        </b>
                                    </td>
                                </tr>
                                <tr class="docLineStyle">
                                    <td colspan="5">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                            <asp:GridView ID="gvHistory" runat="server" AlternatingRowStyle-BackColor="#dddddd"
                                                AutoGenerateColumns="False" BorderColor="#235705" EmptyDataText="No Violation History Found"
                                                HeaderStyle-BackColor="#CCCCCC" HeaderStyle-Font-Bold="true" OnRowDataBound="gvHistory_RowDataBound"
                                                Width="100%">
                                                <EmptyDataRowStyle CssClass="fromdate" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Index NO">
                                                        <ItemTemplate>
                                                            <a id="lkIndex" href="#" onclick='OpenModalWindow(&#039;ViewComplaintInfo.aspx?ID=<%# Eval("IndexNo")%>&amp;Mode=VIEW&#039;);return false;'
                                                                style="font-size: 1.0em;">
                                                                <%# Eval("ComplaintId")%></a>
                                                        </ItemTemplate>
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="80px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Size="1.2em" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="80px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="IndexNo" DataFormatString="" HeaderText="Index No">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="150px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Bold="false" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                            Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ViolationDate" DataFormatString="" HeaderText="Violation Date">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="150px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Bold="false" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                            Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Violations" HeaderText="Charges">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="80px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Bold="false" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="80px"
                                                            Wrap="False" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="ViolationDescription">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="180px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Bold="false" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="180px"
                                                            Wrap="False" />
                                                    </asp:BoundField>

                                                    <asp:BoundField DataField="Penalty" DataFormatString="" HeaderText="Penalty">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="180px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Bold="false" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="180px"
                                                            Wrap="False" />
                                                    </asp:BoundField>

                                                     <asp:BoundField DataField="RoutingLocation" HeaderText="Routed To">
                                                        <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                            VerticalAlign="Middle" Width="150px" Wrap="False" />
                                                        <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Bold="false" Font-Names="Arial,Verdana,sans-serif"
                                                            Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                            Wrap="False" />
                                                    </asp:BoundField>
                                                    </Columns>
                                                <HeaderStyle BackColor="#a4d49a" ForeColor="White" Font-Underline="False" Wrap="False"
                                                    Font-Bold="True" />
                                                <AlternatingRowStyle BackColor="#CCCCCC" />
                                            </asp:GridView>
                                        </table>
                                    </td>
                                </tr>                               
                                <tr>
                                    <td colspan="4" class="dashStatuBar" height="30">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:Image runat="server" SkinId="skidBottomCap" />
                                    </td>
                                </tr>
                            </table>
                            <asp:DropDownList Visible="false" ID="ddlRoutingLocation" runat="server">
                                <asp:ListItem Value="">[SELECT]</asp:ListItem>
                                <asp:ListItem Value="A">Advocate</asp:ListItem>
                                <asp:ListItem Value="B">BCAD</asp:ListItem>
                                <asp:ListItem Value="M">Medical</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList Visible="false" ID="ddlLocation" runat="server" AutoPostBack="True"
                                AppendDataBoundItems="true">
                                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList Visible="false" ID="ddlBorough" runat="server" AppendDataBoundItems="true">
                                <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
                            </asp:DropDownList>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</asp:Content>
