﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using System.Text;
using System.IO;
using DSNY.DSNYCP.DTO;

namespace DSNY.DSNYCP.DS249.Complaints
{

    public partial class Upload : System.Web.UI.Page
    {

        private long m_ParentID;
        private short m_CountID;
        public long ParentID
        {
            get { return m_ParentID; }
            set { m_ParentID = value; }
        }
        private string m_ChildID1;

        public string ChildID1
        {
            get { return m_ChildID1; }
            set { m_ChildID1 = value; }
        }
        public Int16 CountID
        {
            get { return m_CountID; }
            set { m_CountID = value; }
        }
        private bool is_New;

        public bool Is_New
        {
            get { return is_New; }
            set { is_New = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            btnAttach.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_attach_roll.png'");
            btnAttach.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_attach.png'");

            CancelButton.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_close_roll.png'");
            CancelButton.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_close_static.png'");

            string onclick = "javascript:self.close();return false;";

            CancelButton.Attributes.Add("onclick", onclick);

            if (Request.QueryString["ParentID"] != null)
            {
                ParentID = Convert.ToInt64(Request.QueryString["ParentID"]);
            }

            if (Request.QueryString["ChildID1"] != null)
            {
                ChildID1 = Convert.ToString(Request.QueryString["ChildID1"]);
            }

            if (Request.QueryString["CountID"] != null)
            {
                CountID = Convert.ToInt16(Request.QueryString["CountID"]);
            }
            if (Request.QueryString["Mode"] != null)
            {
                if (Request.QueryString["Mode"] == "NEW")
                {
                    Is_New = true;
                }
                else
                {
                    Is_New = false;
                }
            }

            if (Request.QueryString["PageFrom"] != null)
            {
                if (Request.QueryString["PageFrom"] == "Advocate")
                {
                    FileUpload1.Visible = false;
                    btnAttach.Visible = false;
                }
                else
                {
                    FileUpload1.Visible = true;
                    btnAttach.Visible = true;
                }
            }
            else
            {
                FileUpload1.Visible = true;
                btnAttach.Visible = true;
            }


            if (!Page.IsPostBack)
            {
                if (Is_New == false)
                {
                    
                    BingGrid();
                }
                else
                {
                    LoadFromSession();
                }

            }



        }

        private void SetVisibilityFalse()
        {
            gvUploadFile.Columns[0].Visible = false;
            gvUploadFile.Columns[1].Visible = true;
            gvUploadFile.Columns[2].Visible = false;
            gvUploadFile.Columns[3].Visible = true;



            if (Request.QueryString["PageFrom"] != null)
            {
                if (Request.QueryString["PageFrom"] == "Advocate")
                {
                    gvUploadFile.Columns[6].Visible = false;
                }
                else
                {
                    gvUploadFile.Columns[6].Visible = true;
                }
            }
            else
            {
                gvUploadFile.Columns[6].Visible = true;
            }
        }
        private void SetVisibilityTrue()
        {
            gvUploadFile.Columns[0].Visible = false;
            gvUploadFile.Columns[1].Visible = true;
            gvUploadFile.Columns[2].Visible = false;
            gvUploadFile.Columns[3].Visible = true;



            if (Request.QueryString["PageFrom"] != null)
            {
                if (Request.QueryString["PageFrom"] == "Advocate")
                {
                    gvUploadFile.Columns[6].Visible = false;
                }
                else
                {
                    gvUploadFile.Columns[6].Visible = true;
                }
            }
            else
            {
                gvUploadFile.Columns[6].Visible = true;
            }
        }

        private void LoadFromSession()
        {
            List<UploadFile> upLoadDTO;
            if (Session["upLoadDTO"] != null)
            {
                upLoadDTO = (List<UploadFile>)Session["upLoadDTO"];
                if (upLoadDTO.Count > 0)
                {

                    gvUploadFile.DataSource = upLoadDTO;
                    gvUploadFile.DataBind();
                }
            }
            else
            {
                lblError.Text = "No attachment found.";
            }
        }

        private void BingGrid()
        {
            List<UploadFile> upLoadDTO;
            UploadFile uf = new UploadFile();

            
           

            //Commented below line to resolve TFS#7004
            //if (Session["upLoadDTO"] != null)


            if (Session["upLoadDTO"] != null && Request.QueryString["PageFrom"] != "Advocate")

            {
                gvUploadFile.DataSource = Session["upLoadDTO"];
                gvUploadFile.DataBind();
            }
            else
            {
                upLoadDTO = uf.LoadComplaintAttachments(0, ParentID, ChildID1);              
                Session["upLoadDTO"] = upLoadDTO;
                if (upLoadDTO.Count > 0)
                {
                    gvUploadFile.DataSource = upLoadDTO;
                    gvUploadFile.DataBind();
                }
                else
                {
                    lblError.Text = "No attachment found.";
                }
           }
        }


        protected void gvUploadFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            int IndexNo = (int)gvUploadFile.DataKeys[e.RowIndex].Value;

            List<UploadFile> upLoadDTO;

            upLoadDTO = GetUpLoadFiles();

            if (upLoadDTO != null)
            {

                ArrayList ItemList = new ArrayList();

                foreach (UploadFile upFile in upLoadDTO)
                    if (upFile.IndexNo == IndexNo)
                    {
                   
                        ItemList.Add(CountID);
                        ItemList.Add(upFile.ChildID);

                        upLoadDTO.Remove(upFile);
                        gvUploadFile.DataSource = upLoadDTO;
                        gvUploadFile.DataBind();

                        
                        Session["Pannel"] = ItemList;
                        break;
                    }

                Session["upLoadDTO"] = upLoadDTO;

            }

        }
        protected void gvUploadFile_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                SetVisibilityFalse();
                ImageButton viewFile = (ImageButton)e.Row.Cells[5].FindControl("ViewButton");

                string onclick = "javascript:window.open('ViewDocument.aspx?indexId=" + e.Row.Cells[1].Text + "&ParentID=" + ParentID + "&ChildID1=" + e.Row.Cells[3].Text + "&Mode=" + Request.QueryString["Mode"] + "','ViewDocument','scrollbars=yes,resizable=yes,width=600,height=600');return false;";


                viewFile.Attributes.Add("onclick", onclick);

                ImageButton View = (ImageButton)e.Row.FindControl("ViewButton");

                View.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_view_roll.png'");
                View.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_view_static.png'");

                ImageButton Delete = (ImageButton)e.Row.FindControl("deleteButton");

                Delete.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_clear_roll.png'");
                Delete.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_clear_static.png'");

            }


        }
        private List<UploadFile> GetUpLoadFiles()
        {
            if (Session["upLoadDTO"] != null)
            {
                return (List<UploadFile>)Session["upLoadDTO"];
            }
            else
            {
                return null;
            }
        }

        protected void btnAttach_Click(object sender, EventArgs e)
        {

            if (FileUpload1.FileBytes.Length > 5000000)
            {

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Please be Patient you file may take time to Upload . File Size greater that 5mb !');", true);
            }  

                lblError.Text = string.Empty;

                int counter = 1;


                List<UploadFile> upLoadDTO= null;

                if (FileUpload1.HasFile)
                {
                    using (BinaryReader reader = new BinaryReader(FileUpload1.PostedFile.InputStream))
                    {
                        upLoadDTO = GetUpLoadFiles();
                        if (upLoadDTO != null)
                        {
                            counter = upLoadDTO.Count + 1;
                        }

                        UploadFile upLoadValue = new UploadFile();
                        byte[] image = reader.ReadBytes(FileUpload1.PostedFile.ContentLength);
                        upLoadValue.IndexNo = counter;
                        upLoadValue.ParentID = ParentID;
                        
                        upLoadValue.ChildID = ChildID1;

                        upLoadValue.AttachmentName = FileUpload1.FileName;
                        upLoadValue.Image = image;
                        upLoadValue.ContentType = FileUpload1.PostedFile.ContentType;
                        upLoadValue.PanelId = CountID;                      

                        upLoadDTO = GetUpLoadFiles();

                        if (upLoadDTO != null)
                        {
                            upLoadDTO.Add(upLoadValue);
                            Session["upLoadDTO"] = upLoadDTO;
                        }
                        else
                        {
                            upLoadDTO = new List<UploadFile>();
                            upLoadDTO.Add(upLoadValue);
                            Session["upLoadDTO"] = upLoadDTO;
                        }
                    }

                    gvUploadFile.DataSource = upLoadDTO;
                    gvUploadFile.DataBind();

                }         

        }

    }
}
