﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Collections;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.DS249
{
    public partial class Complaints_ComplainantStatement : System.Web.UI.Page
    {
        String Statement;
        ComplainantList complaintantList;
        Complainant complainant;
        
        string selectedTitle;
        protected void Page_Init(object sender, EventArgs e)
        {

            #region Validate Controls
            ddlOccurQ.Items.Insert(0, "");
            ddlDoQ.Items.Insert(0, "");
            ddlCatQ1.Items.Insert(0, "");
            ddlCatQ2.Items.Insert(0, "");
            ddlCatQ3.Items.Insert(0, "");
            ddlCatQ4.Items.Insert(0, "");

            ddlCatT1.Items.Insert(0, "");
            ddlCatT2.Items.Insert(0, "");
            ddlCatT3.Items.Insert(0, "");
            ddlCatT4.Items.Insert(0, "");
            ddlCatT5.Items.Insert(0, "");

            ddlCatU1.Items.Insert(0, "");
            ddlCatU2.Items.Insert(0, "");
            ddlCatU3.Items.Insert(0, "");
            ddlCatU4.Items.Insert(0, "");
            ddlCatU5.Items.Insert(0, "");
            ddlCatU6.Items.Insert(0, "");
            ddlACategory.Items.Insert(0, "");
            ddlBCategory.Items.Insert(0, "");
            ddlCCategory.Items.Insert(0, "");

            ddlDCategory.Items.Insert(0, "");
            ddlECategory.Items.Insert(0, "");
            ddlFCategory.Items.Insert(0, "");


            ddlGCategory.Items.Insert(0, "");
            ddlHCategory.Items.Insert(0, "");
            ddlICategory.Items.Insert(0, "");

            ddlJ1Category.Items.Insert(0, "");
            ddlJ2Category.Items.Insert(0, "");

            ddlP1Category.Items.Insert(0, "");
            ddlP2Category.Items.Insert(0, "");


            txtQstartTime.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
            txtQendTime.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
            
            txtTstartTime.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
            txtTendTime.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
            txtTtime.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");            

            txtUstartTime.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
            txtUendTime.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");
            txtUtime.Attributes.Add("onkeypress", "javascript:return allownumbers(event);");            

            #endregion
            

        }
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Complaint complaint;

            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);           

                lbMessage.Text = String.Empty;
                hdnStmtID.Value = Request.QueryString["StmtCID"].ToString();
                ibSubmit.Visible = false;
               
                complaint = (Complaint)Session["complaint"];
                if (complaint != null)
                    hdnEmpName.Value = complaint.FullName.ToString();

                if (IsPostBack)
                {
                    txtQOnDate.Attributes.Add("readonly", "readonly");
                    txtTOnDate.Attributes.Add("readonly", "readonly");
                    txtUOnDate.Attributes.Add("readonly", "readonly");

                    if (ddlType.SelectedItem.Value != String.Empty)
                        ibSubmit.Visible = true;
                    switch (ddlType.SelectedItem.Value)
                    {
                        case "A":
                            PrepareStatementA();
                            break;
                        case "B":
                            PrepareStatementB();
                            break;
                        case "C":
                            PrepareStatementC();
                            break;
                        case "D":
                            PrepareStatementD();
                            break;
                        case "E":
                            PrepareStatementE();
                            break;
                        case "F":
                            PrepareStatementF();
                            break;
                        case "G":
                            PrepareStatementG();
                            break;
                        case "H":
                            PrepareStatementH();
                            break;
                        case "I":
                            PrepareStatementI();
                            break;
                        case "J":
                            PrepareStatementJ();
                            break;
                        case "K":
                            PrepareStatementK();
                            break;
                        case "L":
                            PrepareStatementL();
                            break;
                        case "M":
                            PrepareStatementM();
                            break;
                        case "N":
                            PrepareStatementN();
                            break;
                        case "O":
                            PrepareStatementO();
                            break;
                        case "P":
                            PrepareStatementP();
                            break;
                        case "Q":
                            PrepareStatementQ();
                            break;
                        case "R":
                            PrepareStatementR();
                            break;
                        case "S":
                            PrepareStatementS();
                            break;
                        case "T":
                            PrepareStatementT();
                            break;
                        case "U":
                            PrepareStatementU();
                            break;
                        default:
                            ChangeControlStatus();
                            break;
                    }
                }            
        }
        protected void ibSubmit_Click(object sender, ImageClickEventArgs e)
        {
            hdnintTitle.Value = "";
            hdnstrTitleDesc.Value = "";
        }

        protected void ibCancel_Click(object sender, ImageClickEventArgs e)
        {

        }

        #region Private Methods
        /// <summary>
        /// This method instantiates the complaint from the list
        /// </summary>
        private void InstantiateComplaint()
        {           
                complaintantList = (ComplainantList)Session["ComplainantList"];
                complainant = complaintantList.List.Find(delegate(Complainant p) { return p.UniqueId == Convert.ToInt64(hdnUniqueID.Value); });
            
        }
        /// <summary>
        /// This method saves the data from the form.
        /// </summary>
        private void ClearData()
        {
            complainant.OnDate = DateTime.MinValue;
            complainant.Text = String.Empty;
            complainant.ThisDate = DateTime.MinValue;
            complainant.ReportedDate = DateTime.MinValue;
            complainant.FromLocation = String.Empty;
            complainant.ReportedTime = String.Empty;
            complainant.ReportingTime = String.Empty;
            complainant.TelephoneNo = String.Empty;
            complainant.Statement = String.Empty;

        }

        private String title(DropDownList ddl, RequiredFieldValidator ddlRq, TextBox txt, RequiredFieldValidator txtRq)
        {

            if (ddl.SelectedIndex == 4)
            {
                txt.Visible = true;
                txtRq.Enabled = true;
                ddlRq.Enabled = false;
                selectedTitle = txt.Text;
            }

            else
            {
                txt.Visible = false;
                txtRq.Enabled = false;
                ddlRq.Enabled = true;
                selectedTitle = ddl.SelectedItem.Text;
            }
            return selectedTitle;
        }
        #region Statement A
        /// <summary>
        /// This method changes the status of the controls as per the statement list.
        /// </summary>
        private void ChangeControlStatus()
        {
            hdnStatement.Value = String.Empty;
            foreach (Control c in UpdatePanel1.Controls)
            {
                foreach (Control ctrl in c.Controls)
                {
                    if (ctrl is Panel)
                        ctrl.Visible = false;
                }
            }

        }


        /// <summary>
        /// This method changes the status of the controls as per the statement listto be perpared.
        /// </summary>
        private void PrepareStatementA()
        {
            String StatementA;           
                ChangeControlStatus();
                pnTypeA.Visible = true;
                lbAEmpName.Text = hdnEmpName.Value;
                string ACat= title(ddlACategory, RequiredFieldValidatorACategory, txtACategory, RequiredFieldValidatortxtA);
                StatementA = "PAP 2007-04.";
                StatementA = StatementA + Environment.NewLine + "On {0}, {1} {2} Category A employee, reported sick/LODI and began medical leave. You were ordered to report to the DS Clinic on {3} and failed to report as required. You also failed to submit any / adequate medical documentation to substantiate the illness and the inablility to travel.";
                StatementA = StatementA + Environment.NewLine + "As of this date {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                Statement = StatementA;
                hdnStatement.Value = string.Format(Statement, txtAOnDate.Text, ACat,lbAEmpName.Text, txtAReportingDate.Text, txtAThisDate.Text);
           
        }
        /// <summary>
        /// This method is on change text change event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtAOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementA;
            lbAEmpName.Text = hdnEmpName.Value;
            string ACat = title(ddlACategory, RequiredFieldValidatorACategory, txtACategory, RequiredFieldValidatortxtA);
            StatementA = "PAP 2007-04.";
            StatementA = StatementA + Environment.NewLine + "On {0}, {1} {2} Category A employee, reported sick/LODI and began medical leave. You were ordered to report to the DS Clinic on {3} and failed to report as required. You also failed to submit any / adequate medical documentation to substantiate the illness and the inablility to travel.";
            StatementA = StatementA + Environment.NewLine + "As of this date {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            Statement = StatementA;
            hdnStatement.Value = string.Format(Statement, txtAOnDate.Text, ACat, lbAEmpName.Text, txtAReportingDate.Text, txtAThisDate.Text);            
        }
        /// <summary>
        /// This method is on change text change event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtAReportingDate_TextChanged(object sender, EventArgs e)
        {
            String StatementA;
            lbAEmpName.Text = hdnEmpName.Value;
            string ACat = title(ddlACategory, RequiredFieldValidatorACategory, txtACategory, RequiredFieldValidatortxtA);
            StatementA = "PAP 2007-04.";
            StatementA = StatementA + Environment.NewLine + "On {0}, {1} {2} Category A employee, reported sick/LODI and began medical leave. You were ordered to report to the DS Clinic on {3} and failed to report as required. You also failed to submit any / adequate medical documentation to substantiate the illness and the inablility to travel.";
            StatementA = StatementA + Environment.NewLine + "As of this date {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            Statement = StatementA;
            hdnStatement.Value = string.Format(Statement, txtAOnDate.Text, ACat,lbAEmpName.Text, txtAReportingDate.Text, txtAThisDate.Text);           
        }
        /// <summary>
        /// This method is on change text change event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtAThisDate_TextChanged(object sender, EventArgs e)
        {
            String StatementA;
            lbAEmpName.Text = hdnEmpName.Value;
            string ACat = title(ddlACategory, RequiredFieldValidatorACategory, txtACategory, RequiredFieldValidatortxtA);
            StatementA = "PAP 2007-04.";
            StatementA = StatementA + Environment.NewLine + "On {0}, {1} {2} Category A employee, reported sick/LODI and began medical leave. You were ordered to report to the DS Clinic on {3} and failed to report as required. You also failed to submit any / adequate medical documentation to substantiate the illness and the inablility to travel.";
            StatementA = StatementA + Environment.NewLine + "As of this date {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            Statement = StatementA;
            hdnStatement.Value = string.Format(Statement, txtAOnDate.Text, ACat,lbAEmpName.Text, txtAReportingDate.Text, txtAThisDate.Text);           
        }

        #endregion

        #region Statement B

        /// <summary>
        /// This method prepares the statement by collecting the assigned values 
        /// </summary>
        private void PrepareStatementB()
        {
            String StatementB;
            ChangeControlStatus();
                pnTypeB.Visible = true;
                lbBEmpName.Text = hdnEmpName.Value;
                string BCategory = title(ddlBCategory, RequiredFieldValidatorBCategory, txtBCategory, RequiredFieldValidatortxtB);
                StatementB = "PAP 2007-04.";
                StatementB = StatementB + Environment.NewLine + "On {0}, {1} {2} reported sick and began medical leave. As a Catetory B employee, employee is required to report to the DS Clinic on the second day of sick leave. Employee failed to report as required.";
                StatementB = StatementB + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
                StatementB = StatementB + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                Statement = StatementB;
                hdnStatement.Value = string.Format(Statement, txtBOnDate.Text,BCategory,lbBEmpName.Text, txtBReportedDate.Text, txtBFromLoc.Text, txtBThisDate.Text);           
        }
        /// <summary>
        /// This method is on change text change event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtBOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementB;
            lbBEmpName.Text = hdnEmpName.Value;
            string BCategory = title(ddlBCategory, RequiredFieldValidatorBCategory, txtBCategory, RequiredFieldValidatortxtB);
            StatementB = "PAP 2007-04.";
            StatementB = StatementB + Environment.NewLine + "On {0}, {1} {2} reported sick and began medical leave. As a Catetory B employee, employee is required to report to the DS Clinic on the second day of sick leave. Employee failed to report as required.";
            StatementB = StatementB + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
            StatementB = StatementB + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            Statement = StatementB;
            hdnStatement.Value = string.Format(Statement, txtBOnDate.Text, BCategory, lbBEmpName.Text, txtBReportedDate.Text, txtBFromLoc.Text, txtBThisDate.Text);
           
        }
        /// <summary>
        /// This method is on change text change event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtBReportedDate_TextChanged(object sender, EventArgs e)
        {
            String StatementB;
            lbBEmpName.Text = hdnEmpName.Value;
            string BCategory = title(ddlBCategory, RequiredFieldValidatorBCategory, txtBCategory, RequiredFieldValidatortxtB);
            StatementB = "PAP 2007-04.";
            StatementB = StatementB + Environment.NewLine + "On {0}, {1} {2} reported sick and began medical leave. As a Catetory B employee, employee is required to report to the DS Clinic on the second day of sick leave. Employee failed to report as required.";
            StatementB = StatementB + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
            StatementB = StatementB + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            Statement = StatementB;
            hdnStatement.Value = string.Format(Statement, txtBOnDate.Text, BCategory, lbBEmpName.Text, txtBReportedDate.Text, txtBFromLoc.Text, txtBThisDate.Text);
        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtBFromLoc_TextChanged(object sender, EventArgs e)
        {
            String StatementB;
            lbBEmpName.Text = hdnEmpName.Value;
            string BCategory = title(ddlBCategory, RequiredFieldValidatorBCategory, txtBCategory, RequiredFieldValidatortxtB);
            StatementB = "PAP 2007-04.";
            StatementB = StatementB + Environment.NewLine + "On {0}, {1} {2} reported sick and began medical leave. As a Catetory B employee, employee is required to report to the DS Clinic on the second day of sick leave. Employee failed to report as required.";
            StatementB = StatementB + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
            StatementB = StatementB + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            Statement = StatementB;
            hdnStatement.Value = string.Format(Statement, txtBOnDate.Text, BCategory, lbBEmpName.Text, txtBReportedDate.Text, txtBFromLoc.Text, txtBThisDate.Text);
          
        }
        /// <summary>
        /// This method is on change text change event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtBThisDate_TextChanged(object sender, EventArgs e)
        {
            String StatementB;
            lbBEmpName.Text = hdnEmpName.Value;
            string BCategory = title(ddlBCategory, RequiredFieldValidatorBCategory, txtBCategory, RequiredFieldValidatortxtB);
            StatementB = "PAP 2007-04.";
            StatementB = StatementB + Environment.NewLine + "On {0}, {1} {2} reported sick and began medical leave. As a Catetory B employee, employee is required to report to the DS Clinic on the second day of sick leave. Employee failed to report as required.";
            StatementB = StatementB + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
            StatementB = StatementB + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            Statement = StatementB;
            hdnStatement.Value = string.Format(Statement, txtBOnDate.Text, BCategory, lbBEmpName.Text, txtBReportedDate.Text, txtBFromLoc.Text, txtBThisDate.Text);
        }

        #endregion

        #region Statement C
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementC()
        {
                String StatementC;  
                ChangeControlStatus();
                pnTypeC.Visible = true;                  
                lbCEmpName.Text = hdnEmpName.Value;
                string CCategory = title(ddlCCategory, RequiredFieldValidatorCCategory, txtCCategory, RequiredFieldValidatortxtC);
                StatementC = "PAP 2007-04.";
                StatementC = StatementC + Environment.NewLine + "On {0},{1} {2} reported sick / LODI and began medical leave. As a Catetory C employee, employee is required to report to the DS Clinic on the first day of medical leave or on the first day the DS Clinic is open. Employee failed to report as required.";
                StatementC = StatementC + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
                StatementC = StatementC + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                Statement = StatementC;
                hdnStatement.Value = string.Format(Statement, txtCOnDate.Text,CCategory ,lbCEmpName.Text, txtCReportedDate.Text, txtCFromLoc.Text, txtCThisDate.Text);
           
        }


        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtCOnDate_TextChanged(object sender, EventArgs e)
        {            

                if (txtCOnDate.Text != String.Empty)
                {
                    String StatementC;
                    lbCEmpName.Text = hdnEmpName.Value;
                    string CCategory = title(ddlCCategory, RequiredFieldValidatorCCategory, txtCCategory, RequiredFieldValidatortxtC);
                    StatementC = "PAP 2007-04.";
                    StatementC = StatementC + Environment.NewLine + "On {0},{1} {2} reported sick / LODI and began medical leave. As a Catetory C employee, employee is required to report to the DS Clinic on the first day of medical leave or on the first day the DS Clinic is open. Employee failed to report as required.";
                    StatementC = StatementC + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
                    StatementC = StatementC + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                    Statement = StatementC;
                    hdnStatement.Value = string.Format(Statement, txtCOnDate.Text,CCategory, lbCEmpName.Text, txtCReportedDate.Text, txtCFromLoc.Text, txtCThisDate.Text);
                }            
        }
        /// <summary>
        /// This method is on change text change event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtCReportedDate_TextChanged(object sender, EventArgs e)
        {          
                if (txtCReportedDate.Text != String.Empty)
                {
                    String StatementC;
                    lbCEmpName.Text = hdnEmpName.Value;
                    string CCategory = title(ddlCCategory, RequiredFieldValidatorCCategory, txtCCategory, RequiredFieldValidatortxtC);
                    StatementC = "PAP 2007-04.";
                    StatementC = StatementC + Environment.NewLine + "On {0},{1} {2} reported sick / LODI and began medical leave. As a Catetory C employee, employee is required to report to the DS Clinic on the first day of medical leave or on the first day the DS Clinic is open. Employee failed to report as required.";
                    StatementC = StatementC + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
                    StatementC = StatementC + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                    Statement = StatementC;
                    hdnStatement.Value = string.Format(Statement, txtCOnDate.Text, CCategory, lbCEmpName.Text, txtCReportedDate.Text, txtCFromLoc.Text, txtCThisDate.Text);

                }            
        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtCFromLoc_TextChanged(object sender, EventArgs e)
        {           
                if (txtCFromLoc.Text != String.Empty)
                {
                    String StatementC;
                    lbCEmpName.Text = hdnEmpName.Value;
                    string CCategory = title(ddlCCategory, RequiredFieldValidatorCCategory, txtCCategory, RequiredFieldValidatortxtC);
                    StatementC = "PAP 2007-04.";
                    StatementC = StatementC + Environment.NewLine + "On {0},{1} {2} reported sick / LODI and began medical leave. As a Catetory C employee, employee is required to report to the DS Clinic on the first day of medical leave or on the first day the DS Clinic is open. Employee failed to report as required.";
                    StatementC = StatementC + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
                    StatementC = StatementC + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                    Statement = StatementC;
                    hdnStatement.Value = string.Format(Statement, txtCOnDate.Text, CCategory, lbCEmpName.Text, txtCReportedDate.Text, txtCFromLoc.Text, txtCThisDate.Text);
                }           
        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtCThisDate_TextChanged(object sender, EventArgs e)
        {
            if (txtCThisDate.Text != String.Empty)
                {
                    String StatementC;
                    lbCEmpName.Text = hdnEmpName.Value;
                    string CCategory = title(ddlCCategory, RequiredFieldValidatorCCategory, txtCCategory, RequiredFieldValidatortxtC);
                    StatementC = "PAP 2007-04.";
                    StatementC = StatementC + Environment.NewLine + "On {0},{1} {2} reported sick / LODI and began medical leave. As a Catetory C employee, employee is required to report to the DS Clinic on the first day of medical leave or on the first day the DS Clinic is open. Employee failed to report as required.";
                    StatementC = StatementC + "Employee reported to the DS Clinic on {3} and failed to present any / adequate medical documentation to substantiate the illness and/ or inability to travel to the DS Clinic on / from {4}";
                    StatementC = StatementC + Environment.NewLine + "As of this date {5}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                    Statement = StatementC;
                    hdnStatement.Value = string.Format(Statement, txtCOnDate.Text, CCategory, lbCEmpName.Text, txtCReportedDate.Text, txtCFromLoc.Text, txtCThisDate.Text);
                }            
        }

        #endregion

        #region Statement D
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementD()
        {
                 String StatementD;
                ChangeControlStatus();            
                pnTypeD.Visible = true;              
                lbDEmpName.Text = hdnEmpName.Value;
                string DCategory = title(ddlDCategory, RequiredFieldValidatorDCategory, txtDCategory, RequiredFieldValidatortxtD);
                StatementD = "PAP 2007-04.";
                StatementD = StatementD + Environment.NewLine + "On {0},{1} {2}, a category {3}, reported to the DS Clinic at {4} hours.";
                StatementD = StatementD + Environment.NewLine + "Employee failed to report at {5} hours as required by PAP 2007-04.";
                StatementD = StatementD + Environment.NewLine + "Employee is to be marked late {6} and docked the same by using Payroll Code 3613 in the PMS System and an entry is to be made in the absence and lateness log.";
                Statement = StatementD;
                hdnStatement.Value = string.Format(Statement, txtDOnDate.Text, DCategory,ddlCategory.SelectedItem.Text, lbDEmpName.Text, txtDReportedTime.Text, txtDReportingTime.Text, txtDText.Text);                      

        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtDOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementD;
            lbDEmpName.Text = hdnEmpName.Value;
            string DCategory = title(ddlDCategory, RequiredFieldValidatorDCategory, txtDCategory, RequiredFieldValidatortxtD);
            StatementD = "PAP 2007-04.";
            StatementD = StatementD + Environment.NewLine + "On {0},{1} {2}, a category {3}, reported to the DS Clinic at {4} hours.";
            StatementD = StatementD + Environment.NewLine + "Employee failed to report at {5} hours as required by PAP 2007-04.";
            StatementD = StatementD + Environment.NewLine + "Employee is to be marked late {6} and docked the same by using Payroll Code 3613 in the PMS System and an entry is to be made in the absence and lateness log.";
            Statement = StatementD;
            hdnStatement.Value = string.Format(Statement, txtDOnDate.Text,DCategory, ddlCategory.SelectedItem.Text, lbDEmpName.Text, txtDReportedTime.Text, txtDReportingTime.Text, txtDText.Text);           
        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtDReportedTime_TextChanged(object sender, EventArgs e)
        {
            String StatementD;
            lbDEmpName.Text = hdnEmpName.Value;
            string DCategory = title(ddlDCategory, RequiredFieldValidatorDCategory, txtDCategory, RequiredFieldValidatortxtD);
            StatementD = "PAP 2007-04.";
            StatementD = StatementD + Environment.NewLine + "On {0},{1} {2}, a category {3}, reported to the DS Clinic at {4} hours.";
            StatementD = StatementD + Environment.NewLine + "Employee failed to report at {5} hours as required by PAP 2007-04.";
            StatementD = StatementD + Environment.NewLine + "Employee is to be marked late {6} and docked the same by using Payroll Code 3613 in the PMS System and an entry is to be made in the absence and lateness log.";
            Statement = StatementD;
            hdnStatement.Value = string.Format(Statement, txtDOnDate.Text,DCategory ,lbDEmpName.Text, ddlCategory.SelectedItem.Text, txtDReportedTime.Text, txtDReportingTime.Text, txtDText.Text);           
        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtDReportingTime_TextChanged(object sender, EventArgs e)
        {
            String StatementD;
            lbDEmpName.Text = hdnEmpName.Value;
            string DCategory = title(ddlDCategory, RequiredFieldValidatorDCategory, txtDCategory, RequiredFieldValidatortxtD);
            StatementD = "PAP 2007-04.";
            StatementD = StatementD + Environment.NewLine + "On {0},{1} {2}, a category {3}, reported to the DS Clinic at {4} hours.";
            StatementD = StatementD + Environment.NewLine + "Employee failed to report at {5} hours as required by PAP 2007-04.";
            StatementD = StatementD + Environment.NewLine + "Employee is to be marked late {6} and docked the same by using Payroll Code 3613 in the PMS System and an entry is to be made in the absence and lateness log.";
            Statement = StatementD;
            hdnStatement.Value = string.Format(Statement, txtDOnDate.Text, DCategory, lbDEmpName.Text,ddlCategory.SelectedItem.Text, txtDReportedTime.Text, txtDReportingTime.Text, txtDText.Text);
            
        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtDText_TextChanged(object sender, EventArgs e)
        {
            String StatementD;
            lbDEmpName.Text = hdnEmpName.Value;
            string DCategory = title(ddlDCategory, RequiredFieldValidatorDCategory, txtDCategory, RequiredFieldValidatortxtD);
            StatementD = "PAP 2007-04.";
            StatementD = StatementD + Environment.NewLine + "On {0},{1} {2}, a category {3}, reported to the DS Clinic at {4} hours.";
            StatementD = StatementD + Environment.NewLine + "Employee failed to report at {5} hours as required by PAP 2007-04.";
            StatementD = StatementD + Environment.NewLine + "Employee is to be marked late {6} and docked the same by using Payroll Code 3613 in the PMS System and an entry is to be made in the absence and lateness log.";
            Statement = StatementD;
            hdnStatement.Value = string.Format(Statement, txtDOnDate.Text,DCategory, lbDEmpName.Text, ddlCategory.SelectedItem.Text, txtDReportedTime.Text, txtDReportingTime.Text, txtDText.Text);

         }

        #endregion

        #region Statement E
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementE()
        {
                String StatementE;
                ChangeControlStatus();
                pnTypeE.Visible = true;               
                lbEEmpName.Text = hdnEmpName.Value;               
                string ECategory = title(ddlECategory, RequiredFieldValidatorECategory, txtECategory, RequiredFieldValidatortxtE);
                StatementE = "PAP 2007-04.";
                StatementE = StatementE + Environment.NewLine + "On {0} at {1} hours a sick leave visit was made to  {2} {3}, a category {4} employee, at {5} by {6}, an authorized employee of the ";
                StatementE = StatementE + "Supervise Sick Leave Unit. The employee was not at home and did not call for or receive permission from the Home Visitation Program to be away from home while on medical leave";
                Statement = StatementE;
                hdnStatement.Value = string.Format(Statement, txtEOnDate.Text, txtEReportingTime.Text, ECategory,lbEEmpName.Text, ddlLodiA.SelectedItem.Text, txtEFromLoc.Text, txtEText.Text);

        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementE;
            lbEEmpName.Text = hdnEmpName.Value;
            string ECategory = title(ddlECategory, RequiredFieldValidatorECategory, txtECategory, RequiredFieldValidatortxtE);
            StatementE = "PAP 2007-04.";
            StatementE = StatementE + Environment.NewLine + "On {0} at {1} hours a sick leave visit was made to  {2} {3}, a category {4} employee, at {5} by {6}, an authorized employee of the ";
            StatementE = StatementE + "Supervise Sick Leave Unit. The employee was not at home and did not call for or receive permission from the Home Visitation Program to be away from home while on medical leave";
            Statement = StatementE;
            hdnStatement.Value = string.Format(Statement, txtEOnDate.Text, txtEReportingTime.Text, ECategory,lbEEmpName.Text, ddlLodiA.SelectedItem.Text, txtEFromLoc.Text, txtEText.Text);
           
        }
        /// <summary>
        /// This method is on change text  event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEReportingTime_TextChanged(object sender, EventArgs e)
        {
            String StatementE;
            lbEEmpName.Text = hdnEmpName.Value;
            string ECategory = title(ddlECategory, RequiredFieldValidatorECategory, txtECategory, RequiredFieldValidatortxtE);
            StatementE = "PAP 2007-04.";
            StatementE = StatementE + Environment.NewLine + "On {0} at {1} hours a sick leave visit was made to  {2} {3}, a category {4} employee, at {5} by {6}, an authorized employee of the ";
            StatementE = StatementE + "Supervise Sick Leave Unit. The employee was not at home and did not call for or receive permission from the Home Visitation Program to be away from home while on medical leave";
            Statement = StatementE;
            hdnStatement.Value = string.Format(Statement, txtEOnDate.Text,  txtEReportingTime.Text,ECategory, lbEEmpName.Text, ddlLodiA.SelectedItem.Text, txtEFromLoc.Text, txtEText.Text);
        
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEFromLoc_TextChanged(object sender, EventArgs e)
        {
            String StatementE;
            lbEEmpName.Text = hdnEmpName.Value;
            string ECategory = title(ddlECategory, RequiredFieldValidatorECategory, txtECategory, RequiredFieldValidatortxtE);
            StatementE = "PAP 2007-04.";
            StatementE = StatementE + Environment.NewLine + "On {0} at {1} hours a sick leave visit was made to  {2} {3}, a category {4} employee, at {5} by {6}, an authorized employee of the ";
            StatementE = StatementE + "Supervise Sick Leave Unit. The employee was not at home and did not call for or receive permission from the Home Visitation Program to be away from home while on medical leave";
            Statement = StatementE;
            hdnStatement.Value = string.Format(Statement, txtEOnDate.Text, txtEReportingTime.Text,ECategory, lbEEmpName.Text, ddlLodiA.SelectedItem.Text, txtEFromLoc.Text, txtEText.Text);

        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtEText_TextChanged(object sender, EventArgs e)
        {
            String StatementE;
            lbEEmpName.Text = hdnEmpName.Value;
            string ECategory = title(ddlECategory, RequiredFieldValidatorECategory, txtECategory, RequiredFieldValidatortxtE);
            StatementE = "PAP 2007-04.";
            StatementE = StatementE + Environment.NewLine + "On {0} at {1} hours a sick leave visit was made to  {2} {3}, a category {4} employee, at {5} by {6}, an authorized employee of the ";
            StatementE = StatementE + "Supervise Sick Leave Unit. The employee was not at home and did not call for or receive permission from the Home Visitation Program to be away from home while on medical leave";
            Statement = StatementE;
            hdnStatement.Value = string.Format(Statement, txtEOnDate.Text, txtEReportingTime.Text, ECategory,lbEEmpName.Text, ddlLodiA.SelectedItem.Text, txtEFromLoc.Text, txtEText.Text);
          
        }

        protected void ddlLodiA_SelectedIndexChanged(object sender, EventArgs e)
        {
            String StatementE;
            lbEEmpName.Text = hdnEmpName.Value;
            string ECategory = title(ddlECategory, RequiredFieldValidatorECategory, txtECategory, RequiredFieldValidatortxtE);
            StatementE = "PAP 2007-04.";
            StatementE = StatementE + Environment.NewLine + "On {0} at {1} hours a sick leave visit was made to  {2} {3}, a category {4} employee, at {5} by {6}, an authorized employee of the ";
            StatementE = StatementE + "Supervise Sick Leave Unit. The employee was not at home and did not call for or receive permission from the Home Visitation Program to be away from home while on medical leave";
            Statement = StatementE;
                hdnStatement.Value = string.Format(Statement, txtEOnDate.Text,  txtEReportingTime.Text,ECategory, lbEEmpName.Text, ddlLodiA.SelectedItem.Text, txtEFromLoc.Text, txtEText.Text);
          
        }

        #endregion

        #region Statement F
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementF()
        {

                String StatementF;  
                ChangeControlStatus();
                pnTypeF.Visible = true;
                lbFEmpName.Text = hdnEmpName.Value;               
                string FCategory = title(ddlFCategory, RequiredFieldValidatorFCategory, txtFCategory, RequiredFieldValidatortxtF);
                StatementF = "PAP 2007-04.";
                StatementF = StatementF + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called  {1}  {2}at his/her reported telephone number {3} ";
                StatementF = StatementF + "at various times and left instructions for the employee on his/her answering machine. The employee, on paid medical leave, failed to respond to the telephone call/s and failed to call for or receive permission from the ";
                StatementF = StatementF + "Home Visitation Program to be away from home at the instance of each call.";
                StatementF = StatementF + Environment.NewLine + "As of this date, {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                StatementF = StatementF + Environment.NewLine + "The employee was in category {5} at the time of incident.";
                Statement = StatementF;
                hdnStatement.Value = string.Format(Statement, txtFOnDate.Text, FCategory,lbFEmpName.Text, txtFTelNo.Text, txtFThisDate.Text, ddlCatF.SelectedItem.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtFOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementF;
            string FCategory = title(ddlFCategory, RequiredFieldValidatorFCategory, txtFCategory, RequiredFieldValidatortxtF);
            StatementF = "PAP 2007-04.";
            StatementF = StatementF + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called  {1}  {2}at his/her reported telephone number {3} ";
            StatementF = StatementF + "at various times and left instructions for the employee on his/her answering machine. The employee, on paid medical leave, failed to respond to the telephone call/s and failed to call for or receive permission from the ";
            StatementF = StatementF + "Home Visitation Program to be away from home at the instance of each call.";
            StatementF = StatementF + Environment.NewLine + "As of this date, {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementF = StatementF + Environment.NewLine + "The employee was in category {5} at the time of incident.";
            Statement = StatementF;
            hdnStatement.Value = string.Format(Statement, txtFOnDate.Text, FCategory, lbFEmpName.Text, txtFTelNo.Text, txtFThisDate.Text,ddlCatF.SelectedItem.Text);            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtFTelNo_TextChanged(object sender, EventArgs e)
        {
            String StatementF;
            string FCategory = title(ddlFCategory, RequiredFieldValidatorFCategory, txtFCategory, RequiredFieldValidatortxtF);
            StatementF = "PAP 2007-04.";
            StatementF = StatementF + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called  {1}  {2}at his/her reported telephone number {3} ";
            StatementF = StatementF + "at various times and left instructions for the employee on his/her answering machine. The employee, on paid medical leave, failed to respond to the telephone call/s and failed to call for or receive permission from the ";
            StatementF = StatementF + "Home Visitation Program to be away from home at the instance of each call.";
            StatementF = StatementF + Environment.NewLine + "As of this date, {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementF = StatementF + Environment.NewLine + "The employee was in category {5} at the time of incident.";
            Statement = StatementF;
            hdnStatement.Value = string.Format(Statement, txtFOnDate.Text, FCategory, lbFEmpName.Text, txtFTelNo.Text, txtFThisDate.Text, ddlCatF.SelectedItem.Text);
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtFThisDate_TextChanged(object sender, EventArgs e)
        {
            String StatementF;
            string FCategory = title(ddlFCategory, RequiredFieldValidatorFCategory, txtFCategory, RequiredFieldValidatortxtF);
            StatementF = "PAP 2007-04.";
            StatementF = StatementF + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called  {1}  {2}at his/her reported telephone number {3} ";
            StatementF = StatementF + "at various times and left instructions for the employee on his/her answering machine. The employee, on paid medical leave, failed to respond to the telephone call/s and failed to call for or receive permission from the ";
            StatementF = StatementF + "Home Visitation Program to be away from home at the instance of each call.";
            StatementF = StatementF + Environment.NewLine + "As of this date, {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementF = StatementF + Environment.NewLine + "The employee was in category {5} at the time of incident.";
            Statement = StatementF;
            hdnStatement.Value = string.Format(Statement, txtFOnDate.Text, FCategory, lbFEmpName.Text, txtFTelNo.Text, txtFThisDate.Text, ddlCatF.SelectedItem.Text);

        }

        protected void ddlCatF_SelectedIndexChanged(object sender, EventArgs e)
        {
            String StatementF;
            string FCategory = title(ddlFCategory, RequiredFieldValidatorFCategory, txtFCategory, RequiredFieldValidatortxtF);
            StatementF = "PAP 2007-04.";
            StatementF = StatementF + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called  {1}  {2}at his/her reported telephone number {3} ";
            StatementF = StatementF + "at various times and left instructions for the employee on his/her answering machine. The employee, on paid medical leave, failed to respond to the telephone call/s and failed to call for or receive permission from the ";
            StatementF = StatementF + "Home Visitation Program to be away from home at the instance of each call.";
            StatementF = StatementF + Environment.NewLine + "As of this date, {4}, no / inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementF = StatementF + Environment.NewLine + "The employee was in category {5} at the time of incident.";
            Statement = StatementF;
            hdnStatement.Value = string.Format(Statement, txtFOnDate.Text, FCategory, lbFEmpName.Text, txtFTelNo.Text, txtFThisDate.Text, ddlCatF.SelectedItem.Text);
        }

        #endregion

        #region Statement G
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementG()
        {
                String StatementG;          
                ChangeControlStatus();
                pnTypeG.Visible = true;
                string GCategory = title(ddlGCategory, RequiredFieldValidatorGCategory, txtGCategory, RequiredFieldValidatortxtG);
                lbGEmpName.Text = hdnEmpName.Value;
                if (ddlEmpCategory.SelectedIndex == 0)
                    ddlEmpCategory.SelectedItem.Text = "";
                StatementG = "PAP 2007-04.";
                StatementG = StatementG + Environment.NewLine + "On {0}  {1} {2}reported sick/lodi. The employee found not at home on a phone visit/home visit had called for authorization on {3} to leave home and was given code {4} for {5}. ";
                StatementG = StatementG + Environment.NewLine + "As of this date {6} no/inadequate medical documentation has been received by the Medical Division for the above date of incident.";
                StatementG = StatementG + Environment.NewLine + "The employee was in category {7} at the time of incident.";
                Statement = StatementG;
                hdnStatement.Value = string.Format(Statement, txtGOnDate.Text,GCategory, lbGEmpName.Text, txtGReportedDate.Text, txtGText.Text, ddlEmpCategory.SelectedItem.Text, txtGThisDate.Text, ddlCatG.SelectedItem.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtGOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementG; 
            string GCategory = title(ddlGCategory, RequiredFieldValidatorGCategory, txtGCategory, RequiredFieldValidatortxtG);
            lbGEmpName.Text = hdnEmpName.Value;
            if (ddlEmpCategory.SelectedIndex == 0)
                ddlEmpCategory.SelectedItem.Text = "";
            StatementG = "PAP 2007-04.";
            StatementG = StatementG + Environment.NewLine + "On {0}  {1} {2}reported sick/lodi. The employee found not at home on a phone visit/home visit had called for authorization on {3} to leave home and was given code {4} for {5}. ";
            StatementG = StatementG + Environment.NewLine + "As of this date {6} no/inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementG = StatementG + Environment.NewLine + "The employee was in category {7} at the time of incident.";
            Statement = StatementG;
                hdnStatement.Value = string.Format(Statement, txtGOnDate.Text,GCategory, lbGEmpName.Text, txtGReportedDate.Text, txtGText.Text, ddlEmpCategory.SelectedItem.Text, txtGThisDate.Text, ddlCatG.SelectedItem.Text);            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtGReportedDate_TextChanged(object sender, EventArgs e)
        {
            String StatementG;
            string GCategory = title(ddlGCategory, RequiredFieldValidatorGCategory, txtGCategory, RequiredFieldValidatortxtG);
            lbGEmpName.Text = hdnEmpName.Value;
            if (ddlEmpCategory.SelectedIndex == 0)
                ddlEmpCategory.SelectedItem.Text = "";
            StatementG = "PAP 2007-04.";
            StatementG = StatementG + Environment.NewLine + "On {0}  {1} {2}reported sick/lodi. The employee found not at home on a phone visit/home visit had called for authorization on {3} to leave home and was given code {4} for {5}. ";
            StatementG = StatementG + Environment.NewLine + "As of this date {6} no/inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementG = StatementG + Environment.NewLine + "The employee was in category {7} at the time of incident.";
            Statement = StatementG;
            hdnStatement.Value = string.Format(Statement, txtGOnDate.Text, GCategory,lbGEmpName.Text, txtGReportedDate.Text, txtGText.Text, ddlEmpCategory.SelectedItem.Text, txtGThisDate.Text, ddlCatG.SelectedItem.Text);           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtGText_TextChanged(object sender, EventArgs e)
        {
            String StatementG;
            string GCategory = title(ddlGCategory, RequiredFieldValidatorGCategory, txtGCategory, RequiredFieldValidatortxtG);
            lbGEmpName.Text = hdnEmpName.Value;
            if (ddlEmpCategory.SelectedIndex == 0)
                ddlEmpCategory.SelectedItem.Text = "";
            StatementG = "PAP 2007-04.";
            StatementG = StatementG + Environment.NewLine + "On {0}  {1} {2}reported sick/lodi. The employee found not at home on a phone visit/home visit had called for authorization on {3} to leave home and was given code {4} for {5}. ";
            StatementG = StatementG + Environment.NewLine + "As of this date {6} no/inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementG = StatementG + Environment.NewLine + "The employee was in category {7} at the time of incident.";
            Statement = StatementG;
            hdnStatement.Value = string.Format(Statement, txtGOnDate.Text,GCategory, lbGEmpName.Text, txtGReportedDate.Text, txtGText.Text, ddlEmpCategory.SelectedItem.Text, txtGThisDate.Text, ddlCatG.SelectedItem.Text);
            
        }
        protected void txtGThisDate_TextChanged(object sender, EventArgs e)
        {
            String StatementG;
            string GCategory = title(ddlGCategory, RequiredFieldValidatorGCategory, txtGCategory, RequiredFieldValidatortxtG);
            lbGEmpName.Text = hdnEmpName.Value;
            if (ddlEmpCategory.SelectedIndex == 0)
                ddlEmpCategory.SelectedItem.Text = "";
            StatementG = "PAP 2007-04.";
            StatementG = StatementG + Environment.NewLine + "On {0}  {1} {2}reported sick/lodi. The employee found not at home on a phone visit/home visit had called for authorization on {3} to leave home and was given code {4} for {5}. ";
            StatementG = StatementG + Environment.NewLine + "As of this date {6} no/inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementG = StatementG + Environment.NewLine + "The employee was in category {7} at the time of incident.";
            Statement = StatementG;
                hdnStatement.Value = string.Format(Statement, txtGOnDate.Text,GCategory, lbGEmpName.Text, txtGReportedDate.Text, txtGText.Text, ddlEmpCategory.SelectedItem.Text, txtGThisDate.Text, ddlCatG.SelectedItem.Text);
                      
        }
        protected void ddlCatG_SelectedIndexChanged(object sender, EventArgs e)
        {
            String StatementG;
            string GCategory = title(ddlGCategory, RequiredFieldValidatorGCategory, txtGCategory, RequiredFieldValidatortxtG);
            lbGEmpName.Text = hdnEmpName.Value;
            if (ddlEmpCategory.SelectedIndex == 0)
                ddlEmpCategory.SelectedItem.Text = "";
            StatementG = "PAP 2007-04.";
            StatementG = StatementG + Environment.NewLine + "On {0}  {1} {2}reported sick/lodi. The employee found not at home on a phone visit/home visit had called for authorization on {3} to leave home and was given code {4} for {5}. ";
            StatementG = StatementG + Environment.NewLine + "As of this date {6} no/inadequate medical documentation has been received by the Medical Division for the above date of incident.";
            StatementG = StatementG + Environment.NewLine + "The employee was in category {7} at the time of incident.";
            Statement = StatementG;
            hdnStatement.Value = string.Format(Statement, txtGOnDate.Text, GCategory,lbGEmpName.Text, txtGReportedDate.Text, txtGText.Text, ddlEmpCategory.SelectedItem.Text, txtGThisDate.Text, ddlCatG.SelectedItem.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       

        #endregion

        #region Statement H
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementH()
        {
                String StatementH;           
                ChangeControlStatus();
                pnTypeH.Visible = true;
                lbHEmpName.Text = hdnEmpName.Value;
                string HCategory = title(ddlHCategory, RequiredFieldValidatorHCategory, txtHCategory, RequiredFieldValidatortxtH);
                StatementH = "PAP 2007-04.";
                StatementH = StatementH + Environment.NewLine + "On {0}, {1} {2} reported sick/LODI, began medical leave, and currently does not have a home telephone. ";
                StatementH = StatementH + "An employee that does not have a home telephone must call the DS Clinic to report a medical leave absence on the first day of medical leave and must call the DS Clinic ";
                StatementH = StatementH + "each day they are on medical leave, providing an update of their medical condition, until their first visit to the DS Clinic.";
                StatementH = StatementH + Environment.NewLine + "Employee failed to call as required on {3}.";
                Statement = StatementH;
                hdnStatement.Value = string.Format(Statement, txtHOnDate.Text,HCategory, lbHEmpName.Text, txtHReportedDate.Text);
          
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtHOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementH;  
            lbHEmpName.Text = hdnEmpName.Value;
            string HCategory = title(ddlHCategory, RequiredFieldValidatorHCategory, txtHCategory, RequiredFieldValidatortxtH);
            StatementH = "PAP 2007-04.";
            StatementH = StatementH + Environment.NewLine + "On {0}, {1} {2} reported sick/LODI, began medical leave, and currently does not have a home telephone. ";
            StatementH = StatementH + "An employee that does not have a home telephone must call the DS Clinic to report a medical leave absence on the first day of medical leave and must call the DS Clinic ";
            StatementH = StatementH + "each day they are on medical leave, providing an update of their medical condition, until their first visit to the DS Clinic.";
            StatementH = StatementH + Environment.NewLine + "Employee failed to call as required on {3}.";
            Statement = StatementH;
            hdnStatement.Value = string.Format(Statement, txtHOnDate.Text, HCategory,lbHEmpName.Text, txtHReportedDate.Text);           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtHReportedDate_TextChanged(object sender, EventArgs e)
        {
            String StatementH;
            lbHEmpName.Text = hdnEmpName.Value;
            string HCategory = title(ddlHCategory, RequiredFieldValidatorHCategory, txtHCategory, RequiredFieldValidatortxtH);
            StatementH = "PAP 2007-04.";
            StatementH = StatementH + Environment.NewLine + "On {0}, {1} {2} reported sick/LODI, began medical leave, and currently does not have a home telephone. ";
            StatementH = StatementH + "An employee that does not have a home telephone must call the DS Clinic to report a medical leave absence on the first day of medical leave and must call the DS Clinic ";
            StatementH = StatementH + "each day they are on medical leave, providing an update of their medical condition, until their first visit to the DS Clinic.";
            StatementH = StatementH + Environment.NewLine + "Employee failed to call as required on {3}.";
            Statement = StatementH;
                hdnStatement.Value = string.Format(Statement, txtHOnDate.Text,HCategory, lbHEmpName.Text, txtHReportedDate.Text);            
        }

        #endregion

        #region Statement I
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementI()
        {
                String StatementI;            
                ChangeControlStatus();
                pnTypeI.Visible = true;
                lbIEmpName.Text = hdnEmpName.Value;
                string ICategory = title(ddlICategory, RequiredFieldValidatorICategory, txtICategory, RequiredFieldValidatortxtI);
                StatementI = "PAP 2007-04.";
                StatementI = StatementI + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called {1} {2} at his/her reported telephone number {3} at various times. ";
                StatementI = StatementI + Environment.NewLine + " A message stating \"This telephone number is not in service or is disconnected\" was received. Employee must notify their Supervisor of any change of their telephone number.";
                StatementI = StatementI + "While on paid leave, an employee shall remain accessible and available for telephone sick leave calls conducted by Supervised Sick Leave Unit personnel.";
                StatementI = StatementI + Environment.NewLine + "The employee was in category {4} at the time of incident.";
                Statement = StatementI;
                hdnStatement.Value = string.Format(Statement, txtIOnDate.Text, ICategory,lbIEmpName.Text, txtITelNo.Text, ddlCatI.SelectedItem.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtIOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementI;  
            lbIEmpName.Text = hdnEmpName.Value;
            string ICategory = title(ddlICategory, RequiredFieldValidatorICategory, txtICategory, RequiredFieldValidatortxtI);
            StatementI = "PAP 2007-04.";
            StatementI = StatementI + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called {1} {2} at his/her reported telephone number {3} at various times. ";
            StatementI = StatementI + Environment.NewLine + " A message stating \"This telephone number is not in service or is disconnected\" was received. Employee must notify their Supervisor of any change of their telephone number.";
            StatementI = StatementI + "While on paid leave, an employee shall remain accessible and available for telephone sick leave calls conducted by Supervised Sick Leave Unit personnel.";
            StatementI = StatementI + Environment.NewLine + "The employee was in category {4} at the time of incident.";
            Statement = StatementI;
            hdnStatement.Value = string.Format(Statement, txtIOnDate.Text, ICategory,lbIEmpName.Text, txtITelNo.Text, ddlCatI.SelectedItem.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtITelNo_TextChanged(object sender, EventArgs e)
        {
            String StatementI;
            lbIEmpName.Text = hdnEmpName.Value;
            string ICategory = title(ddlICategory, RequiredFieldValidatorICategory, txtICategory, RequiredFieldValidatortxtI);
            StatementI = "PAP 2007-04.";
            StatementI = StatementI + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called {1} {2} at his/her reported telephone number {3} at various times. ";
            StatementI = StatementI + Environment.NewLine + " A message stating \"This telephone number is not in service or is disconnected\" was received. Employee must notify their Supervisor of any change of their telephone number.";
            StatementI = StatementI + "While on paid leave, an employee shall remain accessible and available for telephone sick leave calls conducted by Supervised Sick Leave Unit personnel.";
            StatementI = StatementI + Environment.NewLine + "The employee was in category {4} at the time of incident.";
            Statement = StatementI;
            hdnStatement.Value = string.Format(Statement, txtIOnDate.Text, ICategory,lbIEmpName.Text, txtITelNo.Text, ddlCatI.SelectedItem.Text);
        }

        protected void ddlCatI_SelectedIndexChanged(object sender, EventArgs e)
        {
            String StatementI;
            lbIEmpName.Text = hdnEmpName.Value;
            string ICategory = title(ddlICategory, RequiredFieldValidatorICategory, txtICategory, RequiredFieldValidatortxtI);
            StatementI = "PAP 2007-04.";
            StatementI = StatementI + Environment.NewLine + "On {0} authorized personnel from the Supervised Sick Leave Unit called {1} {2} at his/her reported telephone number {3} at various times. ";
            StatementI = StatementI + Environment.NewLine + " A message stating \"This telephone number is not in service or is disconnected\" was received. Employee must notify their Supervisor of any change of their telephone number.";
            StatementI = StatementI + "While on paid leave, an employee shall remain accessible and available for telephone sick leave calls conducted by Supervised Sick Leave Unit personnel.";
            StatementI = StatementI + Environment.NewLine + "The employee was in category {4} at the time of incident.";
            Statement = StatementI;
            hdnStatement.Value = string.Format(Statement, txtIOnDate.Text,ICategory, lbIEmpName.Text, txtITelNo.Text, ddlCatI.SelectedItem.Text);            
        }

        #endregion

        #region Statement J
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementJ()
        {
                String StatementJ1;           
                ChangeControlStatus();
                pnTypeJ.Visible = true;
                lbJ1EmpName.Text = hdnEmpName.Value;
                lbJ2EmpName.Text = hdnEmpName.Value;
                string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
                StatementJ1 = "On {0},  {1} {2} reported to (the) {3} as ordered. A urine sample for toxicology testing was taken. The results were positive for {4}";
                StatementJ1 = StatementJ1 + Environment.NewLine;
                StatementJ1 = StatementJ1 + Environment.NewLine;
                StatementJ1 = StatementJ1 + "On {5},  {1} {2} reported to (the) {6} as ordered. A breathalyzer test was conducted under the supervision of {7}";
                StatementJ1 = StatementJ1 + Environment.NewLine;
                StatementJ1 = StatementJ1 + "The result was a positive BAC of {8}";
                Statement = StatementJ1;
                hdnStatement.Value = string.Format(Statement, txtJ1OnDate.Text,J1Category, lbJ1EmpName.Text, txtJ1FromLoc.Text, txtJ1Text.Text, txtJ2OnDate.Text, txtJ2FromLoc.Text, txtJ2Super.Text, txtJ2Text.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJ2Text_TextChanged(object sender, EventArgs e)
        {
            String StatementJ1;  
            lbJ1EmpName.Text = hdnEmpName.Value;
            lbJ2EmpName.Text = hdnEmpName.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            StatementJ1 = "On {0},  {1} {2} reported to (the) {3} as ordered. A urine sample for toxicology testing was taken. The results were positive for {4}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "On {5},  {1} {2} reported to (the) {6} as ordered. A breathalyzer test was conducted under the supervision of {7}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "The result was a positive BAC of {8}";
            Statement = StatementJ1;
            hdnStatement.Value = string.Format(Statement, txtJ1OnDate.Text, J1Category,lbJ1EmpName.Text, txtJ1FromLoc.Text, txtJ1Text.Text, txtJ2OnDate.Text, txtJ2FromLoc.Text, txtJ2Super.Text, txtJ2Text.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJ1OnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementJ1;
            lbJ1EmpName.Text = hdnEmpName.Value;
            lbJ2EmpName.Text = hdnEmpName.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            StatementJ1 = "On {0},  {1} {2} reported to (the) {3} as ordered. A urine sample for toxicology testing was taken. The results were positive for {4}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "On {5},  {1} {2} reported to (the) {6} as ordered. A breathalyzer test was conducted under the supervision of {7}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "The result was a positive BAC of {8}";
            Statement = StatementJ1;

                hdnStatement.Value = string.Format(Statement, txtJ1OnDate.Text,J1Category, lbJ1EmpName.Text, txtJ1FromLoc.Text, txtJ1Text.Text, txtJ2OnDate.Text, txtJ2FromLoc.Text, txtJ2Super.Text, txtJ2Text.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJ1FromLoc_TextChanged(object sender, EventArgs e)
        {
            String StatementJ1;
            lbJ1EmpName.Text = hdnEmpName.Value;
            lbJ2EmpName.Text = hdnEmpName.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            StatementJ1 = "On {0},  {1} {2} reported to (the) {3} as ordered. A urine sample for toxicology testing was taken. The results were positive for {4}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "On {5},  {1} {2} reported to (the) {6} as ordered. A breathalyzer test was conducted under the supervision of {7}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "The result was a positive BAC of {8}";
            Statement = StatementJ1;
            hdnStatement.Value = string.Format(Statement, txtJ1OnDate.Text,J1Category, lbJ1EmpName.Text, txtJ1FromLoc.Text, txtJ1Text.Text, txtJ2OnDate.Text, txtJ2FromLoc.Text, txtJ2Super.Text, txtJ2Text.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJ1Text_TextChanged(object sender, EventArgs e)
        {
            String StatementJ1;
            lbJ1EmpName.Text = hdnEmpName.Value;
            lbJ2EmpName.Text = hdnEmpName.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            StatementJ1 = "On {0},  {1} {2} reported to (the) {3} as ordered. A urine sample for toxicology testing was taken. The results were positive for {4}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "On {5},  {1} {2} reported to (the) {6} as ordered. A breathalyzer test was conducted under the supervision of {7}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "The result was a positive BAC of {8}";
            Statement = StatementJ1;
               hdnStatement.Value = string.Format(Statement, txtJ1OnDate.Text,J1Category, lbJ1EmpName.Text, txtJ1FromLoc.Text, txtJ1Text.Text, txtJ2OnDate.Text, txtJ2FromLoc.Text, txtJ2Super.Text, txtJ2Text.Text);
       
        }        
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJ2OnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementJ1;
            lbJ1EmpName.Text = hdnEmpName.Value;
            lbJ2EmpName.Text = hdnEmpName.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            StatementJ1 = "On {0},  {1} {2} reported to (the) {3} as ordered. A urine sample for toxicology testing was taken. The results were positive for {4}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "On {5},  {1} {2} reported to (the) {6} as ordered. A breathalyzer test was conducted under the supervision of {7}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "The result was a positive BAC of {8}";
            Statement = StatementJ1;
            hdnStatement.Value = string.Format(Statement, txtJ1OnDate.Text,J1Category, lbJ1EmpName.Text, txtJ1FromLoc.Text, txtJ1Text.Text, txtJ2OnDate.Text, txtJ2FromLoc.Text, txtJ2Super.Text, txtJ2Text.Text);

        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJ2FromLoc_TextChanged(object sender, EventArgs e)
        {
            String StatementJ1;
            lbJ1EmpName.Text = hdnEmpName.Value;
            lbJ2EmpName.Text = hdnEmpName.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            StatementJ1 = "On {0},  {1} {2} reported to (the) {3} as ordered. A urine sample for toxicology testing was taken. The results were positive for {4}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "On {5},  {1} {2} reported to (the) {6} as ordered. A breathalyzer test was conducted under the supervision of {7}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "The result was a positive BAC of {8}";
            Statement = StatementJ1;
            hdnStatement.Value = string.Format(Statement, txtJ1OnDate.Text, J1Category,lbJ1EmpName.Text, txtJ1FromLoc.Text, txtJ1Text.Text, txtJ2OnDate.Text, txtJ2FromLoc.Text, txtJ2Super.Text, txtJ2Text.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJ2Super_TextChanged(object sender, EventArgs e)
        {
            String StatementJ1;
            lbJ1EmpName.Text = hdnEmpName.Value;
            lbJ2EmpName.Text = hdnEmpName.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            StatementJ1 = "On {0},  {1} {2} reported to (the) {3} as ordered. A urine sample for toxicology testing was taken. The results were positive for {4}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "On {5},  {1} {2} reported to (the) {6} as ordered. A breathalyzer test was conducted under the supervision of {7}";
            StatementJ1 = StatementJ1 + Environment.NewLine;
            StatementJ1 = StatementJ1 + "The result was a positive BAC of {8}";
            Statement = StatementJ1;
            hdnStatement.Value = string.Format(Statement, txtJ1OnDate.Text,J1Category, lbJ1EmpName.Text, txtJ1FromLoc.Text, txtJ1Text.Text, txtJ2OnDate.Text, txtJ2FromLoc.Text, txtJ2Super.Text, txtJ2Text.Text);           
        }

        #endregion

        #region Statement K
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementK()
        {
                String StatementK;           
                ChangeControlStatus();
                pnTypeK.Visible = true;
                lbAEmpName.Text = hdnEmpName.Value;
                StatementK = "The Safety Unit has determined you were negligent on {0} at approximately {1} hours.  While operating Vehicle code # {2}  you were involved in a collision at {3}";
                StatementK = StatementK + " that resulted in property damage and / or personal injury. This is your chargeable accident in a 12 month period.";
                Statement = StatementK;
                hdnStatement.Value = string.Format(Statement, txtKOnDate.Text, txtKHours.Text, txtKVehicle.Text, txtKLocation.Text);          

        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtKOnDate_TextChanged(object sender, EventArgs e)
        {            
                hdnStatement.Value = string.Format(Statement, txtKOnDate.Text, txtKHours.Text, txtKVehicle.Text, txtKLocation.Text);
            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtKHours_TextChanged(object sender, EventArgs e)
        {
           hdnStatement.Value = string.Format(Statement, txtKOnDate.Text, txtKHours.Text, txtKVehicle.Text, txtKLocation.Text);
            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtKVehicle_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtKOnDate.Text, txtKHours.Text, txtKVehicle.Text, txtKLocation.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtKLocation_TextChanged(object sender, EventArgs e)
        {
              hdnStatement.Value = string.Format(Statement, txtKOnDate.Text, txtKHours.Text, txtKVehicle.Text, txtKLocation.Text);
            
        }

        #endregion

        #region Statement L
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementL()
        {
                String StatementL;          
                ChangeControlStatus();
                pnTypeL.Visible = true;
                lbAEmpName.Text = hdnEmpName.Value;
                StatementL = "On {0}  the New York State Department of Motor Vehicles revoked / suspended your Commercial Driver’s License, Order No. {1}";
                StatementL = StatementL + " for DMV violation. You have not had in your possession a valid Commercial Driver’s License for over 10 days.";
                Statement = StatementL;
                hdnStatement.Value = string.Format(Statement, txtLOnDate.Text, txtLOrderNo.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtLOnDate_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtLOnDate.Text, txtLOrderNo.Text);
            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtLOrderNo_TextChanged(object sender, EventArgs e)
        {
           
                hdnStatement.Value = string.Format(Statement, txtLOnDate.Text, txtLOrderNo.Text);
           
        }

        #endregion
        
        #region Statement M
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementM()
        {
                String StatementM;          
                ChangeControlStatus();
                pnTypeM.Visible = true;
                lbAEmpName.Text = hdnEmpName.Value;
                StatementM = "The Safety Unit has determined you were negligent on at {0} approximately {1} hours.  While guiding Vehicle code # {2}";
                StatementM = StatementM + " in a backwards direction you were involved in a collision at {3} that resulted in property damage and / or personal injury.";
                Statement = StatementM;
                hdnStatement.Value = string.Format(Statement, txtMOnDate.Text, txtMHours.Text, txtMVehicle.Text, txtMLocation.Text);
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtMHours_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtMOnDate.Text, txtMHours.Text, txtMVehicle.Text, txtMLocation.Text);
          
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtMOnDate_TextChanged(object sender, EventArgs e)
        {
            hdnStatement.Value = string.Format(Statement, txtMOnDate.Text, txtMHours.Text, txtMVehicle.Text, txtMLocation.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtMVehicle_TextChanged(object sender, EventArgs e)
        {
            
                hdnStatement.Value = string.Format(Statement, txtMOnDate.Text, txtMHours.Text, txtMVehicle.Text, txtMLocation.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtMLocation_TextChanged(object sender, EventArgs e)
        {          
                hdnStatement.Value = string.Format(Statement, txtMOnDate.Text, txtMHours.Text, txtMVehicle.Text, txtMLocation.Text);
           
        }

        #endregion

        #region Statement N
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementN()
        {
                String StatementN;           
                ChangeControlStatus();
                pnTypeN.Visible = true;
                lbAEmpName.Text = hdnEmpName.Value;
                StatementN = "On {0} at approximately {1} hours. while operating vehicle code {2} you were observed by Safety Officer {3}";
                StatementN = StatementN + " pass a steady red signal at {4}. I had a clear and unobstructed view of this violation. The signal was in proper working order.";
                Statement = StatementN;
                hdnStatement.Value = string.Format(Statement, txtNOnDate.Text, txtNHours.Text, txtNVehicle.Text, txtNOfficer.Text, txtNLocation.Text);
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtNOnDate_TextChanged(object sender, EventArgs e)
        {            
                hdnStatement.Value = string.Format(Statement, txtNOnDate.Text, txtNHours.Text, txtNVehicle.Text, txtNOfficer.Text, txtNLocation.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtNHours_TextChanged(object sender, EventArgs e)
        {          
                hdnStatement.Value = string.Format(Statement, txtNOnDate.Text, txtNHours.Text, txtNVehicle.Text, txtNOfficer.Text, txtNLocation.Text);
         }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtNVehicle_TextChanged(object sender, EventArgs e)
        {
            hdnStatement.Value = string.Format(Statement, txtNOnDate.Text, txtNHours.Text, txtNVehicle.Text, txtNOfficer.Text, txtNLocation.Text);
          
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtNOfficer_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtNOnDate.Text, txtNHours.Text, txtNVehicle.Text, txtNOfficer.Text, txtNLocation.Text);            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtNLocation_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtNOnDate.Text, txtNHours.Text, txtNVehicle.Text, txtNOfficer.Text, txtNLocation.Text);           
        }

        #endregion

        #region Statement O
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>
        private void PrepareStatementO()
        {
                String StatementO;           
                ChangeControlStatus();
                pnTypeO.Visible = true;
                lbAEmpName.Text = hdnEmpName.Value;
                StatementO = "On {0} at approximately hours {1} hours. you were observed by {2} operating / occupying Vehicle Code No. {3}";
                StatementO = StatementO + " at {4}. You were not wearing a seat belt at the time. This is a direct violation of the above orders and rules.";
                Statement = StatementO;
                hdnStatement.Value = string.Format(Statement, txtOOnDate.Text, txtOHours.Text, txtOSuper.Text, txtOVehicle.Text, txtOLocation.Text);        

        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtOOnDate_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtOOnDate.Text, txtOHours.Text, txtOSuper.Text, txtOVehicle.Text, txtOLocation.Text);
            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtOHours_TextChanged(object sender, EventArgs e)
        {          
                hdnStatement.Value = string.Format(Statement, txtOOnDate.Text, txtOHours.Text, txtOSuper.Text, txtOVehicle.Text, txtOLocation.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtOSuper_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtOOnDate.Text, txtOHours.Text, txtOSuper.Text, txtOVehicle.Text, txtOLocation.Text);            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtOVehicle_TextChanged(object sender, EventArgs e)
        {          
                hdnStatement.Value = string.Format(Statement, txtOOnDate.Text, txtOHours.Text, txtOSuper.Text, txtOVehicle.Text, txtOLocation.Text);
           
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtOLocation_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtOOnDate.Text, txtOHours.Text, txtOSuper.Text, txtOVehicle.Text, txtOLocation.Text);
            
        }

        #endregion

        #region Statement P
        /// <summary>
        /// This method prepares the statement by collecting the hours worked, date, vehicle an location
        /// </summary>
        private void PrepareStatementP()
        {
                String StatementP;           
                ChangeControlStatus();
                pnTypeP.Visible = true;
                string P1Category = title(ddlP1Category, RequiredFieldValidatorP1Category, txtP1Category, RequiredFieldValidatortxtP1);
                lbPEmp.Text = hdnEmpName.Value;
                StatementP = "On {0}, {1}, {2} {3} was absent for his/her assigned shift, he did not call and had no authorization to be off. ";
                StatementP = StatementP + "{2} has been marked absent and docked 8 hours pay.";
                StatementP = StatementP + Environment.NewLine + "This incident occurred {4}, {5}.";
                Statement = StatementP;
                hdnStatement.Value = string.Format(Statement, txtPWeek.Text, txtPOnDate.Text,P1Category , lbPEmp.Text, txtPWhen.Text, txtPReason.Text);         

        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPWeek_TextChanged(object sender, EventArgs e)
        {
            String StatementP;  
            string P1Category = title(ddlP1Category, RequiredFieldValidatorP1Category, txtP1Category, RequiredFieldValidatortxtP1);
            lbPEmp.Text = hdnEmpName.Value;
            StatementP = "On {0}, {1}, {2} {3} was absent for his/her assigned shift, he did not call and had no authorization to be off. ";
            StatementP = StatementP + "{2} has been marked absent and docked 8 hours pay.";
            StatementP = StatementP + Environment.NewLine + "This incident occurred {4}, {5}.";
            Statement = StatementP;
                hdnStatement.Value = string.Format(Statement, txtPWeek.Text, txtPOnDate.Text,P1Category, lbPEmp.Text, txtPWhen.Text, txtPReason.Text);         
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPWhen_TextChanged(object sender, EventArgs e)
        {
            String StatementP;
            string P1Category = title(ddlP1Category, RequiredFieldValidatorP1Category, txtP1Category, RequiredFieldValidatortxtP1);
            lbPEmp.Text = hdnEmpName.Value;
            StatementP = "On {0}, {1}, {2} {3} was absent for his/her assigned shift, he did not call and had no authorization to be off. ";
            StatementP = StatementP + "{2} has been marked absent and docked 8 hours pay.";
            StatementP = StatementP + Environment.NewLine + "This incident occurred {4}, {5}.";
            Statement = StatementP;
             hdnStatement.Value = string.Format(Statement, txtPWeek.Text, txtPOnDate.Text,P1Category, lbPEmp.Text, txtPWhen.Text, txtPReason.Text);            
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPReason_TextChanged(object sender, EventArgs e)
        {
            String StatementP;
            string P1Category = title(ddlP1Category, RequiredFieldValidatorP1Category, txtP1Category, RequiredFieldValidatortxtP1);
            lbPEmp.Text = hdnEmpName.Value;
            StatementP = "On {0}, {1}, {2} {3} was absent for his/her assigned shift, he did not call and had no authorization to be off. ";
            StatementP = StatementP + "{2} has been marked absent and docked 8 hours pay.";
            StatementP = StatementP + Environment.NewLine + "This incident occurred {4}, {5}.";
            Statement = StatementP;
            hdnStatement.Value = string.Format(Statement, txtPWeek.Text, txtPOnDate.Text, P1Category, lbPEmp.Text, txtPWhen.Text, txtPReason.Text);
          
        }
        /// <summary>
        /// This method is on change text event that assigns the value to a hiden control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtPOnDate_TextChanged(object sender, EventArgs e)
        {
            String StatementP;
            string P1Category = title(ddlP1Category, RequiredFieldValidatorP1Category, txtP1Category, RequiredFieldValidatortxtP1);
            lbPEmp.Text = hdnEmpName.Value;
            StatementP = "On {0}, {1}, {2} {3} was absent for his/her assigned shift, he did not call and had no authorization to be off. ";
            StatementP = StatementP + "{2} has been marked absent and docked 8 hours pay.";
            StatementP = StatementP + Environment.NewLine + "This incident occurred {4}, {5}.";
            Statement = StatementP;
            hdnStatement.Value = string.Format(Statement, txtPWeek.Text, txtPOnDate.Text,P1Category, lbPEmp.Text, txtPWhen.Text, txtPReason.Text);
            
        }
        #endregion

        #region Statement Q
        /// <summary>
        /// This method prepares the statement by collecting the hours worked, date, vehicle an location
        /// </summary>
        
        
 
        private void PrepareStatementQ()
        {
            String StatementQ;           
                ChangeControlStatus();
                pnTypeQ.Visible = true;
                lblQemp1.Text = lblQemp2.Text = lblQemp3.Text = hdnEmpName.Value;
                lblQemp4.Text = hdnEmpName.Value + "'s";
                 string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                 string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                 string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                 string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                 StatementQ = "On {0}, {1} {2} was assigned to {3} on the {4} to {5} shift. {6} {7} was a no call, no show. ";
                StatementQ = StatementQ + "As a result {8} {9} was marked AWOL and docked 8 hours pay. ";
                StatementQ = StatementQ + "This {10} occur {11} a scheduled day off.";
                StatementQ = StatementQ + " This is {12} {13} {14} incident in a 12 month period.";
                Statement = StatementQ;
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }

        protected void ddlCatQWeek_SelectedIndexChanged(object sender, EventArgs e)
        {
             if (hdnstrTitleDesc.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }
        protected void txtQOnDate_TextChanged(object sender, EventArgs e)
        {            
                if (hdnstrTitleDesc.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
            
        }
        protected void ddlCatQ1_SelectedIndexChanged(object sender, EventArgs e)
        {           
                hdnintTitle.Value = Convert.ToString(ddlCatQ1.SelectedIndex);
                ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
            
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);                
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);           
        }

        protected void ddlACategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlACategory.SelectedIndex);
            ddlACategory.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtACategory.Text =  hdnstrTitleDesc.Value;
            string ACategory = title(ddlACategory, RequiredFieldValidatorACategory, txtACategory, RequiredFieldValidatortxtA);                   
        }

        protected void ddlBCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlBCategory.SelectedIndex);
            ddlBCategory.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtBCategory.Text =  hdnstrTitleDesc.Value;
            string BCategory = title(ddlBCategory, RequiredFieldValidatorBCategory, txtBCategory, RequiredFieldValidatortxtB);                      
        }

        protected void ddlCCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlCCategory.SelectedIndex);
            ddlCCategory.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtCCategory.Text = hdnstrTitleDesc.Value;
            string CCategory = title(ddlCCategory, RequiredFieldValidatorCCategory, txtCCategory, RequiredFieldValidatortxtC);
        }
        protected void ddlDCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlDCategory.SelectedIndex);
            ddlDCategory.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtDCategory.Text = hdnstrTitleDesc.Value;
            string DCategory = title(ddlDCategory, RequiredFieldValidatorDCategory, txtDCategory, RequiredFieldValidatortxtD);
        }
        protected void ddlECategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlECategory.SelectedIndex);
            ddlECategory.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtECategory.Text = hdnstrTitleDesc.Value;
            string ECategory = title(ddlECategory, RequiredFieldValidatorECategory, txtECategory, RequiredFieldValidatortxtE);
        }
        protected void ddlFCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlFCategory.SelectedIndex);
            ddlFCategory.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtFCategory.Text = hdnstrTitleDesc.Value;
            string FCategory = title(ddlFCategory, RequiredFieldValidatorFCategory, txtFCategory, RequiredFieldValidatortxtF);
        }


        protected void ddlGCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlGCategory.SelectedIndex);
            ddlGCategory.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtGCategory.Text = hdnstrTitleDesc.Value;
            string GCategory = title(ddlGCategory, RequiredFieldValidatorGCategory, txtGCategory, RequiredFieldValidatortxtG);
        }

        protected void ddlHCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlHCategory.SelectedIndex);
            ddlHCategory.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtHCategory.Text = hdnstrTitleDesc.Value;
            string HCategory = title(ddlHCategory, RequiredFieldValidatorHCategory, txtHCategory, RequiredFieldValidatortxtH);
        }
        
        protected void ddlICategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlICategory.SelectedIndex);
            ddlICategory.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtICategory.Text = hdnstrTitleDesc.Value;
            string ICategory = title(ddlICategory, RequiredFieldValidatorICategory, txtICategory, RequiredFieldValidatortxtI);
        }
        protected void ddlJ1Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlJ1Category.SelectedIndex);
            ddlJ1Category.SelectedIndex =   ddlJ2Category.SelectedIndex =Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtJ1Category.Text = txtJ2Category.Text= hdnstrTitleDesc.Value;
            txtJ2Category.Text = hdnstrTitleDesc.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            string J2Category = title(ddlJ2Category, RequiredFieldValidatorJ2Category, txtJ2Category, RequiredFieldValidatortxtJ2);
        }


        protected void ddlJ2Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlJ2Category.SelectedIndex);
            ddlJ2Category.SelectedIndex = ddlJ2Category.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtJ2Category.Text = txtJ1Category.Text=hdnstrTitleDesc.Value;
            txtJ1Category.Text = hdnstrTitleDesc.Value;
            string J1Category = title(ddlJ1Category, RequiredFieldValidatorJ1Category, txtJ1Category, RequiredFieldValidatortxtJ1);
            string J2Category = title(ddlJ2Category, RequiredFieldValidatorJ2Category, txtJ2Category, RequiredFieldValidatortxtJ2);
        }
        

        protected void ddlP1Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlP1Category.SelectedIndex);
            ddlP1Category.SelectedIndex = ddlP2Category.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtP1Category.Text = txtP2Category.Text = hdnstrTitleDesc.Value;
            string P1Category = title(ddlP1Category, RequiredFieldValidatorP1Category, txtP1Category, RequiredFieldValidatortxtP1);
            string P2Category = title(ddlP2Category, RequiredFieldValidatorP2Category, txtP2Category, RequiredFieldValidatortxtP2);
        }


        protected void ddlP2Category_SelectedIndexChanged(object sender, EventArgs e)
        {
            hdnintTitle.Value = Convert.ToString(ddlP2Category.SelectedIndex);
            ddlP1Category.SelectedIndex = ddlP2Category.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
            if (hdnstrTitleDesc.Value != "")
                txtP2Category.Text =txtP1Category.Text = hdnstrTitleDesc.Value;
            string P2Category = title(ddlP2Category, RequiredFieldValidatorP2Category, txtP2Category, RequiredFieldValidatortxtP2);
            string P1Category = title(ddlP1Category, RequiredFieldValidatorP1Category, txtP1Category, RequiredFieldValidatortxtP1);
        }


        protected void txtQGarage_TextChanged(object sender, EventArgs e)
        {          
                if (hdnstrTitleDesc.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }
        protected void txtQstartTime_TextChanged(object sender, EventArgs e)
        {            
                if (hdnstrTitleDesc.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }
        protected void txtQendTime_TextChanged(object sender, EventArgs e)
        {           
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);            
        }
        protected void ddlCatQ2_SelectedIndexChanged(object sender, EventArgs e)
        {
              hdnintTitle.Value = Convert.ToString(ddlCatQ2.SelectedIndex);
                ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
            
        }
        protected void ddlCatQ3_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                hdnintTitle.Value = Convert.ToString(ddlCatQ3.SelectedIndex);
                ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }
        protected void ddlDoQ_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                if (hdnstrTitleDesc.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }
        protected void ddlOccurQ_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                if (hdnstrTitleDesc.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
            
        }
        protected void ddlCatQ4_SelectedIndexChanged(object sender, EventArgs e)
        {
             hdnintTitle.Value = Convert.ToString(ddlCatQ4.SelectedIndex);
                ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }
        protected void txtQincidents_TextChanged(object sender, EventArgs e)
        {           
                if (hdnstrTitleDesc.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);

                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
            
        }
        protected void txtQemp1_TextChanged(object sender, EventArgs e)
        {
          
                hdnstrTitleDesc.Value = txtQemp1.Text;
                txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }
        protected void txtQemp2_TextChanged(object sender, EventArgs e)
        {
                hdnstrTitleDesc.Value = txtQemp2.Text;
                txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);           

        }
        protected void txtQemp3_TextChanged(object sender, EventArgs e)
        {           
                hdnstrTitleDesc.Value = txtQemp3.Text;
                txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
            
        }
        protected void txtQemp4_TextChanged(object sender, EventArgs e)
        {            
                hdnstrTitleDesc.Value = txtQemp4.Text;
                txtQemp1.Text = txtQemp2.Text = txtQemp3.Text = txtQemp4.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatQ1.SelectedIndex = ddlCatQ2.SelectedIndex = ddlCatQ3.SelectedIndex = ddlCatQ4.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatQ1 = title(ddlCatQ1, RequiredFieldValidator26, txtQemp1, RequiredFieldValidator1);
                string CatQ2 = title(ddlCatQ2, RequiredFieldValidator28, txtQemp2, RequiredFieldValidator7);
                string CatQ3 = title(ddlCatQ3, RequiredFieldValidator29, txtQemp3, RequiredFieldValidator24);
                string CatQ4 = title(ddlCatQ4, RequiredFieldValidator32, txtQemp4, RequiredFieldValidator34);
                            hdnStatement.Value = string.Format(Statement, txtQOnDate.Text, CatQ1, lblQemp1.Text, txtQGarage.Text, txtQstartTime.Text, txtQendTime.Text, CatQ2, lblQemp2.Text, CatQ3, lblQemp3.Text, ddlDoQ.SelectedItem.Text, ddlOccurQ.SelectedItem.Text, CatQ4, lblQemp4.Text, txtQincidents.Text);
           
        }

        #endregion

        #region Statement R
        /// <summary>
        /// This method prepares the statement by collecting the assigned values
        /// </summary>

        private void PrepareStatementR()
        {

            String StatementR;           

                ChangeControlStatus();

                pnTypeR.Visible = true;

                StatementR = "On {0}, {1} was scheduled to appear at DSNY Trial room, located at 44 Beaver Street,NY at 0800. {2} ";

                StatementR = StatementR + "was no call no show and failed to contact the Advocate's Office as to the reason for his/her absence or inability to travel to DSNY Trial room.";

                Statement = StatementR;

                hdnStatement.Value = string.Format(Statement, txtTrialName.Text, txtTrialDate.Text, txtIncident.Text);
           

        }

        /// <summary>

        /// This method is on change text event that assigns the value to a hiden control

        /// </summary>

        /// <param name="sender"></param>

        /// <param name="e"></param>

        protected void txtTrialName_TextChanged(object sender, EventArgs e)
        {          
                hdnStatement.Value = string.Format(Statement, txtTrialName.Text, txtTrialDate.Text, txtIncident.Text);          
        }


        protected void txtTrialDate_TextChanged(object sender, EventArgs e)
        {           
                hdnStatement.Value = string.Format(Statement, txtTrialName.Text, txtTrialDate.Text, txtIncident.Text);
        }

        protected void txtIncident_TextChanged(object sender, EventArgs e)
        {            
                hdnStatement.Value = string.Format(Statement, txtTrialName.Text, txtTrialDate.Text, txtIncident.Text);            
        }

        #endregion

        #region Statement S
        private void PrepareStatementS()
        {
            String StatementS;
           
                ChangeControlStatus();
                pnTypeS.Visible = true;
                StatementS = "You have engaged in activities that are deemed to be prejudicial to the good order and discipline of this Department. Your activities and your arrest of ";
                StatementS = StatementS + "{0} for {1} has brought discredit to the City of New York.";
                Statement = StatementS;
                hdnStatement.Value = string.Format(Statement, txtprejDate.Text, txtprejName.Text);
           
        }
        protected void txtprejDate_TextChanged(object sender, EventArgs e)
        {           
            hdnStatement.Value = string.Format(Statement, txtprejDate.Text, txtprejName.Text);            
        }

        protected void txtprejName_TextChanged(object sender, EventArgs e)
        {            
                hdnStatement.Value = string.Format(Statement, txtprejDate.Text, txtprejName.Text);           
        }


        #endregion

        #region Statement T
        /// <summary>
        /// This method prepares the statement by collecting the hours worked, date, vehicle an location
        /// </summary>
        private void PrepareStatementT()
        {
            String StatementT;           
                ChangeControlStatus();
                pnTypeT.Visible = true;
                lblTemp1.Text = lblTemp2.Text = lblTemp3.Text = lblTemp4.Text = hdnEmpName.Value;
                lblTemp5.Text = hdnEmpName.Value + "'s";
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                StatementT = "On  {0}, {1} {2} was assigned to {3} on the {4} to {5} shift. {6} {7} called {8} at {9} requesting leave for {10}. ";
                StatementT = StatementT + "Emergency leave was granted pending proof.  {11} {12} failed to submit proof within 2 work days. As a result  {13} {14} ";
                StatementT = StatementT + "was marked absent and docked 8 hours pay. This is {15} {16} {17} incident in a 12 month period. ";
                Statement = StatementT;
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void ddlCatTWeek_SelectedIndexChanged(object sender, EventArgs e)
        {           
                if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void txtTOnDate_TextChanged(object sender, EventArgs e)
        {
            
                if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void ddlCatT1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
                hdnintTitle.Value = Convert.ToString(ddlCatT1.SelectedIndex);
                ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void txtT1Garage_TextChanged(object sender, EventArgs e)
        {
             if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void txtTstartTime_TextChanged(object sender, EventArgs e)
        {
            
                if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void txtTendTime_TextChanged(object sender, EventArgs e)
        {
             if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                   txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
            
        }
        protected void ddlCatT2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
                hdnintTitle.Value = Convert.ToString(ddlCatT2.SelectedIndex);
                ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
            
        }
        protected void txtT2Garage_TextChanged(object sender, EventArgs e)
        {
           
                if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void txtTtime_TextChanged(object sender, EventArgs e)
        {
            
                if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
            
        }
        protected void txtTtype_TextChanged(object sender, EventArgs e)
        {
           
                if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void ddlCatT3_SelectedIndexChanged(object sender, EventArgs e)
        {           
                hdnintTitle.Value = Convert.ToString(ddlCatT3.SelectedIndex);
                ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void ddlCatT4_SelectedIndexChanged(object sender, EventArgs e)
        {            
                hdnintTitle.Value = Convert.ToString(ddlCatT4.SelectedIndex);
                ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void ddlCatT5_SelectedIndexChanged(object sender, EventArgs e)
        {
             hdnintTitle.Value = Convert.ToString(ddlCatT5.SelectedIndex);
                ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                 string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
           
        }
        protected void txtTincidents_TextChanged(object sender, EventArgs e)
        {            
                if (hdnstrTitleDesc.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);            
        }

         protected void txtTemp1_TextChanged(object sender, EventArgs e)
            {
           
                hdnstrTitleDesc.Value = txtTemp1.Text;
                txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text =  txtTemp5.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);           
        }
         protected void txtTemp2_TextChanged(object sender, EventArgs e)
         {            
                 hdnstrTitleDesc.Value = txtTemp2.Text;
                 txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                 if (hdnintTitle.Value != "")
                     ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);

                 string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                 string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                 string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                 string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                 string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                 hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
             
         }
         protected void txtTemp3_TextChanged(object sender, EventArgs e)
         {             
                 hdnstrTitleDesc.Value = txtTemp3.Text;
                 txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                 if (hdnintTitle.Value != "")
                     ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);

                 string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                 string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                 string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                 string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                 string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                 hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
            
         }
         protected void txtTemp4_TextChanged(object sender, EventArgs e)
         {            
                 hdnstrTitleDesc.Value = txtTemp4.Text;
                 txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                 if (hdnintTitle.Value != "")
                     ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                 string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                 string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                 string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                 string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                 string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                 hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
             
         }
         protected void txtTemp5_TextChanged(object sender, EventArgs e)
         {            
                 hdnstrTitleDesc.Value = txtTemp5.Text;
                 txtTemp1.Text = txtTemp2.Text = txtTemp3.Text = txtTemp4.Text = txtTemp5.Text = hdnstrTitleDesc.Value;
                 if (hdnintTitle.Value != "")
                     ddlCatT1.SelectedIndex = ddlCatT2.SelectedIndex = ddlCatT3.SelectedIndex = ddlCatT4.SelectedIndex = ddlCatT5.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                 string CatT1 = title(ddlCatT1, RequiredFieldValidator15, txtTemp1, RequiredFieldValidator35);
                 string CatT2 = title(ddlCatT2, RequiredFieldValidator17, txtTemp2, RequiredFieldValidator36);
                 string CatT3 = title(ddlCatT3, RequiredFieldValidator19, txtTemp3, RequiredFieldValidator37);
                 string CatT4 = title(ddlCatT4, RequiredFieldValidator20, txtTemp4, RequiredFieldValidator38);
                 string CatT5 = title(ddlCatT5, RequiredFieldValidator21, txtTemp5, RequiredFieldValidator39);
                 hdnStatement.Value = string.Format(Statement, txtTOnDate.Text, CatT1, lblTemp1.Text, txtT1Garage.Text, txtTstartTime.Text, txtTendTime.Text, CatT2, lblTemp2.Text, txtT2Garage.Text, txtTtime.Text, txtTtype.Text, CatT3, lblTemp3.Text, CatT4, lblTemp4.Text, CatT5, lblTemp5.Text, txtTincidents.Text);
            
         }
        #endregion

        #region Statement U
        /// <summary>
        /// This method prepares the statement by collecting the hours worked, date, vehicle an location
        /// </summary>
        private void PrepareStatementU()
        {
            String StatementU;
               ChangeControlStatus();
                pnTypeU.Visible = true;
                lblUemp1.Text = lblUemp2.Text = lblUemp3.Text = lblUemp4.Text = lblUemp5.Text =  hdnEmpName.Value;
                lblUemp6.Text = hdnEmpName.Value+"'s";
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                StatementU = "On {0}, {1} {2} was assigned to {3} on the {4} to {5} shift. {6} {7} called {8} at {9} requesting leave for {10}. ";
                StatementU = StatementU + "Emergency leave was granted pending proof.  {11} {12} proof was deemed unsatisfactory by {13}. {14} {15} was given 5 additional work days to submit satisfactory proof, which was also unsatisfactory. As a result ";
               StatementU = StatementU + "{16} {17} was marked absent and docked 8 hours pay.  This is {18} {19} {20} incident in a 12 month period. ";
                Statement = StatementU;
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void ddlCatUWeek_SelectedIndexChanged(object sender, EventArgs e)
        {           
                if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;       
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
               hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtUOnDate_TextChanged(object sender, EventArgs e)
        {           
                if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
          
        }
        protected void ddlCatU1_SelectedIndexChanged(object sender, EventArgs e)
        {           
                hdnintTitle.Value= Convert.ToString(ddlCatU1.SelectedIndex);
                ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
       
        }
        protected void txtU1Garage_TextChanged(object sender, EventArgs e)
        {
             if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value; 
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtUstartTime_TextChanged(object sender, EventArgs e)
        {           
                if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtUendTime_TextChanged(object sender, EventArgs e)
        {          
                if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void ddlCatU2_SelectedIndexChanged(object sender, EventArgs e)
        {
             hdnintTitle.Value = Convert.ToString(ddlCatU2.SelectedIndex);
                ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtU2Garage_TextChanged(object sender, EventArgs e)
        {            
                if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
            
        }
        protected void txtUtime_TextChanged(object sender, EventArgs e)
        {
           if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtUtype_TextChanged(object sender, EventArgs e)
        {
              if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
            
        }
        protected void ddlCatU3_SelectedIndexChanged(object sender, EventArgs e)
        {           
                hdnintTitle.Value = Convert.ToString(ddlCatU3.SelectedIndex);
                ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
            
        }
        protected void txtUEmp_TextChanged(object sender, EventArgs e)
        {
               if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
          
        }
        protected void ddlCatU4_SelectedIndexChanged(object sender, EventArgs e)
        {
                hdnintTitle.Value = Convert.ToString(ddlCatU4.SelectedIndex);
                ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
     
        }
        protected void ddlCatU5_SelectedIndexChanged(object sender, EventArgs e)
        {            
                hdnintTitle.Value = Convert.ToString(ddlCatU5.SelectedIndex);
                ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void ddlCatU6_SelectedIndexChanged(object sender, EventArgs e)
        {
                hdnintTitle.Value = Convert.ToString(ddlCatU6.SelectedIndex);
                ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU6, RequiredFieldValidator13, txtUemp6, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtUincidents_TextChanged(object sender, EventArgs e)
        {
              if (hdnstrTitleDesc.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                if (hdnstrTitleDesc.Value != "")
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;         
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU5, RequiredFieldValidator13, txtUemp5, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
          
        }
        protected void txtUemp1_TextChanged(object sender, EventArgs e)
        {                        
                    hdnstrTitleDesc.Value = txtUemp1.Text;
                    txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU5, RequiredFieldValidator13, txtUemp5, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtUemp2_TextChanged(object sender, EventArgs e)
        {
             hdnstrTitleDesc.Value = txtUemp2.Text;
                txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU5, RequiredFieldValidator13, txtUemp5, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
         
        }
        protected void txtUemp3_TextChanged(object sender, EventArgs e)
        {            
                hdnstrTitleDesc.Value = txtUemp3.Text;
                txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU5, RequiredFieldValidator13, txtUemp5, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtUemp4_TextChanged(object sender, EventArgs e)
        {
              hdnstrTitleDesc.Value = txtUemp4.Text;
                txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU5, RequiredFieldValidator13, txtUemp5, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
           
        }
        protected void txtUemp5_TextChanged(object sender, EventArgs e)
        {           
                hdnstrTitleDesc.Value = txtUemp5.Text;
                txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU5, RequiredFieldValidator13, txtUemp5, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);
            
        }
        protected void txtUemp6_TextChanged(object sender, EventArgs e)
        {           
                hdnstrTitleDesc.Value = txtUemp6.Text;
                txtUemp1.Text = txtUemp2.Text = txtUemp3.Text = txtUemp4.Text = txtUemp5.Text = txtUemp6.Text = hdnstrTitleDesc.Value;
                if (hdnintTitle.Value != "")
                    ddlCatU1.SelectedIndex = ddlCatU2.SelectedIndex = ddlCatU3.SelectedIndex = ddlCatU4.SelectedIndex = ddlCatU5.SelectedIndex = ddlCatU6.SelectedIndex = Convert.ToInt32(hdnintTitle.Value);
                string CatU1 = title(ddlCatU1, RequiredFieldValidator3, txtUemp1, RequiredFieldValidator40);
                string CatU2 = title(ddlCatU2, RequiredFieldValidator2, txtUemp2, RequiredFieldValidator41);
                string CatU3 = title(ddlCatU3, RequiredFieldValidator10, txtUemp3, RequiredFieldValidator42);
                string CatU4 = title(ddlCatU4, RequiredFieldValidator12, txtUemp4, RequiredFieldValidator43);
                string CatU5 = title(ddlCatU5, RequiredFieldValidator11, txtUemp5, RequiredFieldValidator44);
                string CatU6 = title(ddlCatU5, RequiredFieldValidator13, txtUemp5, RequiredFieldValidator45);
                hdnStatement.Value = string.Format(Statement, txtUOnDate.Text, CatU1, lblUemp1.Text, txtU1Garage.Text, txtUstartTime.Text, txtUendTime.Text, CatU2, lblUemp2.Text, txtU2Garage.Text, txtUtime.Text, txtUtype.Text, CatU3, lblUemp3.Text, txtUEmp.Text, CatU4, lblUemp4.Text, CatU5, lblUemp5.Text, CatU6, lblUemp6.Text, txtUincidents.Text);           
        }
        #endregion
        #endregion

    }
}