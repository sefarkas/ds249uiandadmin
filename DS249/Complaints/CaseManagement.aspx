<%@ Page Title="DSNY: DS-249" Language="C#" MasterPageFile="~/MasterPage.master"
    EnableEventValidation="false" AutoEventWireup="true" Inherits="DSNY.DSNYCP.DS249.Complaints_CaseManagement"
    CodeBehind="CaseManagement.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControl/WitnessControl.ascx" TagName="Witness" TagPrefix="uc1" %>
<%@ Register Src="~/UserControl/ComplainantControl.ascx" TagName="Witness" TagPrefix="uc2" %>
<%@ Register Src="~/UserControl/IncidentDateControl.ascx" TagName="Witness" TagPrefix="uc3" %>
<%@ Register Src="~/UserControl/VoilationControl.ascx" TagName="Witness" TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<%--    <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .style2
        {
            width: 602px;
        }
        .style7
        {
            font-size: medium;
            font-weight: bold;
        }
        .style11
        {
            width: 232px;
        }
        .style13
        {
            width: 231px;
        }
        .style15
        {
            width: 439px;
        }
        .style18
        {
            border: 1px solid #1e951f;
            width: 18%;
        }
        .style19
        {
            width: 18%;
        }
        .style20
        {
            width: 170px;
        }
        .style22
        {
            width: 419px;
        }
    </style>
    <script language="javascript" type="text/javascript">

        var hasResult = 0;
        var getttingResult = 0;

        function OpenStatement(hdnStmt) {

            var crt = document.getElementById(hdnStmt.id);
            var URL = "ComplainantStatement.aspx?StmtCID=" + hdnStmt.id + "&Garbage=Utf-8?B?VGFyYWtlc2h3YXIgUmVkZHk=?=";
            var dialogResults = window.showModalDialog(URL, '', 'dialogHeight=350px;dialogWidth=620px;toolbar=no;location=no;menubar=no');


            if ((dialogResults != undefined) && (dialogResults != false)) {

                if (dialogResults.substring(0, 6) == "CANCEL") {
                    return;
                }
                else {
                    document.getElementById(hdnStmt.id).value = dialogResults;
                }
            }
        }


        function setDDLVal(val, control) {
            if (control != undefined) {
                for (i = 0; i < document.getElementById(control.id).length; i++) {
                    if (document.getElementById(control.id).options[i].value == val) {
                        document.getElementById(control.id).selectedIndex = i
                    }
                }
            }
        }


        function openEditStatement(ddlType, hdnClientVal, hdnStmt, hlEditID) {

            var ddl = document.getElementById(ddlType.id);
            var selVal = ddl.options[ddl.selectedIndex].value;
            OpenStatement(selVal, hdnClientVal, hdnStmt, hlEditID);
        }

        function OpenModalWindow(URL) {
            window.showModalDialog(URL, 'Complaint History', 'dialogWidth:975px;dialogHeight:770px;scroll:yes');
            return false;
        }


        function HideContent(d) {
            if (d.length < 1) { return; }
            document.getElementById('divProgress').style.display = "none";
        }

        var cX = 0; var cY = 0; var rX = 0; var rY = 0;
        function UpdateCursorPosition(e) {
            cX = e.pageX; cY = e.pageY;
        }

        function UpdateCursorPositionDocAll(e) {
            cX = event.clientX; cY = event.clientY;
        }

        if (document.all) {
            document.onmousemove = UpdateCursorPositionDocAll;
        }
        else {
            document.onmousemove = UpdateCursorPosition;
        }

        function AssignPosition(d) {
            if (self.pageYOffset) {
                rX = self.pageXOffset;
                rY = self.pageYOffset;
            }
            else if (document.documentElement && document.documentElement.scrollTop) {
                rX = document.documentElement.scrollLeft;
                rY = document.documentElement.scrollTop;
            }
            else if (document.body) {
                rX = document.body.scrollLeft;
                rY = document.body.scrollTop;
            }
            if (document.all) {
                cX += rX;
                cY += rY;
            }
            d.style.left = (cX + 10) + "px";
            d.style.top = (cY + 10) + "px";
        }

        function HideContent(d) {
            if (d.length < 1) { return; }
            document.getElementById('divProgress').style.display = "none";
        }

        function ShowContent(d) {
            if (d.length < 1) { return; }
            var dd = document.getElementById('divProgress');
            AssignPosition(dd);
            dd.innerHTML = '<asp:Image runat="server" ImageUrl="~/includes/img/mozilla_blu.gif" />';
            dd.style.display = "block";

        }
        function ReverseContentDisplay(d) {
            if (d.length < 1) { return; }
            var dd = document.getElementById('divProgress');
            AssignPosition(dd);
            if (dd.style.display == "none") { dd.style.display = "block"; }
            else { dd.style.display = "none"; }
        }

        function PerformPostBack(sender, e) {
            $get('ctl00$ContentPlaceHolder1$Button1').click();
        }

        function OpenPrint(IndexNo) {
            if (IndexNo != "") {
                var URL = "Print.aspx?Id=" + IndexNo;
                // alert(URL);
                var dialogResults = window.open(URL, '');
            }
            else {
                alert("ComplaintID is Blank Or Missing");
            }
            return false;
        }

        // Function for Employee Search
        function shownev() {
            hasResult = 1;
            getttingResult = 0;
            $get('<%=lbSearchMsg1.ClientID%>').innerText = "";
        }

        function populatingev() {
            getttingResult = 1;
            hasResult = 0;         
            $get('<%=lbSearchMsg1.ClientID%>').innerText = "Retreiving..";
        }

        function onhiding() {

            if (getttingResult == 1) {
                if (hasResult == 0) {
                    $get('<%=lbSearchMsg1.ClientID%>').innerText = "No Match Found";

                }
            }
        }

        function validatePage() {
            //Executes all the validation controls associated with group1 validaiton Group1.
            window.scrollTo(0, 0);
            var flag = Page_ClientValidate('Approve');
            if (flag)
            //Executes all the validation controls which are not associated with any validation group. 
                flag = Page_ClientValidate();
            return flag;
        }
        //************PENDING DOCUMENT************

       // This method will take the confirmation from the user at the SAVE/APPROVE
        function CloseComfirmation(type) {
       //     var chkbox = document.getElementById("ctl00_ContentPlaceHolder1_chkSuppDoc");
          //  var lblConfirm = document.getElementById("ctl00_ContentPlaceHolder1_SupCheckBox");
            var hdnPendingvalue = document.getElementById("ctl00_ContentPlaceHolder1_hdnPendingValue");
            execScript('n = msgbox("Is complaint information complete? If applicable- Are all documents uploaded to the Advocate?","4132")', "vbscript");

            if (n == 6) {
                hdnPendingvalue.value = "";
              //  lblConfirm.visible = true;

            }
            else {

                hdnPendingvalue.value = "P";             
                if (type == "A")
                    hdnPendingvalue.value = "P";
                    
            }
        }

        
 //This method will popUp the upload popup and display the confirmation checkbox
        function OpenComfirmation(p1, p2, p3, p4) 
        {
           // if (confirm("Are you sure you want to attach the documents?"))
            //{
                   if (confirm("Attach the document only when you route the complaint to the Advocate.\n Please confirm.")) {
                       //************PENDING DOCUMENT************
                    document.getElementById("ctl00_ContentPlaceHolder1_hdnPendingValue").value = 'P';
                    var chkbox = document.getElementById("ctl00_ContentPlaceHolder1_SupCheckBox");
                    if (chkbox.style.visibility == "hidden") 
                    {
                        chkbox.style.visibility = "visible";
                    }

                    var stringURL = "Upload.aspx?ChildID1=" + p1 + "&Mode=" + p2 + "&ParentID=" + p3 + "&CountID=" + p4;
                    window.open(stringURL, "UpLoad", "scrollbars=yes,resizable=yes,width=400,height=200,titlebar=no");
                    return false;

              //  }
            }
        }
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

<%-- ************PENDING DOCUMENT************--%>  
    <asp:HiddenField runat="server"  ID="hdnPendingDocs" />
  

  <center>
        <table style="width: 832px;" class="tableMargin" cellpadding="0px" cellspacing="0px"
            border="0">
            <tr>
                <td class="styleHeading">
                    <br />
                    <div>
                        <span>&nbsp;&nbsp; DS-249&nbsp;
                            <asp:Label ID="lbHeader" runat="server" Text=""></asp:Label></span></div>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image runat="server" SkinId="skidClear" height="15" width="63" />
                </td>
            </tr>
            <tr>
                <td valign="top" align="left">
                    <!-- Button Goes Here -->
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                                OnClick="ImageButton1_Click" CausesValidation="False" />
                            <asp:Image runat="server" SkinId="skidClear" width="25" />
                            &nbsp;
                            <asp:ImageButton ID="ibSave" runat="server" ImageUrl="~/includes/img/ds249_btn_save_static.png"
                                OnClick="ibSave_Click" OnClientClick="javascript:window.scrollTo(0,0);" />
                            <asp:Image runat="server" SkinId="skidClear" width="25" />
                            <asp:ImageButton ID="ibPrint" runat="server" ImageUrl="~/includes/img/ds249_btn_print_static.png"
                                CausesValidation="false" />
                            <asp:Image runat="server" SkinId="skidClear" width="25" />
                            <asp:ImageButton ID="ibApprove" runat="server" ImageUrl="~/includes/img/ds249_btn_approve_static.png"
                                OnClick="ibApprove_Click" />
                            <asp:Image runat="server" SkinId="skidClear" width="25" />
                            <asp:ImageButton ID="ibReject" runat="server" ImageUrl="~/includes/img/PMD_btn_return_static.png"
                                OnClick="Reject_Click" ToolTip="Send back for additional editing" />
                            <asp:ImageButton ID="ibSubmit" runat="server" ImageUrl="~/includes/img/submit_btn.png"
                                OnClick="ibSubmit_Click" OnClientClick="javascript:return validatePage();" Visible="False" />
                            <cc1:ConfirmButtonExtender ID="ibSubmit_ConfirmButtonExtender" runat="server" ConfirmText="Are you sure you want to submit?"
                                Enabled="True" TargetControlID="ibSubmit">
                            </cc1:ConfirmButtonExtender>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td colspan="1" valign="top">
                    <!-- Search content Goes here -->
                    <asp:UpdatePanel ID="upSearch" runat="server">
                        <ContentTemplate>
                            <table border="0" style="width: 955px">
                                <tr>
                                    <td width="30%">
                                    </td>
                                    <td class="searchCase" valign="middle" width="40%">
                                        EMPLOYEE SEARCH
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="txtSearch" runat="server" Width="240px" OnTextChanged="txtSearch_TextChanged"
                                            CssClass="inputFieldStyle"></asp:TextBox>
                                        <asp:Image runat="server" SkinId="skidMiniBtn"  height="24" width="24" CssClass="infoIconStyle" />
                                        <cc1:TextBoxWatermarkExtender ID="txtSearch_TextBoxWatermarkExtender" runat="server"
                                            TargetControlID="txtSearch" WatermarkCssClass="WaterMark" WatermarkText="Enter First 2 Characters of the Name">
                                        </cc1:TextBoxWatermarkExtender>
                                        <cc1:AutoCompleteExtender ID="txtSearchACE1" runat="server" BehaviorID="txtSearchace1"
                                            DelimiterCharacters="" CompletionInterval="1000" MinimumPrefixLength="2" CompletionSetCount="12"
                                            EnableCaching="true" Enabled="true" ServicePath="" UseContextKey="true" ServiceMethod="SearchEmployee"
                                            TargetControlID="txtSearch" CompletionListCssClass="CompletionList" CompletionListHighlightedItemCssClass="HighlightedItem"
                                            CompletionListItemCssClass="ListItem" OnClientItemSelected="PerformPostBack"
                                            OnClientPopulating="populatingev" OnClientPopulated="shownev" OnClientHiding="onhiding">
                                        </cc1:AutoCompleteExtender>
                                        <asp:RequiredFieldValidator ID="rvSearch" runat="server" ErrorMessage="Please Select an Employee."
                                            ControlToValidate="txtSearch" Display="None">!</asp:RequiredFieldValidator>
                                        <br />
                                        <asp:Label ID="lbSearchMsg1" runat="server" Text="" Font-Bold="True" Font-Size="Small"></asp:Label>
                                                                                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" CausesValidation="False" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" valign="top">
                    <!-- Error Message Content Goes here -->
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Label ID="lbMessage" runat="server" Width="50%" Font-Size="Medium" Font-Bold="true"></asp:Label>
                            <asp:Image runat="server" SkinId="skidClear"  height="15" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                        runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                        <Animations>                                
                                                <OnUpdated>
                                                    <FadeOut Duration="5.0" Fps="24" />
                                                </OnUpdated>
                        </Animations>
                    </cc1:UpdatePanelAnimationExtender>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center" valign="top">
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Please Correct the Following Errors:"
                        EnableViewState="False" />
                </td>
            </tr>
            <tr>
                <td colspan="2" style="color: #FF0000; font-weight: bold">
                    &nbsp;Fields Marked with * are required.
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image runat="server" SkinId="skidTopCap" width="88%" />
                </td>
            </tr>
            <tr>
                <td height="30">
                    <asp:Image runat="server" ImageUrl="~/includes/img/ds249_bar.png" style="height: 30px;" width="88%" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:UpdatePanel ID="upComplaint" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="txtSearch" EventName="TextChanged" />
                        </Triggers>
                        <ContentTemplate>
                            <!-- Main content goes here -->
                            <table style="width: 832px" class="bordergreen" cellspacing="0px" cellpadding="0px"
                                border="0">
                                <tr>
                                    <td>
                                        <table border="0" width="100%">
                                          <tbody>
                                                <tr>
                                                    <td >
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="1" class="bordergreen">
                                        <table border="0" width="90%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        DATE ENTERED
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblDate" runat="server" CssClass="labelStyle inputFieldStyle"></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="1" class="bordergreen">
                                        <table border="0" width="90%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        INDEX NO
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblIndexNumber" runat="server" CssClass="labelStyle inputFieldStyle"></asp:Label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>
                                        &nbsp;PROMOTION DATE
                                        <br />
                                        &nbsp;<asp:TextBox ID="txtPromotionDate" runat="server" Height="17px" Width="140px"></asp:TextBox>
                                        <asp:Image ID="imgPromotionDate" runat="server" SkinID="skidMiniCalButton" />
                                        <cc1:CalendarExtender ID="txtPromotionDate_CalendarExtender" runat="server" CssClass="MyCalendar"
                                            Enabled="True" Format="MM/dd/yyyy" PopupButtonID="imgPromotionDate" TargetControlID="txtPromotionDate">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="2" class="tdReadOnly">
                                        <table border="0" width="90%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        PAYROLL LOCATION
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtPayrollLoc" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="184px"></asp:TextBox>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" class="style18">
                                        <table border="0" style="width: 99%">
                                            <tr>
                                                <td>
                                                    DISTRICT/DIVISION
                                                </td>
                                            </tr>
                                        </table>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:DropDownList ID="ddlLocation" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlLocation_SelectedIndexChanged"
                                            AppendDataBoundItems="true" Width="234px" CssClass="inputFieldStyle" Height="20px">
                                            <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="LocValidator" runat="server" ControlToValidate="ddlLocation"
                                            ErrorMessage="Please Select District/Division" InitialValue="-1">!</asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="1" class="bordergreen">
                                        <table border="0" width="90%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        BOROUGH
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:DropDownList ID="ddlBorough" runat="server" AppendDataBoundItems="true" Width="185px"
                                                            CssClass="inputFieldStyle">
                                                            <asp:ListItem Value="-1">[SELECT]</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="3" class="tdReadOnly">
                                        <table border="0" style="width: 98%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        NAME (LAST)
                                                    </td>
                                                    <td class="labelBoldStyle">
                                                        (FIRST)
                                                    </td>
                                                    <td class="labelBoldStyle">
                                                        (MI)
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style11">
                                                        <asp:TextBox ID="txtLastName" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="184px"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnEmployeeID" runat="server" />
                                                        <br />
                                                    </td>
                                                    <td class="style20">
                                                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="168px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtMName" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="1" class="tdReadOnly">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="3" class="tdReadOnly">
                                        <table border="0" style="width: 97%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        TITLE
                                                    </td>
                                                    <td class="labelBoldStyle">
                                                        DATE OF BIRTH
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="style22">
                                                        <asp:TextBox ID="txtTitle" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="400px"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtDOB" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="1" class="tdReadOnly">
                                        <table border="0" style="width: 100%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        EMPLOYEE REFERENCE NO
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtRefNumber" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="177px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="4" class="tdReadOnly">
                                        <table border="0" width="100%" style="width: 96%">
                                            <tr>
                                                <td class="labelBoldStyle">
                                                    STREET NO.
                                                </td>
                                                <td class="labelBoldStyle">
                                                    STREET NAME
                                                </td>
                                                <td class="labelBoldStyle">
                                                    APT
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="style13">
                                                    <asp:TextBox ID="txtStreetNumber" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                        Width="184px"></asp:TextBox>
                                                </td>
                                                <td class="style15">
                                                    <asp:TextBox ID="txtStreetName" runat="server" Width="365px" CssClass="txtBoxReadOnly"
                                                        ReadOnly="true"></asp:TextBox>
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAptNo" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="4" class="tdReadOnly">
                                        <table border="0" style="width: 96%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        CITY
                                                    </td>
                                                    <td class="labelBoldStyle">
                                                        STATE
                                                    </td>
                                                    <td class="labelBoldStyle" align="left">
                                                        ZIP CODE&nbsp;&nbsp;&nbsp; SUFFIX
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtCity" runat="server" CssClass="txtBoxReadOnly" Width="403px" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtState" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="170px"></asp:TextBox>
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtZip" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true" Width="40px"></asp:TextBox>
                                                        &nbsp;
                                                        <asp:TextBox ID="txtZipPrefix" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="30px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="tdReadOnly">
                                    <td colspan="2" class="bordergreen">
                                        <table border="0" style="width: 93%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        HIRE DATE
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtApptDate" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="155px"></asp:TextBox>(i.e. Month/Date/Year; xx/xx/xxxx)
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="1" class="bordergreen">
                                        &nbsp;
                                    </td>
                                    <td colspan="1" class="bordergreen">
                                        <table border="0" style="width: 85%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        CHART DAY NO.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtChartDay" runat="server" CssClass="txtBoxReadOnly" ReadOnly="true"
                                                            Width="177px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="4" class="bordergreen">
                                        <table border="0" width="100%">
                                            <tr>
                                                <td class="labelBoldStyle">
                                                    RULES AND ORDERS VIOLATED AS FOLLOWS (LIST OF RULE NOS.)
                                                    <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/includes/img/ds249_mini_plus_btn.png"
                                                        OnClick="ImageButton6_Click" CausesValidation="False" />
                                                    &nbsp;<br />
                                                    <%--  This checkBox is added to display confirmation on pending docuemnt.--%>

                                                    <%--************PENDING DOCUMENT************--%>                                             

                                                    <label id="SupCheckBox" runat="server">
                                                        <asp:CheckBox ID="chkSuppDoc" runat="server" />
                                                        Are all of the required documents uploaded so that the complaint can be routed to
                                                        the Advocate?</label>
                                                    <asp:HiddenField ID="hdnPendingValue" runat="server" />                                                
                                                    <br />
                                                    <table id="form1:chargeItemsTable" width="100%">
                                                        <tr>
                                                            <td>
                                                                <asp:PlaceHolder ID="phVoilations" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="bordergreen">
                                        <table>
                                            <tr class="labelBoldStyle">
                                                <td>
                                                    COMMENTS
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" 
                                                        Height="80px" Width="750px" CssClass="inputFieldStyle commentsStyle" ValidationGroup="a"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="4" class="bordergreen">
                                        <table border="0" width="100%">
                                            <tr>
                                                <td class="labelBoldStyle">
                                                    COMPLAINANT'S STATEMENT (SPECIFY ACTS WHICH CONSTITUTE VIOLATIONS)
                                                    <asp:ImageButton ID="ibAddComplainant" runat="server" ImageUrl="~/includes/img/ds249_mini_plus_btn.png"
                                                        OnClick="ibAddComplainant_Click" CausesValidation="False" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:PlaceHolder ID="pnComplainant" runat="server"></asp:PlaceHolder>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="4" class="bordergreen">
                                        <table width="100%">
                                            <tr>
                                                <td style="width: 50%;" class="style7">
                                                    HISTORY SECTION
                                                    <br />
                                                    <br />
                                                    <asp:GridView ID="gvHistory" runat="server" AutoGenerateColumns="False" BorderColor="#235705"
                                                        EmptyDataText="No Violation History Found" HeaderStyle-BackColor="#a4d49a" HeaderStyle-Font-Bold="true"
                                                        OnRowDataBound="gvHistory_RowDataBound" Width="830px">
                                                        <EmptyDataRowStyle CssClass="fromdate" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="INDEX NO">
                                                                <ItemTemplate>
                                                                    <a id="lkIndex" href="#" onclick="OpenModalWindow('ViewComplaintDetails.aspx?ID=<%# Eval("ComplaintID")%>&Mode=VIEW');return false;"
                                                                        style="font-size: 1.0em;">
                                                                        <%# Eval("IndexNo")%></a>
                                                                </ItemTemplate>
                                                                <HeaderStyle BorderColor="#235705" Font-Names="Arial,Verdana,sans-serif" Font-Size="1.0em"
                                                                    HorizontalAlign="Center" VerticalAlign="Middle" BorderWidth="2px" ForeColor="Black"
                                                                    Font-Underline="False" Wrap="False" Width="80px" />
                                                                <ItemStyle BorderColor="#235705" BackColor="#A4D49A" BorderWidth="1px" Font-Size="1.0em"
                                                                    HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="ComplaintID" DataFormatString="" HeaderText="INDEX NO">
                                                                <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                    VerticalAlign="Middle" Width="150px" Wrap="False" />
                                                                <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                                    Font-Bold="false" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="ViolationDateAsString" DataFormatString="" HeaderText="VIOLATION DATE">
                                                                <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                    VerticalAlign="Middle" Width="150px" Wrap="False" />
                                                                <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="150px"
                                                                    Font-Bold="false" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Violations" HeaderText="CHARGES">
                                                                <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                    VerticalAlign="Middle" Width="80px" Wrap="False" />
                                                                <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="80px"
                                                                    Font-Bold="false" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="ViolationDescription">
                                                                <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                    VerticalAlign="Middle" Width="180px" Wrap="False" />
                                                                <ItemStyle BackColor="#A4D49A" BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" HorizontalAlign="Center" VerticalAlign="Middle" Width="180px"
                                                                    Font-Bold="false" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Penalty" DataFormatString="" HeaderText="PENALTY">
                                                                <HeaderStyle BorderColor="#235705" BorderWidth="2px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" Font-Underline="False" ForeColor="Black" HorizontalAlign="Center"
                                                                    VerticalAlign="Middle" Width="450px" Wrap="False" />
                                                                <ItemStyle BorderColor="#235705" BorderWidth="1px" Font-Names="Arial,Verdana,sans-serif"
                                                                    Font-Size="1.0em" HorizontalAlign="left" VerticalAlign="Middle" Width="450px"
                                                                    Font-Bold="false" Wrap="true" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <HeaderStyle BackColor="#A4D49A" Font-Bold="True" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="2" class="bordergreen">
                                        <table border="0">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        WITNESSES
                                                        <asp:ImageButton ID="btnAddWitness" runat="server" ImageUrl="~/includes/img/ds249_mini_plus_btn.png"
                                                            OnClick="btnAddWitness_Click" CommandName="btnAddWitness" CausesValidation="False" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:PlaceHolder ID="pnWitnesses" runat="server"></asp:PlaceHolder>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="2" class="bordergreen">
                                        <table border="0" width="90%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle" valign="top">
                                                        <asp:CheckBox ID="chkProbation" runat="server" Text="Check If Employee Under Probation"
                                                            SkinID="" Font-Bold="true"/>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="bordergreen" >
                                    <td colspan="1" class="style2">
                                        &nbsp;
                                    </td>
                                    <td colspan="1" class="style19">
                                        &nbsp;
                                    </td>
                                    <td colspan="1" width="25%">
                                        &nbsp;
                                    </td>
                                    <td colspan="1" width="25%">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr class="bordergreen">
                                    <td colspan="1" class="style2">
                                        <table border="0" width="90%">
                                            <tbody>
                                                <tr>
                                                    <td>&nbsp;
                                                    </td>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="1" class="style19">
                                        <table border="0" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td class="labelBoldStyle">
                                                        <%--<input type="checkbox" name="form1:statementOfRights" id="form1:statementOfRights"
                                                            checked="checked" disabled="disabled" />Statement Of Rights--%>
                                                        <asp:CheckBox ID="chkStatementOfRights" runat="server" Enabled="true" Checked="true"
                                                            Text="Statement Of Rights" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="1">
                                        <table border="0" width="90%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td colspan="1">
                                        <table border="0" width="90%">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <div id="divProgress" style="display: none; position: absolute; border-style: none;
                                                            background-color: white;">&nbsp;
                                                        </div>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td height="30">
                    <asp:Image runat="server" ImageUrl="~/includes/img/ds249_bar.png"  style="width: 88%; height: 30px" />
                </td>
                <td height="30" style="width: 88%">&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Image runat="server" SkinId="skidBottomCap"  width="88%" />
                </td>
            </tr>
        </table>
    </center>
    <script language="javascript" type="text/javascript">

        Sys.Application.add_init(application_init);
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        function application_init(sender, args) {
            //prm.add_initializeRequest(prm_beginRequest);
            prm.add_beginRequest(prm_beginRequest);
            //prm.add_pageLoading(prm_pageLoading);
            //prm.add_pageLoaded(prm_pageLoaded);
            prm.add_endRequest(prm_endRequest);
        }

        function prm_beginRequest(sender, args) {

            // document.body.style.cursor = "wait";
            ShowContent('divProgress');
            //document.body.style = "cursor: url(..\includes\img\loading.gif);"

        }

        function prm_endRequest() {
            //document.body.style.cursor = "default";
            HideContent('divProgress');
        }

    </script>
</asp:Content>
