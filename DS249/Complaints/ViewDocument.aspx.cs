﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;
using System.Globalization;
using ExpertPdf.HtmlToPdf;
using System.Drawing;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.DS249.Complaints
{
    public partial class ViewDocument : System.Web.UI.Page
    {      
        protected void Page_Load(object sender, EventArgs e)
        {        

                Int64 parentID = Convert.ToInt64(Request.QueryString["ParentID"]);
                Int64 indexID = Convert.ToInt64(Request.QueryString["indexId"]);
                String ChildID1 = Convert.ToString(Request.QueryString["ChildID1"]);
                String Mode = Convert.ToString(Request.QueryString["Mode"]);
                List<UploadFile> upLoadDTO = new List<UploadFile>();               
                    if (Session["upLoadDTO"] != null)
                    {                       
                        upLoadDTO = (List<UploadFile>)Session["upLoadDTO"];
                        foreach (UploadFile upFile in upLoadDTO)
                        {
                            if ((upFile.IndexNo == indexID) & (upFile.ChildID  == ChildID1))
                            {        

                                Response.ClearHeaders();
                                Response.ClearContent();
                                Response.ContentType = upFile.ContentType;
                                Response.AddHeader("Content-Disposition", "inline");
                                Response.BinaryWrite(upFile.Image);                                
                                Response.End();
                            }

                        }

                    }               
        }
    }
}
