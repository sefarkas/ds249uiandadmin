﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.Sql;
using System.Text;
using DSNY.DSNYCP.ClassHierarchy;
using AjaxControlToolkit;
using System.ComponentModel;
using System.Configuration;
using System.Globalization;
/// <summary>
/// This class provides the functionality for events and methods for the DS249 Dashboard.
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class Complaints_Dashboard : System.Web.UI.Page
    {

        #region Page Level Variables

        ComplaintList complaintList;
        PersonnelList personnelList;
        User user;


        #endregion

        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
       
            /// <summary>
            /// THis property Gets total no of pages
            /// </summary>
            protected int? TotalPages
            {
                get { return (int?)ViewState["tp"]; }
                set { ViewState["tp"] = value; }
            }

        /// <summary>
        ///  This method sorts the databound items in a gridview
        /// </summary>
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!this.DesignMode)
            Pager1.OnPageChange += delegate(int nextPage, string pagerQueryKey)
            {                
                if (txtSearch.Text.Length > 0)
                {
                    SearchComplaints(true, nextPage);
                }
                else
                {
                    PopulateGrid(nextPage);
                }                
            };
        }

        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //asp:Image rendered as HTML input type=image which has an attribute of src not ImageUrl
            btnRef.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_refresh_roll.png'");
            btnRef.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_refresh_static.png'");

            btnNew.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_new_roll.png'");
            btnNew.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_new_static.png'");

            ibReports.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_report_roll.png'");
            ibReports.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_report_static.png'");



            btnSearch.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_search_roll.png'");
            btnSearch.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_search_static.png'");

            btnRef.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
            btnRef.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");


            user = (User)Session["clsUser"];
            if (user != null)
            {
                if (!user.IsInMemberShip(GroupName.DS249))
                    Response.Redirect("../UI/UnAuthorized.aspx");
                else
                {
                    user.CurrentMembership = GroupName.DS249;
                    SecurityUtility.AttachRolesToUser(user);
                }

                if (!User.IsInRole("Read"))
                    Response.Redirect("../UI/UnAuthorized.aspx");
            }
            else
                Response.Redirect("../UI/Login.aspx");

            if (Session["IsAuthenticated"] == null)
            {
                Response.Redirect("../UI/login.aspx?Session=False");
            }
            if (Session["IsAuthenticated"].ToString() == "false")
            {
                Response.Redirect("../UI/login.aspx?Session=False");
            }
            Page.Form.DefaultButton = btnSearch.UniqueID;

            CleanSession();
            DisplayButtonBasedOnPermission();
            lbMessage.Text = String.Empty;
            if (!IsPostBack)
            {
                PopulateGrid(gvComplaints.PageIndex);
            }
            Pager1.Visible = true;

        }
       
        public void DisableJumpTextBox()
        {
            if (Pager1.TotalPages <= 8)
            {
                Pager1.txtJumptoPage.Enabled = false;
            }
            else
            {
                Pager1.txtJumptoPage.Enabled = true;
            }
        }

        #region Private Methods
        /// <summary>
        /// This method gets the list of Charges based on the context key
        /// </summary>
        /// <param name="contextKey"></param>
        /// <returns></returns>
        [System.Web.Services.WebMethodAttribute(), System.Web.Script.Services.ScriptMethodAttribute()]
        public static string GetChargesList(string contextKey)
        {
            StringBuilder sTemp = new StringBuilder();

            VoilationList voilationList = new VoilationList();
           
                voilationList.Load(Convert.ToInt64(contextKey));

                if (voilationList.List.Count > 0)
                {
                    String code;
                    String desc;
                    int spaceIndex;
                    sTemp.Append("<table style='width:300px; border: #060F40 2px solid;color: #060F40;background: #ffffcc;'>");
                    sTemp.Append(String.Format("<tr><td colspan={0} align='left'><b>Charges:</b></td><tr>", voilationList.List.Count));
                    sTemp.Append("<tr><td><table>");
                    foreach (Voilation voilation in voilationList.List)
                    {
                        if (voilation.ViolationDescription != String.Empty)
                        {
                            spaceIndex = voilation.ViolationDescription.IndexOf(" ");
                            code = voilation.ViolationDescription.Substring(0, spaceIndex);
                            desc = voilation.ViolationDescription.Substring(spaceIndex).Trim();
                            sTemp.Append("<tr><td valign='top' align='left'>" + code + "</td><td align='left'>" + desc + "</td></tr>");
                        }
                    }
                    sTemp.Append("</table></td></tr></table>");
                }
                else
                    sTemp.Append("<i>Charges for this Complaint Not Found...</i>");
                return sTemp.ToString();            
        }
        /// <summary>
        /// This method displays the button based on the membership role permissions
        /// </summary>
        private void DisplayButtonBasedOnPermission()
        {
            if (!User.IsInRole("Write"))
                btnNew.Visible = false;
            if (!User.IsInRole("Report"))
            {
                ibReports.Visible = false;
                pnReportsMenu.Visible = false;
            }
        }

        /// <summary>
        /// This method cleans the sessions
        /// </summary>
        private void CleanSession()
        {            
                Session["Complaint"] = null;
                Session["Personnel"] = null;
                Session["witnessList"] = null;
                Session["ComplainantList"] = null;
                Session["voilationList"] = null;                         
        }
        /// <summary>
        /// This method populates the grid view
        /// </summary>
        private void PopulateGrid(int pageNo)
        {                         
                int maximumRows =50;
                Int64 rowsCount=0;
                DataTable dt = MergeData(pageNo, maximumRows,ref rowsCount);
                DataView dv = new DataView(dt);
                ViewState["gridData"]= dt;
                gvComplaints.DataSource = dv;
                gvComplaints.DataBind();            
                TotalPages = Convert.ToInt32(rowsCount);               
                Pager1.TotalResults = this.TotalPages.Value;
                Pager1.BindPager();            
        }
               
        /// <summary>
        /// This method loads the DS249 complaints
        /// </summary>
        private void LoadComplaints(int pageNo, int maximumRows,ref Int64 rowsCount)
        {
            complaintList = new ComplaintList();
            complaintList.Load(RequestSource.DS249, pageNo, maximumRows,ref rowsCount);            
        }
        /// <summary>
        /// This method loads the Personnels info
        /// </summary>
        private void LoadPersonnels()
        {           
                personnelList = new PersonnelList();
                personnelList.Load(true);           
        }
        
        /// <summary>
        /// This method bind the data from the DTO to the data table
        /// </summary>
        /// <returns></returns>       
        public DataTable MergeData(int pageNo, int maximumRows,ref Int64 rowsCount)
        {           
                LoadComplaints(pageNo, maximumRows, ref rowsCount);
                DataTable dt = new DataTable();
                dt.Columns.Add("CreatedDate", Type.GetType("System.DateTime"));
                dt.Columns.Add("ComplaintID", Type.GetType("System.Int64"));
                dt.Columns.Add("IndexNo", Type.GetType("System.Int64"));
                dt.Columns.Add("RoutingLocation", Type.GetType("System.String"));
                dt.Columns.Add("ComplaintStatus", Type.GetType("System.Int16"));
                dt.Columns.Add("LocationName", Type.GetType("System.String"));
                dt.Columns.Add("FirstName", Type.GetType("System.String"));
                dt.Columns.Add("LastName", Type.GetType("System.String"));
                dt.Columns.Add("InitialAndLastName", Type.GetType("System.String"));
                dt.Columns.Add("RefNo", Type.GetType("System.String"));
                dt.Columns.Add("CreatedBy", Type.GetType("System.String"));
                dt.Columns.Add("Charge", Type.GetType("System.String"));
                dt.Columns.Add("ChargeDesc", Type.GetType("System.String"));
                dt.Columns.Add("Indicator", Type.GetType("System.String"));
                dt.Columns.Add("Jt1",Type.GetType("System.String"));
                DataRow dr;
                foreach (Complaint complaint in complaintList.List)
                {
                    if (complaint.IsDeleted == false)
                    {
                        dr = dt.NewRow();
                        dr["CreatedDate"] = complaint.CreatedDate.ToShortDateString();
                        dr["ComplaintID"] = complaint.ComplaintId.ToString();
                        dr["IndexNo"] = complaint.IndexNo.ToString();
                        dr["RoutingLocation"] = complaint.RoutingLocationDescription;
                        dr["ComplaintStatus"] = complaint.ComplaintStatus;
                        dr["LocationName"] = complaint.LocationName;
                        dr["RefNo"] = complaint.ReferenceNumber;
                        dr["CreatedBy"] = complaint.CreatedBy;
                        dr["Charge"] = complaint.Charge;
                        dr["ChargeDesc"] = complaint.ChargeDescription;
                        dr["InitialAndLastName"] = complaint.InitialAndLastName.Trim();
                        dr["FirstName"] = complaint.RespondentFirstName.Trim();
                        dr["LastName"] = complaint.RespondentLastName.Trim();
                        dr["Indicator"] = complaint.Indicator.Trim();
                        dr["Jt1"] = complaint.Jt1.Trim();
                        dt.Rows.Add(dr);
                    }
                }                  
                    return dt;      
        }

        /// <summary>
        ///  Populates the data to the data table
        /// </summary>
        /// <returns></returns>
        private DataTable PopulateTable(int pageNo)
        {           
                DataTable dt;
                Int64 rowsCount=0;
                int maximumRows = Convert.ToInt32(ConfigurationManager.AppSettings["MaximumRows"]);
                dt = MergeData(pageNo, maximumRows,ref rowsCount);
                return dt;           
        }              

        /// <summary>
        /// Returns true if the search is sucessfull
        /// </summary>
        /// <param name="Search"></param>
        private void SearchComplaints(bool Search, int pageNo)
        {                          
                int maximumRows = 50;
                int numericValue = 0;
                string decimalValue = "";
                string dateValue = "";
                string stringValue = "";                
                bool isDecimal;
                decimal bal;
                isDecimal = decimal.TryParse(txtSearch.Text.Trim(), out bal);                 
                if (IsNumeric(txtSearch.Text.Trim()))
                {
                    numericValue = Convert.ToInt32(txtSearch.Text.Trim());                 
                }
                else if (isDecimal)
                {
                    decimalValue = txtSearch.Text.Trim();                    
                }
                else if (IsDate(txtSearch.Text.Trim()))
                {
                    DateTime myDateTime = DateTime.Parse(txtSearch.Text.Trim());
                    dateValue = Convert.ToString(myDateTime);
                    dateValue = dateValue.Substring(0, dateValue.IndexOf(" "));

                    if (dateValue.Substring(0, dateValue.IndexOf("/")).Length ==1)
                    {
                        dateValue = dateValue.Insert(0, "0");
                    }
                    string strTemp1 = dateValue;
                    strTemp1 = strTemp1.Remove(0, dateValue.IndexOf("/") + 1);
                    if (strTemp1.Substring(0, strTemp1.IndexOf("/")).Length == 1)
                    {
                        dateValue = dateValue.Insert(dateValue.IndexOf("/")+1, "0");
                    }
                }
                else
                {
                    stringValue = txtSearch.Text.Trim();
                    if (stringValue.Contains("'") == true || stringValue.Contains(" "))
                    {
                        stringValue = stringValue.Replace("'", "");
                        stringValue = stringValue.Replace(" ", "");
                    }
                }
                ComplaintList complaintList = new ComplaintList();
                Int32 rowCount=0;
                List<Complaint> objComplaintList = complaintList.LoadSearchComplaintList(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue, ref rowCount);
                DataTable dt = MergeSearchData(objComplaintList);
                DataView dv;
                dv = new DataView(dt);
                ViewState["gridData"] = dt;
                gvComplaints.DataSource = dv;
                gvComplaints.DataBind();
                TotalPages = rowCount;              
                if (TotalPages == 0)
                {
                    Pager1.Visible = false;                   
                }
                else
                {
                    Pager1.Visible = true;
                }
                Pager1.TotalResults = this.TotalPages.Value;
                Pager1.BindPager();                           
        }

        /// <summary>
        /// This method bind the data from the DTO to the data table
        /// </summary>
        /// <returns></returns>       
        public DataTable MergeSearchData(List<Complaint> complaintList)
        {
           
                DataTable dt = new DataTable();
                dt.Columns.Add("CreatedDate", Type.GetType("System.DateTime"));
                dt.Columns.Add("ComplaintID", Type.GetType("System.Int64"));
                dt.Columns.Add("IndexNo", Type.GetType("System.Int64"));
                dt.Columns.Add("RoutingLocation", Type.GetType("System.String"));
                dt.Columns.Add("ComplaintStatus", Type.GetType("System.Int16"));
                dt.Columns.Add("LocationName", Type.GetType("System.String"));
                dt.Columns.Add("FirstName", Type.GetType("System.String"));
                dt.Columns.Add("LastName", Type.GetType("System.String"));
                dt.Columns.Add("InitialAndLastName", Type.GetType("System.String"));
                dt.Columns.Add("RefNo", Type.GetType("System.String"));
                dt.Columns.Add("CreatedBy", Type.GetType("System.String"));
                dt.Columns.Add("Charge", Type.GetType("System.String"));
                dt.Columns.Add("ChargeDesc", Type.GetType("System.String"));
                dt.Columns.Add("Indicator", Type.GetType("System.String"));
                dt.Columns.Add("Jt1", Type.GetType("System.String"));

                DataRow dr;

                foreach (Complaint complaint in complaintList)
                {
                    if (complaint.IsDeleted == false)
                    {
                        dr = dt.NewRow();
                        dr["CreatedDate"] = complaint.CreatedDate.ToShortDateString();
                        dr["ComplaintID"] = complaint.ComplaintId.ToString();
                        dr["IndexNo"] = complaint.IndexNo.ToString();
                        dr["RoutingLocation"] = complaint.RoutingLocationDescription;
                        dr["ComplaintStatus"] = complaint.ComplaintStatus;
                        dr["LocationName"] = complaint.LocationName;
                        dr["RefNo"] = complaint.ReferenceNumber;
                        dr["CreatedBy"] = complaint.CreatedBy;
                        dr["Charge"] = complaint.Charge;
                        dr["ChargeDesc"] = complaint.ChargeDescription;
                        dr["InitialAndLastName"] = complaint.InitialAndLastName.Trim();
                        dr["FirstName"] = complaint.RespondentFirstName.Trim();
                        dr["LastName"] = complaint.RespondentLastName.Trim();
                        dr["Indicator"] = complaint.Indicator.Trim();
                        dr["Jt1"] = complaint.Jt1.Trim();
                        dt.Rows.Add(dr);
                    }
                }                
                return dt;
            
        }


        /// <summary>
        /// This method returns true if the search text entered is numeric
        /// </summary>
        /// <param name="strTextEntry"></param>
        /// <returns></returns>
        private bool IsNumeric(string strTextEntry)
        {            
                Regex objNotWholePattern = new Regex("[^0-9]");
                return !objNotWholePattern.IsMatch(strTextEntry);           
        }
        /// <summary>
        /// This method returns true if the search text entered is a valid date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public bool IsDate(string strDate)
        {              
                return ClassHierarchy.Utilities.CommonFormats.IsDate(strDate);
        }

        #endregion

        protected void btnNew_Click(object sender, ImageClickEventArgs e)
        {
            Session["upLoadDTO"] = null;
            Session.Remove("upLoadDTO");
            Session["Mode"] = "NEW";
            Response.Redirect("CaseManagement.aspx");

        }
        /// <summary>
        /// This is a button click event that fires when the user hits on the refresh button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRef_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = String.Empty;
            Pager1.CurrentPage = gvComplaints.PageIndex;
            PopulateGrid(gvComplaints.PageIndex);
            lbMessage.Text = "Data Refreshed";
        }   
        

        /// <summary>
        /// This is a button click event that fires when the user enters the search criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            if ((!String.IsNullOrEmpty(Pager1.txtJumptoPage.Text)) && (String.IsNullOrEmpty(txtSearch.Text)))
            {
                if ((Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()) > 0) && (Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()) <= ((this.TotalPages / 50) + 1)))
                {
                    Pager1.CurrentPage = Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim());
                    PopulateGrid(Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()));
                    Pager1.txtJumptoPage.Text = "";
                }
                else
                {
                    lbMessage.Text = "Invalid Page Index Entered.";
                }
            }
            else if ((!String.IsNullOrEmpty(Pager1.txtJumptoPage.Text)) && (!String.IsNullOrEmpty(txtSearch.Text)))
            {
                if ((Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()) > 0) && (Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()) <= ((this.TotalPages / 50) + 1)))
                {
                    Pager1.CurrentPage = Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim());
                    SearchComplaints(true, Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()));
                }
                else
                {
                    lbMessage.Text = "Invalid Page Index Entered.";
                }
            }
            else
            {

                if (txtSearch.Text.Length > 0)
                {
                    Pager1.CurrentPage = 0;
                    SearchComplaints(true, 0);
                }
                else
                {
                    Pager1.CurrentPage = gvComplaints.PageIndex;
                    PopulateGrid(gvComplaints.PageIndex);
                }
            }
        }
        /// <summary>
        /// This is a button click event that fires when the user hits on the cancel button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchCancel_Click(object sender, ImageClickEventArgs e)
        {           
                txtSearch.Text = String.Empty;           
        }

        #region GridView Events
        /// <summary>
        /// The RowCommand event is raised when a button is clicked in the GridView control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_RowCommand(object sender, GridViewCommandEventArgs e)
        {            
                if (e.CommandName == "Edit")
                {
                    String URL = String.Format("CaseManagement.aspx?Id={0}", e.CommandArgument.ToString());
                    Session["Mode"] = "EDIT";
                    Response.Redirect(URL);
                }

                if (e.CommandName == "Route")
                {
                    String URL = String.Format("RouteComplaint.aspx?Id={0}", e.CommandArgument.ToString());
                    Session["Mode"] = "EDIT";
                    Response.Redirect(URL);
                }

                if (e.CommandName == "View")
                {
                    String URL = String.Format("ViewComplaintDetails.aspx?Id={0}&Mode=CLOSE", e.CommandArgument.ToString());
                    Response.Redirect(URL);
                }            

        }

        /// <summary>
        /// This event is fired when a data row is bound to create a new row based on the membership status.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Pager)   
            {
                e.Row.Visible = true;        
            }
            
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                PopupControlExtender pce = e.Row.FindControl("pceStatus") as PopupControlExtender;

                string behaviorID = string.Concat("pce", e.Row.RowIndex);
                pce.BehaviorID = behaviorID;

                // replaced wih CSS
                //Image i = (Image)e.Row.Cells[1].FindControl("imgStatus");

                //string OnMouseOverScript = string.Format("$find('{0}').showPopup();this.ImageUrl='~/includes/img/ds249_mini_+_roll_btn.png'", behaviorID);
                //string OnMouseOutScript = string.Format("$find('{0}').hidePopup(); this.ImageUrl='~/includes/img/ds249_mini_+_btn.png'", behaviorID);

                //i.Attributes.Add("onmouseover", OnMouseOverScript);
                //i.Attributes.Add("onmouseout", OnMouseOutScript);                
            }           
        }
        
        /// <summary>
        /// This event is fired when a data row is bound to data in a GridView control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[9].Visible = false;
                    e.Row.Cells[12].Visible = false;
                    e.Row.Cells[13].Visible = false;
                    e.Row.Cells[15].Visible = false;
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int DaysDiff = -1;
                    DaysDiff = Convert.ToInt16(e.Row.Cells[13].Text);

                    

                    Image Charges = (Image)e.Row.FindControl("imgStatus");
                    Image status = (Image)e.Row.FindControl("imgAlert");
                    String routingLocation = e.Row.Cells[12].Text.ToString();

                    if (routingLocation == string.Empty)
                    {
                        if (DaysDiff <= 1)
                        {
                            status.Visible = false;
                        }
                        else if ((DaysDiff >= 2 && DaysDiff <= 4))
                        {
                            status.ImageUrl = "~/includes/img/AlertYellow.png";
                        }
                        else if (DaysDiff >= 5)
                        {
                            status.ImageUrl = "~/includes/img/AlertRed.png";
                        }
                        else
                        {

                            status.Visible = false;
                        }
                    }
                    else
                    {
                        status.Visible = false;
                    }

                    String district = e.Row.Cells[5].Text.Trim();
                    if (e.Row.Cells[5].Text != String.Empty)
                    {
                        if (e.Row.Cells[5].Text.Length > 20)
                        {
                            e.Row.Cells[5].Text = e.Row.Cells[5].Text.Substring(0, 17) + "...";
                            e.Row.Cells[5].Attributes.Add("title", district);
                        }
                    }


                    Label lblCharge = (Label)e.Row.FindControl("lbCharge");

                    Boolean display = false;
                    String charge = Common.GetStringValue(e.Row.Cells[9].Text, 2, ref display);
                    if (display)
                    {
                        lblCharge.Text = charge.ToString();
                        e.Row.FindControl("imgStatus").Visible = true;
                    }
                    else
                    {
                        lblCharge.Text = charge.ToString();
                        e.Row.FindControl("imgStatus").Visible = false;
                    }
                    e.Row.Cells[9].Visible = false;

                    ImageButton vbtn = (ImageButton)e.Row.FindControl("ViewButton");
                    String URL = String.Format("return OpenModalWindow('ViewComplaintDetails.aspx?ID={0}&Mode=VIEW');", e.Row.Cells[15].Text);
                    vbtn.Attributes.Add("onclick", URL);
                    switch (e.Row.Cells[3].Text)
                    {
                        case "1":
                            if (User.IsInRole("Write"))
                                e.Row.FindControl("EditButton").Visible = true;
                            else
                                e.Row.FindControl("ViewButton").Visible = true;
                            e.Row.FindControl("PrintButton").Visible = false;

                            break;
                        case "2":
                            if (User.IsInRole("DistrictApprover"))
                                e.Row.FindControl("EditButton").Visible = true;

                            else
                                e.Row.FindControl("ViewButton").Visible = true;
                            e.Row.FindControl("PrintButton").Visible = false;
                            break;
                        case "3":
                            if (User.IsInRole("BoroApprover"))
                                e.Row.FindControl("EditButton").Visible = true;
                            else
                                e.Row.FindControl("ViewButton").Visible = true;
                            e.Row.FindControl("PrintButton").Visible = false;
                            break;
                        case "4":
                            if (User.IsInRole("BoroApprover") && e.Row.Cells[12].Text.ToString().Trim() != "Pending")
                            {
                                e.Row.FindControl("PrintButton").Visible = true;
                            }
                            else if (User.IsInRole("BoroApprover") && e.Row.Cells[12].Text.ToString().Trim() == "Pending")
                            {
                                e.Row.FindControl("ViewButton").Visible = false;
                                e.Row.FindControl("EditButton").Visible = true;
                            }
                            else
                            {
                                e.Row.FindControl("ViewButton").Visible = true;
                                e.Row.FindControl("EditButton").Visible = false;
                            }
                            break;
                        case "5":
                            e.Row.FindControl("EditButton").Visible = false;
                            e.Row.FindControl("PrintButton").Visible = false;
                            e.Row.FindControl("ViewButton").Visible = true;
                            break;
                        default:
                            break;
                    }


                    if (e.Row.Cells[3].Text != null)
                    {

                        ImageButton Edit = (ImageButton)e.Row.FindControl("EditButton");
                        ImageButton View = (ImageButton)e.Row.FindControl("ViewButton");
                        ImageButton Print = (ImageButton)e.Row.FindControl("PrintButton");

                        Edit.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_edit_roll.png'");
                        Edit.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_edit_static.png'");
                        Edit.ToolTip = "Edit";

                        View.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_view_roll.png'");
                        View.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_view_static.png'");
                        View.ToolTip = "View";

                        Print.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_dash_route_roll.png'");
                        Print.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_dash_route_static.png'");
                    }
                    e.Row.Cells[3].Visible = false;
                    String author = e.Row.Cells[11].Text.ToString();
                    if (author.Length > 15)
                    {
                        e.Row.Cells[11].Text = author.Substring(0, 12) + "...";
                        e.Row.Cells[11].Attributes.Add("title", author);
                    }


                    Image route = (Image)e.Row.FindControl("imgRoute");
                    
                    switch (routingLocation.ToUpper())
                    {
                        case "ADVOCATE":
                            route.ImageUrl = "~/includes/img/ds249_mini_advocate_btn.png";
                            route.ToolTip = routingLocation;
                            break;
                        case "BCAD":
                            route.ImageUrl = "~/includes/img/ds249_mini_bcad_btn.png";
                            route.ToolTip = routingLocation;
                            break;
                        case "MEDICAL":
                            route.ImageUrl = "~/includes/img/ds249_mini_medical_btn.png";
                            route.ToolTip = routingLocation;
                            break;

                        //************PENDING DOCUMENT************
                        case "PENDING":
                            route.ImageUrl = "~/includes/img/ds249_PendingDocs.PNG";
                            route.ToolTip = "Documents are pending to be uploaded.";
                            break;

                        default:
                            route.Visible = false;
                            break;
                    }

                    e.Row.Cells[12].Visible = false;
                    e.Row.Cells[13].Visible = false;
                    e.Row.Cells[15].Visible = false;
                }           
        }
        /// <summary>
        /// This event Occurs when the hyperlink to sort a column is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_Sorting(object sender, GridViewSortEventArgs e)
        {           

                string sortExpression = e.SortExpression;
                if (GridViewSortDirection == SortDirection.Descending)
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, ASCENDING);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, DESCENDING);
                }          

        }


        /// <summary>
        /// This method sorts the data from the dataview based on the search criteria
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="direction"></param>
        private void SortGridView(string sortExpression, string direction)
        {
            DataView dv = new DataView((DataTable)ViewState["gridData"])
            {
                Sort = sortExpression + direction
            };
            gvComplaints.DataSource = dv;
                gvComplaints.DataBind();           
        }          

       

        /// <summary>
        /// In this event, For each GridView row is databound, the GridView's RowDataBound event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_DataBound(object sender, EventArgs e)
        {

            

        }
      
        #endregion


        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }
        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton2_Click(object sender, ImageClickEventArgs e)
        {
            //Session["DT"] = null;            
            Response.Redirect("../BCAD/Dashboard.aspx");
        }
        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ImageButton3_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Advocate/Dashboard.aspx");
        }
              
        public void txtSearch_TextChanged(object sender, EventArgs e)
        {
            Pager1.txtJumptoPage.Text = "";
        }
   
    }
}
