﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Collections;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.DS249
{

    /// <summary>
    /// This class provides the functionality for the common used methods in DS249 application
    /// </summary>
    public class Common
    {

        public Common()
        {

        }

        /// <summary>
        /// This method returns the charges from the search string
        /// </summary>
        /// <param name="prefixText"></param>
        /// <param name="count"></param>
        /// <returns></returns>.
        public static string[] GetCharges(string prefixText, int count)
        {
            try
            {
                ArrayList arrCharge = new ArrayList();
                ChargesList cList = new ChargesList();
                cList.Load(prefixText);

                if (cList.ChargeList.Count > 0)
                {
                    foreach (Charge charge in cList.ChargeList)
                    {
                        arrCharge.Add(charge.ChargeCode + " " + charge.ChargeDescription);
                    }
                }
                return (string[])arrCharge.ToArray(typeof(string));
            }
            catch (Exception)
            {
                return null;
            }

        }

        /// <summary>
        /// This method redirects the user to there respective page based on there group(BCAD/Advocate/DS249) 
        /// </summary>
        public static void RedirectToDefaultPage()
        {
            User user = (User)HttpContext.Current.Session["clsUser"];
            if (user.IsTempPassword == true)
                HttpContext.Current.Response.Redirect("~/UI/ResetPassword.aspx");
            else if (user.IsInMemberShip(GroupName.DS249))
                HttpContext.Current.Response.Redirect("~/Complaints/Dashboard.aspx");
            else if (user.IsInMemberShip(GroupName.ADVOCATE))
            {
                user.CurrentMembership = GroupName.ADVOCATE;
                HttpContext.Current.Response.Redirect("~/Advocate/Dashboard.aspx");
            }
            else if (user.IsInMemberShip(GroupName.BCAD))
                HttpContext.Current.Response.Redirect("~/BCAD/Dashboard.aspx");
            else if (user.IsInMemberShip(GroupName.MEDICAL))
                HttpContext.Current.Response.Redirect("~/Medical/Dashboard.aspx");
            else if (user.IsInMemberShip(GroupName.EMPLOYEE))
                HttpContext.Current.Response.Redirect("~/EmployeeApp/Dashboard.aspx");


            else
                HttpContext.Current.Response.Redirect("~/UI/UnAuthorized.aspx");
        }

        /// <summary>
        /// This method returns the number from the string expression
        /// </summary>
        /// <param name="expr"></param>
        /// <returns></returns>
        public static string ExtractNumbers(string expr)
        {
            int start = expr.LastIndexOf(" ");
            int length = expr.Length - start;
            if (start != -1)
                return expr.Substring(start, length).Trim();
            else
                return string.Empty;
        }

        /// <summary>
        /// This method returns true if the search text entered is numeric
        /// </summary>
        /// <param name="strTextEntry"></param>
        /// <returns></returns>
        private bool IsNumeric(string strTextEntry)
        {
            Regex objNotWholePattern = new Regex("[^0-9]");
            return !objNotWholePattern.IsMatch(strTextEntry);
        }
        /// <summary>
        ///  This method returns true if the search text entered is a valid date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public static bool IsDate(string strDate)
        {
            bool blnIsDate;
            blnIsDate = false;
            try
            {
                DateTime myDateTime = DateTime.Parse(strDate);
                blnIsDate = true;
            }
            catch { }
            return (blnIsDate);
        }
        /// <summary>
        /// This method string based on the search text entered
        /// </summary>
        /// <param name="str"></param>
        /// <param name="characterToCount"></param>
        /// <param name="Display"></param>
        /// <returns></returns>
        public static String GetStringValue(String str, int characterToCount, ref Boolean Display)
        {

            String[] aryList;
            String[] separator = { "," };
            try
            {
                aryList = str.Split(separator, StringSplitOptions.None);
                String result = String.Empty;
                if (aryList.Length > characterToCount)
                {
                    for (int i = 0; i < characterToCount; i++)
                    {
                        result += aryList[i].ToString() + ", ";
                    }
                    Display = true;
                    return result;
                }
                else
                    return str;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}