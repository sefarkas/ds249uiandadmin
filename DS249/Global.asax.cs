﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using DSNY.DSNYCP.ClassHierarchy;
//using DSNY.DSNYCP.DAL;

namespace DSNY.DSNYCP.DS249
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
         System.Diagnostics.Debug.Print("anything");
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.Print("anything");

        }
                    
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.Print("anything");

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            System.Diagnostics.Debug.Print("anything");

        }

        protected void Application_Error(object sender, EventArgs e)
        {


            AppError err = new AppError();
            Exception ex = Server.GetLastError();
            Exception exTemp = ex;
            int levCnt = 1;
            while (exTemp != null)
            {
                err.ErrorMessage += " Level " + levCnt.ToString() + "==> " + exTemp.Message;
                err.StackTrace += " Level " + levCnt.ToString() + "==> " + exTemp.StackTrace;
                levCnt++;
                exTemp = exTemp.InnerException;
            }
            err.RequestType = Request.RequestType;
            err.Url = Request.RawUrl;
            err.UserID = User.Identity.Name;
           // ClearSessions();           
            //Session["errorID"] = err.SaveErrorLog();
        }

           
        //private void ClearSessions()
        //{          
        //        int count = HttpContext.Current.Session.Keys.Count;
        //        for (int index = count - 1; index > -1; index--)
        //        {
        //            string key = Session.Keys[index];
        //            //Do not clear user sessions
        //            if (string.Compare(key.Trim(), "UserRoles", true) != 0
        //                && string.Compare(key.Trim(), "IsAuthenticated", true) != 0
        //                && string.Compare(key.Trim(), "UserId", true) != 0
        //                && string.Compare(key.Trim(), "UserEmpID", true) != 0
        //                && string.Compare(key.Trim(), "User", true) != 0)
        //            {
        //                Session.Remove(key);
        //            }
        //        }          
        //}

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}