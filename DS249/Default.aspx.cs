﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// This class provides a basic functionality to redirect the user to the login page
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class _Default : System.Web.UI.Page
    {
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/UI/Login.aspx");
        }
    }
}