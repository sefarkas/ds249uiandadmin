ALTER FUNCTION [dbo].[fnGetDisposition]
(
	-- Add the parameters for the function here
	@IndexNo bigint
)
RETURNS varchar(1000)
AS
BEGIN
	
	Begin Tran	
	Begin Try
	-- Declare the return variable here
	
	DECLARE @tblDisposition	 table
	(
		disposition varchar(1000)
	)
	
	
	-- Add the T-SQL statements to compute the return value here
	Insert into @tblDisposition
	select  Case when b.Code_Name ='' OR b.Code_Name IS null then ''

                              when b.Code_Category_Id=9 and b.Code_Name = 'Suspension' and a.SuspensionDays>0 then CAST(a.SuspensionDays AS varchar) + ' '+ 'Days Suspension'

                              when b.Code_Category_Id=9 and b.Code_Name = 'Vacation' and a.FineDays >0 and a.DataValue>0 then CAST(a.FineDays AS varchar) + ' '+ 'Days Vacation, ' + CAST(a.DataValue AS varchar) + ' DataValue'

                              when b.Code_Category_Id=9 and b.Code_Name = 'Vacation' and a.FineDays >0 and (a.DataValue=0 OR a.DataValue is null) then CAST(a.FineDays AS varchar) + ' '+ 'Days Vacation'

                              when b.Code_Category_Id=9 and b.Code_Name = 'Vacation' and a.DataValue >0 and (a.FineDays=0 OR a.FineDays is null) then CAST(a.DataValue AS varchar) + ' DataValue'

                              when b.Code_Category_Id=9 and b.Code_Name = 'Fined' and a.FineDays >0 then CAST(a.FineDays AS varchar) + ' Fine Days'

                              when b.Code_Category_Id=9 and b.Code_Name = 'Reprim' then 'Reprim'

                              when b.Code_Category_Id=20 and b.Code_Name = 'REPRIMAND' Then 'REPRIMAND'

                              when b.Code_Category_Id=20 and b.Code_Name = 'SUSPENSION' 

                                    and a.Days <>'' and a.Hours<>'' and a.Minutes <>''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins SUSPENSION'

                              when b.Code_Category_Id=20 and b.Code_Name = 'SUSPENSION'   

                                    and a.Days <>'' and a.Hours<>'' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours SUSPENSION'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'SUSPENSION'   

                                    and a.Days <>'' and a.Hours='' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days SUSPENSION'    

                              when b.Code_Category_Id=20 and b.Code_Name = 'SUSPENSION'   

                                    and a.Days ='' and a.Hours<>'' and a.Minutes <>''  then CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins SUSPENSION'

                              when b.Code_Category_Id=20 and b.Code_Name = 'SUSPENSION'   

                                    and a.Days ='' and a.Hours<>'' and a.Minutes =''  then CAST(a.Hours as varchar)+ ' Hours SUSPENSION'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'SUSPENSION'   

                                    and a.Days ='' and a.Hours='' and a.Minutes <>''  then CAST(a.Minutes as varchar)+ ' Mins SUSPENSION'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'SUSPENSION'   

                                    and a.Days <>'' and a.Hours='' and a.Minutes <>''  then CAST(a.Days as varchar)+ ' Days, ' + CAST(a.Minutes as varchar)+ ' Mins SUSPENSION'  

  

                              when b.Code_Category_Id=20 and b.Code_Name = 'VACATION' 

                                    and a.Days <>'' and a.Hours<>'' and a.Minutes <>''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins VACATION'

                              when b.Code_Category_Id=20 and b.Code_Name = 'VACATION'     

                                    and a.Days <>'' and a.Hours<>'' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours VACATION'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'VACATION'     

                                    and a.Days <>'' and a.Hours='' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days VACATION'    

                              when b.Code_Category_Id=20 and b.Code_Name = 'VACATION'     

                                    and a.Days ='' and a.Hours<>'' and a.Minutes <>''  then CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins VACATION'

                              when b.Code_Category_Id=20 and b.Code_Name = 'VACATION'     

                                    and a.Days ='' and a.Hours<>'' and a.Minutes =''  then CAST(a.Hours as varchar)+ ' Hours VACATION'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'VACATION'     

                                    and a.Days ='' and a.Hours='' and a.Minutes <>''  then CAST(a.Minutes as varchar)+ ' Mins VACATION'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'VACATION'     

                                    and a.Days <>'' and a.Hours='' and a.Minutes <>''  then CAST(a.Days as varchar)+ ' Days, ' + CAST(a.Minutes as varchar)+ ' Mins VACATION'  

  

                              when b.Code_Category_Id=20 and b.Code_Name = 'HOLIDAY/CompTime' 

                                    and a.Days <>'' and a.Hours<>'' and a.Minutes <>''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins HOLIDAY/CompTime'

                              when b.Code_Category_Id=20 and b.Code_Name = 'HOLIDAY/CompTime'      

                                    and a.Days <>'' and a.Hours<>'' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours HOLIDAY/CompTime'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'HOLIDAY/CompTime'      

                                    and a.Days <>'' and a.Hours='' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days HOLIDAY/CompTime'    

                              when b.Code_Category_Id=20 and b.Code_Name = 'HOLIDAY/CompTime'      

                                    and a.Days ='' and a.Hours<>'' and a.Minutes <>''  then CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins HOLIDAY/CompTime'

                              when b.Code_Category_Id=20 and b.Code_Name = 'HOLIDAY/CompTime'      

                                    and a.Days ='' and a.Hours<>'' and a.Minutes =''  then CAST(a.Hours as varchar)+ ' Hours HOLIDAY/CompTime'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'SUSPENSION'   

                                    and a.Days ='' and a.Hours='' and a.Minutes <>''  then CAST(a.Minutes as varchar)+ ' Mins HOLIDAY/CompTime'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'HOLIDAY/CompTime'      

                                    and a.Days <>'' and a.Hours='' and a.Minutes <>''  then CAST(a.Days as varchar)+ ' Days, ' + CAST(a.Minutes as varchar)+ ' Mins HOLIDAY/CompTime'  

  

                              when b.Code_Category_Id=20 and b.Code_Name = 'CompTime' 

                                    and a.Days <>'' and a.Hours<>'' and a.Minutes <>''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins CompTime'

                              when b.Code_Category_Id=20 and b.Code_Name = 'CompTime'     

                                    and a.Days <>'' and a.Hours<>'' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours CompTime'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'CompTime'     

                                    and a.Days <>'' and a.Hours='' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days CompTime'    

                              when b.Code_Category_Id=20 and b.Code_Name = 'CompTime'     

                                    and a.Days ='' and a.Hours<>'' and a.Minutes <>''  then CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins CompTime'

                              when b.Code_Category_Id=20 and b.Code_Name = 'CompTime'     

                                    and a.Days ='' and a.Hours<>'' and a.Minutes =''  then CAST(a.Hours as varchar)+ ' Hours CompTime'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'CompTime'     

                                    and a.Days ='' and a.Hours='' and a.Minutes <>''  then CAST(a.Minutes as varchar)+ ' Mins CompTime'  

                              when b.Code_Category_Id=20 and b.Code_Name = 'CompTime'     

                                    and a.Days <>'' and a.Hours='' and a.Minutes <>''  then CAST(a.Days as varchar)+ ' Days, ' + CAST(a.Minutes as varchar)+ ' Mins CompTime'  

  

                              else '' end   as Disposition

From 

(  

   

select distinct(c.IndexNo), C.RespondentFName, C.RespondentLName, C.CreatedDate

            , Case when c.RoutingLocation = 'A' then ap.PenaltyTypeCd

                     when c.RoutingLocation = 'B' then bp.BCADPenaltyTypecd

                     else '' end as PenaltyCD

            , ap.DataValue, ap.FineDays, ap.SuspensionDays

            , bp.Days, bp.Hours, bp.Minutes

             

 

From  tblDS249Complaints C inner  join tblDS249Charges ca on c.ComplaintId = ca.ComplaintId 

            left outer join dbo.tblComplaintAdvocacy ac on ac.ComplaintId = ca.ComplaintId

            left outer join dbo.tblAdvocatedPenalty ap on ac.AdvocacyCaseId = ap.AdvocacyCaseID

            left outer join dbo.tblBCADComplaint bc on c.ComplaintId = bc.ComplaintId

            left outer join dbo.tblBCADPenalty bp on bc.BCADCaseId =bp.PenaltyGroupId

            

            

where  ac.IsAvailable is null and bc.IsAvailable is null and c.indexno = @IndexNo 

            --I.IncidentDate >= @TStart_Date and I.IncidentDate <=@TEnd_Date and ct.ReferenceNumber = @refNumber

       

)a

left outer join 

 

(select code_ID, Code_Category_ID, Code_Name from Code where Code_Category_Id in (9, 20)  ) b

on a.PenaltyCD = b.Code_Id

		DECLARE @Disposition varchar(200)
	
		Select @Disposition = isnull(@Disposition,'') + Disposition + ', '
		from @tblDisposition
		Where Disposition is not null or LTRIM(RTRIM(Disposition)) <> ''
		
		-- Return the result of the function		
		SET @Disposition = left(@Disposition,len(@Disposition)-1)
	
	RETURN REPLACE(@Disposition,', ,',',')
	
	-- Return the result of the function
	

  End Try	
  Begin Catch		
  Rollback	
  Print 'This EmployeePicsMapping didnt get executed'
  End Catch
 
 END




begin try
		begin tran
Delete from tblHideCharges where ChargeTypeId in(78,79,80,96,82,16,15)

Commit tran
end try
begin catch

	Rollback tran
	
	print error_message()
	
	
end catch

