/****** Object:  StoredProcedure [dbo].[usp_GetRoles]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetRoles]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaintsBySearch]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetBCADComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsBySearch]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetMedicalComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaints]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaints]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOpenBCADComplaints]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetOpenBCADComplaints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetOpenBCADComplaints]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeesStatus]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetEmployeesStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetEmployeesStatus]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchPersonnelList]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchPersonnelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SearchPersonnelList]
GO
/****** Object:  UserDefinedFunction [dbo].[GetBusinessDays]    Script Date: 06/25/2012 11:04:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBusinessDays]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetBusinessDays]
GO
/****** Object:  UserDefinedFunction [dbo].[GetBusinessDays]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBusinessDays]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'  
-- Select dbo.GetBusinessDays (''01/04/2009'',''01/10/2009'')  
  
  
CREATE function [dbo].[GetBusinessDays]  
(  
     @StartDate datetime,  
     @EndDate datetime  
)   
returns int  
 as  
begin  
  
  --declare @DaysBetween int  
  declare @BusinessDays int 
  set @BusinessDays = 0  
  --declare @Cnt int  
  --declare @EvalDate datetime  
  
  --select @DaysBetween = 0  
  --select @BusinessDays = 0  
  --select @Cnt=0  
  
  --select @DaysBetween = datediff(Day,@StartDate,@endDate) + 1    

  -- while @Cnt < @DaysBetween  
  --   begin  
    
		--set @EvalDate = @StartDate + @Cnt    
		--	if ((datepart(dw,@EvalDate) <> 6) and (datepart(dw,@EvalDate) <> 7))  
		--	BEGIN  
		--		 set @BusinessDays = @BusinessDays + 1  
		--	END          
		--set @Cnt = @Cnt + 1  
  --  end  
  
  
		--SET DATEFIRST  1
		SET @BusinessDays =DATEDIFF (day, @StartDate, @EndDate) - (2 * DATEDIFF(week, @StartDate, @EndDate)) - 

		CASE WHEN DATEPART(weekday, @StartDate + @@DATEFIRST) = 1 
			THEN 1 
			ELSE 0 
		END - 
		CASE WHEN DATEPART(weekday, @StartDate + @@DATEFIRST) = 1 
			THEN 1 
			ELSE 0 
		END + 1
		
		return @BusinessDays  
end  
  
  
  
  
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchPersonnelList]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchPersonnelList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[usp_SearchPersonnelList] 
	-- Add the parameters for the stored procedure here	
@SearchString varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.FirstName, a.MiddleName, a.LastName, b.EmployeeID, b.ReferenceNo
	FROM tblPersons a INNER JOIN tblEmployees b
	ON a.PersonID = b.PersonID
	WHERE a.LastName like @SearchString + ''%''
	AND b.IsActive is null
	Order by LastName, FirstName
	
		--SELECT a.FirstName, a.MiddleName, a.LastName, b.EmployeeID, b.ReferenceNo
		--FROM tblPersons a INNER JOIN tblEmployees b
		--ON a.PersonID = b.PersonID
		--WHERE a.LastName + '' ''+ a.FirstName like @SearchString + ''%''
		--AND b.IsActive = 1
	
	--and b.ReferenceNo is not null or b.ReferenceNo <> ''''
	
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetEmployeesStatus]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetEmployeesStatus]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
create PROCEDURE [dbo].[usp_GetEmployeesStatus] 
@RefNo varchar(7)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select distinct p.FirstName + '' ''  + p.LastName Name
		,e.ReferenceNo
		,s.Status
		,es.EffectiveDate
		,p.FirstName
		,p.LastName
	from 
		tblEmployees e inner join tblEmployeeStatus es on es.EmployeeID =e.EmployeeID
		inner join tblPersons p on p.PersonID = e.PersonID
		inner join tblStatus s on s.StatusID = es.StatusID
	where ReferenceNo = ISNULL(nullif(ltrim( rtrim(@RefNo)), '''') ,ReferenceNo)
	order by FirstName asc
			,LastName asc
			,EffectiveDate desc
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetOpenBCADComplaints]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetOpenBCADComplaints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

-- =============================================
-- Author:		<Shiva Lakshmi	Mahankali>
-- Create date: <05/04/2012>
-- Description:	<Get all BCAD opened complaints>
-- =============================================
create PROCEDURE [dbo].[usp_GetOpenBCADComplaints] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select  isnull(lb.LocationName, ''-'') Zone,
			l.LocationName District,
			c.IndexNo 
			,RespondentLName+ '' '' + RespondentFName+ '' '' + RespondentMName Employee_Name  
			,c.RespondentRefNumber
			,lb.LocationName
	from	tblDS249Complaints c 
			inner join tblBCADComplaint bc on (c.ComplaintId = bc.ComplaintId   and IsAvailable is null)
			inner join tblEmployees e on e.EmployeeID = c.RespondentEmpID
			inner join tblLocations l on l.LocationId = c.LocationId
			left join tblLocations lb on lb.LocationId = c.BoroughID
			left join tblBCADPenalty bp on bp.PenaltyGroupId = bc.BCADCaseId
	where 
			bc.FinalStatuscd = 96 
			and c.RoutingLocation = ''B''
			and (bp.PenaltyGroupId is null or bp.BCADPenaltyTypecd = 0)
			and isnull(c.isDeleted, 0) = 0
			and isnull(NonPenaltyResultcd,0) <> 134 -- For Void/Void
			and c.ComplaintStatus > 4
	union 

	select  isnull(lb.LocationName, ''-'') Zone
			,l.LocationName District
			,c.IndexNo 
			,RespondentLName+ '' '' + RespondentFName+ '' '' + RespondentMName Employee_Name  
			,c.RespondentRefNumber
			,lb.LocationName
					
	from	tblDS249Complaints c 
			left join tblBCADComplaint bc on (c.ComplaintId = bc.ComplaintId and  bc.IsAvailable is null)
			inner join tblEmployees e on e.EmployeeID = c.RespondentEmpID
			inner join tblLocations l on l.LocationId = c.LocationId
			left join tblLocations lb on lb.LocationId = c.BoroughID
			
	where 
			(bc.BCADCaseId is null )
			and c.RoutingLocation = ''B'' 
			and isnull(c.isDeleted, 0) = 0
			and isnull(NonPenaltyResultcd,0) <> 134 -- For Void/Void
			and c.ComplaintStatus > 4
	order by lb.LocationName 
			,l.LocationName
			,IndexNo			
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

--[usp_GetComplaintsBySearch] 10045,0, 50,0,'''',''01/02/2010'','''',64
CREATE PROCEDURE [dbo].[usp_GetComplaintsBySearch]
	-- Add the parameters for the stored procedure here
@CreatedBy int=0,
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@RowCount int OUTPUT 
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @numericStringValue varchar(50)
	DECLARE  @chargesLikeValue varchar(22)
	DECLARE @stringLikeValue varchar(52)
	DECLARE @FirstRow INT 
	DECLARE @LastRow INT
	DECLARE @count INT
	declare @FullStop char(2) = ''. ''
	declare @Blank char(1) = ''''
	
	SELECT   @chargesLikeValue = ''%'' +@chargesValue + ''%'' 
			,@stringLikeValue = ''%'' + @stringValue +  ''%'' 
			,@numericStringValue =''%'' +CAST( @numericValue as varchar) + ''%''
			,@FirstRow = (@PageNo * @maximumRows) + 1
			,@LastRow = (@PageNo * @maximumRows) + @maximumRows ;  


	
	select @count=COUNT(m.Id) 
	from tblMembershipRoles m 
	where 
		PersonalID = @CreatedBy 
		and m.RoleId = 10 --RoleID 10 = ''Commissioner Access''
	
	if @count>=1
		Begin		
		if @numericValue != 0  
				Begin					
					SELECT 
						ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
						[ComplaintId]
						,IndexNo
						,a.[LocationId]
						,a.[BoroughID]
						,a.promotiondate
						,a.LocationName
						,(case when a.complaintStatus >=4 then  a.RoutingLocation
							when a.RoutingLocation =''P'' then  a.RoutingLocation
							else null 
						end) as RoutingLocation
						,[RespondentEmpID]
						,[RespondentRefNumber]      
						,[Approved]      
						,CreatedBy      
						,CONVERT(varchar,CreatedDate,101) as CreatedDate
						--,'''' as Status
						,charges as Charges
						,Chargesdesc as ChargesDesc
						, RespondentFName
						,RespondentLName
						,A.complaintStatus
						,A.complaintStatusDate
						,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
								else 0  end) as Indicator,
						a.IsDeleted,
						a.jt1,0  as FinalStatusCD 
					into #Complaints5  
					FROM dbo.vw_ComissionerAcess a 			 
					where 
						IndexNo =@numericValue
						OR RespondentRefNumber like @numericStringValue
						or isnull(a.jt1,@Blank) like @numericStringValue
					order by a.CreatedDate desc

					SELECT  * 
					FROM  #Complaints5 
					where ROWID BETWEEN @FirstRow AND @LastRow 
					ORDER BY ROWID asc     

					SELECT @RowCount=COUNT([ComplaintId]) 
					FROM #Complaints5

					drop table #Complaints5  

				End
				else if @chargesValue != @Blank 
						Begin
							SELECT 
								ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
								[ComplaintId]
								,IndexNo
								,a.[LocationId]
								,a.[BoroughID]
								,a.promotiondate
								,a.LocationName
								,(case when a.complaintStatus >=4 then  a.RoutingLocation
										when a.RoutingLocation =''P'' then  a.RoutingLocation
										else null 
										end) as RoutingLocation
								,[RespondentEmpID]
								,[RespondentRefNumber]      
								,[Approved]      
								, CreatedBy      
								,CONVERT(varchar,CreatedDate,101) as CreatedDate
								--,@Blank as Status
								,charges as Charges
								,Chargesdesc as ChargesDesc
								, RespondentFName
								,RespondentLName
								,A.complaintStatus
								,A.complaintStatusDate
								,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
										else 0  end) as Indicator
								,a.IsDeleted
								,a.jt1
								,0  as FinalStatusCD 
							into #Complaints6  
							FROM dbo.vw_ComissionerAcess a 
							where 
								Charges like @chargesLikeValue
							order by  a.CreatedDate desc

							SELECT  * 
							FROM  #Complaints6 
							where ROWID BETWEEN @FirstRow AND @LastRow  
							ORDER BY ROWID asc  
							
							SELECT @RowCount=COUNT([ComplaintId]) 
							FROM #Complaints6
							
							drop table #Complaints6     
						End	
				 else if @complaintDate != @Blank
						Begin
							SELECT 	  
								ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
								[ComplaintId]
								,IndexNo
								,a.[LocationId]
								,a.[BoroughID]
								,a.promotiondate
								,a.LocationName
								,(case when a.complaintStatus >=4 then  a.RoutingLocation
									when a.RoutingLocation =''P'' then  a.RoutingLocation
									else null 
									end) as RoutingLocation
								,[RespondentEmpID]
								,[RespondentRefNumber]      
								,[Approved]      
								,CreatedBy      
								,CONVERT(varchar,CreatedDate,101) as CreatedDate
								--,@Blank as Status
								,charges as Charges
								,Chargesdesc as ChargesDesc
								, RespondentFName
								,RespondentLName
								,A.complaintStatus
								,A.complaintStatusDate
								,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
										else 0  end) as Indicator
								,a.IsDeleted
								,a.jt1
								,0  as FinalStatusCD 
							  into #Complaints7  
						 FROM dbo.vw_ComissionerAcess a 
						 where  cast(CreatedDate as date) = CAST(@complaintDate as date)
						 order by  a.CreatedDate desc
						 	

						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #Complaints7
						
						SELECT  * 
						FROM  #Complaints7  
						where ROWID BETWEEN @FirstRow AND @LastRow  
						ORDER BY ROWID asc   
						
						drop table #Complaints7 
												
					End	
				 else if @stringValue != @Blank
						Begin	
						   SELECT 
									ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,       
									[ComplaintId]
									,IndexNo
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,a.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation
											when a.RoutingLocation =''P'' then  a.RoutingLocation
											else null 
											end) as RoutingLocation
									,[RespondentEmpID]
									,[RespondentRefNumber]      
									,[Approved]      
									,CreatedBy      
									,CONVERT(varchar,CreatedDate,101) as CreatedDate
									--, @Blank as Status
									,charges as Charges
									,Chargesdesc as ChargesDesc
									, RespondentFName
									,RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
											else 0  end) as Indicator
									,a.IsDeleted
									,a.jt1
									,0  as FinalStatusCD 
						 into #Complaints8  
						 FROM dbo.vw_ComissionerAcess a 					 
						 where 
								RespondentFName like @stringLikeValue
								OR RespondentLName like @stringLikeValue  
								OR CreatedBy like @stringLikeValue 
								OR LocationName like @stringLikeValue 
								or isnull(a.jt1,@Blank) like @stringLikeValue
						 order by  a.CreatedDate desc
							
						SELECT  * 
						FROM  #Complaints8  
						where ROWID BETWEEN @FirstRow AND @LastRow  
						ORDER BY ROWID asc  
						
						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #Complaints8
						
						drop table #Complaints8    
						 
				   End	
			End
	Else
		Begin				
				create table #Complaints(ComplaintId bigint)     
				
				insert  into #Complaints
							exec dbo.[usp_GetComplaintsSecurity] @CreatedBy;
							
				if @numericValue != 0  
					Begin		
						SELECT 
							a.[ComplaintId]
							,a.IndexNo
							,a.[LocationId]
							,a.[BoroughID]
							,a.promotiondate
							,b.LocationName
							,(case when a.complaintStatus >=4 then  a.RoutingLocation			
									else null 
									end) as RoutingLocation
							,a.[RespondentEmpID]
							,a.[RespondentRefNumber]      
							,a.[Approved]
							,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
							,a.[CreatedDate]
							--, @Blank as Status
							,a.Charges as Charges
							,a.Chargesdesc as ChargesDesc
							, a.RespondentFName
							,a.RespondentLName
							,A.complaintStatus
							,A.complaintStatusDate
							,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end)as Indicator
							,a.IsDeleted
							,a.Jt1
							,0  as FinalStatusCD 
							,ROWID =IDENTITY(INT,1,1) 
						into #tempNumeric
						FROM 
							[dbo].[tblDS249Complaints] a 
							inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
							LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
							LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
						where 
							IndexNo = @numericValue 
							OR RespondentRefNumber like @numericStringValue 
							or jt1 like   @numericStringValue
						order by a.CreatedDate desc

						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #tempNumeric
						
						select * from #tempNumeric
						WHERE ROWID BETWEEN @FirstRow AND @LastRow
						ORDER BY  ROWID asc;
						
						drop table #Complaints						
						drop table  #tempNumeric
					End
				else if @chargesValue != @Blank 
						Begin
								SELECT 
									a.[ComplaintId]
									,a.IndexNo
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,b.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation	
									else null 
									end) as RoutingLocation
									,a.[RespondentEmpID]
									,a.[RespondentRefNumber]      
									,a.[Approved]
									,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy 							     
									,a.[CreatedDate]
									--,@Blank as [Status]
									,a.Charges as Charges
									,a.Chargesdesc as ChargesDesc
									, a.RespondentFName
									,a.RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end) as Indicator,
									a.IsDeleted,
									a.Jt1,0  as FinalStatusCD ,
									ROWID =IDENTITY(INT,1,1) 
								into #tempCharges
								FROM [dbo].[tblDS249Complaints] a 
									inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
									LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
									LEFT OUTER JOIN uv_EmployeePerson c on  a.CreatedBy = c.EmployeeID  
								where Charges like @chargesLikeValue 
								order by  a.CreatedDate desc
								SELECT @RowCount=COUNT([ComplaintId]) FROM #tempCharges

								select * from #tempCharges
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints
								drop table  #tempCharges

						End	
				else if @complaintDate != @Blank
						Begin	
								SELECT 
									a.[ComplaintId]
									,a.IndexNo
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,b.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation	
									else null 
									end) as RoutingLocation
									,a.[RespondentEmpID]
									,a.[RespondentRefNumber]      
									,a.[Approved]
									,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
									,a.[CreatedDate]
									--, @Blank as Status
									,a.Charges as Charges
									,a.Chargesdesc as ChargesDesc
									, a.RespondentFName
									,a.RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end) as Indicator
									,a.IsDeleted
									,a.Jt1
									,0  as FinalStatusCD
									,ROWID =IDENTITY(INT,1,1) 
								into #tempComplaintDate
								FROM [dbo].[tblDS249Complaints] a 
									inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
									LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
									LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
								where							
									cast(CreatedDate as date) = CAST(@complaintDate as date)
								order by  a.CreatedDate desc



								SELECT @RowCount=COUNT([ComplaintId]) 
								FROM #tempComplaintDate

								select * from #tempComplaintDate
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints 
								drop table  #tempComplaintDate

						End	
				else if @stringValue != @Blank
						Begin
								SELECT 
										a.[ComplaintId]
										,a.IndexNo
										,a.[LocationId]
										,a.[BoroughID]
										,a.promotiondate
										,b.LocationName
										,(case when a.complaintStatus >=4 then  a.RoutingLocation
			when a.RoutingLocation =''P'' then  a.RoutingLocation							
												else null 
												end) as RoutingLocation
										,a.[RespondentEmpID]
										,a.[RespondentRefNumber]      
										,a.[Approved]
										,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
										,a.[CreatedDate]
										--, @Blank as [Status]
										,a.Charges as Charges
										,a.Chargesdesc as ChargesDesc
										, a.RespondentFName
										,a.RespondentLName
										,A.complaintStatus
										,A.complaintStatusDate
										,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
												else 0
												end) as Indicator
										,a.IsDeleted
										,a.Jt1
										,0  as FinalStatusCD 
										,ROWID =IDENTITY(INT,1,1) 
								into #tempStringValue
								FROM [dbo].[tblDS249Complaints] a 
										inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
										LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
										LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
								where 
										RespondentFName like @stringLikeValue 
										OR RespondentLName like @stringLikeValue  
										--OR CreatedBy like ''%''+Convert(varchar,@stringLikeValue)+ ''%'' 
										OR LocationName like @stringLikeValue 
										or  jt1 like   @stringLikeValue
										or c.FirstName like @stringLikeValue 
										or c.LastName like @stringLikeValue 
								order by  a.CreatedDate desc

								SELECT @RowCount=COUNT([ComplaintId]) 
								FROM #tempStringValue


								select * 
								from #tempStringValue
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints
								drop table  #tempStringValue
						End	 
	END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaints]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE PROCEDURE [dbo].[usp_GetComplaints] 
	-- Add the parameters for the stored procedure here
@CreatedBy int = 0,
@PageNo int = 0,
@maximumRows int = 0,
@rowsCount int OUTPUT
AS
BEGIN
		SET NOCOUNT ON;

		DECLARE @Count int
		
		--SELECT	@Count = COUNT(*) 
		--FROM [dbo].[tblDS249Complaints] a  
		--		LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
		--		LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  

		DECLARE @FirstRow INT
				,@LastRow INT
				
		--SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,
		--		@LastRow = (@PageNo * @maximumRows) + @maximumRows;


		select	@Count=COUNT(Id) 
				,@FirstRow = (@PageNo * @maximumRows) + 1
				,@LastRow = (@PageNo * @maximumRows) + @maximumRows
		from tblMembershipRoles 
		where 
				PersonalID=@CreatedBy 
				and tblMembershipRoles.RoleId= 10 --RoleID 10 =''Commissioner Access''
  
		if @count>=1
		Begin

			SELECT @rowsCount=COUNT(ComplaintId) 
			FROM dbo.vw_ComissionerAcess

			SELECT   
					ComplaintId
					,IndexNo
					,LocationId
					,BoroughID
					,promotiondate
					,LocationName
					,RoutingLocation
					,[RespondentEmpID]
					,[RespondentRefNumber]      
					,[Approved]      
					,CreatedBy      
					,[CreatedDate]
					--, Status
					,Charges
					,ChargesDesc
					, RespondentFName
					,RespondentLName
					,complaintStatus
					,complaintStatusDate
					,Indicator,
					IsDeleted,
					ROWID, 
					jt1
					,0 as FinalStatusCD 
			FROM  dbo.vw_ComissionerAcess
			WHERE ROWID BETWEEN @FirstRow AND @LastRow
			ORDER BY ROWID asc;
			
		end
		else
		begin
			create table #Complaints(ComplaintId bigint)
			
			insert  into #Complaints
					exec dbo.[usp_GetComplaintsSecurity] @CreatedBy;

			SELECT @rowsCount=COUNT(ComplaintId) FROM #Complaints;
			
			SELECT 
					a.[ComplaintId]
					,a.IndexNo
					,a.[LocationId]
					,a.[BoroughID]
					,a.promotiondate
					,b.LocationName
					,(case when a.complaintStatus >=4 then  a.RoutingLocation
					else null 
					end) as RoutingLocation
					,a.[RespondentEmpID]
					,a.[RespondentRefNumber]      
					,a.[Approved]
					,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
					,a.[CreatedDate]
					--, '''' as ''Status''
					,a.Charges as Charges
					,a.Chargesdesc as ChargesDesc
					, a.RespondentFName
					,a.RespondentLName
					,A.complaintStatus
					,A.complaintStatusDate
					,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
					else 0
					end) as Indicator,
					a.IsDeleted,
					a.Jt1
					,0  as FinalStatusCD ,
					ROWID =IDENTITY(INT,1,1) 
			into #temp
			FROM [dbo].[tblDS249Complaints] a 
				inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
				LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
				LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
			order by a.CreatedDate desc


			select * 
			from #temp
			WHERE ROWID BETWEEN @FirstRow AND @LastRow
			ORDER BY  ROWID asc;
			
			drop table #Complaints
		end

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsBySearch]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetMedicalComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
  	
if @numericValue != 0  
  Begin
  
  SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges  
		,i.IncidentDate   		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName	
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
	
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints1  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 		
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
	    order by ComplaintStatusDate Desc	       
		SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
		FROM #MedicalComplaints1 WHERE compIndex=1)tblMedComplaints 
		WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	
	drop table #MedicalComplaints1
	  
  End
 else if @chargesValue != '''' 
  Begin
    SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges   
		,i.IncidentDate  		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints2  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (Charges like ''%''+@chargesValue+ ''%'')
	 
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
	FROM #MedicalComplaints2 where compIndex=1)tblMedComplaints 
	WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints2
	
  End
 else if @complaintDate != ''''
  Begin
     SELECT	
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate 		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints3 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0))
 
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2, * into #adf0  FROM  #MedicalComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 	select * from (select *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
   from  #adf0 where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints3  
	drop table #adf0     
	 
  End
 else if @stringValue != ''''
  Begin
      SELECT	
       ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,		
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges   
		,i.IncidentDate 	
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints4 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0) ORDER BY ComplaintId desc
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #MedicalComplaints4  where RespondentFName like ''%''+@stringValue+ ''%'' OR RespondentLName like ''%''+@stringValue+ ''%'' OR CreatedBy like ''%''+@stringValue+ ''%'' OR LocationName like ''%''+@stringValue+ ''%'' OR Status  like ''%''+@stringValue+ ''%''
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
   FROM #adf where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints4
	drop table #adf
  End 	 	
   
     
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaintsBySearch]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetBCADComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)

AS
BEGIN
	SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,
      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;        
if @numericValue != 0  
  Begin
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted 
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints1  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
	
	drop table #CadComplaints1  
	
  End	
 else if @chargesValue != '''' 
  Begin
  
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (Charges like ''%''+@chargesValue+ ''%'') 
	   
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints2  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;	
	
	drop table #CadComplaints2	    
  End
 else if @complaintDate != ''''
  Begin
  SELECT
  		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
			,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
          ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	     
 	 SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2, * into #adf0 FROM  #CadComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
		
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf0   where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc; 	 
	
	drop table #CadComplaints3
	drop table #adf0 
  End
 else if @stringValue != ''''
  Begin  
    SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges 				
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
       ,i.IncidentDate
         into #CadComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #CadComplaints4  where RespondentFName like ''%''+@stringValue+ ''%'' OR RespondentLName like ''%''+@stringValue+ ''%'' OR CreatedBy like ''%''+@stringValue+ ''%'' OR LocationName like ''%''+@stringValue+ ''%'' OR Status  like ''%''+@stringValue+ ''%''  
	
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
		
	drop table #CadComplaints4
	drop table #adf
  End  
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRoles]    Script Date: 06/25/2012 11:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Abhishek Sinha>
-- Create date: <02/18/2009>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetRoles]
	-- Add the parameters for the stored procedure here	
	@MembershipID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--Select * from tblRoles
	select r.Id
			,r.Roles 
	from tblRoles r
	inner join tblMemberShipRolesAssociation rm on rm.roleid= r.id
	where rm.MembershipID = @MembershipID
	
END


' 
END
GO



--DE Duplicate
Go

begin try
	begin tran

		--46
		update c set  
				c.RespondentApptDt = e1.AppointmentDate
				,c.RespondentDOB = p1.DOB
				,c.RespondentEmpID = e1.EmployeeID
				,c.RespondentFName = p1.FirstName
				,c.RespondentLName = p1.LastName
				,c.RespondentMName = LEFT( p1.MiddleName,1)
				,c.RespondentRefNumber = e1.ReferenceNo 
		from tblPersons p1 inner join tblPersons p2 
				on p1.FirstName = p2.FirstName
				and p1.LastName=p2.LastName
				and p1.SSN = p2.SSN
		inner join tblEmployees e1 on e1.PersonID = p1.PersonID
		inner join tblEmployees e2 on e2.PersonID = p2.PersonID
		inner join tblDS249Complaints c on c.RespondentEmpID = e2.EmployeeID			 
		where p2.DataSource = 'dataease' 
				and p1.DataSource is null

		--38 rows					
		delete from tblEmployeeAddress 
		where EmployeeId in (
				select 
					e2.EmployeeID
				from tblPersons p1 inner join tblPersons p2 
					on p1.FirstName = p2.FirstName
						and p1.LastName=p2.LastName
						and p1.SSN = p2.SSN
					 inner join tblEmployees e1 on e1.PersonID = p1.PersonID
					 inner join tblEmployees e2 on e2.PersonID = p2.PersonID
					 inner join tblEmployeeAddress a on a.EmployeeId = e2.EmployeeID
				 where 
					p2.DataSource = 'dataease' 
					and p1.DataSource is null)
					 
		--38
		delete from tblEmployeeLocations 
		where EmployeeLocationID in (			
				select 
					EmployeeLocationID
				from tblPersons p1 inner join tblPersons p2 
						on p1.FirstName = p2.FirstName
							 and p1.LastName=p2.LastName
							 and p1.SSN = p2.SSN
						inner join tblEmployees e1 on e1.PersonID = p1.PersonID
						inner join tblEmployees e2 on e2.PersonID = p2.PersonID		 
						inner join tblEmployeeLocations   a on a.EmployeeId = e2.EmployeeID
				 where p2.DataSource = 'dataease' 
						and p1.DataSource is null)
					 
		--38	
		delete from tblEmployeeBadges 
		where EmployeeBadgeID in (
				select 
					distinct b.EmployeeBadgeID
				from tblPersons p1 inner join tblPersons p2 
						on p1.FirstName = p2.FirstName
						and p1.LastName=p2.LastName
						and p1.SSN = p2.SSN
				inner join tblEmployees e1 on e1.PersonID = p1.PersonID
				inner join tblEmployees e2 on e2.PersonID = p2.PersonID			 
				inner join tblEmployeeTitles a on a.EmployeeId = e2.EmployeeID
				inner join tblEmployeeBadges b on a.EmployeeTitleID=b.EmployeeTitleID
				where p2.DataSource = 'dataease' 
						and p1.DataSource is null)	
					 
		--38					 
		delete from tblEmployeeTitles 
		where EmployeeTitleID in (
				select 
					EmployeeTitleID
				from tblPersons p1 inner join tblPersons p2 
						on p1.FirstName = p2.FirstName
						and p1.LastName=p2.LastName
						and p1.SSN = p2.SSN
						inner join tblEmployees e1 on e1.PersonID = p1.PersonID
						inner join tblEmployees e2 on e2.PersonID = p2.PersonID		
						inner join tblEmployeeTitles a on a.EmployeeId = e2.EmployeeID
				where p2.DataSource = 'dataease' 
						and p1.DataSource is null)		
		
		--39					 
		delete from tblEmployees 
		where PersonID in (
				select 
					distinct p2.PersonID 
				from tblPersons p1 inner join tblPersons p2 
						on p1.FirstName = p2.FirstName
						and p1.LastName=p2.LastName
						and p1.SSN = p2.SSN
				where p2.DataSource = 'dataease' 
				and p1.DataSource is null)
					  
		--39
		delete from tblPersons 
		where PersonID in (
				select 
					distinct p2.PersonID 
				from tblPersons p1 inner join tblPersons p2 
						on p1.FirstName = p2.FirstName
						and p1.LastName=p2.LastName
						and p1.SSN = p2.SSN
				where p2.DataSource = 'dataease' 
				and p1.DataSource is null)
	
	commit tran
end try
begin catch
	rollback tran
	print error_message()
end catch	



--Duplicate employee
Go
--select 'tblDS249Complaints', COUNT(b.EmployeeID)
--from       tblEmployees b 
--join tblPersons   c on b.PersonID=c.PersonID
----inner join tblEmployeeLocations   h on h.EmployeeID = c.PersonID
--inner join tblDS249Complaints d on  d.RespondentEmpID = b.EmployeeID
--where b.ReferenceNo in( select ReferenceNo 
--from(
--                              select COUNT(*) cnt , referenceno 
--from tblEmployees 
--                              group by ReferenceNo
--                              having COUNT(referenceno) >1
--    )aa
--    )
--    and b.IsActive = 0


--44
delete from tblEmployeeLocations where EmployeeLocationID in(
select h.EmployeeLocationID
from       tblEmployees b 
join tblPersons   c on b.PersonID=c.PersonID
inner join tblEmployeeLocations   h on h.EmployeeID = b.EmployeeID
where b.ReferenceNo in( select ReferenceNo 
from(
                              select COUNT(*) cnt , referenceno 
from tblEmployees 
                              group by ReferenceNo
                              having COUNT(referenceno) >1
    )aa
    )
    and b.IsActive = 0)
    

--6
--DElete badges before titles
delete 
from tblEmployeeBadges where EmployeeBadgeID in(
select eb.EmployeeBadgeID
from        tblEmployees b
join tblPersons   c on b.PersonID=c.PersonID
inner join tblEmployeeTitles   h on h.EmployeeID =  b.EmployeeID
inner join tblEmployeeBadges   eb on eb.EmployeeTitleID= h.EmployeeTitleID
where b.ReferenceNo in( select ReferenceNo 
from(
                              select COUNT(*) cnt , referenceno 
from tblEmployees 
                              group by ReferenceNo
                              having COUNT(referenceno) >1
    )aa
    )
    and b.IsActive = 0)

--42
delete from tblEmployeeTitles where EmployeeTitleID in (
select h.EmployeeTitleID
from        tblEmployees b 
join tblPersons   c on b.PersonID=c.PersonID
inner join tblEmployeeTitles   h on h.EmployeeID = b.EmployeeID
where b.ReferenceNo in( select ReferenceNo 
from(
                              select COUNT(*) cnt , referenceno 
from tblEmployees 
                              group by ReferenceNo
                              having COUNT(referenceno) >1
    )aa
    )
    and b.IsActive = 0)
    

  
  --42  
delete from tblEmployeeAddress where EmployeeId in(
select b.EmployeeID 
from       tblEmployees b
join tblPersons   c on b.PersonID=c.PersonID
inner join tblEmployeeAddress   h on h.EmployeeID = b.EmployeeID
where b.ReferenceNo in( select ReferenceNo 
from(
                              select COUNT(*) cnt , referenceno 
from tblEmployees 
                              group by ReferenceNo
                              having COUNT(referenceno) >1
    )aa
    )
    and b.IsActive = 0)
    


 

--43    

delete from tblEmployeeStatus where
EmployeeStatusID in(
select h.EmployeeStatusID
from       tblEmployees b 
join tblPersons   c on b.PersonID=c.PersonID
inner join tblEmployeeStatus h on h.EmployeeID = b.EmployeeID
where b.ReferenceNo in( select ReferenceNo 
from(
                              select COUNT(*) cnt , referenceno 
from tblEmployees 
                              group by ReferenceNo
                              having COUNT(referenceno) >1
    )aa
    )
    and b.IsActive = 0)
    
 --42     
delete from tblPersons where personid in (    
select  c.personid
from 
 tblEmployees b 
join tblPersons   c on b.PersonID=c.PersonID
where b.ReferenceNo in( select ReferenceNo 
from(
                              select COUNT(*) cnt , referenceno 
from tblEmployees 
                              group by ReferenceNo
                              having COUNT(referenceno) >1
    )aa
    )
    and b.IsActive = 0)
   
--42
delete from tblEmployees where EmployeeID in (    
select  b.EmployeeID
from 
 tblEmployees b 
where b.ReferenceNo in( select ReferenceNo 
from(
                              select COUNT(*) cnt , referenceno 
from tblEmployees 
                              group by ReferenceNo
                              having COUNT(referenceno) >1
    )aa
    )
    and b.IsActive = 0)


--tbl Membership association

GO



CREATE TABLE [dbo].[tblMemberShipRolesAssociation](
	[MembershipID] [int] NULL,
	[RoleID] [int] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblMemberShipRolesAssociation]  WITH CHECK ADD  CONSTRAINT [FK_tblMemberShipRolesAssociation_tblMembership] FOREIGN KEY([MembershipID])
REFERENCES [dbo].[tblMembership] ([Id])
GO

ALTER TABLE [dbo].[tblMemberShipRolesAssociation] CHECK CONSTRAINT [FK_tblMemberShipRolesAssociation_tblMembership]
GO

ALTER TABLE [dbo].[tblMemberShipRolesAssociation]  WITH CHECK ADD  CONSTRAINT [FK_tblMemberShipRolesAssociation_tblRoles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[tblRoles] ([Id])
GO

ALTER TABLE [dbo].[tblMemberShipRolesAssociation] CHECK CONSTRAINT [FK_tblMemberShipRolesAssociation_tblRoles]
GO
insert into tblMemberShipRolesAssociation 
--(MembershipID,RoleID) 
select 1,1 union
select 1,2 union
select 1,3 union
select 1,4 union
select 1,5 union
select 1,6 union
select 1,7 union
select 1,10 union
select 2,1 union
select 2,2 union
select 2,4 union
select 2,5 union
select 3,1 union
select 3,2 union
select 3,4 union
select 3,5 union
select 7,1 union
select 7,2 union
select 7,4 union
select 7,5 union
select 6,3 union
select 6,8 union
select 6,9 union
select 8,1 union
select 6,20 union

select 4,11 union
select 4,12 union
select 4,13 union

select 10,14 union
select 10,15 union
select 10,16 union
select 10,17 union
select 10,18 union
select 10,19