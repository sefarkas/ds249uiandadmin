
alter PROCEDURE usp_GetEmployeesStatus 
@RefNo varchar(7)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	select distinct p.FirstName + ' '  + p.LastName Name
		,e.ReferenceNo
		,s.Status
		,es.EffectiveDate
		,p.FirstName
		,p.LastName
	from 
		tblEmployees e inner join tblEmployeeStatus es on es.EmployeeID =e.EmployeeID
		inner join tblPersons p on p.PersonID = e.PersonID
		inner join tblStatus s on s.StatusID = es.StatusID
	where ReferenceNo like '%' +  ISNULL(nullif(ltrim( rtrim(@RefNo)), '') ,ReferenceNo)+'%'
	order by FirstName asc
			,LastName asc
			,EffectiveDate desc
END
GO

