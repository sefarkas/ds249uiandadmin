--copy "\\it\Share\MIS\USERDIRS\Shamala\Ds249\ImageIssue\PORTRAIT" folder on QA server at path "C:\temp\PORTRAIT"
--first import data from excel at location "\\it\Share\MIS\USERDIRS\Shamala\Ds249\ImageIssue\09_21_2011.xls" in DS249 database
--Rename the table created by step1 with name "tblEmpImageDataFromPranav"
--then run the following script
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmployeePics_test]') AND type in (N'U'))
DROP TABLE [dbo].[EmployeePics_test]
GO
CREATE TABLE [dbo].[EmployeePics_test](
	[ImageID] [numeric](18, 0) NOT NULL,
	[Images] [varbinary](max) NULL,
 CONSTRAINT [PK_EmployeesPics_Test] PRIMARY KEY CLUSTERED 
(
	[ImageID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

go

begin try
begin tran

--Bring all the nex images from HR data

	DECLARE @img varchar(80)
	DECLARE @insert varchar(3000)
	declare @Number int;
	declare  @ID int
	SET @ID= 1

	WHILE @ID < 1001044  --last ID + 1

	BEGIN

	SET @img = 'C:\temp\PORTRAIT\' + CONVERT(varchar,@ID) + '.jpg'
begin try
	SET @insert = N'INSERT INTO EmployeePics_test(ImageID, Images) 
	SELECT '+cast(@ID as varchar(10))+', * FROM OPENROWSET(BULK N''' + @img + ''', SINGLE_BLOB) as a'

	EXEC(@insert)
end try
begin catch 
print error_message()
end catch
	SET @ID = @ID + 1

	END

--Update all the new images in DS249 DB
update ep set  ep.Images = ept.Images from dbo.EmployeesPic ep inner join  EmployeePics_test ept
on ep.imageid = ept.imageid

	--	below are for the tif records

	--INSERT INTO [EmployeesPic_test](ImageID,Images) 
	--SELECT 30, * FROM OPENROWSET(BULK N'\\it\Share\MIS\USERDIRS\Shamala\Ds249\ImageIssue\PORTRAIT\30.tif', SINGLE_BLOB) a

	--INSERT INTO [EmployeesPic_test](ImageID,Images) 
	--SELECT 40, * FROM OPENROWSET(BULK N'\\it\Share\MIS\USERDIRS\Shamala\Ds249\ImageIssue\PORTRAIT\40.tif', SINGLE_BLOB) a

--Update incorrect image ids from HR data
	update em set em.Imageid = p.imageid from tblEmpImageDataFromPranav p inner join 
    (select ReferenceNo,em.ImageID   from dbo.EmployeePicsMapping  em
  left join tblEmpImageDataFromPranav p on employeeid = em.ReferenceNo and  p.imageid = em.ImageID
  where employeeid is null) t on p.employeeid = t.ReferenceNo
  inner join dbo.EmployeePicsMapping  em on em.ReferenceNo = t.ReferenceNo


----Insert unavailable images in DS249
insert into EmployeesPic (ImageID,Images)
select t.ImageID,t.Images from  EmployeePics_test  t left join EmployeesPic  e
on  t.ImageID= e.ImageID
where e.ImageID is null


--Insert missing images and emplyees in EmployeePicsMapping

insert into EmployeePicsMapping ( ReferenceNo,ImageID)
  select  ReferenceNo, imageid from 
  (select distinct a.ReferenceNo from (
   select distinct ReferenceNo from tblEmployees em inner join 
  tblEmpImageDataFromPranav p on p.employeeid = CAST( em.ReferenceNo as int)
  where  em.ReferenceNo not in('000000a','000000b','0000N/A','00000S')) a
  left join 
 
 ( select distinct  em.ReferenceNo from tblEmployees em inner join 
  EmployeePicsMapping p on p.ReferenceNo =CAST( em.ReferenceNo as int)
  where  em.ReferenceNo not in('000000a','000000b','0000N/A','00000S')) b
  on a.ReferenceNo = b.ReferenceNo
 where b.ReferenceNo  is null) c inner join 
  tblEmpImageDataFromPranav p on p.employeeid = c.ReferenceNo


	update em set em.FirstName = p.FirstName , em.LastName = p.LastName from EmployeePicsMapping em
inner join tblEmployees e on CAST( e.ReferenceNo as int) = em.ReferenceNo
inner join tblPersons p on e.PersonID = p.PersonID
where em.LastName is null and em.FirstName is null and em.ReferenceNo is not null
and  e.ReferenceNo not in('000000a','000000b','0000N/A','00000S')


delete from EmployeePicsMapping where ImageID in (7238,9581)

commit tran
end try
begin catch
rollback tran
print error_message()
end catch






--select distinct * from EmployeePicsMapping em
--inner join tblEmployees e on e.ReferenceNo = em.ReferenceNo

--select  ReferenceNo from EmployeePicsMapping em
--where em.LastName is null and em.FirstName is null and em.ReferenceNo is not null


--select * from EmployeePicsMapping where 
----LastName like 'scar%'
--ImageID = 8

--select 

--select * from EmployeePicsMapping where ReferenceNo in (

--select ReferenceNo from EmployeePicsMapping group by ReferenceNo having COUNT(*) >1)

----delete from EmployeePicsMapping where ImageID in (7238,9581)


--select * 
--from EmployeePicsMapping em
--inner join tblEmployees e on CAST( e.ReferenceNo as int) = em.ReferenceNo
--inner join tblPersons p on e.PersonID = p.PersonID
--and em.LastName = p.LastName and em.FirstName = p.FirstName
--and  e.ReferenceNo not in('000000a','000000b','0000N/A','00000S')