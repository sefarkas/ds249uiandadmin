
ALTER Procedure [dbo].[usp_RptGetRoutingHistory] 
  
  @IndexNo  bigint
  --= null

  As
--  if (@IndexNo is null) or (@IndexNo='')
--  begin
--select b.IndexNo,b.RespondentLName+', '+ b.RespondentFName as RespondentName,    c.LastName+', '+ c.FirstName as SentBy,a.SentDate,a.SentFrom,a.SentTo from 
--  tblRoutingHistory a 
--  left join tblPersons c on c.PersonID=a.SentBy 
--  inner join tblDS249Complaints b
--  on a.ComplaintID=b.ComplaintId
--  --where  b.IndexNo =@IndexNo
--  order by b.IndexNo desc, a.SentDate asc
--  end
--  else
  begin
  select b.IndexNo,b.RespondentLName+', '+ b.RespondentFName as RespondentName,    c.LastName+', '+ c.FirstName as SentBy,a.SentDate,a.SentFrom,a.SentTo from 
  tblRoutingHistory a 
  left join tblPersons c on c.PersonID=a.SentBy 
  inner join tblDS249Complaints b
  on a.ComplaintID=b.ComplaintId
  where  b.IndexNo =@IndexNo
  order by a.SentDate desc
  end

GO

ALTER PROCEDURE [dbo].[usp_GetAdvocateComplaints] 
	-- Add the parameters for the stored procedure here	
@PageNo int = 0,
@maximumRows int = 0,
@rowsCount int OUTPUT
AS
BEGIN

SET NOCOUNT ON;
DECLARE @Count int
Set @rowsCount = (

SELECT	COUNT(*) as AdvocateComplaintsCount
          
  FROM 
        
		 [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) left outer JOIN Code e
		On d.FinalStatusCd = e.Code_Id 	
    where (a.ComplaintStatus = 5 or IsSubmitted = '1')	
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy] t	
	where t.ComplaintId=a.ComplaintId	and IsAvailable is null	
	order by a.ComplaintStatusDate desc 
	))

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;


WITH AdvocateComplaints  AS
(

--select * into #advComplaints from
--(
SELECT    
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else a.DataSource 
		End as DataSource
		,d.AdvocacyCaseId		
		,a.ComplaintId
		,a.IndexNo as IndexNo,
		 a.ApprovedDate
		,a.RoutingLocation
		,a.[LocationId]
		,a.Jt1 		
		--,i.IncidentDate 		
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		--,ISNULL((select top 1 sentdate from tblRoutingHistory 
		--where ComplaintId=a.ComplaintId and SentTo='Advocate' order by SentDate desc ),a.ComplaintStatusDate)   
,ComplaintStatusDate
		,'false' as Approved 
		,Charges    
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName,
		a.ParentIndexNo,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         
        -- ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
        ,(select top 1 IncidentDate from tblDS249Incidents 
		where ComplaintId=a.ComplaintId   order by IncidentDate desc ) IncidentDate
		

         
  FROM 
        
		 [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) left outer JOIN Code e
		On d.FinalStatusCd = e.Code_Id 	
    where (a.ComplaintStatus = 5 or IsSubmitted = '1')	
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy] t	
	where t.ComplaintId=a.ComplaintId	and IsAvailable is null	
	order by a.ComplaintStatusDate Desc		
	)	
--) a	
	)


 select * from  AdvocateComplaints 
WHERE ROWID BETWEEN @FirstRow AND @LastRow
ORDER BY ROWID asc;


END
  
  
  
 
  
  
  