CREATE TABLE [dbo].[tblAppErrorLog](
	[ErrorID] [bigint] IDENTITY(1,1) NOT NULL,
	[Url] [nvarchar](max) NULL,
	[RequestType] [nvarchar](50) NULL,
	[UserID] [nvarchar](50) NULL,
	[ErrorMessage] [nvarchar](max) NULL,
	[StackTrace] [nvarchar](max) NULL,
	[EntryDate] [datetime] NULL,
 CONSTRAINT [PK_tblAppErrorLog] PRIMARY KEY CLUSTERED 
(
	[ErrorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblAppErrorLog] ADD  CONSTRAINT [DF_tblAppErrorLog_EntryDate]  DEFAULT (getdate()) FOR [EntryDate]
GO




CREATE procedure [dbo].[usp_SaveError] 
@Url VARCHAR(255)
,@RequestType nvarchar(50)
,@UserID nvarchar(50) = null
,@ErrorMessage nvarchar(max)
,@StackTrace nvarchar(max)
--, @AppID int
AS
BEGIN
     INSERT INTO dbo.tblAppErrorLog
           (Url
           ,RequestType
           ,UserID
           ,ErrorMessage
           ,StackTrace
           --,AppID
           )
     VALUES
           (@Url
           ,@RequestType
           ,@UserID
           ,@ErrorMessage
           ,@StackTrace
           --,@AppID
           )
           

     select @@Identity

END
GO


CREATE PROCEDURE [dbo].[usp_RptGetAnualCharges] 
AS

BEGIN

	declare @DateNow date = getdate()
	declare @LastOneYear date = getdate()-365
	
	SELECT DISTINCT 
	        comp.RespondentRefNumber,
			comp.RespondentLName,
			comp.RespondentFName, 
			loc.LocationName
			,COUNT(*) as NoOfComplaints
			
	FROM 
		(
		select * from tblDS249Charges 
		where ChargeTypeID 
		in (75,76)
		) a 
	INNER JOIN tblDS249Complaints comp on a.ComplaintId=comp.ComplaintId 
	LEFT JOIN tblLocations loc on loc.LocationId=comp.LocationId
	WHERE  CreatedDate >= @LastOneYear
	AND CreatedDate <=@DateNow 
	AND comp.isDeleted=0

	GROUP BY 
	comp.RespondentRefNumber,
	comp.RespondentLName,
	comp.RespondentFName, 
	loc.LocationName
	HAVING
	COUNT(*) >3
	ORDER BY LocationName

END

GO

CREATE PROCEDURE usp_EmployeeRank
AS

BEGIN

	SELECT   
			
			e.ReferenceNo, 
			p.FirstName,
			p.MiddleName,
			p.LastName, 
			l.LocationName,
			ISNULL(t.Title, '') Title

	FROM    dbo.tblEmployees e INNER JOIN
			dbo.tblPersons p ON e.PersonID = p.PersonID
			INNER Join dbo.tblEmployeeTitles et
			ON e.EmployeeID = et.EmployeeID
			INNER Join dbo.tblTitle t
			ON t.TitleID = et.TitleID
			INNER join tblEmployeeLocations el 
			ON el.EmployeeID=e.EmployeeID
			INNER join tblLocations l
			ON el.LocationID=l.LocationId
			INNER JOIN tblMembershipRoles mr
			ON mr.PersonalID=p.PersonID
	WHERE 
	        mr.MembershipID=1
	        and mr.RoleId=2
			and et.IsCurrent=1 
			and el.IsCurrent=1
			and e.IsActive is null 
	ORDER BY
	        p.LastName
        
END

 GO
ALTER PROCEDURE usp_GetComplaintForDayCount
@Days int
As
BEGIN
		DECLARE @DateNow date = getdate()
		SELECT * FROM (
		SELECT DISTINCT loc.LocationName
				,comp.IndexNo
				,rout.sentDate as createddate
				,comp.Charges
				,comp.RoutingLocation 
		FROM 
			(
				select * 
				from tblDS249Charges 
				where 
					ChargeTypeID in (75,76)
			) a 
			inner join tblDS249Complaints comp on a.ComplaintId=comp.ComplaintId 
			inner join tblRoutingHistory rout  on rout.ComplaintID=comp.ComplaintId
			left join tblComplaintAdvocacy ac on comp.ComplaintId=ac.ComplaintId
			left join tblLocations loc on loc.LocationId=comp.LocationId
		WHERE  
				comp.CreatedDate >=DATEADD(day,-@Days, @DateNow)
				and comp.CreatedDate <= GETDATE()
				and comp.isDeleted=0
				and comp.RoutingLocation='A' 
				and ac.FinalStatuscd=98 
		
		UNION
		
			SELECT DISTINCT loc.LocationName
				,comp.IndexNo
				,rout.sentDate as createddate
				,comp.Charges
				,comp.RoutingLocation 
		FROM 
			(
				select * 
				from tblDS249Charges 
				where 
					ChargeTypeID in (75,76)
			) a 
			inner join tblDS249Complaints comp on a.ComplaintId=comp.ComplaintId 
			inner join tblRoutingHistory rout  on rout.ComplaintID=comp.ComplaintId
			left join tblBCADComplaint bc on comp.ComplaintId=bc.ComplaintId
			left join tblMedicalComplaint mc on comp.ComplaintId=mc.ComplaintId 
			left join tblLocations loc on loc.LocationId=comp.LocationId
		WHERE  
				comp.CreatedDate >=DATEADD(day,-@Days, @DateNow)
				and comp.CreatedDate <= GETDATE()
				and comp.isDeleted=0
				and bc.ComplaintId is null
				and (comp.RoutingLocation='B' OR comp.RoutingLocation='M') 
		) temp 
		ORDER BY temp.LocationName
END
GO
insert into Code_Category values(21,'Medical-Penalty Type')
GO
ALTER FUNCTION [dbo].[fnGetDisposition]
		(
			-- Add the parameters for the function here
			@IndexNo bigint
		)
		RETURNS varchar(1000)
		AS
		BEGIN
			-- Declare the return variable here
			
			DECLARE @tblDisposition	 table
			(
				disposition varchar(1000)
			)
			
			
			-- Add the T-SQL statements to compute the return value here
			Insert into @tblDisposition
			select 
			isnull(
			 Case 
			 --Added for VOIDED
			 when  a.FinalStatuscd=97 and a.NonPenaltyResultcd='134' and a.RoutingLocation='B' then  + 'VOIDED'
			 when b.Code_Name ='' OR b.Code_Name IS null then ''
			 
									 when b.Code_Category_Id=9 and b.Code_Name = 'REPRIMAND' then  + 'REPRIMAND'

									  when b.Code_Category_Id=9 and b.Code_Name = 'SUSPENSION' and a.Term>0  then CAST(a.Term AS varchar) + ' '+ CAST(a.Term_Desc AS varchar)+ ' SUSPENSION' 

									  when b.Code_Category_Id=9 and b.Code_Name = 'Vacation'   and a.Term>0 then CAST(a.Term AS varchar) + ' '+ CAST(a.Term_Desc AS varchar)+ ' VACATION' 
		                              
									  when b.Code_Category_Id=9 and b.Code_Name = 'FINED'  and a.datavalue >0 then  'FINED $' + CAST(a.DataValue AS varchar) 
		                              
									  when b.Code_Category_Id=9 and b.Code_Name = 'FINED'  and a.Term>0 then  CAST(a.Term AS varchar) + ' '+  ' FINE ' + CAST(a.Term_Desc AS varchar)

		                              
									  when b.Code_Category_Id=9 and b.Code_Name = 'Fired' then  + 'FIRED'
		                              
									  when b.Code_Category_Id=9 and b.Code_Name = 'Demoted' then  + 'DEMOTED'
		                                                            
									  when b.Code_Category_Id=9 and b.Code_Name = 'Other' then 'OTHER - '+CAST(a.Comments as varchar(max))

		                              
									   when b.Code_Category_Id=9 and b.Code_Name = 'ACD - 6 Months' then  + 'ACD - 6 MONTHS'
		                              
									   when b.Code_Category_Id=9 and b.Code_Name = 'ACD - 12 Months' then  + 'ACD - 12 MONTHS'
		     
		                              
									   when b.Code_Category_Id=9 and b.Code_Name = 'Suspension Time Served' then  + 'SUSPENSION TIME SERVED'
		                              
		                              
		                              
									  when b.Code_Category_Id=9 and b.Code_Name = 'Comp Time'  and a.Term>0  then CAST(a.Term AS varchar) + ' '+ CAST(a.Term_Desc AS varchar)+' COMP TIME'
									  
									  when b.Code_Category_Id=9 and b.Code_Name = 'Void/Dismissed' then  + 'VOID/DISMISSED'
									  
									  when b.Code_Category_Id=9 and b.Code_Name = 'Last Chance Agreement - 24 months' then  + 'LAST CHANCE AGREEMENT - 24 MONTHS'
									  when b.Code_Category_Id=9 and b.Code_Name = 'Lifetime substance abuse testing' then  + 'LIFETIME SUBSTANCE ABUSE TETSING'

									  
									  when b.Code_Category_Id=9 and b.Code_Name = 'Commissioners Probabation'  and  a.Term>0 then CAST(a.Term AS varchar) + ' '+ CAST(a.Term_Desc AS varchar)	+ ' COMMISIONERS PROBABTION'	 
		             

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21) and b.Code_Name = 'REPRIMAND' Then 'REPRIMAND'

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'SUSPENSION' 

											and a.Days <>'' and a.Hours<>'' and a.Minutes <>''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins SUSPENSION'

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'SUSPENSION'   

											and a.Days <>'' and a.Hours<>'' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours SUSPENSION'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'SUSPENSION'   

											and a.Days <>'' and a.Hours='' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days SUSPENSION'    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'SUSPENSION'   

											and a.Days ='' and a.Hours<>'' and a.Minutes <>''  then CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins SUSPENSION'

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'SUSPENSION'   

											and a.Days ='' and a.Hours<>'' and a.Minutes =''  then CAST(a.Hours as varchar)+ ' Hours SUSPENSION'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'SUSPENSION'   

											and a.Days ='' and a.Hours='' and a.Minutes <>''  then CAST(a.Minutes as varchar)+ ' Mins SUSPENSION'  

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'SUSPENSION'   

											and a.Days <>'' and a.Hours='' and a.Minutes <>''  then CAST(a.Days as varchar)+ ' Days, ' + CAST(a.Minutes as varchar)+ ' Mins SUSPENSION'  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'VACATION' 

											and a.Days <>'' and a.Hours<>'' and a.Minutes <>''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins VACATION'

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'VACATION'     

											and a.Days <>'' and a.Hours<>'' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours VACATION'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'VACATION'     

											and a.Days <>'' and a.Hours='' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days VACATION'    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'VACATION'     

											and a.Days ='' and a.Hours<>'' and a.Minutes <>''  then CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins VACATION'

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'VACATION'     

											and a.Days ='' and a.Hours<>'' and a.Minutes =''  then CAST(a.Hours as varchar)+ ' Hours VACATION'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'VACATION'     

											and a.Days ='' and a.Hours='' and a.Minutes <>''  then CAST(a.Minutes as varchar)+ ' Mins VACATION'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'VACATION'     

											and a.Days <>'' and a.Hours='' and a.Minutes <>''  then CAST(a.Days as varchar)+ ' Days, ' + CAST(a.Minutes as varchar)+ ' Mins VACATION'  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'HOLIDAY/CompTime' 

											and a.Days <>'' and a.Hours<>'' and a.Minutes <>''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins HOLIDAY/CompTime'

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'HOLIDAY/CompTime'      

											and a.Days <>'' and a.Hours<>'' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours HOLIDAY/CompTime'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'HOLIDAY/CompTime'      

											and a.Days <>'' and a.Hours='' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days HOLIDAY/CompTime'    

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'HOLIDAY/CompTime'      

											and a.Days ='' and a.Hours<>'' and a.Minutes <>''  then CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins HOLIDAY/CompTime'

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'HOLIDAY/CompTime'      

											and a.Days ='' and a.Hours<>'' and a.Minutes =''  then CAST(a.Hours as varchar)+ ' Hours HOLIDAY/CompTime'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'SUSPENSION'   

											and a.Days ='' and a.Hours='' and a.Minutes <>''  then CAST(a.Minutes as varchar)+ ' Mins HOLIDAY/CompTime'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'HOLIDAY/CompTime'      

											and a.Days <>'' and a.Hours='' and a.Minutes <>''  then CAST(a.Days as varchar)+ ' Days, ' + CAST(a.Minutes as varchar)+ ' Mins HOLIDAY/CompTime'  

		  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'CompTime' 

											and a.Days <>'' and a.Hours<>'' and a.Minutes <>''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins CompTime'

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'CompTime'     

											and a.Days <>'' and a.Hours<>'' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days, ' + CAST(a.Hours as varchar)+ ' Hours CompTime'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'CompTime'     

											and a.Days <>'' and a.Hours='' and a.Minutes =''  then CAST(a.days as varchar)+ ' Days CompTime'    

									 when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'CompTime'     

											and a.Days ='' and a.Hours<>'' and a.Minutes <>''  then CAST(a.Hours as varchar)+ ' Hours, ' + CAST(a.Minutes as varchar)+ ' Mins CompTime'

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'CompTime'     

											and a.Days ='' and a.Hours<>'' and a.Minutes =''  then CAST(a.Hours as varchar)+ ' Hours CompTime'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'CompTime'     

											and a.Days ='' and a.Hours='' and a.Minutes <>''  then CAST(a.Minutes as varchar)+ ' Mins CompTime'  

									  when (b.Code_Category_Id=20 or b.Code_Category_Id=21)  and b.Code_Name = 'CompTime'     

											and a.Days <>'' and a.Hours='' and a.Minutes <>''  then CAST(a.Days as varchar)+ ' Days, ' + CAST(a.Minutes as varchar)+ ' Mins CompTime'  

										--Added comments to be added in the history section view model of DS249
										
										when b.Code_Category_Id=21  and b.Code_Name = 'FINED' 

											and a.MoneyValue <>'' then +'$'+CAST(a.MoneyValue as varchar)+' FINED'


									  else '' end 
									  -- Added to get comment for parent Index
									  ,'') 
									  + isnull( ' see ' + cast(nullif(a.ParentIndexNo,0) as varchar(max)) ,'')
		                              
									  as Disposition
		                               
		                              
		                              

		From 

		(  

		   

		select distinct (c.IndexNo), C.RespondentFName, C.RespondentLName, C.CreatedDate

					, Case when c.RoutingLocation = 'A' then ap.PenaltyTypeCd

							 when c.RoutingLocation = 'B' then bp.BCADPenaltyTypecd

							 else '' end as PenaltyCD

					, ap.DataValue,ap.Term,ap.Term_Desc,
					ap.comments,
					 ap.FineDays, ap.SuspensionDays,
		             
					 ap.AdvocatePenaltyID

					, bp.Days, bp.Hours, bp.Minutes,bp.MoneyValue
					-- Added to get comment for parent Index
					, isnull(C.ParentIndexNo,'') as ParentIndexNo
					--Added for VOIDED
					,bc.FinalStatuscd
					--Added for VOIDED
		            ,bc.NonPenaltyResultcd       
					--Added for VOIDED
					,c.RoutingLocation
		 

		From  tblDS249Complaints C inner  join tblDS249Charges ca on c.ComplaintId = ca.ComplaintId 

					left outer join dbo.tblComplaintAdvocacy ac on ac.ComplaintId = ca.ComplaintId

					left outer join dbo.tblAdvocatedPenalty ap on ac.AdvocacyCaseId = ap.AdvocacyCaseID

					left outer join dbo.tblBCADComplaint bc on c.ComplaintId = bc.ComplaintId

					left outer join dbo.tblBCADPenalty bp on bc.BCADCaseId =bp.PenaltyGroupId
					
					

		where  ac.IsAvailable is null  and c.indexno = @IndexNo 

		UNION

		select distinct (c.IndexNo), C.RespondentFName, C.RespondentLName, C.CreatedDate

					, Case when c.RoutingLocation = 'A' then ap.PenaltyTypeCd

							 when c.RoutingLocation = 'M' then mp.MedicalPenaltyTypecd

							 else '' end as PenaltyCD

					, ap.DataValue,ap.Term,ap.Term_Desc,
					ap.comments,
					 ap.FineDays, ap.SuspensionDays,
		             
					 ap.AdvocatePenaltyID

		            
					--The below line of code is added to display the dispositions for the medicals.
					, mp.Days, mp.Hours, mp.Minutes,mp.MoneyValue  
					-- Added to get comment for parent Index         
		, isnull(C.ParentIndexNo,'') as ParentIndexNo
		--Added for VOIDED
		,mc.FinalStatuscd
		--Added for VOIDED
		 ,mc.NonPenaltyResultcd
		--Added for VOIDED
		,c.RoutingLocation
		From  tblDS249Complaints C inner  join tblDS249Charges ca on c.ComplaintId = ca.ComplaintId 

					left outer join dbo.tblComplaintAdvocacy ac on ac.ComplaintId = ca.ComplaintId

					left outer join dbo.tblAdvocatedPenalty ap on ac.AdvocacyCaseId = ap.AdvocacyCaseID

		            
		left outer join dbo.tblMedicalComplaint mc on c.ComplaintId = mc.ComplaintId
		left outer join dbo.tblMedicalPenalty mp on mc.MedicalCaseId =mp.PenaltyGroupId
		        

		where  mc.IsAvailable is null  and c.indexno =@IndexNo
					--I.IncidentDate >= @TStart_Date and I.IncidentDate <=@TEnd_Date and ct.ReferenceNumber = @refNumber
		          

		)a

		left outer join 

		 

		(select code_ID, Code_Category_ID, Code_Name from Code where Code_Category_Id in (9, 20,21)  ) b

		on a.PenaltyCD = b.Code_Id

		order by a.AdvocatePenaltyID
				
				DECLARE @Disposition varchar(200)
			
				Select @Disposition = isnull(@Disposition,'') + Disposition + ', '
				from @tblDisposition
				Where Disposition is not null or LTRIM(RTRIM(Disposition)) <> ''
				
				--Added for VOIDED
				if  @Disposition like '%VOIDED%'
					select @Disposition='VOIDED.'

			
				SET @Disposition = left(@Disposition,len(@Disposition)-1)
			
			--RETURN REPLACE(@Disposition,', ,',',')
		--	 IF (substring(@Disposition,len(@Disposition),len(@Disposition)))=','
			
		--SET @Disposition=REPLACE(@Disposition,(substring(@Disposition,len(@Disposition),len(@Disposition))),'.')

		--ELSE IF (substring(@Disposition,len(@Disposition)+1,len(@Disposition)))=''
			
		--SET @Disposition=REPLACE(@Disposition,(substring(@Disposition,len(@Disposition),len(@Disposition))),'.')	
			
			RETURN REPLACE(@Disposition,', ,',',')	

		END
		

GO












 