 ALTER Procedure usp_RptGetAdcocateDEInfo 
  @StartDate date,
  @EndDate date
  As


Select * from
(  
SELECT 
	   d.LastName 
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then 'Open'
            when a.FinalStatuscd=97
            then 'Closed'
            when a.FinalStatuscd=98
            then 'DS249 Not Yet Recieved'
            when a.FinalStatuscd=130
            then 'Send to BCAD'
            when a.FinalStatuscd=131
            then 'Return to Author'
            when a.FinalStatuscd=132
            then 'Send to Advocate'
            else 'Not Mentioned'
            end
        as 'Case Status'
      ,cast(h.HearingDt as DATE) as 'Hearing Date'
      ,b.Charges
     ,e.Code_Name as 'DE Status',
     b.CreatedDate
  FROM [dbo].[tblComplaintAdvocacy] a
  left join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  left join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  left join Code e on a.Actioncd=e.Code_Id
  left join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  left join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Name in('VOID/VOID','Void/Redraft','Warn Letter','PERSONNEL')
  and b.CreatedDate between @StartDate and @EndDate
  
  UNION
  
SELECT 
	   d.LastName 
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then 'Open'
            when a.FinalStatuscd=97
            then 'Closed'
            when a.FinalStatuscd=98
            then 'DS249 Not Yet Recieved'
            when a.FinalStatuscd=130
            then 'Send to BCAD'
            when a.FinalStatuscd=131
            then 'Return to Author'
            when a.FinalStatuscd=132
            then 'Send to Advocate'
            else 'Not Mentioned'
            end
       as 'Case Status'
     ,cast(h.HearingDt as DATE) as 'Hearing Date'
      ,b.Charges
     ,e.Code_Name as 'DE Status',
     b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  left join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  left join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  left join Code e on a.ReviewStatuscd=e.Code_Id
  left join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  left join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Name in('HELD','COMPLETED','R/R/T WCT','DECEASED','REINSTATED')
  AND b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   d.LastName 
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then 'Open'
            when a.FinalStatuscd=97
            then 'Closed'
            when a.FinalStatuscd=98
            then 'DS249 Not Yet Recieved'
            when a.FinalStatuscd=130
            then 'Send to BCAD'
            when a.FinalStatuscd=131
            then 'Return to Author'
            when a.FinalStatuscd=132
            then 'Send to Advocate'
            else 'Not Mentioned'
            end
        as 'Case Status'
     ,cast(h.HearingDt as DATE) as 'Hearing Date'
      ,b.Charges
     ,e.Code_Name as 'DE Status',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  left join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  left join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  left join Code e on a.SpecialStatus_cd=e.Code_Id
  left join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  left join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Name in('FINAL WARN','NOTABLE','EXIC','PROB','A-CAT','B-CAT','C-CAT')
  and b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   d.LastName 
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then 'Open'
            when a.FinalStatuscd=97
            then 'Closed'
            when a.FinalStatuscd=98
            then 'DS249 Not Yet Recieved'
            when a.FinalStatuscd=130
            then 'Send to BCAD'
            when a.FinalStatuscd=131
            then 'Return to Author'
            when a.FinalStatuscd=132
            then 'Send to Advocate'
            else 'Not Mentioned'
            end
       as 'Case Status'
      ,cast(h.HearingDt as DATE) as 'Hearing Date'
      ,b.Charges
     ,e.Code_Name as 'DE Status',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  left join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  left join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  left join Code e on a.TrialReturnStatuscd=e.Code_Id
  left join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  left join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Name ='PENDING DISP'
  and b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   d.LastName 
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then 'Open'
            when a.FinalStatuscd=97
            then 'Closed'
            when a.FinalStatuscd=98
            then 'DS249 Not Yet Recieved'
            when a.FinalStatuscd=130
            then 'Send to BCAD'
            when a.FinalStatuscd=131
            then 'Return to Author'
            when a.FinalStatuscd=132
            then 'Send to Advocate'
            else 'Not Mentioned'
            end
      as 'Case Status'
      ,cast(h.HearingDt as DATE) as 'Hearing Date'
      ,b.Charges
     ,e.Code_Name as 'DE Status',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  left join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  left join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  left join Code e on h.CalStatusCd=e.Code_Id
  left join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  left join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Name in('OATH REQ','NOT SCHD','CONCLUDED')
  and b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   d.LastName 
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then 'Open'
            when a.FinalStatuscd=97
            then 'Closed'
            when a.FinalStatuscd=98
            then 'DS249 Not Yet Recieved'
            when a.FinalStatuscd=130
            then 'Send to BCAD'
            when a.FinalStatuscd=131
            then 'Return to Author'
            when a.FinalStatuscd=132
            then 'Send to Advocate'
            else 'Not Mentioned'
            end
       as 'Case Status'
     ,cast(h.HearingDt as DATE) as 'Hearing Date'
      ,b.Charges
     ,e.Code_Name as 'DE Status',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  left join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  left join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  left join Code e on h.HearingStatusCd=e.Code_Id
  left join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  left join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Name in('OATH MEDIATED','H/HELD DEC REVERSED','R/R/T','RESPONDENT NOT					NOTIFIED','STIPULATION PENDING')
  and b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   d.LastName 
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then 'Open'
            when a.FinalStatuscd=97
            then 'Closed'
            when a.FinalStatuscd=98
            then 'DS249 Not Yet Recieved'
            when a.FinalStatuscd=130
            then 'Send to BCAD'
            when a.FinalStatuscd=131
            then 'Return to Author'
            when a.FinalStatuscd=132
            then 'Send to Advocate'
            else 'Not Mentioned'
            end
       as 'Case Status'
      ,cast(h.HearingDt as DATE) as 'Hearing Date'
      ,b.Charges
     ,e.Code_Name as 'DE Status',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  left join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  left join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  left join Code e on a.FinalStatuscd=e.Code_Id
  left join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  left join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Name in('SIU','R.R.T W/CHARGES PENDING','UNKNOWN') 
  and b.CreatedDate between @StartDate and @EndDate
  ) 
  as DEInfo order by DEInfo.CreatedDate desc
  GO
  
  
  
  
  
  
  ALTER Procedure usp_RptGetRoutingHistory 
  
  @IndexNo bigint = null

  As
  if (@IndexNo is null) or (@IndexNo='')
  begin
select b.IndexNo,b.RespondentLName+', '+ b.RespondentFName as RespondentName,    c.LastName+', '+ c.FirstName as SentBy,a.SentDate,a.SentFrom,a.SentTo from 
  tblRoutingHistory a 
  left join tblPersons c on c.PersonID=a.SentBy 
  inner join tblDS249Complaints b
  on a.ComplaintID=b.ComplaintId
  --where  b.IndexNo =@IndexNo
  order by b.IndexNo desc, a.SentDate asc
  end
  else
  begin
  select b.IndexNo,b.RespondentLName+', '+ b.RespondentFName as RespondentName,    c.LastName+', '+ c.FirstName as SentBy,a.SentDate,a.SentFrom,a.SentTo from 
  tblRoutingHistory a 
  left join tblPersons c on c.PersonID=a.SentBy 
  inner join tblDS249Complaints b
  on a.ComplaintID=b.ComplaintId
  where  b.IndexNo =@IndexNo
  order by b.IndexNo desc, a.SentDate asc
  end
  
  
  
 
  
  
  