begin try
	begin tran

		--46
		update c set  
				c.RespondentApptDt = e1.AppointmentDate
				,c.RespondentDOB = p1.DOB
				,c.RespondentEmpID = e1.EmployeeID
				,c.RespondentFName = p1.FirstName
				,c.RespondentLName = p1.LastName
				,c.RespondentMName = LEFT( p1.MiddleName,1)
				,c.RespondentRefNumber = e1.ReferenceNo 
		from tblPersons p1 inner join tblPersons p2 
				on  REPLACE(p1.FirstName, ' ','') = replace(p2.FirstName  , ' ','')
                  and REPLACE( p1.LastName, ' ','')= replace(p2.LastName, ' ','') 
				and p1.SSN = p2.SSN
		inner join tblEmployees e1 on e1.PersonID = p1.PersonID
		inner join tblEmployees e2 on e2.PersonID = p2.PersonID
		inner join tblDS249Complaints c on c.RespondentEmpID = e2.EmployeeID			 
		where p2.DataSource = 'dataease' 
				and p1.DataSource is null

		--38 rows					
		delete from tblEmployeeAddress 
		where EmployeeId in (
				select 
					e2.EmployeeID
				from tblPersons p1 inner join tblPersons p2 
					on  REPLACE(p1.FirstName, ' ','') = replace(p2.FirstName  , ' ','')
                  and REPLACE( p1.LastName, ' ','')= replace(p2.LastName, ' ','') 
						and p1.SSN = p2.SSN
					 inner join tblEmployees e1 on e1.PersonID = p1.PersonID
					 inner join tblEmployees e2 on e2.PersonID = p2.PersonID
					 inner join tblEmployeeAddress a on a.EmployeeId = e2.EmployeeID
				 where 
					p2.DataSource = 'dataease' 
					and p1.DataSource is null)
					 
		--38
		delete from tblEmployeeLocations 
		where EmployeeLocationID in (			
				select 
					EmployeeLocationID
				from tblPersons p1 inner join tblPersons p2 
						on  REPLACE(p1.FirstName, ' ','') = replace(p2.FirstName  , ' ','')
                  and REPLACE( p1.LastName, ' ','')= replace(p2.LastName, ' ','') 
							 and p1.SSN = p2.SSN
						inner join tblEmployees e1 on e1.PersonID = p1.PersonID
						inner join tblEmployees e2 on e2.PersonID = p2.PersonID		 
						inner join tblEmployeeLocations   a on a.EmployeeId = e2.EmployeeID
				 where p2.DataSource = 'dataease' 
						and p1.DataSource is null)
					 
		--38	
		delete from tblEmployeeBadges 
		where EmployeeBadgeID in (
				select 
					distinct b.EmployeeBadgeID
				from tblPersons p1 inner join tblPersons p2 
						on  REPLACE(p1.FirstName, ' ','') = replace(p2.FirstName  , ' ','')
                  and REPLACE( p1.LastName, ' ','')= replace(p2.LastName, ' ','') 
						and p1.SSN = p2.SSN
				inner join tblEmployees e1 on e1.PersonID = p1.PersonID
				inner join tblEmployees e2 on e2.PersonID = p2.PersonID			 
				inner join tblEmployeeTitles a on a.EmployeeId = e2.EmployeeID
				inner join tblEmployeeBadges b on a.EmployeeTitleID=b.EmployeeTitleID
				where p2.DataSource = 'dataease' 
						and p1.DataSource is null)	
					 
		--38					 
		delete from tblEmployeeTitles 
		where EmployeeTitleID in (
				select 
					EmployeeTitleID
				from tblPersons p1 inner join tblPersons p2 
						on  REPLACE(p1.FirstName, ' ','') = replace(p2.FirstName  , ' ','')
                  and REPLACE( p1.LastName, ' ','')= replace(p2.LastName, ' ','') 
						and p1.SSN = p2.SSN
						inner join tblEmployees e1 on e1.PersonID = p1.PersonID
						inner join tblEmployees e2 on e2.PersonID = p2.PersonID		
						inner join tblEmployeeTitles a on a.EmployeeId = e2.EmployeeID
				where p2.DataSource = 'dataease' 
						and p1.DataSource is null)		
		
		--39					 
		delete from tblEmployees 
		where PersonID in (
				select 
					distinct p2.PersonID 
				from tblPersons p1 inner join tblPersons p2 
						on  REPLACE(p1.FirstName, ' ','') = replace(p2.FirstName  , ' ','')
                  and REPLACE( p1.LastName, ' ','')= replace(p2.LastName, ' ','') 
						and p1.SSN = p2.SSN
				where p2.DataSource = 'dataease' 
				and p1.DataSource is null)
					  
		--39
		delete from tblPersons 
		where PersonID in (
				select 
					distinct p2.PersonID 
				from tblPersons p1 inner join tblPersons p2 
						on  REPLACE(p1.FirstName, ' ','') = replace(p2.FirstName  , ' ','')
                  and REPLACE( p1.LastName, ' ','')= replace(p2.LastName, ' ','') 
						and p1.SSN = p2.SSN
				where p2.DataSource = 'dataease' 
				and p1.DataSource is null)
	
	commit tran
end try
begin catch
	rollback tran
	print error_message()
end catch