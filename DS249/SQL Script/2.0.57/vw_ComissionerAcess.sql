
/****** Object:  View [dbo].[vw_ComissionerAcess]    Script Date: 07/27/2012 10:28:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[vw_ComissionerAcess]
AS
 SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.IsProbation
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
      
ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
     Jt1
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 --WHERE a.DataSource='OEDM'
 
--order by CreatedDate

GO


