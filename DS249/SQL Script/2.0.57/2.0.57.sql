/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateComplaintsBySearch]    Script Date: 08/07/2012 10:01:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAdvocateComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaintsBySearch]    Script Date: 08/07/2012 10:01:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetBCADComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaintsCountSearch]    Script Date: 08/07/2012 10:01:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaintsCountSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetBCADComplaintsCountSearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsBySearch]    Script Date: 08/07/2012 10:01:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetMedicalComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsCountSearch]    Script Date: 08/07/2012 10:01:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsCountSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetMedicalComplaintsCountSearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 08/07/2012 10:01:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProbationEmployees]    Script Date: 08/07/2012 10:01:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetProbationEmployees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetProbationEmployees]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProbationEmployees]    Script Date: 08/07/2012 10:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetProbationEmployees]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- [dbo].[usp_GetprobationEmployees] ''Bcad'',08/21/2008,08/31/2012
CREATE PROCEDURE [dbo].[usp_GetProbationEmployees]
@RoutingLocation varchar(10),
@CreatedStartDate date,
@CreatedEndDate date
AS

BEGIN

	SELECT ( case when C.RoutingLocation=''A'' then ''ADVOCATE'' when c.RoutingLocation=''B'' then ''BCAD'' 
	WHEN c.RoutingLocation=''M'' then ''MEDICAL'' 
	WHEN c.ComplaintStatus =4 and c.RoutingLocation='' ''
	THEN ''APPROVED''	
	 ELSE '' '' END) AS ''RoutingLocation'',p.LastName ,P.FirstName ,C.IndexNo ,C.RespondentRefNumber,C.ComplaintId ,c.CreatedDate,ISNULL(L.LocationName,''-'') AS	DistrictName, ISNULL(LB.LocationName, ''-'') Zone 
 INTO #temp
	FROM  tblDS249Complaints C INNER JOIN tblEmployees E ON E.EmployeeID = C.RespondentEmpID INNER JOIN tblLocations L ON L.LocationId = C.LocationId	LEFT  JOIN tblLocations LB ON LB.LocationId = C.BoroughID INNER JOIN tblPersons P ON P.PersonID = E.PersonID
		WHERE IsProbation =1 and IsDeleted =0 --and ComplaintStatus >=4
	
	
	IF(@RoutingLocation =''ADVOCATE'' OR @RoutingLocation=''BCAD'' or @RoutingLocation=''MEDICAL'')
	BEGIN
		SELECT * from #temp where CAST(createddate as DATE)>=@CreatedStartDate and 
		CAST(createddate AS DATE)<=@CreatedEndDate  and Routinglocation=@RoutingLocation ORDER BY createddate DESC
	END
	ELSE IF(@RoutingLocation=''DS249'')
	BEGIN
	SELECT * FROM #temp WHERE CAST(createddate AS DATE)>=@CreatedStartDate and 
	CAST(createddate AS DATE)<=@CreatedEndDate  and Routinglocation in (''ADVOCATE'',''BCAD'',''MEDICAL'',''APPROVED'',null,'' '') ORDER BY createddate DESC
	END 
	
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 08/07/2012 10:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

--[usp_GetComplaintsBySearch] 10045,0, 50,0,'''',''01/02/2010'','''',64
CREATE PROCEDURE [dbo].[usp_GetComplaintsBySearch]
	-- Add the parameters for the stored procedure here
@CreatedBy int=0,
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@RowCount int OUTPUT 
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @numericStringValue varchar(50)
	DECLARE  @chargesLikeValue varchar(22)
	DECLARE @stringLikeValue varchar(52)
	DECLARE @FirstRow INT 
	DECLARE @LastRow INT
	DECLARE @count INT
	declare @FullStop char(2) = ''. ''
	declare @Blank char(1) = ''''
	
	SELECT   @chargesLikeValue = ''%'' +@chargesValue + ''%'' 
			,@stringLikeValue = ''%'' +@stringValue +  ''%'' 			
			,@numericStringValue =''%'' +CAST( @numericValue as varchar) + ''%''
			,@FirstRow = (@PageNo * @maximumRows) + 1
			,@LastRow = (@PageNo * @maximumRows) + @maximumRows ;        
	
	select @count=COUNT(m.Id) 
	from tblMembershipRoles m 
	where 
		PersonalID = @CreatedBy 
		and m.RoleId = 10 --RoleID 10 = ''Commissioner Access''
	
	if @count>=1
		Begin		
		if @numericValue != 0  
				Begin					
					SELECT 
						ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
						[ComplaintId]
						,IndexNo
						,a.isprobation						
						,a.[LocationId]
						,a.[BoroughID]
						,a.promotiondate
						,a.LocationName
						,(case when a.complaintStatus >=4 then  a.RoutingLocation
							when a.RoutingLocation =''P'' then  a.RoutingLocation
							else null 
						end) as RoutingLocation
						,[RespondentEmpID]
						,[RespondentRefNumber]      
						,[Approved]      
						,CreatedBy      
						,CONVERT(varchar,CreatedDate,101) as CreatedDate
						--,'''' as Status
						,charges as Charges
						,Chargesdesc as ChargesDesc
						, RespondentFName
						,RespondentLName
						,A.complaintStatus
						,A.complaintStatusDate
						,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
								else 0  end) as Indicator,
						a.IsDeleted,
						a.jt1,0  as FinalStatusCD 
					into #Complaints5  
					FROM dbo.vw_ComissionerAcess a 			 
					where 
						IndexNo =@numericValue
						OR RespondentRefNumber like @numericStringValue
						or isnull(a.jt1,@Blank) like @numericStringValue
					order by a.CreatedDate desc

					SELECT  * 
					FROM  #Complaints5 
					where ROWID BETWEEN @FirstRow AND @LastRow 
					ORDER BY ROWID asc     

					SELECT @RowCount=COUNT([ComplaintId]) 
					FROM #Complaints5

					drop table #Complaints5  

				End
				else if @chargesValue != @Blank 
						Begin
							SELECT 
								ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
								[ComplaintId]
								,IndexNo
								,a.isprobation
								,a.[LocationId]
								,a.[BoroughID]
								,a.promotiondate
								,a.LocationName
								,(case when a.complaintStatus >=4 then  a.RoutingLocation
										when a.RoutingLocation =''P'' then  a.RoutingLocation
										else null 
										end) as RoutingLocation
								,[RespondentEmpID]
								,[RespondentRefNumber]      
								,[Approved]      
								, CreatedBy      
								,CONVERT(varchar,CreatedDate,101) as CreatedDate
								--,@Blank as Status
								,charges as Charges
								,Chargesdesc as ChargesDesc
								, RespondentFName
								,RespondentLName
								,A.complaintStatus
								,A.complaintStatusDate
								,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
										else 0  end) as Indicator
								,a.IsDeleted
								,a.jt1
								,0  as FinalStatusCD 
							into #Complaints6  
							FROM dbo.vw_ComissionerAcess a 
							where 
								Charges like @chargesLikeValue
							order by  a.CreatedDate desc

							SELECT  * 
							FROM  #Complaints6 
							where ROWID BETWEEN @FirstRow AND @LastRow  
							ORDER BY ROWID asc  
							
							SELECT @RowCount=COUNT([ComplaintId]) 
							FROM #Complaints6
							
							drop table #Complaints6     
						End	
				 else if @complaintDate != @Blank
						Begin
							SELECT 	  
								ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
								[ComplaintId]
								,IndexNo
								,a.Isprobation
								,a.[LocationId]
								,a.[BoroughID]
								,a.promotiondate
								,a.LocationName
								,(case when a.complaintStatus >=4 then  a.RoutingLocation
									when a.RoutingLocation =''P'' then  a.RoutingLocation
									else null 
									end) as RoutingLocation
								,[RespondentEmpID]
								,[RespondentRefNumber]      
								,[Approved]      
								,CreatedBy      
								,CONVERT(varchar,CreatedDate,101) as CreatedDate
								--,@Blank as Status
								,charges as Charges
								,Chargesdesc as ChargesDesc
								, RespondentFName
								,RespondentLName
								,A.complaintStatus
								,A.complaintStatusDate
								,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
										else 0  end) as Indicator
								,a.IsDeleted
								,a.jt1
								,0  as FinalStatusCD 
							  into #Complaints7  
						 FROM dbo.vw_ComissionerAcess a 
						 where  cast(CreatedDate as date) = CAST(@complaintDate as date)
						 order by  a.CreatedDate desc
						 	

						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #Complaints7
						
						SELECT  * 
						FROM  #Complaints7  
						where ROWID BETWEEN @FirstRow AND @LastRow  
						ORDER BY ROWID asc   
						
						drop table #Complaints7 
												
					End	
				 else if @stringValue != @Blank
						Begin	
						   SELECT 
									ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,       
									[ComplaintId]
									,IndexNo
									,a.IsProbation
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,a.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation
											when a.RoutingLocation =''P'' then  a.RoutingLocation
											else null 
											end) as RoutingLocation
									,[RespondentEmpID]
									,[RespondentRefNumber]      
									,[Approved]      
									,CreatedBy      
									,CONVERT(varchar,CreatedDate,101) as CreatedDate
									--, @Blank as Status
									,charges as Charges
									,Chargesdesc as ChargesDesc
									, RespondentFName
									,RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
											else 0  end) as Indicator
									,a.IsDeleted
									,a.jt1
									,0  as FinalStatusCD 
						 into #Complaints8  
						 FROM dbo.vw_ComissionerAcess a 					 
						 where 
			 									
					replace(replace(RespondentFName,'''''''',''''),'' '','''')like @stringLikeValue		
					OR replace(replace(RespondentLName,'''''''',''''),'' '','''') like @stringLikeValue
					OR REPLACE(replace(CreatedBy,'''''''',''''),'' '','''') like @stringLikeValue						  
					OR LocationName like @stringLikeValue 
					or isnull(a.jt1,@Blank) like @stringLikeValue
					order by  a.CreatedDate desc							
						SELECT  * 
						FROM  #Complaints8  
						where ROWID BETWEEN @FirstRow AND @LastRow  
						ORDER BY ROWID asc  
						
						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #Complaints8
						
						drop table #Complaints8    
						 
				   End	
			End
	Else
		Begin				
				create table #Complaints(ComplaintId bigint)     
				
				insert  into #Complaints
							exec dbo.[usp_GetComplaintsSecurity] @CreatedBy;
							
				if @numericValue != 0  
					Begin		
						SELECT 
							a.[ComplaintId]
							,a.IndexNo
							,a.IsProbation
							,a.[LocationId]
							,a.[BoroughID]
							,a.promotiondate
							,b.LocationName
							,(case when a.complaintStatus >=4 then  a.RoutingLocation			
									else null 
									end) as RoutingLocation
							,a.[RespondentEmpID]
							,a.[RespondentRefNumber]      
							,a.[Approved]
							,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
							,a.[CreatedDate]
							--, @Blank as Status
							,a.Charges as Charges
							,a.Chargesdesc as ChargesDesc
							, a.RespondentFName
							,a.RespondentLName
							,A.complaintStatus
							,A.complaintStatusDate
							,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end)as Indicator
							,a.IsDeleted
							,a.Jt1
							,0  as FinalStatusCD 
							,ROWID =IDENTITY(INT,1,1) 
						into #tempNumeric
						FROM 
							[dbo].[tblDS249Complaints] a 
							inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
							LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
							LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
						where 
							IndexNo = @numericValue 
							OR RespondentRefNumber like @numericStringValue 
							or jt1 like   @numericStringValue
						order by a.CreatedDate desc

						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #tempNumeric
						
						select * from #tempNumeric
						WHERE ROWID BETWEEN @FirstRow AND @LastRow
						ORDER BY  ROWID asc;
						
						drop table #Complaints						
						drop table  #tempNumeric
					End
				else if @chargesValue != @Blank 
						Begin
								SELECT 
									a.[ComplaintId]
									,a.IndexNo
									,a.IsProbation
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,b.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation	
									else null 
									end) as RoutingLocation
									,a.[RespondentEmpID]
									,a.[RespondentRefNumber]      
									,a.[Approved]
									,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy 							     
									,a.[CreatedDate]
									--,@Blank as [Status]
									,a.Charges as Charges
									,a.Chargesdesc as ChargesDesc
									, a.RespondentFName
									,a.RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end) as Indicator,
									a.IsDeleted,
									a.Jt1,0  as FinalStatusCD ,
									ROWID =IDENTITY(INT,1,1) 
								into #tempCharges
								FROM [dbo].[tblDS249Complaints] a 
									inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
									LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
									LEFT OUTER JOIN uv_EmployeePerson c on  a.CreatedBy = c.EmployeeID  
								where Charges like @chargesLikeValue 
								order by  a.CreatedDate desc
								SELECT @RowCount=COUNT([ComplaintId]) FROM #tempCharges

								select * from #tempCharges
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints
								drop table  #tempCharges

						End	
				else if @complaintDate != @Blank
						Begin	
								SELECT 
									a.[ComplaintId]
									,a.IndexNo
									,a.IsProbation
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,b.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation	
									else null 
									end) as RoutingLocation
									,a.[RespondentEmpID]
									,a.[RespondentRefNumber]      
									,a.[Approved]
									,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
									,a.[CreatedDate]
									--, @Blank as Status
									,a.Charges as Charges
									,a.Chargesdesc as ChargesDesc
									, a.RespondentFName
									,a.RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end) as Indicator
									,a.IsDeleted
									,a.Jt1
									,0  as FinalStatusCD
									,ROWID =IDENTITY(INT,1,1) 
								into #tempComplaintDate
								FROM [dbo].[tblDS249Complaints] a 
									inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
									LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
									LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
								where							
									cast(CreatedDate as date) = CAST(@complaintDate as date)
								order by  a.CreatedDate desc



								SELECT @RowCount=COUNT([ComplaintId]) 
								FROM #tempComplaintDate

								select * from #tempComplaintDate
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints 
								drop table  #tempComplaintDate

						End	
				else if @stringValue != @Blank
						Begin
								SELECT 
										a.[ComplaintId]
										,a.IndexNo
										,a.IsProbation 
										,a.[LocationId]
										,a.[BoroughID]
										,a.promotiondate
										,b.LocationName
										,(case when a.complaintStatus >=4 then  a.RoutingLocation
			when a.RoutingLocation =''P'' then  a.RoutingLocation							
												else null 
												end) as RoutingLocation
										,a.[RespondentEmpID]
										,a.[RespondentRefNumber]      
										,a.[Approved]
										,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
										,a.[CreatedDate]
										--, @Blank as [Status]
										,a.Charges as Charges
										,a.Chargesdesc as ChargesDesc
										, a.RespondentFName
										,a.RespondentLName
										,A.complaintStatus
										,A.complaintStatusDate
										,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
												else 0
												end) as Indicator
										,a.IsDeleted
										,a.Jt1
										,0  as FinalStatusCD 
										,ROWID =IDENTITY(INT,1,1) 
								into #tempStringValue
								FROM [dbo].[tblDS249Complaints] a 
										inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
										LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
										LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID
								where 
								replace(replace(RespondentFName,'''''''',''''),'' '','''') like @stringLikeValue	
								OR replace(replace(RespondentLName,'''''''',''''),'' '','''') like @stringLikeValue
								OR LocationName like @stringLikeValue 
								or  jt1 like   @stringLikeValue						
								OR  replace(replace(c.FirstName,'''''''',''''),'' '','''')like @stringLikeValue									OR  replace(replace(c.LastName,'''''''',''''),'' '','''') like @stringLikeValue	
								order by  a.CreatedDate desc
								SELECT @RowCount=COUNT([ComplaintId]) 
								FROM #tempStringValue
								select * 
								from #tempStringValue
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints
								drop table  #tempStringValue
						End	 
	END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsCountSearch]    Script Date: 08/07/2012 10:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsCountSearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetMedicalComplaintsCountSearch] 
	-- Add the parameters for the stored procedure here	
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;
DECLARE @stringLikeValue varchar(52)
SELECT  @stringLikeValue = ''%'' +@stringValue +  ''%'' 
  	
if @numericValue != 0  
  Begin
  
  SELECT	
		COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
	  
  End
 else if @chargesValue != '''' 
  Begin
    SELECT	
			COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (Charges like ''%''+@chargesValue+ ''%'') 	
  End
 else if @complaintDate != ''''
  Begin
      SELECT				
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, '''' as Charges    		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         ,a.IsProbation 
         into #MedicalComplaints3 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) ORDER BY ComplaintId desc
 	SELECT  Count(*) FROM  #MedicalComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 	drop table #MedicalComplaints3  
	
  End
 else if @stringValue != ''''
  Begin
      SELECT			
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, '''' as Charges    		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         ,a.IsProbation
         into #MedicalComplaints4 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0) 
	SELECT COUNT(*) FROM  #MedicalComplaints4  where 
	 
	replace(replace(RespondentFName,'''''''',''''),'' '','''')like @stringLikeValue		
OR replace(replace(RespondentLName,'''''''',''''),'' '','''') like @stringLikeValue
OR REPLACE(replace(CreatedBy,'''''''',''''),'' '','''') like @stringLikeValue	
OR LocationName like @stringLikeValue 
OR  Status  like @stringLikeValue

	
	drop table #MedicalComplaints4
	
  End 	 	
   
     
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsBySearch]    Script Date: 08/07/2012 10:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetMedicalComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;
	
DECLARE @stringLikeValue varchar(52)
SELECT  @stringLikeValue = ''%'' +@stringValue +  ''%'' 

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
  	
if @numericValue != 0  
  Begin
  
  SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges  
		,i.IncidentDate   		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName	
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
	
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints1  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 		
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
	    order by ComplaintStatusDate Desc	       
		SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
		FROM #MedicalComplaints1 WHERE compIndex=1)tblMedComplaints 
		WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	
	drop table #MedicalComplaints1
	  
  End
 else if @chargesValue != '''' 
  Begin
    SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges   
		,i.IncidentDate  		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints2  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (Charges like ''%''+@chargesValue+ ''%'')
	 
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
	FROM #MedicalComplaints2 where compIndex=1)tblMedComplaints 
	WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints2
	
  End
 else if @complaintDate != ''''
  Begin
     SELECT	
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate 		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints3 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0))
 
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2, * into #adf0  FROM  #MedicalComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 	select * from (select *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
   from  #adf0 where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints3  
	drop table #adf0     
	 
  End
 else if @stringValue != ''''
  Begin
      SELECT	
       ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,		
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges   
		,i.IncidentDate 	
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints4 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0) ORDER BY ComplaintId desc
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #MedicalComplaints4  where	
	
replace(replace(RespondentFName,'''''''',''''),'' '','''')like @stringLikeValue		
OR replace(replace(RespondentLName,'''''''',''''),'' '','''') like @stringLikeValue
OR REPLACE(replace(CreatedBy,'''''''',''''),'' '','''') like @stringLikeValue	
OR LocationName like @stringLikeValue 
OR  Status  like @stringLikeValue
	  
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
   FROM #adf where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints4
	drop table #adf
  End 	 	
   
     
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaintsCountSearch]    Script Date: 08/07/2012 10:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaintsCountSearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- exec [dbo].[usp_GetBCADComplaintsCountSearch]  0,'''','''',''Closed''
-- =============================================
-- Author:		<Anshul>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetBCADComplaintsCountSearch] 
	-- Add the parameters for the stored procedure here	
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @stringLikeValue varchar(52)
SELECT  @stringLikeValue = ''%'' +@stringValue +  ''%'' 

if @numericValue != 0  
  Begin
  
  
	SELECT		
		COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
	
  End	
 else if @chargesValue != '''' 
  Begin
  
  	SELECT		
  		COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (Charges like ''%''+@chargesValue+ ''%'') 
	    
  End
 else if @complaintDate != ''''
  Begin
  SELECT				
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, '''' as Charges    
		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted         
         into #CadComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	     ORDER BY ComplaintId desc
 	 SELECT COUNT(*) FROM  #CadComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 	
	drop table #CadComplaints3
 	
  End
 else if @stringValue != ''''
  Begin
  
    SELECT		
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, '''' as Charges    
		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted         
         into #CadComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 )) 
  
	SELECT Count(*) FROM  #CadComplaints4  where 
	
	 replace(replace(RespondentFName,'''''''',''''),'' '','''')like @stringLikeValue		
OR replace(replace(RespondentLName,'''''''',''''),'' '','''') like @stringLikeValue
OR REPLACE(replace(CreatedBy,'''''''',''''),'' '','''') like @stringLikeValue	
OR LocationName like @stringLikeValue 
OR  Status  like @stringLikeValue
	
	drop table #CadComplaints4
	
  End
 	 	
  
  
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaintsBySearch]    Script Date: 08/07/2012 10:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetBCADComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)

AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @stringLikeValue varchar(52)
SELECT  @stringLikeValue = ''%'' +@stringValue +  ''%'' 

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,
      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;        
if @numericValue != 0  
  Begin
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.IsProbation,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted 
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints1  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
	
	drop table #CadComplaints1  
	
  End	
 else if @chargesValue != '''' 
  Begin
  
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.IsProbation,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (Charges like ''%''+@chargesValue+ ''%'') 
	   
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints2  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;	
	
	drop table #CadComplaints2	    
  End
 else if @complaintDate != ''''
  Begin
  SELECT
  		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.IsProbation,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
			,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
          ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	     
 	 SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2, * into #adf0 FROM  #CadComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
		
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf0   where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc; 	 
	
	drop table #CadComplaints3
	drop table #adf0 
  End
 else if @stringValue != ''''
  Begin  
    SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.IsProbation,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges 				
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
       ,i.IncidentDate
         into #CadComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #CadComplaints4  where 
	 
	 replace(replace(RespondentFName,'''''''',''''),'' '','''')like @stringLikeValue		
OR replace(replace(RespondentLName,'''''''',''''),'' '','''') like @stringLikeValue
OR REPLACE(replace(CreatedBy,'''''''',''''),'' '','''') like @stringLikeValue	
OR LocationName like @stringLikeValue 
OR  Status  like @stringLikeValue
	  
	
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
		
	drop table #CadComplaints4
	drop table #adf
  End  
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateComplaintsBySearch]    Script Date: 08/07/2012 10:01:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetAdvocateComplaintsBySearch] 
	-- Add the parameters for the stored procedure here	
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@rowCount int output
AS
BEGIN

SET NOCOUNT ON;

DECLARE @stringLikeValue varchar(52)
SELECT  @stringLikeValue = ''%'' +@stringValue +  ''%'' 

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
	
if @numericValue != 0  
 Begin
 
 SELECT		
		--ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else ''DATAEASE''
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId
		,a.IsProbation 
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.Charges as charges    
		--,c.LastName + '' ,'' + c.FirstName as ''CreatedBy''
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId		
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = ''1'')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))	
	 and (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue) OR AdvocacyCaseId  like ''%''+Convert(varchar,@numericValue))  
	--ORDER BY ComplaintId desc
	order by ComplaintStatusDate Desc	  
	--SELECT  * FROM  #AdvocateComplaints1 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     	


select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #AdvocateComplaints1 where compIndex = 1) #AdvocateComplaints1
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;	

select @rowCount=COUNT(*) from #AdvocateComplaints1 where compIndex = 1
	
	drop table #AdvocateComplaints1	
 End
 else if @chargesValue != '''' 
 Begin
 
  SELECT
		--ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else ''DATAEASE''
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IsProbation
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		--,c.LastName + '' ,'' + c.FirstName as ''CreatedBy''
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId			
		--once database is updated check only for a.ComplaintStatus=4
		-- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = ''1'')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))	
	and (Charges like ''%''+@chargesValue+ ''%'')
	 --ORDER BY ComplaintId desc
	 ORDER BY ComplaintStatusDate Desc	
	--SELECT  * FROM  #AdvocateComplaints2 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
	

	select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #AdvocateComplaints2 where compIndex = 1) #AdvocateComplaints2
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;

select @rowCount=COUNT(*) from #AdvocateComplaints2 where compIndex = 1

	drop table #AdvocateComplaints2
 End
 else if @complaintDate != ''''
 Begin
   SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else ''DATAEASE''
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IsProbation
		,a.IndexNo as IndexNo
		,a.Jt1 
	    ,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		--,c.LastName + '' ,'' + c.FirstName as ''CreatedBy''
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
          ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId			
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = ''1'')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo )) 	 
	--ORDER BY ComplaintId desc   
	ORDER BY ComplaintStatusDate Desc	
	
	
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,* into #adf0 FROM  #AdvocateComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 		
 	--SELECT * from #adf0 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc    
 		

 	select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #adf0 where compIndex = 1) #adf0
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc; 

select @rowCount=COUNT(*) from #adf0 where compIndex = 1
	
	drop table #AdvocateComplaints3
	drop table #adf0 
 End
 else if @stringValue != ''''
 Begin
 
    SELECT
   		--ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,					
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else ''DATAEASE''
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId
		,a.IsProbation
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		--,c.LastName + '' ,'' + c.FirstName as ''CreatedBy''
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = ''1'')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = 
		(Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))
	
	--ORDER BY ComplaintId desc;
	ORDER BY ComplaintStatusDate Desc	
		
			
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,* into #adf FROM  #AdvocateComplaints4  where
	 (		 
replace(replace(RespondentFName,'''''''',''''),'' '','''')like @stringLikeValue		
OR replace(replace(RespondentLName,'''''''',''''),'' '','''') like @stringLikeValue
OR REPLACE(replace(CreatedBy,'''''''',''''),'' '','''') like	 @stringLikeValue
OR LocationName like @stringLikeValue	 
	 OR Status like @stringLikeValue
	  or jt1 like @stringLikeValue ) 	
	
		
select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #adf where compIndex = 1) #adf
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;
	
	select @rowCount=COUNT(*) from #adf where compIndex = 1
	
	drop table #AdvocateComplaints4
	drop table #adf
	
 End	 	 
 
END


' 
END
GO
