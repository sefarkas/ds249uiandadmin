ALTER Procedure [dbo].[usp_RptGetAdcocateDEInfo] 

  @StartDate date,

  @EndDate date

  As

 

Select * from

(  

SELECT 

                   b.IndexNo

                  ,d.LastName

      ,d.FirstName

      ,c.ReferenceNo

      ,case when a.FinalStatuscd=96

            then 'Open'

            when a.FinalStatuscd=97

            then 'Closed'

            when a.FinalStatuscd=98

            then 'DS249 Not Yet Recieved'

            when a.FinalStatuscd=130

            then 'Send to BCAD'

            when a.FinalStatuscd=131

            then 'Return to Author'

            when a.FinalStatuscd=132

            then 'Send to Advocate'

            else 'Not Mentioned'

            end

        as 'Case Status'

      ,cast(h.HearingDt as DATE) as 'Hearing Date'

      ,b.Charges

     ,e.Code_Name as 'DE Status',

     b.CreatedDate

  FROM [dbo].[tblComplaintAdvocacy] a

  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId

  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId

  inner join Code e on a.Actioncd=e.Code_Id

  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo

  inner join tblPersons d on c.PersonID=d.PersonID

  where 

  e.Code_Id in(42,43,45)

  and (b.DataSource='DE' or a.DataSource='DE')
  and b.CreatedDate between @StartDate and @EndDate

  

  UNION

  

SELECT 

                   b.IndexNo

                  ,d.LastName

      ,d.FirstName

      ,c.ReferenceNo

      ,case when a.FinalStatuscd=96

            then 'Open'

            when a.FinalStatuscd=97

            then 'Closed'

            when a.FinalStatuscd=98

            then 'DS249 Not Yet Recieved'

            when a.FinalStatuscd=130

            then 'Send to BCAD'

            when a.FinalStatuscd=131

            then 'Return to Author'

            when a.FinalStatuscd=132

            then 'Send to Advocate'

            else 'Not Mentioned'

            end

       as 'Case Status'

     ,cast(h.HearingDt as DATE) as 'Hearing Date'

      ,b.Charges

     ,e.Code_Name as 'DE Status',

     b.CreatedDate

     

  FROM [dbo].[tblComplaintAdvocacy] a

  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId

  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId

  inner join Code e on a.ReviewStatuscd=e.Code_Id

  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo

  inner join tblPersons d on c.PersonID=d.PersonID

  where 

  e.Code_Id in(32,33,34,35,38)

    and (b.DataSource='DE' or a.DataSource='DE')

  AND b.CreatedDate between @StartDate and @EndDate

  

  UNION

  SELECT 

                   b.IndexNo

                  ,d.LastName

      ,d.FirstName

      ,c.ReferenceNo

      ,case when a.FinalStatuscd=96

            then 'Open'

            when a.FinalStatuscd=97

            then 'Closed'

            when a.FinalStatuscd=98

            then 'DS249 Not Yet Recieved'

            when a.FinalStatuscd=130

            then 'Send to BCAD'

            when a.FinalStatuscd=131

            then 'Return to Author'

            when a.FinalStatuscd=132

            then 'Send to Advocate'

            else 'Not Mentioned'

            end

        as 'Case Status'

     ,cast(h.HearingDt as DATE) as 'Hearing Date'

      ,b.Charges

     ,e.Code_Name as 'DE Status',

      b.CreatedDate

     

  FROM [dbo].[tblComplaintAdvocacy] a

  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId

  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId

  inner join Code e on a.SpecialStatus_cd=e.Code_Id

  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo

  inner join tblPersons d on c.PersonID=d.PersonID

  where 

  e.Code_Id in(23,24,25,26,27,28,29)

    and (b.DataSource='DE' or a.DataSource='DE')

  and b.CreatedDate between @StartDate and @EndDate

  UNION

  SELECT 

                   b.IndexNo

                  ,d.LastName

      ,d.FirstName

      ,c.ReferenceNo

      ,case when a.FinalStatuscd=96

            then 'Open'

            when a.FinalStatuscd=97

            then 'Closed'

            when a.FinalStatuscd=98

            then 'DS249 Not Yet Recieved'

           when a.FinalStatuscd=130

            then 'Send to BCAD'

            when a.FinalStatuscd=131

            then 'Return to Author'

            when a.FinalStatuscd=132

            then 'Send to Advocate'

            else 'Not Mentioned'

            end

        as 'Case Status'

      ,cast(h.HearingDt as DATE) as 'Hearing Date'

      ,b.Charges

     ,e.Code_Name as 'DE Status',

      b.CreatedDate

     

  FROM [dbo].[tblComplaintAdvocacy] a

  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId

  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId

  inner join Code e on a.TrialReturnStatuscd=e.Code_Id

  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo

  inner join tblPersons d on c.PersonID=d.PersonID

  where 

  e.Code_Id =74

     and (b.DataSource='DE' or a.DataSource='DE')

  and b.CreatedDate between @StartDate and @EndDate

  UNION

  SELECT 

                   b.IndexNo

                  ,d.LastName

      ,d.FirstName

      ,c.ReferenceNo

       ,case when a.FinalStatuscd=96

            then 'Open'

            when a.FinalStatuscd=97

            then 'Closed'

            when a.FinalStatuscd=98

            then 'DS249 Not Yet Recieved'

            when a.FinalStatuscd=130

            then 'Send to BCAD'

            when a.FinalStatuscd=131

            then 'Return to Author'

            when a.FinalStatuscd=132

            then 'Send to Advocate'

            else 'Not Mentioned'

            end

        as 'Case Status'

      ,cast(h.HearingDt as DATE) as 'Hearing Date'

      ,b.Charges

     ,e.Code_Name as 'DE Status',

      b.CreatedDate

     

  FROM [dbo].[tblComplaintAdvocacy] a

  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId

  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId

  inner join Code e on h.CalStatusCd=e.Code_Id

  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo

  inner join tblPersons d on c.PersonID=d.PersonID

  where 

  e.Code_Id in(48,49,52)

     and (b.DataSource='DE' or a.DataSource='DE')

  and b.CreatedDate between @StartDate and @EndDate

  UNION

  SELECT 

                   b.IndexNo

                  ,d.LastName

      ,d.FirstName

      ,c.ReferenceNo

       ,case when a.FinalStatuscd=96

            then 'Open'

            when a.FinalStatuscd=97

            then 'Closed'

           when a.FinalStatuscd=98

            then 'DS249 Not Yet Recieved'

            when a.FinalStatuscd=130

            then 'Send to BCAD'

            when a.FinalStatuscd=131

            then 'Return to Author'

            when a.FinalStatuscd=132

            then 'Send to Advocate'

            else 'Not Mentioned'

            end

        as 'Case Status'

      ,cast(h.HearingDt as DATE) as 'Hearing Date'

      ,b.Charges

     ,e.Code_Name as 'DE Status',

      b.CreatedDate

     

  FROM [dbo].[tblComplaintAdvocacy] a

  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId

  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId

  inner join Code e on h.HearingStatusCd=e.Code_Id

  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo

  inner join tblPersons d on c.PersonID=d.PersonID

  where 

  e.Code_Id in(53,67,68,69)

    and (b.DataSource='DE' or a.DataSource='DE')

  and b.CreatedDate between @StartDate and @EndDate

  UNION

  SELECT 

                   b.IndexNo

                  ,d.LastName 

      ,d.FirstName

      ,c.ReferenceNo

       ,case when a.FinalStatuscd=96

            then 'Open'

            when a.FinalStatuscd=97

            then 'Closed'

            when a.FinalStatuscd=98

            then 'DS249 Not Yet Recieved'

            when a.FinalStatuscd=130

            then 'Send to BCAD'

            when a.FinalStatuscd=131

            then 'Return to Author'

            when a.FinalStatuscd=132

            then 'Send to Advocate'

            else 'Not Mentioned'

            end

        as 'Case Status'

      ,cast(h.HearingDt as DATE) as 'Hearing Date'

      ,b.Charges

     ,e.Code_Name as 'DE Status',

      b.CreatedDate

     

  FROM [dbo].[tblComplaintAdvocacy] a

  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId

  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId

  inner join Code e on a.COBStatus_cd=e.Code_Id

  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo

  inner join tblPersons d on c.PersonID=d.PersonID

  where 

  e.Code_id in(127,128,178) 

    and (b.DataSource='DE' or a.DataSource='DE')

  and b.CreatedDate between @StartDate and @EndDate

  ) 

  as DEInfo order by DEInfo.CreatedDate desc


--select * from code 
--where Code_Id in(41,55,129)
--where code_Name like '%VOID%'