ALTER procedure [dbo].[usp_LoadComplaintAttachments]
@DocumentID numeric(18, 0),
@ParentID numeric,
@ChildId1 varchar(50)
As
BEGIN
set @ChildId1=null
if(@ParentID is NULL) AND (@ChildId1 is NULL)
	begin
	
		SELECT DocumentID as IndexNo
			  ,ParentID
			  ,Attachments
			  ,ApplicationName
			  ,AttachementName
			  ,ContentType
			  ,ChildId1
			  ,ChildId2
			  ,ChildId3
		FROM SupportedDocument
		WHERE DocumentID=@DocumentID
		and Isdeleted =0
	end
else
	begin
	
		if(@ChildId1 is null)
		begin
		
			SELECT DocumentID as IndexNo
				  ,ParentID
				  ,Attachments
				  ,ApplicationName
				  ,AttachementName
				  ,ContentType
				  ,ChildId1
				  ,ChildId2
				  ,ChildId3
			FROM SupportedDocument
			WHERE ParentID=@ParentID  
			and Isdeleted =0
		end
		else
		begin
			
			SELECT DocumentID as IndexNo
				  ,ParentID
				  ,Attachments
				  ,ApplicationName
				  ,AttachementName
				  ,ContentType
				  ,ChildId1
				  ,ChildId2
				  ,ChildId3
			FROM SupportedDocument
			WHERE ParentID=@ParentID AND ChildId1=@ChildId1 and Isdeleted =0
		end
	end
	END
