/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaints]    Script Date: 07/31/2012 09:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetMedicalComplaints]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsBySearch]    Script Date: 07/31/2012 09:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetMedicalComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsCountSearch]    Script Date: 07/31/2012 09:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsCountSearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetMedicalComplaintsCountSearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateComplaint]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateComplaint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAdvocateComplaint]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateComplaints]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateComplaints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAdvocateComplaints]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateComplaintsBySearch]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAdvocateComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaints]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetBCADComplaints]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaintsBySearch]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetBCADComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaint]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaint]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsForAdmin]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsForAdmin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsForAdmin]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaints]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaints]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaint]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetBCADComplaint]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaint]    Script Date: 07/31/2012 09:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaint]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetMedicalComplaint]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProbationEmployees]    Script Date: 07/31/2012 09:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetProbationEmployees]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetProbationEmployees]
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary1]    Script Date: 07/31/2012 09:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary1]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RptGetDisciplinary1]
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary2]    Script Date: 07/31/2012 09:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RptGetDisciplinary2]
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary3]    Script Date: 07/31/2012 09:56:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RptGetDisciplinary3]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsSecurity]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsSecurity]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsSecurity]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateHistory]    Script Date: 07/31/2012 09:56:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateHistory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetAdvocateHistory]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateHistory]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateHistory]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetAdvocateHistory] 
	-- Add the parameters for the stored procedure here
	@EmpID bigint,
	@ComplaintID  bigint 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

select * into #advocateComplaint from
(
	SELECT  DS249Com.ComplaintId,
			DS249Com.IndexNo,
			DS249Inc.IncidentDate as CreatedDate,
			DS249Com.ParentIndexNo,
			(Select dbo.GetChargesAsString(DS249Com.ComplaintId)) as ''Charges'',
			(Select dbo.GetChargesDescAsString(DS249Com.ComplaintId)) as ''ChargesDesc'',
			dbo.fnGetDisposition(DS249Com.IndexNo) as ''Penalty'',
			isnull(DS249Com.ParentIndexNo,DS249Com.indexno) parent_index,DS249Com.JT1,
			 ROW_NUMBER() OVER (ORDER BY ltrim(rtrim([parentindexno])) desc) rowid 
			 ,ROW_NUMBER() OVER (partition by DS249Com.indexno ORDER BY DS249Inc.serialNo asc) indexcnt
	FROM tblDS249Complaints DS249Com inner join tblDS249Incidents DS249Inc on DS249Com.ComplaintId=DS249Inc.ComplaintId 
	Where RespondentEmpID = @EmpID 
	and RoutingLocation=''A''
	and IsDeleted=0) a
		order by parentindexno desc
						
select  a.ComplaintId,
			a.IndexNo,
				
			a.CreatedDate,
		a.parentindexno,
			a.charges,
			a.chargesdesc,
			a.penalty,
			a.parent_index,
			a.Jt1
			
			,indexcnt
			 from #advocateComplaint a
			 where  indexcnt = 1
			group by a.parent_index,a.parentindexno,a.indexno,a.Jt1,a.createddate,a.charges,a.chargesdesc,a.penalty,a.complaintid,a.rowid
		,indexcnt
				order by a.CreatedDate desc
	
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsSecurity]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsSecurity]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
--[dbo].[usp_GetComplaintsSecurityDistinctComplaintBackupTestDipeshGill] 107
CREATE PROCEDURE [dbo].[usp_GetComplaintsSecurity]
	-- Add the parameters for the stored procedure here
@CreatedBy bigint = 0

AS
BEGIN

	SET NOCOUNT ON;
  declare @count int
   
  set @count=0;

  Declare @locationid int	
	
	select @locationid=LocationID from tblEmployeeLocations Where EmployeeID=@CreatedBy and IsCurrent=1
 
  
  select @count=COUNT(*) from tblMembershipRoles where PersonalID=@CreatedBy and tblMembershipRoles.RoleId=
  (Select tblRoles.Id from tblRoles where tblRoles.Roles=''Commissioner Access'')

  if @count>=1
     Begin
        SELECT
		[ComplaintId]
		,IndexNo
		,a.IsProbation 
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as ''RoutingLocation''
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''      

      ,[CreatedDate]
      , '''' as ''Status''
     ,charges as ''Charges''
	,Chargesdesc as ''ChargesDesc''
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as ''Indicator'',
      a.IsDeleted,
     a.Jt1
 FROM [dbo].[tblDS249Complaints] a 

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  


 
 UNION 
 
 SELECT
		[ComplaintId]
		,IndexNo
		,a.IsProbation 
      ,a.[LocationId]
	  ,a.[BoroughID]
	  ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as ''RoutingLocation''
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''      
     
      ,[CreatedDate]
      , '''' as ''Status''
     ,charges as ''Charges''
	,Chargesdesc as ''ChargesDesc''
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as ''Indicator'',
      a.IsDeleted,
     a.Jt1
 FROM [dbo].[tblDS249Complaints] a 

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID   
 where 

  CreatedBy = case when @createdBy = 0 then
					CreatedBy
				Else 
					@createdBy
				end

  Order by ComplaintId desc
     
     
     End
  
  Else
  
     Begin	
     

	WITH tmpCTE (LocationID, ParentLocationID) AS
	(
		SELECT LocationID,ParentLocationID 
		FROM tbllocations
		WHERE locationID = (select LocationID from tblEmployeeLocations Where EmployeeID=@CreatedBy and IsCurrent=1)
		UNION ALL
		SELECT l.LocationID, l.ParentLocationID
		FROM tbllocations l INNER JOIN tmpCTE t	
		ON l.ParentLocationID = t.locationID
	)

	select tmpCTE.LocationID,tmpCTE.ParentLocationID into #TBLTEMPLOCATION FROM tmpCTE 

	SELECT tblEmployeeLocations.EmployeeID INTO #TBLTEMPORARY FROM #TBLTEMPLOCATION left outer join tblEmployeeLocations on #TBLTEMPLOCATION.LocationID=tblEmployeeLocations.LocationID
	where tblEmployeeLocations.IsCurrent=1


select distinct  ComplaintId
from 
(

SELECT 
		a.ComplaintId
		
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
 where a.ComplaintId not in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId))

 
 UNION All

 SELECT  
		a.ComplaintId
		
     
 FROM [dbo].[tblDS249Complaints] a 

 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

 a.ComplaintId  in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId) 
 where hc.EmployeeId=@CreatedBy)
 
 
 union All
 
 --- Select those complaints whose charges not
 -- if charges is created by that person.
-- done  # 3 0
 SELECT 
		a.ComplaintId
		
  
 FROM [dbo].[tblDS249Complaints] a 

 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

  c.PersonID = @CreatedBy
  and a.ComplaintId  in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId))


  
 UNION All
 

   SELECT 
		a.ComplaintId
		
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.RespondentEmpID

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
 where a.ComplaintId not in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId))

 
 UNION All
-- done #5 0 
 SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 

 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.RespondentEmpID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

  a.ComplaintId  in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId) 
 where hc.EmployeeId=@CreatedBy)

   UNION  All
   
--***************** Select the complaint Filed at particular Locations
-- done #6 24487
	SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN tblDS249Charges on a.ComplaintId=tblDS249Charges.ComplaintId

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
 where a.ComplaintId not in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=Convert(int,hc.ChargeTypeId))
 and tblDS249Charges.IncidentLocationID in (select #TBLTEMPLOCATION.LocationID from #TBLTEMPLOCATION)

 union All
 -- done #7 0	
 SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 

 inner JOIN tblDS249Charges on a.ComplaintId=tblDS249Charges.ComplaintId
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

  a.ComplaintId  in 
 (
select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=Convert(int,hc.ChargeTypeId) 
 where hc.EmployeeId=@CreatedBy)
 and tblDS249Charges.IncidentLocationID in (select #TBLTEMPLOCATION.LocationID from #TBLTEMPLOCATION)

  union all
  

		SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPLOCATION on a.LocationId=#TBLTEMPLOCATION.locationid

 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
 where a.ComplaintId not in 
 (select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId))

 union All
-- done  #9 0
 SELECT  a.ComplaintId
     
 FROM [dbo].[tblDS249Complaints] a 

  inner JOIN #TBLTEMPLOCATION on a.LocationId=#TBLTEMPLOCATION.locationid
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID   
 where 

 a.ComplaintId  in 
 (
select  dc.ComplaintId from tblDS249Charges dc inner join tblHideCharges hc on 
 dc.ChargeTypeID=convert(int,hc.ChargeTypeId) 
 where hc.EmployeeId=@CreatedBy)

  ) tbl order by ComplaintId

  
  drop table #TBLTEMPLOCATION
  drop table #TBLTEMPORARY
  


End  
  
END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary3]    Script Date: 07/31/2012 09:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary3]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_RptGetDisciplinary3]
	-- Add the parameters for the stored procedure here
	
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
select distinct p.FirstName+ '' ''+p.LastName Name ,
		CONVERT(date,adv.HearingDt) HearingDate  
		from tblDS249Complaints comp inner join
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
				on ca.AdvocacyCaseId=ah.AdvocacyCaseID
				where ah.CalStatusCd  in (47) and  
	 HearingTypeCd =84	and		
	
					 cast(HearingDt as DATE) >=@FromDate
	 and cast(HearingDt as DATE) <=@ToDate  
 		        and YEAR(ah.HearingDt) =year(@ToDate)
			    and ca.IsAvailable is null
			    and ah.HearingDt is not null	
			    and ca.FinalStatuscd =96			    
			    ) adv
			   
		on comp.ComplaintId=adv.ComplaintId
		inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		inner join tblPersons p on p.PersonID=emp.PersonID		 
		order by HearingDate asc 
END















' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary2]    Script Date: 07/31/2012 09:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary2]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_RptGetDisciplinary2]
	
-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	Declare @CountAdvocateYearly INT 
	Declare @CountFieldYearly INT 
	Declare @CountMedicalYearly INT 
	Declare @CountSSLUYearly INT 
	Declare @CountDATYearly INT 
	Declare @CountTrainingYearly INT 
	Declare @Month int
	Declare @Year int
	SET NOCOUNT ON;
	declare @yearStart DATE
	SET @yearStart=CONVERT(DATE, ''01/01/''+ CONVERT(VARCHAR,YEAR(CONVERT(DATE, @FromDate))));
   
	--************************************ 
	 select @CountAdvocateYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and cast(compAdvocacy.RecieveDate as DATE)>= @yearStart
		and cast(compAdvocacy.RecieveDate as DATE)<=@ToDate
		and complanant.LocationId in (519,512)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR”( 8275760) 
	 and complanant.ComplainantType=''C''
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 
	 
	  
	 select @CountFieldYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and cast(compAdvocacy.RecieveDate as DATE)>= @yearStart
		and cast(compAdvocacy.RecieveDate as DATE)<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging Field
	    and complanant.ComplainantType=''C''
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 
	 
	  
	 select @CountMedicalYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 		
	and cast(compAdvocacy.RecieveDate as DATE)>= @yearStart
		and CAST(compAdvocacy.RecieveDate as DATE)<=@ToDate
		and complanant.LocationId=565  --  Location belonging 	MEDICAL OFFICE 
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 
	 
	   select @CountSSLUYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 		
	and cast(compAdvocacy.RecieveDate as DATE)>= @yearStart
		and CAST(compAdvocacy.RecieveDate as DATE)<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 
	 
	 set @CountDATYearly=0
	
	 	 
	select @CountTrainingYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and cast(compAdvocacy.RecieveDate as DATE)>= @yearStart
		and cast(compAdvocacy.RecieveDate as DATE)<=@ToDate
		and complanant.LocationId in (14,15,16)  --16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 

-- Complaints disposed 

	Declare @AwaitingTrialDateAtOath int
	Declare @CountCasesScheduled int
	Declare @CountCasesHeard int	
	Declare @CountDispostionFromOath int 
	
	Declare @CountAwaitingAdjudicationComplaintsNo int
	Declare @CountAwaitingAdjudicationComplaintsEmp int
	Declare @CountAwaitingOathTrialComplaintsNo int
	Declare @CountAwaitingOathTrialComplaintsEmp int

	
	
	--Updated on 5/22/2012
	
	--Awaiting trial date at OATH
	
	select @AwaitingTrialDateAtOath=COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
	ca.FinalStatuscd=96  -- Open Complaints
	 	and ah.HearingStatusCd=65 	
	and ca.IsAvailable is null

	

	
	--Updated on 5/22/2012
	
	--Cases scheduled for 
	
	
	select   @CountCasesScheduled=COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ah.CalStatusCd=47   -- SCHED			
		and cast(HearingDt as DATE) >=@FromDate 
	and cast(HearingDt as DATE) <=@ToDate 
	and ca.IsAvailable is null
	
	
	--Updated on 5/22/2012
	
	--Cases heard for 
	
	
	
select @CountCasesHeard= COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
		 ah.HearingStatusCd in (54,55,56,57,58)   -- SCHED			
		and cast(HearingDt as DATE) >=cast(@FromDate  as DATE)
	and cast(HearingDt as DATE)<=cast(@ToDate as DATE)
	and ca.IsAvailable is null
	
	

--Updated on 5/22/2012
	
	--Cases SCHEDULED for OATH
   
	
	If(MONTH(@ToDate)<=11)
	
		BEGIN
			set @Month=MONTH(@ToDate)+1
			set @Year=YEAR(@ToDate)
		END
	ELSE
			BEGIN
				set @Month=1
				set @Year=YEAR(@ToDate)+1
			END
	
	

select distinct CONVERT(date,HearingDt) HearingDt into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where month(HearingDt)=@Month 
	and year(HearingDt) =@Year
	and ca.IsAvailable is null	
	and  ah.CalStatusCd in (47,50)




--select * from #tblHearingDate 

	Declare @CaseScheduledDates varchar(800)
		select @CaseScheduledDates= 
		coalesce(@CaseScheduledDates+'','','''')+ CONVERT(varchar, HearingDt,101) 
		from   #tblHearingDate 
   
	
	
		
--Cases Awaiting Disposition
		
		select @CountDispostionFromOath=COUNT(*) 
				from tblDS249Complaints comp inner join
				
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
			on ca.AdvocacyCaseId=ah.AdvocacyCaseID

			where 
 			 ah.HearingStatusCd=55   -- H Held Desc Reserved.
 			and cast(HearingDt as DATE) >=@FromDate 
			and cast(HearingDt as DATE) <=@ToDate
			and ca.IsAvailable is null
			
			) adv
			on comp.ComplaintId=adv.ComplaintId
			inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
			inner join tblPersons p on p.PersonID=emp.PersonID
	
			
		
		
		
	--Updated on 5/22/2012
	
	--Awaiting Adjudictaion at Dept Trials-Complaints
	
	
	select @CountAwaitingAdjudicationComplaintsNo=COUNT(*) from tblComplaintAdvocacy ca
inner join tblAdvocateHearing ah on ah.AdvocacyCaseID=ca.AdvocacyCaseId 
inner join tblDS249Complaints comp on comp.ComplaintId =ca.ComplaintId 
where FinalStatuscd in (96) and HearingDt is null and IsAvailable is null

	
	
	--Updated on 5/22/2012
	
	--Awaiting Adjudictaion at Dept Trials-Employees	
	

select @CountAwaitingAdjudicationComplaintsEmp=
 COUNT(distinct RespondentEmpID ) from tblDS249Complaints comp 						
inner join tblComplaintAdvocacy ca	on comp.ComplaintId=ca.ComplaintId					
inner join tblAdvocateHearing ah on ah.AdvocacyCaseID=ca.AdvocacyCaseId 						
where FinalStatuscd in (96) and HearingDt is null and IsAvailable is null						

--		--Updated on 5/22/2012
	
--	--Awaiting OATH Trial at Dept Trials-Complaints		
	
--	select @CountAwaitingOathTrialComplaintsNo=COUNT(*) from tblComplaintAdvocacy ca						
--inner join tblAdvocateHearing ah on ah.AdvocacyCaseID=ca.AdvocacyCaseId 						
--inner join tblDS249Complaints comp on comp.ComplaintId =ca.ComplaintId 						
--where FinalStatuscd in (96) and
-- ah.HearingStatusCd in (65) 
--and HearingDt is null and IsAvailable is null	
	
	select  @CountAwaitingOathTrialComplaintsNo=COUNT(distinct ca.AdvocacyCaseId) 
from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
on ca.AdvocacyCaseId=ah.AdvocacyCaseID
where ah.HearingStatusCd in (65)   
and FinalStatuscd in (96)					
and ca.IsAvailable is null
	
	
--Updated on 5/22/2012
	
--Awaiting OATH Trial at Dept Trials-Employees
			
	select  @CountAwaitingOathTrialComplaintsEmp=COUNT(distinct comp.RespondentEmpID) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
			on comp.ComplaintId=ca.ComplaintId
			inner join
			(
			select distinct ca.AdvocacyCaseId 
			from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
			on ca.AdvocacyCaseId=ah.AdvocacyCaseID
			where ah.HearingStatusCd in (65)   
			and FinalStatuscd in (96)		
			and ca.IsAvailable is null
			) adv on ca.AdvocacyCaseId=adv.AdvocacyCaseId
	
	
		Declare @DiscretionaryReport table
     (
      CountAdvocateYearly int,
      CountMedicalYearly int,
      CountSSLUYearly int,
      CountDATYearly int,
      CountTrainingYearly INT,
      CountFieldYearly int,
	  AwaitingTrialDateAtOath int,
	  CountCasesScheduled int,
	  CountCasesHeard int	,
      CountDispostionFromOath int, 
	  CaseScheduledDates varchar(800),
      CountAwaitingAdjudicationComplaintsNo int,
	  CountAwaitingAdjudicationComplaintsEmp int,
	  CountAwaitingOathTrialComplaintsNo int,
	  CountAwaitingOathTrialComplaintsEmp int
      )


  insert @DiscretionaryReport 
  values(@CountAdvocateYearly,@CountMedicalYearly,@CountSSLUYearly,
   @CountDATYearly,@CountTrainingYearly,@CountFieldYearly,
   @AwaitingTrialDateAtOath ,@CountCasesScheduled ,
   @CountCasesHeard ,@CountDispostionFromOath ,
   @CaseScheduledDates ,@CountAwaitingAdjudicationComplaintsNo ,
   @CountAwaitingAdjudicationComplaintsEmp ,
   @CountAwaitingOathTrialComplaintsNo,
   @CountAwaitingOathTrialComplaintsEmp
  )
  select * from @DiscretionaryReport
	

END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_RptGetDisciplinary1]    Script Date: 07/31/2012 09:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetDisciplinary1]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_RptGetDisciplinary1]
	-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @CountNotAdjudicated INT 
	Declare @ComplaintsReceived INT 
	Declare @CountAdvocateMonthly INT 
	Declare @CountFieldMonthly INT 
	Declare @CountMedicalMonthly INT 
	Declare @CountSSLUMonthly INT 
	Declare @CountDATMonthly INT 
	Declare @CountTrainingMonthly INT 
	SET NOCOUNT ON;
	
	
		--Updated on 5/22/2012
	
	--NotAdjudicated
	     
	select @CountNotAdjudicated=COUNT(*) from
(
-- Only open Complaints where hearing , actioncd,appeal and penalty is null
select distinct(comp.indexno)
      from tblDS249Complaints comp inner join 
      tblComplaintAdvocacy ca on ca.ComplaintId =comp.ComplaintId 
      left join tblAdvocateHearing ah on ah.AdvocacyCaseID = ca.AdvocacyCaseId 
      left join tblAdvocatedPenalty ap on ap.AdvocacyCaseID =ca.AdvocacyCaseId 
      --left join tblEmployees emp on emp.EmployeeID  =comp.RespondentEmpID 
      --left join tblAdvocateAppeal aapp on aapp.AdvocacyCaseID=ca.AdvocacyCaseId
     
      WHERE 
      RecieveDate is not null and 
      FinalStatuscd=96
      --and cast(ActionDate as DATE) <= ''07/16/2012'' 
      and (ca.Actioncd =-1 or ca.Actioncd is null)
      and ca.ClosedDate is null
      and IsAvailable is null
      and  nullif(ca.trialreturnstatuscd ,-1) is null
      and nullif( ah.CalStatusCd, -1) is null
      and nullif(ah.HearingTypeCd,-1) is null
      and nullif(ah.HearingStatusCd,-1)is null     
      and nullif(ap.PenaltyTypeCd ,-1) is null      
      --and nullif(aapp.appealresultcd ,-1) is null
      
      and ah.HearingDt is null  
     --and (ah.HearingDt) is   null

union
-- Only open Complaints where hearing , actioncd , appeal and penalty is null and action is calendar
select distinct(comp.indexno)
      from tblDS249Complaints comp inner join 
      tblComplaintAdvocacy ca on ca.ComplaintId =comp.ComplaintId 
      left join tblAdvocateHearing ah on ah.AdvocacyCaseID = ca.AdvocacyCaseId
      left join tblAdvocatedPenalty ap on ap.AdvocacyCaseID =ca.AdvocacyCaseId 
      --inner join tblEmployees emp on emp.EmployeeID  =comp.RespondentEmpID 
      left join tblAdvocateAppeal aapp on aapp.AdvocacyCaseID=ca.AdvocacyCaseId
      --inner join tblPersons p on p.PersonID =emp.PersonID 
      WHERE 
      RecieveDate is not null and 
      FinalStatuscd=96
      --and cast(ActionDate as DATE) <= ''07/16/2012'' 
      and (ca.Actioncd =44)
      and IsAvailable is null
      and  nullif(ca.trialreturnstatuscd ,-1) is null
      and nullif( ah.CalStatusCd, -1) is null
      and nullif(ah.HearingTypeCd,-1) is null
      and nullif(ah.HearingStatusCd,-1)is null
      and nullif(ap.PenaltyTypeCd ,-1) is null
      and ca.ClosedDate is null
      and nullif(aapp.appealresultcd ,-1) is null
      and (ah.HearingDt) is   null

 union     
 
 select distinct(comp.indexno)
      from tblDS249Complaints comp inner join 
      tblComplaintAdvocacy ca on ca.ComplaintId =comp.ComplaintId 
      left join tblAdvocateHearing ah on ah.AdvocacyCaseID = ca.AdvocacyCaseId
      left join tblAdvocatedPenalty ap on ap.AdvocacyCaseID =ca.AdvocacyCaseId 
      inner join tblEmployees emp on emp.EmployeeID  =comp.RespondentEmpID 
      inner join tblAdvocateAppeal aapp on aapp.AdvocacyCaseID=ca.AdvocacyCaseId
      inner join tblPersons p on p.PersonID =emp.PersonID 
      WHERE 
      RecieveDate is not null and 
      FinalStatuscd=96
      --and cast(ActionDate as DATE) <= ''07/16/2012'' 
      and (ca.Actioncd =44)
      and IsAvailable is null
      and  nullif(ca.trialreturnstatuscd ,-1) is null
      and  ah.CalStatusCd=50 -- CalStatusCd= Sched
      and ah.HearingTypeCd=84 -- hearing type= Intial Schedule
      and ah.HearingStatusCd=64 -- hearing status
      and nullif(ap.PenaltyTypeCd ,-1) is null
      and ca.ClosedDate is null
      --and nullif(aapp.appealresultcd ,-1) is null
      and (ah.HearingDt) is not  null

 union    
     
   select distinct(comp.indexno)
      from tblDS249Complaints comp inner join 
      tblComplaintAdvocacy ca on ca.ComplaintId =comp.ComplaintId 
      left join tblAdvocateHearing ah on ah.AdvocacyCaseID = ca.AdvocacyCaseId
      left join tblAdvocatedPenalty ap on ap.AdvocacyCaseID =ca.AdvocacyCaseId 
      inner join tblEmployees emp on emp.EmployeeID  =comp.RespondentEmpID 
      inner join tblAdvocateAppeal aapp on aapp.AdvocacyCaseID=ca.AdvocacyCaseId
      inner join tblPersons p on p.PersonID =emp.PersonID 
      WHERE 
      RecieveDate is not null and 
      FinalStatuscd=96
      --and cast(ActionDate as DATE) <= ''07/16/2012'' 
      and (ca.Actioncd =44)
      and IsAvailable is null
      and  nullif(ca.trialreturnstatuscd ,-1) is null
       and  ah.CalStatusCd=50 -- CalStatusCd= Sched
      and ah.HearingTypeCd=84 -- hearing type= Intial Schedule
      and ah.HearingStatusCd=64 -- hearing status
      and ap.PenaltyTypeCd in (174,170,176)
      and ca.ClosedDate is null
      ----and nullif(aapp.appealresultcd ,-1) is null
      and (ah.HearingDt) is not  null
     
  )   a
     
      
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	  SELECT @ComplaintsReceived=COUNT( distinct ca.ComplaintId)
	 FROM tblComplaintAdvocacy ca inner join tblDS249Complainant cn
	 on ca.ComplaintId=cn.ComplaintId
	 WHERE FinalStatuscd in (96,97)
		and IsAvailable is null 
	   and cast(RecieveDate as DATE)>=@FromDate
		and cast(RecieveDate as DATE)<=@ToDate 
		and cn.ComplainantType=''C''
		and cn.LocationId is not null 
		and cn.LocationId <>-1
			
	
	--************************************ 
	 select @CountAdvocateMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and cast(compAdvocacy.RecieveDate as DATE)>=@FromDate 
		and cast(compAdvocacy.RecieveDate as DATE)<=@ToDate 
		and complanant.LocationId in (512,519)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR”( 8275760) 
	 and complanant.ComplainantType=''C''
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	
	 
	  
	 select @CountFieldMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null		
	and cast(compAdvocacy.RecieveDate as DATE)>=@FromDate 
		and cast(compAdvocacy.RecieveDate as DATE)<=@ToDate 
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging to field
	    and complanant.ComplainantType=''C''
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	
	 
	  
	 select @CountMedicalMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and cast(compAdvocacy.RecieveDate as DATE)>=@FromDate
		and cast(compAdvocacy.RecieveDate as DATE)<=@ToDate
		and complanant.LocationId=565  --  Location belonging to MEDICAL 
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	
	 
	   select @CountSSLUMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and cast(compAdvocacy.RecieveDate as DATE)>=@FromDate 
		and cast(compAdvocacy.RecieveDate as DATE)<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType=''C''
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 
	 
	 set @CountDATMonthly=0
	
		 
		select @CountTrainingMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
		on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and cast(compAdvocacy.RecieveDate as DATE)>=@FromDate
		and cast(compAdvocacy.RecieveDate as DATE)<=@ToDate 
		and complanant.LocationId in (14,15,16)  -- 16 Location belonging Training and 14,15 are child locations
		and complanant.ComplainantType=''C''
		and complanant.LocationId is not null 
		and complanant.LocationId <>-1

-- Complaints disposed 

	Declare @CountPleadGuilty int
	Declare @CountDismissal int
	Declare @CountBCAD int
	Declare @CountInformalConferences int
	Declare @CountVoids int
	Declare @Retired_Resigned_Terminated int
	Declare @TotalComplaintDisposed int
	Declare @ComplaintRemainingOpened int
	Declare @BcadRouteCount int
    Declare @BcadActionCount int


	select @CountPleadGuilty = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah							
on ca.AdvocacyCaseId=ah.AdvocacyCaseID							
where ca.FinalStatuscd=97  -- Closed Complaints							
and ah.HearingStatusCd=64   -- Plead Guilty		
	and cast(ah.HearingDt as DATE)>=@FromDate 
	and cast(ah.HearingDt as DATE)<=@ToDate 
	and ca.IsAvailable is null	
	
	
		select @CountDismissal = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		where ca.FinalStatuscd=97  -- Closed Complaints
		and ah.HearingStatusCd=62   -- Motion to Dismiss
		and cast(ah.HearingDt as DATE)>=@FromDate 
		and cast(ah.HearingDt as DATE)<=@ToDate
		and ca.IsAvailable is null


		select @CountBCAD= COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
		where Actioncd=39 
		and cast(ActionDate as DATE)>=@FromDate 
		and cast(ActionDate as DATE)<=@ToDate 
	
				
		select @CountInformalConferences = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
		where Actioncd=40 
		and	FinalStatuscd=97 
		and IsAvailable is null
		and cast(ActionDate as DATE)>=@FromDate 
		and cast(ActionDate as DATE)<=@ToDate 
		
		--Updated on 5/22/2012
	
	--Voids	
		
		
	select @CountVoids=	count(distinct(comp.ComplaintId)) from							
tblDS249Complaints comp inner join 							
tblComplaintAdvocacy ca on ca.ComplaintId =comp.ComplaintId 							
inner join tblAdvocatedPenalty ap on ap.AdvocacyCaseID =ca.AdvocacyCaseId 							
inner join  tblAdvocateHearing ah on ah.AdvocacyCaseID =ca.AdvocacyCaseId							
where ap.PenaltyTypeCd =175 and ca.FinalStatuscd =97							
and ca.IsAvailable is null and cast(ah.HearingDt as DATE) >=@FromDate 						
and cast(ah.HearingDt as DATE) <=@ToDate and ca.Actioncd =44							
and ah.CalStatusCd in(50,51) and ah.HearingDt is not null 	
		
		
		
		
		

		select @Retired_Resigned_Terminated = COUNT(distinct ca.AdvocacyCaseId) from    
		 tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		where ca.FinalStatuscd=97  -- Closed Complaints
		and (ah.HearingStatusCd=71 or ah.HearingStatusCd=67 or ah.HearingStatusCd=165 or ah.   HearingStatusCd=166) -- Retired /Resigned /Terminated	
		and ca.IsAvailable is null
		and cast(HearingDt as DATE)>=@FromDate
		and cast(HearingDt as DATE)<=@ToDate 


		set @TotalComplaintDisposed=@CountPleadGuilty+@CountDismissal+@CountBCAD+
		@CountInformalConferences+@CountVoids + @Retired_Resigned_Terminated
	
	
	select @ComplaintRemainingOpened = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=96 
	and IsAvailable is null	
	and convert(date,ModifiedDate,101) <=@ToDate


		select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
		from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		where ca.FinalStatuscd=96  -- Open Complaints
		and cast(ah.HearingDt as DATE) >=@FromDate 
		and cast(ah.HearingDt as DATE) <=@ToDate
		and ca.IsAvailable is null		
		and ah.CalStatusCd= 50 --SCHED


		Declare @ConferenceCalendarDates varchar(800)
		select @ConferenceCalendarDates= 
		coalesce(@ConferenceCalendarDates+'','','''') +  convert( varchar,HearingDt,101) --cast
		from   #tblHearingDate 

	Declare @DiscretionaryReport table
     (CountNotAdjudicated int ,
      ComplaintsReceived int,
      CountAdvocateMonthly int,
      CountMedicalMonthly int,
      CountSSLUMonthly int,
      CountDATMonthly int,
      CountTrainingMonthly int,
      CountFieldMonthly int,
      CountPleadGuilty int,
	  CountDismissal int,
	  CountBCAD int,
	 CountInformalConferences int,
	 CountVoids int,
	 Retired_Resigned_Terminated int,
	 TotalComplaintDisposed int,
	 ComplaintRemainingOpened int,
	 ConferenceCalendarDates varchar(800)
      )


  insert @DiscretionaryReport 
  values(@CountNotAdjudicated,@ComplaintsReceived,@CountAdvocateMonthly,
   @CountMedicalMonthly,@CountSSLUMonthly,@CountDATMonthly,@CountTrainingMonthly,
   @CountFieldMonthly,
   @CountPleadGuilty ,@CountDismissal ,@CountBCAD ,@CountInformalConferences ,
   @CountVoids ,@Retired_Resigned_Terminated ,@TotalComplaintDisposed ,
   @ComplaintRemainingOpened ,@ConferenceCalendarDates
  )
  select * from @DiscretionaryReport

END





' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetProbationEmployees]    Script Date: 07/31/2012 09:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetProbationEmployees]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- [dbo].[usp_GetprobationEmployees] ''Bcad'',08/21/2008,08/31/2012
CREATE PROCEDURE [dbo].[usp_GetProbationEmployees]
@RoutingLocation varchar(10),
@CreatedStartDate date,
@CreatedEndDate date
AS

BEGIN
DECLARE  @sqlStr varchar(1000)=''''


	
	
	SELECT ( case when C.RoutingLocation=''A'' then ''ADVOCATE'' when c.RoutingLocation=''B'' then ''BCAD'' 
	when c.RoutingLocation=''M'' then ''MEDICAL'' else '''''''' end) as ''RoutingLocation'',p.LastName ,P.FirstName ,C.IndexNo ,C.RespondentRefNumber,C.ComplaintId ,c.CreatedDate,ISNULL(L.LocationName,''-'') AS	DistrictName, ISNULL(LB.LocationName, ''-'') Zone 
 into #temp
	FROM  tblDS249Complaints C INNER JOIN tblEmployees E ON E.EmployeeID = C.RespondentEmpID INNER JOIN tblLocations L ON L.LocationId = C.LocationId	LEFT  JOIN tblLocations LB ON LB.LocationId = C.BoroughID INNER JOIN tblPersons P ON P.PersonID = E.PersonID
		WHERE IsProbation =1
	
	
	if(@RoutingLocation =''ADVOCATE'' or @RoutingLocation=''BCAD'' or @RoutingLocation=''MEDICAL'')
	begin
		select * from #temp where CAST(createddate as DATE)>=@CreatedStartDate and 
	CAST(createddate as DATE)<=@CreatedEndDate  and Routinglocation=@RoutingLocation order by createddate desc
	end
	else if(@RoutingLocation=''DS249'')
	begin
	select * from #temp where CAST(createddate as DATE)>=@CreatedStartDate and 
	CAST(createddate as DATE)<=@CreatedEndDate  and Routinglocation in (''ADVOCATE'',''BCAD'',''MEDICAL'','' '',null) order by createddate desc
	end 
	
END




' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaint]    Script Date: 07/31/2012 09:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaint]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetMedicalComplaint]
	-- Add the parameters for the stored procedure here
@ComplaintID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT  MedicalCaseId,
			med.ComplaintId,
			CreatedUser,
			IsProbation ,
			med.CreatedDate,
			ModifiedUser,
			ModifiedDate,
			FinalStatuscd,
			NonPenaltyResultcd,
			AdjudicationBodycd,
			AdjudicationDate,
			IsAvailable		   
	from tblMedicalComplaint med	
	inner join tblDS249Complaints comp on comp.ComplaintId =med.ComplaintId
	Where med.ComplaintId = @ComplaintID And IsAvailable is null
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaint]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaint]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetBCADComplaint]
	-- Add the parameters for the stored procedure here
@ComplaintID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT BCADCaseId,
			bcad.ComplaintId,
			CreatedUser,
			comp.IsProbation 
			,bcad.CreatedDate,
			ModifiedUser,
			ModifiedDate,
			FinalStatuscd,
			NonPenaltyResultcd,
			AdjudicationBodycd,
			AdjudicationDate,
			IsAvailable		   
	from tblBCADComplaint bcad
	inner join tblDS249Complaints comp on comp.ComplaintId =bcad.ComplaintId
	Where bcad.ComplaintId = @ComplaintID And IsAvailable is null
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

--[usp_GetComplaintsBySearch] 10045,0, 50,0,'''',''01/02/2010'','''',64
CREATE PROCEDURE [dbo].[usp_GetComplaintsBySearch]
	-- Add the parameters for the stored procedure here
@CreatedBy int=0,
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@RowCount int OUTPUT 
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @numericStringValue varchar(50)
	DECLARE  @chargesLikeValue varchar(22)
	DECLARE @stringLikeValue varchar(52)
	DECLARE @FirstRow INT 
	DECLARE @LastRow INT
	DECLARE @count INT
	declare @FullStop char(2) = ''. ''
	declare @Blank char(1) = ''''
	
	SELECT   @chargesLikeValue = ''%'' +@chargesValue + ''%'' 
			,@stringLikeValue = ''%'' + @stringValue +  ''%'' 
			,@numericStringValue =''%'' +CAST( @numericValue as varchar) + ''%''
			,@FirstRow = (@PageNo * @maximumRows) + 1
			,@LastRow = (@PageNo * @maximumRows) + @maximumRows ;  


	
	select @count=COUNT(m.Id) 
	from tblMembershipRoles m 
	where 
		PersonalID = @CreatedBy 
		and m.RoleId = 10 --RoleID 10 = ''Commissioner Access''
	
	if @count>=1
		Begin		
		if @numericValue != 0  
				Begin					
					SELECT 
						ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
						[ComplaintId]
						,IndexNo
						,a.isprobation						
						,a.[LocationId]
						,a.[BoroughID]
						,a.promotiondate
						,a.LocationName
						,(case when a.complaintStatus >=4 then  a.RoutingLocation
							when a.RoutingLocation =''P'' then  a.RoutingLocation
							else null 
						end) as RoutingLocation
						,[RespondentEmpID]
						,[RespondentRefNumber]      
						,[Approved]      
						,CreatedBy      
						,CONVERT(varchar,CreatedDate,101) as CreatedDate
						--,'''' as Status
						,charges as Charges
						,Chargesdesc as ChargesDesc
						, RespondentFName
						,RespondentLName
						,A.complaintStatus
						,A.complaintStatusDate
						,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
								else 0  end) as Indicator,
						a.IsDeleted,
						a.jt1,0  as FinalStatusCD 
					into #Complaints5  
					FROM dbo.vw_ComissionerAcess a 			 
					where 
						IndexNo =@numericValue
						OR RespondentRefNumber like @numericStringValue
						or isnull(a.jt1,@Blank) like @numericStringValue
					order by a.CreatedDate desc

					SELECT  * 
					FROM  #Complaints5 
					where ROWID BETWEEN @FirstRow AND @LastRow 
					ORDER BY ROWID asc     

					SELECT @RowCount=COUNT([ComplaintId]) 
					FROM #Complaints5

					drop table #Complaints5  

				End
				else if @chargesValue != @Blank 
						Begin
							SELECT 
								ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
								[ComplaintId]
								,IndexNo
								,a.isprobation
								,a.[LocationId]
								,a.[BoroughID]
								,a.promotiondate
								,a.LocationName
								,(case when a.complaintStatus >=4 then  a.RoutingLocation
										when a.RoutingLocation =''P'' then  a.RoutingLocation
										else null 
										end) as RoutingLocation
								,[RespondentEmpID]
								,[RespondentRefNumber]      
								,[Approved]      
								, CreatedBy      
								,CONVERT(varchar,CreatedDate,101) as CreatedDate
								--,@Blank as Status
								,charges as Charges
								,Chargesdesc as ChargesDesc
								, RespondentFName
								,RespondentLName
								,A.complaintStatus
								,A.complaintStatusDate
								,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
										else 0  end) as Indicator
								,a.IsDeleted
								,a.jt1
								,0  as FinalStatusCD 
							into #Complaints6  
							FROM dbo.vw_ComissionerAcess a 
							where 
								Charges like @chargesLikeValue
							order by  a.CreatedDate desc

							SELECT  * 
							FROM  #Complaints6 
							where ROWID BETWEEN @FirstRow AND @LastRow  
							ORDER BY ROWID asc  
							
							SELECT @RowCount=COUNT([ComplaintId]) 
							FROM #Complaints6
							
							drop table #Complaints6     
						End	
				 else if @complaintDate != @Blank
						Begin
							SELECT 	  
								ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
								[ComplaintId]
								,IndexNo
								,a.Isprobation
								,a.[LocationId]
								,a.[BoroughID]
								,a.promotiondate
								,a.LocationName
								,(case when a.complaintStatus >=4 then  a.RoutingLocation
									when a.RoutingLocation =''P'' then  a.RoutingLocation
									else null 
									end) as RoutingLocation
								,[RespondentEmpID]
								,[RespondentRefNumber]      
								,[Approved]      
								,CreatedBy      
								,CONVERT(varchar,CreatedDate,101) as CreatedDate
								--,@Blank as Status
								,charges as Charges
								,Chargesdesc as ChargesDesc
								, RespondentFName
								,RespondentLName
								,A.complaintStatus
								,A.complaintStatusDate
								,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
										else 0  end) as Indicator
								,a.IsDeleted
								,a.jt1
								,0  as FinalStatusCD 
							  into #Complaints7  
						 FROM dbo.vw_ComissionerAcess a 
						 where  cast(CreatedDate as date) = CAST(@complaintDate as date)
						 order by  a.CreatedDate desc
						 	

						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #Complaints7
						
						SELECT  * 
						FROM  #Complaints7  
						where ROWID BETWEEN @FirstRow AND @LastRow  
						ORDER BY ROWID asc   
						
						drop table #Complaints7 
												
					End	
				 else if @stringValue != @Blank
						Begin	
						   SELECT 
									ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,       
									[ComplaintId]
									,IndexNo
									,a.IsProbation
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,a.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation
											when a.RoutingLocation =''P'' then  a.RoutingLocation
											else null 
											end) as RoutingLocation
									,[RespondentEmpID]
									,[RespondentRefNumber]      
									,[Approved]      
									,CreatedBy      
									,CONVERT(varchar,CreatedDate,101) as CreatedDate
									--, @Blank as Status
									,charges as Charges
									,Chargesdesc as ChargesDesc
									, RespondentFName
									,RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
											else 0  end) as Indicator
									,a.IsDeleted
									,a.jt1
									,0  as FinalStatusCD 
						 into #Complaints8  
						 FROM dbo.vw_ComissionerAcess a 					 
						 where 
								
								 dbo.fn_StripCharacters(RespondentFName,''^a-z0-9A-Z'')
										like  									
										
										''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
										OR 
										 dbo.fn_StripCharacters(RespondentLName,''^a-z0-9A-Z'') 
										like										 
									 ''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
								OR  
								dbo.fn_StripCharacters(CreatedBy,''^a-z0-9A-Z'')								
								like
								 ''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''								
								
								  
								OR LocationName like @stringLikeValue 
								or isnull(a.jt1,@Blank) like @stringLikeValue
						 order by  a.CreatedDate desc
							
						SELECT  * 
						FROM  #Complaints8  
						where ROWID BETWEEN @FirstRow AND @LastRow  
						ORDER BY ROWID asc  
						
						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #Complaints8
						
						drop table #Complaints8    
						 
				   End	
			End
	Else
		Begin				
				create table #Complaints(ComplaintId bigint)     
				
				insert  into #Complaints
							exec dbo.[usp_GetComplaintsSecurity] @CreatedBy;
							
				if @numericValue != 0  
					Begin		
						SELECT 
							a.[ComplaintId]
							,a.IndexNo
							,a.IsProbation
							,a.[LocationId]
							,a.[BoroughID]
							,a.promotiondate
							,b.LocationName
							,(case when a.complaintStatus >=4 then  a.RoutingLocation			
									else null 
									end) as RoutingLocation
							,a.[RespondentEmpID]
							,a.[RespondentRefNumber]      
							,a.[Approved]
							,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
							,a.[CreatedDate]
							--, @Blank as Status
							,a.Charges as Charges
							,a.Chargesdesc as ChargesDesc
							, a.RespondentFName
							,a.RespondentLName
							,A.complaintStatus
							,A.complaintStatusDate
							,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end)as Indicator
							,a.IsDeleted
							,a.Jt1
							,0  as FinalStatusCD 
							,ROWID =IDENTITY(INT,1,1) 
						into #tempNumeric
						FROM 
							[dbo].[tblDS249Complaints] a 
							inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
							LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
							LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
						where 
							IndexNo = @numericValue 
							OR RespondentRefNumber like @numericStringValue 
							or jt1 like   @numericStringValue
						order by a.CreatedDate desc

						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #tempNumeric
						
						select * from #tempNumeric
						WHERE ROWID BETWEEN @FirstRow AND @LastRow
						ORDER BY  ROWID asc;
						
						drop table #Complaints						
						drop table  #tempNumeric
					End
				else if @chargesValue != @Blank 
						Begin
								SELECT 
									a.[ComplaintId]
									,a.IndexNo
									,a.IsProbation
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,b.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation	
									else null 
									end) as RoutingLocation
									,a.[RespondentEmpID]
									,a.[RespondentRefNumber]      
									,a.[Approved]
									,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy 							     
									,a.[CreatedDate]
									--,@Blank as [Status]
									,a.Charges as Charges
									,a.Chargesdesc as ChargesDesc
									, a.RespondentFName
									,a.RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end) as Indicator,
									a.IsDeleted,
									a.Jt1,0  as FinalStatusCD ,
									ROWID =IDENTITY(INT,1,1) 
								into #tempCharges
								FROM [dbo].[tblDS249Complaints] a 
									inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
									LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
									LEFT OUTER JOIN uv_EmployeePerson c on  a.CreatedBy = c.EmployeeID  
								where Charges like @chargesLikeValue 
								order by  a.CreatedDate desc
								SELECT @RowCount=COUNT([ComplaintId]) FROM #tempCharges

								select * from #tempCharges
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints
								drop table  #tempCharges

						End	
				else if @complaintDate != @Blank
						Begin	
								SELECT 
									a.[ComplaintId]
									,a.IndexNo
									,a.IsProbation
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,b.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation	
									else null 
									end) as RoutingLocation
									,a.[RespondentEmpID]
									,a.[RespondentRefNumber]      
									,a.[Approved]
									,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
									,a.[CreatedDate]
									--, @Blank as Status
									,a.Charges as Charges
									,a.Chargesdesc as ChargesDesc
									, a.RespondentFName
									,a.RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end) as Indicator
									,a.IsDeleted
									,a.Jt1
									,0  as FinalStatusCD
									,ROWID =IDENTITY(INT,1,1) 
								into #tempComplaintDate
								FROM [dbo].[tblDS249Complaints] a 
									inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
									LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
									LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
								where							
									cast(CreatedDate as date) = CAST(@complaintDate as date)
								order by  a.CreatedDate desc



								SELECT @RowCount=COUNT([ComplaintId]) 
								FROM #tempComplaintDate

								select * from #tempComplaintDate
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints 
								drop table  #tempComplaintDate

						End	
				else if @stringValue != @Blank
						Begin
								SELECT 
										a.[ComplaintId]
										,a.IndexNo
										,a.IsProbation 
										,a.[LocationId]
										,a.[BoroughID]
										,a.promotiondate
										,b.LocationName
										,(case when a.complaintStatus >=4 then  a.RoutingLocation
			when a.RoutingLocation =''P'' then  a.RoutingLocation							
												else null 
												end) as RoutingLocation
										,a.[RespondentEmpID]
										,a.[RespondentRefNumber]      
										,a.[Approved]
										,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
										,a.[CreatedDate]
										--, @Blank as [Status]
										,a.Charges as Charges
										,a.Chargesdesc as ChargesDesc
										, a.RespondentFName
										,a.RespondentLName
										,A.complaintStatus
										,A.complaintStatusDate
										,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
												else 0
												end) as Indicator
										,a.IsDeleted
										,a.Jt1
										,0  as FinalStatusCD 
										,ROWID =IDENTITY(INT,1,1) 
								into #tempStringValue
								FROM [dbo].[tblDS249Complaints] a 
										inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
										LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
										LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
								where 
										 
										 dbo.fn_StripCharacters(RespondentFName,''^a-z0-9A-Z'')
										like  									
										
										''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
										OR 
										 dbo.fn_StripCharacters(RespondentLName,''^a-z0-9A-Z'') 
										like										 
									 ''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
										OR LocationName like @stringLikeValue 
										or  jt1 like   @stringLikeValue
										or 
										 dbo.fn_StripCharacters(c.FirstName ,''^a-z0-9A-Z'')
										like 									 
										''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
										or 
										 dbo.fn_StripCharacters(c.LastName,''^a-z0-9A-Z'')
										 like
										''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
										  
								order by  a.CreatedDate desc

								SELECT @RowCount=COUNT([ComplaintId]) 
								FROM #tempStringValue


								select * 
								from #tempStringValue
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints
								drop table  #tempStringValue
						End	 
	END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaints]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE PROCEDURE [dbo].[usp_GetComplaints] 
	-- Add the parameters for the stored procedure here
@CreatedBy int = 0,
@PageNo int = 0,
@maximumRows int = 0,
@rowsCount int OUTPUT
AS
BEGIN
		SET NOCOUNT ON;

		DECLARE @Count int
		
		--SELECT	@Count = COUNT(*) 
		--FROM [dbo].[tblDS249Complaints] a  
		--		LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
		--		LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  

		DECLARE @FirstRow INT
				,@LastRow INT
				
		--SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,
		--		@LastRow = (@PageNo * @maximumRows) + @maximumRows;


		select	@Count=COUNT(Id) 
				,@FirstRow = (@PageNo * @maximumRows) + 1
				,@LastRow = (@PageNo * @maximumRows) + @maximumRows
		from tblMembershipRoles 
		where 
				PersonalID=@CreatedBy 
				and tblMembershipRoles.RoleId= 10 --RoleID 10 =''Commissioner Access''
  
		if @count>=1
		Begin

			SELECT @rowsCount=COUNT(ComplaintId) 
			FROM dbo.vw_ComissionerAcess

			SELECT   
					ComplaintId
					,IndexNo
					,LocationId
					,BoroughID
					,promotiondate
					,LocationName
					,RoutingLocation
					,[RespondentEmpID]
					,[RespondentRefNumber]      
					,[Approved]      
					,CreatedBy      
					,[CreatedDate]
					--, Status
					,Charges
					,ChargesDesc
					, RespondentFName
					,RespondentLName
					,complaintStatus
					,complaintStatusDate
					,Indicator,
					IsDeleted,
					IsProbation,
					ROWID, 
					jt1
					,0 as FinalStatusCD 
			FROM  dbo.vw_ComissionerAcess
			WHERE ROWID BETWEEN @FirstRow AND @LastRow
			ORDER BY ROWID asc;
			
		end
		else
		begin
			create table #Complaints(ComplaintId bigint)
			
			insert  into #Complaints
					exec dbo.[usp_GetComplaintsSecurity] @CreatedBy;

			SELECT @rowsCount=COUNT(ComplaintId) FROM #Complaints;
			
			SELECT 
					a.[ComplaintId]
					,a.IndexNo
					,a.IsProbation 
					,a.[LocationId]
					,a.[BoroughID]
					,a.promotiondate
					,b.LocationName
					,(case when a.complaintStatus >=4 then  a.RoutingLocation
					else null 
					end) as RoutingLocation
					,a.[RespondentEmpID]
					,a.[RespondentRefNumber]      
					,a.[Approved]
					,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
					,a.[CreatedDate]
					--, '''' as ''Status''
					,a.Charges as Charges
					,a.Chargesdesc as ChargesDesc
					, a.RespondentFName
					,a.RespondentLName
					,A.complaintStatus
					,A.complaintStatusDate
					,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
					else 0
					end) as Indicator,
					a.IsDeleted,
					a.Jt1
					,0  as FinalStatusCD ,
					ROWID =IDENTITY(INT,1,1) 
			into #temp
			FROM [dbo].[tblDS249Complaints] a 
				inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
				LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
				LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
			order by a.CreatedDate desc


			select * 
			from #temp
			WHERE ROWID BETWEEN @FirstRow AND @LastRow
			ORDER BY  ROWID asc;
			
			drop table #Complaints
		end

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsForAdmin]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsForAdmin]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetComplaintsForAdmin]

@CreatedBy int = 0,
@PageNo int = 0,
@maximumRows int = 0
AS
BEGIN
	SET NOCOUNT ON;
	
	
SELECT 
			comp.[ComplaintId]		
			,IndexNo
			,IsProbation 
			,ca.FinalStatuscd 
			,comp.[LocationId]
			,comp.BoroughID
			,comp.Jt1 
			,comp.promotiondate
			,b.LocationName
			,(CASE
				 WHEN comp.complaintStatus >=4 
			 THEN  comp.RoutingLocation
				 ELSE null 
			   END) AS ''RoutingLocation''
			,[RespondentEmpID]
			,[RespondentRefNumber]      
			,[Approved]
			,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy''   
			,comp.[CreatedDate]     
			,'''' AS ''Status''
			,charges AS ''Charges''
			,Chargesdesc AS ''ChargesDesc''
			,RespondentFName
			,RespondentLName
			,comp.complaintStatus
			,comp.complaintStatusDate 
			,(CASE 
				WHEN comp.complaintStatus =1 
				THEN dbo.GetBusinessDays(comp.CreatedDate,GETDATE())  
				ELSE 0   
			  END) AS ''Indicator''
				,comp.IsDeleted 
		FROM [dbo].[tblDS249Complaints] comp
			LEFT OUTER JOIN tblLocations b ON comp.LocationId = b.LocationId 
			LEFT OUTER JOIN uv_EmployeePerson c ON convert(bigint,comp.CreatedBy) = c.EmployeeID
			INNER JOIN  tblComplaintAdvocacy ca on ca.ComplaintId =comp.ComplaintId 
		WHERE ca.IsAvailable is null 
			  and RoutingLocation = ''A''
		  
UNION	
	
SELECT 
		comp.[ComplaintId]		
		,IndexNo	
		,IsProbation 	
		,NULL AS ''FinalStatuscd''
		,comp.[LocationId]
		,comp.BoroughID
		,comp.Jt1 
		,comp.promotiondate
		,b.LocationName
		,(CASE WHEN comp.complaintStatus >=4 THEN  comp.RoutingLocation
		ELSE null 
		END) AS ''RoutingLocation''
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,[Approved]
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as ''CreatedBy'' 
		,comp.[CreatedDate]
		,'''' AS ''Status''
		,charges AS ''Charges''
		,Chargesdesc AS ''ChargesDesc''
		,RespondentFName
		,RespondentLName
		,comp.complaintStatus
		,comp.complaintStatusDate 
		,(CASE 
			WHEN comp.complaintStatus =1
			THEN dbo.GetBusinessDays(comp.CreatedDate,GETDATE())
			ELSE 0  
		END) AS ''Indicator'',
		comp.IsDeleted 
	FROM [dbo].[tblDS249Complaints] comp 
			LEFT OUTER JOIN tblLocations b ON comp.LocationId = b.LocationId 
			LEFT OUTER JOIN uv_EmployeePerson c ON convert(bigint,comp.CreatedBy) = c.EmployeeID
	WHERE ISNULL(comp.RoutingLocation, '''')<>''A''
		
  Order BY ComplaintId desc
  
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaint]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaint]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetComplaint]
	-- Add the parameters for the stored procedure here
@ComplaintID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT
		a.ComplaintId
		,ISNULL(a.IndexNo,a.AdvocateIndexNo) as IndexNo
	  ,a.LocationId
	  ,a.BoroughID
	  ,a.promotiondate
      ,[RespondentEmpID]
      ,[RespondentRefNumber] 
      --, (ltrim(rtrim(b.FirstName)) + '' ''+ ltrim(rtrim(b.MiddleName)) + '' ''+ ltrim(rtrim(b.LastName))) as ''RespondentName''
      ,a.RespondentFName
      ,A.RespondentLName
      ,A.RespondentMName
      ,a.TitleId
      ,B.Title
      ,[BadgeNumber] 
      ,[ComplaintActionID]      
      ,[IsSubmitted]
      ,[ChargesServedDate]
      ,[SuspensionOrderedByEmpId]
      ,(ltrim(rtrim(e.FirstName)) + '' ''+ ltrim(rtrim(e.MiddleName)) + '' ''+ ltrim(rtrim(e.LastName))) as ''SuspensionOrderedName''
      , e.ReferenceNo as ''SuspensionOrderedRefNo''
      ,[SuspensionOrderedDt]
      ,[ChargesServedByEmpId]
      ,(ltrim(rtrim(d.FirstName)) + '' ''+ ltrim(rtrim(d.MiddleName)) + '' ''+ ltrim(rtrim(d.LastName))) as ''ChargesServedByName''
      , d.ReferenceNo as ''ChargesServedByRefNo''
      ,[SuspensionLiftedByEmpId]
      ,(ltrim(rtrim(c.FirstName)) + '' ''+ ltrim(rtrim(c.MiddleName)) + '' ''+ ltrim(rtrim(c.LastName))) as ''SuspensionLiftedByName''
      , c.ReferenceNo as ''SuspensionLiftedByRefNo''      
      ,[SuspensionLiftedDt]
      ,[RoutingLocation]
      ,[IsProbation]
      ,[Approved]
      ,[ApprovedDate]
      ,[CreatedBy]
      ,[CreatedDate]
      ,a.Comments
      ,f.LocationName
      ,[StreetNumber]
      ,[StreetName]
      ,[AptNo]
      ,[City]
      ,[State]
      ,[Zip]
      ,[zipSuffix]
      ,A.PayCodeId
      ,G.PayCode + ''-'' + G.PayDesc as ''PayDesc''
      ,A.ChartNumber
      ,A.VacationSchedule
      ,A.RespondentDOB
      ,A.IsProbation 
      ,(select	DSNYHIREDATE FROM tblEmployees WHERE EmployeeID=A.RespondentEmpID ) as ''RespondentApptDt'' 
      ,A.complaintStatus
      ,A.complaintStatusDate
      ,(Select MAX(SentDate) from tblRoutingHistory Where ComplaintId=A.ComplaintId)   AS ''SentDate''
  FROM [dbo].[tblDS249Complaints] A INNER JOIN tblTitle B
  on A.TitleId = B.TitleID 
  left join uv_EmployeePerson C 
  ON A.SuspensionLiftedByEmpId  = C.EmployeeID 
  left join uv_EmployeePerson D 
  ON A.ChargesServedByEmpId  = D.EmployeeID 
  left join uv_EmployeePerson E 
  ON A.SuspensionOrderedByEmpId  = E.EmployeeID   
  left outer join tblLocations F
  on A.LocationId = f.LocationId 
  LEFT OUTER JOIN tblPayCode G
  on A.PayCodeID = G.PayCodeId
  --LEFT OUTER JOIN tblEmployeeTitles H
  --on A.TitleId = H.TitleID 	
  
  --Right OUTER JOIN tblRoutingHistory h
  --ON A.ComplaintId =H.ComplaintID
  WHERE A.ComplaintId = @ComplaintID 
  --AND MAX(H.SentDate)
  
  
  
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaintsBySearch]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetBCADComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)

AS
BEGIN
	SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,
      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;        
if @numericValue != 0  
  Begin
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.IsProbation,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted 
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints1  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
	
	drop table #CadComplaints1  
	
  End	
 else if @chargesValue != '''' 
  Begin
  
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.IsProbation,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (Charges like ''%''+@chargesValue+ ''%'') 
	   
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints2  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;	
	
	drop table #CadComplaints2	    
  End
 else if @complaintDate != ''''
  Begin
  SELECT
  		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.IsProbation,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
			,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
          ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	     
 	 SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2, * into #adf0 FROM  #CadComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
		
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf0   where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc; 	 
	
	drop table #CadComplaints3
	drop table #adf0 
  End
 else if @stringValue != ''''
  Begin  
    SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.IsProbation,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges 				
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
       ,i.IncidentDate
         into #CadComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #CadComplaints4  where 
	 
	  dbo.fn_StripCharacters(RespondentFName,''^a-z0-9A-Z'')
										like  									
										
										''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
										OR 
										 dbo.fn_StripCharacters(RespondentLName,''^a-z0-9A-Z'') 
										like										 
									 ''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
								OR  
								dbo.fn_StripCharacters(CreatedBy,''^a-z0-9A-Z'')								
								like
								 ''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''									 
	 
	 OR LocationName like ''%''+@stringValue+ ''%'' 
	 OR Status  like ''%''+@stringValue+ ''%''  
	
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
		
	drop table #CadComplaints4
	drop table #adf
  End  
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetBCADComplaints]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetBCADComplaints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetBCADComplaints] 
@PageNo int = 0,
@maximumRows int = 0
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @Count int
Set @Count = (SELECT COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0)
 
 DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;

 WITH BCadComplaints  AS
(
	SELECT
		ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ISNULL((select top 1 sentdate from tblRoutingHistory 
		where ComplaintId=a.ComplaintId and SentTo=''BCAD'' order by SentDate desc ),a.CreatedDate) ComplaintStatusDate
		,''false'' as Approved 
		, Charges    
		, (select top 1 IncidentDate from tblDS249Incidents 
		where ComplaintId=a.ComplaintId   order by IncidentDate desc ) IncidentDate   
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator, a.IsDeleted 
         
           
        -- ,ROW_NUMBER() over (PARTITION BY r.complaintid order by i.complaintid) as compIndex1     
 FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		--left outer join tblRoutingHistory r on a.ComplaintId =r.ComplaintID 
		
 where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''B'' or a.RoutingLocation = ''M'')
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0 
	    
	    
   

)
		select * from 
		BCadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
		
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateComplaintsBySearch]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetAdvocateComplaintsBySearch] 
	-- Add the parameters for the stored procedure here	
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@rowCount int output
AS
BEGIN

SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
	
if @numericValue != 0  
 Begin
 
 SELECT		
		--ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else ''DATAEASE''
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId
		,a.IsProbation 
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.Charges as charges    
		--,c.LastName + '' ,'' + c.FirstName as ''CreatedBy''
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId		
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = ''1'')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))	
	 and (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue) OR AdvocacyCaseId  like ''%''+Convert(varchar,@numericValue))  
	--ORDER BY ComplaintId desc
	order by ComplaintStatusDate Desc	  
	--SELECT  * FROM  #AdvocateComplaints1 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     	


select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #AdvocateComplaints1 where compIndex = 1) #AdvocateComplaints1
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;	

select @rowCount=COUNT(*) from #AdvocateComplaints1 where compIndex = 1
	
	drop table #AdvocateComplaints1	
 End
 else if @chargesValue != '''' 
 Begin
 
  SELECT
		--ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else ''DATAEASE''
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IsProbation
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		--,c.LastName + '' ,'' + c.FirstName as ''CreatedBy''
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId			
		--once database is updated check only for a.ComplaintStatus=4
		-- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = ''1'')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))	
	and (Charges like ''%''+@chargesValue+ ''%'')
	 --ORDER BY ComplaintId desc
	 ORDER BY ComplaintStatusDate Desc	
	--SELECT  * FROM  #AdvocateComplaints2 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
	

	select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #AdvocateComplaints2 where compIndex = 1) #AdvocateComplaints2
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;

select @rowCount=COUNT(*) from #AdvocateComplaints2 where compIndex = 1

	drop table #AdvocateComplaints2
 End
 else if @complaintDate != ''''
 Begin
   SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else ''DATAEASE''
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IsProbation
		,a.IndexNo as IndexNo
		,a.Jt1 
	    ,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		--,c.LastName + '' ,'' + c.FirstName as ''CreatedBy''
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
          ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId			
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = ''1'')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo )) 	 
	--ORDER BY ComplaintId desc   
	ORDER BY ComplaintStatusDate Desc	
	
	
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,* into #adf0 FROM  #AdvocateComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 		
 	--SELECT * from #adf0 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc    
 		

 	select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #adf0 where compIndex = 1) #adf0
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc; 

select @rowCount=COUNT(*) from #adf0 where compIndex = 1
	
	drop table #AdvocateComplaints3
	drop table #adf0 
 End
 else if @stringValue != ''''
 Begin
 
    SELECT
   		--ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,					
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else ''DATAEASE''
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId
		,a.IsProbation
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		--,c.LastName + '' ,'' + c.FirstName as ''CreatedBy''
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = ''1'')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = 
		(Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))
	
	--ORDER BY ComplaintId desc;
	ORDER BY ComplaintStatusDate Desc	
		
			
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,* into #adf FROM  #AdvocateComplaints4  where
	 (	 
					dbo.fn_StripCharacters(RespondentFName,''^a-z0-9A-Z'')
					like ''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
					OR 
					dbo.fn_StripCharacters(RespondentLName,''^a-z0-9A-Z'') 
					like										 
					''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
					OR  
					dbo.fn_StripCharacters(CreatedBy,''^a-z0-9A-Z'')								
					like
					''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''										OR LocationName like ''%''+@stringValue+ ''%'' 	 
	 
	 OR Status like ''%''+@stringValue+ ''%'' or jt1 like ''%''+@stringValue+ ''%''  ) 	
	
		
select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #adf where compIndex = 1) #adf
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;
	
	select @rowCount=COUNT(*) from #adf where compIndex = 1
	
	drop table #AdvocateComplaints4
	drop table #adf
	
 End	 	 
 
END


' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateComplaints]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateComplaints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetAdvocateComplaints] 
	-- Add the parameters for the stored procedure here	
@PageNo int = 0,
@maximumRows int = 0,
@rowsCount int OUTPUT
AS
BEGIN

SET NOCOUNT ON;
DECLARE @Count int
Set @rowsCount = (

SELECT	COUNT(*) as AdvocateComplaintsCount
          
  FROM 
        
		 [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) left outer JOIN Code e
		On d.FinalStatusCd = e.Code_Id 	
    where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')	
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy] t	
	where t.ComplaintId=a.ComplaintId	and IsAvailable is null	
	order by a.ComplaintStatusDate desc 
	))

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;


WITH AdvocateComplaints  AS
(

--select * into #advComplaints from
--(
SELECT    
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when ''OEDM'' then ''OEDM''
		else a.DataSource 
		End as DataSource
		,d.AdvocacyCaseId	
		,a.IsProbation 	
		,a.ComplaintId
		,a.IndexNo as IndexNo,
		 a.ApprovedDate
		,a.RoutingLocation
		,a.[LocationId]
		,a.Jt1 		
		--,i.IncidentDate 		
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		--,ISNULL((select top 1 sentdate from tblRoutingHistory 
		--where ComplaintId=a.ComplaintId and SentTo=''Advocate'' order by SentDate desc ),a.ComplaintStatusDate)   
,ComplaintStatusDate
		,''false'' as Approved 
		,Charges    
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName,
		a.ParentIndexNo,		
		Status = (case when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         When d.FinalStatuscd = ''98'' then  e.Code_Name 
                         end),
         '''' as Indicator, a.IsDeleted
         
        -- ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
        ,(select top 1 IncidentDate from tblDS249Incidents 
		where ComplaintId=a.ComplaintId   order by IncidentDate desc ) IncidentDate
		

         
  FROM 
        
		 [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) left outer JOIN Code e
		On d.FinalStatusCd = e.Code_Id 	
    where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')	
 		and (a.RoutingLocation = ''A'')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy] t	
	where t.ComplaintId=a.ComplaintId	and IsAvailable is null	
	order by a.ComplaintStatusDate Desc		
	)	
--) a	
	)


 select * from  AdvocateComplaints 
WHERE ROWID BETWEEN @FirstRow AND @LastRow
ORDER BY ROWID asc;


END
  
  
  
 
  
  
  ' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetAdvocateComplaint]    Script Date: 07/31/2012 09:56:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetAdvocateComplaint]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
-- exec usp_GetAdvocateComplaint 10781
-- =============================================
-- Author:		<Amit Khandelwal>
-- Create date: <12/09/2009>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetAdvocateComplaint]
	-- Add the parameters for the stored procedure here
@ComplaintID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AdvocacyCaseId,
			comp.IsProbation
			,a.ComplaintId,			
			CaseClassificationcd,
			OriginationSourcecd,
			SpecialStatus_cd,
			TrialReturnStatuscd,
			TrialReturnDate,
			ReviewStatuscd,
			ReviewStatusDate,
			Actioncd,
			ActionDate,
			isClosed,
			ClosedDate,
			COBStatus_cd,
			COBDate,
			FinalStatuscd,
			CaseComment,
			C.LastName + '', '' + C.FirstName As RecieveUser, 
			RecieveDate,
			D.LastName + '', '' + D.FirstName As ModifiedUser,
			ModifiedDate,
			IsAvailable	   
	from tblComplaintAdvocacy A 
	left join uv_EmployeePerson C 
	ON IsNull(A.RecieveUser, '''') = C.EmployeeID
	left join uv_EmployeePerson D 
	ON IsNull(A.ModifiedUser, '''') = D.EmployeeID
	inner join tblDS249Complaints comp on comp.ComplaintId =a.ComplaintId 
	Where a.ComplaintId = @ComplaintID AND IsAvailable is null
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsCountSearch]    Script Date: 07/31/2012 09:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsCountSearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetMedicalComplaintsCountSearch] 
	-- Add the parameters for the stored procedure here	
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

  	
if @numericValue != 0  
  Begin
  
  SELECT	
		COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
	  
  End
 else if @chargesValue != '''' 
  Begin
    SELECT	
			COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (Charges like ''%''+@chargesValue+ ''%'') 	
  End
 else if @complaintDate != ''''
  Begin
      SELECT				
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, '''' as Charges    		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         ,a.IsProbation 
         into #MedicalComplaints3 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) ORDER BY ComplaintId desc
 	SELECT  Count(*) FROM  #MedicalComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 	drop table #MedicalComplaints3  
	
  End
 else if @stringValue != ''''
  Begin
      SELECT			
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, '''' as Charges    		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         ,a.IsProbation
         into #MedicalComplaints4 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0) 
	SELECT COUNT(*) FROM  #MedicalComplaints4  where 
	 
	dbo.fn_StripCharacters(RespondentFName,''^a-z0-9A-Z'') 
	
	like ''%''+dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'') + ''%'' 
	OR  
	dbo.fn_StripCharacters(RespondentLName,''^a-z0-9A-Z'') 
	like 	 
	''%''+dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'') + ''%''
	 OR  
	 dbo.fn_StripCharacters(CreatedBy,''^a-z0-9A-Z'') 
	 like ''%''+dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'') + ''%''
	  OR LocationName like ''%''+@stringValue+ ''%'' 
	  OR Status  like ''%''+@stringValue+ ''%''
	
	drop table #MedicalComplaints4
	
  End 	 	
   
     
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaintsBySearch]    Script Date: 07/31/2012 09:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[usp_GetMedicalComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
  	
if @numericValue != 0  
  Begin
  
  SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges  
		,i.IncidentDate   		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName	
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
	
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints1  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 		
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (IndexNo like ''%''+Convert(varchar,@numericValue)+ ''%'' OR RespondentRefNumber like ''%''+Convert(varchar,@numericValue)) 
	    order by ComplaintStatusDate Desc	       
		SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
		FROM #MedicalComplaints1 WHERE compIndex=1)tblMedComplaints 
		WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	
	drop table #MedicalComplaints1
	  
  End
 else if @chargesValue != '''' 
  Begin
    SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges   
		,i.IncidentDate  		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints2  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (Charges like ''%''+@chargesValue+ ''%'')
	 
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
	FROM #MedicalComplaints2 where compIndex=1)tblMedComplaints 
	WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints2
	
  End
 else if @complaintDate != ''''
  Begin
     SELECT	
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges    
		,i.IncidentDate 		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints3 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0))
 
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2, * into #adf0  FROM  #MedicalComplaints3 where CreatedDate like ''%''+Convert(varchar,@complaintDate)+ ''%''
 	select * from (select *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
   from  #adf0 where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints3  
	drop table #adf0     
	 
  End
 else if @stringValue != ''''
  Begin
      SELECT	
       ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,		
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,''false'' as Approved 
		, a.charges as Charges   
		,i.IncidentDate 	
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         into #MedicalComplaints4 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0) ORDER BY ComplaintId desc
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #MedicalComplaints4  where
	
	 dbo.fn_StripCharacters(RespondentFName,''^a-z0-9A-Z'')
										like  									
										
										''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
										OR 
										 dbo.fn_StripCharacters(RespondentLName,''^a-z0-9A-Z'') 
										like										 
									 ''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''
								OR  
								dbo.fn_StripCharacters(CreatedBy,''^a-z0-9A-Z'')								
								like
								 ''%'' + dbo.fn_StripCharacters(@stringValue,''^a-z0-9A-Z'')+''%''								
	
	
	  OR LocationName like ''%''+@stringValue+ ''%'' OR Status  like ''%''+@stringValue+ ''%''
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
   FROM #adf where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints4
	drop table #adf
  End 	 	
   
     
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetMedicalComplaints]    Script Date: 07/31/2012 09:56:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetMedicalComplaints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[usp_GetMedicalComplaints] 
@PageNo int = 0,
@maximumRows int = 0
AS
BEGIN
	
	SET NOCOUNT ON;

DECLARE @Count int
Set @Count = (SELECT
		COUNT(*)  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
   where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0 )
	    
	    DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;


WITH MedicalComplaints  AS
(    
	 SELECT
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.IsProbation,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ISNULL((select top 1 sentdate from tblRoutingHistory 
		where ComplaintId=a.ComplaintId and (SentTo=''MED''or sentto= ''BCAD'') order by SentDate desc ),a.CreatedDate) ComplaintStatusDate
		,''false'' as Approved 
		,Charges    		
		,(select top 1 IncidentDate from tblDS249Incidents 
		where ComplaintId=a.ComplaintId   order by IncidentDate desc ) IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName		
		
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = ''B'' then ''UnRead''
						 when d.ComplaintId is null and a.RoutingLocation = ''M'' then ''UnRead''
                         when d.FinalStatuscd = ''96'' then  e.Code_Name
                         when d.FinalStatuscd = ''97'' then  e.Code_Name 
                         when a.RoutingLocation = ''A'' then ''Advocate'' end),
         '''' as Indicator
         
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		
		
  where (a.ComplaintStatus = 5 or IsSubmitted = ''1'')
 		and a.RoutingLocation = ''M''
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0 
  )
  
 
  
   select * from MedicalComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
   ORDER BY ComplaintStatusDate Desc;
END




' 
END
GO
