
GO
ALTER PROCEDURE [dbo].[usp_GetCombinedComplaints]
	
@PageNo int = 0,
@EmpID bigint,
@maximumRows int = 0,
@ComplaintId int=0,
@IndexNo int=0,
@rowsCount int OUTPUT
AS
BEGIN
SET NOCOUNT ON;
DECLARE @Count int
Set @rowsCount = (SELECT	COUNT(*) as AdvocateComplaintsCount
 FROM [dbo].[tblDS249Complaints] a
 		LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) 
		--LEFT OUTER JOIN Code e
		--On d.FinalStatusCd = e.Code_Id		
 ---- where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 ----		and (a.RoutingLocation = 'A')
	----	AND (d.IsAvailable is null) 
	----	AND a.IsDeleted =0 
	----	and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	----FROM [dbo].[tblComplaintAdvocacy]	
	----where ComplaintId=a.ComplaintId
	where (ParentIndexNo IS NULL or ParentIndexNo =@IndexNo )
	 and d.ComplaintId=a.ComplaintId	and  a.ComplaintId!=@ComplaintId	
		 and RespondentEmpID =@EmpID and d.FinalStatuscd =96 
		  and (d.Actioncd=46 or d.Actioncd=44 or d.Actioncd=39 or d.Actioncd=40 or d.Actioncd=41)	
	and IndexNo not in --(isnull(ParentIndexNo,0))
	(select isnull(ParentIndexNo,0) from  tblDS249Complaints 
	where RespondentEmpID=@EmpID)
	
	
	)
	

DECLARE @FirstRow INT, @LastRow INT
declare @strsql varchar(1000)
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;


WITH AdvocateComplaints  AS
(

SELECT    
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,		
		d.AdvocacyCaseId		
		,a.ComplaintId		
		,a.IndexNo as IndexNo		
		,i.IncidentDate 				
		,ComplaintStatusDate		
		,Charges    
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy		
		,a.RespondentFName
		,a.RespondentLName
	   , a.ParentIndexNo 	    
	    ,Status = (case when d.FinalStatuscd = '96' then 'Open' --e.Code_Name
                         --when d.FinalStatuscd = '97' then  e.Code_Name 
                         --When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),       
         ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex          
  FROM 
        
		 [dbo].[tblDS249Complaints] a 
		LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID 
		inner JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) 
		--left outer JOIN Code e
		--On d.FinalStatusCd = e.Code_Id 		 
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId	
    where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy] t	
	where (ParentIndexNo IS NULL or ParentIndexNo =@IndexNo )
	 and t.ComplaintId=a.ComplaintId	and  ComplaintId!=@ComplaintId	
		 and RespondentEmpID =@EmpID and d.FinalStatuscd =96 
		  and (t.Actioncd=46 or t.Actioncd=44 or t.Actioncd =39 or t.Actioncd=40 or Actioncd=41)	
	and IndexNo not in --(isnull(ParentIndexNo,0))
	(select isnull(ParentIndexNo,0) from  tblDS249Complaints 
	where RespondentEmpID=@EmpID)
	) )
	
select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from AdvocateComplaints where compIndex = 1) tblAdvocate
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc; 
END

GO
create PROCEDURE [dbo].[usp_RptGetDisciplinary1]
	-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @CountNotAdjudicated INT 
	Declare @ComplaintsReceived INT 
	Declare @CountAdvocateMonthly INT 
	Declare @CountFieldMonthly INT 
	Declare @CountMedicalMonthly INT 
	Declare @CountSSLUMonthly INT 
	Declare @CountDATMonthly INT 
	Declare @CountTrainingMonthly INT 
	SET NOCOUNT ON;
	
	SELECT @CountNotAdjudicated=COUNT(*) 
	FROM tblComplaintAdvocacy 
	WHERE RecieveDate is not null
	and ModifiedDate is null and FinalStatuscd=96
	and IsAvailable is null 
	and RecieveDate >=@FromDate and RecieveDate <=@ToDate
    -- Insert statements for procedure here

	--set @month=MONTH(@FromDate);
	--set @year=YEAR(@FromDate);





	print  '@CountNotAdjudicated'+ convert(varchar, @CountNotAdjudicated);
	 --print @month;
	 --print @year;
	 
	  SELECT @ComplaintsReceived=COUNT( distinct ca.ComplaintId)
	 FROM tblComplaintAdvocacy ca inner join tblDS249Complainant cn
	 on ca.ComplaintId=cn.ComplaintId
	 WHERE FinalStatuscd in (96,97)
		and IsAvailable is null 
		and ModifiedDate is not null
		and RecieveDate>=@FromDate
		and RecieveDate<=@ToDate
		and cn.ComplainantType='C'
		and cn.LocationId is not null 
		and cn.LocationId <>-1
		
		
	 print '@ComplaintsReceived:'+ convert(varchar, @ComplaintsReceived)
	 
	--************************************ 
	 select @CountAdvocateMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=519  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateMonthly:'+ convert(varchar, @CountAdvocateMonthly)
	 
	  
	 select @CountFieldMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,565,566,16) -- Location belonging to 8278053	MEDICAL OFFICE 
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldMonthly:'+ convert(varchar, @CountFieldMonthly)
	 
	  
	 select @CountMedicalMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging to 8278053	MEDICAL OFFICE 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalMonthly:'+ convert(varchar, @CountMedicalMonthly)
	 
	   select @CountSSLUMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to 8278053	MEDICAL OFFICE 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUMonthly:'+ convert(varchar, @CountSSLUMonthly)
	 
	 set @CountDATMonthly=0
	 --select @CountDATMonthly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 --on complanant.ComplainantId=compAdvocacy.ComplaintId
		--WHERE compAdvocacy.FinalStatuscd in (96,97)
		--and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		--and year(compAdvocacy.RecieveDate)>=year(@FromDate)
		--and year(compAdvocacy.RecieveDate)<=year(@ToDate)
		--and complanant.LocationId=519  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 --and complanant.ComplainantType='C'
	 --	 print '@CountDATMonthly:'+ convert(varchar, @CountDATMonthly)
	 	 
	select @CountTrainingMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=16  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingMonthly:'+ convert(varchar, @CountTrainingMonthly)

-- Complaints disposed 

	Declare @CountPleadGuilty int
	Declare @CountDismissal int
	Declare @CountBCAD int
	Declare @CountInformalConferences int
	Declare @CountVoids int
	Declare @Retired_Resigned_Terminated int
	Declare @TotalComplaintDisposed int
	Declare @ComplaintRemainingOpened int

	select @CountPleadGuilty = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=64   -- Plead Guilty
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	select @CountDismissal = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=62   -- Motion to Dismiss
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	select @CountBCAD= COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=39 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	select @CountInformalConferences = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=40 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	
	select @CountVoids = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=41 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	
	select @Retired_Resigned_Terminated = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and (ah.HearingStatusCd=71 or ah.HearingStatusCd=165 or ah.HearingStatusCd=166)
	   -- Retired /Resigned /Terminated
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	and ca.IsAvailable is null
-- Declaring and Returning Table.

	select @TotalComplaintDisposed = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=97 
	and IsAvailable is null
	and convert(date,ClosedDate,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate

	select @ComplaintRemainingOpened = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=96 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	and convert(date,ModifiedDate,101) <=@ToDate--GETDATE()



	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ca.ModifiedDate >=@FromDate
		and ca.ModifiedDate <=@ToDate
	and ca.IsAvailable is null	
	and ah.HearingStatusCd=65 -- Conf Requested Trial

--select * from #tblHearingDate 

	Declare @ConferenceCalendarDates varchar(800)
		select @ConferenceCalendarDates= 
		--coalesce(@ConferenceCalendarDates+',','')+ CONVERT(varchar(Max), HearingDt,101) 
		--coalesce(@ConferenceCalendarDates+',','') +  CAST( HearingDt as varchar(Max)) 
	coalesce(@ConferenceCalendarDates+',','') +  convert( varchar,HearingDt,101) --cast(HearingDt as varchar(Max)) 

		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @ConferenceCalendarDates
	
	
	



	Declare @DiscretionaryReport table
     (CountNotAdjudicated int ,
      ComplaintsReceived int,
      CountAdvocateMonthly int,
      CountMedicalMonthly int,
      CountSSLUMonthly int,
      CountDATMonthly int,
      CountTrainingMonthly int,
      CountFieldMonthly int,
      CountPleadGuilty int,
	  CountDismissal int,
	  CountBCAD int,
	 CountInformalConferences int,
	 CountVoids int,
	 Retired_Resigned_Terminated int,
	 TotalComplaintDisposed int,
	 ComplaintRemainingOpened int,
	 ConferenceCalendarDates varchar(800)
      )


  insert @DiscretionaryReport 
  values(@CountNotAdjudicated,@ComplaintsReceived,@CountAdvocateMonthly,
   @CountMedicalMonthly,@CountSSLUMonthly,@CountDATMonthly,@CountTrainingMonthly,
   @CountFieldMonthly,
   @CountPleadGuilty ,@CountDismissal ,@CountBCAD ,@CountInformalConferences ,
   @CountVoids ,@Retired_Resigned_Terminated ,@TotalComplaintDisposed ,
   @ComplaintRemainingOpened ,@ConferenceCalendarDates
  )
  select * from @DiscretionaryReport

END

GO

create PROCEDURE [dbo].[usp_RptGetDisciplinary2]
	
-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	Declare @CountAdvocateMonthly INT 
	Declare @CountFieldMonthly INT 
	Declare @CountMedicalMonthly INT 
	Declare @CountSSLUMonthly INT 
	Declare @CountDATMonthly INT 
	Declare @CountTrainingMonthly INT 
	SET NOCOUNT ON;
	

	--************************************ 
	 select @CountAdvocateMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and year(compAdvocacy.RecieveDate)=year(@ToDate)
		and complanant.LocationId=519  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateMonthly:'+ convert(varchar, @CountAdvocateMonthly)
	 
	  
	 select @CountFieldMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and year(compAdvocacy.RecieveDate)=year(@ToDate)
		and complanant.LocationId not in (519,565,566,16) -- Location belonging to 8278053	MEDICAL OFFICE 
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldMonthly:'+ convert(varchar, @CountFieldMonthly)
	 
	  
	 select @CountMedicalMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and year(compAdvocacy.RecieveDate)=year(@ToDate)
		and complanant.LocationId=565  --  Location belonging to 8278053	MEDICAL OFFICE 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalMonthly:'+ convert(varchar, @CountMedicalMonthly)
	 
	   select @CountSSLUMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and year(compAdvocacy.RecieveDate)=year(@ToDate)
		and complanant.LocationId=566  --  Location belonging to 8278053	MEDICAL OFFICE 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUMonthly:'+ convert(varchar, @CountSSLUMonthly)
	 
	 set @CountDATMonthly=0
	 select @CountDATMonthly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplainantId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and year(compAdvocacy.RecieveDate)>=year(@FromDate)
		and year(compAdvocacy.RecieveDate)<=year(@ToDate)
		and complanant.LocationId=519  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	 print '@CountDATMonthly:'+ convert(varchar, @CountDATMonthly)
	 	 
	select @CountTrainingMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and year(compAdvocacy.RecieveDate)=year(@ToDate)
		and complanant.LocationId=16  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingMonthly:'+ convert(varchar, @CountTrainingMonthly)

-- Complaints disposed 

	Declare @AwaitingTrialDateAtOath int
	Declare @CountCasesScheduled int
	Declare @CountCasesHeard int	
	Declare @CountDispostionFromOath int 
	
	Declare @CountAwaitingAdjudicationComplaintsNo int
	Declare @CountAwaitingAdjudicationComplaintsEmp int
	Declare @CountAwaitingOathTrialComplaintsNo int
	Declare @CountAwaitingOathTrialComplaintsEmp int

	select @AwaitingTrialDateAtOath = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
	ca.FinalStatuscd=96  -- Open Complaints
	 	and ah.HearingStatusCd=65   -- CONF. - REQUEST TRIAL
		and ModifiedDate >=@FromDate 
	and ModifiedDate <=@ToDate
	and ca.IsAvailable is null

	
	select @CountCasesScheduled = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ah.CalStatusCd=50   -- SCHED			
		and convert(date,ModifiedDate,101 ) >=@FromDate 
	and convert(date,ModifiedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	
select @CountCasesHeard= COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
		 ah.HearingStatusCd in (54,55,56,57,58)   -- SCHED			
		and convert(date,ModifiedDate,101 ) >=@FromDate 
	and convert(date,ModifiedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	
	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where
	-- ca.FinalStatuscd=96  -- Open Complaints
	--and month(ModifiedDate) =01
	--and year(ModifiedDate) =2010 and
	 month(HearingDt)=MONTH(@FromDate)+1
	and year(HearingDt) =year(@ToDate)
	
	and ca.IsAvailable is null	
	and ah.CalStatusCd=47 -- Oath Sched  

--select * from #tblHearingDate 

	Declare @CaseScheduledDates varchar(800)
		select @CaseScheduledDates= 
		coalesce(@CaseScheduledDates+',','')+ CONVERT(varchar, HearingDt,101) 
		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @CaseScheduledDates
	
	
		
				select @CountDispostionFromOath=COUNT(*) 
				from tblDS249Complaints comp inner join
				
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
			on ca.AdvocacyCaseId=ah.AdvocacyCaseID

			where 
 			 ah.HearingStatusCd=54   -- H Held Desc Reserved.
 			 and convert(date,ca.ModifiedDate,101) >=@FromDate
 			 and convert(date,ca.ModifiedDate,101) <=@ToDate
			and ca.IsAvailable is null
			
			) adv
			on comp.ComplaintId=adv.ComplaintId
			inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
			inner join tblPersons p on p.PersonID=emp.PersonID
	
	
		
		--select p.FirstName+ ' '+p.LastName Name ,
		--CONVERT(date,adv.HearingDt) HearingDate  
		--from tblDS249Complaints comp inner join
		--		(select distinct ca.ComplaintId,ah.HearingDt 
		--		from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		--		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		--		where ah.HearingStatusCd=54   -- H Held Desc Reserved.
 	--	        and YEAR(ca.ModifiedDate) =year(@ToDate)
		--	    and ca.IsAvailable is null
		--	    and ah.HearingDt is not null) adv
		--on comp.ComplaintId=adv.ComplaintId
		--inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		--inner join tblPersons p on p.PersonID=emp.PersonID
	
	
	select @CountAwaitingAdjudicationComplaintsNo=COUNT(*) from tblComplaintAdvocacy
	where FinalStatuscd in (96,98) 
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
		
	and IsAvailable is null
	
	select @CountAwaitingAdjudicationComplaintsEmp=COUNT(distinct RespondentEmpID ) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
	on comp.ComplaintId=ca.ComplaintId
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
	
	where FinalStatuscd in (96,98)
	

		
	select @CountAwaitingOathTrialComplaintsNo= COUNT(distinct ca.AdvocacyCaseId) 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ah.HearingStatusCd in (65)   -- SCHED	
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
					
		and ca.IsAvailable is null
	
	
	select @CountAwaitingOathTrialComplaintsEmp= COUNT(distinct comp.RespondentEmpID) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
	on comp.ComplaintId=ca.ComplaintId
	inner join
	(
	select distinct ca.AdvocacyCaseId 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ah.HearingStatusCd in (65)   -- SCHED		
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
				
		and ca.IsAvailable is null
	) adv on ca.AdvocacyCaseId=adv.AdvocacyCaseId
	
	
		Declare @DiscretionaryReport table
     (
      CountAdvocateMonthly int,
      CountMedicalMonthly int,
      CountSSLUMonthly int,
      CountDATMonthly int,
      CountTrainingMonthly INT,
      CountFieldMonthly int,
	  AwaitingTrialDateAtOath int,
	  CountCasesScheduled int,
	  CountCasesHeard int	,
      CountDispostionFromOath int, 
	  CaseScheduledDates varchar(800),
      CountAwaitingAdjudicationComplaintsNo int,
	  CountAwaitingAdjudicationComplaintsEmp int,
	  CountAwaitingOathTrialComplaintsNo int,
	  CountAwaitingOathTrialComplaintsEmp int
      )


  insert @DiscretionaryReport 
  values(@CountAdvocateMonthly,@CountMedicalMonthly,@CountSSLUMonthly,
   @CountDATMonthly,@CountTrainingMonthly,@CountFieldMonthly,
   @AwaitingTrialDateAtOath ,@CountCasesScheduled ,
   @CountCasesHeard ,@CountDispostionFromOath ,
   @CaseScheduledDates ,@CountAwaitingAdjudicationComplaintsNo ,
   @CountAwaitingAdjudicationComplaintsEmp ,
   @CountAwaitingOathTrialComplaintsNo,
   @CountAwaitingOathTrialComplaintsEmp
  )
  select * from @DiscretionaryReport
	

END



GO
create PROCEDURE [dbo].[usp_RptGetDisciplinary3]
	-- Add the parameters for the stored procedure here
	
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
select distinct p.FirstName+ ' '+p.LastName Name ,
		CONVERT(date,adv.HearingDt) HearingDate  
		from tblDS249Complaints comp inner join
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
				on ca.AdvocacyCaseId=ah.AdvocacyCaseID
				where ah.HearingStatusCd=54   -- H Held Desc Reserved.
 		        and YEAR(ah.HearingDt) =year(@ToDate)
			    and ca.IsAvailable is null
			    and ah.HearingDt is not null) adv
		on comp.ComplaintId=adv.ComplaintId
		inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		inner join tblPersons p on p.PersonID=emp.PersonID
		order by HearingDate asc 

END



GO
create PROCEDURE [dbo].[usp_RptGetDisciplinary4]
	-- Add the parameters for the stored procedure here	
AS
BEGIN  
  
  Declare @Resignations table
     (Jan int,
     Feb  int,
     Mar int,
      Apr int,
      May  int,
      June  int,
      July  int,
      Aug  int,
	  Sep int,Oct int,Nov  int,Dec  int)
   insert @Resignations 
  values(null,null,null,null,null,null,null,null,null,null,null,null) 
  
  
  select * from @Resignations
 
  END


GO

UPDATE tblLocations SET LocationCode ='0000000' WHERE LocationId =588



GO

ALTER PROCEDURE [dbo].[usp_GetAdvocateComplaintsBySearch] 
	-- Add the parameters for the stored procedure here	
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@rowCount int output
AS
BEGIN

SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
	
if @numericValue != 0  
 Begin
 
 SELECT		
		--ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.Charges as charges    
		--,c.LastName + ' ,' + c.FirstName as 'CreatedBy'
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId		
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = '1')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))	
	 and (IndexNo like '%'+Convert(varchar,@numericValue)+ '%' OR RespondentRefNumber like '%'+Convert(varchar,@numericValue) OR AdvocacyCaseId  like '%'+Convert(varchar,@numericValue))  
	--ORDER BY ComplaintId desc
	order by ComplaintStatusDate Desc	  
	--SELECT  * FROM  #AdvocateComplaints1 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     	


select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #AdvocateComplaints1 where compIndex = 1) #AdvocateComplaints1
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;	

select @rowCount=COUNT(*) from #AdvocateComplaints1 where compIndex = 1
	
	drop table #AdvocateComplaints1	
 End
 else if @chargesValue != '' 
 Begin
 
  SELECT
		--ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges    
		--,c.LastName + ' ,' + c.FirstName as 'CreatedBy'
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId			
		--once database is updated check only for a.ComplaintStatus=4
		-- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = '1')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))	
	and (Charges like '%'+@chargesValue+ '%')
	 --ORDER BY ComplaintId desc
	 ORDER BY ComplaintStatusDate Desc	
	--SELECT  * FROM  #AdvocateComplaints2 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
	

	select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #AdvocateComplaints2 where compIndex = 1) #AdvocateComplaints2
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;

select @rowCount=COUNT(*) from #AdvocateComplaints2 where compIndex = 1

	drop table #AdvocateComplaints2
 End
 else if @complaintDate != ''
 Begin
   SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IndexNo as IndexNo
		,a.Jt1 
	    ,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges    
		--,c.LastName + ' ,' + c.FirstName as 'CreatedBy'
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
          ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId			
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = '1')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo )) 	 
	--ORDER BY ComplaintId desc   
	ORDER BY ComplaintStatusDate Desc	
	
	
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,* into #adf0 FROM  #AdvocateComplaints3 where CreatedDate like '%'+Convert(varchar,@complaintDate)+ '%'
 		
 	--SELECT * from #adf0 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc    
 		

 	select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #adf0 where compIndex = 1) #adf0
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc; 

select @rowCount=COUNT(*) from #adf0 where compIndex = 1
	
	drop table #AdvocateComplaints3
	drop table #adf0 
 End
 else if @stringValue != ''
 Begin
 
    SELECT
   		--ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,					
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.ParentIndexNo
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges    
		--,c.LastName + ' ,' + c.FirstName as 'CreatedBy'
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = '1')  
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = 
		(Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))
	
	--ORDER BY ComplaintId desc;
	ORDER BY ComplaintStatusDate Desc	
		
			
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,* into #adf FROM  #AdvocateComplaints4  where
	 (RespondentFName like '%'+@stringValue+ '%' OR RespondentLName like '%'+@stringValue+ '%' 
	 OR CreatedBy like '%'+@stringValue+ '%' OR LocationName like '%'+@stringValue+ '%' 
	 OR Status like '%'+@stringValue+ '%' or jt1 like '%'+@stringValue+ '%'  ) 
	
	--SELECT  * from #adf where (ROWID BETWEEN @FirstRow AND @LastRow) ORDER BY ROWID asc 
		
select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #adf where compIndex = 1) #adf
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;
	
	select @rowCount=COUNT(*) from #adf where compIndex = 1
	
	drop table #AdvocateComplaints4
	drop table #adf
	
 End	 	 
 
END


GO

ALTER PROCEDURE [dbo].[usp_GetBCADComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
      
if @numericValue != 0  
  Begin
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted 
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (IndexNo like '%'+Convert(varchar,@numericValue)+ '%' OR RespondentRefNumber like '%'+Convert(varchar,@numericValue)) 
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints1  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
	
	drop table #CadComplaints1  
	
  End	
 else if @chargesValue != '' 
  Begin
  
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (Charges like '%'+@chargesValue+ '%') 
	   
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;	
	
	drop table #CadComplaints2	    
  End
 else if @complaintDate != ''
  Begin
  SELECT
  		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges    
			,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted  
          ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	     
 	 SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2, * into #adf0 FROM  #CadComplaints3 where CreatedDate like '%'+Convert(varchar,@complaintDate)+ '%'
		
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf0   where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc; 	 
	
	drop table #CadComplaints3
	drop table #adf0 
  End
 else if @stringValue != ''
  Begin  
    SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges 				
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
       ,i.IncidentDate
         into #CadComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #CadComplaints4  where RespondentFName like '%'+@stringValue+ '%' OR RespondentLName like '%'+@stringValue+ '%' OR CreatedBy like '%'+@stringValue+ '%' OR LocationName like '%'+@stringValue+ '%' OR Status  like '%'+@stringValue+ '%'  
	
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
		
	drop table #CadComplaints4
	drop table #adf
  End  
END

GO
ALTER PROCEDURE [dbo].[usp_GetMedicalComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
  	
if @numericValue != 0  
  Begin
  
  SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges  
		,i.IncidentDate   		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName	
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
	
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         into #MedicalComplaints1  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 		
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (IndexNo like '%'+Convert(varchar,@numericValue)+ '%' OR RespondentRefNumber like '%'+Convert(varchar,@numericValue)) 
	    order by ComplaintStatusDate Desc	       
		SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
		FROM #MedicalComplaints1 WHERE compIndex=1)tblMedComplaints 
		WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	
	drop table #MedicalComplaints1
	  
  End
 else if @chargesValue != '' 
  Begin
    SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges   
		,i.IncidentDate  		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         into #MedicalComplaints2  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (Charges like '%'+@chargesValue+ '%')
	 
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
	FROM #MedicalComplaints2 where compIndex=1)tblMedComplaints 
	WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints2
	
  End
 else if @complaintDate != ''
  Begin
     SELECT	
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges    
		,i.IncidentDate 		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         into #MedicalComplaints3 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0))
 
 	SELECT  ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID2, * into #adf0  FROM  #MedicalComplaints3 where CreatedDate like '%'+Convert(varchar,@complaintDate)+ '%'
 	select * from (select *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
   from  #adf0 where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints3  
	drop table #adf0     
	 
  End
 else if @stringValue != ''
  Begin
      SELECT	
       ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,		
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, a.charges as Charges   
		,i.IncidentDate 	
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         into #MedicalComplaints4 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0) ORDER BY ComplaintId desc
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #MedicalComplaints4  where RespondentFName like '%'+@stringValue+ '%' OR RespondentLName like '%'+@stringValue+ '%' OR CreatedBy like '%'+@stringValue+ '%' OR LocationName like '%'+@stringValue+ '%' OR Status  like '%'+@stringValue+ '%'
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
   FROM #adf where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints4
	drop table #adf
  End 	 	
   
     
END

GO
update ap set ap.DataValue=CONVERT(float, SUBSTRING(datavalue,2,len(datavalue))) 
--from  Comments, 
from tblAdvocatedPenalty ap where DataValue
like '%$%'

Go

update tblAdvocatedPenalty set Comments='Void',  DataValue=0 where DataValue
like 'Void'
 
 Go
 
 

 update tblAdvocatedPenalty set ApplyToTotal=1 where convert(int,DataValue)
 >=0
 
Go

   update tblLocations set ParentLocationID=2
   where LocationCode in ('8270110')
   
   Go
   
   update tblLocations set ParentLocationID=151
   where LocationCode in  ('8271450','8271260')
  
  Go 
   update tblLocations set ParentLocationID=270 where 
   LocationCode in ('8272190')
	   
Go	   
	   
	   
	   
GO
ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary1]
	-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @CountNotAdjudicated INT 
	Declare @ComplaintsReceived INT 
	Declare @CountAdvocateMonthly INT 
	Declare @CountFieldMonthly INT 
	Declare @CountMedicalMonthly INT 
	Declare @CountSSLUMonthly INT 
	Declare @CountDATMonthly INT 
	Declare @CountTrainingMonthly INT 
	SET NOCOUNT ON;
	
	SELECT @CountNotAdjudicated=COUNT(*) 
	FROM tblComplaintAdvocacy 
	WHERE RecieveDate is not null
	and ModifiedDate is null and FinalStatuscd=96
	and IsAvailable is null 
	and RecieveDate >=@FromDate and RecieveDate <=@ToDate
    -- Insert statements for procedure here

	--set @month=MONTH(@FromDate);
	--set @year=YEAR(@FromDate);





	print  '@CountNotAdjudicated'+ convert(varchar, @CountNotAdjudicated);
	 --print @month;
	 --print @year;
	 
	  SELECT @ComplaintsReceived=COUNT( distinct ca.ComplaintId)
	 FROM tblComplaintAdvocacy ca inner join tblDS249Complainant cn
	 on ca.ComplaintId=cn.ComplaintId
	 WHERE FinalStatuscd in (96,97)
		and IsAvailable is null 
		and ModifiedDate is not null
		and RecieveDate>=@FromDate
		and RecieveDate<=@ToDate
		and cn.ComplainantType='C'
		and cn.LocationId is not null 
		and cn.LocationId <>-1
		
		
	 print '@ComplaintsReceived:'+ convert(varchar, @ComplaintsReceived)
	 
	--************************************ 
	 select @CountAdvocateMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (512,519)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateMonthly:'+ convert(varchar, @CountAdvocateMonthly)
	 
	  
	 select @CountFieldMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging to field
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldMonthly:'+ convert(varchar, @CountFieldMonthly)
	 
	  
	 select @CountMedicalMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging to MEDICAL 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalMonthly:'+ convert(varchar, @CountMedicalMonthly)
	 
	   select @CountSSLUMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUMonthly:'+ convert(varchar, @CountSSLUMonthly)
	 
	 set @CountDATMonthly=0
	 --select @CountDATMonthly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 --on complanant.ComplainantId=compAdvocacy.ComplaintId
		--WHERE compAdvocacy.FinalStatuscd in (96,97)
		--and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		--and year(compAdvocacy.RecieveDate)>=year(@FromDate)
		--and year(compAdvocacy.RecieveDate)<=year(@ToDate)
		--and complanant.LocationId=519  -- Location belonging to DAT
	 --and complanant.ComplainantType='C'
	 --	 print '@CountDATMonthly:'+ convert(varchar, @CountDATMonthly)
	 	 
	select @CountTrainingMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  -- 16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingMonthly:'+ convert(varchar, @CountTrainingMonthly)

-- Complaints disposed 

	Declare @CountPleadGuilty int
	Declare @CountDismissal int
	Declare @CountBCAD int
	Declare @CountInformalConferences int
	Declare @CountVoids int
	Declare @Retired_Resigned_Terminated int
	Declare @TotalComplaintDisposed int
	Declare @ComplaintRemainingOpened int

	select @CountPleadGuilty = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=64   -- Plead Guilty
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	select @CountDismissal = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=62   -- Motion to Dismiss
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	select @CountBCAD= COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=39 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	select @CountInformalConferences = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=40 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	
	select @CountVoids = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=41 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	
	select @Retired_Resigned_Terminated = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and (ah.HearingStatusCd=71 or ah.HearingStatusCd=165 or ah.HearingStatusCd=166)
	   -- Retired /Resigned /Terminated
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	and ca.IsAvailable is null
-- Declaring and Returning Table.

	select @TotalComplaintDisposed = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=97 
	and IsAvailable is null
	and convert(date,ClosedDate,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate

	select @ComplaintRemainingOpened = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=96 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	and convert(date,ModifiedDate,101) <=@ToDate--GETDATE()



	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ca.ModifiedDate >=@FromDate
		and ca.ModifiedDate <=@ToDate
	and ca.IsAvailable is null	
	and ah.HearingStatusCd=65 -- Conf Requested Trial

--select * from #tblHearingDate 

	Declare @ConferenceCalendarDates varchar(800)
		select @ConferenceCalendarDates= 
		--coalesce(@ConferenceCalendarDates+',','')+ CONVERT(varchar(Max), HearingDt,101) 
		--coalesce(@ConferenceCalendarDates+',','') +  CAST( HearingDt as varchar(Max)) 
	coalesce(@ConferenceCalendarDates+',','') +  convert( varchar,HearingDt,101) --cast(HearingDt as varchar(Max)) 

		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @ConferenceCalendarDates
	
	
	



	Declare @DiscretionaryReport table
     (CountNotAdjudicated int ,
      ComplaintsReceived int,
      CountAdvocateMonthly int,
      CountMedicalMonthly int,
      CountSSLUMonthly int,
      CountDATMonthly int,
      CountTrainingMonthly int,
      CountFieldMonthly int,
      CountPleadGuilty int,
	  CountDismissal int,
	  CountBCAD int,
	 CountInformalConferences int,
	 CountVoids int,
	 Retired_Resigned_Terminated int,
	 TotalComplaintDisposed int,
	 ComplaintRemainingOpened int,
	 ConferenceCalendarDates varchar(800)
      )


  insert @DiscretionaryReport 
  values(@CountNotAdjudicated,@ComplaintsReceived,@CountAdvocateMonthly,
   @CountMedicalMonthly,@CountSSLUMonthly,@CountDATMonthly,@CountTrainingMonthly,
   @CountFieldMonthly,
   @CountPleadGuilty ,@CountDismissal ,@CountBCAD ,@CountInformalConferences ,
   @CountVoids ,@Retired_Resigned_Terminated ,@TotalComplaintDisposed ,
   @ComplaintRemainingOpened ,@ConferenceCalendarDates
  )
  select * from @DiscretionaryReport

END




GO

ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary2]
	
-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	Declare @CountAdvocateYearly INT 
	Declare @CountFieldYearly INT 
	Declare @CountMedicalYearly INT 
	Declare @CountSSLUYearly INT 
	Declare @CountDATYearly INT 
	Declare @CountTrainingYearly INT 
	SET NOCOUNT ON;
	declare @yearStart DATE
	SET @yearStart=CONVERT(DATE, '01/01/'+ CONVERT(VARCHAR,YEAR(CONVERT(DATE, @FromDate))));
    PRINT @yearStart;
	--************************************ 
	 select @CountAdvocateYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (519,512)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateYearly:'+ convert(varchar, @CountAdvocateYearly)
	 
	  
	 select @CountFieldYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging Field
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldYearly:'+ convert(varchar, @CountFieldYearly)
	 
	  
	 select @CountMedicalYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging 	MEDICAL OFFICE 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalYearly:'+ convert(varchar, @CountMedicalYearly)
	 
	   select @CountSSLUYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUYearly:'+ convert(varchar, @CountSSLUYearly)
	 
	 set @CountDATYearly=0
	 --select @CountDATYearly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 --on complanant.ComplainantId=compAdvocacy.ComplaintId
		--WHERE compAdvocacy.FinalStatuscd in (96,97)
		--and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		--and compAdvocacy.RecieveDate>= @yearStart
		--and compAdvocacy.RecieveDate<=@ToDate
		--and complanant.LocationId=519  -- Location belonging to DAT
	 --and complanant.ComplainantType='C'
	 --	 print '@CountDATYearly:'+ convert(varchar, @CountDATYearly)
	 	 
	select @CountTrainingYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  --16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingYearly:'+ convert(varchar, @CountTrainingYearly)

-- Complaints disposed 

	Declare @AwaitingTrialDateAtOath int
	Declare @CountCasesScheduled int
	Declare @CountCasesHeard int	
	Declare @CountDispostionFromOath int 
	
	Declare @CountAwaitingAdjudicationComplaintsNo int
	Declare @CountAwaitingAdjudicationComplaintsEmp int
	Declare @CountAwaitingOathTrialComplaintsNo int
	Declare @CountAwaitingOathTrialComplaintsEmp int

	select @AwaitingTrialDateAtOath = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
	ca.FinalStatuscd=96  -- Open Complaints
	 	and ah.HearingStatusCd=65   -- CONF. - REQUEST TRIAL
		and ModifiedDate >=@FromDate 
	and ModifiedDate <=@ToDate
	and ca.IsAvailable is null

	
	select @CountCasesScheduled = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ah.CalStatusCd=50   -- SCHED			
		and convert(date,ModifiedDate,101 ) >=@FromDate 
	and convert(date,ModifiedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	
select @CountCasesHeard= COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
		 ah.HearingStatusCd in (54,55,56,57,58)   -- SCHED			
		and convert(date,ModifiedDate,101 ) >=@FromDate 
	and convert(date,ModifiedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	
	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where
	-- ca.FinalStatuscd=96  -- Open Complaints
	--and month(ModifiedDate) =01
	--and year(ModifiedDate) =2010 and
	 month(HearingDt)=MONTH(@FromDate)+1
	and year(HearingDt) =year(@ToDate)
	
	and ca.IsAvailable is null	
	and ah.CalStatusCd=47 -- Oath Sched  

--select * from #tblHearingDate 

	Declare @CaseScheduledDates varchar(800)
		select @CaseScheduledDates= 
		coalesce(@CaseScheduledDates+',','')+ CONVERT(varchar, HearingDt,101) 
		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @CaseScheduledDates
	
	
		
				select @CountDispostionFromOath=COUNT(*) 
				from tblDS249Complaints comp inner join
				
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
			on ca.AdvocacyCaseId=ah.AdvocacyCaseID

			where 
 			 ah.HearingStatusCd=54   -- H Held Desc Reserved.
 			 and convert(date,ca.ModifiedDate,101) >=@FromDate
 			 and convert(date,ca.ModifiedDate,101) <=@ToDate
			and ca.IsAvailable is null
			
			) adv
			on comp.ComplaintId=adv.ComplaintId
			inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
			inner join tblPersons p on p.PersonID=emp.PersonID
	
	
		
		--select p.FirstName+ ' '+p.LastName Name ,
		--CONVERT(date,adv.HearingDt) HearingDate  
		--from tblDS249Complaints comp inner join
		--		(select distinct ca.ComplaintId,ah.HearingDt 
		--		from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		--		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		--		where ah.HearingStatusCd=54   -- H Held Desc Reserved.
 	--	        and YEAR(ca.ModifiedDate) =year(@ToDate)
		--	    and ca.IsAvailable is null
		--	    and ah.HearingDt is not null) adv
		--on comp.ComplaintId=adv.ComplaintId
		--inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		--inner join tblPersons p on p.PersonID=emp.PersonID
	
	
	select @CountAwaitingAdjudicationComplaintsNo=COUNT(*) from tblComplaintAdvocacy
	where FinalStatuscd in (96,98) 
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
		
	and IsAvailable is null
	
	select @CountAwaitingAdjudicationComplaintsEmp=COUNT(distinct RespondentEmpID ) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
	on comp.ComplaintId=ca.ComplaintId
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
	
	where FinalStatuscd in (96,98)
	

		
	select @CountAwaitingOathTrialComplaintsNo= COUNT(distinct ca.AdvocacyCaseId) 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ah.HearingStatusCd in (65)   -- SCHED	
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
					
		and ca.IsAvailable is null
	
	
	select @CountAwaitingOathTrialComplaintsEmp= COUNT(distinct comp.RespondentEmpID) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
	on comp.ComplaintId=ca.ComplaintId
	inner join
	(
	select distinct ca.AdvocacyCaseId 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ah.HearingStatusCd in (65)   -- SCHED		
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
				
		and ca.IsAvailable is null
	) adv on ca.AdvocacyCaseId=adv.AdvocacyCaseId
	
	
		Declare @DiscretionaryReport table
     (
      CountAdvocateYearly int,
      CountMedicalYearly int,
      CountSSLUYearly int,
      CountDATYearly int,
      CountTrainingYearly INT,
      CountFieldYearly int,
	  AwaitingTrialDateAtOath int,
	  CountCasesScheduled int,
	  CountCasesHeard int	,
      CountDispostionFromOath int, 
	  CaseScheduledDates varchar(800),
      CountAwaitingAdjudicationComplaintsNo int,
	  CountAwaitingAdjudicationComplaintsEmp int,
	  CountAwaitingOathTrialComplaintsNo int,
	  CountAwaitingOathTrialComplaintsEmp int
      )


  insert @DiscretionaryReport 
  values(@CountAdvocateYearly,@CountMedicalYearly,@CountSSLUYearly,
   @CountDATYearly,@CountTrainingYearly,@CountFieldYearly,
   @AwaitingTrialDateAtOath ,@CountCasesScheduled ,
   @CountCasesHeard ,@CountDispostionFromOath ,
   @CaseScheduledDates ,@CountAwaitingAdjudicationComplaintsNo ,
   @CountAwaitingAdjudicationComplaintsEmp ,
   @CountAwaitingOathTrialComplaintsNo,
   @CountAwaitingOathTrialComplaintsEmp
  )
  select * from @DiscretionaryReport
	

END



GO
ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary1]
	-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @CountNotAdjudicated INT 
	Declare @ComplaintsReceived INT 
	Declare @CountAdvocateMonthly INT 
	Declare @CountFieldMonthly INT 
	Declare @CountMedicalMonthly INT 
	Declare @CountSSLUMonthly INT 
	Declare @CountDATMonthly INT 
	Declare @CountTrainingMonthly INT 
	SET NOCOUNT ON;
	
	SELECT @CountNotAdjudicated=COUNT(*) 
	FROM tblComplaintAdvocacy 
	WHERE RecieveDate is not null
	and ModifiedDate is null and FinalStatuscd=96
	and IsAvailable is null 
	and RecieveDate >=@FromDate and RecieveDate <=@ToDate
    -- Insert statements for procedure here

	--set @month=MONTH(@FromDate);
	--set @year=YEAR(@FromDate);





	print  '@CountNotAdjudicated'+ convert(varchar, @CountNotAdjudicated);
	 --print @month;
	 --print @year;
	 
	  SELECT @ComplaintsReceived=COUNT( distinct ca.ComplaintId)
	 FROM tblComplaintAdvocacy ca inner join tblDS249Complainant cn
	 on ca.ComplaintId=cn.ComplaintId
	 WHERE FinalStatuscd in (96,97)
		and IsAvailable is null 
		and ModifiedDate is not null
		and RecieveDate>=@FromDate
		and RecieveDate<=@ToDate
		and cn.ComplainantType='C'
		and cn.LocationId is not null 
		and cn.LocationId <>-1
		
		
	 print '@ComplaintsReceived:'+ convert(varchar, @ComplaintsReceived)
	 
	--************************************ 
	 select @CountAdvocateMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (512,519)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateMonthly:'+ convert(varchar, @CountAdvocateMonthly)
	 
	  
	 select @CountFieldMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging to field
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldMonthly:'+ convert(varchar, @CountFieldMonthly)
	 
	  
	 select @CountMedicalMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging to MEDICAL 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalMonthly:'+ convert(varchar, @CountMedicalMonthly)
	 
	   select @CountSSLUMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUMonthly:'+ convert(varchar, @CountSSLUMonthly)
	 
	 set @CountDATMonthly=0
	 --select @CountDATMonthly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 --on complanant.ComplainantId=compAdvocacy.ComplaintId
		--WHERE compAdvocacy.FinalStatuscd in (96,97)
		--and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		--and year(compAdvocacy.RecieveDate)>=year(@FromDate)
		--and year(compAdvocacy.RecieveDate)<=year(@ToDate)
		--and complanant.LocationId=519  -- Location belonging to DAT
	 --and complanant.ComplainantType='C'
	 --	 print '@CountDATMonthly:'+ convert(varchar, @CountDATMonthly)
	 	 
	select @CountTrainingMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  -- 16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingMonthly:'+ convert(varchar, @CountTrainingMonthly)

-- Complaints disposed 

	Declare @CountPleadGuilty int
	Declare @CountDismissal int
	Declare @CountBCAD int
	Declare @CountInformalConferences int
	Declare @CountVoids int
	Declare @Retired_Resigned_Terminated int
	Declare @TotalComplaintDisposed int
	Declare @ComplaintRemainingOpened int

	select @CountPleadGuilty = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=64   -- Plead Guilty
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	select @CountDismissal = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=62   -- Motion to Dismiss
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	select @CountBCAD= COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=39 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	select @CountInformalConferences = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=40 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	
	select @CountVoids = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=41 
	and	FinalStatuscd=97 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	
	select @Retired_Resigned_Terminated = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and (ah.HearingStatusCd=71 or ah.HearingStatusCd=165 or ah.HearingStatusCd=166)
	   -- Retired /Resigned /Terminated
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	and convert(date,ClosedDate  ,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	and ca.IsAvailable is null
-- Declaring and Returning Table.

	select @TotalComplaintDisposed = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=97 
	and IsAvailable is null
	and convert(date,ClosedDate,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate

	select @ComplaintRemainingOpened = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=96 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	and convert(date,ModifiedDate,101) <=@ToDate--GETDATE()



	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ca.ModifiedDate >=@FromDate
		and ca.ModifiedDate <=@ToDate
	and ca.IsAvailable is null	
	and ah.HearingStatusCd=65 -- Conf Requested Trial

--select * from #tblHearingDate 

	Declare @ConferenceCalendarDates varchar(800)
		select @ConferenceCalendarDates= 
		--coalesce(@ConferenceCalendarDates+',','')+ CONVERT(varchar(Max), HearingDt,101) 
		--coalesce(@ConferenceCalendarDates+',','') +  CAST( HearingDt as varchar(Max)) 
	coalesce(@ConferenceCalendarDates+',','') +  convert( varchar,HearingDt,101) --cast(HearingDt as varchar(Max)) 

		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @ConferenceCalendarDates
	
	
	



	Declare @DiscretionaryReport table
     (CountNotAdjudicated int ,
      ComplaintsReceived int,
      CountAdvocateMonthly int,
      CountMedicalMonthly int,
      CountSSLUMonthly int,
      CountDATMonthly int,
      CountTrainingMonthly int,
      CountFieldMonthly int,
      CountPleadGuilty int,
	  CountDismissal int,
	  CountBCAD int,
	 CountInformalConferences int,
	 CountVoids int,
	 Retired_Resigned_Terminated int,
	 TotalComplaintDisposed int,
	 ComplaintRemainingOpened int,
	 ConferenceCalendarDates varchar(800)
      )


  insert @DiscretionaryReport 
  values(@CountNotAdjudicated,@ComplaintsReceived,@CountAdvocateMonthly,
   @CountMedicalMonthly,@CountSSLUMonthly,@CountDATMonthly,@CountTrainingMonthly,
   @CountFieldMonthly,
   @CountPleadGuilty ,@CountDismissal ,@CountBCAD ,@CountInformalConferences ,
   @CountVoids ,@Retired_Resigned_Terminated ,@TotalComplaintDisposed ,
   @ComplaintRemainingOpened ,@ConferenceCalendarDates
  )
  select * from @DiscretionaryReport

END




GO

ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary2]
	
-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	Declare @CountAdvocateYearly INT 
	Declare @CountFieldYearly INT 
	Declare @CountMedicalYearly INT 
	Declare @CountSSLUYearly INT 
	Declare @CountDATYearly INT 
	Declare @CountTrainingYearly INT 
	SET NOCOUNT ON;
	declare @yearStart DATE
	SET @yearStart=CONVERT(DATE, '01/01/'+ CONVERT(VARCHAR,YEAR(CONVERT(DATE, @FromDate))));
    PRINT @yearStart;
	--************************************ 
	 select @CountAdvocateYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (519,512)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateYearly:'+ convert(varchar, @CountAdvocateYearly)
	 
	  
	 select @CountFieldYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging Field
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldYearly:'+ convert(varchar, @CountFieldYearly)
	 
	  
	 select @CountMedicalYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging 	MEDICAL OFFICE 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalYearly:'+ convert(varchar, @CountMedicalYearly)
	 
	   select @CountSSLUYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUYearly:'+ convert(varchar, @CountSSLUYearly)
	 
	 set @CountDATYearly=0
	 --select @CountDATYearly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 --on complanant.ComplainantId=compAdvocacy.ComplaintId
		--WHERE compAdvocacy.FinalStatuscd in (96,97)
		--and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		--and compAdvocacy.RecieveDate>= @yearStart
		--and compAdvocacy.RecieveDate<=@ToDate
		--and complanant.LocationId=519  -- Location belonging to DAT
	 --and complanant.ComplainantType='C'
	 --	 print '@CountDATYearly:'+ convert(varchar, @CountDATYearly)
	 	 
	select @CountTrainingYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  --16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingYearly:'+ convert(varchar, @CountTrainingYearly)

-- Complaints disposed 

	Declare @AwaitingTrialDateAtOath int
	Declare @CountCasesScheduled int
	Declare @CountCasesHeard int	
	Declare @CountDispostionFromOath int 
	
	Declare @CountAwaitingAdjudicationComplaintsNo int
	Declare @CountAwaitingAdjudicationComplaintsEmp int
	Declare @CountAwaitingOathTrialComplaintsNo int
	Declare @CountAwaitingOathTrialComplaintsEmp int

	select @AwaitingTrialDateAtOath = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
	ca.FinalStatuscd=96  -- Open Complaints
	 	and ah.HearingStatusCd=65   -- CONF. - REQUEST TRIAL
		and ModifiedDate >=@FromDate 
	and ModifiedDate <=@ToDate
	and ca.IsAvailable is null

	
	select @CountCasesScheduled = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ah.CalStatusCd=50   -- SCHED			
		and convert(date,ModifiedDate,101 ) >=@FromDate 
	and convert(date,ModifiedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	
select @CountCasesHeard= COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
		 ah.HearingStatusCd in (54,55,56,57,58)   -- SCHED			
		and convert(date,ModifiedDate,101 ) >=@FromDate 
	and convert(date,ModifiedDate,101 ) <=@ToDate
	and ca.IsAvailable is null
	
	
	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where
	-- ca.FinalStatuscd=96  -- Open Complaints
	--and month(ModifiedDate) =01
	--and year(ModifiedDate) =2010 and
	 month(HearingDt)=MONTH(GETDATE())
	 
	and year(HearingDt) =year(@ToDate)
	
	and ca.IsAvailable is null	
	and ah.CalStatusCd=47 -- Oath Sched  
print MONTH(@FromDate)+1
--select * from #tblHearingDate 

	Declare @CaseScheduledDates varchar(800)
		select @CaseScheduledDates= 
		coalesce(@CaseScheduledDates+',','')+ CONVERT(varchar, HearingDt,101) 
		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @CaseScheduledDates
	
	
		
				select @CountDispostionFromOath=COUNT(*) 
				from tblDS249Complaints comp inner join
				
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
			on ca.AdvocacyCaseId=ah.AdvocacyCaseID

			where 
 			 ah.HearingStatusCd=54   -- H Held Desc Reserved.
 			 and convert(date,ca.ModifiedDate,101) >=@FromDate
 			 and convert(date,ca.ModifiedDate,101) <=@ToDate
			and ca.IsAvailable is null
			
			) adv
			on comp.ComplaintId=adv.ComplaintId
			inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
			inner join tblPersons p on p.PersonID=emp.PersonID
	
	
		
		--select p.FirstName+ ' '+p.LastName Name ,
		--CONVERT(date,adv.HearingDt) HearingDate  
		--from tblDS249Complaints comp inner join
		--		(select distinct ca.ComplaintId,ah.HearingDt 
		--		from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		--		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		--		where ah.HearingStatusCd=54   -- H Held Desc Reserved.
 	--	        and YEAR(ca.ModifiedDate) =year(@ToDate)
		--	    and ca.IsAvailable is null
		--	    and ah.HearingDt is not null) adv
		--on comp.ComplaintId=adv.ComplaintId
		--inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		--inner join tblPersons p on p.PersonID=emp.PersonID
	
	
	select @CountAwaitingAdjudicationComplaintsNo=COUNT(*) from tblComplaintAdvocacy
	where FinalStatuscd in (96,98) 
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
		
	and IsAvailable is null
	
	select @CountAwaitingAdjudicationComplaintsEmp=COUNT(distinct RespondentEmpID ) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
	on comp.ComplaintId=ca.ComplaintId
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
	
	where FinalStatuscd in (96,98)
	

		
	select @CountAwaitingOathTrialComplaintsNo= COUNT(distinct ca.AdvocacyCaseId) 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ah.HearingStatusCd in (65)   -- SCHED	
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
					
		and ca.IsAvailable is null
	
	
	select @CountAwaitingOathTrialComplaintsEmp= COUNT(distinct comp.RespondentEmpID) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
	on comp.ComplaintId=ca.ComplaintId
	inner join
	(
	select distinct ca.AdvocacyCaseId 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ah.HearingStatusCd in (65)   -- SCHED		
	 and convert(date,ModifiedDate,101) >=@FromDate
 			 and convert(date,ModifiedDate,101)<=@ToDate
				
		and ca.IsAvailable is null
	) adv on ca.AdvocacyCaseId=adv.AdvocacyCaseId
	
	
		Declare @DiscretionaryReport table
     (
      CountAdvocateYearly int,
      CountMedicalYearly int,
      CountSSLUYearly int,
      CountDATYearly int,
      CountTrainingYearly INT,
      CountFieldYearly int,
	  AwaitingTrialDateAtOath int,
	  CountCasesScheduled int,
	  CountCasesHeard int	,
      CountDispostionFromOath int, 
	  CaseScheduledDates varchar(800),
      CountAwaitingAdjudicationComplaintsNo int,
	  CountAwaitingAdjudicationComplaintsEmp int,
	  CountAwaitingOathTrialComplaintsNo int,
	  CountAwaitingOathTrialComplaintsEmp int
      )


  insert @DiscretionaryReport 
  values(@CountAdvocateYearly,@CountMedicalYearly,@CountSSLUYearly,
   @CountDATYearly,@CountTrainingYearly,@CountFieldYearly,
   @AwaitingTrialDateAtOath ,@CountCasesScheduled ,
   @CountCasesHeard ,@CountDispostionFromOath ,
   @CaseScheduledDates ,@CountAwaitingAdjudicationComplaintsNo ,
   @CountAwaitingAdjudicationComplaintsEmp ,
   @CountAwaitingOathTrialComplaintsNo,
   @CountAwaitingOathTrialComplaintsEmp
  )
  select * from @DiscretionaryReport
	

END



GO
ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary3]
	-- Add the parameters for the stored procedure here
	
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
select distinct p.FirstName+ ' '+p.LastName Name ,
		CONVERT(date,adv.HearingDt) HearingDate  
		from tblDS249Complaints comp inner join
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
				on ca.AdvocacyCaseId=ah.AdvocacyCaseID
				where ah.HearingStatusCd=54   -- H Held Desc Reserved.
 		        and YEAR(ah.HearingDt) =year(@ToDate)
			    and ca.IsAvailable is null
			    and ah.HearingDt is not null) adv
		on comp.ComplaintId=adv.ComplaintId
		inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		inner join tblPersons p on p.PersonID=emp.PersonID
		order by HearingDate asc 

END







GO
ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary1]
	-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @CountNotAdjudicated INT 
	Declare @ComplaintsReceived INT 
	Declare @CountAdvocateMonthly INT 
	Declare @CountFieldMonthly INT 
	Declare @CountMedicalMonthly INT 
	Declare @CountSSLUMonthly INT 
	Declare @CountDATMonthly INT 
	Declare @CountTrainingMonthly INT 
	SET NOCOUNT ON;
	
	SELECT @CountNotAdjudicated=COUNT(*) 
	FROM tblComplaintAdvocacy 
	WHERE RecieveDate is not null
	and ModifiedDate is null and FinalStatuscd=96
	--and IsAvailable is null 
	and RecieveDate >=@FromDate and RecieveDate <=@ToDate
    -- Insert statements for procedure here

	--set @month=MONTH(@FromDate);
	--set @year=YEAR(@FromDate);





	print  '@CountNotAdjudicated'+ convert(varchar, @CountNotAdjudicated);
	 --print @month;
	 --print @year;
	 
	  SELECT @ComplaintsReceived=COUNT( distinct ca.ComplaintId)
	 FROM tblComplaintAdvocacy ca inner join tblDS249Complainant cn
	 on ca.ComplaintId=cn.ComplaintId
	 WHERE FinalStatuscd in (96,97)
		and IsAvailable is null 
		and ModifiedDate is not null
		and RecieveDate>=@FromDate
		and RecieveDate<=@ToDate
		and cn.ComplainantType='C'
		and cn.LocationId is not null 
		and cn.LocationId <>-1
		
		
	 print '@ComplaintsReceived:'+ convert(varchar, @ComplaintsReceived)
	 
	--************************************ 
	 select @CountAdvocateMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (512,519)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateMonthly:'+ convert(varchar, @CountAdvocateMonthly)
	 
	  
	 select @CountFieldMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging to field
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldMonthly:'+ convert(varchar, @CountFieldMonthly)
	 
	  
	 select @CountMedicalMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging to MEDICAL 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalMonthly:'+ convert(varchar, @CountMedicalMonthly)
	 
	   select @CountSSLUMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUMonthly:'+ convert(varchar, @CountSSLUMonthly)
	 
	 set @CountDATMonthly=0
	 --select @CountDATMonthly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 --on complanant.ComplainantId=compAdvocacy.ComplaintId
		--WHERE compAdvocacy.FinalStatuscd in (96,97)
		--and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		--and year(compAdvocacy.RecieveDate)>=year(@FromDate)
		--and year(compAdvocacy.RecieveDate)<=year(@ToDate)
		--and complanant.LocationId=519  -- Location belonging to DAT
	 --and complanant.ComplainantType='C'
	 --	 print '@CountDATMonthly:'+ convert(varchar, @CountDATMonthly)
	 	 
	select @CountTrainingMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  -- 16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingMonthly:'+ convert(varchar, @CountTrainingMonthly)

-- Complaints disposed 

	Declare @CountPleadGuilty int
	Declare @CountDismissal int
	Declare @CountBCAD int
	Declare @CountInformalConferences int
	Declare @CountVoids int
	Declare @Retired_Resigned_Terminated int
	Declare @TotalComplaintDisposed int
	Declare @ComplaintRemainingOpened int

	select @CountPleadGuilty = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=64   -- Plead Guilty
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	--and convert(date,ClosedDate  ,101 )>=@FromDate 
	--and convert(date,ClosedDate,101 ) <=@ToDate
	and ah.HearingDt>=@FromDate 
	and ah.HearingDt<=@ToDate
	and ca.IsAvailable is null
	
	select @CountDismissal = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=62   -- Motion to Dismiss
	and ah.HearingDt>=@FromDate 
	and ah.HearingDt<=@ToDate	
	and ca.IsAvailable is null
	
	select @CountBCAD= COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=39 
	and	FinalStatuscd=97 
	and IsAvailable is null
	and ActionDate>=@FromDate 
	and ActionDate<=@ToDate
	
	
	
	select @CountInformalConferences = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=40 
	and	FinalStatuscd=97 
	and IsAvailable is null
	and ActionDate>=@FromDate 
	and ActionDate<=@ToDate
	
	
	select @CountVoids = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=41 
	and	FinalStatuscd=97 
	and IsAvailable is null
	and ActionDate>=@FromDate 
	and ActionDate<=@ToDate
	
	
	select @Retired_Resigned_Terminated = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and (ah.HearingStatusCd=71 or ah.HearingStatusCd=67 or ah.HearingStatusCd=165 or ah.HearingStatusCd=166)
	   -- Retired /Resigned /Terminated
	
	and ca.IsAvailable is null
	and ActionDate>=@FromDate 
	and ActionDate<=@ToDate
	
	
	
	
-- Declaring and Returning Table.

	select @TotalComplaintDisposed = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=97 
	and IsAvailable is null
	and convert(date,ClosedDate,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate

	select @ComplaintRemainingOpened = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=96 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	and convert(date,ModifiedDate,101) <=@ToDate--GETDATE()



	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ah.HearingDt >=@FromDate
		and ah.HearingDt <=@ToDate
	and ca.IsAvailable is null	
	and ah.HearingStatusCd=65 -- Conf Requested Trial

--select * from #tblHearingDate 

	Declare @ConferenceCalendarDates varchar(800)
		select @ConferenceCalendarDates= 
		--coalesce(@ConferenceCalendarDates+',','')+ CONVERT(varchar(Max), HearingDt,101) 
		--coalesce(@ConferenceCalendarDates+',','') +  CAST( HearingDt as varchar(Max)) 
	coalesce(@ConferenceCalendarDates+',','') +  convert( varchar,HearingDt,101) --cast(HearingDt as varchar(Max)) 

		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @ConferenceCalendarDates
	
	
	



	Declare @DiscretionaryReport table
     (CountNotAdjudicated int ,
      ComplaintsReceived int,
      CountAdvocateMonthly int,
      CountMedicalMonthly int,
      CountSSLUMonthly int,
      CountDATMonthly int,
      CountTrainingMonthly int,
      CountFieldMonthly int,
      CountPleadGuilty int,
	  CountDismissal int,
	  CountBCAD int,
	 CountInformalConferences int,
	 CountVoids int,
	 Retired_Resigned_Terminated int,
	 TotalComplaintDisposed int,
	 ComplaintRemainingOpened int,
	 ConferenceCalendarDates varchar(800)
      )


  insert @DiscretionaryReport 
  values(@CountNotAdjudicated,@ComplaintsReceived,@CountAdvocateMonthly,
   @CountMedicalMonthly,@CountSSLUMonthly,@CountDATMonthly,@CountTrainingMonthly,
   @CountFieldMonthly,
   @CountPleadGuilty ,@CountDismissal ,@CountBCAD ,@CountInformalConferences ,
   @CountVoids ,@Retired_Resigned_Terminated ,@TotalComplaintDisposed ,
   @ComplaintRemainingOpened ,@ConferenceCalendarDates
  )
  select * from @DiscretionaryReport

END




GO

ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary2]
	
-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.

	Declare @CountAdvocateYearly INT 
	Declare @CountFieldYearly INT 
	Declare @CountMedicalYearly INT 
	Declare @CountSSLUYearly INT 
	Declare @CountDATYearly INT 
	Declare @CountTrainingYearly INT 
	Declare @Month int
	Declare @Year int
	SET NOCOUNT ON;
	declare @yearStart DATE
	SET @yearStart=CONVERT(DATE, '01/01/'+ CONVERT(VARCHAR,YEAR(CONVERT(DATE, @FromDate))));
    PRINT @yearStart;
	--************************************ 
	 select @CountAdvocateYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (519,512)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateYearly:'+ convert(varchar, @CountAdvocateYearly)
	 
	  
	 select @CountFieldYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging Field
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldYearly:'+ convert(varchar, @CountFieldYearly)
	 
	  
	 select @CountMedicalYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging 	MEDICAL OFFICE 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalYearly:'+ convert(varchar, @CountMedicalYearly)
	 
	   select @CountSSLUYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUYearly:'+ convert(varchar, @CountSSLUYearly)
	 
	 set @CountDATYearly=0
	 --select @CountDATYearly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 --on complanant.ComplainantId=compAdvocacy.ComplaintId
		--WHERE compAdvocacy.FinalStatuscd in (96,97)
		--and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		--and compAdvocacy.RecieveDate>= @yearStart
		--and compAdvocacy.RecieveDate<=@ToDate
		--and complanant.LocationId=519  -- Location belonging to DAT
	 --and complanant.ComplainantType='C'
	 --	 print '@CountDATYearly:'+ convert(varchar, @CountDATYearly)
	 	 
	select @CountTrainingYearly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>= @yearStart
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  --16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingYearly:'+ convert(varchar, @CountTrainingYearly)

-- Complaints disposed 

	Declare @AwaitingTrialDateAtOath int
	Declare @CountCasesScheduled int
	Declare @CountCasesHeard int	
	Declare @CountDispostionFromOath int 
	
	Declare @CountAwaitingAdjudicationComplaintsNo int
	Declare @CountAwaitingAdjudicationComplaintsEmp int
	Declare @CountAwaitingOathTrialComplaintsNo int
	Declare @CountAwaitingOathTrialComplaintsEmp int

	select @AwaitingTrialDateAtOath = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
	ca.FinalStatuscd=96  -- Open Complaints
	 	and ah.HearingStatusCd=65   -- CONF. - REQUEST TRIAL
		and HearingDt >=@FromDate 
	and HearingDt <=@ToDate
	and ca.IsAvailable is null

	
	select @CountCasesScheduled = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ah.CalStatusCd=47   -- SCHED			
		and HearingDt >=@FromDate 
	and HearingDt <=@ToDate
	and ca.IsAvailable is null
	
	
select @CountCasesHeard= COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where 
		 ah.HearingStatusCd in (54,55,56,57,58)   -- SCHED			
		and HearingDt >=@FromDate 
	and HearingDt <=@ToDate
	and ca.IsAvailable is null
	
	
	
	
--	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
--	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
--	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
--	where
--	-- ca.FinalStatuscd=96  -- Open Complaints
--	--and month(ModifiedDate) =01
--	--and year(ModifiedDate) =2010 and
--	 month(HearingDt)=MONTH(GETDATE())
	 
--	and year(HearingDt) =year(@ToDate)
	
--	and ca.IsAvailable is null	
--	and ah.CalStatusCd=47 -- Oath Sched  
--print MONTH(@FromDate)+1



   
	
	If(MONTH(@ToDate)<=11)
	
		BEGIN
			set @Month=MONTH(@ToDate)+1
			set @Year=YEAR(@ToDate)
		END
	ELSE
			BEGIN
				set @Month=1
				set @Year=YEAR(@ToDate)+1
			END
	
	
	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where month(HearingDt)=@Month --MONTH(@ToDate)+1	 
	and year(HearingDt) =@year--@year(@ToDate)
	and ca.IsAvailable is null	
	and ah.CalStatusCd=47 -- Oath Sched  



--select * from #tblHearingDate 

	Declare @CaseScheduledDates varchar(800)
		select @CaseScheduledDates= 
		coalesce(@CaseScheduledDates+',','')+ CONVERT(varchar, HearingDt,101) 
		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @CaseScheduledDates
	
	
		
		
		
		
				select @CountDispostionFromOath=COUNT(*) 
				from tblDS249Complaints comp inner join
				
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
			on ca.AdvocacyCaseId=ah.AdvocacyCaseID

			where 
 			 ah.HearingStatusCd=55   -- H Held Desc Reserved.
 			and HearingDt >=@FromDate 
			and HearingDt <=@ToDate
			and ca.IsAvailable is null
			
			) adv
			on comp.ComplaintId=adv.ComplaintId
			inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
			inner join tblPersons p on p.PersonID=emp.PersonID
	
	
		
		--select p.FirstName+ ' '+p.LastName Name ,
		--CONVERT(date,adv.HearingDt) HearingDate  
		--from tblDS249Complaints comp inner join
		--		(select distinct ca.ComplaintId,ah.HearingDt 
		--		from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
		--		on ca.AdvocacyCaseId=ah.AdvocacyCaseID
		--		where ah.HearingStatusCd=54   -- H Held Desc Reserved.
 	--	        and YEAR(ca.ModifiedDate) =year(@ToDate)
		--	    and ca.IsAvailable is null
		--	    and ah.HearingDt is not null) adv
		--on comp.ComplaintId=adv.ComplaintId
		--inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		--inner join tblPersons p on p.PersonID=emp.PersonID
	
	
	select @CountAwaitingAdjudicationComplaintsNo=COUNT(*) from tblComplaintAdvocacy
	where FinalStatuscd in (96,98) 
	 and  ActionDate >=@FromDate
 			 and ActionDate<=@ToDate
		
	and IsAvailable is null
	
	select @CountAwaitingAdjudicationComplaintsEmp=COUNT(distinct RespondentEmpID ) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
	on comp.ComplaintId=ca.ComplaintId
	 and ActionDate >=@FromDate
 			 and ActionDate<=@ToDate
	
	where FinalStatuscd in (96,98)
	

		
	select @CountAwaitingOathTrialComplaintsNo= COUNT(distinct ca.AdvocacyCaseId) 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ah.HearingStatusCd in (65)   -- SCHED	
	 and HearingDt  >=@FromDate
 			 and HearingDt <=@ToDate
					
		and ca.IsAvailable is null
	
	
	select @CountAwaitingOathTrialComplaintsEmp= COUNT(distinct comp.RespondentEmpID) from tblDS249Complaints comp inner join tblComplaintAdvocacy ca
	on comp.ComplaintId=ca.ComplaintId
	inner join
	(
	select distinct ca.AdvocacyCaseId 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ah.HearingStatusCd in (65)   -- SCHED		
	 and HearingDt >=@FromDate
 			 and HearingDt <=@ToDate
				
		and ca.IsAvailable is null
	) adv on ca.AdvocacyCaseId=adv.AdvocacyCaseId
	
	
		Declare @DiscretionaryReport table
     (
      CountAdvocateYearly int,
      CountMedicalYearly int,
      CountSSLUYearly int,
      CountDATYearly int,
      CountTrainingYearly INT,
      CountFieldYearly int,
	  AwaitingTrialDateAtOath int,
	  CountCasesScheduled int,
	  CountCasesHeard int	,
      CountDispostionFromOath int, 
	  CaseScheduledDates varchar(800),
      CountAwaitingAdjudicationComplaintsNo int,
	  CountAwaitingAdjudicationComplaintsEmp int,
	  CountAwaitingOathTrialComplaintsNo int,
	  CountAwaitingOathTrialComplaintsEmp int
      )


  insert @DiscretionaryReport 
  values(@CountAdvocateYearly,@CountMedicalYearly,@CountSSLUYearly,
   @CountDATYearly,@CountTrainingYearly,@CountFieldYearly,
   @AwaitingTrialDateAtOath ,@CountCasesScheduled ,
   @CountCasesHeard ,@CountDispostionFromOath ,
   @CaseScheduledDates ,@CountAwaitingAdjudicationComplaintsNo ,
   @CountAwaitingAdjudicationComplaintsEmp ,
   @CountAwaitingOathTrialComplaintsNo,
   @CountAwaitingOathTrialComplaintsEmp
  )
  select * from @DiscretionaryReport
	

END





GO
ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary3]
	-- Add the parameters for the stored procedure here
	
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
select distinct p.FirstName+ ' '+p.LastName Name ,
		CONVERT(date,adv.HearingDt) HearingDate  
		from tblDS249Complaints comp inner join
				(select distinct ca.ComplaintId,ah.HearingDt 
				from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
				on ca.AdvocacyCaseId=ah.AdvocacyCaseID
				where ah.HearingStatusCd=55   -- H Held Desc Reserved.
 		        and YEAR(ah.HearingDt) =year(@ToDate)
			    and ca.IsAvailable is null
			    and ah.HearingDt is not null) adv
		on comp.ComplaintId=adv.ComplaintId
		inner join tblEmployees emp on emp.EmployeeID=comp.RespondentEmpID
		inner join tblPersons p on p.PersonID=emp.PersonID
		order by HearingDate asc 

END















GO

CREATE PROCEDURE [dbo].[usp_RptBreakDownDisciplinaryADV]
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	
		SET NOCOUNT ON;	
	 	 
	--************************************************************************
	--Breakdown of Disciplinary Complaints received For The Month For Advocate
	--************************************************************************	
	 
	    select UPPER(ct.ChargeDesc) as AdvRuleDesc  ,COUNT(*) as  AdvCount
		from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
		on complanant.ComplaintId=compAdvocacy.ComplaintId
		inner join tblDS249Charges ch on ch.ComplaintId=compAdvocacy.ComplaintId
		inner join tblChargeType ct on ch.ChargeTypeID=ct.ChargeTypeId		
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate 
		and complanant.LocationId in (512,519)  -- Location belonging to ADVOCATE office
		 and complanant.ComplainantType='C'
		and complanant.LocationId is not null 
		and complanant.LocationId <>-1
		group by ct.ChargeDesc 
END



GO

CREATE PROCEDURE [dbo].[usp_RptBreakDownDisciplinaryField]
		
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	
		SET NOCOUNT ON;	
	 
	--***********************************************************************
	--Breakdown of Disciplinary Complaints received For the  Month For Field
	--***********************************************************************
	 
	   select UPPER(ct.ChargeDesc) as FieldRuleDesc  ,COUNT(*) as  FieldCount
		from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
		on complanant.ComplaintId=compAdvocacy.ComplaintId
		inner join tblDS249Charges ch on ch.ComplaintId=compAdvocacy.ComplaintId
		inner join tblChargeType ct on ch.ChargeTypeID=ct.ChargeTypeId	
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging to FIELD
		and complanant.ComplainantType='C'
		and complanant.LocationId is not null 
		and complanant.LocationId <>-1
		group by ct.ChargeDesc
		 
END



GO

CREATE PROCEDURE [dbo].[usp_RptBreakDownDisciplinaryMedDiv]	
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN	
		SET NOCOUNT ON;		 
	 
	--************************************************************************
	--Breakdown of Disciplinary Complaints received For the Month For Medical
	--************************************************************************	
		  
     --DAT complaints have considered under Medical
		  
		select UPPER(ct.ChargeDesc) as MedDivRuleDesc  ,COUNT(*) as  MedDivCount
		from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
		on complanant.ComplaintId=compAdvocacy.ComplaintId
		inner join tblDS249Charges ch on ch.ComplaintId=compAdvocacy.ComplaintId
		inner join tblChargeType ct on ch.ChargeTypeID=ct.ChargeTypeId		
	    WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging to MEDICAL 
		and complanant.ComplainantType='C'
		and complanant.LocationId is not null 
		and complanant.LocationId <>-1
		group by ct.ChargeDesc

END


GO

CREATE PROCEDURE [dbo].[usp_RptBreakDownDisciplinarySSLU]	
	
	@FromDate date,
	@ToDate date	
	
AS
BEGIN	
		SET NOCOUNT ON;		 
	 
	--*********************************************************************
	--Breakdown of Disciplinary Complaints received For the Month For SSLU
	--*********************************************************************	
	
	    select UPPER(ct.ChargeDesc) as SSLURuleDesc  ,COUNT(*) as  SSLUCount
		from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
		on complanant.ComplaintId=compAdvocacy.ComplaintId
		inner join tblDS249Charges ch on ch.ComplaintId=compAdvocacy.ComplaintId
		inner join tblChargeType ct on ch.ChargeTypeID=ct.ChargeTypeId		
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
		and complanant.ComplainantType='C'
		and complanant.LocationId is not null 
		and complanant.LocationId <>-1
		group by ct.ChargeDesc
END

GO

CREATE PROCEDURE [dbo].[usp_RptBreakDownDisciplinaryTraining]	
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	
	SET NOCOUNT ON;	 
	 
	--************************************************************************
	--Breakdown of Disciplinary Complaints received For The Month For Training
	--************************************************************************	
	 	 
		select UPPER(ct.ChargeDesc) as TrainingRuleDesc  ,COUNT(*) as  TrainingCount
		from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
		on complanant.ComplaintId=compAdvocacy.ComplaintId
		inner join tblDS249Charges ch on ch.ComplaintId=compAdvocacy.ComplaintId
		inner join tblChargeType ct on ch.ChargeTypeID=ct.ChargeTypeId		
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  -- 16 Location belonging to Training and 14,15 are child locations
		and complanant.ComplainantType='C'
		and complanant.LocationId is not null 
		and complanant.LocationId <>-1
		group by ct.ChargeDesc 
	
	
END



GO

ALTER PROCEDURE [dbo].[usp_GetBCADComplaints] 
@PageNo int = 0,
@maximumRows int = 0
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @Count int
Set @Count = (SELECT COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0)
 
 DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;

 WITH BCadComplaints  AS
(
	SELECT
		ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ISNULL((select top 1 sentdate from tblRoutingHistory 
		where ComplaintId=a.ComplaintId and SentTo='BCAD' order by SentDate desc ),a.CreatedDate) ComplaintStatusDate
		,'false' as Approved 
		, Charges    
		, (select top 1 IncidentDate from tblDS249Incidents 
		where ComplaintId=a.ComplaintId   order by IncidentDate desc ) IncidentDate   
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted 
         
           
        -- ,ROW_NUMBER() over (PARTITION BY r.complaintid order by i.complaintid) as compIndex1     
 FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		--left outer join tblRoutingHistory r on a.ComplaintId =r.ComplaintID 
		
 where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0 
	    
	    
   

)
		select * from 
		BCadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
		
END


GO
ALTER PROCEDURE [dbo].[usp_RptGetDisciplinary1]
	-- Add the parameters for the stored procedure here
	
	@FromDate date,
	@ToDate date
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @CountNotAdjudicated INT 
	Declare @ComplaintsReceived INT 
	Declare @CountAdvocateMonthly INT 
	Declare @CountFieldMonthly INT 
	Declare @CountMedicalMonthly INT 
	Declare @CountSSLUMonthly INT 
	Declare @CountDATMonthly INT 
	Declare @CountTrainingMonthly INT 
	SET NOCOUNT ON;
	
	SELECT @CountNotAdjudicated=COUNT(*) 
	FROM tblComplaintAdvocacy 
	WHERE RecieveDate is not null
	and ModifiedDate is null and FinalStatuscd=96
	--and IsAvailable is null 
	and RecieveDate >=@FromDate and RecieveDate <=@ToDate
    -- Insert statements for procedure here

	--set @month=MONTH(@FromDate);
	--set @year=YEAR(@FromDate);





	print  '@CountNotAdjudicated'+ convert(varchar, @CountNotAdjudicated);
	 --print @month;
	 --print @year;
	 
	  SELECT @ComplaintsReceived=COUNT( distinct ca.ComplaintId)
	 FROM tblComplaintAdvocacy ca inner join tblDS249Complainant cn
	 on ca.ComplaintId=cn.ComplaintId
	 WHERE FinalStatuscd in (96,97)
		and IsAvailable is null 
		--and ModifiedDate is not null
		and RecieveDate>=@FromDate
		and RecieveDate<=@ToDate
		and cn.ComplainantType='C'
		and cn.LocationId is not null 
		and cn.LocationId <>-1
		
		
	 print '@ComplaintsReceived:'+ convert(varchar, @ComplaintsReceived)
	 
	--************************************ 
	 select @CountAdvocateMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (512,519)  -- Location belonging to advocate ADV/OFC EMPLOY-DISC MTR�( 8275760) 
	 and complanant.ComplainantType='C'
	 	and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountAdvocateMonthly:'+ convert(varchar, @CountAdvocateMonthly)
	 
	  
	 select @CountFieldMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId not in (519,512,565,566,14,15,16) -- Location belonging to field
	    and complanant.ComplainantType='C'
	    and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountFieldMonthly:'+ convert(varchar, @CountFieldMonthly)
	 
	  
	 select @CountMedicalMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
	and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=565  --  Location belonging to MEDICAL 
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountMedicalMonthly:'+ convert(varchar, @CountMedicalMonthly)
	 
	   select @CountSSLUMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId=566  --  Location belonging to SSLU
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountSSLUMonthly:'+ convert(varchar, @CountSSLUMonthly)
	 
	 set @CountDATMonthly=0
	 --select @CountDATMonthly=COUNT(*) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	 --on complanant.ComplainantId=compAdvocacy.ComplaintId
		--WHERE compAdvocacy.FinalStatuscd in (96,97)
		--and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
		--and year(compAdvocacy.RecieveDate)>=year(@FromDate)
		--and year(compAdvocacy.RecieveDate)<=year(@ToDate)
		--and complanant.LocationId=519  -- Location belonging to DAT
	 --and complanant.ComplainantType='C'
	 --	 print '@CountDATMonthly:'+ convert(varchar, @CountDATMonthly)
	 	 
	select @CountTrainingMonthly=COUNT( distinct complanant.ComplaintId) from tblDS249Complainant complanant inner join tblComplaintAdvocacy compAdvocacy
	on complanant.ComplaintId=compAdvocacy.ComplaintId
		WHERE compAdvocacy.FinalStatuscd in (96,97)
		and compAdvocacy.IsAvailable is null 
		--and compAdvocacy.ModifiedDate is not null
and compAdvocacy.RecieveDate>=@FromDate
		and compAdvocacy.RecieveDate<=@ToDate
		and complanant.LocationId in (14,15,16)  -- 16 Location belonging Training and 14,15 are child locations
	 and complanant.ComplainantType='C'
	 and complanant.LocationId is not null 
		and complanant.LocationId <>-1
	 	 print '@CountTrainingMonthly:'+ convert(varchar, @CountTrainingMonthly)

-- Complaints disposed 

	Declare @CountPleadGuilty int
	Declare @CountDismissal int
	Declare @CountBCAD int
	Declare @CountInformalConferences int
	Declare @CountVoids int
	Declare @Retired_Resigned_Terminated int
	Declare @TotalComplaintDisposed int
	Declare @ComplaintRemainingOpened int

	select @CountPleadGuilty = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=64   -- Plead Guilty
	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate
	--and convert(date,ClosedDate  ,101 )>=@FromDate 
	--and convert(date,ClosedDate,101 ) <=@ToDate
	and ah.HearingDt>=@FromDate 
	and ah.HearingDt<=@ToDate
	and ca.IsAvailable is null
	
	select @CountDismissal = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and ah.HearingStatusCd=62   -- Motion to Dismiss
	and ah.HearingDt>=@FromDate 
	and ah.HearingDt<=@ToDate	
	and ca.IsAvailable is null
	
	select @CountBCAD= COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=39 
	and	FinalStatuscd=97 
	and IsAvailable is null
	and ActionDate>=@FromDate 
	and ActionDate<=@ToDate
	
	
	
	select @CountInformalConferences = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=40 
	and	FinalStatuscd=97 
	and IsAvailable is null
	and ActionDate>=@FromDate 
	and ActionDate<=@ToDate
	
	
	select @CountVoids = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where Actioncd=41 
	and	FinalStatuscd=97 
	and IsAvailable is null
	and ActionDate>=@FromDate 
	and ActionDate<=@ToDate
	
	
	select @Retired_Resigned_Terminated = COUNT(distinct ca.AdvocacyCaseId) from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=97  -- Closed Complaints
	and (ah.HearingStatusCd=71 or ah.HearingStatusCd=67 or ah.HearingStatusCd=165 or ah.HearingStatusCd=166)
	   -- Retired /Resigned /Terminated
	
	and ca.IsAvailable is null
	and ActionDate>=@FromDate 
	and ActionDate<=@ToDate
	
	
	
	
-- Declaring and Returning Table.

	select @TotalComplaintDisposed = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=97 
	and IsAvailable is null
	and convert(date,ClosedDate,101 )>=@FromDate 
	and convert(date,ClosedDate,101 ) <=@ToDate

	--and ModifiedDate >=@FromDate 
	--and ModifiedDate <=@ToDate

	select @ComplaintRemainingOpened = COUNT(distinct ComplaintId) from tblComplaintAdvocacy 
	where FinalStatuscd=96 
	and IsAvailable is null
	--and ModifiedDate >=@FromDate 
	and convert(date,ModifiedDate,101) <=@ToDate--GETDATE()



	select distinct CONVERT(date,HearingDt) HearingDt  into #tblHearingDate 
	from tblComplaintAdvocacy ca inner join tblAdvocateHearing ah
	on ca.AdvocacyCaseId=ah.AdvocacyCaseID
	where ca.FinalStatuscd=96  -- Open Complaints
	and ah.HearingDt >=@FromDate
		and ah.HearingDt <=@ToDate
	and ca.IsAvailable is null	
	and ah.HearingStatusCd=65 -- Conf Requested Trial

--select * from #tblHearingDate 

	Declare @ConferenceCalendarDates varchar(800)
		select @ConferenceCalendarDates= 
		--coalesce(@ConferenceCalendarDates+',','')+ CONVERT(varchar(Max), HearingDt,101) 
		--coalesce(@ConferenceCalendarDates+',','') +  CAST( HearingDt as varchar(Max)) 
	coalesce(@ConferenceCalendarDates+',','') +  convert( varchar,HearingDt,101) --cast(HearingDt as varchar(Max)) 

		from   #tblHearingDate 
   print '@CaseScheduledDates'	+ @ConferenceCalendarDates
	
	
	



	Declare @DiscretionaryReport table
     (CountNotAdjudicated int ,
      ComplaintsReceived int,
      CountAdvocateMonthly int,
      CountMedicalMonthly int,
      CountSSLUMonthly int,
      CountDATMonthly int,
      CountTrainingMonthly int,
      CountFieldMonthly int,
      CountPleadGuilty int,
	  CountDismissal int,
	  CountBCAD int,
	 CountInformalConferences int,
	 CountVoids int,
	 Retired_Resigned_Terminated int,
	 TotalComplaintDisposed int,
	 ComplaintRemainingOpened int,
	 ConferenceCalendarDates varchar(800)
      )


  insert @DiscretionaryReport 
  values(@CountNotAdjudicated,@ComplaintsReceived,@CountAdvocateMonthly,
   @CountMedicalMonthly,@CountSSLUMonthly,@CountDATMonthly,@CountTrainingMonthly,
   @CountFieldMonthly,
   @CountPleadGuilty ,@CountDismissal ,@CountBCAD ,@CountInformalConferences ,
   @CountVoids ,@Retired_Resigned_Terminated ,@TotalComplaintDisposed ,
   @ComplaintRemainingOpened ,@ConferenceCalendarDates
  )
  select * from @DiscretionaryReport

END




