BEGIN TRY

BEGIN TRAN


insert into Code Values(178,17,'DISMISSED FROM SERVICE','',null,null,null,null,null)


update a set a.ReviewStatuscd =

case ltrim(rtrim(s.IgReview)) 

      when 'Deceased' then 35 

      when 'Held' then 32 

      when 'Await Doc' then 31 

      end

from dbo.Staging_Advocate_Compalints_Test s

inner join tblDS249Complaints c on s.Jt1 = c.Jt1

inner join tblComplaintAdvocacy a on a.ComplaintId = c.ComplaintId

where igreview in ('Deceased','Held','Await Doc')



update a set a.actioncd =case(s.Igaction) when 'Void/Redraft' then 42 end

from dbo.Staging_Advocate_Compalints_Test s

inner join tblDS249Complaints c on s.Jt1 = c.Jt1

inner join tblComplaintAdvocacy a on a.ComplaintId = c.ComplaintId

where  (c.DataSource='DE' or a.DataSource ='DE') 

and s.Igaction ='Void/Redraft'



update a set a.COBStatus_cd = 178

  

  FROM [dbo].[tblComplaintAdvocacy] a

  inner join  tblDS249Complaints b on a.ComplaintId=b.ComplaintId

  inner join  dbo.Staging_Advocate_Compalints_Test s on s.Jt1 = b.Jt1

  inner join  tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId

  where   CobStat = 'DISMISSED FROM SERVICE'

      and (b.DataSource='DE' or a.DataSource ='DE')










Insert into Code
	(Code_Id,Code_Category_Id,Code_Name,Code_Description,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)values
(178,21,'REPRIMAND',NULL,NULL,NULL,'Y',NULL,GETDATE())

Insert into Code
	(Code_Id,Code_Category_Id,Code_Name,Code_Description,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)values
(179,21,'SUSPENSION',NULL,NULL,NULL,'Y',NULL,GETDATE())

Insert into Code
	(Code_Id,Code_Category_Id,Code_Name,Code_Description,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)values
(180,21,'VACATION',NULL,NULL,NULL,'Y',NULL,GETDATE())

Insert into Code
	(Code_Id,Code_Category_Id,Code_Name,Code_Description,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)values
(181,21,'HOLIDAY/CompTime',NULL,NULL,NULL,'Y',NULL,GETDATE())

Insert into Code
	(Code_Id,Code_Category_Id,Code_Name,Code_Description,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)values
(182,21,'CompTime',NULL,NULL,NULL,'Y',NULL,GETDATE())

Insert into Code
	(Code_Id,Code_Category_Id,Code_Name,Code_Description,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)values
(183,21,'FINED',NULL,NULL,NULL,'Y',NULL,GETDATE())

  



IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetAdcocateDEInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_RptGetAdcocateDEInfo]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_RptGetAdcocateDEInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N' --usp_RptGetAdcocateDEInfo ''01/01/2011'',''09/01/2011''
 
  CREATE Procedure [dbo].[usp_RptGetAdcocateDEInfo] 
  @StartDate date,
  @EndDate date
  As

Select * from
(  
SELECT 
	   b.IndexNo
	  ,d.LastName
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then ''Open''
            when a.FinalStatuscd=97
            then ''Closed''
            when a.FinalStatuscd=98
            then ''DS249 Not Yet Recieved''
            when a.FinalStatuscd=130
            then ''Send to BCAD''
            when a.FinalStatuscd=131
            then ''Return to Author''
            when a.FinalStatuscd=132
            then ''Send to Advocate''
            else ''Not Mentioned''
            end
        as ''Case Status''
      ,cast(h.HearingDt as DATE) as ''Hearing Date''
      ,b.Charges
     ,e.Code_Name as ''DE Status'',
     b.CreatedDate
  FROM [dbo].[tblComplaintAdvocacy] a
  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  inner join Code e on a.Actioncd=e.Code_Id
  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  inner join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Id in(41,42,43,45)
    and (b.DataSource=''DE'' or a.DataSource=''DE'')
  and b.CreatedDate between @StartDate and @EndDate
  
  UNION
  
SELECT 
	   b.IndexNo
	  ,d.LastName
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then ''Open''
            when a.FinalStatuscd=97
            then ''Closed''
            when a.FinalStatuscd=98
            then ''DS249 Not Yet Recieved''
            when a.FinalStatuscd=130
            then ''Send to BCAD''
            when a.FinalStatuscd=131
            then ''Return to Author''
            when a.FinalStatuscd=132
            then ''Send to Advocate''
            else ''Not Mentioned''
            end
       as ''Case Status''
     ,cast(h.HearingDt as DATE) as ''Hearing Date''
      ,b.Charges
     ,e.Code_Name as ''DE Status'',
     b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  inner join Code e on a.ReviewStatuscd=e.Code_Id
  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  inner join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Id in(32,33,34,35,38)
    and (b.DataSource=''DE'' or a.DataSource=''DE'')
  AND b.CreatedDate between @StartDate and @EndDate
  
  UNION
  SELECT 
	   b.IndexNo
	  ,d.LastName
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then ''Open''
            when a.FinalStatuscd=97
            then ''Closed''
            when a.FinalStatuscd=98
            then ''DS249 Not Yet Recieved''
            when a.FinalStatuscd=130
            then ''Send to BCAD''
            when a.FinalStatuscd=131
            then ''Return to Author''
            when a.FinalStatuscd=132
            then ''Send to Advocate''
            else ''Not Mentioned''
            end
        as ''Case Status''
     ,cast(h.HearingDt as DATE) as ''Hearing Date''
      ,b.Charges
     ,e.Code_Name as ''DE Status'',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  inner join Code e on a.SpecialStatus_cd=e.Code_Id
  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  inner join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Id in(23,24,25,26,27,28,29)
    and (b.DataSource=''DE'' or a.DataSource=''DE'')
  and b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   b.IndexNo
	  ,d.LastName
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then ''Open''
            when a.FinalStatuscd=97
            then ''Closed''
            when a.FinalStatuscd=98
            then ''DS249 Not Yet Recieved''
            when a.FinalStatuscd=130
            then ''Send to BCAD''
            when a.FinalStatuscd=131
            then ''Return to Author''
            when a.FinalStatuscd=132
            then ''Send to Advocate''
            else ''Not Mentioned''
            end
       as ''Case Status''
      ,cast(h.HearingDt as DATE) as ''Hearing Date''
      ,b.Charges
     ,e.Code_Name as ''DE Status'',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  inner join Code e on a.TrialReturnStatuscd=e.Code_Id
  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  inner join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Id =74
     and (b.DataSource=''DE'' or a.DataSource=''DE'')
  and b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   b.IndexNo
	  ,d.LastName
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then ''Open''
            when a.FinalStatuscd=97
            then ''Closed''
            when a.FinalStatuscd=98
            then ''DS249 Not Yet Recieved''
            when a.FinalStatuscd=130
            then ''Send to BCAD''
            when a.FinalStatuscd=131
            then ''Return to Author''
            when a.FinalStatuscd=132
            then ''Send to Advocate''
            else ''Not Mentioned''
            end
      as ''Case Status''
      ,cast(h.HearingDt as DATE) as ''Hearing Date''
      ,b.Charges
     ,e.Code_Name as ''DE Status'',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  inner join Code e on h.CalStatusCd=e.Code_Id
  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  inner join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Id in(48,49,52)
     and (b.DataSource=''DE'' or a.DataSource=''DE'')
  and b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   b.IndexNo
	  ,d.LastName
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then ''Open''
            when a.FinalStatuscd=97
            then ''Closed''
            when a.FinalStatuscd=98
            then ''DS249 Not Yet Recieved''
            when a.FinalStatuscd=130
            then ''Send to BCAD''
            when a.FinalStatuscd=131
            then ''Return to Author''
            when a.FinalStatuscd=132
            then ''Send to Advocate''
            else ''Not Mentioned''
            end
       as ''Case Status''
     ,cast(h.HearingDt as DATE) as ''Hearing Date''
      ,b.Charges
     ,e.Code_Name as ''DE Status'',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  inner join Code e on h.HearingStatusCd=e.Code_Id
  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  inner join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_Id in(53,55,67,68,69)
    and (b.DataSource=''DE'' or a.DataSource=''DE'')
  and b.CreatedDate between @StartDate and @EndDate
  UNION
  SELECT 
	   b.IndexNo
	  ,d.LastName 
      ,d.FirstName
      ,c.ReferenceNo
      ,case when a.FinalStatuscd=96
            then ''Open''
            when a.FinalStatuscd=97
            then ''Closed''
            when a.FinalStatuscd=98
            then ''DS249 Not Yet Recieved''
            when a.FinalStatuscd=130
            then ''Send to BCAD''
            when a.FinalStatuscd=131
            then ''Return to Author''
            when a.FinalStatuscd=132
            then ''Send to Advocate''
            else ''Not Mentioned''
            end
       as ''Case Status''
      ,cast(h.HearingDt as DATE) as ''Hearing Date''
      ,b.Charges
     ,e.Code_Name as ''DE Status'',
      b.CreatedDate
     
  FROM [dbo].[tblComplaintAdvocacy] a
  inner join tblDS249Complaints b on a.ComplaintId=b.ComplaintId
  inner join tblAdvocateHearing h on h.AdvocacyCaseID=a.AdvocacyCaseId
  inner join Code e on a.COBStatus_cd=e.Code_Id
  inner join tblEmployees c on b.RespondentRefNumber=c.ReferenceNo
  inner join tblPersons d on c.PersonID=d.PersonID
  where 
  e.Code_id in(127,128,129,178) 
    and (b.DataSource=''DE'' or a.DataSource=''DE'')
  and b.CreatedDate between @StartDate and @EndDate
  ) 
  as DEInfo order by DEInfo.CreatedDate desc
  
  



' 
END









 COMMIT TRAN

END TRY

BEGIN CATCH

ROLLBACK TRAN

PRINT ERROR_MESSAGE()

END CATCH


