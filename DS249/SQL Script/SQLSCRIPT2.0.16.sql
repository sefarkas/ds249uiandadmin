



CREATE VIEW [dbo].[vw_ComissionerAcess]
AS
 SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
     Jt1
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 --WHERE a.DataSource='OEDM'
 
--order by CreatedDate
Go




ALTER PROCEDURE [dbo].[usp_GetComplaintsBySearch]
	-- Add the parameters for the stored procedure here
@CreatedBy int=0,
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@RowCount int OUTPUT 
AS
BEGIN
	SET NOCOUNT ON;
 
 DECLARE @FirstRow INT, @LastRow INT,@count INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
 
 select @count=COUNT(*) from tblMembershipRoles where PersonalID=@CreatedBy and tblMembershipRoles.RoleId=
  (Select tblRoles.Id from tblRoles where tblRoles.Roles='Commissioner Access')
  --End
  if @count>=1
     Begin
     --************************
     
      if @numericValue != 0  
   Begin      
   
   SELECT 
	    ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      a.jt1
      into #Complaints5  
 FROM dbo.vw_ComissionerAcess a 
 
 where IndexNo like '%'+Convert(varchar,@numericValue)+ '%' OR RespondentRefNumber like '%'+Convert(varchar,@numericValue) 
 or isnull(a.jt1,' ') like '%'+Convert(varchar,@numericValue)+ '%'
 order by ComplaintId desc
   
 SELECT  * FROM  #Complaints5 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
 
 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints5
 
 drop table #Complaints5  
   
   End
 else if @chargesValue != '' 
   Begin
   
      SELECT 
	    ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      , CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      a.jt1
      into #Complaints6  
 FROM dbo.vw_ComissionerAcess a 
 where Charges like '%'+@chargesValue+ '%' order by ComplaintId desc
 
	SELECT  * FROM  #Complaints6 where ROWID BETWEEN @FirstRow AND @LastRow  ORDER BY ROWID asc  
	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints6
	drop table #Complaints6     
   End	
 else if @complaintDate != ''
   Begin
   
    SELECT 	  
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      a.jt1
      into #Complaints7  
 FROM dbo.vw_ComissionerAcess a 
 order by ComplaintId desc
 	
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID,* into #adf FROM  #Complaints7 where CreatedDate like '%'+Convert(varchar,@complaintDate)+ '%'
 	SELECT * from #adf where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
 	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints7
 	drop table #Complaints7 
 	drop table #adf 
   End	
 else if @stringValue != ''
   Begin
       SELECT 
       ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,       
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      a.jt1
      into #Complaints8  
 FROM dbo.vw_ComissionerAcess a 
 
 where 
 --(LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) like '%'+@stringValue+ '%')
 RespondentFName like '%'+@stringValue+ '%' 
 OR RespondentLName like '%'+@stringValue+ '%'  
OR CreatedBy like '%'+Convert(varchar,@stringValue)+ '%' 
 OR LocationName like '%'+@stringValue+ '%' 
 or isnull(a.jt1,' ') like '%'+@stringValue+ '%'
 order by ComplaintId desc
	
	SELECT  * FROM  #Complaints8  where ROWID BETWEEN @FirstRow AND @LastRow  ORDER BY ROWID asc  
	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints8
	drop table #Complaints8     
   End	
     
     
     --*****************************
     
     
     
     End
 Else
  Begin
 
 create table #Complaints(ComplaintId int
		,IndexNo int
      ,LocationId int
      ,BoroughID int
      ,Promotiondate datetime
	  ,LocationName varchar(400)
	  ,RoutingLocation char(1)
      ,[RespondentEmpID] int
      ,[RespondentRefNumber] varchar(50)      
      ,[Approved]   int   
      ,CreatedBy  varchar(50)    
      ,[CreatedDate] datetime
      , Status varchar(2)
     ,Charges varchar(50)
	,ChargesDesc varchar(500) 
	, RespondentFName varchar(100)
	,RespondentLName varchar(100)
	,complaintStatus int
      ,complaintStatusDate datetime
      ,Indicator int,
      IsDeleted int,
      jt1 varchar(50),
      ROWID int
      )
 
insert  into #Complaints
exec dbo.usp_GetComplaintsSecurity @CreatedBy



 if @numericValue != 0  
   Begin      
   
   SELECT 
	    ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      jt1
      into #Complaints1  
 FROM [dbo].[#Complaints] a 
 
 where IndexNo like '%'+Convert(varchar,@numericValue)+ '%' OR RespondentRefNumber like '%'+Convert(varchar,@numericValue) 
 or jt1 like   Convert(varchar,@numericValue)
 order by ComplaintId desc
   
 SELECT  * FROM  #Complaints1 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
  SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints1
 drop table #Complaints1  
   
   End
 else if @chargesValue != '' 
   Begin
   
      SELECT 
	    ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      , CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      jt1
      into #Complaints2  
 FROM [dbo].#Complaints a 
 where Charges like '%'+@chargesValue+ '%' order by ComplaintId desc
 
	SELECT  * FROM  #Complaints2 where ROWID BETWEEN @FirstRow AND @LastRow  ORDER BY ROWID asc  
	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints2
	drop table #Complaints2     
   End	
 else if @complaintDate != ''
   Begin
   
    SELECT 	  
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      jt1
      into #Complaints3  
 FROM [dbo].#Complaints a 
 order by ComplaintId desc
 	
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID,* into #adf1 FROM  #Complaints3 where CreatedDate like '%'+Convert(varchar,@complaintDate)+ '%'
 	SELECT * from #adf1 where ROWID BETWEEN @FirstRow AND @LastRow ORDER BY ROWID asc     
 	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints3
 	drop table #Complaints3 
 	drop table #adf 
   End	
 else if @stringValue != ''
   Begin
       SELECT 
       ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,       
	  [ComplaintId]
	  ,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,a.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,CONVERT(varchar,CreatedDate,101) as CreatedDate
      , '' as Status
     ,charges as Charges
	,Chargesdesc as ChargesDesc
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0  end) as Indicator,
      a.IsDeleted,
      jt1
      into #Complaints4  
 FROM [dbo].#Complaints a 
 
 where 
 --(LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) like '%'+@stringValue+ '%')
 RespondentFName like '%'+@stringValue+ '%' 
 OR RespondentLName like '%'+@stringValue+ '%'  
OR CreatedBy like '%'+Convert(varchar,@stringValue)+ '%' 
 OR LocationName like '%'+@stringValue+ '%' 
 or  jt1 like   '%'+@stringValue+ '%' 
 order by ComplaintId desc
	
	SELECT  * FROM  #Complaints4  where ROWID BETWEEN @FirstRow AND @LastRow  ORDER BY ROWID asc  
	 SELECT @RowCount=COUNT([ComplaintId]) FROM #Complaints4
	drop table #Complaints4     
   End	
 	 	
 
END
END
GO


ALTER PROCEDURE [dbo].[usp_GetComplaints]
	-- Add the parameters for the stored procedure here
@CreatedBy int = 0,
@PageNo int = 0,
@maximumRows int = 0,
@rowsCount int OUTPUT
--@RowsCount int out
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @Count int
Set @Count = (SELECT	COUNT(*) as ComplaintsCount
FROM [dbo].[tblDS249Complaints] a  
LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  )

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,


      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;



select @count=COUNT(*) from tblMembershipRoles where PersonalID=@CreatedBy and tblMembershipRoles.RoleId=
  (Select tblRoles.Id from tblRoles where tblRoles.Roles='Commissioner Access')
  --End
  if @count>=1
     Begin
     
     SELECT @rowsCount=COUNT(ComplaintId) FROM dbo.vw_ComissionerAcess
     
      SELECT   ComplaintId
		,IndexNo
      ,LocationId
      ,BoroughID
      ,promotiondate
	  ,LocationName
	  ,RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,[CreatedDate]
      , Status
     ,Charges
	,ChargesDesc
	, RespondentFName
	,RespondentLName
	,complaintStatus
      ,complaintStatusDate
      ,Indicator,
      IsDeleted,
      ROWID, 
	  jt1
      FROM  dbo.vw_ComissionerAcess

      WHERE ROWID BETWEEN @FirstRow AND @LastRow

      ORDER BY ROWID asc;
      
      --select @RowsCount=COUNT(ComplaintId) from dbo.vw_ComissionerAcess
     end
     else
     begin
     


create table #Complaints(ComplaintId int
		,IndexNo int
      ,LocationId int
      ,BoroughID int
      ,Promotiondate datetime
	  ,LocationName varchar(400)
	  ,RoutingLocation char(1)
      ,[RespondentEmpID] int
      ,[RespondentRefNumber] varchar(50)      
      ,[Approved]   int   
      ,CreatedBy  varchar(50)    
      ,[CreatedDate] datetime
      , Status varchar(2)
     ,Charges varchar(50)
	,ChargesDesc varchar(500) 
	, RespondentFName varchar(100)
	,RespondentLName varchar(100)
	,complaintStatus int
      ,complaintStatusDate datetime
      ,Indicator int,
      IsDeleted int,
      jt1 varchar(15),
      ROWID int
      )

 
insert  into #Complaints
exec dbo.usp_GetComplaintsSecurity @CreatedBy;

    SELECT @rowsCount=COUNT(ComplaintId) FROM #Complaints;
     

-- WITH ComplaintsData  AS
--(

-- SELECT 
--		[ComplaintId]
--		,IndexNo
--      ,a.[LocationId]
--      ,a.[BoroughID]
--      ,a.promotiondate
--	  ,a.LocationName
--	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
--	  else null 
--	  end) as RoutingLocation
--      ,[RespondentEmpID]
--      ,[RespondentRefNumber]      
--      ,[Approved]      
--      ,CreatedBy
--      ,[CreatedDate]
--      , '' as Status
--     ,charges as Charges
--	,Chargesdesc as ChargesDesc
--	, RespondentFName
--	,RespondentLName
--	,A.complaintStatus
--      ,A.complaintStatusDate
--      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
--      else 0  end) as Indicator,
--      a.IsDeleted,
--      ROW_NUMBER() OVER (ORDER BY [CreatedDate] DESC) AS ROWID,
--      jt1
-- FROM #Complaints a 

--  )
  
  
   SELECT   ComplaintId
		,IndexNo
      ,LocationId
      ,BoroughID
      ,promotiondate
	  ,LocationName
	  ,RoutingLocation
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,CreatedBy      
      ,[CreatedDate]
      , Status
     ,Charges
	,ChargesDesc
	, RespondentFName
	,RespondentLName
	,complaintStatus
      ,complaintStatusDate
      ,Indicator,
      IsDeleted,
      ROWID,
		jt1
      --FROM  ComplaintsData
from #Complaints
      WHERE ROWID BETWEEN @FirstRow AND @LastRow

      ORDER BY ROWID asc;
      --select @RowsCount = COUNT(ComplaintId) from #Complaints
      
      drop table #Complaints
      end
  
END



Go

ALTER PROCEDURE [dbo].[usp_GetComplaintsSecurity]
	-- Add the parameters for the stored procedure here
@CreatedBy int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  declare @count int
    -- Insert statements for procedure here
  set @count=0;
  -- When data is fetched from tblFullAcces where user has 
 --select @count=count(EmployeeID) from tblFullAccessUsers where EmployeeID=@CreatedBy
  
 -- if(@count<1)
  --Begin
  Declare @locationid int	
	
	select @locationid=LocationID from tblEmployeeLocations Where EmployeeID=@CreatedBy and IsCurrent=1
 
  
  select @count=COUNT(*) from tblMembershipRoles where PersonalID=@CreatedBy and tblMembershipRoles.RoleId=
  (Select tblRoles.Id from tblRoles where tblRoles.Roles='Commissioner Access')
  --End
  if @count>=1
     Begin
        SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 --WHERE a.DataSource='OEDM'
 
 UNION 
 
 SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
	  ,a.[BoroughID]
	  ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID   
 where 
  --a.LocationId in (Select LocationID from tmpCTE) --or 
  --CreatedBy = 10044
  CreatedBy = case when @createdBy = 0 then
					CreatedBy
				Else 
					@createdBy
				end
--and a.DataSource='OEDM'
  Order by ComplaintId desc
     
     
     End
  
  Else
  
     Begin	
     

	WITH tmpCTE (LocationID, ParentLocationID) AS
	(
		SELECT LocationID,ParentLocationID 
		FROM tbllocations
		WHERE locationID = (select Top 1 LocationID from tblEmployeeLocations Where EmployeeID=@CreatedBy and IsCurrent=1)
		UNION ALL
		SELECT l.LocationID, l.ParentLocationID
		FROM tbllocations l INNER JOIN tmpCTE t	
		ON l.ParentLocationID = t.locationID
	)
--select * from tmpCTE
	select tmpCTE.LocationID,tmpCTE.ParentLocationID into #TBLTEMPLOCATION FROM tmpCTE 
-- Drop Table  #TBLTEMPLOCATION	
	SELECT tblEmployeeLocations.EmployeeID INTO #TBLTEMPORARY FROM #TBLTEMPLOCATION left outer join tblEmployeeLocations on #TBLTEMPLOCATION.LocationID=tblEmployeeLocations.LocationID
	where tblEmployeeLocations.IsCurrent=1

--	select * from #TBLTEMPORARY

--Drop table #TBLTEMPORARY	
-- Select those complaints whose charges are not in tblhidecharges.
	select *,  ROW_NUMBER() OVER (ORDER BY [CreatedDate] DESC) AS ROWID  from 

(
	SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy
-- INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 where rtrim(ltrim(a.Charges)) not in 
 (select tblDS249Complaints.Charges from 
 tblDS249Charges ,
 tblDS249Complaints ,tblHideCharges  
 where tblDS249Charges.ComplaintId=tblDS249Complaints.ComplaintId
 and tblDS249Complaints.ComplaintId=a.ComplaintId
 and tblHideCharges.ChargeTypeId=tblDS249Charges.ChargeTypeID)
 
 
 UNION
 
 SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
     
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID   
 where 
  --a.LocationId in (Select LocationID from tmpCTE) --or 
  rtrim(ltrim(a.Charges))  in 
 (select ds249Complaints.Charges from 
 tblDS249Charges ds249Charges,
 tblDS249Complaints ds249Complaints,tblHideCharges hideCharges 
 where ds249Charges.ComplaintId=ds249Complaints.ComplaintId
 and ds249Complaints.ComplaintId=a.ComplaintId
 and hideCharges.EmployeeID=@CreatedBy)
 
 
 union
 
 --- Select those complaints whose charges not
 -- if charges is created by that person.
 
 SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
  
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.CreatedBy
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID   
 where 
  --a.LocationId in (Select LocationID from tmpCTE) --or 
  c.PersonID = @CreatedBy
  and  rtrim(ltrim(a.Charges))  in 
 (select ds249Complaints.Charges from 
 tblDS249Charges ds249Charges,
 tblDS249Complaints ds249Complaints,tblHideCharges hideCharges 
 where ds249Charges.ComplaintId=ds249Complaints.ComplaintId
 and ds249Complaints.ComplaintId=a.ComplaintId)
  
  --CreatedBy = case when 226 = 0 then
		--			CreatedBy
		--		Else 
		--			226
		--		end
--and a.DataSource='OEDM'
  
 UNION 
 
 --- Select the complaints filed.
  
   SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.RespondentEmpID
-- INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 where rtrim(ltrim(a.Charges)) not in 
 (select tblDS249Complaints.Charges from 
 tblDS249Charges ,
 tblDS249Complaints ,tblHideCharges  
 where tblDS249Charges.ComplaintId=tblDS249Complaints.ComplaintId
 and tblDS249Complaints.ComplaintId=a.ComplaintId
 and tblHideCharges.ChargeTypeId=tblDS249Charges.ChargeTypeID)
 
 
 UNION
 
 SELECT
		[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
     
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 inner JOIN #TBLTEMPORARY ON #TBLTEMPORARY.EmployeeID=a.RespondentEmpID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID   
 where 
  --a.LocationId in (Select LocationID from tmpCTE) --or 
  rtrim(ltrim(a.Charges))  in 
 (select ds249Complaints.Charges from 
 tblDS249Charges ds249Charges,
 tblDS249Complaints ds249Complaints,tblHideCharges hideCharges 
 where ds249Charges.ComplaintId=ds249Complaints.ComplaintId
 and ds249Complaints.ComplaintId=a.ComplaintId
 and hideCharges.EmployeeID=@CreatedBy)
  
   UNION  
   
--***************** Select the complaint Filed at particular Locations
	
	SELECT
		a.[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN tblDS249Charges on a.ComplaintId=tblDS249Charges.ComplaintId
-- INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 where rtrim(ltrim(a.Charges)) not in 
 (select tblDS249Complaints.Charges from 
 tblDS249Charges ,
 tblDS249Complaints ,tblHideCharges  
 where tblDS249Charges.ComplaintId=tblDS249Complaints.ComplaintId
 and tblDS249Complaints.ComplaintId=a.ComplaintId
 and tblHideCharges.ChargeTypeId=tblDS249Charges.ChargeTypeID)
 and tblDS249Charges.IncidentLocationID in (select #TBLTEMPLOCATION.LocationID from #TBLTEMPLOCATION)
 union
 
 SELECT
		a.[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
     
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 inner JOIN tblDS249Charges on a.ComplaintId=tblDS249Charges.ComplaintId
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID   
 where 
  --a.LocationId in (Select LocationID from tmpCTE) --or 
  rtrim(ltrim(a.Charges))  in 
 (select ds249Complaints.Charges from 
 tblDS249Charges ds249Charges,
 tblDS249Complaints ds249Complaints,tblHideCharges hideCharges 
 where ds249Charges.ComplaintId=ds249Complaints.ComplaintId
 and ds249Complaints.ComplaintId=a.ComplaintId
 and hideCharges.EmployeeID=@CreatedBy)
 and tblDS249Charges.IncidentLocationID in (select #TBLTEMPLOCATION.LocationID from #TBLTEMPLOCATION)
  
  union
  
  --***************** Select the complaint Filed differently from the permanent Locations
	
		SELECT
		a.[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
     
 FROM [dbo].[tblDS249Complaints] a 
 inner JOIN #TBLTEMPLOCATION on a.LocationId=#TBLTEMPLOCATION.locationid
-- INNER JOIN tmpCTE d on a.LocationId = d.LocationID
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID  
 where rtrim(ltrim(a.Charges)) not in 
 (select tblDS249Complaints.Charges from 
 tblDS249Charges ,
 tblDS249Complaints ,tblHideCharges  
 where tblDS249Charges.ComplaintId=tblDS249Complaints.ComplaintId
 and tblDS249Complaints.ComplaintId=a.ComplaintId
 and tblHideCharges.ChargeTypeId=tblDS249Charges.ChargeTypeID)
 

 union
 
 SELECT
		a.[ComplaintId]
		,IndexNo
      ,a.[LocationId]
      ,a.[BoroughID]
      ,a.promotiondate
	  ,b.LocationName
	  ,(case when a.complaintStatus >=4 then  a.RoutingLocation
	  else null 
	  end) as 'RoutingLocation'
      ,[RespondentEmpID]
      ,[RespondentRefNumber]      
      ,[Approved]      
      ,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as 'CreatedBy'      
      --,LTRIM(RTRIM(c.LastName)) + ' ,' + LTRIM(RTRIM(c.FirstName)) as 'CreatedBy'
      ,[CreatedDate]
      , '' as 'Status'
     ,charges as 'Charges'
	,Chargesdesc as 'ChargesDesc'
	, RespondentFName
	,RespondentLName
	,A.complaintStatus
      ,A.complaintStatusDate
      ,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
      else 0
      end)
      as 'Indicator',
      a.IsDeleted,
     a.Jt1
     
 FROM [dbo].[tblDS249Complaints] a 
 --INNER JOIN tmpCTE d on a.LocationId = d.LocationID
  inner JOIN #TBLTEMPLOCATION on a.LocationId=#TBLTEMPLOCATION.locationid
 LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
 LEFT OUTER JOIN uv_EmployeePerson c on convert(bigint,a.CreatedBy) = c.EmployeeID   
 where 
  --a.LocationId in (Select LocationID from tmpCTE) --or 
  rtrim(ltrim(a.Charges))  in 
 (select ds249Complaints.Charges from 
 tblDS249Charges ds249Charges,
 tblDS249Complaints ds249Complaints,tblHideCharges hideCharges 
 where ds249Charges.ComplaintId=ds249Complaints.ComplaintId
 and ds249Complaints.ComplaintId=a.ComplaintId
 and hideCharges.EmployeeID=@CreatedBy)
  
  
  
  --Order by ComplaintId desc
   ) tbl 
  
  drop table #TBLTEMPORARY
  Drop Table  #TBLTEMPLOCATION
End  
  
END



Go

ALTER PROCEDURE [dbo].[usp_GetVoilationList]
	-- Add the parameters for the stored procedure here
@ComplaintID bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.[ChargeId]
      ,a.[ComplaintId]
      ,a.ChargeTypeID
      ,b.ReferenceNumber
      ,b.ChargeDesc
      ,[OverRideChargeTypeId]
      ,d.ReferenceNumber as 'OverRideRefNo'
      ,d.ChargeDesc as 'OverRideDesc'
      ,[PrimaryInd]
      ,[IncidentType]
      ,[IncidentLocationID]
      ,c.LocationName
      ,i.incidentdate
  FROM [dbo].[tblDS249Charges] a Left outer JOIN tblChargeType b
  ON a.ChargeTypeID = b.ChargeTypeId LEFT OUTER JOIN tblChargeType d
  ON a.OverRideChargeTypeId = d.ChargeTypeId
  LEFT OUTER JOIN tblLocations c
  On a.IncidentLocationID = c.LocationId
  left outer join tblDS249Incidents i
  on a.ChargeId =i.ChargeID
  WHERE a.ComplaintId = @ComplaintID 
  
  and a.ComplaintId =i.ComplaintId
	
END

Go

CREATE PROCEDURE [dbo].[usp_GetCaseType]
AS
BEGIN
	
	SET NOCOUNT ON;    
    SELECT 'All' as Code_Name
    UNION ALL
    SELECT Code_Name FROM dbo.Code 
    WHERE Code_Category_Id  =13 AND Code_Id IN(96,97)
    UNION ALL
    SELECT Code_Name FROM dbo.Code 
    WHERE Code_Category_Id  =8 AND Code_Id IN(72)
   UNION ALL
    SELECT Code_Name FROM dbo.Code 
    WHERE Code_Category_Id  =5 AND Code_Id IN(46) 
    UNION ALL     
    SELECT 'Exclude OATH' AS Code_Name
    UNION ALL
	SELECT Code_Name  FROM dbo.Code 
    WHERE Code_Category_Id  =7  AND Code_Id IN(54,55,56,57,58,59,60,61,62,63,70,64,66,71,65,167,168)
END


Go

ALTER PROCEDURE [dbo].[usp_GetAdvocateComplaints] 
	-- Add the parameters for the stored procedure here	
@PageNo int = 0,
@maximumRows int = 0
AS
BEGIN

SET NOCOUNT ON;
DECLARE @Count int
Set @Count = (SELECT	COUNT(*) as AdvocateComplaintsCount
 FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id		
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId))

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;


WITH AdvocateComplaints  AS
(

SELECT    
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId		
		,a.ComplaintId
		,a.IndexNo as IndexNo,
		 a.ApprovedDate
		,a.RoutingLocation
		,a.[LocationId]
		,a.Jt1 		
		,i.IncidentDate 		
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		,Charges    
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 

         
  FROM 
        
		 [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) left outer JOIN Code e
		On d.FinalStatusCd = e.Code_Id 
		 
		 
		left outer join tblDS249Incidents i
		
		On a.ComplaintId = i.ComplaintId
		
	
    where (a.ComplaintStatus = 5 or IsSubmitted = '1')
	
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy] t
	
	
	
	where t.ComplaintId=a.ComplaintId	and IsAvailable is null
	
	
	
	order by ComplaintStatusDate Desc	
	
	
	))
	 




select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from AdvocateComplaints where compIndex = 1) tblAdvocate
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;
 
END

Go


ALTER PROCEDURE [dbo].[usp_GetAdvocateComplaintsBySearch] 
	-- Add the parameters for the stored procedure here	
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN

SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
	
if @numericValue != 0  
 Begin
 
 SELECT		
		--ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
		--,c.LastName + ' ,' + c.FirstName as 'CreatedBy'
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId		
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId order by AdvocacyCaseId desc)) and (IndexNo like '%'+Convert(varchar,@numericValue)+ '%' OR RespondentRefNumber like '%'+Convert(varchar,@numericValue) OR AdvocacyCaseId  like '%'+Convert(varchar,@numericValue))  
	order by ComplaintStatusDate Desc	  
select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #AdvocateComplaints1 where compIndex = 1) #AdvocateComplaints1
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;		
	drop table #AdvocateComplaints1	
 End
 else if @chargesValue != '' 
 Begin
 
  SELECT
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IndexNo as IndexNo
		,a.Jt1 
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId			
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId)) and (Charges like '%'+@chargesValue+ '%')
	 ORDER BY ComplaintStatusDate Desc	
	
	select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #AdvocateComplaints2 where compIndex = 1) #AdvocateComplaints2
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;

	drop table #AdvocateComplaints2
 End
 else if @complaintDate != ''
 Begin
   SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IndexNo as IndexNo
		,a.Jt1 
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
          ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId			
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId))  
	ORDER BY ComplaintStatusDate Desc	
 	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,* into #adf0 FROM  #AdvocateComplaints3 where CreatedDate like '%'+Convert(varchar,@complaintDate)+ '%'
 		
 	
 	select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #adf0 where compIndex = 1) #adf0
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc; 	
	drop table #AdvocateComplaints3
	drop table #adf0 
 End
 else if @stringValue != ''
 Begin
 
    SELECT
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId
		,a.IndexNo as IndexNo
		,a.Jt1 
		,a.RoutingLocation
		,a.[LocationId]
		,i.IncidentDate 
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         ,ROW_NUMBER( ) OVER (PARTITION BY i.ComplaintId ORDER BY i.ComplaintId DESC) AS compIndex 
         into #AdvocateComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i		
		On a.ComplaintId = i.ComplaintId
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId))  
	ORDER BY ComplaintStatusDate Desc			
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,* into #adf FROM  #AdvocateComplaints4  where (RespondentFName like '%'+@stringValue+ '%' OR RespondentLName like '%'+@stringValue+ '%' OR CreatedBy like '%'+@stringValue+ '%' OR LocationName like '%'+@stringValue+ '%' OR Status like '%'+@stringValue+ '%' ) 
	
select * from (select *,ROW_NUMBER() OVER (ORDER BY ComplaintStatusDate DESC) AS ROWID1			
from #adf where compIndex = 1) #adf
WHERE ROWID1 BETWEEN @FirstRow AND @LastRow
ORDER BY ComplaintStatusDate desc;
	
	drop table #AdvocateComplaints4
	drop table #adf
	
 End	 	 
 
END

Go

ALTER PROCEDURE [dbo].[usp_GetBCADComplaints] 
@PageNo int = 0,
@maximumRows int = 0
AS
BEGIN
	SET NOCOUNT ON;
	
DECLARE @Count int
Set @Count = (SELECT COUNT(*)
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0)
 
 DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;

 WITH CadComplaints  AS
(
	SELECT
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted 
         
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
		
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0 
   

)
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		CadComplaints  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
		
END

Go

ALTER PROCEDURE [dbo].[usp_GetBCADComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
      
if @numericValue != 0  
  Begin
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted 
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints1
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (IndexNo like '%'+Convert(varchar,@numericValue)+ '%' OR RespondentRefNumber like '%'+Convert(varchar,@numericValue)) 
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints1  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
	
	drop table #CadComplaints1  
	
  End	
 else if @chargesValue != '' 
  Begin
  
  	SELECT		
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
		,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints2
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	    AND (Charges like '%'+@chargesValue+ '%') 
	   
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#CadComplaints  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;	
	
	drop table #CadComplaints2	    
  End
 else if @complaintDate != ''
  Begin
  SELECT
  		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
			,i.IncidentDate
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted  
          ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
         into #CadComplaints3
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	     
 	 SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2, * into #adf0 FROM  #CadComplaints3 where CreatedDate like '%'+Convert(varchar,@complaintDate)+ '%'
		
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf0   where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc; 	 
	
	drop table #CadComplaints3
	drop table #adf0 
  End
 else if @stringValue != ''
  Begin  
    SELECT		
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,RespondentEmpID
		,RespondentRefNumber      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges 				
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator, a.IsDeleted  
         ,ROW_NUMBER() over (PARTITION BY i.complaintid order by i.complaintid) as compIndex        
       ,i.IncidentDate
         into #CadComplaints4
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN tblBCADComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id  
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'B' or a.RoutingLocation = 'M')
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0 ))
	SELECT ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #CadComplaints4  where RespondentFName like '%'+@stringValue+ '%' OR RespondentLName like '%'+@stringValue+ '%' OR CreatedBy like '%'+@stringValue+ '%' OR LocationName like '%'+@stringValue+ '%' OR Status  like '%'+@stringValue+ '%'  
	
		select * from (select *,ROW_NUMBER() over (order by ComplaintStatusDate desc)as ROWID1 from 
		#adf  where compIndex=1)tblBcadComplaints
		WHERE ROWID BETWEEN @FirstRow AND @LastRow
		ORDER BY ComplaintStatusDate desc;
		
	drop table #CadComplaints4
	drop table #adf
  End  
END

Go

ALTER PROCEDURE [dbo].[usp_GetMedicalComplaints] 
@PageNo int = 0,
@maximumRows int = 0
AS
BEGIN
	
	SET NOCOUNT ON;

DECLARE @Count int
Set @Count = (SELECT
		COUNT(*)  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
   where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0 )
	    
	    DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;


WITH MedicalComplaints  AS
(    
	SELECT
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		,Charges    		
		,i.IncidentDate 
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName		
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
		
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND a.IsDeleted =0 	 
  )
  
 
  
   select * from (select *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
   from MedicalComplaints where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
   ORDER BY ComplaintStatusDate Desc;
END

Go

ALTER PROCEDURE [dbo].[usp_GetMedicalComplaintsBySearch] 
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;

DECLARE @FirstRow INT, @LastRow INT
SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;  
  	
if @numericValue != 0  
  Begin
  
  SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges  
		,i.IncidentDate   		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName	
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
	
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         into #MedicalComplaints1  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 		
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (IndexNo like '%'+Convert(varchar,@numericValue)+ '%' OR RespondentRefNumber like '%'+Convert(varchar,@numericValue)) 
	    order by ComplaintStatusDate Desc	       
		SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
		FROM #MedicalComplaints1 WHERE compIndex=1)tblMedComplaints 
		WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	
	drop table #MedicalComplaints1
	  
  End
 else if @chargesValue != '' 
  Begin
    SELECT	
		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges   
		,i.IncidentDate  		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         into #MedicalComplaints2  
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0)) AND (Charges like '%'+@chargesValue+ '%')
	 
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
	FROM #MedicalComplaints2 where compIndex=1)tblMedComplaints 
	WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints2
	
  End
 else if @complaintDate != ''
  Begin
     SELECT	
   		ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,			
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
		,i.IncidentDate 		
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         into #MedicalComplaints3 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where ((a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0))
 
 	SELECT  ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID2, * into #adf0  FROM  #MedicalComplaints3 where CreatedDate like '%'+Convert(varchar,@complaintDate)+ '%'
 	select * from (select *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) as ROWID1   
   from  #adf0 where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints3  
	drop table #adf0     
	 
  End
 else if @stringValue != ''
  Begin
      SELECT	
       ROW_NUMBER() OVER (ORDER BY a.ComplaintStatusDate DESC) AS ROWID,		
		a.ComplaintId,
		a.indexNo,
		a.[LocationId]
		,a.RoutingLocation
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges   
		,i.IncidentDate 	
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,CONVERT(varchar,a.CreatedDate,101) as CreatedDate
		,a.RespondentFName
		,a.RespondentLName
		,ROW_NUMBER() OVER(PARTITION BY  a.ComplaintId ORDER BY a.ComplaintId DESC) as compIndex
		,Status = (case when d.ComplaintId is null and a.RoutingLocation = 'B' then 'UnRead'
						 when d.ComplaintId is null and a.RoutingLocation = 'M' then 'UnRead'
                         when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         when a.RoutingLocation = 'A' then 'Advocate' end),
         '' as Indicator
         into #MedicalComplaints4 
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID LEFT OUTER JOIN TBLMedicalComplaint d
		on (a.ComplaintId = d.ComplaintID AND d.IsAvailable is null) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		left outer join tblDS249Incidents i
		on a.ComplaintId =i.ComplaintId 
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and a.RoutingLocation = 'M'
		AND (d.IsAvailable is null)  
	    AND (a.IsDeleted =0) ORDER BY ComplaintId desc
	SELECT  ROW_NUMBER() OVER (ORDER BY ComplaintId DESC) AS ROWID2,	* into #adf FROM  #MedicalComplaints4  where RespondentFName like '%'+@stringValue+ '%' OR RespondentLName like '%'+@stringValue+ '%' OR CreatedBy like '%'+@stringValue+ '%' OR LocationName like '%'+@stringValue+ '%' OR Status  like '%'+@stringValue+ '%'
	SELECT * FROM (SELECT *,ROW_NUMBER() OVER(ORDER BY ComplaintStatusDate DESC) AS ROWID1   
   FROM #adf where compIndex=1)tblMedComplaints 
   WHERE ROWID BETWEEN @FirstRow AND @LastRow 
	drop table #MedicalComplaints4
	drop table #adf
  End 	 	
   
     
END




 update dbo.code set code_name='VOID' WHERE Code_Category_ID =5 AND Code_ID=41
 update dbo.code set code_name='INF CON' WHERE Code_Category_ID =5 AND Code_ID=40
 update dbo.code set code_name='CALENDAR' WHERE Code_Category_ID =5 AND Code_ID=42
 update dbo.code SET is_active =NULL WHERE Code_Category_Id =5 AND Code_Id IN(43,44,45)



update dbo.code set code_name='AWAIT INV' WHERE Code_Category_ID =4 AND Code_ID=30
update dbo.code set code_name='AWAIT DOC' WHERE Code_Category_ID =4 AND Code_ID=31

update dbo.code set code_name='PROBATIONARY' WHERE Code_Category_ID =4 AND Code_ID=36

update dbo.code SET is_active =NULL WHERE Code_Category_Id =4 AND Code_Id IN(32,33,34,35,38,144)


update dbo.code SET is_active =NULL WHERE Code_Category_Id =3 AND Code_Id IN(23,24,25,26,27,28,29)


update dbo.code set code_name='RET VOID' WHERE Code_Category_ID =8 AND Code_ID=73

INSERT INTO dbo.Code (CODE_ID,Code_Category_Id,Code_Name,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)
  SELECT MAX([Code_Id]) + 1 CODE_ID,8 Code_Category_Id,
  'Adjourned to OATH' Code_Name,(select max([Item_Order]) + 1 from code where Code_Category_Id=8) Item_Order  ,NULL Value_Data_Type,'Y' is_active,NULL Created_User,GETDATE()  Created_Date FROM Code
  
update dbo.code set is_active =NULL WHERE Code_Category_ID =8 AND Code_ID=74


update dbo.code set code_name='OATH SCH' WHERE Code_Category_ID =6 AND Code_ID=47
update dbo.code set code_name='SCHED' WHERE Code_Category_ID =6 AND Code_ID=50

update dbo.code set code_name='RESCHED' WHERE Code_Category_ID =6 AND Code_ID=51

update dbo.code SET is_active =NULL WHERE Code_Category_Id =6 AND Code_Id IN(48,49,52)

 INSERT INTO dbo.Code (CODE_ID,Code_Category_Id,Code_Name,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)
  SELECT MAX([Code_Id]) + 1 CODE_ID,10 Code_Category_Id,
  'PLEA REJECTION' Code_Name,(select max([Item_Order]) + 1 from code where Code_Category_Id=10) Item_Order ,NULL Value_Data_Type,'Y' is_active,NULL Created_User,GETDATE()  Created_Date FROM Code
  

update dbo.code SET is_active =NULL WHERE Code_Category_Id =10 AND Code_Id IN(
85,146,147,148,149,153,154,155,156,157,158,159,161,162)


update dbo.code set code_name='OATH DECISSION RESERVED' WHERE Code_Category_ID =7 AND Code_ID=54

update dbo.code set code_name='H/HELD DEC RESERVED' WHERE Code_Category_ID =7 AND Code_ID=55

update dbo.code set code_name='H/HELD - CONT''D' WHERE Code_Category_ID =7 AND Code_ID=56

update dbo.code set code_name='H/HELD - FOUND GUILTY' WHERE Code_Category_ID =7 AND Code_ID=57

update dbo.code set code_name='MOTION TO DISMISS' WHERE Code_Category_ID =7 AND Code_ID=62

update dbo.code set code_name='EAU - LONG TERM' WHERE Code_Category_ID =7 AND Code_ID=70

update dbo.code set code_name='PLEAD GUILTY' WHERE Code_Category_ID =7 AND Code_ID=64

update dbo.code set code_name='CONF. - REQUEST TRIAL' WHERE Code_Category_ID =7 AND Code_ID=65

update dbo.code set code_name='VOIDED' WHERE Code_Category_ID =7 AND Code_ID=66

update dbo.code set code_name='TERM W/CHARGES PEND' WHERE Code_Category_ID =7 AND Code_ID=71


update dbo.code SET is_active =NULL WHERE Code_Category_Id =7 AND Code_Id IN(
53,67,68,69)


 INSERT INTO dbo.Code (CODE_ID,Code_Category_Id,Code_Name,Item_Order,is_active,Created_User,Created_Date)
  SELECT MAX([Code_Id]) + 1 CODE_ID,7 Code_Category_Id,
  'RESIGNED W/CHARGES PEND' Code_Name,(select max([Item_Order]) + 1 from code where Code_Category_Id=7) Item_Order,'Y' is_active,NULL Created_User,NULL  Created_Date FROM Code
  
 
  INSERT INTO dbo.Code (CODE_ID,Code_Category_Id,Code_Name,Item_Order,is_active,Created_User,Created_Date)
  SELECT MAX([Code_Id]) + 1 CODE_ID,7 Code_Category_Id,
  'RETIRED W/CHARGES PEND' Code_Name,(select max([Item_Order]) + 1 from code where Code_Category_Id=7) Item_Order ,'Y' is_active,NULL Created_User,NULL  Created_Date FROM Code
  



update dbo.code set code_name='CS COMM' WHERE Code_Category_ID =11 AND Code_ID=87

update dbo.code set code_name='SUP CT' WHERE Code_Category_ID =11 AND Code_ID=88

update dbo.code set code_name='APP DIV' WHERE Code_Category_ID =11 AND Code_ID=89

update dbo.code set code_name='CT APPEAL' WHERE Code_Category_ID =11 AND Code_ID=90





update dbo.code set code_name='AFFIRM' WHERE Code_Category_ID =12 AND Code_ID=91

update dbo.code set code_name='MODIFY' WHERE Code_Category_ID =12 AND Code_ID=92

update dbo.code set code_name='REVERSE' WHERE Code_Category_ID =12 AND Code_ID=93

update dbo.code set code_name='DISMISS' WHERE Code_Category_ID =12 AND Code_ID=94

update dbo.code set code_name='REMAND' WHERE Code_Category_ID =12 AND Code_ID=95



update dbo.code set code_name='ACCIDENT',is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=119

update dbo.code set code_name='ORDINARY' ,is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=120

update dbo.code set code_name='RETIRED UNSPECIFIED' ,is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=121

update dbo.code set code_name='SERVICE' ,is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=122

update dbo.code set code_name='RESIGNED' ,is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=123

update dbo.code set code_name='DECEASED' ,is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=124



update dbo.code set code_name='DISABILITY - TIER 3 OR 4' ,is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=125

update dbo.code set code_name='MEDICAL SEPARATION' ,is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=126

update dbo.code set code_name='UNKNOWN' ,is_active ='Y' WHERE Code_Category_ID =17 AND Code_ID=129

 update Code set is_active='Y' where Code_Category_Id =20 and Code_Id in(139,140,141,142,143)

 update Code set is_active='Y' where Code_Category_Id =18 and Code_Id in(133,134)



INSERT INTO dbo.Code (CODE_ID,Code_Category_Id,Code_Name,Item_Order,Created_User,Created_Date,is_active)
  SELECT MAX([Code_Id]) + 1 CODE_ID,17 Code_Category_Id,
  'RETIRED W/CHARGES PEND' Code_Name,NULL Item_Order ,NULL Created_User,NULL  Created_Date,'Y' FROM Code
  

 INSERT INTO dbo.Code (CODE_ID,Code_Category_Id,Code_Name,Item_Order,Created_User,Created_Date,is_active)
  SELECT MAX([Code_Id]) + 1 CODE_ID,17 Code_Category_Id,
  'RESIGNED W/CHARGES PEND' Code_Name,NULL Item_Order ,NULL Created_User,NULL  Created_Date,'Y' FROM Code
 
 INSERT INTO dbo.Code (CODE_ID,Code_Category_Id,Code_Name,Item_Order,Created_User,Created_Date,is_active)
  SELECT MAX([Code_Id]) + 1 CODE_ID,17 Code_Category_Id,
  'TERMINATED W/CHARGES PEND' Code_Name,NULL Item_Order ,NULL Created_User,NULL  Created_Date,'Y' FROM Code
	 Go

 CREATE PROCEDURE [dbo].[usp_RptGetAdvocateCases_Boros]  
	@Attorney VARCHAR(50),
	@CaseType VARCHAR(15),
	@BeginDate VARCHAR(50),
	@EndDate VARCHAR(50)	
AS
SET NOCOUNT ON;  
DECLARE @strSQL VARCHAR(1000)=''
DECLARE @Code_Id int
DECLARE @Code_Category_Id varchar(30)
BEGIN
			
		IF(@CaseType <>'Charges')
				BEGIN
					SET @strSQL='SELECT COUNT(*) AS Count ,l.LocationName as BoroName,l.LocationId FROM 
					tblDs249Complaints co LEFT OUTER JOIN tblComplaintAdvocacy ca ON
					co.ComplaintId=ca.ComplaintId LEFT OUTER JOIN tblLocations l
					ON co.BoroughID =l.Locationid WHERE'					
				END
			ELSE
				BEGIN					
					SET @strSQL='SELECT COUNT(*) AS Count ,l.LocationName as BoroName ,l.LocationId FROM tblDS249Charges ch
								LEFT OUTER JOIN tblDs249Complaints co ON
								ch.ComplaintId=co.ComplaintId LEFT OUTER JOIN 
							    tblComplaintAdvocacy ca on ch.ComplaintId=ca.ComplaintId 
							    LEFT OUTER JOIN tblLocations l ON co.BoroughID =l.Locationid
							    WHERE '
				END													
		IF(@Attorney<>'ALL') 		
				BEGIN
					SET @strSQL=@strSQL + ' RecieveUser='				
					SET @STRSQL=@STRSQL+'(SELECT PersonId from tblPersons WHERE '
					SET @STRSQL=@STRSQL+' FirstName=SUBSTRING('''+ @Attorney +''', 1, CHARINDEX('' '', '''+ @Attorney +''') - 1)' 
					SET @STRSQL=@STRSQL+'and LastName=SUBSTRING('''+ @Attorney + ''', CHARINDEX('' '', '''+ @Attorney +''') + 1, LEN('''+ @Attorney +'''))) AND '								
				END																						
		IF (@BeginDate IS NOT NULL AND  @EndDate IS NOT NULL)   
				BEGIN
					 SET @strSQL=@strSQL + '  RecieveDate  BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''						
				END
		IF(@CaseType ='Open')
			BEGIN
				SET @strSQL=@strSQL +' AND FinalStatusCd=96 AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL GROUP BY co.BoroughID,l.Locationname,l.LocationId'
			END
		ELSE IF(@CaseType ='Closed')
			BEGIN
				 SET @strSQL=@strSQL +' AND FinalStatusCd=97 AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL GROUP BY co.BoroughID,l.Locationname,l.LocationId'
			END								
		ELSE
		BEGIN
		 SET @strSQL=@strSQL +' AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL GROUP BY co.BoroughID,l.locationname,l.LocationId'
		END									
	   EXECUTE (@strSQL)    	
END

Go

CREATE PROCEDURE [dbo].[usp_RptGetAdvocateCases_Districts]  
	@Attorney VARCHAR(50),
	@CaseType VARCHAR(15),
	@BoroName VARCHAR(100),
	@BeginDate VARCHAR(50),
	@EndDate VARCHAR(50)	
AS
SET NOCOUNT ON;  
DECLARE @strSQL VARCHAR(1000)=''
DECLARE @Code_Id int
DECLARE @Code_Category_Id varchar(30)
BEGIN
		IF(@CaseType <>'Charges')
				BEGIN
					SET @strSQL='SELECT COUNT(*) AS Count ,l.LocationName as DistrictName ,l.Locationid FROM 
					tblDs249Complaints co LEFT OUTER JOIN tblComplaintAdvocacy ca ON
					co.ComplaintId=ca.ComplaintId LEFT OUTER JOIN tblLocations l
					ON co.locationid =l.Locationid WHERE'					
				END
			ELSE
				BEGIN					
					SET @strSQL='SELECT COUNT(*) AS Count ,l.LocationName as DistrictName,l.Locationid FROM tblDS249Charges ch
								LEFT OUTER JOIN tblDs249Complaints co ON
								ch.ComplaintId=co.ComplaintId LEFT OUTER JOIN 
							    tblComplaintAdvocacy ca on ch.ComplaintId=ca.ComplaintId 
							    LEFT OUTER JOIN tblLocations l ON co.locationid =l.Locationid
							    WHERE '
				END													
		IF(@Attorney<>'ALL') 		
				BEGIN
					SET @strSQL=@strSQL + ' RecieveUser='				
					SET @STRSQL=@STRSQL+'(SELECT PersonId from tblPersons WHERE '
					SET @STRSQL=@STRSQL+' FirstName=SUBSTRING('''+ @Attorney +''', 1, CHARINDEX('' '', '''+ @Attorney +''') - 1)' 
					SET @STRSQL=@STRSQL+'and LastName=SUBSTRING('''+ @Attorney + ''', CHARINDEX('' '', '''+ @Attorney +''') + 1, LEN('''+ @Attorney +'''))) AND '								
				END																						
		IF (@BeginDate IS NOT NULL AND  @EndDate IS NOT NULL)   
				BEGIN
					 SET @strSQL=@strSQL + '  RecieveDate  BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''						
				END
		SET @strSQL=@strSQL + ' AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   												
		IF(@CaseType ='Open')
			BEGIN
				SET @strSQL=@strSQL +' AND FinalStatusCd=96 AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL GROUP BY co.BoroughID,l.locationname,l.Locationid'			
			END
		ELSE IF(@CaseType ='Closed')
			BEGIN
				 SET @strSQL=@strSQL +' AND FinalStatusCd=97 AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL GROUP BY co.BoroughID,l.locationname,l.Locationid'
			END								
		ELSE
		BEGIN
		 SET @strSQL=@strSQL +' AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL GROUP BY co.BoroughID,l.locationname,l.Locationid'
		END									
	   EXECUTE (@strSQL)    	
END

Go

CREATE PROCEDURE [dbo].[usp_RptGetAdvocateCases_Details]  
	@Attorney VARCHAR(50),
	@CaseType VARCHAR(15),
	@BoroName VARCHAR(100),
	@DistrictName VARCHAR(100),
	@BeginDate VARCHAR(50),	
	@EndDate VARCHAR(50)	
AS

SET NOCOUNT ON;  
DECLARE @strSQL VARCHAR(1000)=''
DECLARE @Code_Id INT
DECLARE @Code_Category_Id VARCHAR(30)

BEGIN
		SET @strSQL='SELECT l.LocationName,co.RespondentFName,co.RespondentLName,
					co.Indexno,co.Charges,co.ChargesDesc,co.CreatedDate,    
					Status = (CASE WHEN ca.FinalStatuscd = ''96'' THEN  
					''Open'' WHEN ca.FinalStatuscd=''97'' THEN ''Closed'' ELSE '''' END)				
					,ca.RecieveDate FROM tblDs249Complaints co 					
					right OUTER JOIN tblComplaintAdvocacy ca ON
					co.ComplaintId=ca.ComplaintId left OUTER JOIN tblLocations l
					ON co.LocationId =l.LocationId WHERE'
																	
		IF(@Attorney<>'ALL') 		
				BEGIN
					SET @strSQL=@strSQL + ' RecieveUser='				
					SET @STRSQL=@STRSQL+'(SELECT PersonId FROM tblPersons WHERE '
					SET @STRSQL=@STRSQL+' FirstName=SUBSTRING('''+ @Attorney +''', 1, CHARINDEX('' '', '''+ @Attorney +''') - 1)' 
					SET @STRSQL=@STRSQL+' AND LastName=SUBSTRING('''+ @Attorney + ''', CHARINDEX('' '', '''+ @Attorney +''') + 1, LEN('''+ @Attorney +'''))) AND '								
				END																						
		IF (@BeginDate IS NOT NULL AND  @EndDate IS NOT NULL)   
				BEGIN
					 SET @strSQL=@strSQL + '  RecieveDate  BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''						
				END
		SET @strSQL=@strSQL + ' AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   												
		IF(@CaseType ='Open')
			BEGIN
				SET @strSQL=@strSQL +' AND FinalStatusCd=96 AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL '			
			END
		ELSE IF(@CaseType ='Closed')
			BEGIN
				 SET @strSQL=@strSQL +' AND FinalStatusCd=97 AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL '
			END								
		ELSE
		BEGIN
		 SET @strSQL=@strSQL +' AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL '
		END				
		SET @strSQL=@strSQL + ' AND l.LocationName = '+ '''' + @DistrictName +''''	
	   EXECUTE (@strSQL)    	
END

Go
    -----------------------------------------
	--------------------------------------------
	------Added Go Statement-------------
CREATE PROCEDURE [dbo].[usp_RptGetComplaintsCharges_BOROS] 
	@Rtype VARCHAR(15),
	@ReportLevel VARCHAR(15),
	@BeginDate VARCHAR(50),
	@EndDate VARCHAR(50),
	@CaseType VARCHAR(50)
	
AS
DECLARE @strSQL VARCHAR(1000)=''
BEGIN
SET NOCOUNT ON;
DECLARE @Code_Id int
DECLARE @Code_Category_Id VARCHAR(30)
SET @Code_Id=(SELECT Code_Id FROM dbo.code WHERE Code_Name = ''+@CaseType+'' and Code_Category_Id in (13,5,7,8))	
SET @Code_Category_Id=(SELECT Code_Category_Id FROM dbo.code WHERE Code_Name=''+@CaseType+'' AND Code_Id =@Code_Id)		
	IF (@Rtype='COMPLAINTS') AND (@REPORTLEVEL='BOROS') 	
	BEGIN	 										
			SET @strSQL='SELECT COUNT(*) AS Count,l.LocationName,L.LocationId 
						FROM tblDS249Complaints co						     
						LEFT OUTER JOIN  tblComplaintAdvocacy  ca
						ON co.ComplaintId=ca.ComplaintId LEFT OUTER JOIN tblAdvocateHearing ah  
						ON ah.AdvocacyCaseId =ca.AdvocacyCaseID LEFT OUTER JOIN tblLocations l
						ON co.BoroughID =l.LocationId WHERE '									
			IF (@Code_Category_Id=7)
				BEGIN
					SET @strSQL=@strSQL + '[HearingStatusCd]='+ convert(varchar,@Code_Id)	
					SET @strSQL=@strSQL + '  AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL' 		
				END				
				IF (@Code_Category_Id=5)	
				BEGIN
					SET @strSQL=@strSQL + '[Actioncd]=' +CONVERT(VARCHAR,@Code_Id)	
					SET @strSQL=@strSQL + '  AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL' 							
				END										
				IF (@Code_Category_Id=13)	
				BEGIN
					SET @strSQL=@strSQL + '[FinalStatuscd]=' +CONVERT(VARCHAR,@Code_Id)	
					SET @strSQL=@strSQL + '  AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL' 
				END				
				IF (@Code_Category_Id=8)	
				BEGIN
					SET @strSQL=@strSQL + '[TrialReturnStatuscd]=' +CONVERT(VARCHAR,@Code_Id)	
					SET @strSQL=@strSQL + '  AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL' 
				END					
				IF (@CaseType='All')
				BEGIN				
					SET @strSQL=@strSQL + ' [Actioncd]<>46'
					SET @strSQL=@strSQL + ' AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL'								
				END				
				IF (@CaseType='Exclude OATH')
				BEGIN		    
						SET @strSQL=@strSQL + ' [Actioncd]<>46'
					    SET @strSQL=@strSQL + '  AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL' 
				END											
			IF (@BeginDate IS NOT NULL AND @EndDate IS NOT NULL)   
					BEGIN
						 SET @strSQL=@strSQL + ' AND  '						 
						 IF (@Code_Category_Id=7)
						 begin
								SET @strSQL=@strSQL + ' ah.ModifiedDt BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''				
						end
						 ELSE
							SET @strSQL=@strSQL + ' ca.ModifiedDate BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''				
					END
					SET @strSQL=@strSQL + ' GROUP BY co.BoroughID,l.locationname,l.LocationId'				
			 
			EXECUTE  (@strSQL) 	    
	END	      
	  
    IF (@Rtype='CHARGES') AND (@REPORTLEVEL='BOROS')            
    BEGIN   		   
		   SET @strSQL='SELECT COUNT(*) AS Count,l.LocationName,L.LocationId
						FROM  tblDS249Charges  ch LEFT OUTER JOIN tblDS249Complaints co
						ON  ch.ComplaintId =co.ComplaintId LEFT OUTER JOIN tblComplaintAdvocacy ca
						ON  ch.ComplaintId =ca.ComplaintId LEFT OUTER JOIN tblAdvocateHearing ah ON ah.AdvocacyCaseId =ca.AdvocacyCaseID 														
						LEFT OUTER JOIN tblLocations l ON co.BoroughID =l.Locationid WHERE '
		    IF (@Code_Category_Id=7)
				BEGIN						    
					SET @strSQL=@strSQL + '[HearingStatusCd]='+ CONVERT(VARCHAR,@Code_Id)			
					SET @strSQL=@strSQL + ' AND  co.BoroughID =l.Locationid  AND co.ComplaintId=ch.ComplaintId AND ca.FinalStatuscd =96 AND l.LocationName IS NOT NULL '			
				END				
			IF (@Code_Category_Id=5)
				BEGIN						    
					SET @strSQL=@strSQL + '[Actioncd]='+ CONVERT(VARCHAR,@Code_Id)			
					SET @strSQL=@strSQL + ' AND  co.BoroughID =l.Locationid  AND co.ComplaintId=ch.ComplaintId AND ca.FinalStatuscd =96 AND l.LocationName IS NOT NULL AND co.BoroughID IS NOT NULL'						
				END
			IF(@Code_Category_Id=13)
					BEGIN			    
							SET @strSQL=@strSQL + 'ca.FinalStatuscd ='+CONVERT(VARCHAR,@Code_Id)
							SET @strSQL=@strSQL + ' AND l.LocationName IS NOT NULL  AND co.BoroughID IS NOT NULL'	    										
					END
			IF(@Code_Category_Id=8)
				BEGIN			    
						SET @strSQL=@strSQL + 'ca.TrialReturnStatuscd ='+CONVERT(VARCHAR,@Code_Id)
						SET @strSQL=@strSQL + ' AND l.LocationName IS NOT NULL  AND co.BoroughID IS NOT NULL'	    										
				END					
			IF(@CaseType='All')
			BEGIN		
					SET @strSQL=@strSQL + ' [Actioncd]<>46'
					SET @strSQL=@strSQL + ' AND l.LocationName IS NOT NULL AND co.BoroughID IS NOT NULL '	    					
			END
			IF (@CaseType='Exclude OATH')
				BEGIN		    
						SET @strSQL=@strSQL + ' [Actioncd]<>46'
						SET @strSQL=@strSQL + '  AND co.BoroughID IS NOT NULL AND l.LocationName IS NOT NULL' 
				END
			IF (@BeginDate IS NOT NULL AND  @EndDate IS NOT NULL)   
				BEGIN
					 SET @strSQL=@strSQL + ' AND  '
					 IF (@Code_Category_Id=7)
						SET @strSQL=@strSQL + ' ah.ModifiedDt BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''				
					 ELSE			
						SET @strSQL=@strSQL + ' ca.ModifiedDate BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''				
					 
				END
				SET @strSQL=@strSQL + ' GROUP BY co.BoroughID,l.locationname,l.LocationId'					
				EXECUTE  (@strSQL)
   END 											   
 END 
    Go
CREATE PROCEDURE [dbo].[usp_RptGetComplaintsCharges_Districts] 
	@Rtype VARCHAR(15),
	@ReportLevel VARCHAR(15),
	@BoroName VARCHAR(100),
	@BeginDate VARCHAR(50),
	@EndDate VARCHAR(50),
	@CaseType VARCHAR(50)
AS
DECLARE @strSQL VARCHAR(1000)=''	
BEGIN
SET NOCOUNT ON;   
DECLARE @Code_Id INT
DECLARE @Code_Category_Id VARCHAR(30)
SET @Code_Id=(SELECT Code_Id FROM dbo.code WHERE Code_Name=''+@CaseType+'' and Code_Category_Id IN (13,5,7,8))	
SET @Code_Category_Id=(SELECT Code_Category_Id FROM dbo.code WHERE Code_Name=''+@CaseType+'' AND Code_Id =@Code_Id)		
    IF (UPPER(@Rtype)='COMPLAINTS') 
    BEGIN			
			SET @strSQL='SELECT l.LocationName AS DistrictName,l.LocationId'					  
			IF (@BoroName<>'')
			BEGIN
					SET @strSQL=@strSQL+ ','+''''+ @BoroName + ''' AS BoroName'	
			END
				
							      
			SET @strSQL=@strSQL+',COUNT(*) AS Count FROM tblDS249Complaints co LEFT OUTER JOIN  tblComplaintAdvocacy  ca
								ON Co.ComplaintId=ca.ComplaintId LEFT OUTER JOIN tblAdvocateHearing ah  
								ON ah.AdvocacyCaseId =ca.AdvocacyCaseID LEFT OUTER JOIN tblLocations l ON co.LocationId =l.Locationid WHERE '
			IF (@Code_Category_Id=7) 			
			BEGIN 
					SET @strSQL=@strSQL + '[HearingStatusCd]='+ CONVERT(VARCHAR,@Code_Id)
					IF (@REPORTLEVEL='BOROS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   												
						END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
					BEGIN						
						SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'	
					END
			END				
			IF (@Code_Category_Id=5) 
			BEGIN
				SET @strSQL=@strSQL + ' [Actioncd]='+CONVERT(VARCHAR,@Code_Id)  
				IF (@REPORTLEVEL='BOROS')
				BEGIN				     
						SET @strSQL=@strSQL + '  AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   						
				END
				ELSE IF(@REPORTLEVEL='DISTRICTS')
					BEGIN
						SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
					END				
			END			
			IF (@Code_Category_Id=13) 
				BEGIN
					SET @strSQL=@strSQL + '[FinalStatuscd]='+CONVERT(VARCHAR,@Code_Id)
					IF (@REPORTLEVEL='BOROS')
						BEGIN    
							SET @strSQL=@strSQL + '  AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   						
						END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID '						
						END			
				END					
				IF (@Code_Category_Id=8) 
				BEGIN
					SET @strSQL=@strSQL + ' [TrialReturnStatuscd]='+CONVERT(VARCHAR,@Code_Id)
					IF (@REPORTLEVEL='BOROS')
						BEGIN    
							SET @strSQL=@strSQL + '  AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   						
						END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID '						
						END			
				END					
								
			IF (@CaseType ='All') 			
			BEGIN
					SET @strSQL=@strSQL + '[Actioncd]<>46'
				IF (@REPORTLEVEL='BOROS')
					BEGIN
						SET @strSQL=@strSQL + '  AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   			
					END	
				ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
						END					
			END				
			IF (@CaseType='Exclude OATH')  
			BEGIN 
					SET @strSQL=@strSQL + '[Actioncd]<>46'
					IF (@REPORTLEVEL='BOROS')
						BEGIN    
							SET @strSQL=@strSQL + '  AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   						
						END
						
						ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
						END	
			END							
					
			IF (@BeginDate IS NOT NULL AND  @EndDate IS NOT NULL)   
						BEGIN
							 SET @strSQL=@strSQL + ' AND  '
							 IF (@Code_Category_Id=7) 
								 SET @strSQL=@strSQL + ' ah.ModifiedDt  BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''						
							 ELSE
 								 SET @strSQL=@strSQL + ' ca.ModifiedDate  BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''						
						END

				IF (@REPORTLEVEL='BOROS')
						BEGIN    
							SET @strSQL=@strSQL + ' AND co.BoroughID IS NOT NULL GROUP BY co.BoroughID,l.LocationName,ca.FinalStatuscd,l.LocationId  '		   						
						END
						
						ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' GROUP BY l.LocationName,ca.FinalStatuscd,l.LocationId'						
						END								
			
		   EXECUTE  (@strSQL)    			
		END 		    

		IF (@Rtype='CHARGES') 
			BEGIN
				SET @strSQL='SELECT l.LocationName AS DistrictName,l.LocationId' 				  
			IF (@BoroName<>'')
				BEGIN
						SET @strSQL=@strSQL+ ','+''''+ @BoroName + ''' AS BoroName'	
				END
					
					
					SET @strSQL=@strSQL+',COUNT(*) AS Count FROM tblDS249Charges ch LEFT OUTER JOIN  tblComplaintAdvocacy  ca ON Ch.ComplaintId=ca.ComplaintId
										LEFT OUTER JOIN tblDS249Complaints  co ON Co.ComplaintId=ca.ComplaintId   
										LEFT OUTER JOIN tblAdvocateHearing ah ON ah.AdvocacyCaseId =ca.AdvocacyCaseID 
										LEFT OUTER JOIN tblLocations l ON co.LocationId =l.Locationid  WHERE '    						
				IF (@Code_Category_Id=7)
				 BEGIN
					SET @strSQL=@strSQL + '[HearingStatusCd]='+ CONVERT(VARCHAR,@Code_Id)			
					IF (@REPORTLEVEL='BOROS')
						BEGIN						
							SET @strSQL=@strSQL + ' AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'         
						END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
						END															
				END	    					
				IF (@Code_Category_Id=13)
				BEGIN
					SET @strSQL=@strSQL + '[FinalStatuscd]='+CONVERT(VARCHAR,@Code_Id)
					IF (@REPORTLEVEL='BOROS')
					BEGIN
						SET @strSQL=@strSQL + ' AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'         				
					END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
						END	    						
				END 				
				
				IF (@Code_Category_Id=8)
				BEGIN
					SET @strSQL=@strSQL + ' [TrialReturnStatuscd]='+CONVERT(VARCHAR,@Code_Id)
					IF (@REPORTLEVEL='BOROS')
					BEGIN
						SET @strSQL=@strSQL + ' AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'         				
					END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
						END	    						
				END  			
									
				IF (@Code_Category_Id=5)
				BEGIN
				IF (@REPORTLEVEL='BOROS')
					BEGIN
						SET @strSQL=@strSQL + '[Actioncd]='+CONVERT(VARCHAR,@Code_Id)
						SET @strSQL=@strSQL + ' AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'         				
					END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
						END	  							
				END						
				IF (@CaseType='All')	
				BEGIN
					SET @strSQL=@strSQL + '[Actioncd]<>46'							
				IF (@REPORTLEVEL='BOROS')
					BEGIN
						SET @strSQL=@strSQL + ' AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'         								
					END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
						END	  	
				END						
				IF (@CaseType='Exclude OATH')        
					BEGIN  
						SET @strSQL=@strSQL + '[Actioncd]<>46'
					IF (@REPORTLEVEL='BOROS') 
						BEGIN 
							SET @strSQL=@strSQL + '  AND co.BoroughID = (SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+ @BoroName +''') '		   						
						END
					ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' AND co.LOCATIONID=L.LOCATIONID'						
						END	  	
					END
					
			   IF (@BeginDate IS NOT NULL AND  @EndDate IS NOT NULL)   
						BEGIN
							 SET @strSQL=@strSQL + ' AND  '
							 IF (@Code_Category_Id=7) 
								 SET @strSQL=@strSQL + ' ah.ModifiedDt  BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''						
							 ELSE
 								 SET @strSQL=@strSQL + ' ca.ModifiedDate  BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''						
						END
			 IF (@REPORTLEVEL='BOROS')
						BEGIN    
							SET @strSQL=@strSQL + ' AND co.BoroughID IS NOT NULL GROUP BY co.BoroughID,l.LocationName,ca.FinalStatuscd,l.LocationId  '		   						
						END
						
						ELSE IF(@REPORTLEVEL='DISTRICTS')
						BEGIN
							SET @strSQL=@strSQL + ' GROUP BY l.LocationName,ca.FinalStatuscd,l.LocationId'						
						END		
					 EXECUTE  (@strSQL)	
				 END  
END



Go

CREATE PROCEDURE [dbo].[usp_RptGetComplaintsCharges_Individuals]  
@Rtype VARCHAR(15),
	@ReportLevel VARCHAR(15),
	@BoroName VARCHAR(100),
	@DistrictName VARCHAR(100),
	@BeginDate VARCHAR(50),
	@EndDate VARCHAR(50),
	@CaseType VARCHAR(50),
	@LocationId int
AS
SET NOCOUNT ON;  
DECLARE @strSQL VARCHAR(1000)=''
DECLARE @Code_Id int
DECLARE @Code_Category_Id varchar(30)
BEGIN
SET @Code_Id=(SELECT Code_Id FROM dbo.code WHERE Code_Name=''+@CaseType+'' AND Code_Category_Id IN (13,5,7,8))	
SET @Code_Category_Id=(SELECT Code_Category_Id FROM dbo.code WHERE Code_Name=''+@CaseType+'' AND Code_Id =@Code_Id)		
				SET @strSQL='SELECT l.LocationName,l.locationId,c.RespondentFName,c.RespondentLName,c.Indexno,c.Charges,c.ChargesDesc,c.CreatedDate,    
							Status = (CASE WHEN ca.FinalStatuscd = ''96'' THEN  ''Open'' WHEN ca.FinalStatuscd=''97'' THEN ''Closed'' ELSE '''' END)
							FROM tblDS249Complaints c LEFT OUTER JOIN  tblComplaintAdvocacy  ca
							ON C.ComplaintId=ca.ComplaintId	LEFT OUTER JOIN tblAdvocateHearing ah  
							ON ah.AdvocacyCaseId =ca.AdvocacyCaseID LEFT OUTER JOIN tblLocations l ON c.LocationId =l.Locationid WHERE '  		    							
				IF (@Code_Category_Id=7)
					BEGIN
						SET @strSQL=@strSQL + '[HearingStatusCd]='+ CONVERT(VARCHAR,@Code_Id)
						IF (@REPORTLEVEL='BOROS') 	
							BEGIN 
  							 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
								SET @strSQL=@strSQL + ' AND c.BoroughID =(SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'					
							END
							
							IF  (@REPORTLEVEL='DISTRICTS')	
								BEGIN
 
							   		 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
									 SET @strSQL=@strSQL + ' AND L.LocationName='''+@DistrictName+''''
							END
								
							ELSE IF(@REPORTLEVEL='INDIVIDUALS')
								BEGIN
									SET @strSQL=@strSQL + ' AND c.BoroughID IS NOT NULL AND C.LocationId IS NOT NULL'										
								END						
					END	   					
					
				IF (@Code_Category_Id=5)
					BEGIN
						SET @strSQL=@strSQL + ' [Actioncd]='+CONVERT(VARCHAR,@Code_Id)
					IF (@REPORTLEVEL='BOROS') 	
							BEGIN 
								 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
								SET @strSQL=@strSQL + ' AND c.BoroughID =(SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'					
							END
							
							IF  (@REPORTLEVEL='DISTRICTS')	
							BEGIN 
									BEGIN 
							   		 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
									 SET @strSQL=@strSQL + ' AND L.LocationName='''+@DistrictName+''''
							END
							END
							
							ELSE IF(@REPORTLEVEL='INDIVIDUALS')
								BEGIN
									SET @strSQL=@strSQL + ' AND c.BoroughID IS NOT NULL AND C.LocationId IS NOT NULL'	
								END		
									
						END
				 IF (@Code_Category_Id=13)
					BEGIN  
						SET @strSQL=@strSQL + ' [FinalStatuscd]='+CONVERT(VARCHAR,@Code_Id)  
						IF (@REPORTLEVEL='BOROS') 	
							BEGIN 
             				 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
								SET @strSQL=@strSQL + ' AND c.BoroughID =(SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'					
							END
							
							IF  (@REPORTLEVEL='DISTRICTS')	
							BEGIN 

							   		 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
									 SET @strSQL=@strSQL + ' AND L.LocationName='''+@DistrictName+''''
							END
							
							ELSE IF(@REPORTLEVEL='INDIVIDUALS')
								BEGIN
									SET @strSQL=@strSQL + ' AND c.BoroughID IS NOT NULL AND C.LocationId IS NOT NULL'										
								END		
					END	
					
			  IF (@Code_Category_Id=8)
					BEGIN  
						SET @strSQL=@strSQL + ' [TrialReturnStatuscd]='+CONVERT(VARCHAR,@Code_Id)  
						IF (@REPORTLEVEL='BOROS') 	
							BEGIN 
								SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)

								SET @strSQL=@strSQL + ' AND c.BoroughID =(SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'					
							END
							
							IF  (@REPORTLEVEL='DISTRICTS')	
							BEGIN 

							   		 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
									 SET @strSQL=@strSQL + ' AND L.LocationName='''+@DistrictName+''''
							END
							
							ELSE IF(@REPORTLEVEL='INDIVIDUALS')
								BEGIN
								SET @strSQL=@strSQL + ' AND c.BoroughID IS NOT NULL AND C.LocationId IS NOT NULL'			
								END					
									
					END					
					
				IF (@CaseType='All')
					BEGIN		
						SET @strSQL=@strSQL + ' [Actioncd]<>46'   
						IF (@REPORTLEVEL='BOROS') 	
							BEGIN 
				     			 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)

								SET @strSQL=@strSQL + ' AND c.BoroughID =(SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'					
							END
							
							IF  (@REPORTLEVEL='DISTRICTS')	
							BEGIN 

							   		 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
									 SET @strSQL=@strSQL + ' AND L.LocationName='''+@DistrictName+''''
							END
							
							ELSE IF(@REPORTLEVEL='INDIVIDUALS')
								BEGIN
   								SET @strSQL=@strSQL + ' AND c.BoroughID IS NOT NULL AND C.LocationId IS NOT NULL'
								END								
					END				
					
				IF (@CaseType='Exclude OATH')
					BEGIN	
						IF (@REPORTLEVEL='BOROS') 	
							BEGIN 
								 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)

								SET @strSQL=@strSQL + ' AND c.BoroughID =(SELECT LOCATIONID FROM tblLocations WHERE LocationName ='''+@BoroName+''')'					
							END
							
							IF  (@REPORTLEVEL='DISTRICTS')	
							BEGIN 

							   		 SET @strSQL=@strSQL + ' AND c.LOCATIONID = ' +CONVERT(VARCHAR,@LocationId)
									 SET @strSQL=@strSQL + ' AND L.LocationName='''+@DistrictName+''''
							END
							
							ELSE IF(@REPORTLEVEL='INDIVIDUALS')
								BEGIN
									SET @strSQL=@strSQL + ' AND c.BoroughID IS NOT NULL AND C.LocationId IS NOT NULL'	
								END									
					END								
							
							
					IF(@BeginDate IS NOT NULL AND  @EndDate IS NOT NULL)   
						BEGIN
							 SET @strSQL=@strSQL + ' AND  '
							 
							 IF (@Code_Category_Id=7)
								 SET @strSQL=@strSQL + ' ah.ModifiedDt BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''						
							 ELSE
							  SET @strSQL=@strSQL + ' ca.ModifiedDate BETWEEN ''' + @BeginDate + ''' AND ''' + @EndDate + ''''													 
						END					
						
						SET @strSQL=@strSQL + ' AND c.BoroughID IS NOT NULL AND C.LocationId IS NOT NULL'
										
				   EXECUTE  (@strSQL)						
		END    
    
    


Go
ALTER PROCEDURE [dbo].[usp_GetCodeList]
@CategoryID int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT  Code_Id,upper(Code_Name) as Code_Name,upper(Code_Description) as Code_Description 
	FROM Code
	WHERE Code_Category_Id = @CategoryID and is_active ='Y'
	ORDER BY Item_Order
END
