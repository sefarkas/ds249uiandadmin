
BEGIN TRY

BEGIN TRAN

update Code
set Value_Data_Type='T'
where Code_Id=86


Insert into Code

      (Code_Id,Code_Category_Id,Code_Name,Code_Description,Item_Order,Value_Data_Type,is_active,Created_User,Created_Date)values

(184,21,'REPRIMAND',NULL,NULL,NULL,'Y',NULL,GETDATE())

Update p set p.ApplyToTotal=1 from tblDS249Complaints c
inner join tblComplaintAdvocacy a
on c.ComplaintId=a.ComplaintId
inner join tblAdvocatedPenalty p 
on p.AdvocacyCaseID=a.AdvocacyCaseId
where (c.DataSource='DE' or a.DataSource='DE')
and Term_DESC='DAYS'




update SupportedDocument
set ContentType='application/msword'
where DocumentID in
(select DocumentID from dbo.SupportedDocument
where SUBSTRING(AttachementName,len(AttachementName)-3,len(AttachementName))='.doc'
and ContentType='application/octet-stream'
and IsDeleted=0)

update SupportedDocument
set AttachementName='bocchino t.b 1-22-11.doc'
,ContentType='application/msword'
where DocumentID=11295

--tiff
--select * from dbo.SupportedDocument
--where DocumentID in(8458,11822,11852)


 COMMIT TRAN

END TRY

BEGIN CATCH

ROLLBACK TRAN

PRINT ERROR_MESSAGE()

END CATCH




----select INDEXno from tblDS249Complaints
----where ComplaintId=20802

-- select c.IndexNo,c.DataSource, a.DataSource,p.ApplyToTotal,a.IsAvailable,Term_DESC
--from tblDS249Complaints c
--inner join tblComplaintAdvocacy a
--on c.ComplaintId=a.ComplaintId
--inner join tblAdvocatedPenalty p 
--on p.AdvocacyCaseID=a.AdvocacyCaseId
--where (c.DataSource='DE' or a.DataSource='DE')
--and Term_DESC='DAYS' and IndexNo = 97542
