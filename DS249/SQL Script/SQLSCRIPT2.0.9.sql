
ALTER PROCEDURE [dbo].[usp_GetAdvocateComplaints] 
	-- Add the parameters for the stored procedure here	
@PageNo int = 0,
@maximumRows int = 0

AS
BEGIN

SET NOCOUNT ON;
DECLARE @Count int
Set @Count = (SELECT	COUNT(*) as AdvocateComplaintsCount
 FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id		
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId))

DECLARE @FirstRow INT, @LastRow INT

SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,

      @LastRow = (@PageNo * @maximumRows) + @maximumRows ;

WITH AdvocateComplaints  AS
(
SELECT
		ROW_NUMBER() OVER (ORDER BY a.ComplaintId DESC) AS ROWID,
		case a.DataSource 
		when 'OEDM' then 'OEDM'
		else 'DATAEASE'
		End as DataSource
		,d.AdvocacyCaseId
		,a.ComplaintId,
		a.IndexNo as IndexNo,
		 a.ApprovedDate
		,a.RoutingLocation
		,a.[LocationId]
		,b.LocationName
		,[RespondentEmpID]
		,[RespondentRefNumber]      
		,ComplaintStatus 
		,ComplaintStatusDate
		,'false' as Approved 
		, '' as Charges    
		--,c.LastName + ' ,' + c.FirstName as 'CreatedBy'
		,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ '. ' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
		,a.CreatedDate
		,a.RespondentFName
		,a.RespondentLName,		
		Status = (case when d.FinalStatuscd = '96' then  e.Code_Name
                         when d.FinalStatuscd = '97' then  e.Code_Name 
                         When d.FinalStatuscd = '98' then  e.Code_Name 
                         end),
         '' as Indicator, a.IsDeleted
         
  FROM [dbo].[tblDS249Complaints] a LEFT OUTER JOIN tblLocations b
		ON a.LocationId = b.LocationId LEFT OUTER JOIN uv_EmployeePerson c
		on convert(bigint,a.CreatedBy) = c.EmployeeID INNER JOIN  tblComplaintAdvocacy d
		on (a.ComplaintId = d.ComplaintID) LEFT OUTER JOIN Code e
		On d.FinalStatusCd = e.Code_Id
		--once database is updated check only for a.ComplaintStatus=4
  -- where (a.ComplaintStatus = 2 or a.ComplaintStatus=3 or IsSubmitted = '1')  
  where (a.ComplaintStatus = 5 or IsSubmitted = '1')
 		and (a.RoutingLocation = 'A')
		AND (d.IsAvailable is null) 
		AND a.IsDeleted =0 
		and  AdvocacyCaseId = (Select TOP 1 [AdvocacyCaseId]   
	FROM [dbo].[tblComplaintAdvocacy]	
	where ComplaintId=a.ComplaintId	and IsAvailable is null
	order by a.ApprovedDate Desc,IndexNo ))
 
select * from AdvocateComplaints WHERE ROWID BETWEEN @FirstRow AND @LastRow 

      ORDER BY ROWID asc;
 
END

GO

ALTER PROCEDURE [dbo].[usp_GetComplaintHistory] 
	
	@EmpID bigint,
	@ComplaintID  bigint 
AS
BEGIN
	
	SET NOCOUNT ON;
	   
	SELECT ComplaintId,
			IndexNo,
			RoutingLocation=(case RoutingLocation
         WHEN  'A' THEN 'OEDM'
         WHEN  'B' THEN 'BCAD'
         WHEN 'M' THEN ''
         ELSE ''
        END),	
			CreatedDate,
			
			(Select dbo.GetChargesAsString(ComplaintId)) as 'Charges',
			(Select dbo.GetChargesDescAsString(ComplaintId)) as 'ChargesDesc',
			dbo.fnGetDisposition(IndexNo) as 'Penalty'
	FROM tblDS249Complaints
	Where RespondentEmpID = @EmpID 
	and IsDeleted=0
	and 
	CreatedDate <= Case When @ComplaintID = -1
								Then (Select dateadd(dd, 1,Max(CreatedDate)) from tblDS249Complaints)
								Else (Select CreatedDate from tblDS249Complaints where ComplaintId = @ComplaintID)	
						End		
	Order by CreatedDate desc
END

Go


Update tblLocations set ParentLocationID=270 where LocationId in (196,214,217)