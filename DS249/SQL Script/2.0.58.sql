

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 1 and RoleID=1

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 1 and RoleID=2

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 1 and RoleID=3

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 1 and RoleID=4

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 1 and RoleID=5
DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 1 and RoleID=6

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 1 and RoleID=7

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 1 and RoleID=10



DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 2 and RoleID=1

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 2 and RoleID=2

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 2 and RoleID=4

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 2 and RoleID=5



DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 3 and RoleID=1

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 3 and RoleID=2

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 3 and RoleID=4

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 3 and RoleID=5



DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 4 and RoleID=11

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 4 and RoleID=12

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 4 and RoleID=13



DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 6 and RoleID=3

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 6 and RoleID=8

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 6 and RoleID=9

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 6 and RoleID=20


DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 7 and RoleID=1

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 7 and RoleID=2

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 7 and RoleID=4

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 7 and RoleID=5


DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 8 and RoleID=1

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 10 and RoleID=14

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 10 and RoleID=15

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 10 and RoleID=16

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 10 and RoleID=17

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 10 and RoleID=18

DELETE TOP(1) FROM [tblMemberShipRolesAssociation] WHERE membershipid = 10 and RoleID=19




ALTER TABLE [dbo].[SupportedDocument]
    ALTER COLUMN ContentType nVARCHAR (100)

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_InsertComplaintAttachments] 
	-- Add the parameters for the stored procedure here
	@ParentID  numeric,
	@Attachments varbinary(MAX),
	@ApplicationName nvarchar(50),
	@AttachementName nvarchar(50),
	@ContentType nvarchar(100),
	@ChildId1 nvarchar(50)=null,
	@ChildId2 numeric(8, 0)=null,
	@ChildId3 numeric(8, 0)=null
AS

	BEGIN
    
	INSERT INTO SupportedDocument 
	(ParentID, Attachments,ApplicationName, AttachementName,ContentType,ChildId1,ChildId2,ChildId3,IsDeleted) 
	VALUES (@ParentID, @Attachments,@ApplicationName,@AttachementName,@ContentType,@ChildId1,@ChildId2,@ChildId3,0) 
END




Update dbo.SupportedDocument 
set ContentType ='application/vnd.openxmlformats-officedocument.wordprocessingml.document' 
where DocumentID =18123


Update dbo.SupportedDocument 
set ContentType ='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' 
where DocumentID in (29419,29436,29439)