/****** Object:  StoredProcedure [dbo].[usp_GetRoles]    Script Date: 06/06/2012 09:52:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetRoles]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaints]    Script Date: 06/06/2012 09:52:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaints]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaints]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 06/06/2012 09:52:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_GetComplaintsBySearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchPersonnelList]    Script Date: 06/06/2012 09:52:38 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchPersonnelList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[usp_SearchPersonnelList]
GO
/****** Object:  UserDefinedFunction [dbo].[GetBusinessDays]    Script Date: 06/06/2012 09:52:39 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBusinessDays]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[GetBusinessDays]
GO
/****** Object:  UserDefinedFunction [dbo].[GetBusinessDays]    Script Date: 06/06/2012 09:52:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetBusinessDays]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
BEGIN
execute dbo.sp_executesql @statement = N'  
-- Select dbo.GetBusinessDays (''01/04/2009'',''01/10/2009'')  
  
  
CREATE function [dbo].[GetBusinessDays]  
(  
     @StartDate datetime,  
     @EndDate datetime  
)   
returns int  
 as  
begin  
  
  --declare @DaysBetween int  
  declare @BusinessDays int 
  set @BusinessDays = 0  
  --declare @Cnt int  
  --declare @EvalDate datetime  
  
  --select @DaysBetween = 0  
  --select @BusinessDays = 0  
  --select @Cnt=0  
  
  --select @DaysBetween = datediff(Day,@StartDate,@endDate) + 1    

  -- while @Cnt < @DaysBetween  
  --   begin  
    
		--set @EvalDate = @StartDate + @Cnt    
		--	if ((datepart(dw,@EvalDate) <> 6) and (datepart(dw,@EvalDate) <> 7))  
		--	BEGIN  
		--		 set @BusinessDays = @BusinessDays + 1  
		--	END          
		--set @Cnt = @Cnt + 1  
  --  end  
  
  
		--SET DATEFIRST  1
		SET @BusinessDays =DATEDIFF (day, @StartDate, @EndDate) - (2 * DATEDIFF(week, @StartDate, @EndDate)) - 

		CASE WHEN DATEPART(weekday, @StartDate + @@DATEFIRST) = 1 
			THEN 1 
			ELSE 0 
		END - 
		CASE WHEN DATEPART(weekday, @StartDate + @@DATEFIRST) = 1 
			THEN 1 
			ELSE 0 
		END + 1
		
		return @BusinessDays  
end  
  
  
  
  
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_SearchPersonnelList]    Script Date: 06/06/2012 09:52:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_SearchPersonnelList]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

CREATE PROCEDURE [dbo].[usp_SearchPersonnelList] 
	-- Add the parameters for the stored procedure here	
@SearchString varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT a.FirstName, a.MiddleName, a.LastName, b.EmployeeID, b.ReferenceNo
	FROM tblPersons a INNER JOIN tblEmployees b
	ON a.PersonID = b.PersonID
	WHERE a.LastName like @SearchString + ''%''
	AND b.IsActive is null
	Order by LastName, FirstName
	
		--SELECT a.FirstName, a.MiddleName, a.LastName, b.EmployeeID, b.ReferenceNo
		--FROM tblPersons a INNER JOIN tblEmployees b
		--ON a.PersonID = b.PersonID
		--WHERE a.LastName + '' ''+ a.FirstName like @SearchString + ''%''
		--AND b.IsActive = 1
	
	--and b.ReferenceNo is not null or b.ReferenceNo <> ''''
	
END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaintsBySearch]    Script Date: 06/06/2012 09:52:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaintsBySearch]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'

--[usp_GetComplaintsBySearch] 10045,0, 50,0,'''',''01/02/2010'','''',64
CREATE PROCEDURE [dbo].[usp_GetComplaintsBySearch]
	-- Add the parameters for the stored procedure here
@CreatedBy int=0,
@pageNo int = 0,
@maximumRows int = 0,
@numericValue int = 0,
@chargesValue varchar(20),
@complaintDate varchar(20),
@stringValue varchar(50),
@RowCount int OUTPUT 
AS
BEGIN
	SET NOCOUNT ON;
 
	DECLARE @numericStringValue varchar(50)
	DECLARE  @chargesLikeValue varchar(22)
	DECLARE @stringLikeValue varchar(52)
	DECLARE @FirstRow INT 
	DECLARE @LastRow INT
	DECLARE @count INT
	declare @FullStop char(2) = ''. ''
	declare @Blank char(1) = ''''
	
	SELECT   @chargesLikeValue = ''%'' +@chargesValue + ''%'' 
			,@stringLikeValue = ''%'' + @stringValue +  ''%'' 
			,@numericStringValue =''%'' +CAST( @numericValue as varchar) + ''%''
			,@FirstRow = (@PageNo * @maximumRows) + 1
			,@LastRow = (@PageNo * @maximumRows) + @maximumRows ;  


	
	select @count=COUNT(m.Id) 
	from tblMembershipRoles m 
	where 
		PersonalID = @CreatedBy 
		and m.RoleId = 10 --RoleID 10 = ''Commissioner Access''
	
	if @count>=1
		Begin		
		if @numericValue != 0  
				Begin					
					SELECT 
						ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
						[ComplaintId]
						,IndexNo
						,a.[LocationId]
						,a.[BoroughID]
						,a.promotiondate
						,a.LocationName
						,(case when a.complaintStatus >=4 then  a.RoutingLocation
							when a.RoutingLocation =''P'' then  a.RoutingLocation
							else null 
						end) as RoutingLocation
						,[RespondentEmpID]
						,[RespondentRefNumber]      
						,[Approved]      
						,CreatedBy      
						,CONVERT(varchar,CreatedDate,101) as CreatedDate
						--,'''' as Status
						,charges as Charges
						,Chargesdesc as ChargesDesc
						, RespondentFName
						,RespondentLName
						,A.complaintStatus
						,A.complaintStatusDate
						,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
								else 0  end) as Indicator,
						a.IsDeleted,
						a.jt1,0  as FinalStatusCD 
					into #Complaints5  
					FROM dbo.vw_ComissionerAcess a 			 
					where 
						IndexNo =@numericValue
						OR RespondentRefNumber like @numericStringValue
						or isnull(a.jt1,@Blank) like @numericStringValue
					order by a.CreatedDate desc

					SELECT  * 
					FROM  #Complaints5 
					where ROWID BETWEEN @FirstRow AND @LastRow 
					ORDER BY ROWID asc     

					SELECT @RowCount=COUNT([ComplaintId]) 
					FROM #Complaints5

					drop table #Complaints5  

				End
				else if @chargesValue != @Blank 
						Begin
							SELECT 
								ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
								[ComplaintId]
								,IndexNo
								,a.[LocationId]
								,a.[BoroughID]
								,a.promotiondate
								,a.LocationName
								,(case when a.complaintStatus >=4 then  a.RoutingLocation
										when a.RoutingLocation =''P'' then  a.RoutingLocation
										else null 
										end) as RoutingLocation
								,[RespondentEmpID]
								,[RespondentRefNumber]      
								,[Approved]      
								, CreatedBy      
								,CONVERT(varchar,CreatedDate,101) as CreatedDate
								--,@Blank as Status
								,charges as Charges
								,Chargesdesc as ChargesDesc
								, RespondentFName
								,RespondentLName
								,A.complaintStatus
								,A.complaintStatusDate
								,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
										else 0  end) as Indicator
								,a.IsDeleted
								,a.jt1
								,0  as FinalStatusCD 
							into #Complaints6  
							FROM dbo.vw_ComissionerAcess a 
							where 
								Charges like @chargesLikeValue
							order by  a.CreatedDate desc

							SELECT  * 
							FROM  #Complaints6 
							where ROWID BETWEEN @FirstRow AND @LastRow  
							ORDER BY ROWID asc  
							
							SELECT @RowCount=COUNT([ComplaintId]) 
							FROM #Complaints6
							
							drop table #Complaints6     
						End	
				 else if @complaintDate != @Blank
						Begin
							SELECT 	  
								ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,
								[ComplaintId]
								,IndexNo
								,a.[LocationId]
								,a.[BoroughID]
								,a.promotiondate
								,a.LocationName
								,(case when a.complaintStatus >=4 then  a.RoutingLocation
									when a.RoutingLocation =''P'' then  a.RoutingLocation
									else null 
									end) as RoutingLocation
								,[RespondentEmpID]
								,[RespondentRefNumber]      
								,[Approved]      
								,CreatedBy      
								,CONVERT(varchar,CreatedDate,101) as CreatedDate
								--,@Blank as Status
								,charges as Charges
								,Chargesdesc as ChargesDesc
								, RespondentFName
								,RespondentLName
								,A.complaintStatus
								,A.complaintStatusDate
								,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
										else 0  end) as Indicator
								,a.IsDeleted
								,a.jt1
								,0  as FinalStatusCD 
							  into #Complaints7  
						 FROM dbo.vw_ComissionerAcess a 
						 where  cast(CreatedDate as date) = CAST(@complaintDate as date)
						 order by  a.CreatedDate desc
						 	

						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #Complaints7
						
						SELECT  * 
						FROM  #Complaints7  
						where ROWID BETWEEN @FirstRow AND @LastRow  
						ORDER BY ROWID asc   
						
						drop table #Complaints7 
												
					End	
				 else if @stringValue != @Blank
						Begin	
						   SELECT 
									ROW_NUMBER() OVER (ORDER BY a.CreatedDate DESC) AS ROWID,       
									[ComplaintId]
									,IndexNo
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,a.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation
											when a.RoutingLocation =''P'' then  a.RoutingLocation
											else null 
											end) as RoutingLocation
									,[RespondentEmpID]
									,[RespondentRefNumber]      
									,[Approved]      
									,CreatedBy      
									,CONVERT(varchar,CreatedDate,101) as CreatedDate
									--, @Blank as Status
									,charges as Charges
									,Chargesdesc as ChargesDesc
									, RespondentFName
									,RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(CreatedDate,GETDATE()) 
											else 0  end) as Indicator
									,a.IsDeleted
									,a.jt1
									,0  as FinalStatusCD 
						 into #Complaints8  
						 FROM dbo.vw_ComissionerAcess a 					 
						 where 
								RespondentFName like @stringLikeValue
								OR RespondentLName like @stringLikeValue  
								OR CreatedBy like @stringLikeValue 
								OR LocationName like @stringLikeValue 
								or isnull(a.jt1,@Blank) like @stringLikeValue
						 order by  a.CreatedDate desc
							
						SELECT  * 
						FROM  #Complaints8  
						where ROWID BETWEEN @FirstRow AND @LastRow  
						ORDER BY ROWID asc  
						
						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #Complaints8
						
						drop table #Complaints8    
						 
				   End	
			End
	Else
		Begin				
				create table #Complaints(ComplaintId bigint)     
				
				insert  into #Complaints
							exec dbo.[usp_GetComplaintsSecurity] @CreatedBy;
							
				if @numericValue != 0  
					Begin		
						SELECT 
							a.[ComplaintId]
							,a.IndexNo
							,a.[LocationId]
							,a.[BoroughID]
							,a.promotiondate
							,b.LocationName
							,(case when a.complaintStatus >=4 then  a.RoutingLocation
									else null 
									end) as RoutingLocation
							,a.[RespondentEmpID]
							,a.[RespondentRefNumber]      
							,a.[Approved]
							,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
							,a.[CreatedDate]
							--, @Blank as Status
							,a.Charges as Charges
							,a.Chargesdesc as ChargesDesc
							, a.RespondentFName
							,a.RespondentLName
							,A.complaintStatus
							,A.complaintStatusDate
							,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end)as Indicator
							,a.IsDeleted
							,a.Jt1
							,0  as FinalStatusCD 
							,ROWID =IDENTITY(INT,1,1) 
						into #tempNumeric
						FROM 
							[dbo].[tblDS249Complaints] a 
							inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
							LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
							LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
						where 
							IndexNo = @numericValue 
							OR RespondentRefNumber like @numericStringValue 
							or jt1 like   @numericStringValue
						order by a.CreatedDate desc

						SELECT @RowCount=COUNT([ComplaintId]) 
						FROM #tempNumeric
						
						select * from #tempNumeric
						WHERE ROWID BETWEEN @FirstRow AND @LastRow
						ORDER BY  ROWID asc;
						
						drop table #Complaints						
						drop table  #tempNumeric
					End
				else if @chargesValue != @Blank 
						Begin
								SELECT 
									a.[ComplaintId]
									,a.IndexNo
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,b.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation
									else null 
									end) as RoutingLocation
									,a.[RespondentEmpID]
									,a.[RespondentRefNumber]      
									,a.[Approved]
									,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy 							     
									,a.[CreatedDate]
									--,@Blank as [Status]
									,a.Charges as Charges
									,a.Chargesdesc as ChargesDesc
									, a.RespondentFName
									,a.RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end) as Indicator,
									a.IsDeleted,
									a.Jt1,0  as FinalStatusCD ,
									ROWID =IDENTITY(INT,1,1) 
								into #tempCharges
								FROM [dbo].[tblDS249Complaints] a 
									inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
									LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
									LEFT OUTER JOIN uv_EmployeePerson c on  a.CreatedBy = c.EmployeeID  
								where Charges like @chargesLikeValue 
								order by  a.CreatedDate desc
								SELECT @RowCount=COUNT([ComplaintId]) FROM #tempCharges

								select * from #tempCharges
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints
								drop table  #tempCharges

						End	
				else if @complaintDate != @Blank
						Begin	
								SELECT 
									a.[ComplaintId]
									,a.IndexNo
									,a.[LocationId]
									,a.[BoroughID]
									,a.promotiondate
									,b.LocationName
									,(case when a.complaintStatus >=4 then  a.RoutingLocation
									else null 
									end) as RoutingLocation
									,a.[RespondentEmpID]
									,a.[RespondentRefNumber]      
									,a.[Approved]
									,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
									,a.[CreatedDate]
									--, @Blank as Status
									,a.Charges as Charges
									,a.Chargesdesc as ChargesDesc
									, a.RespondentFName
									,a.RespondentLName
									,A.complaintStatus
									,A.complaintStatusDate
									,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
									else 0
									end) as Indicator
									,a.IsDeleted
									,a.Jt1
									,0  as FinalStatusCD
									,ROWID =IDENTITY(INT,1,1) 
								into #tempComplaintDate
								FROM [dbo].[tblDS249Complaints] a 
									inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
									LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
									LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
								where							
									cast(CreatedDate as date) = CAST(@complaintDate as date)
								order by  a.CreatedDate desc



								SELECT @RowCount=COUNT([ComplaintId]) 
								FROM #tempComplaintDate

								select * from #tempComplaintDate
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints 
								drop table  #tempComplaintDate

						End	
				else if @stringValue != @Blank
						Begin
								SELECT 
										a.[ComplaintId]
										,a.IndexNo
										,a.[LocationId]
										,a.[BoroughID]
										,a.promotiondate
										,b.LocationName
										,(case when a.complaintStatus >=4 then  a.RoutingLocation
												else null 
												end) as RoutingLocation
										,a.[RespondentEmpID]
										,a.[RespondentRefNumber]      
										,a.[Approved]
										,LEFT (LTRIM(RTRIM(c.FirstName)),1)+ @FullStop + LTRIM(RTRIM(c.LastName)) as CreatedBy      
										,a.[CreatedDate]
										--, @Blank as [Status]
										,a.Charges as Charges
										,a.Chargesdesc as ChargesDesc
										, a.RespondentFName
										,a.RespondentLName
										,A.complaintStatus
										,A.complaintStatusDate
										,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
												else 0
												end) as Indicator
										,a.IsDeleted
										,a.Jt1
										,0  as FinalStatusCD 
										,ROWID =IDENTITY(INT,1,1) 
								into #tempStringValue
								FROM [dbo].[tblDS249Complaints] a 
										inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
										LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
										LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
								where 
										RespondentFName like @stringLikeValue 
										OR RespondentLName like @stringLikeValue  
										--OR CreatedBy like ''%''+Convert(varchar,@stringLikeValue)+ ''%'' 
										OR LocationName like @stringLikeValue 
										or  jt1 like   @stringLikeValue
										or c.FirstName like @stringLikeValue 
										or c.LastName like @stringLikeValue 
								order by  a.CreatedDate desc


								SELECT @RowCount=COUNT([ComplaintId]) 
								FROM #tempStringValue


								select * 
								from #tempStringValue
								WHERE ROWID BETWEEN @FirstRow AND @LastRow
								ORDER BY  ROWID asc;
								
								drop table #Complaints
								drop table  #tempStringValue
						End	 
	END
END
' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetComplaints]    Script Date: 06/06/2012 09:52:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetComplaints]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'


CREATE PROCEDURE [dbo].[usp_GetComplaints] 
	-- Add the parameters for the stored procedure here
@CreatedBy int = 0,
@PageNo int = 0,
@maximumRows int = 0,
@rowsCount int OUTPUT
AS
BEGIN
		SET NOCOUNT ON;

		DECLARE @Count int
		
		SELECT	@Count = COUNT(*) 
		FROM [dbo].[tblDS249Complaints] a  
				LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
				LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  

		DECLARE @FirstRow INT
				,@LastRow INT
				
		SELECT  @FirstRow = (@PageNo * @maximumRows) + 1,
				@LastRow = (@PageNo * @maximumRows) + @maximumRows;


		select @count=COUNT(Id) 
		from tblMembershipRoles 
		where PersonalID=@CreatedBy 
				and tblMembershipRoles.RoleId= 10 --RoleID 10 =''Commissioner Access''
  
		if @count>=1
		Begin

			SELECT @rowsCount=COUNT(ComplaintId) 
			FROM dbo.vw_ComissionerAcess

			SELECT   
				ComplaintId
				,IndexNo
				,LocationId
				,BoroughID
				,promotiondate
				,LocationName
				,RoutingLocation
				,[RespondentEmpID]
				,[RespondentRefNumber]      
				,[Approved]      
				,CreatedBy      
				,[CreatedDate]
				--, Status
				,Charges
				,ChargesDesc
				, RespondentFName
				,RespondentLName
				,complaintStatus
				,complaintStatusDate
				,Indicator,
				IsDeleted,
				ROWID, 
				jt1
				,0 as FinalStatusCD 
			FROM  dbo.vw_ComissionerAcess
			WHERE ROWID BETWEEN @FirstRow AND @LastRow
			ORDER BY ROWID asc;
			
		end
		else
		begin
			create table #Complaints(ComplaintId bigint)
			
			insert  into #Complaints
					exec dbo.[usp_GetComplaintsSecurity] @CreatedBy;

			SELECT @rowsCount=COUNT(ComplaintId) FROM #Complaints;
			
			SELECT 
				a.[ComplaintId]
				,a.IndexNo
				,a.[LocationId]
				,a.[BoroughID]
				,a.promotiondate
				,b.LocationName
				,(case when a.complaintStatus >=4 then  a.RoutingLocation
				else null 
				end) as RoutingLocation
				,a.[RespondentEmpID]
				,a.[RespondentRefNumber]      
				,a.[Approved]
				,LTRIM(RTRIM(LEFT (c.FirstName,1)))+ ''. '' + LTRIM(RTRIM(c.LastName)) as CreatedBy      
				,a.[CreatedDate]
				--, '''' as ''Status''
				,a.Charges as Charges
				,a.Chargesdesc as ChargesDesc
				, a.RespondentFName
				,a.RespondentLName
				,A.complaintStatus
				,A.complaintStatusDate
				,(case when a.complaintStatus =1 then dbo.GetBusinessDays(a.CreatedDate,GETDATE()) 
				else 0
				end) as Indicator,
				a.IsDeleted,
				a.Jt1
				,0  as FinalStatusCD ,
				ROWID =IDENTITY(INT,1,1) 
			into #temp
			FROM [dbo].[tblDS249Complaints] a 
				inner join #Complaints temp on a.ComplaintId=temp.ComplaintId
				LEFT OUTER JOIN tblLocations b ON a.LocationId = b.LocationId 
				LEFT OUTER JOIN uv_EmployeePerson c on a.CreatedBy = c.EmployeeID  
			order by a.CreatedDate desc


			select * 
			from #temp
			WHERE ROWID BETWEEN @FirstRow AND @LastRow
			ORDER BY  ROWID asc;
			
			drop table #Complaints
		end

END

' 
END
GO
/****** Object:  StoredProcedure [dbo].[usp_GetRoles]    Script Date: 06/06/2012 09:52:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[usp_GetRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'-- =============================================
-- Author:		<Abhishek Sinha>
-- Create date: <02/18/2009>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[usp_GetRoles]
	-- Add the parameters for the stored procedure here	
	@MembershipID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--Select * from tblRoles
	select r.Id
			,r.Roles 
	from tblRoles r
	inner join tblMemberShipRolesAssociation rm on rm.roleid= r.id
	where rm.MembershipID = @MembershipID
	
END


' 
END
GO



CREATE TABLE [dbo].[tblMemberShipRolesAssociation](
	[MembershipID] [int] NULL,
	[RoleID] [int] NULL
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tblMemberShipRolesAssociation]  WITH CHECK ADD  CONSTRAINT [FK_tblMemberShipRolesAssociation_tblMembership] FOREIGN KEY([MembershipID])
REFERENCES [dbo].[tblMembership] ([Id])
GO

ALTER TABLE [dbo].[tblMemberShipRolesAssociation] CHECK CONSTRAINT [FK_tblMemberShipRolesAssociation_tblMembership]
GO

ALTER TABLE [dbo].[tblMemberShipRolesAssociation]  WITH CHECK ADD  CONSTRAINT [FK_tblMemberShipRolesAssociation_tblRoles] FOREIGN KEY([RoleID])
REFERENCES [dbo].[tblRoles] ([Id])
GO

ALTER TABLE [dbo].[tblMemberShipRolesAssociation] CHECK CONSTRAINT [FK_tblMemberShipRolesAssociation_tblRoles]
GO
insert into tblMemberShipRolesAssociation 
--(MembershipID,RoleID) 
select 1,1 union
select 1,2 union
select 1,3 union
select 1,4 union
select 1,5 union
select 1,6 union
select 1,7 union
select 1,10 union
select 2,1 union
select 2,2 union
select 2,4 union
select 2,5 union
select 3,1 union
select 3,2 union
select 3,4 union
select 3,5 union
select 7,1 union
select 7,2 union
select 7,4 union
select 7,5 union
select 6,3 union
select 6,8 union
select 6,9 union
select 8,1 union
select 6,20 union

select 4,11 union
select 4,12 union
select 4,13 union

select 10,14 union
select 10,15 union
select 10,16 union
select 10,17 union
select 10,18 union
select 10,19