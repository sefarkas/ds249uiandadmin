﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.DS249
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected System.Web.UI.WebControls.ImageButton ibHelp;
        protected System.Web.UI.WebControls.ImageButton ibDS249;
        protected System.Web.UI.WebControls.ImageButton ibAdvocate;
        protected System.Web.UI.WebControls.ImageButton ibBCAD;
        protected System.Web.UI.WebControls.ImageButton ibMedical;
        protected System.Web.UI.WebControls.ImageButton btnlogout;
        protected System.Web.UI.WebControls.ImageButton ibEmployee;
        User user;

        protected void Page_Load(object sender, EventArgs e)
        {
            string script = "$(document).ready(function () { $('[id*=ubmit]').click(); });";

            // Define the name and type of the client scripts on the page.
            String csname1 = "load";
            Type cstype = this.GetType();

            // Get a ClientScriptManager reference from the Page class.
            ClientScriptManager cs = Page.ClientScript;

            if (!IsPostBack)
            {
                 cs.RegisterStartupScript( cstype, csname1, script, true );
            }
            else
            {
               // Check to see if the startup script is already registered.
                if (!cs.IsStartupScriptRegistered( cstype, csname1 ))
                {
                    cs.RegisterStartupScript( cstype, csname1, script, true );
                }
            }



            ibHelp.Attributes.Add("onclick", "return OpenHelp();");
            user = (User)Session["clsUser"];
            if (user != null)
            {
                if (user.IsInMemberShip(GroupName.DS249))
                    ibDS249.Visible = true;
                ibHelp.Visible = true;
                btnlogout.Visible = true;
                if (user.IsInMemberShip(GroupName.ADVOCATE))
                    ibAdvocate.Visible = true;
                    ibHelp.Visible = true;
                    btnlogout.Visible = true;
                if (user.IsInMemberShip(GroupName.BCAD))
                    ibBCAD.Visible = true;
                if (user.IsInMemberShip(GroupName.MEDICAL))
                    ibMedical.Visible = true;
                if (user.IsInMemberShip(GroupName.EMPLOYEE))
                    ibEmployee.Visible = true;
                ibHelp.Visible = true;
                btnlogout.Visible = true;
                if (user.CurrentMembership == GroupName.ADVOCATE)
                    ibHelp.Attributes.Add("onclick", "return OpenAdvocateHelp();");
                else if (user.CurrentMembership == GroupName.BCAD)
                    ibHelp.Attributes.Add("onclick", "return OpenBCADHelp();");
                else
                    ibHelp.Attributes.Add("onclick", "return OpenHelp();");
                if (user.CurrentMembership == GroupName.DS249)
                    ibDS249.ImageUrl = "~/includes/img/ds249_main_tab_static.png";
                if (user.CurrentMembership == GroupName.BCAD)
                    ibBCAD.ImageUrl = "~/includes/img/tab_bcad_static.png";
                if (user.CurrentMembership == GroupName.ADVOCATE)
                    ibAdvocate.ImageUrl = "~/includes/img/tab_advocate_static.png";
                if (user.CurrentMembership == GroupName.MEDICAL)
                    ibMedical.ImageUrl = "~/includes/img/tab_medical_static.png";
                if (user.CurrentMembership == GroupName.EMPLOYEE)
                    ibEmployee.ImageUrl = "~/includes/img/EMPLOYEE_main_tabs.png";
            }
            else if (user == null)
            {
                ibHelp.Visible = false;
                btnlogout.Visible = false;
            }
            else
                ibHelp.Attributes.Add("onclick", "return OpenHelp();");

            if (Request.QueryString["Mode"] == "VIEW")
            {
                ibDS249.Visible = false;
                ibBCAD.Visible = false;
                ibAdvocate.Visible = false;
                btnlogout.Visible = false;
                ibHelp.Visible = false;
                ibMedical.Visible = false;
                ibEmployee.Visible = false;
            }
        }      

        protected void ibDS249_Click(object sender, ImageClickEventArgs e)
        {
            user.CurrentMembership = GroupName.DS249;
            Session["DV_BCAD"] = null;
            Session["DT_BCAD"] = null;
            Session["DV_ADVOCATE"] = null;            
            Response.Redirect("~/Complaints/Dashboard.aspx");
        }

        protected void ibBCAD_Click(object sender, ImageClickEventArgs e)
        {
            user.CurrentMembership = GroupName.BCAD;
            Session["DT"] = null;            
            Session["DV_ADVOCATE"] = null;            
            Response.Redirect("~/BCAD/Dashboard.aspx");
        }
        protected void ibAdvocate_Click(object sender, ImageClickEventArgs e)
        {
            user.CurrentMembership = GroupName.ADVOCATE;
            Session["DT"] = null;            
            Session["DV_BCAD"] = null;
            Session["DT_BCAD"] = null;
            Session["SearchCriteriaAdvocate"] = null;
            Session["RespEmployeeId"] = null;
            Session["ComplaintID"] = null;
            Session["IndexNo"] = null;
            Response.Redirect("~/Advocate/Dashboard.aspx");
        }

        protected void btnlogout_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("~/UI/Login.aspx");
        }

        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            ibHelp.Attributes.Add("onclick", "return OpenHelp();");
        }

        protected void ibMedical_Click(object sender, ImageClickEventArgs e)
        {
            user.CurrentMembership = GroupName.BCAD;
            Session["DT"] = null;
            Session["DV"] = null;
            Session["DV_ADVOCATE"] = null;
            Session["DT_Advocate"] = null;
            Response.Redirect("~/Medical/Dashboard.aspx");
        }

        protected void ibEmployee_Click(object sender, ImageClickEventArgs e)
        {
            user.CurrentMembership = GroupName.EMPLOYEE;
            Session["DT"] = null;
            Session["DV"] = null;
            Session["DV_ADVOCATE"] = null;
            Session["DT_Advocate"] = null;
            string strUrl = "https://msswva-dsnmyd01.csc.nycnet/myDSNYProd/";
            Response.Redirect(strUrl);
        }
        
    }
}