﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DSNY.DSNYCP.ClassHierarchy;
using System.IO;
namespace DSNY.DSNYCP.DS249
{
    public partial class EmplyeeApp_WebForm1 : System.Web.UI.Page
    {
        PersonnelList personnelList;       
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadEmployeeData();
        }

        private void LoadEmployeeData()
        {               
                personnelList = new PersonnelList();
                Int64 ID = Convert.ToInt64(Request.QueryString["Id"]);
                personnelList.LoadEmployeeInfo(ID);
                List<Personnel> EmployeeDetail = personnelList.List;
                Personnel EmployeeInfo = EmployeeDetail[0];
                if (EmployeeInfo.ImageId != -1)
                {
                    Response.ContentType = "image/JPEG";
                    Response.BinaryWrite(EmployeeInfo.Images);   
                }
            }
          
        }
    }

