﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeBehind="DashBoard.aspx.cs" Inherits="DSNY.DSNYCP.DS249.DashBoard_EmployeeApp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    
    <title>Employee Dashboard</title>



    <link href="../../~/includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
 <link href="../includes/css/DashboardStyle.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
function openWindow(windowUrl)
{

 // Set your window features here, especially left, top, width, and height...
 var windowFeatures = 
  'channelmode=no,directories=no,fullscreen=no,' +
  'location=no,dependent=yes,menubar=no,resizable=no,scrollbars=yes,' + 
  'status=no,toolbar=no,titlebar=no,' +
  'left=0,top=0,width=400px,height=200px';

 var windowReference = window.open(windowUrl, windowFeatures);

 windowReference.focus(); 
}   
</script>

    <table style="width: 900px; height: 20px" class="tableMargin" border="0" cellpadding="0px"
                    cellspacing="0px" align="center">
                    <tr>
                    <td  style="height:90px"></td>
                    </tr>
                    <tr>
                        <td width="25%">
                            &nbsp;
                        </td>
                        <td class="searchCase" align="right">
                            SEARCH FOR NAME
                        </td>
                        <td align="right" >
                            <asp:TextBox ID="txtSearch" runat="server" Width="200px" Height="17px" 
                                CssClass="inputFieldStyle" ontextchanged="txtSearch_TextChanged"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnSearch" runat="server" ImageUrl="~/includes/img/ds249_btn_search_static.png" 
                                OnClick="btnSearch_Click" />
                            <asp:ImageButton ID="btnSearchCancel" ImageUrl="~/includes/img/ds249_btn_cancel.png" runat="server"
                                OnClick="btnSearchCancel_Click" CausesValidation="False" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                    <td  style="height:20px"></td>
                    <td colspan="2"> <asp:Label ID="lblMessage" Width="50%" runat="server" CssClass="feedbackStatus"></asp:Label></td>
                    </tr>
                </table>
   <table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
                                <tr>
                                    <td>
                                        <asp:Image runat="server" SkinId="skidTopCap" style="width:900px" /></td>
                                </tr>
        </table>
            <center>

                <asp:UpdatePanel ID="upButtons" runat="server">
                    <ContentTemplate>
                        <table style="width: 900px" class="tableMargin" border="0" cellpadding="0px" cellspacing="0px"
                            align="center">
                            <tr>
                                <td>
                                    <div>
                                        <asp:GridView ID="gvEmployees" runat="server" Width="900px" AutoGenerateColumns="False"
                                            BorderColor="#235705" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a"
                                            AllowSorting="True" PageSize="50" PagerSettings-Position="TopAndBottom" PagerStyle-HorizontalAlign="Left"
                                            OnSorting="gvEmployees_Sorting" OnRowDataBound="gvEmployees_RowDataBound" OnPageIndexChanging="gvEmployees_PageIndexChanging"
                                            AllowPaging="True" AlternatingRowStyle-BackColor="#ededed" EmptyDataText="Sorry. No employee found with that criteria"
                                            PagerSettings-Visible="true" OnDataBound="gvEmployees_DataBound">
                                            <PagerTemplate>
                                                <table style="width: 99%;" border="0" cellpadding="0px" cellspacing="0px">
                                                    <tr>
                                                        <td style="width: 2%;"></td>
                                                        <td style="text-align: left; width: 4%; font-family: :Verdana, Geneva, sans-serif; font-size: 11px; margin-left: 2px;">Page
                                                        </td>
                                                        <td style="width: 6%;" align="left">
                                                            <asp:ImageButton ID="imgDoublePrevious" ImageUrl="~/includes/img/arrow_l_multi.png"
                                                                CommandName="Page" CausesValidation="false" CommandArgument='<%#gvEmployees.PageIndex - 4%>'
                                                                runat="server" />
                                                            <asp:ImageButton ID="imgPrevious" ImageUrl="~/includes/img/arrow_l_one.png" CommandName="Page"
                                                                CausesValidation="false" CommandArgument="Prev" ToolTip="Previous Page" runat="server" />
                                                            <td>
                                                                <asp:Menu ID="menuPager" Visible="true" Orientation="Horizontal" OnMenuItemClick="menuPager_MenuItemClick"
                                                                    Font-Size="11px" ForeColor="#1e951f" StaticSelectedStyle-ForeColor="Black" StaticSelectedStyle-BorderStyle="Solid"
                                                                    StaticSelectedStyle-BorderWidth="1px" DynamicSelectedStyle-ForeColor="Black"
                                                                    StaticMenuItemStyle-Width="30px" StaticSelectedStyle-BorderColor="#A4D49A" runat="server" />
                                                            </td>
                                                            <td style="width: 6%;">of
                                                                    <%=gvEmployees.PageCount%>
                                                            </td>
                                                            <td style="text-align: left; width: 6%">
                                                                <asp:ImageButton ID="imgNext" runat="server" CausesValidation="false" CommandName="Page"
                                                                    CommandArgument="Next" ToolTip="Next Page" ImageUrl="~/includes/img/arrow_r_multi.png" />
                                                                <asp:ImageButton ID="imgDoubleNext" runat="server" CausesValidation="false" CommandName="Page"
                                                                    CommandArgument='<%#gvEmployees.PageIndex + 6%>' ImageUrl="~/includes/img/arrow_r_one.png" />
                                                            </td>
                                                            <td style="font-family: Arial,Verdana,sans-serif; font-size: 1.2em; width: 7%;">
                                                                <td style="width: 90%; text-align: right; margin-left: 0px; padding-left: 0px;">
                                                                    <asp:Label ID="lblPage" runat="server" Text="Jump to Page:" ForeColor="Black" Font-Names="Arial,Verdana,sans-serif"
                                                                        Font-Size="11px"></asp:Label>
                                                                    <asp:TextBox ID="txtJmpToPage" runat="server" Width="30px" AutoPostBack="true" OnTextChanged="txtJmpToPage_TextChanged"></asp:TextBox>
                                                                </td>
                                                    </tr>
                                                </table>
                                            </PagerTemplate>
                                            <PagerSettings Position="TopAndBottom"></PagerSettings>
                                            <Columns>
                                                <asp:BoundField HeaderText="Last Name" DataField="LastName" SortExpression="LastName">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="30%" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="First Name" DataField="FirstName" SortExpression="FirstName">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="30%" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="ImageID" DataField="ImageID" SortExpression="Status"
                                                    Visible="false">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="30%" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Reference No" DataField="ReferenceNo" SortExpression="ReferenceNo"
                                                    Visible="True">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="30%" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="30%" />
                                                    <ItemTemplate>
                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <asp:ImageButton ID="EmpInfo" runat="server" Text="View" ImageUrl="~/includes/img/ds249_btn_view_static.png" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Left" />
                                            <HeaderStyle BackColor="#A4D49A" Font-Bold="True" />
                                            <AlternatingRowStyle BackColor="#ededed" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 900px" class="tableMargin" border="0" cellpadding="0px" cellspacing="0px"
                            align="center">
                            <tr>
                                <td>
                                    <asp:Image runat="server" SkinID="skidBottomCap" Style="width: 900px" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </center>
         
       
    </asp:Content>
