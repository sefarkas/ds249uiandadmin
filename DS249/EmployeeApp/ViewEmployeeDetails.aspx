﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewEmployeeDetails.aspx.cs" Inherits="DSNY.DSNYCP.DS249.EmplyeeApp_ViewEmployeeDetails" %>

<%@ Register src="~/UserControl/ImageControl.ascx" tagname="ImageControl" tagprefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    
    <title>Employee Information</title>
 <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />    
    
    <style type="text/css">
        .style2
        {
            width: 83px;
        }
        #mainiFrame
        {
            width: 379px;
            height: 385px;
        }
        .style3
        {
            width: 764px;
        }
    </style>
    
    
    
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="680px">
        <tr>
        <td  style="height:5px"></td>
        </tr>
            <tr>
                <td>
                    <asp:Image runat="server" SkinId="skidTopCap" style="width: 680px; height: 20px;" />
                    <asp:Label ID="lblMessage" runat="server" Width="50%" CssClass="feedbackStatus"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <table>
                        <tbody>
                            <tr valign="top">
                                <td>
                                    <table style="width: 350px">
                                        <tr>
                                            <td class="style2">
                                                <b>First Name: </b>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblFirstName"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="style2">
                                                <b>Last Name:</b>
                                            </td>
                                            <td>
                                                <asp:Label runat="server" ID="lblLastName"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style="width: 300px; height: 350px">
                                        <tr>
                                            <td>
                                                <div style="float: left; width: 300px; height: 350px;">
                                                    <iframe id="mainiFrame" name="mainiFrame" scrolling="no" frameborder="0" height="350px"
                                                        width="300" src="" runat="server"></iframe>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Image runat="server" SkinId="skidBottomCap" style="width: 680px; height: 20px;" />
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
