﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.Sql;
using System.Text;
using DSNY.DSNYCP.ClassHierarchy;
using AjaxControlToolkit;
using System.ComponentModel;
using System.Configuration;



namespace DSNY.DSNYCP.DS249
{

    public partial class DashBoard_EmployeeApp : System.Web.UI.Page
    {
        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
        PersonnelList personnelList;   
       
        protected void Page_Load(object sender, EventArgs e)
        {
                User user;
                btnSearch.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_search_roll.png'");
                btnSearch.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_search_static.png'");

                btnSearchCancel.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_cancel_roll.png'");
                btnSearchCancel.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_cancel.png'");

                user = (User)Session["clsUser"];
                if (user != null)
                {
                    if (!user.IsInMemberShip(GroupName.EMPLOYEE))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                    else
                    {
                        user.CurrentMembership = GroupName.EMPLOYEE;
                        SecurityUtility.AttachRolesToUser(user);
                    }

                    if (!User.IsInRole("Read"))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                }
                else
                    Response.Redirect("../UI/Login.aspx");

                if (Session["IsAuthenticated"] == null)
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }
                if (Session["IsAuthenticated"].ToString() == "false")
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }
                Page.Form.DefaultButton = btnSearch.UniqueID;

                CleanSession();
                DisplayButtonBasedOnPermission();
                lblMessage.Text = String.Empty;
                if (!IsPostBack)
                {
                    //set focus on search text box
                    Type cstype = this.GetType();
                    ClientScript.RegisterStartupScript(cstype,"SetFocus", "<script>document.getElementById('" + txtSearch.ClientID + "').focus();</script>");
                    GridPopulate();
                }        
            
        }
        private void CleanSession()
        {
           
                Session["Complaint"] = null;
                Session["Personnel"] = null;
                Session["witnessList"] = null;
                Session["ComplainantList"] = null;
                Session["voilationList"] = null;            
        }
        private void DisplayButtonBasedOnPermission()
        {
            if (!User.IsInRole("Write"))
               
            if (!User.IsInRole("Report"))
            {
               
            }
        }
        private void GridPopulate()
        {           
                gvEmployees.DataSource = MergeData();
                gvEmployees.DataBind();           
        }


        private void LoadPersonnels()
        {            
                personnelList = new PersonnelList();
                personnelList.LoadEmployees(true);            
        }

        private DataTable MergeData()
        {           
                LoadPersonnels();
                DataTable dt = new DataTable();
                dt.Columns.Add("FirstName", Type.GetType("System.String"));
                dt.Columns.Add("LastName", Type.GetType("System.String"));
                dt.Columns.Add("ImageID", Type.GetType("System.Int32"));
                dt.Columns.Add("ReferenceNo", Type.GetType("System.String"));
                DataRow dr;
                foreach (Personnel personnel in personnelList.List)                
                {
                    dr = dt.NewRow();                   
                    dr["FirstName"] = personnel.FirstName.ToString();
                    dr["LastName"] = personnel.LastName.ToString();
                    dr["ImageID"] = personnel.ImageId;
                    dr["ReferenceNo"] = personnel.ReferenceNo;
                    dt.Rows.Add(dr);
                }                
                return dt;      
           
        }

        protected void menuPager_MenuItemClick(object sender, MenuEventArgs e)
        {
              gvEmployees.PageIndex = Int32.Parse(e.Item.Value);                
              gvEmployees.DataSource = PopulateTable();
              gvEmployees.DataBind();
           
        }
        

        protected void txtJmpToPage_TextChanged(object sender, EventArgs e)
        {            
                GridViewRow rowTop = gvEmployees.TopPagerRow;
                TextBox txtJTPTop = (TextBox)rowTop.Cells[0].FindControl("txtJmpToPage");
                GridViewRow rowBottom = gvEmployees.BottomPagerRow;
                TextBox txtJTPBottom = (TextBox)rowBottom.Cells[0].FindControl("txtJmpToPage");
                if (txtJTPTop.Text.Trim().Length > 0)
                {
                    gvEmployees.PageIndex = Convert.ToInt32(txtJTPTop.Text.Trim()) - 1;
                    if (Session["DV"] != null)
                        gvEmployees.DataSource = (DataView)Session["DV"];
                    else
                        gvEmployees.DataSource = PopulateTable();
                    gvEmployees.DataBind();
                }
                else if (txtJTPBottom.Text.Trim().Length > 0)
                {
                    gvEmployees.PageIndex = Convert.ToInt32(txtJTPBottom.Text.Trim()) - 1;
                    if (Session["DV"] != null)
                        gvEmployees.DataSource = (DataView)Session["DV"];
                    else
                        gvEmployees.DataSource = PopulateTable();
                    gvEmployees.DataBind();
                }
                else
                {
                    return;
                }           

        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {           
                if (txtSearch.Text.Length > 0)
                {                  
                    SearchComplaints(true);
                }
                else
                {
                    SearchComplaints(false);
                }           
        }

        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
              if (txtSearch.Text.Length > 0)
                {                   
                    SearchComplaints(true);
                }
                else
                {
                    SearchComplaints(false);
                }           
        }

        private void SearchComplaints(bool Search)
        {            
                DataView dv;
                if (Session["DV"] == null)
                {
                    dv = new DataView(PopulateTable());
                }
                else
                    dv = (DataView)Session["DV"];

                if (Search == true)
                {
                    bool isDecimal;
                    decimal bal;
                    isDecimal = decimal.TryParse(txtSearch.Text.Trim(), out bal);

                    if (IsNumeric(txtSearch.Text.Trim()))
                    {
                        dv.RowFilter = String.Format("ReferenceNo like '%{0}%'", txtSearch.Text.Trim());
                    }                    
                    else
                        dv.RowFilter = GetSearchString();
                }
                else
                    dv.RowFilter = String.Empty;
                Session["DV"] = dv;
                DataTable dt_sort = new DataTable();
                dt_sort.Columns.Add("FirstName", Type.GetType("System.String"));
                dt_sort.Columns.Add("LastName", Type.GetType("System.String"));
                dt_sort.Columns.Add("ImageID", Type.GetType("System.Int32"));
                dt_sort.Columns.Add("ReferenceNo", Type.GetType("System.String"));

                DataRow dr;
                foreach (DataRowView drv in dv)
                {
                    dr = dt_sort.NewRow();
                    dr["FirstName"]=drv.Row[0].ToString();
                dr["LastName"]=drv.Row[1].ToString();
                dr["ImageID"] = Convert.ToInt32(drv.Row[2]);
                dr["ReferenceNo"] = Convert.ToInt32(drv.Row[3]);
                dt_sort.Rows.Add(dr);
                }
                Session["DV_Sort"] = dt_sort;
                gvEmployees.DataSource = dv;
                gvEmployees.DataBind();            
        }

        private DataTable PopulateTable()
        {           
                DataTable dt;       
               
                if (Session["DT_Employees"] != null)
                    dt = (DataTable)Session["DT_Employees"];
                else if (Session["DV_Sort"] != null)
                {
                    dt = (DataTable)Session["DV_Sort"];
                }
                else
                    dt = MergeData();
                return dt;           
        }
        private bool IsNumeric(string strTextEntry)
        {            
                Regex objNotWholePattern = new Regex("[^0-9]");
                return !objNotWholePattern.IsMatch(strTextEntry);           
        }

        private String GetSearchString()
        {           
                StringBuilder searchString = new StringBuilder();
                searchString.Append(String.Format("FirstName Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("LastName Like '%{0}%'", txtSearch.Text.Trim()));
                return searchString.ToString();           
        }

        protected void gvEmployees_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    ImageButton hp = (ImageButton)e.Row.Cells[4].FindControl("EmpInfo");                 
                    string onclick = "javascript:window.open('ViewEmployeeDetails.aspx?Id=" + e.Row.Cells[3].Text + "','mywindow','width=680','height=200');";
                    hp.Attributes.Add("onclick", onclick);                
                    ImageButton View = (ImageButton)e.Row.FindControl("EmpInfo");
                    View.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_view_roll.png'");
                    View.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_view_static.png'");                   
                }
            
        }

        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }
        protected void gvEmployees_Sorting(object sender, GridViewSortEventArgs e)
        {          
                string sortExpression = e.SortExpression;
                if (GridViewSortDirection == SortDirection.Descending)
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, ASCENDING);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, DESCENDING);
                }           

        }
        private void SortGridView(string sortExpression, string direction)
        {          
                DataView dv;
            dv = new DataView(PopulateTable())
            {
                Sort = sortExpression + direction
            };
            Session["DV"] = dv;
                gvEmployees.DataSource = dv;
                gvEmployees.DataBind();            
        }

        protected void gvEmployees_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {          
                if (e.NewPageIndex >= 0)
                {
                    gvEmployees.PageIndex = e.NewPageIndex;

                    if (Session["DV"] != null)
                        gvEmployees.DataSource = (DataView)Session["DV"];
                    else
                        gvEmployees.DataSource = PopulateTable();
                    gvEmployees.DataBind();
                }
                else if ((e.NewPageIndex == -1) || (e.NewPageIndex == -2) || (e.NewPageIndex == -3) || (e.NewPageIndex == -4) || (e.NewPageIndex == -5))
                {
                    gvEmployees.PageIndex = 0;
                    if (Session["DV"] != null)
                        gvEmployees.DataSource = (DataView)Session["DV"];
                    else
                        gvEmployees.DataSource = PopulateTable();
                    gvEmployees.DataBind();
                }
                else
                {
                    return;
                }           
        }

        protected void btnSearchCancel_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }

        protected void gvEmployees_DataBound(object sender, EventArgs e)
        {           
                Menu menuPagerTop = (Menu)gvEmployees.TopPagerRow.FindControl("menuPager");
                Menu menuPagerBottom = (Menu)gvEmployees.BottomPagerRow.FindControl("menuPager");
                if ((gvEmployees.PageCount - gvEmployees.PageIndex) >= 5)
                {
                    for (int i = gvEmployees.PageIndex; i < gvEmployees.PageIndex + 5; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvEmployees.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }
                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);
                    }
                }
                else if ((gvEmployees.PageCount - gvEmployees.PageIndex) == 4)
                {
                    int index = 0;
                    if (gvEmployees.PageIndex > 4)
                    {
                        index = gvEmployees.PageIndex - 5;
                    }
                    for (int i = index; i < gvEmployees.PageIndex + 4; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvEmployees.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }
                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);

                    }
                }
                else if ((gvEmployees.PageCount - gvEmployees.PageIndex) == 3)
                {
                    int index = 0;
                    if (gvEmployees.PageIndex > 3)
                    {
                        index = gvEmployees.PageIndex - 4;
                    }
                    for (int i = gvEmployees.PageIndex; i < gvEmployees.PageIndex + 3; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvEmployees.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }
                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);
                    }
                }
                else if ((gvEmployees.PageCount - gvEmployees.PageIndex) == 2)
                {
                    int index = 0;
                    if (gvEmployees.PageIndex > 2)
                    {
                        index = gvEmployees.PageIndex - 3;
                    }
                    for (int i = index; i < gvEmployees.PageIndex + 2; i++)                 
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvEmployees.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }
                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);
                    }

                }
                else if ((gvEmployees.PageCount - gvEmployees.PageIndex) == 1)
                {
                    int index = 0;
                    if (gvEmployees.PageIndex > 1)
                    {
                        index = gvEmployees.PageIndex - 2;
                    }

                    for (int i = index; i < gvEmployees.PageIndex + 1; i++)
                    {
                        MenuItem itemTop = new MenuItem();
                        MenuItem itemBottom = new MenuItem();
                        itemTop.Text = String.Format("{0}", i + 1);
                        itemBottom.Text = String.Format("{0}", i + 1);
                        itemTop.Value = i.ToString();
                        itemBottom.Value = i.ToString();
                        if (gvEmployees.PageIndex == i)
                        {
                            itemTop.Selected = true;
                            itemBottom.Selected = true;
                        }
                        menuPagerTop.Items.Add(itemTop);
                        menuPagerBottom.Items.Add(itemBottom);
                    }

                }
                else
                {
                    return;
                }

                if (gvEmployees.PageIndex == gvEmployees.PageCount - 1)
                {
                    ImageButton imgDbleNxt = (ImageButton)gvEmployees.TopPagerRow.FindControl("imgDoubleNext");
                    ImageButton imgNxt = (ImageButton)gvEmployees.TopPagerRow.FindControl("imgNext");
                    imgDbleNxt.Enabled = false;
                    imgNxt.Enabled = false;
                    imgNxt.ToolTip = "";

                }
                else if (gvEmployees.PageIndex == 0)
                {
                    ImageButton imgDblePrv = (ImageButton)gvEmployees.TopPagerRow.FindControl("imgDoublePrevious");
                    ImageButton imgPrv = (ImageButton)gvEmployees.TopPagerRow.FindControl("imgPrevious");
                    imgDblePrv.Enabled = false;
                    imgPrv.Enabled = false;
                    imgPrv.ToolTip = "";

                }
                else
                {
                    return;
                }
            
        }

    }

    }