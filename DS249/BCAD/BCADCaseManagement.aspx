<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.DS249.BCAD_BCADCaseManagement" CodeBehind="BCADCaseManagement.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="~/UserControl/BCADPenaltyControl.ascx" TagName="BCADPenalty" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        //function OpenPrint(IndexNo) {
        //    if (IndexNo != "") {
        //        var URL = "Print.aspx?Id=" + IndexNo;
        //        var dialogResults = window.open(URL, '');
        //    }
        //    else {
        //        alert("ComplaintID is Blank Or Missing");
        //    }
        //    return false;
        //}

        function OpenPrint(IndexNo) {
            if (IndexNo != "") {
                var URL = "Print.aspx?Id=" + IndexNo;
                var dialogResults = window.open(URL, '');
            }
            else {
                alert("ComplaintID is Blank Or Missing");
            }
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <table id="caseManagementTable" class="tableCase" style="height: 30px">
            <tbody>
                <tr>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="BulletList"
                                        ShowMessageBox="true" ShowSummary="false" EnableClientScript="true" Enabled="true" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="styleHeading">
                        <div>
                            <span>BCAD CASE MANAGEMENT</span></div>
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lbMessage" runat="server" CssClass="feedbackStatus"></asp:Label>
                                <asp:Image runat="server" SkinId="skidClear"  height="15" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                            runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                            <animations>                                
                                                <OnUpdated>
                                                    <FadeOut Duration="5.0" Fps="24" />
                                                </OnUpdated>
                            </animations>
                        </cc1:UpdatePanelAnimationExtender>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="tableCase" id="searchTable">
            <tr>
                <td>
                    <asp:ImageButton ID="btnCaseList" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                        runat="server" CausesValidation="false" OnClick="btnCaseList_Click" />
                    <asp:Image runat="server" SkinId="skidClear" width="25" />
                    <asp:ImageButton ID="ibPrint" runat="server" ImageUrl="~/includes/img/ds249_btn_print_static.png"
                        CausesValidation="false" />
                    <asp:Image runat="server" ImageUrl="~/includes/img/ds249_btn_approve_static.png" style="visibility: hidden;" />
                    <asp:Image runat="server" SkinId="skidClear" width="25" />
                    <asp:Image runat="server" ImageUrl="~/includes/img/ds249_btn_route_static.png" style="visibility: hidden;" />
                    <asp:Image runat="server" SkinId="skidClear" width="25" />
                    <asp:ImageButton ID="btnSave" ImageUrl="~/includes/img/ds249_btn_save_static.png"
                        runat="server" OnClick="btnSave_Click" />
                </td>
            </tr>
        </table>
        <asp:UpdatePanel ID="upComplaint" runat="server">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <table class="tableCase" id="tblCase1" border="0" cellspacing="0px" cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdCase">
                                &nbsp;
                            </td>
                            <td class="tdCreatDate">
                                <table id="tblCreateDateId">
                                    <tr>
                                        <td class="tdLabeling">
                                            DATE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtCreatDt" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdIdxNo">
                                <table id="tblIdxNo">
                                    <tr>
                                        <td class="tdLabeling">
                                            INDEX NO
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtIdxNo" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdPayLoc">
                                <table id="tblPayLoc">
                                    <tr>
                                        <td class="tdLabeling">
                                            PAYROLL LOCATION
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtPayLoc" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table class="tableCase" id="tblCase2" border="0" cellspacing="0px" cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdRespDistrict">
                                <table id="tblCaseStatus">
                                    <tr>
                                        <td class="tdLabeling">
                                            CASE STATUS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlCaseStatus" runat="server" Font-Size="0.9em" Width="200px"
                                                Font-Names="Arial" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCaseStatus_SelectedIndexChanged">
                                                <asp:ListItem Value="0">[SELECT]</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdDivision">
                                <table id="tblDivision">
                                    <tr>
                                        <td class="tdLabeling">
                                            DISTRICT/DIVISION
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtDivision" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdBorough">
                                <table id="tblBorough">
                                    <tr>
                                        <td class="tdLabeling">
                                            BOROUGH
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtBorough" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdBadge">
                                <table id="tblBadge">
                                    <tr>
                                        <td class="tdLabeling">
                                            BADGE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtBadge" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblRespondentDetails1" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdRespDetails">
                                &nbsp;
                            </td>
                            <td class="tdRespName">
                                <table id="tblRespName">
                                    <tr>
                                        <td class="tdLabeling">
                                            RESPONDENT NAME
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRespName" runat="server" Width="430px" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRespTitle">
                                <table id="tblRespTitle">
                                    <tr>
                                        <td class="tdLabeling">
                                            TITLE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRespTitle" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblRespondentDetails2" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdRespRefNo">
                                <table id="tblRespRefNo">
                                    <tr>
                                        <td class="tdLabeling">
                                            REFERENCE NO.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRespRefNo" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRespSSNo">
                                <table id="tblRespSocialSecurity">
                                    <tr>
                                        <td class="tdLabeling">
                                            SOCIAL SECURITY NUMBER
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRespSSNo" runat="server" Font-Size="0.9em" Font-Names="Arial"
                                                MaxLength="9" Width="200px" CssClass="txtBox" TextMode="Password" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRespAppoint">
                                <table id="tblRespAppoint">
                                    <tr>
                                        <td class="tdLabeling">
                                            APPOINTMENT DATE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRespAppointment" runat="server" CssClass="txtBox" Width="200px"
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRespAppoint">
                                <table id="Table1">
                                    <tr>
                                        <td nowrap="nowrap">
                                            <asp:CheckBox ID="chkProbation" runat="server" Enabled="false" SkinID="" Font-Bold="true" />Employee
                                            Under Probation
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblRespondentDetails3" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdGnrlCmts">
                                <table id="tblGnrlCmts">
                                    <tr>
                                        <td class="tdLabeling">
                                            GENERAL COMMENTS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtGnrlCmts" runat="server" CssClass="txtBox" Width="430px" Height="41px"
                                                TextMode="MultiLine" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRespVacSch">
                                <table id="tblRespVacSch">
                                    <tr>
                                        <td class="tdLabeling">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtRespVacSch" runat="server" Width="200px" CssClass="txtBox" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdRespChartDay">
                                <table id="tblRespChartDay">
                                    <tr>
                                        <td class="tdLabeling">
                                            CHART DAY NO.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtChartDayNo" runat="server" CssClass="txtBox" Width="200px" ReadOnly="true"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="ttblRespondentDetails4" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdRulesViolated">
                                <table id="tblRulesViolated">
                                    <tr>
                                        <td class="tdLabeling">
                                            RULES AND ORDERS VIOLATED AS FOLLOWS
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptViolations" runat="server">
                                                <HeaderTemplate>
                                                    <table width="100%">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td style="width: 50%;" align="left">
                                                            <asp:TextBox ID="TextBox1" runat="server" CssClass="txtBox" Text='<%#Bind("ViolationDescription")%>'
                                                                Width="450px" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 30%;" align="left">
                                                            <asp:TextBox ID="TextBox2" runat="server" CssClass="txtBox" Text='<%#Bind("IncidentLocationName")%>'
                                                                Width="222px" ReadOnly="true"></asp:TextBox>
                                                            <asp:Label ID="Label2" runat="server"></asp:Label>
                                                        </td>
                                                        <td style="width: 20%;" align="left">
                                                            <asp:TextBox ID="TextBox3" runat="server" CssClass="txtBox" Text='<%#Bind("ParseIncidentDateAsString")%>'
                                                                Width="210px" ReadOnly="true"> </asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                    </table>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblComplaintDetails1" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdComplainantName">
                                <table id="tblComplainantName">
                                    <tr>
                                        <td class="tdLabeling">
                                            COMPLAINANT NAME
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptCompName" runat="server">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCompName" runat="server" Width="350px" CssClass="txtBox" Text='<%#Bind("complainantName")%>'
                                                        ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdComplainantTitle">
                                <table id="tblComplainantTitle">
                                    <tr>
                                        <td class="tdLabeling">
                                            TITLE
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptCompTitle" runat="server">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCompTitle" runat="server" CssClass="txtBox" Text='<%#Bind("title")%>'
                                                        ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdComplainantDistrict">
                                <table id="tblComplainantDistrict">
                                    <tr>
                                        <td class="tdLabeling">
                                            DISTRICT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptCompDistrict" runat="server">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCompDist" runat="server" CssClass="txtBox" Text='<%#Bind("locationName")%>'
                                                        ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblComplaintDetails2" class="tableCase" border="0" cellspacing="0px"
                    cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdCompStmt">
                                <table id="tblCompStmt">
                                    <tr>
                                        <td class="tdLabeling">
                                            COMPLAINANT STATEMENT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Repeater ID="rptCompStmt" runat="server">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtCompStmt" runat="server" CssClass="txtBox" Height="80px" Width="900px"
                                                        TextMode="MultiLine" Text='<%#Bind("Statement")%>' ReadOnly="true"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblAdjudication" class="tableCase" border="0" cellspacing="0px" cellpadding="3px">
                    <tbody>
                        <tr>
                            <td class="tdAdjudication">
                                &nbsp;
                                <asp:HiddenField ID="hdnValue" runat="server" />
                                <asp:HiddenField ID="hdnText" runat="server" />
                            </td>
                            <td class="tdNonPltyRslt">
                                <table id="tblNonPltyRslt">
                                    <tr>
                                        <td class="tdLabeling">
                                            NON-PENALTY RESULT
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlNonPltyRslt" runat="server" Width="200px" AutoPostBack="true"
                                                AppendDataBoundItems="true" OnSelectedIndexChanged="ddlNonPltyRslt_SelectedIndexChanged">
                                                <asp:ListItem Value="0">[SELECT]</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="tdAdjudicationDate">
                                <table id="tblAdjudicationDate">
                                    <tr>
                                        <td class="tdLabeling">
                                            DATE
                                            <label style="font-size: 1.0em">
                                                (Month/Day/Year;xx/xx/xxxx)</label>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidatorForAdjudication" runat="server"
                                                Font-Size="0.4em" ErrorMessage="Invalid Adjudication Date" ControlToValidate="txtAdjudDate"
                                                ValidationExpression="^[0,1]?\d{1}\/(([0-2]?\d{1})|([3][0,1]{1}))\/(([1]{1}[9]{1}[9]{1}\d{1})|([2-9]{1}\d{3}))$"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtAdjudDate" runat="server" Width="180px"></asp:TextBox>
                                            <cc1:CalendarExtender ID="txtAdjudDate_CalendarExtender" runat="server" Enabled="True"
                                                TargetControlID="txtAdjudDate" PopupButtonID="imgAdjudCal" CssClass="MyCalendar">
                                            </cc1:CalendarExtender>
                                            <asp:Image ID="imgAdjudCal" AlternateText="Calender" ImageUrl="~/includes/img/ds249_mini_cal_btn.png"
                                                runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblPenalty1" class="tableCase" border="0" cellspacing="0px" cellpadding="0px">
                    <tbody>
                        <tr>
                            <td class="tdPenalty">
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblPenalty2" class="tableCase" border="0" cellspacing="0px" cellpadding="0px">
                    <tbody>
                        <tr>
                            <td class="tdAboutPenalty" valign="top">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:ImageButton ID="btnAddNewPenalty" ImageUrl="~/includes/img/addnew.gif"
                                                runat="server" CausesValidation="false" OnClick="btnAddNewPenalty_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="vertical-align: top">
                                                <asp:PlaceHolder ID="pnPenalty" runat="server"></asp:PlaceHolder>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table id="tblCancelSave" width="930px" style="margin-left: 30px; margin-right: 10px">
                    <tbody>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td style="width: 73%">
                                &nbsp;
                            </td>
                            <td>
                                <input id="IsDirty" name="hiddenIsDirty" value="false" type="hidden" />
                            </td>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </center>
</asp:Content>
