<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    Inherits="DSNY.DSNYCP.DS249.BCAD_DashBoard" CodeBehind="DashBoard.aspx.cs" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/Pager.ascx" TagName="Pager" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../includes/css/DSNYcss.css" rel="stylesheet" type="text/css" />
     <style type="text/css">
        
        .pnReports
        {
            border: #060F40 2px solid;
            color: #060F40;
            background: #ffffcc;
        }
       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <center>
        <asp:UpdatePanel ID="upButtons" runat="server">
            <ContentTemplate>
                <table width="955px" border="0" cellpadding="0px" cellspacing="0px" align="center">
                    <tr>
                        <td class="headertext" align="left">
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="82" />
                            BCAD
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="25" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Image runat="server" SkinId="skidClear" width="15" />
                            <asp:ImageButton ID="btnRef" ImageUrl="~/includes/img/ds249_btn_complaint.png"
                                runat="server" OnClick="btnRef_Click" CausesValidation="False" />
                            <asp:Image runat="server" SkinId="skidClear" height="10" width="606" />
                            <asp:ImageButton ID="btnReport" ImageUrl="~/includes/img/ds249_btn_report_static.png"
                                runat="server" CausesValidation="False" OnClientClick="JavaScript:return false;"
                                OnClick="btnReport_Click" Visible="true" />
                            <cc1:PopupControlExtender ID="ibReports_PopupControlExtender" runat="server" 
                                Enabled="True" ExtenderControlID="" PopupControlID="pnReportsMenu" Position="Bottom"
                                TargetControlID="btnReport">
                            </cc1:PopupControlExtender>
                        </td>
                    </tr>
                </table>
                <asp:Image runat="server" SkinId="skidClear" height="10" width="25" />
                <table width="955px" border="0" cellpadding="0px" cellspacing="0px">
                    <tr>
                        <td width="33%">
                        </td>
                        <td class="searchCase" align="left">
                            SEARCH FOR COMPLAINTS&nbsp;&nbsp;
                        </td>
                        <td class="style1" align="left">
                            <asp:TextBox ID="txtSearch" runat="server" Width="172px" Height="17px" CssClass="inputFieldStyle"></asp:TextBox>
                        </td>
                        <td>
                            <asp:ImageButton ID="btnSearch" ImageUrl="~/includes/img/ds249_btn_search_static.png"
                                runat="server" OnClick="btnSearch_Click" />
                            <asp:ImageButton ID="btnSearchCancel" ImageUrl="~/includes/img/btn_cancel.gif"
                                runat="server" OnClick="btnSearchCancel_Click" CausesValidation="False" Visible="false" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table style="width: 832px" border="0" cellpadding="0px" cellspacing="0px" align="center">
            <tr>
                <td colspan="2" align="center">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <asp:Label ID="lbMessage" CssClass="feedbackStatus" runat="server" Width="100%" Font-Size="Small"
                                Font-Bold="True"></asp:Label>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <cc1:UpdatePanelAnimationExtender ID="UpdatePanel1_UpdatePanelAnimationExtender"
                        runat="server" Enabled="True" TargetControlID="UpdatePanel1">
                        <Animations>                                
                        <OnUpdated>
                            <FadeOut Duration="5.0" Fps="24" />
                        </OnUpdated>
                        </Animations>
                    </cc1:UpdatePanelAnimationExtender>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="832" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                            <td>
                                <asp:Image runat="server" SkinId="skidTopCap" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel ID="upGrid" runat="server">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="btnSearch" EventName="Click" />
                                        <asp:AsyncPostBackTrigger ControlID="btnRef" EventName="Click" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <uc1:Pager ID="Pager1" runat="server" />
                                        <asp:GridView ID="gvComplaints" runat="server" Width="832px" AutoGenerateColumns="False"
                                            BorderColor="#235705" HeaderStyle-Font-Bold="true" HeaderStyle-BackColor="#a4d49a"
                                            AllowSorting="True" PageSize="50" AllowPaging="True" OnRowCommand="gvComplaints_RowCommand"
                                            OnRowDataBound="gvComplaints_RowDataBound" OnSorting="gvComplaints_Sorting" OnDataBound="gvComplaints_DataBound"
                                            AlternatingRowStyle-BackColor="#ededed" EmptyDataText="Sorry. No Complaints Found.">
                                            <EmptyDataRowStyle CssClass="fromdate" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <div align="center">
                                                            <asp:ImageButton ID="btnOpen" ImageUrl="~/includes/img/ds249_btn_dash_open_static.png"
                                                                runat="server" CommandName="Open" Visible="false" CommandArgument='<%#Eval("ComplaintId") %>'
                                                                OnClientClick="javascript:window.scrollTo(0,0); return confirm('Do you want to open the case?');" />
                                                            <asp:ImageButton ID="btnEdit" ImageUrl="~/includes/img/ds249_btn_edit_static.png"
                                                                runat="server" CommandName="Edit" Visible="false" CommandArgument='<%#Eval("ComplaintId") %>'
                                                                OnClientClick="javascript:window.scrollTo(0,0); return confirm('Do you want to edit the case?');" />
                                                            <asp:ImageButton ID="btnView" ImageUrl="~/includes/img/ds249_btn_view_static.png"
                                                                runat="server" CommandName="View" Visible="false" CommandArgument='<%#Eval("ComplaintId") %>' />
                                                            <asp:Label ID="lbAdvocate" Visible="false" runat="server" Text="Restricted" Class="dashLineStyle"></asp:Label>
                                                        </div>
                                                    </ItemTemplate>
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="100px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:TemplateField>
                                                <asp:BoundField HeaderText="Date Routed" DataField="ComplaintStatusDate" SortExpression="ComplaintStatusDate"
                                                    DataFormatString="{0:MMM dd, yyyy}" HtmlEncode="false">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="100px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Incident Date" DataField="IncidentDate" SortExpression="IncidentDate"
                                                    DataFormatString="{0:MMM dd, yyyy}" HtmlEncode="false">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="130px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Index #" DataField="IndexNo" SortExpression="IndexNo">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="80px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Status" DataField="Status" SortExpression="Status">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="80px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="District" DataField="LocationName" SortExpression="LocationName">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="200px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Name (F. Last)" DataField="FirstName" SortExpression="LastName">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="120px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Charges" DataField="Charge" SortExpression="Charge">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="90px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="NAME (F. Last)" DataField="LastName" SortExpression="LastName">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="125px" />
                                                    <ItemStyle CssClass="dashLineStyle" />
                                                </asp:BoundField>
                                                <asp:BoundField HeaderText="Reference #" DataField="RefNo" SortExpression="RefNo">
                                                    <HeaderStyle CssClass="dashHeaderTxt" ForeColor="White" Font-Underline="False" Wrap="False"
                                                        Width="100px" />
                                                    <ItemStyle CssClass="dashRefLineStyle" />
                                                </asp:BoundField>
                                            </Columns>
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Image runat="server" SkinId="skidBottomCap" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

    </center>
    <asp:Panel ID="pnReportsMenu" runat="server" CssClass="pnReports">
        <table>
            <tr align="left">
                <td align="left">
                    <a href="../Reports/BCADReports.aspx?Id=1">BCAD No Disposition Complaints</a>
                </td>
            </tr>

            <tr align="left">
                <td align="left">
                    <a href="../Reports/BCADReports.aspx?Id=2">Probation Employees</a>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
