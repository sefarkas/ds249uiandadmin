﻿using System.Collections.Generic;
using System;
using System.Collections;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text;
using System.Net;
using System.Xml;
using DSNY.DSNYCP.ClassHierarchy;

namespace DSNY.DSNYCP.DS249
{
    /// <summary>
    /// This class provides the functionality for events and methods for the BCAD case management.
    /// </summary>
    public partial class BCAD_BCADCaseManagement : System.Web.UI.Page
    {

        #region Page Variables

        CodeList codeList;     
        ComplainantList complainantList;       
        Complaint complaint;
        BCADPenaltyList bcadPenaltyList;
        BCADComplaint bcadComplaint;
        UserControl_BCADPenaltyControl PenaltyControl;
        User user;
        LocationList boroughList;

        #endregion
        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
                ibPrint.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_print_roll.png'");
                ibPrint.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_print_static.png'");

                btnCaseList.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
                btnCaseList.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");

                btnSave.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_save_roll.png'");
                btnSave.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_save_static.png'");

                user = (User)Session["clsUser"];
                if (user != null)
                {
                    if (!user.IsInMemberShip(GroupName.BCAD))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                    else
                    {
                        user.CurrentMembership = GroupName.BCAD;
                        SecurityUtility.AttachRolesToUser(user);
                    }

                    if (!User.IsInRole("Read"))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                }
                else
                    Response.Redirect("../UI/Login.aspx");

                DisplayButtonBasedOnPermission();
                initializeUserControls();
                if (!IsPostBack)
                {
                    LoadDropDownLists();
                    LoadComplaintData();
                }

                if (Session["Mode"].ToString() == "EDIT" || Session["MODE"].ToString() == "VIEW")
                {
                    ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
                }
                if (Session["MODE"].ToString() == "VIEW")
                {
                    //***Changes*** 2.0.9 AFTER
                    ChangeControlStatus(true);
                    ddlCaseStatus.Enabled = true;
                }            
        }


        #region Private Methods

        #region UserControl
        /// <summary>
        /// This method initialises the user control
        /// </summary>
        private void initializeUserControls()
        {           
                //Initialize Witness User Control.
                initializePenaltyControl();            
        }
        /// <summary>
        /// This method initialises the user control
        /// </summary>
        private void initializePenaltyControl()
        {           
                if (Session["bcadPenaltyList"] == null)
                {
                    bcadPenaltyList = new BCADPenaltyList();
                    Session["bcadPenaltyList"] = bcadPenaltyList;
                    bcadPenaltyList.AddBcadPenalty();
                }
                else
                    bcadPenaltyList = (BCADPenaltyList)Session["bcadPenaltyList"];

                if (bcadPenaltyList.PenaltyList.Count == 0)
                {
                    bcadPenaltyList = new BCADPenaltyList();
                    Session["bcadPenaltyList"] = bcadPenaltyList;
                    bcadPenaltyList.AddBcadPenalty();
                }
                CreatePenaltyControl();           
        }
        /// <summary>
        /// Add penalty object in the list.
        /// </summary>
        private void AddPenaltyControl()
        {           
                bcadPenaltyList.AddBcadPenalty();
                CreatePenaltyControl();           
        }
        /// <summary>
        /// Create list of controls from the penalty list in the user control 
        /// </summary>
        private void CreatePenaltyControl()
        {            
                int i = 1;
                pnPenalty.Controls.Clear();
                foreach (BcadPenalty bcadPenalty in bcadPenaltyList.PenaltyList)
                {
                    PenaltyControl = (UserControl_BCADPenaltyControl)LoadControl("~/UserControl/BCADPenaltyControl.ascx");
                    PenaltyControl.ID = "ucPenalty" + bcadPenalty.UniqueId;
                    PenaltyControl.ControlLocation = UserControl_BCADPenaltyControl.ControlType.BCAD;
                    if (i > 1)
                    {
                        ((ImageButton)PenaltyControl.FindControl("btnRemovePenalty")).Visible = true;
                    }
                    PenaltyControl.RemoveUserControl += this.HandleRemovePenaltyControl;
                    PopulatePenaltyData(bcadPenalty, PenaltyControl);

                    pnPenalty.Controls.Add(PenaltyControl);
                    i++;
                }           
        }
        /// <summary>
        /// Bind the BCAD penalty values to the user control 
        /// </summary>
        /// <param name="bcadPenalty"></param>
        /// <param name="ctrPenalty"></param>
        private void PopulatePenaltyData(BcadPenalty bcadPenalty, Control ctrPenalty)
        {           
                ((HiddenField)ctrPenalty.FindControl("hdnUniqueID")).Value = bcadPenalty.UniqueId.ToString();
                ((TextBox)ctrPenalty.FindControl("txtPltyDays")).Text = bcadPenalty.Days.ToString();
                ((TextBox)ctrPenalty.FindControl("txtPltyHours")).Text = bcadPenalty.Hours.ToString();
                ((TextBox)ctrPenalty.FindControl("txtPltyMnts")).Text = bcadPenalty.Minutes.ToString();
                ((TextBox)ctrPenalty.FindControl("txtMonetary")).Text = bcadPenalty.MoneyValue.ToString();
                ((TextBox)ctrPenalty.FindControl("txtPltyNotes")).Text = bcadPenalty.Notes.ToString();           
        }

        /// <summary>
        /// This handles delete event fired from the user control 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void HandleRemovePenaltyControl(object sender, EventArgs e)
        {           
                ImageButton but = sender as ImageButton;
                UserControl_BCADPenaltyControl PenaltyControl = (UserControl_BCADPenaltyControl)but.Parent;
                Int64 UniqueID = Convert.ToInt64((((HiddenField)PenaltyControl.FindControl("hdnUniqueID")).Value));
                bcadPenaltyList.RemoveBCADPenalty(UniqueID);
                pnPenalty.Controls.Remove(PenaltyControl);           
        }

        #endregion
        /// <summary>
        /// Display the permisions based on the membership roles
        /// </summary>
        private void DisplayButtonBasedOnPermission()
        {
            if (!User.IsInRole("Write"))
            {
                btnSave.Visible = false;
            }
            if (!User.IsInRole("Print"))
            {
                ibPrint.Visible = false;
            }

        }
        /// <summary>
        /// This method saves the BCAD complaints to the database
        /// </summary>
        private void SaveBCADComplaint()
        {
                    Type cstype = this.GetType(); 
            if (bcadComplaint == null)
                    bcadComplaint = (BCADComplaint)Session["BCADComplaint"];
                if (ddlCaseStatus.SelectedItem.Value == "131")
                {
                    bcadComplaint.ReturnToAuthor(user.PersonalId);

                    ClientScript.RegisterStartupScript(cstype, @"message", @"<script>alert('Case Returned to Author!');document.location ='Dashboard.aspx';</script>");
                   //Page.ClientScript.RegisterClientScriptBlock(typeof(Page),@"message", @"<script>alert('Case Returned to Author!');document.location ='Dashboard.aspx';</script>");

                }

                else if (ddlCaseStatus.SelectedItem.Value == "132")
                {
                    bcadComplaint.ReturnToAdvocate(user.PersonalId);
                    ClientScript.RegisterStartupScript(cstype, @"message", @"<script>alert('Case Sent to Advocacy!');document.location ='Dashboard.aspx';</script>");
                }
                else
                {
                    bcadComplaint.FinalStatusCD = Convert.ToInt16(ddlCaseStatus.SelectedItem.Value.ToString());
                    if (txtAdjudDate.Text != String.Empty)
                        bcadComplaint.AdjudicationDate = Convert.ToDateTime(txtAdjudDate.Text);                    
                    bcadComplaint.NonPenaltyResultCD = Convert.ToInt16(ddlNonPltyRslt.SelectedItem.Value.ToString());
                    bcadComplaint.ModifiedUser = Convert.ToInt16(Session["UserEmpID"]);
                    bcadComplaint.ModifiedDate = DateTime.Now;
                    bcadComplaint.Penalty = bcadPenaltyList;
                    if (bcadComplaint.Save())
                    {
                        String script = "<script>alert('Case Saved Successfully!'); document.location ='Dashboard.aspx';</script>";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "message", script, false);
                        ibPrint.Attributes.Add("onclick", String.Format("return OpenPrint({0})", Request.QueryString["Id"].ToString()));
                    }
                    else
                    {
                        lbMessage.Text = "Error Occured. Unable to Save Complaint Data";
                        lbMessage.ForeColor = System.Drawing.Color.Red;
                    }
                }        

        }
        /// <summary>
        /// Loads the complaint data to the form
        /// </summary>
        private void LoadComplaintData()
        {
            
                Int64 ID = Convert.ToInt64(Request.QueryString["Id"]);
                complaint = new Complaint();
                complaint.Load(ID, RequestSource.BCAD);
                if (complaint.ComplaintId != -1)
                {
                    BindComplaintData();
                    Session["BCADComplaint"] = complaint.BcadComplaint;
                    bcadPenaltyList = new BCADPenaltyList();
                    bcadPenaltyList = complaint.BcadComplaint.Penalty;
                    bcadPenaltyList.NewUniqueId = Convert.ToInt16(bcadPenaltyList.PenaltyList.Count);
                    Session["bcadPenaltyList"] = bcadPenaltyList;
                    BindBCADComplaint();
                    BindPersonnelData();
                    complainantList = new ComplainantList();
                    complainantList = complaint.Complainants;
                    BindComplaintantData();
                    BindVoilations();
                    initializePenaltyControl();
                }
                else
                {
                    Response.Redirect("Dashboard.aspx");
                }
           
        }
        /// <summary>
        /// This method binds the BCAD complaint data to the form
        /// </summary>
        private void BindBCADComplaint()
        {
            bcadComplaint = complaint.BcadComplaint;
            if (ddlCaseStatus.Items.IndexOf(ddlCaseStatus.Items.FindByValue(bcadComplaint.FinalStatusCD.ToString())) != -1) 
            ddlCaseStatus.Items.FindByValue(bcadComplaint.FinalStatusCD.ToString()).Selected = true;
            hdnValue.Value = ddlCaseStatus.SelectedItem.Value;
            hdnText.Value = ddlCaseStatus.SelectedItem.ToString();           
            if (ddlNonPltyRslt.Items.IndexOf(ddlNonPltyRslt.Items.FindByValue(bcadComplaint.NonPenaltyResultCD.ToString())) != -1) 
            ddlNonPltyRslt.Items.FindByValue(bcadComplaint.NonPenaltyResultCD.ToString()).Selected = true;
            if (bcadComplaint.AdjudicationDate != DateTime.MinValue)
                txtAdjudDate.Text = bcadComplaint.AdjudicationDate.ToShortDateString();
            chkProbation.Checked = bcadComplaint.IsProbation;
        }
        /// <summary>
        /// This method binds the voilation data to the form
        /// </summary>
        private void BindVoilations()
        {           
                rptViolations.DataSource = complaint.Violations.List;
                rptViolations.DataBind();           
        }
        /// <summary>
        /// This method binds the complaint data to the form
        /// </summary>
        private void BindComplaintantData()
        {            
                rptCompName.DataSource = complainantList.List;
                rptCompName.DataBind();
                rptCompTitle.DataSource = complainantList.List;
                rptCompTitle.DataBind();
                rptCompDistrict.DataSource = complainantList.List;
                rptCompDistrict.DataBind();
                rptCompStmt.DataSource = complainantList.List;
                rptCompStmt.DataBind();           
        }
        /// <summary>
        /// This method binds the complaint data to the form
        /// </summary>
        private void BindComplaintData()
        {            
                txtCreatDt.Text = complaint.CreatedDate.ToShortDateString();
                txtIdxNo.Text = complaint.IndexNo.ToString();
                txtGnrlCmts.Text = complaint.Comments.ToString();
                txtDivision.Text = complaint.LocationName.ToString();
                LoadBoroughByLocation();           
        }
        /// <summary>
        /// This method loads the list of Borough w.r.t location
        /// </summary>
        private void LoadBoroughByLocation()
        {           
                boroughList = new LocationList();
                boroughList.Load(true);
                LocationDS borough = boroughList.List.Find(delegate(LocationDS q) { return q.LocationId == Convert.ToInt64(complaint.BoroughId); });
                if (borough != null)
                {
                    String Borough = borough.LocationName.ToString();
                    txtBorough.Text = Borough;
                }
                else
                    txtBorough.Text = "";           
        }
        /// <summary>
        /// This method binds the personnel data to the form
        /// </summary>
        private void BindPersonnelData()
        {           
                txtRespName.Text = complaint.FullName.ToString();
                txtRespRefNo.Text = complaint.ReferenceNumber.ToString();
                if (complaint.RespondentAptDate != DateTime.MinValue)
                    txtRespAppointment.Text = complaint.RespondentAptDate.ToShortDateString();
                txtRespVacSch.Text = complaint.VacationSchedule.ToString();
                txtChartDayNo.Text = complaint.ChartNo.ToString();
                txtBadge.Text = complaint.BadgeNumber.ToString();
                txtRespTitle.Text = complaint.TitleDescription.ToString();
                txtPayLoc.Text = complaint.PayCodeDescription.ToString();                     
        }
        /// <summary>
        /// This method loads the drop down list values
        /// </summary>
        private void LoadDropDownLists()
        {
            LoadBCADCaseStatus();
            LoadNonPenaltyResults();
            LoadAdjudicationBody();
        }
        /// <summary>
        /// This method Load Non-Penalty Results to the form
        /// </summary>
        private void LoadNonPenaltyResults()
        {         
                codeList = new CodeList();
                codeList.Load(18);   
                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                if (code.CodeId.ToString() != "135" && code.CodeName.ToString() != "WARN LETTER")
                        ddlNonPltyRslt.Items.Add(item);
                }          
        }
        /// <summary>
        /// This method Load Adjudication Body to the form
        /// </summary>
        private void LoadAdjudicationBody()
        {          
                codeList = new CodeList();
                codeList.Load(19);
                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
            }            
        }
        /// <summary>
        /// This method Load BCAD Case Status
        /// </summary>
        private void LoadBCADCaseStatus()
        {             codeList = new CodeList();
                codeList.Load();                
                System.Web.UI.WebControls.ListItem item;
                foreach (Code code in codeList.CList)
                {
                item = new System.Web.UI.WebControls.ListItem
                {
                    Value = code.CodeId.ToString(),
                    Text = code.CodeName.ToString()
                };
                ddlCaseStatus.Items.Add(item);
                }            
        }
        /// <summary>
        /// This method returns true if the controls in the form are enabled based on the membership roles
        /// </summary>
        /// <param name="status"></param>
        private void ChangeControlStatus(bool status)
        {
            if (Session["Mode"].ToString() == "NEW")
                {
                    ibPrint.Enabled = status;
                }
                foreach (Control c in upComplaint.Controls)
                {
                    foreach (Control ctrl in c.Controls)
                    {
                        if (ctrl is TextBox)

                            ((TextBox)ctrl).Enabled = status;

                        else if (ctrl is Button)

                            ((Button)ctrl).Enabled = status;

                        else if (ctrl is RadioButton)

                            ((RadioButton)ctrl).Enabled = status;

                        else if (ctrl is ImageButton)

                            ((ImageButton)ctrl).Visible = status;
                        else if (ctrl is System.Web.UI.WebControls.Image)

                            ((System.Web.UI.WebControls.Image)ctrl).Visible = status;

                        else if (ctrl is CheckBox)

                            ((CheckBox)ctrl).Enabled = status;

                        else if (ctrl is DropDownList)

                            ((DropDownList)ctrl).Enabled = status;

                        else if (ctrl is HyperLink)
                            ((HyperLink)ctrl).Enabled = status;
                        else if (ctrl is PlaceHolder)
                        {
                            if (ctrl.ID == "pnPenalty")
                            {
                                foreach (UserControl_BCADPenaltyControl control in ctrl.Controls)
                                {
                                    control.Enable = status;
                                }
                            }
                        }
                    }
                }          

        }
        #endregion
        #region Page Events
        /// <summary>
        /// This button click event will redirect the page to the dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCaseList_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }
        protected void btnPrint_Click(object sender, ImageClickEventArgs e)
        {

        }
        /// <summary>
        /// This click event will save the BCAD complaint data to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, ImageClickEventArgs e)
        {            
                if (Convert.ToInt16(ddlCaseStatus.SelectedItem.Value) == 0)
                {
                    lbMessage.Text = "Please Select Appropriate [Case Status] Option !";
                    lbMessage.ForeColor = System.Drawing.Color.Green;
                
                }
                else
                    SaveBCADComplaint();          
        }
        /// <summary>
        /// This is a dropdown list change event that Allows the user to Reopen the Case.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCaseStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
               if (ddlCaseStatus.SelectedItem.Value == "96" && Session["Mode"].ToString() == "VIEW")
                {
                    if (bcadComplaint == null)
                        bcadComplaint = (BCADComplaint)Session["BCADComplaint"];
                    bcadComplaint.FinalStatusCD = Convert.ToInt16(ddlCaseStatus.SelectedItem.Value);
                    bcadComplaint.SetBcadStatus();
                    Session["Mode"] = "EDIT";
                    ScriptManager.RegisterStartupScript(ddlCaseStatus, this.GetType(), "msg", String.Format("<script>alert('Case Reopened!');document.location ='BCADCaseManagement.aspx?ID={0}';</script>", bcadComplaint.ComplaintId), false);
                }           
        }
        /// <summary>
        /// This event allows the user to create a new penalty 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewPenalty_Click(object sender, ImageClickEventArgs e)
        {            
                AddPenaltyControl();           
        }
        #endregion

        /// <summary>
        /// Added this event handler to resolve TFS#7644
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlNonPltyRslt_SelectedIndexChanged(object sender, EventArgs e)
        {
            /*Resolved TFS 8599 */
           
                ddlCaseStatus.SelectedItem.Selected = false;
                if (ddlNonPltyRslt.SelectedItem.Value == Convert.ToInt32(DS249Enums.NonPenaltyResults.VOID_VOID).ToString())
                {                   
                    ddlCaseStatus.Items.FindByValue(Convert.ToInt32(DS249Enums.CaseStatues.Closed).ToString()).Selected = true;                    
                    ddlCaseStatus.Enabled = false;
                }
                else
                {
                    ddlCaseStatus.Items.FindByValue(hdnValue.Value).Selected = true;
                    ddlCaseStatus.Enabled = true;
                }        
        }
    }
}