﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.Sql;
using System.Text;
using DSNY.DSNYCP.ClassHierarchy;
using System.Configuration;
/// <summary>
/// This class provides the functionality for events and methods for the BCAD Dashboard.
/// /// /// //***Changes*** 2.0.9 AFTER
/// ComplaintStatusDate--RoutedDate
/// </summary>
namespace DSNY.DSNYCP.DS249
{
    public partial class BCAD_DashBoard : System.Web.UI.Page
    {


        #region Page Level Variables

        ComplaintList complaintList;
        PersonnelList personnelList;
        BCADComplaint bcadComplaint;
        
        User user;

        #endregion

        private const string ASCENDING = " ASC";
        private const string DESCENDING = " DESC";
       

        /// <summary>
        /// THis property Gets total no of pages
        /// </summary>
        protected int? TotalPages
        {
            get { return (int?)ViewState["tp"]; }
            set { ViewState["tp"] = value; }
        }

        /// <summary>
        ///  This method sorts the databound items in a gridview
        /// </summary>
        public SortDirection GridViewSortDirection
        {
            get
            {
                if (ViewState["sortDirection"] == null)
                    ViewState["sortDirection"] = SortDirection.Ascending;

                return (SortDirection)ViewState["sortDirection"];
            }
            set { ViewState["sortDirection"] = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if(!this.DesignMode)
            Pager1.OnPageChange += delegate(int nextPage, string pagerQueryKey)
            {
                if (txtSearch.Text.Length > 0)
                {
                    SearchComplaints(true, nextPage);
                }
                else
                {
                    PopulateGrid(nextPage);
                }
            };
        }

        /// <summary>
        /// This event is fired when the page is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
                btnRef.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_complaint_roll.png'");
                btnRef.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_complaint.png'");
                btnSearch.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_search_roll.png'");
                btnSearch.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_search_static.png'");
                btnReport.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_report_roll.png'");
                btnReport.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_report_static.png'");

                user = (User)Session["clsUser"];
                if (user != null)
                {
                    if (!user.IsInMemberShip(GroupName.BCAD))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                    else
                    {
                        user.CurrentMembership = GroupName.BCAD;
                        SecurityUtility.AttachRolesToUser(user);
                    }

                    if (!User.IsInRole("Read"))
                        Response.Redirect("../UI/UnAuthorized.aspx");
                }
                else
                    Response.Redirect("../UI/Login.aspx");


                if (Session["IsAuthenticated"] == null)
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }
                if (Session["IsAuthenticated"].ToString() == "false")
                {
                    Response.Redirect("../UI/login.aspx?Session=False");
                }
                Page.Form.DefaultButton = btnSearch.UniqueID;
                CleanSession();
                DisplayButtonBasedOnPermission();
                lbMessage.Text = String.Empty;
                if (!IsPostBack)
                {
                    PopulateGrid(gvComplaints.PageIndex);
                }
                Pager1.Visible = true;           
            
        }

        public void DisableJumpTextBox()
        {
            if (Pager1.TotalPages <= 8)
            {
                Pager1.txtJumptoPage.Enabled = false;
            }
            else
            {
                Pager1.txtJumptoPage.Enabled = true;
            }
        }

        #region Private Methods
        /// <summary>
        /// This method displays the button based on the membership role permissions
        /// </summary>
        private void DisplayButtonBasedOnPermission()
        {
            if (!User.IsInRole("Report"))
                btnReport.Visible = false;
        }
        /// <summary>
        /// This method cleans the sessions
        /// </summary>
        private void CleanSession()
        {            
                Session["BCADComplaint"] = null;
                Session["BCADPersonnel"] = null;
                Session["bcadPenaltyList"] = null;
                Session["MedicalComplaint"] = null;
                Session["MedicalPersonnel"] = null;                
                Session["medicalPenaltyList"] = null;
                Session["ComplainantList"] = null;
                Session["voilationList"] = null;           
        }
        /// <summary>
        /// This method populates the grid view
        /// </summary>
        private void PopulateGrid(int pageNo)
        {          
                int maximumRows = Convert.ToInt32(ConfigurationManager.AppSettings["MaximumRows"]);
                DataTable dt = MergeData(pageNo, maximumRows);
                DataView dv = new DataView(dt);
                ViewState["gridData"] = dt;
                gvComplaints.DataSource = dv;
                gvComplaints.DataBind();
                ComplaintList complaintList = new ComplaintList();
                TotalPages = complaintList.LoadBCADListCount();               
                Pager1.TotalResults = this.TotalPages.Value;
                Pager1.BindPager();                      
        }
        /// <summary>
        /// This method loads the BCAD complaints
        /// </summary>
        private void LoadComplaints(int pageNo, int maximumRows)
        {            
                Int64 rowsCount=0;
                complaintList = new ComplaintList();
                complaintList.Load(RequestSource.BCAD, pageNo, maximumRows,ref rowsCount);           
        }
        /// <summary>
        /// This method loads the Personnels info
        /// </summary>
        private void LoadPersonnels()
        {           
                personnelList = new PersonnelList();
                personnelList.Load(true);           
        }
        /// <summary>
        /// This method bind the data from the dataset to the data table
        /// </summary>
        /// <returns></returns>
        private DataTable MergeData(int pageNo, int maximumRows)
        {            
                LoadComplaints(pageNo, maximumRows);
                DataTable dt = new DataTable();               
                dt.Columns.Add("IncidentDate", Type.GetType("System.DateTime"));
                dt.Columns.Add("ComplaintID", Type.GetType("System.Int64"));
                dt.Columns.Add("IndexNo", Type.GetType("System.Int64"));
                dt.Columns.Add("ComplaintStatus", Type.GetType("System.Int16"));
                //***Changes*** 2.0.9 AFTER
                dt.Columns.Add("ComplaintStatusDate", Type.GetType("System.DateTime"));
                dt.Columns.Add("LocationName", Type.GetType("System.String"));
                dt.Columns.Add("FirstName", Type.GetType("System.String"));
                dt.Columns.Add("LastName", Type.GetType("System.String"));
                dt.Columns.Add("RefNo", Type.GetType("System.String"));
                dt.Columns.Add("CreatedBy", Type.GetType("System.String"));
                dt.Columns.Add("Status", Type.GetType("System.String"));
                dt.Columns.Add("Charge", Type.GetType("System.String"));
                DataRow dr;
                foreach (Complaint complaint in complaintList.List)
                {
                    dr = dt.NewRow();                   
                    dr["IncidentDate"] = complaint.IncidentDate.ToShortDateString();
                    dr["ComplaintID"] = complaint.ComplaintId.ToString();
                    dr["IndexNo"] = complaint.IndexNo.ToString();
                    dr["ComplaintStatus"] = complaint.ComplaintStatus;
                    //***Changes*** 2.0.9 AFTER
                    dr["ComplaintStatusDate"] = complaint.ComplaintStatusDate;
                    dr["LocationName"] = complaint.LocationName;
                    dr["RefNo"] = complaint.ReferenceNumber;
                    dr["CreatedBy"] = complaint.CreatedBy;
                    dr["Status"] = complaint.Status;
                    dr["FirstName"] = complaint.InitialAndLastName.Trim();
                    dr["LastName"] = complaint.RespondentLastName.Trim();
                    dr["Charge"] = complaint.Charge.Trim();
                    dt.Rows.Add(dr);
                }
                return dt;            
        }
        /// <summary>
        ///  Populates the data to the data table
        /// </summary>
        /// <returns></returns>
        private DataTable PopulateTable(int pageNo)
        {
            int maximumRows = Convert.ToInt32(ConfigurationManager.AppSettings["MaximumRows"]);
                DataTable dt;
                dt = MergeData(pageNo, maximumRows);
                return dt;            
        }
        /// <summary>
        /// Returns true if the search is sucessfull
        /// </summary>
        /// <param name="Search"></param>
        private void SearchComplaints(bool Search, int pageNo)
        {           
                int maximumRows = 50;
                int numericValue = 0;
                string decimalValue = "";
                string dateValue = "";
                string stringValue = "";
                bool isDecimal;
                decimal bal;
                isDecimal = decimal.TryParse(txtSearch.Text.Trim(), out bal);              
                if (IsNumeric(txtSearch.Text.Trim()))
                {
                    numericValue = Convert.ToInt32(txtSearch.Text.Trim());
                }
                else if (isDecimal)
                {
                    decimalValue = txtSearch.Text.Trim();
                }
                else if (IsDate(txtSearch.Text.Trim()))
                {
                    DateTime myDateTime = DateTime.Parse(txtSearch.Text.Trim());
                    dateValue = Convert.ToString(myDateTime);
                    dateValue = dateValue.Substring(0, dateValue.IndexOf(" "));

                    if (dateValue.Substring(0, dateValue.IndexOf("/")).Length == 1)
                    {
                        dateValue = dateValue.Insert(0, "0");
                    }
                    string strTemp1 = dateValue;
                    strTemp1 = strTemp1.Remove(0, dateValue.IndexOf("/") + 1);
                    if (strTemp1.Substring(0, strTemp1.IndexOf("/")).Length == 1)
                    {
                        dateValue = dateValue.Insert(dateValue.IndexOf("/") + 1, "0");
                    }
                }
                else
                {
                    stringValue = txtSearch.Text.Trim();
                    if (stringValue.Contains("'") == true || stringValue.Contains(" "))
                    {
                        stringValue = stringValue.Replace("'", "");
                        stringValue = stringValue.Replace(" ", "");
                    }
                }                                            

                ComplaintList complaintList = new ComplaintList();
                List<Complaint> objComplaintList = complaintList.LoadCadSearchComplaintList(pageNo, maximumRows, numericValue, decimalValue, dateValue, stringValue);                
                DataTable dt = MergeSearchData(objComplaintList);
                DataView dv;
                dv = new DataView(dt);
                ViewState["gridData"] = dt;
                gvComplaints.DataSource = dv;
                gvComplaints.DataBind();
                TotalPages = complaintList.LoadSearchCadListCount(numericValue, decimalValue, dateValue, stringValue);
                if (TotalPages == 0)
                {
                    Pager1.Visible = false;                   
                }
                else
                {
                    Pager1.Visible = true;
                }
                Pager1.TotalResults = this.TotalPages.Value;
                Pager1.BindPager();               
           
        }

        /// <summary>
        /// This method bind the data from the dataset to the data table
        /// </summary>
        /// <returns></returns>
        private DataTable MergeSearchData(List<Complaint> objComplaintList)
        {                       
                DataTable dt = new DataTable();               
                dt.Columns.Add("IncidentDate", Type.GetType("System.DateTime"));
                dt.Columns.Add("ComplaintID", Type.GetType("System.Int64"));
                dt.Columns.Add("IndexNo", Type.GetType("System.Int64"));
                dt.Columns.Add("ComplaintStatus", Type.GetType("System.Int16"));
                //***Changes*** 2.0.9 AFTER
                dt.Columns.Add("ComplaintStatusDate", Type.GetType("System.DateTime"));
                dt.Columns.Add("LocationName", Type.GetType("System.String"));
                dt.Columns.Add("FirstName", Type.GetType("System.String"));
                dt.Columns.Add("LastName", Type.GetType("System.String"));
                dt.Columns.Add("RefNo", Type.GetType("System.String"));
                dt.Columns.Add("CreatedBy", Type.GetType("System.String"));
                dt.Columns.Add("Status", Type.GetType("System.String"));
                dt.Columns.Add("Charge", Type.GetType("System.String"));
                DataRow dr;
                foreach (Complaint complaint in objComplaintList)
                {
                    dr = dt.NewRow();                   
                    dr["IncidentDate"] = complaint.IncidentDate.ToShortDateString();
                    dr["ComplaintID"] = complaint.ComplaintId.ToString();
                    dr["IndexNo"] = complaint.IndexNo.ToString();
                    dr["ComplaintStatus"] = complaint.ComplaintStatus;
                    //***Changes*** 2.0.9 AFTER
                    dr["ComplaintStatusDate"] = complaint.ComplaintStatusDate.ToShortDateString();
                    dr["LocationName"] = complaint.LocationName;
                    dr["RefNo"] = complaint.ReferenceNumber;
                    dr["CreatedBy"] = complaint.CreatedBy;
                    dr["Status"] = complaint.Status;
                    dr["FirstName"] = complaint.InitialAndLastName.Trim();
                    dr["LastName"] = complaint.RespondentLastName.Trim();
                    dr["Charge"] = complaint.Charge.Trim();
                    dt.Rows.Add(dr);
                }
                return dt;          
        }
        
        /// <summary>
        /// This method returns true if the search text entered is numeric
        /// </summary>
        /// <param name="strTextEntry"></param>
        /// <returns></returns>
        private bool IsNumeric(string strTextEntry)
        {          
                Regex objNotWholePattern = new Regex("[^0-9]");
                return !objNotWholePattern.IsMatch(strTextEntry);            
        }
        /// <summary>
        /// This method returns true if the search text entered is a valid date
        /// </summary>
        /// <param name="strDate"></param>
        /// <returns></returns>
        public bool IsDate(string strDate)
        {
            return ClassHierarchy.Utilities.CommonFormats.IsDate(strDate);
        }

        /// <summary>
        /// Returns string by concatinating the data in the search text box
        /// </summary>
        /// <returns></returns>
        private String GetSearchString()
        {
           
                StringBuilder searchString = new StringBuilder();
                searchString.Append(String.Format("RespondentFName Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("RespondentLName Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("RespondentRefNumber Like '%{0}%'", txtSearch.Text.Trim()));
                searchString.Append(" OR ");
                searchString.Append(String.Format("LocationName Like '%{0}%'", txtSearch.Text.Trim()));                
                searchString.Append(" OR ");
                searchString.Append(String.Format("CreatedBy Like '%{0}%'", txtSearch.Text.Trim()));
                return searchString.ToString();            
        }
        #endregion
        /// <summary>
        /// This is a text change event that fires when the user enters the reference number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void btnRef_Click(object sender, ImageClickEventArgs e)
        {           
                txtSearch.Text = String.Empty;
                Pager1.CurrentPage = gvComplaints.PageIndex;
                PopulateGrid(gvComplaints.PageIndex);
                lbMessage.Text = "Data Refreshed";
           
        }
        protected void btnRep_Click(object sender, ImageClickEventArgs e)
        {

        }
        /// <summary>
        /// This is a text change event that fires when the user enters the search criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            
                if ((!String.IsNullOrEmpty(Pager1.txtJumptoPage.Text)) && (String.IsNullOrEmpty(txtSearch.Text)))
                {
                    if ((Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()) > 0) && (Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()) <= ((this.TotalPages / 50) + 1)))                   
                    {
                        Pager1.CurrentPage = Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim());
                        PopulateGrid(Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()));
                        Pager1.txtJumptoPage.Text = "";
                    }
                    else
                    {
                        lbMessage.Text = "Invalid Page Index Entered.";
                    }
                }
                else if ((!String.IsNullOrEmpty(Pager1.txtJumptoPage.Text)) && (!String.IsNullOrEmpty(txtSearch.Text)))
                {
                    if ((Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()) > 0) && (Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()) <= ((this.TotalPages / 50) + 1)))                  
                    {
                        Pager1.CurrentPage = Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim());
                        SearchComplaints(true, Convert.ToInt32(Pager1.txtJumptoPage.Text.Trim()));
                    }
                    else
                    {
                        lbMessage.Text = "Invalid Page Index Entered.";
                    }
                }
                else
                {
                    if (txtSearch.Text.Length > 0)
                    {
                        Pager1.CurrentPage = 0;
                        SearchComplaints(true,0);
                    }
                    else
                    {
                        Pager1.CurrentPage = gvComplaints.PageIndex;
                        PopulateGrid(gvComplaints.PageIndex);                        
                    }
                }            
        }
        /// <summary>
        /// This is a text change event that fires when the user enters the search criteria
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearchCancel_Click(object sender, ImageClickEventArgs e)
        {
           
                txtSearch.Text = String.Empty;
                SearchComplaints(false, 0);        
            
        }

        #region GridView Events
        /// <summary>
        /// The RowCommand event is raised when a button is clicked in the GridView control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        
        protected void gvComplaints_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            
                if (e.CommandName == "Edit")
                {
                    String URL = String.Format("BCADCaseManagement.aspx?Id={0}", e.CommandArgument.ToString());
                    Session["Mode"] = "EDIT";
                    Response.Redirect(URL);
                }
                if (e.CommandName == "Open")
                {
                bcadComplaint = new BCADComplaint
                {
                    ComplaintId = Convert.ToInt64(e.CommandArgument.ToString()),
                    CreateUser = Convert.ToInt16(Session["UserEmpID"])
                };
                if (bcadComplaint.CreateNew())
                    {

                        PopulateGrid(gvComplaints.PageIndex);
                        lbMessage.Text = String.Format("Complaint Successfully Received.");
                    }
                    else
                    {
                        lbMessage.Text = String.Format("Failed to Receive Complaint");
                    }
                }
                if (e.CommandName == "View")
                {
                    String URL = String.Format("BCADCaseManagement.aspx?Id={0}", e.CommandArgument.ToString());
                    Session["Mode"] = "VIEW";
                    Response.Redirect(URL);
                }          

        }
       
        /// <summary>
        /// This event is fired when a data row is bound to data in a GridView control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[8].Visible = false;
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    switch (e.Row.Cells[4].Text)
                    {
                        case "UnRead":
                            if (User.IsInRole("Write"))
                                e.Row.FindControl("btnOpen").Visible = true;
                            else
                                e.Row.FindControl("btnView").Visible = true;
                            break;
                        case "Advocate":
                            e.Row.FindControl("lbAdvocate").Visible = true;
                            break;
                        case "Open":
                            if (User.IsInRole("Write"))
                                e.Row.FindControl("btnEdit").Visible = true;
                            else
                                e.Row.FindControl("btnView").Visible = true;
                            break;
                        case "Closed":
                            e.Row.FindControl("btnView").Visible = true;
                            break;
                        default:
                            break;
                    }

                    if (e.Row.Cells[0].Text != null)
                    {
                        ImageButton Edit = (ImageButton)e.Row.FindControl("btnEdit");
                        ImageButton View = (ImageButton)e.Row.FindControl("btnView");
                        ImageButton Open = (ImageButton)e.Row.FindControl("btnOpen");

                        Open.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_dash_open_roll.png'");
                        Open.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_dash_open_static.png'");

                        Edit.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_edit_roll.png'");
                        Edit.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_edit_static.png'");

                        View.Attributes.Add("onmouseover", "this.src='../includes/img/ds249_btn_view_roll.png'");
                        View.Attributes.Add("onmouseout", "this.src='../includes/img/ds249_btn_view_static.png'");
                    }                  

                    e.Row.Cells[8].Visible = false;
                }           
        }              
        /// <summary>
        /// Occurs when the hyperlink to sort a column is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_Sorting(object sender, GridViewSortEventArgs e)
        {
                   string sortExpression = e.SortExpression;
                if (GridViewSortDirection == SortDirection.Descending)
                {
                    GridViewSortDirection = SortDirection.Ascending;
                    SortGridView(sortExpression, ASCENDING);
                }
                else
                {
                    GridViewSortDirection = SortDirection.Descending;
                    SortGridView(sortExpression, DESCENDING);
                }        

        }


        /// <summary>
        /// Returns true if the search is sucessfull
        /// </summary>
        /// <param name="sortExpression"></param>
        /// <param name="direction"></param>
        private void SortGridView(string sortExpression, string direction)
        {
            DataView dv = new DataView((DataTable)ViewState["gridData"])
            {
                Sort = sortExpression + direction
            };
            gvComplaints.DataSource = dv;
                gvComplaints.DataBind();           
        }          

        
        /// <summary>
        /// This is a text change event that fires when the user enters the page number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtJmpToPage_TextChanged(object sender, EventArgs e)
        {           

        }

        /// <summary>
        /// For each GridView row is databound, the GridView's RowDataBound event is fired
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void gvComplaints_DataBound(object sender, EventArgs e)
        {
            

        }
        /// <summary>
        /// This event Occurs when a menu item in a Menu control is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void menuPager_MenuItemClick(object sender, MenuEventArgs e)
        {
           
        }
        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Complaints/Dashboard.aspx");
        }
        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Dashboard.aspx");
        }
        /// <summary>
        /// This button click events redirects the page to Dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Advocate/Dashboard.aspx");
        }

        #endregion
        /// <summary>
        /// This button click event will redirect the page to the reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReport_Click(object sender, ImageClickEventArgs e)
        {

        }
    }
}