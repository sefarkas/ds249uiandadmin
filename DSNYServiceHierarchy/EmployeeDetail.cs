﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace DSNYServiceHierarchy
{
    [DataContract]
    public class EmployeeDetail
  {
        [DataMember]     
        public string FirstName;
        [DataMember]   
        public string MiddleName;
        [DataMember]    
        public string LastName;
        [DataMember] 
        public string ReferenceNo;
        [DataMember]
        public string SSN4;

    }
}
