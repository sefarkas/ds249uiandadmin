﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using DSNY.DSNYCP.ClassHierarchy;
namespace DSNYServiceHierarchy
{
    // NOTE: If you change the class name "DS249Service" here, you must also update the reference to "DS249Service" in App.config.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class DS249Service : IDS249Service
    {
        DSNY.DSNYCP.ClassHierarchy.PersonelDetailsWS classHierarchyPersonellWS = new DSNY.DSNYCP.ClassHierarchy.PersonelDetailsWS();

        #region IDS249Service Members
        public List<EmployeeDetail> GetEmployeesDetail(string pageNo, string pageSize, string firstName, string middleName, string lastName, string referenceNo, string ssnNo)
        {
            List<DSNY.DSNYCP.ClassHierarchy.PersonnelWs> classHierarchyPWS = new List<DSNY.DSNYCP.ClassHierarchy.PersonnelWs>();



            if (string.IsNullOrEmpty(pageNo))
                pageNo = "0";
            if (string.IsNullOrEmpty(pageSize))
                pageSize = "5";

            if (string.IsNullOrEmpty(firstName))
                firstName = "%%";
            else
                firstName = '%' + firstName + '%';
            if (string.IsNullOrEmpty(middleName))
                middleName = "%%";
            else
                middleName = '%' + middleName.Trim() + '%';

            if (string.IsNullOrEmpty(lastName))
                lastName = "%%";
            else
                lastName = '%' + lastName.Trim() + '%';




            if (string.IsNullOrEmpty(referenceNo))
                referenceNo = "%%";
            else
                referenceNo = '%' + referenceNo.Trim() + '%';


            if (string.IsNullOrEmpty(ssnNo))
                ssnNo = "%%";
            else
                ssnNo = '%' + ssnNo.Trim() + '%';



            classHierarchyPWS = classHierarchyPersonellWS.GetPersonelDataWS(Convert.ToInt32(pageNo), Convert.ToInt32(pageSize), firstName, middleName, lastName, referenceNo, ssnNo);



            List<EmployeeDetail> returnPWS = new List<EmployeeDetail>();
            foreach (DSNY.DSNYCP.ClassHierarchy.PersonnelWs clH in classHierarchyPWS)
            {
                EmployeeDetail pdWS = new EmployeeDetail();
                pdWS.FirstName = clH.FirstName;
                pdWS.MiddleName = clH.MiddleName;
                pdWS.LastName = clH.LastName;
                pdWS.ReferenceNo = clH.ReferenceNo;
                pdWS.SSN4 = clH.SSNo;

                returnPWS.Add(pdWS);
            }
            return returnPWS;
        }

    //    public List<EmployeesDetailWS> GetEmployeesDetails(string pageNo, string pageSize, string firstName, string middleName, string lastName, string referenceNo,string ssnNo)
        public List<EmployeeDetail> GetEmployeesDetails()
        {

            string pageNo = QueryString.GetString("Pageno");
            string pageSize = QueryString.GetString("pageSize");

            string firstName = QueryString.GetString("FirstName");
            string middleName = QueryString.GetString("MiddleName");

            string lastName = QueryString.GetString("LastName");
            string referenceNo = QueryString.GetString("ReferenceNo");

            string ssnNo = QueryString.GetString("SSN4");


            if (string.IsNullOrEmpty(pageNo))
                pageNo = "0";
            if (string.IsNullOrEmpty(pageSize))
                pageSize = "5";

            if (string.IsNullOrEmpty(firstName))
                firstName = "%%";
            else
                firstName = '%' + firstName + '%';
            if (string.IsNullOrEmpty(middleName))
                middleName = "%%";
            else
                middleName = '%' + middleName.Trim() + '%';

            if (string.IsNullOrEmpty(lastName))
                lastName = "%%";
            else
                lastName = '%' + lastName.Trim() + '%';




            if (string.IsNullOrEmpty(referenceNo))
                referenceNo = "%%";
            else
                referenceNo = '%' + referenceNo.Trim() + '%';


            if (string.IsNullOrEmpty(ssnNo))
                ssnNo = "%%";
            else
                ssnNo = '%' + ssnNo.Trim() + '%';



            List<DSNY.DSNYCP.ClassHierarchy.PersonnelWs> classHierarchyPWS = new List<DSNY.DSNYCP.ClassHierarchy.PersonnelWs>();

          classHierarchyPWS = classHierarchyPersonellWS.GetPersonelDataWS(Convert.ToInt32(pageNo),Convert.ToInt32(pageSize), firstName, middleName, lastName, referenceNo,ssnNo);



          List<EmployeeDetail> returnPWS = new List<EmployeeDetail>();
            foreach (DSNY.DSNYCP.ClassHierarchy.PersonnelWs clH in classHierarchyPWS)
            {
                EmployeeDetail pdWS = new EmployeeDetail();
                pdWS.FirstName = clH.FirstName;
                pdWS.MiddleName = clH.MiddleName;
                pdWS.LastName = clH.LastName;
                pdWS.ReferenceNo = clH.ReferenceNo;
                pdWS.SSN4 = clH.SSNo;

                returnPWS.Add(pdWS);
            }
            return returnPWS;
        }

        #endregion
    }
}
