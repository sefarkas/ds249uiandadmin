﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Web;
using System.Collections;

namespace DSNYServiceHierarchy
{
    // NOTE: If you change the interface name "IDS249Service" here, you must also update the reference to "IDS249Service" in App.config.


    [ServiceContract]
    //[XmlSerializerFormat]
    public interface IDS249Service
    {
        [OperationContract]
        [WebGet(UriTemplate = "/employees")]

        List<EmployeeDetail> GetEmployeesDetails();

        List<EmployeeDetail> GetEmployeesDetail(string pageNo, string pageSize, string firstName, string middleName, string lastName, string referenceNo, string SSN);
   
    }
}
