﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DSNYServiceHierarchy
{
    // NOTE: If you change the interface name "IPMDService" here, you must also update the reference to "IPMDService" in App.config.
    [ServiceContract]
    public interface IPMDService
    {
        [OperationContract]
        void DoWork();
    }
}
